﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>



// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87;
// System.Action`1<System.String>
struct Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// System.String
struct String_t;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// WebViewObject
struct WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2;

IL2CPP_EXTERN_C RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tB4B8BFF6E6672C9C5CA86F52CA248539A4A5120B 
{
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87  : public MulticastDelegate_t
{
};

// System.Action`1<System.String>
struct Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A  : public MulticastDelegate_t
{
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// WebViewObject
struct WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Action`1<System.String> WebViewObject::onJS
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onJS_4;
	// System.Action`1<System.String> WebViewObject::onError
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onError_5;
	// System.Action`1<System.String> WebViewObject::onHttpError
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onHttpError_6;
	// System.Action`1<System.String> WebViewObject::onStarted
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onStarted_7;
	// System.Action`1<System.String> WebViewObject::onLoaded
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onLoaded_8;
	// System.Action`1<System.String> WebViewObject::onHooked
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onHooked_9;
	// System.Action`1<System.String> WebViewObject::onCookies
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onCookies_10;
	// System.Boolean WebViewObject::visibility
	bool ___visibility_11;
	// System.Boolean WebViewObject::alertDialogEnabled
	bool ___alertDialogEnabled_12;
	// System.Boolean WebViewObject::scrollBounceEnabled
	bool ___scrollBounceEnabled_13;
	// System.Int32 WebViewObject::mMarginLeft
	int32_t ___mMarginLeft_14;
	// System.Int32 WebViewObject::mMarginTop
	int32_t ___mMarginTop_15;
	// System.Int32 WebViewObject::mMarginRight
	int32_t ___mMarginRight_16;
	// System.Int32 WebViewObject::mMarginBottom
	int32_t ___mMarginBottom_17;
	// System.Boolean WebViewObject::mMarginRelative
	bool ___mMarginRelative_18;
	// System.Single WebViewObject::mMarginLeftComputed
	float ___mMarginLeftComputed_19;
	// System.Single WebViewObject::mMarginTopComputed
	float ___mMarginTopComputed_20;
	// System.Single WebViewObject::mMarginRightComputed
	float ___mMarginRightComputed_21;
	// System.Single WebViewObject::mMarginBottomComputed
	float ___mMarginBottomComputed_22;
	// System.Boolean WebViewObject::mMarginRelativeComputed
	bool ___mMarginRelativeComputed_23;
	// System.IntPtr WebViewObject::webView
	intptr_t ___webView_24;
};

// <Module>

// <Module>

// System.String
struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.String

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Boolean

// System.Int32

// System.Int32

// System.IntPtr
struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.IntPtr

// System.Single

// System.Single

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector2

// System.Void

// System.Void

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};

// UnityEngine.Object

// System.Action`1<System.Object>

// System.Action`1<System.Object>

// System.Action`1<System.String>

// System.Action`1<System.String>

// UnityEngine.MonoBehaviour

// UnityEngine.MonoBehaviour

// WebViewObject

// WebViewObject
#ifdef __clang__
#pragma clang diagnostic pop
#endif


// System.Void System.Action`1<System.Object>::Invoke(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___0_obj, const RuntimeMethod* method) ;

// System.Boolean UnityEngine.TouchScreenKeyboard::get_visible()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TouchScreenKeyboard_get_visible_m5994AFC21D2C48F997FC3DECF1235B0A620B0B63 (const RuntimeMethod* method) ;
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* __this, const RuntimeMethod* method) ;
// System.IntPtr WebViewObject::_CWebViewPlugin_Init(System.String,System.Boolean,System.Boolean,System.String,System.Boolean,System.Int32,System.Boolean,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t WebViewObject__CWebViewPlugin_Init_m486E39DB9D85CB93AE99844C240EA3B53A676AB3 (String_t* ___0_gameObject, bool ___1_transparent, bool ___2_zoom, String_t* ___3_ua, bool ___4_enableWKWebView, int32_t ___5_wkContentMode, bool ___6_wkAllowsLinkPreview, bool ___7_wkAllowsBackForwardNavigationGestures, int32_t ___8_radius, const RuntimeMethod* method) ;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271 (intptr_t ___0_value1, intptr_t ___1_value2, const RuntimeMethod* method) ;
// System.Int32 WebViewObject::_CWebViewPlugin_Destroy(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WebViewObject__CWebViewPlugin_Destroy_m7B8B2C6F90746C56A96C9BB53568927617176498 (intptr_t ___0_instance, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_SetSuspended(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SetSuspended_mF28265A58D9F0A667409706F04F303CAEB86A7C6 (intptr_t ___0_instance, bool ___1_suspended, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_mF608FF3252213E7EFA1F0D2F744C28110E9E5AC9 (const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_m01A3102DE71EE1FBEA51D09D6B0261CF864FE8F9 (const RuntimeMethod* method) ;
// System.Void WebViewObject::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_SetMargins_m1D6F1FD6D8317D2FCD550768B9D794B544564366 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, int32_t ___0_left, int32_t ___1_top, int32_t ___2_right, int32_t ___3_bottom, bool ___4_relative, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_SetMargins(System.IntPtr,System.Single,System.Single,System.Single,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SetMargins_m072CE271F87DDFFF0E134DB1D3CA837D82B9A7BF (intptr_t ___0_instance, float ___1_left, float ___2_top, float ___3_right, float ___4_bottom, bool ___5_relative, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_SetVisibility(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SetVisibility_m400B0F30A5FD5EA774B3BCCE3AF5A05F71A784A6 (intptr_t ___0_instance, bool ___1_visibility, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_SetScrollbarsVisibility(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SetScrollbarsVisibility_m2476178AC1BE9368F8AFF26112B4FEEE003B89C7 (intptr_t ___0_instance, bool ___1_visibility, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_SetInteractionEnabled(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SetInteractionEnabled_mB4505783D1F543FAFAD12086D794E9456F1BDC80 (intptr_t ___0_instance, bool ___1_enabled, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_SetAlertDialogEnabled(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SetAlertDialogEnabled_mEFE2F2DD64EE0893B37054CC08DFB106A38BD971 (intptr_t ___0_instance, bool ___1_enabled, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_SetScrollBounceEnabled(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SetScrollBounceEnabled_mEF514F8022A350897D8DBBBE746D40D29654CFF3 (intptr_t ___0_instance, bool ___1_enabled, const RuntimeMethod* method) ;
// System.Boolean WebViewObject::_CWebViewPlugin_SetURLPattern(System.IntPtr,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebViewObject__CWebViewPlugin_SetURLPattern_m7FAF1AFDB4657A57BBA0C5750092A69342F9DF41 (intptr_t ___0_instance, String_t* ___1_allowPattern, String_t* ___2_denyPattern, String_t* ___3_hookPattern, const RuntimeMethod* method) ;
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478 (String_t* ___0_value, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_LoadURL(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_LoadURL_mF3BC67D423B4A3132E20E77F68DD8F2DCC40210D (intptr_t ___0_instance, String_t* ___1_url, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_LoadHTML(System.IntPtr,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_LoadHTML_m2C486C41677C6444F57DF0D79E37FF706851CD83 (intptr_t ___0_instance, String_t* ___1_html, String_t* ___2_baseUrl, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_EvaluateJS(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_EvaluateJS_mE1DFDCCF574554A64D67C4712574837F276736FA (intptr_t ___0_instance, String_t* ___1_url, const RuntimeMethod* method) ;
// System.Int32 WebViewObject::_CWebViewPlugin_Progress(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WebViewObject__CWebViewPlugin_Progress_mAFC67F67913E8D8D3784BF54C1255889DA073D4B (intptr_t ___0_instance, const RuntimeMethod* method) ;
// System.Boolean WebViewObject::_CWebViewPlugin_CanGoBack(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebViewObject__CWebViewPlugin_CanGoBack_m26BBE9F33159F0D4134E5672B895446502BD73D4 (intptr_t ___0_instance, const RuntimeMethod* method) ;
// System.Boolean WebViewObject::_CWebViewPlugin_CanGoForward(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebViewObject__CWebViewPlugin_CanGoForward_mEE9462A38A2A46F34CB10751A411B44465FAD710 (intptr_t ___0_instance, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_GoBack(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_GoBack_mC15488444CB417A3F9EF294F29CFE4ED65DA8857 (intptr_t ___0_instance, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_GoForward(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_GoForward_mD8F46910E09B394F727F086FA653C806B16B5791 (intptr_t ___0_instance, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_Reload(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_Reload_m7ABD2BC31394ADFAF5AE5EE2F6FFD5ACA8B7381B (intptr_t ___0_instance, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.String>::Invoke(T)
inline void Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* __this, String_t* ___0_obj, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*, String_t*, const RuntimeMethod*))Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline)(__this, ___0_obj, method);
}
// System.String UnityEngine.Networking.UnityWebRequest::UnEscapeURL(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityWebRequest_UnEscapeURL_mF32D6DA1A428A932B9A57A48FE5FA86D4B1446BB (String_t* ___0_s, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_AddCustomHeader(System.IntPtr,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_AddCustomHeader_mAAE78B1CCE04D4A00172E844AE95ED283B14A8F2 (intptr_t ___0_instance, String_t* ___1_headerKey, String_t* ___2_headerValue, const RuntimeMethod* method) ;
// System.String WebViewObject::_CWebViewPlugin_GetCustomHeaderValue(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* WebViewObject__CWebViewPlugin_GetCustomHeaderValue_mA6E51E5AB307D643E11A297ED4D05995355A4E88 (intptr_t ___0_instance, String_t* ___1_headerKey, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_RemoveCustomHeader(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_RemoveCustomHeader_mCCE66F43A174B453751BC0EBF1986A3F1A232460 (intptr_t ___0_instance, String_t* ___1_headerKey, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_ClearCustomHeader(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_ClearCustomHeader_m1855C7017F63969B5EED4506FED160CB771DFC2A (intptr_t ___0_instance, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_ClearCookies()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_ClearCookies_m7FCC8210626A0F022BC8D6F11B2A423A7C300FD4 (const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_SaveCookies()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SaveCookies_m6E9EE5F72F0B403A199A52502AF346782636A6F4 (const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_GetCookies(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_GetCookies_mEBE776075CD1290CE067D39F7C6C75A4F8676399 (intptr_t ___0_instance, String_t* ___1_url, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_SetBasicAuthInfo(System.IntPtr,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SetBasicAuthInfo_m51AE24FCA7CEBC77FA27DD901157649EFAB1BF9A (intptr_t ___0_instance, String_t* ___1_userName, String_t* ___2_password, const RuntimeMethod* method) ;
// System.Void WebViewObject::_CWebViewPlugin_ClearCache(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_ClearCache_m3E0823A7B9908EDB08951A8858A5782D882988D7 (intptr_t ___0_instance, bool ___1_includeDiskFiles, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL _CWebViewPlugin_Init(char*, int32_t, int32_t, char*, int32_t, int32_t, int32_t, int32_t, int32_t);
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _CWebViewPlugin_Destroy(intptr_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_SetMargins(intptr_t, float, float, float, float, int32_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_SetVisibility(intptr_t, int32_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_SetScrollbarsVisibility(intptr_t, int32_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_SetAlertDialogEnabled(intptr_t, int32_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_SetScrollBounceEnabled(intptr_t, int32_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_SetInteractionEnabled(intptr_t, int32_t);
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _CWebViewPlugin_SetURLPattern(intptr_t, char*, char*, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_LoadURL(intptr_t, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_LoadHTML(intptr_t, char*, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_EvaluateJS(intptr_t, char*);
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _CWebViewPlugin_Progress(intptr_t);
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _CWebViewPlugin_CanGoBack(intptr_t);
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _CWebViewPlugin_CanGoForward(intptr_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_GoBack(intptr_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_GoForward(intptr_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_Reload(intptr_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_AddCustomHeader(intptr_t, char*, char*);
IL2CPP_EXTERN_C char* DEFAULT_CALL _CWebViewPlugin_GetCustomHeaderValue(intptr_t, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_RemoveCustomHeader(intptr_t, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_ClearCustomHeader(intptr_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_ClearCookies();
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_SaveCookies();
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_GetCookies(intptr_t, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_SetBasicAuthInfo(intptr_t, char*, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_ClearCache(intptr_t, int32_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_SetSuspended(intptr_t, int32_t);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WebViewObject::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_Awake_mB9D8F439EF77BF9402FC23AAB03F878574228048 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	{
		// alertDialogEnabled = true;
		__this->___alertDialogEnabled_12 = (bool)1;
		// scrollBounceEnabled = true;
		__this->___scrollBounceEnabled_13 = (bool)1;
		// mMarginLeftComputed = -9999;
		__this->___mMarginLeftComputed_19 = (-9999.0f);
		// mMarginTopComputed = -9999;
		__this->___mMarginTopComputed_20 = (-9999.0f);
		// mMarginRightComputed = -9999;
		__this->___mMarginRightComputed_21 = (-9999.0f);
		// mMarginBottomComputed = -9999;
		__this->___mMarginBottomComputed_22 = (-9999.0f);
		// }
		return;
	}
}
// System.Boolean WebViewObject::get_IsKeyboardVisible()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebViewObject_get_IsKeyboardVisible_mBC3BC667F8FDD8B89C55B466E7D0D1567693A173 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	{
		// return TouchScreenKeyboard.visible;
		bool L_0;
		L_0 = TouchScreenKeyboard_get_visible_m5994AFC21D2C48F997FC3DECF1235B0A620B0B63(NULL);
		return L_0;
	}
}
// System.IntPtr WebViewObject::_CWebViewPlugin_Init(System.String,System.Boolean,System.Boolean,System.String,System.Boolean,System.Int32,System.Boolean,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t WebViewObject__CWebViewPlugin_Init_m486E39DB9D85CB93AE99844C240EA3B53A676AB3 (String_t* ___0_gameObject, bool ___1_transparent, bool ___2_zoom, String_t* ___3_ua, bool ___4_enableWKWebView, int32_t ___5_wkContentMode, bool ___6_wkAllowsLinkPreview, bool ___7_wkAllowsBackForwardNavigationGestures, int32_t ___8_radius, const RuntimeMethod* method) 
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, int32_t, char*, int32_t, int32_t, int32_t, int32_t, int32_t);

	// Marshaling of parameter '___0_gameObject' to native representation
	char* ____0_gameObject_marshaled = NULL;
	____0_gameObject_marshaled = il2cpp_codegen_marshal_string(___0_gameObject);

	// Marshaling of parameter '___3_ua' to native representation
	char* ____3_ua_marshaled = NULL;
	____3_ua_marshaled = il2cpp_codegen_marshal_string(___3_ua);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_Init)(____0_gameObject_marshaled, static_cast<int32_t>(___1_transparent), static_cast<int32_t>(___2_zoom), ____3_ua_marshaled, static_cast<int32_t>(___4_enableWKWebView), ___5_wkContentMode, static_cast<int32_t>(___6_wkAllowsLinkPreview), static_cast<int32_t>(___7_wkAllowsBackForwardNavigationGestures), ___8_radius);

	// Marshaling cleanup of parameter '___0_gameObject' native representation
	il2cpp_codegen_marshal_free(____0_gameObject_marshaled);
	____0_gameObject_marshaled = NULL;

	// Marshaling cleanup of parameter '___3_ua' native representation
	il2cpp_codegen_marshal_free(____3_ua_marshaled);
	____3_ua_marshaled = NULL;

	return returnValue;
}
// System.Int32 WebViewObject::_CWebViewPlugin_Destroy(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WebViewObject__CWebViewPlugin_Destroy_m7B8B2C6F90746C56A96C9BB53568927617176498 (intptr_t ___0_instance, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_Destroy)(___0_instance);

	return returnValue;
}
// System.Void WebViewObject::_CWebViewPlugin_SetMargins(System.IntPtr,System.Single,System.Single,System.Single,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SetMargins_m072CE271F87DDFFF0E134DB1D3CA837D82B9A7BF (intptr_t ___0_instance, float ___1_left, float ___2_top, float ___3_right, float ___4_bottom, bool ___5_relative, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, float, float, float, float, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SetMargins)(___0_instance, ___1_left, ___2_top, ___3_right, ___4_bottom, static_cast<int32_t>(___5_relative));

}
// System.Void WebViewObject::_CWebViewPlugin_SetVisibility(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SetVisibility_m400B0F30A5FD5EA774B3BCCE3AF5A05F71A784A6 (intptr_t ___0_instance, bool ___1_visibility, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SetVisibility)(___0_instance, static_cast<int32_t>(___1_visibility));

}
// System.Void WebViewObject::_CWebViewPlugin_SetScrollbarsVisibility(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SetScrollbarsVisibility_m2476178AC1BE9368F8AFF26112B4FEEE003B89C7 (intptr_t ___0_instance, bool ___1_visibility, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SetScrollbarsVisibility)(___0_instance, static_cast<int32_t>(___1_visibility));

}
// System.Void WebViewObject::_CWebViewPlugin_SetAlertDialogEnabled(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SetAlertDialogEnabled_mEFE2F2DD64EE0893B37054CC08DFB106A38BD971 (intptr_t ___0_instance, bool ___1_enabled, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SetAlertDialogEnabled)(___0_instance, static_cast<int32_t>(___1_enabled));

}
// System.Void WebViewObject::_CWebViewPlugin_SetScrollBounceEnabled(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SetScrollBounceEnabled_mEF514F8022A350897D8DBBBE746D40D29654CFF3 (intptr_t ___0_instance, bool ___1_enabled, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SetScrollBounceEnabled)(___0_instance, static_cast<int32_t>(___1_enabled));

}
// System.Void WebViewObject::_CWebViewPlugin_SetInteractionEnabled(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SetInteractionEnabled_mB4505783D1F543FAFAD12086D794E9456F1BDC80 (intptr_t ___0_instance, bool ___1_enabled, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SetInteractionEnabled)(___0_instance, static_cast<int32_t>(___1_enabled));

}
// System.Boolean WebViewObject::_CWebViewPlugin_SetURLPattern(System.IntPtr,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebViewObject__CWebViewPlugin_SetURLPattern_m7FAF1AFDB4657A57BBA0C5750092A69342F9DF41 (intptr_t ___0_instance, String_t* ___1_allowPattern, String_t* ___2_denyPattern, String_t* ___3_hookPattern, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, char*, char*);

	// Marshaling of parameter '___1_allowPattern' to native representation
	char* ____1_allowPattern_marshaled = NULL;
	____1_allowPattern_marshaled = il2cpp_codegen_marshal_string(___1_allowPattern);

	// Marshaling of parameter '___2_denyPattern' to native representation
	char* ____2_denyPattern_marshaled = NULL;
	____2_denyPattern_marshaled = il2cpp_codegen_marshal_string(___2_denyPattern);

	// Marshaling of parameter '___3_hookPattern' to native representation
	char* ____3_hookPattern_marshaled = NULL;
	____3_hookPattern_marshaled = il2cpp_codegen_marshal_string(___3_hookPattern);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SetURLPattern)(___0_instance, ____1_allowPattern_marshaled, ____2_denyPattern_marshaled, ____3_hookPattern_marshaled);

	// Marshaling cleanup of parameter '___1_allowPattern' native representation
	il2cpp_codegen_marshal_free(____1_allowPattern_marshaled);
	____1_allowPattern_marshaled = NULL;

	// Marshaling cleanup of parameter '___2_denyPattern' native representation
	il2cpp_codegen_marshal_free(____2_denyPattern_marshaled);
	____2_denyPattern_marshaled = NULL;

	// Marshaling cleanup of parameter '___3_hookPattern' native representation
	il2cpp_codegen_marshal_free(____3_hookPattern_marshaled);
	____3_hookPattern_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
// System.Void WebViewObject::_CWebViewPlugin_LoadURL(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_LoadURL_mF3BC67D423B4A3132E20E77F68DD8F2DCC40210D (intptr_t ___0_instance, String_t* ___1_url, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*);

	// Marshaling of parameter '___1_url' to native representation
	char* ____1_url_marshaled = NULL;
	____1_url_marshaled = il2cpp_codegen_marshal_string(___1_url);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_LoadURL)(___0_instance, ____1_url_marshaled);

	// Marshaling cleanup of parameter '___1_url' native representation
	il2cpp_codegen_marshal_free(____1_url_marshaled);
	____1_url_marshaled = NULL;

}
// System.Void WebViewObject::_CWebViewPlugin_LoadHTML(System.IntPtr,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_LoadHTML_m2C486C41677C6444F57DF0D79E37FF706851CD83 (intptr_t ___0_instance, String_t* ___1_html, String_t* ___2_baseUrl, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, char*);

	// Marshaling of parameter '___1_html' to native representation
	char* ____1_html_marshaled = NULL;
	____1_html_marshaled = il2cpp_codegen_marshal_string(___1_html);

	// Marshaling of parameter '___2_baseUrl' to native representation
	char* ____2_baseUrl_marshaled = NULL;
	____2_baseUrl_marshaled = il2cpp_codegen_marshal_string(___2_baseUrl);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_LoadHTML)(___0_instance, ____1_html_marshaled, ____2_baseUrl_marshaled);

	// Marshaling cleanup of parameter '___1_html' native representation
	il2cpp_codegen_marshal_free(____1_html_marshaled);
	____1_html_marshaled = NULL;

	// Marshaling cleanup of parameter '___2_baseUrl' native representation
	il2cpp_codegen_marshal_free(____2_baseUrl_marshaled);
	____2_baseUrl_marshaled = NULL;

}
// System.Void WebViewObject::_CWebViewPlugin_EvaluateJS(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_EvaluateJS_mE1DFDCCF574554A64D67C4712574837F276736FA (intptr_t ___0_instance, String_t* ___1_url, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*);

	// Marshaling of parameter '___1_url' to native representation
	char* ____1_url_marshaled = NULL;
	____1_url_marshaled = il2cpp_codegen_marshal_string(___1_url);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_EvaluateJS)(___0_instance, ____1_url_marshaled);

	// Marshaling cleanup of parameter '___1_url' native representation
	il2cpp_codegen_marshal_free(____1_url_marshaled);
	____1_url_marshaled = NULL;

}
// System.Int32 WebViewObject::_CWebViewPlugin_Progress(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WebViewObject__CWebViewPlugin_Progress_mAFC67F67913E8D8D3784BF54C1255889DA073D4B (intptr_t ___0_instance, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_Progress)(___0_instance);

	return returnValue;
}
// System.Boolean WebViewObject::_CWebViewPlugin_CanGoBack(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebViewObject__CWebViewPlugin_CanGoBack_m26BBE9F33159F0D4134E5672B895446502BD73D4 (intptr_t ___0_instance, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_CanGoBack)(___0_instance);

	return static_cast<bool>(returnValue);
}
// System.Boolean WebViewObject::_CWebViewPlugin_CanGoForward(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebViewObject__CWebViewPlugin_CanGoForward_mEE9462A38A2A46F34CB10751A411B44465FAD710 (intptr_t ___0_instance, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_CanGoForward)(___0_instance);

	return static_cast<bool>(returnValue);
}
// System.Void WebViewObject::_CWebViewPlugin_GoBack(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_GoBack_mC15488444CB417A3F9EF294F29CFE4ED65DA8857 (intptr_t ___0_instance, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_GoBack)(___0_instance);

}
// System.Void WebViewObject::_CWebViewPlugin_GoForward(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_GoForward_mD8F46910E09B394F727F086FA653C806B16B5791 (intptr_t ___0_instance, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_GoForward)(___0_instance);

}
// System.Void WebViewObject::_CWebViewPlugin_Reload(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_Reload_m7ABD2BC31394ADFAF5AE5EE2F6FFD5ACA8B7381B (intptr_t ___0_instance, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_Reload)(___0_instance);

}
// System.Void WebViewObject::_CWebViewPlugin_AddCustomHeader(System.IntPtr,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_AddCustomHeader_mAAE78B1CCE04D4A00172E844AE95ED283B14A8F2 (intptr_t ___0_instance, String_t* ___1_headerKey, String_t* ___2_headerValue, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, char*);

	// Marshaling of parameter '___1_headerKey' to native representation
	char* ____1_headerKey_marshaled = NULL;
	____1_headerKey_marshaled = il2cpp_codegen_marshal_string(___1_headerKey);

	// Marshaling of parameter '___2_headerValue' to native representation
	char* ____2_headerValue_marshaled = NULL;
	____2_headerValue_marshaled = il2cpp_codegen_marshal_string(___2_headerValue);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_AddCustomHeader)(___0_instance, ____1_headerKey_marshaled, ____2_headerValue_marshaled);

	// Marshaling cleanup of parameter '___1_headerKey' native representation
	il2cpp_codegen_marshal_free(____1_headerKey_marshaled);
	____1_headerKey_marshaled = NULL;

	// Marshaling cleanup of parameter '___2_headerValue' native representation
	il2cpp_codegen_marshal_free(____2_headerValue_marshaled);
	____2_headerValue_marshaled = NULL;

}
// System.String WebViewObject::_CWebViewPlugin_GetCustomHeaderValue(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* WebViewObject__CWebViewPlugin_GetCustomHeaderValue_mA6E51E5AB307D643E11A297ED4D05995355A4E88 (intptr_t ___0_instance, String_t* ___1_headerKey, const RuntimeMethod* method) 
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*);

	// Marshaling of parameter '___1_headerKey' to native representation
	char* ____1_headerKey_marshaled = NULL;
	____1_headerKey_marshaled = il2cpp_codegen_marshal_string(___1_headerKey);

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_GetCustomHeaderValue)(___0_instance, ____1_headerKey_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___1_headerKey' native representation
	il2cpp_codegen_marshal_free(____1_headerKey_marshaled);
	____1_headerKey_marshaled = NULL;

	return _returnValue_unmarshaled;
}
// System.Void WebViewObject::_CWebViewPlugin_RemoveCustomHeader(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_RemoveCustomHeader_mCCE66F43A174B453751BC0EBF1986A3F1A232460 (intptr_t ___0_instance, String_t* ___1_headerKey, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*);

	// Marshaling of parameter '___1_headerKey' to native representation
	char* ____1_headerKey_marshaled = NULL;
	____1_headerKey_marshaled = il2cpp_codegen_marshal_string(___1_headerKey);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_RemoveCustomHeader)(___0_instance, ____1_headerKey_marshaled);

	// Marshaling cleanup of parameter '___1_headerKey' native representation
	il2cpp_codegen_marshal_free(____1_headerKey_marshaled);
	____1_headerKey_marshaled = NULL;

}
// System.Void WebViewObject::_CWebViewPlugin_ClearCustomHeader(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_ClearCustomHeader_m1855C7017F63969B5EED4506FED160CB771DFC2A (intptr_t ___0_instance, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_ClearCustomHeader)(___0_instance);

}
// System.Void WebViewObject::_CWebViewPlugin_ClearCookies()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_ClearCookies_m7FCC8210626A0F022BC8D6F11B2A423A7C300FD4 (const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_ClearCookies)();

}
// System.Void WebViewObject::_CWebViewPlugin_SaveCookies()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SaveCookies_m6E9EE5F72F0B403A199A52502AF346782636A6F4 (const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SaveCookies)();

}
// System.Void WebViewObject::_CWebViewPlugin_GetCookies(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_GetCookies_mEBE776075CD1290CE067D39F7C6C75A4F8676399 (intptr_t ___0_instance, String_t* ___1_url, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*);

	// Marshaling of parameter '___1_url' to native representation
	char* ____1_url_marshaled = NULL;
	____1_url_marshaled = il2cpp_codegen_marshal_string(___1_url);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_GetCookies)(___0_instance, ____1_url_marshaled);

	// Marshaling cleanup of parameter '___1_url' native representation
	il2cpp_codegen_marshal_free(____1_url_marshaled);
	____1_url_marshaled = NULL;

}
// System.Void WebViewObject::_CWebViewPlugin_SetBasicAuthInfo(System.IntPtr,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SetBasicAuthInfo_m51AE24FCA7CEBC77FA27DD901157649EFAB1BF9A (intptr_t ___0_instance, String_t* ___1_userName, String_t* ___2_password, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, char*);

	// Marshaling of parameter '___1_userName' to native representation
	char* ____1_userName_marshaled = NULL;
	____1_userName_marshaled = il2cpp_codegen_marshal_string(___1_userName);

	// Marshaling of parameter '___2_password' to native representation
	char* ____2_password_marshaled = NULL;
	____2_password_marshaled = il2cpp_codegen_marshal_string(___2_password);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SetBasicAuthInfo)(___0_instance, ____1_userName_marshaled, ____2_password_marshaled);

	// Marshaling cleanup of parameter '___1_userName' native representation
	il2cpp_codegen_marshal_free(____1_userName_marshaled);
	____1_userName_marshaled = NULL;

	// Marshaling cleanup of parameter '___2_password' native representation
	il2cpp_codegen_marshal_free(____2_password_marshaled);
	____2_password_marshaled = NULL;

}
// System.Void WebViewObject::_CWebViewPlugin_ClearCache(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_ClearCache_m3E0823A7B9908EDB08951A8858A5782D882988D7 (intptr_t ___0_instance, bool ___1_includeDiskFiles, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_ClearCache)(___0_instance, static_cast<int32_t>(___1_includeDiskFiles));

}
// System.Void WebViewObject::_CWebViewPlugin_SetSuspended(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__CWebViewPlugin_SetSuspended_mF28265A58D9F0A667409706F04F303CAEB86A7C6 (intptr_t ___0_instance, bool ___1_suspended, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SetSuspended)(___0_instance, static_cast<int32_t>(___1_suspended));

}
// System.Boolean WebViewObject::IsWebViewAvailable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebViewObject_IsWebViewAvailable_mFF06F81E630FDAF0475D5ADA9747C46EC03DAE16 (const RuntimeMethod* method) 
{
	{
		// return true;
		return (bool)1;
	}
}
// System.Void WebViewObject::Init(System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Boolean,System.Boolean,System.String,System.Int32,System.Int32,System.Boolean,System.Int32,System.Boolean,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_Init_mEAF31AD85AEE437FFAED0E788A7A654EC3469FA8 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___0_cb, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___1_err, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___2_httpErr, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___3_ld, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___4_started, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___5_hooked, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___6_cookies, bool ___7_transparent, bool ___8_zoom, String_t* ___9_ua, int32_t ___10_radius, int32_t ___11_androidForceDarkMode, bool ___12_enableWKWebView, int32_t ___13_wkContentMode, bool ___14_wkAllowsLinkPreview, bool ___15_wkAllowsBackForwardNavigationGestures, bool ___16_separated, const RuntimeMethod* method) 
{
	{
		// onJS = cb;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = ___0_cb;
		__this->___onJS_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___onJS_4), (void*)L_0);
		// onError = err;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = ___1_err;
		__this->___onError_5 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___onError_5), (void*)L_1);
		// onHttpError = httpErr;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = ___2_httpErr;
		__this->___onHttpError_6 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___onHttpError_6), (void*)L_2);
		// onStarted = started;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = ___4_started;
		__this->___onStarted_7 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___onStarted_7), (void*)L_3);
		// onLoaded = ld;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_4 = ___3_ld;
		__this->___onLoaded_8 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___onLoaded_8), (void*)L_4);
		// onHooked = hooked;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_5 = ___5_hooked;
		__this->___onHooked_9 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___onHooked_9), (void*)L_5);
		// onCookies = cookies;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_6 = ___6_cookies;
		__this->___onCookies_10 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___onCookies_10), (void*)L_6);
		// webView = _CWebViewPlugin_Init(name, transparent, zoom, ua, enableWKWebView, wkContentMode, wkAllowsLinkPreview, wkAllowsBackForwardNavigationGestures, radius);
		String_t* L_7;
		L_7 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(__this, NULL);
		bool L_8 = ___7_transparent;
		bool L_9 = ___8_zoom;
		String_t* L_10 = ___9_ua;
		bool L_11 = ___12_enableWKWebView;
		int32_t L_12 = ___13_wkContentMode;
		bool L_13 = ___14_wkAllowsLinkPreview;
		bool L_14 = ___15_wkAllowsBackForwardNavigationGestures;
		int32_t L_15 = ___10_radius;
		intptr_t L_16;
		L_16 = WebViewObject__CWebViewPlugin_Init_m486E39DB9D85CB93AE99844C240EA3B53A676AB3(L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, NULL);
		__this->___webView_24 = L_16;
		// }
		return;
	}
}
// System.Void WebViewObject::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_OnDestroy_m7EFD8FA090DB20FDC619991440AE4A11E88A28B6 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_Destroy(webView);
		intptr_t L_3 = __this->___webView_24;
		int32_t L_4;
		L_4 = WebViewObject__CWebViewPlugin_Destroy_m7B8B2C6F90746C56A96C9BB53568927617176498(L_3, NULL);
		// webView = IntPtr.Zero;
		intptr_t L_5 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		__this->___webView_24 = L_5;
		// }
		return;
	}
}
// System.Void WebViewObject::Pause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_Pause_m3F69CAA00AAEBDCAAF3EE4AAA5905B0457434DFE (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	{
		// if (webView == null)
		intptr_t L_0 = __this->___webView_24;
		// _CWebViewPlugin_SetSuspended(webView, true);
		intptr_t L_1 = __this->___webView_24;
		WebViewObject__CWebViewPlugin_SetSuspended_mF28265A58D9F0A667409706F04F303CAEB86A7C6(L_1, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::Resume()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_Resume_m31417432D1821B761DC42AAA4D904D3414978A62 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	{
		// _CWebViewPlugin_SetSuspended(webView, false);
		intptr_t L_0 = __this->___webView_24;
		WebViewObject__CWebViewPlugin_SetSuspended_mF28265A58D9F0A667409706F04F303CAEB86A7C6(L_0, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::SetCenterPositionWithScale(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_SetCenterPositionWithScale_mEAC59D9AB7EE9B5ACC83F8FFE1AEAFE908B6BE4C (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_center, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_scale, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		// float left = (Screen.width - scale.x) / 2.0f + center.x;
		int32_t L_0;
		L_0 = Screen_get_width_mF608FF3252213E7EFA1F0D2F744C28110E9E5AC9(NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = ___1_scale;
		float L_2 = L_1.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = ___0_center;
		float L_4 = L_3.___x_0;
		V_0 = ((float)il2cpp_codegen_add(((float)(((float)il2cpp_codegen_subtract(((float)L_0), L_2))/(2.0f))), L_4));
		// float right = Screen.width - (left + scale.x);
		int32_t L_5;
		L_5 = Screen_get_width_mF608FF3252213E7EFA1F0D2F744C28110E9E5AC9(NULL);
		float L_6 = V_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_7 = ___1_scale;
		float L_8 = L_7.___x_0;
		V_1 = ((float)il2cpp_codegen_subtract(((float)L_5), ((float)il2cpp_codegen_add(L_6, L_8))));
		// float bottom = (Screen.height - scale.y) / 2.0f + center.y;
		int32_t L_9;
		L_9 = Screen_get_height_m01A3102DE71EE1FBEA51D09D6B0261CF864FE8F9(NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_10 = ___1_scale;
		float L_11 = L_10.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_12 = ___0_center;
		float L_13 = L_12.___y_1;
		V_2 = ((float)il2cpp_codegen_add(((float)(((float)il2cpp_codegen_subtract(((float)L_9), L_11))/(2.0f))), L_13));
		// float top = Screen.height - (bottom + scale.y);
		int32_t L_14;
		L_14 = Screen_get_height_m01A3102DE71EE1FBEA51D09D6B0261CF864FE8F9(NULL);
		float L_15 = V_2;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_16 = ___1_scale;
		float L_17 = L_16.___y_1;
		V_3 = ((float)il2cpp_codegen_subtract(((float)L_14), ((float)il2cpp_codegen_add(L_15, L_17))));
		// SetMargins((int)left, (int)top, (int)right, (int)bottom);
		float L_18 = V_0;
		float L_19 = V_3;
		float L_20 = V_1;
		float L_21 = V_2;
		WebViewObject_SetMargins_m1D6F1FD6D8317D2FCD550768B9D794B544564366(__this, il2cpp_codegen_cast_double_to_int<int32_t>(L_18), il2cpp_codegen_cast_double_to_int<int32_t>(L_19), il2cpp_codegen_cast_double_to_int<int32_t>(L_20), il2cpp_codegen_cast_double_to_int<int32_t>(L_21), (bool)0, NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_SetMargins_m1D6F1FD6D8317D2FCD550768B9D794B544564366 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, int32_t ___0_left, int32_t ___1_top, int32_t ___2_right, int32_t ___3_bottom, bool ___4_relative, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// mMarginLeft = left;
		int32_t L_3 = ___0_left;
		__this->___mMarginLeft_14 = L_3;
		// mMarginTop = top;
		int32_t L_4 = ___1_top;
		__this->___mMarginTop_15 = L_4;
		// mMarginRight = right;
		int32_t L_5 = ___2_right;
		__this->___mMarginRight_16 = L_5;
		// mMarginBottom = bottom;
		int32_t L_6 = ___3_bottom;
		__this->___mMarginBottom_17 = L_6;
		// mMarginRelative = relative;
		bool L_7 = ___4_relative;
		__this->___mMarginRelative_18 = L_7;
		// if (relative)
		bool L_8 = ___4_relative;
		if (!L_8)
		{
			goto IL_0067;
		}
	}
	{
		// float w = (float)Screen.width;
		int32_t L_9;
		L_9 = Screen_get_width_mF608FF3252213E7EFA1F0D2F744C28110E9E5AC9(NULL);
		V_5 = ((float)L_9);
		// float h = (float)Screen.height;
		int32_t L_10;
		L_10 = Screen_get_height_m01A3102DE71EE1FBEA51D09D6B0261CF864FE8F9(NULL);
		V_6 = ((float)L_10);
		// ml = left / w;
		int32_t L_11 = ___0_left;
		float L_12 = V_5;
		V_0 = ((float)(((float)L_11)/L_12));
		// mt = top / h;
		int32_t L_13 = ___1_top;
		float L_14 = V_6;
		V_1 = ((float)(((float)L_13)/L_14));
		// mr = right / w;
		int32_t L_15 = ___2_right;
		float L_16 = V_5;
		V_2 = ((float)(((float)L_15)/L_16));
		// mb = bottom / h;
		int32_t L_17 = ___3_bottom;
		float L_18 = V_6;
		V_3 = ((float)(((float)L_17)/L_18));
		goto IL_0074;
	}

IL_0067:
	{
		// ml = left;
		int32_t L_19 = ___0_left;
		V_0 = ((float)L_19);
		// mt = top;
		int32_t L_20 = ___1_top;
		V_1 = ((float)L_20);
		// mr = right;
		int32_t L_21 = ___2_right;
		V_2 = ((float)L_21);
		// mb = bottom;
		int32_t L_22 = ___3_bottom;
		V_3 = ((float)L_22);
	}

IL_0074:
	{
		// bool r = relative;
		bool L_23 = ___4_relative;
		V_4 = L_23;
		// if (ml == mMarginLeftComputed
		//     && mt == mMarginTopComputed
		//     && mr == mMarginRightComputed
		//     && mb == mMarginBottomComputed
		//     && r == mMarginRelativeComputed)
		float L_24 = V_0;
		float L_25 = __this->___mMarginLeftComputed_19;
		if ((!(((float)L_24) == ((float)L_25))))
		{
			goto IL_00a7;
		}
	}
	{
		float L_26 = V_1;
		float L_27 = __this->___mMarginTopComputed_20;
		if ((!(((float)L_26) == ((float)L_27))))
		{
			goto IL_00a7;
		}
	}
	{
		float L_28 = V_2;
		float L_29 = __this->___mMarginRightComputed_21;
		if ((!(((float)L_28) == ((float)L_29))))
		{
			goto IL_00a7;
		}
	}
	{
		float L_30 = V_3;
		float L_31 = __this->___mMarginBottomComputed_22;
		if ((!(((float)L_30) == ((float)L_31))))
		{
			goto IL_00a7;
		}
	}
	{
		bool L_32 = V_4;
		bool L_33 = __this->___mMarginRelativeComputed_23;
		if ((!(((uint32_t)L_32) == ((uint32_t)L_33))))
		{
			goto IL_00a7;
		}
	}
	{
		// return;
		return;
	}

IL_00a7:
	{
		// mMarginLeftComputed = ml;
		float L_34 = V_0;
		__this->___mMarginLeftComputed_19 = L_34;
		// mMarginTopComputed = mt;
		float L_35 = V_1;
		__this->___mMarginTopComputed_20 = L_35;
		// mMarginRightComputed = mr;
		float L_36 = V_2;
		__this->___mMarginRightComputed_21 = L_36;
		// mMarginBottomComputed = mb;
		float L_37 = V_3;
		__this->___mMarginBottomComputed_22 = L_37;
		// mMarginRelativeComputed = r;
		bool L_38 = V_4;
		__this->___mMarginRelativeComputed_23 = L_38;
		// _CWebViewPlugin_SetMargins(webView, ml, mt, mr, mb, r);
		intptr_t L_39 = __this->___webView_24;
		float L_40 = V_0;
		float L_41 = V_1;
		float L_42 = V_2;
		float L_43 = V_3;
		bool L_44 = V_4;
		WebViewObject__CWebViewPlugin_SetMargins_m072CE271F87DDFFF0E134DB1D3CA837D82B9A7BF(L_39, L_40, L_41, L_42, L_43, L_44, NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::SetVisibility(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_SetVisibility_m39DDC96A831F01C141B133FA6DECBBE7C6F3EC73 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, bool ___0_v, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_SetVisibility(webView, v);
		intptr_t L_3 = __this->___webView_24;
		bool L_4 = ___0_v;
		WebViewObject__CWebViewPlugin_SetVisibility_m400B0F30A5FD5EA774B3BCCE3AF5A05F71A784A6(L_3, L_4, NULL);
		// visibility = v;
		bool L_5 = ___0_v;
		__this->___visibility_11 = L_5;
		// }
		return;
	}
}
// System.Boolean WebViewObject::GetVisibility()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebViewObject_GetVisibility_m0890B19FC4DE937652199F48F367F9289B05C5BC (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	{
		// return visibility;
		bool L_0 = __this->___visibility_11;
		return L_0;
	}
}
// System.Void WebViewObject::SetScrollbarsVisibility(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_SetScrollbarsVisibility_mA536E94DB18C1C1008B08D4D8871A5F70B612CC9 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, bool ___0_v, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_SetScrollbarsVisibility(webView, v);
		intptr_t L_3 = __this->___webView_24;
		bool L_4 = ___0_v;
		WebViewObject__CWebViewPlugin_SetScrollbarsVisibility_m2476178AC1BE9368F8AFF26112B4FEEE003B89C7(L_3, L_4, NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::SetInteractionEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_SetInteractionEnabled_m0A170EC9DEB85617D88A81A304578A920A40A020 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, bool ___0_enabled, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_SetInteractionEnabled(webView, enabled);
		intptr_t L_3 = __this->___webView_24;
		bool L_4 = ___0_enabled;
		WebViewObject__CWebViewPlugin_SetInteractionEnabled_mB4505783D1F543FAFAD12086D794E9456F1BDC80(L_3, L_4, NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::SetAlertDialogEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_SetAlertDialogEnabled_mBE0FF528AAF5AA7388FFC15B186F7D5C9E0A6997 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, bool ___0_e, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_SetAlertDialogEnabled(webView, e);
		intptr_t L_3 = __this->___webView_24;
		bool L_4 = ___0_e;
		WebViewObject__CWebViewPlugin_SetAlertDialogEnabled_mEFE2F2DD64EE0893B37054CC08DFB106A38BD971(L_3, L_4, NULL);
		// alertDialogEnabled = e;
		bool L_5 = ___0_e;
		__this->___alertDialogEnabled_12 = L_5;
		// }
		return;
	}
}
// System.Boolean WebViewObject::GetAlertDialogEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebViewObject_GetAlertDialogEnabled_m0BD28C73C7A622198221B7098124F1A9A8E28639 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	{
		// return alertDialogEnabled;
		bool L_0 = __this->___alertDialogEnabled_12;
		return L_0;
	}
}
// System.Void WebViewObject::SetScrollBounceEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_SetScrollBounceEnabled_mA82705EA3501742EDBC32C070EF672DFCA911A13 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, bool ___0_e, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_SetScrollBounceEnabled(webView, e);
		intptr_t L_3 = __this->___webView_24;
		bool L_4 = ___0_e;
		WebViewObject__CWebViewPlugin_SetScrollBounceEnabled_mEF514F8022A350897D8DBBBE746D40D29654CFF3(L_3, L_4, NULL);
		// scrollBounceEnabled = e;
		bool L_5 = ___0_e;
		__this->___scrollBounceEnabled_13 = L_5;
		// }
		return;
	}
}
// System.Boolean WebViewObject::GetScrollBounceEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebViewObject_GetScrollBounceEnabled_m9863E3FF0FB0CE451E300A90B54A602144D404BE (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	{
		// return scrollBounceEnabled;
		bool L_0 = __this->___scrollBounceEnabled_13;
		return L_0;
	}
}
// System.Void WebViewObject::SetCameraAccess(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_SetCameraAccess_mC350596E9C91F387A372225C2B9DCF15AE920741 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, bool ___0_allowed, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void WebViewObject::SetMicrophoneAccess(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_SetMicrophoneAccess_m1C76FBF405A2F295888F5EFF1DAF699B2A6B5033 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, bool ___0_allowed, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Boolean WebViewObject::SetURLPattern(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebViewObject_SetURLPattern_m10C1538DFCA8321950D49EA2AFDE5C91F1656489 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, String_t* ___0_allowPattern, String_t* ___1_denyPattern, String_t* ___2_hookPattern, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0014;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0014:
	{
		// return _CWebViewPlugin_SetURLPattern(webView, allowPattern, denyPattern, hookPattern);
		intptr_t L_3 = __this->___webView_24;
		String_t* L_4 = ___0_allowPattern;
		String_t* L_5 = ___1_denyPattern;
		String_t* L_6 = ___2_hookPattern;
		bool L_7;
		L_7 = WebViewObject__CWebViewPlugin_SetURLPattern_m7FAF1AFDB4657A57BBA0C5750092A69342F9DF41(L_3, L_4, L_5, L_6, NULL);
		return L_7;
	}
}
// System.Void WebViewObject::LoadURL(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_LoadURL_m53B0B97166FD99F560551E5ED196885812A44FF5 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, String_t* ___0_url, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (string.IsNullOrEmpty(url))
		String_t* L_0 = ___0_url;
		bool L_1;
		L_1 = String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478(L_0, NULL);
		if (!L_1)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_2 = __this->___webView_24;
		intptr_t L_3 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_4;
		L_4 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_2, L_3, NULL);
		if (!L_4)
		{
			goto IL_001c;
		}
	}
	{
		// return;
		return;
	}

IL_001c:
	{
		// _CWebViewPlugin_LoadURL(webView, url);
		intptr_t L_5 = __this->___webView_24;
		String_t* L_6 = ___0_url;
		WebViewObject__CWebViewPlugin_LoadURL_mF3BC67D423B4A3132E20E77F68DD8F2DCC40210D(L_5, L_6, NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::LoadHTML(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_LoadHTML_m908C0F70B3099CE8E5299D707A64C8A72238D9A7 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, String_t* ___0_html, String_t* ___1_baseUrl, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (string.IsNullOrEmpty(html))
		String_t* L_0 = ___0_html;
		bool L_1;
		L_1 = String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478(L_0, NULL);
		if (!L_1)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// if (string.IsNullOrEmpty(baseUrl))
		String_t* L_2 = ___1_baseUrl;
		bool L_3;
		L_3 = String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478(L_2, NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		// baseUrl = "";
		___1_baseUrl = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
	}

IL_0018:
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_4 = __this->___webView_24;
		intptr_t L_5 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_6;
		L_6 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_4, L_5, NULL);
		if (!L_6)
		{
			goto IL_002b;
		}
	}
	{
		// return;
		return;
	}

IL_002b:
	{
		// _CWebViewPlugin_LoadHTML(webView, html, baseUrl);
		intptr_t L_7 = __this->___webView_24;
		String_t* L_8 = ___0_html;
		String_t* L_9 = ___1_baseUrl;
		WebViewObject__CWebViewPlugin_LoadHTML_m2C486C41677C6444F57DF0D79E37FF706851CD83(L_7, L_8, L_9, NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::EvaluateJS(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_EvaluateJS_m2117470986EFF11E43A47DEF6E6E45FD2C28C27D (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, String_t* ___0_js, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_EvaluateJS(webView, js);
		intptr_t L_3 = __this->___webView_24;
		String_t* L_4 = ___0_js;
		WebViewObject__CWebViewPlugin_EvaluateJS_mE1DFDCCF574554A64D67C4712574837F276736FA(L_3, L_4, NULL);
		// }
		return;
	}
}
// System.Int32 WebViewObject::Progress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WebViewObject_Progress_m7B3A5EFDAC58AA59F2B77885DBCF54E1C2650018 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0014;
		}
	}
	{
		// return 0;
		return 0;
	}

IL_0014:
	{
		// return _CWebViewPlugin_Progress(webView);
		intptr_t L_3 = __this->___webView_24;
		int32_t L_4;
		L_4 = WebViewObject__CWebViewPlugin_Progress_mAFC67F67913E8D8D3784BF54C1255889DA073D4B(L_3, NULL);
		return L_4;
	}
}
// System.Boolean WebViewObject::CanGoBack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebViewObject_CanGoBack_m8E73FBF10169F93B89060519845A9835D93F1471 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0014;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0014:
	{
		// return _CWebViewPlugin_CanGoBack(webView);
		intptr_t L_3 = __this->___webView_24;
		bool L_4;
		L_4 = WebViewObject__CWebViewPlugin_CanGoBack_m26BBE9F33159F0D4134E5672B895446502BD73D4(L_3, NULL);
		return L_4;
	}
}
// System.Boolean WebViewObject::CanGoForward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebViewObject_CanGoForward_mDC6F06D4F38626DC95BF3E56A845346EE4E121E0 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0014;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0014:
	{
		// return _CWebViewPlugin_CanGoForward(webView);
		intptr_t L_3 = __this->___webView_24;
		bool L_4;
		L_4 = WebViewObject__CWebViewPlugin_CanGoForward_mEE9462A38A2A46F34CB10751A411B44465FAD710(L_3, NULL);
		return L_4;
	}
}
// System.Void WebViewObject::GoBack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_GoBack_m3A6A9D15AAF225D5913AD8057C6499622640C5A5 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_GoBack(webView);
		intptr_t L_3 = __this->___webView_24;
		WebViewObject__CWebViewPlugin_GoBack_mC15488444CB417A3F9EF294F29CFE4ED65DA8857(L_3, NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::GoForward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_GoForward_m417FB43369F7AD99F9526B6D93834354136663B0 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_GoForward(webView);
		intptr_t L_3 = __this->___webView_24;
		WebViewObject__CWebViewPlugin_GoForward_mD8F46910E09B394F727F086FA653C806B16B5791(L_3, NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::Reload()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_Reload_mBA7B99FBF68CC720D0F98CF5662BDE276DEBB158 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_Reload(webView);
		intptr_t L_3 = __this->___webView_24;
		WebViewObject__CWebViewPlugin_Reload_m7ABD2BC31394ADFAF5AE5EE2F6FFD5ACA8B7381B(L_3, NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::CallOnError(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_CallOnError_m787719CEAD0F007F35AF8D4EFA0BCF6A6E63B42A (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, String_t* ___0_error, const RuntimeMethod* method) 
{
	{
		// if (onError != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___onError_5;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		// onError(error);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = __this->___onError_5;
		String_t* L_2 = ___0_error;
		NullCheck(L_1);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_1, L_2, NULL);
	}

IL_0014:
	{
		// }
		return;
	}
}
// System.Void WebViewObject::CallOnHttpError(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_CallOnHttpError_mE10BC848A4035739FDDB3C0BAB14C734B2D8ABBF (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, String_t* ___0_error, const RuntimeMethod* method) 
{
	{
		// if (onHttpError != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___onHttpError_6;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		// onHttpError(error);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = __this->___onHttpError_6;
		String_t* L_2 = ___0_error;
		NullCheck(L_1);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_1, L_2, NULL);
	}

IL_0014:
	{
		// }
		return;
	}
}
// System.Void WebViewObject::CallOnStarted(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_CallOnStarted_mBC28195C9674F3C2CC12456DC8F082F86EAEF854 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, String_t* ___0_url, const RuntimeMethod* method) 
{
	{
		// if (onStarted != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___onStarted_7;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		// onStarted(url);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = __this->___onStarted_7;
		String_t* L_2 = ___0_url;
		NullCheck(L_1);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_1, L_2, NULL);
	}

IL_0014:
	{
		// }
		return;
	}
}
// System.Void WebViewObject::CallOnLoaded(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_CallOnLoaded_m561F518F395A2FDB0BCF19E6D6FA94CC9CF765C2 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, String_t* ___0_url, const RuntimeMethod* method) 
{
	{
		// if (onLoaded != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___onLoaded_8;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		// onLoaded(url);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = __this->___onLoaded_8;
		String_t* L_2 = ___0_url;
		NullCheck(L_1);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_1, L_2, NULL);
	}

IL_0014:
	{
		// }
		return;
	}
}
// System.Void WebViewObject::CallFromJS(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_CallFromJS_mFBB59CD78CA5295A5585D5452F0EEA20E01539BC (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, String_t* ___0_message, const RuntimeMethod* method) 
{
	{
		// if (onJS != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___onJS_4;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		// message = UnityWebRequest.UnEscapeURL(message);
		String_t* L_1 = ___0_message;
		String_t* L_2;
		L_2 = UnityWebRequest_UnEscapeURL_mF32D6DA1A428A932B9A57A48FE5FA86D4B1446BB(L_1, NULL);
		___0_message = L_2;
		// onJS(message);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = __this->___onJS_4;
		String_t* L_4 = ___0_message;
		NullCheck(L_3);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_3, L_4, NULL);
	}

IL_001c:
	{
		// }
		return;
	}
}
// System.Void WebViewObject::CallOnHooked(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_CallOnHooked_m920D16D6528B44DD6DEA45368CED310D51FC2C2F (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, String_t* ___0_message, const RuntimeMethod* method) 
{
	{
		// if (onHooked != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___onHooked_9;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		// message = UnityWebRequest.UnEscapeURL(message);
		String_t* L_1 = ___0_message;
		String_t* L_2;
		L_2 = UnityWebRequest_UnEscapeURL_mF32D6DA1A428A932B9A57A48FE5FA86D4B1446BB(L_1, NULL);
		___0_message = L_2;
		// onHooked(message);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = __this->___onHooked_9;
		String_t* L_4 = ___0_message;
		NullCheck(L_3);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_3, L_4, NULL);
	}

IL_001c:
	{
		// }
		return;
	}
}
// System.Void WebViewObject::CallOnCookies(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_CallOnCookies_mCA0D4DC3E5556089A49E048C5E447E5B8BDFD81E (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, String_t* ___0_cookies, const RuntimeMethod* method) 
{
	{
		// if (onCookies != null)
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___onCookies_10;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		// onCookies(cookies);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = __this->___onCookies_10;
		String_t* L_2 = ___0_cookies;
		NullCheck(L_1);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_1, L_2, NULL);
	}

IL_0014:
	{
		// }
		return;
	}
}
// System.Void WebViewObject::AddCustomHeader(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_AddCustomHeader_mD6588AE2ACF6AAD483217EBFB5DAC2A84939A4D0 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, String_t* ___0_headerKey, String_t* ___1_headerValue, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_AddCustomHeader(webView, headerKey, headerValue);
		intptr_t L_3 = __this->___webView_24;
		String_t* L_4 = ___0_headerKey;
		String_t* L_5 = ___1_headerValue;
		WebViewObject__CWebViewPlugin_AddCustomHeader_mAAE78B1CCE04D4A00172E844AE95ED283B14A8F2(L_3, L_4, L_5, NULL);
		// }
		return;
	}
}
// System.String WebViewObject::GetCustomHeaderValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* WebViewObject_GetCustomHeaderValue_m6C0C211A2FFC0EAB3FE724372F572F5BB56FBD80 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, String_t* ___0_headerKey, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0014;
		}
	}
	{
		// return null;
		return (String_t*)NULL;
	}

IL_0014:
	{
		// return _CWebViewPlugin_GetCustomHeaderValue(webView, headerKey);
		intptr_t L_3 = __this->___webView_24;
		String_t* L_4 = ___0_headerKey;
		String_t* L_5;
		L_5 = WebViewObject__CWebViewPlugin_GetCustomHeaderValue_mA6E51E5AB307D643E11A297ED4D05995355A4E88(L_3, L_4, NULL);
		return L_5;
	}
}
// System.Void WebViewObject::RemoveCustomHeader(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_RemoveCustomHeader_mE5D8106E902C59069674F874CBD870043D661B7B (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, String_t* ___0_headerKey, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_RemoveCustomHeader(webView, headerKey);
		intptr_t L_3 = __this->___webView_24;
		String_t* L_4 = ___0_headerKey;
		WebViewObject__CWebViewPlugin_RemoveCustomHeader_mCCE66F43A174B453751BC0EBF1986A3F1A232460(L_3, L_4, NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::ClearCustomHeader()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_ClearCustomHeader_mD546FA1AB3B09FC9E062EC89ED6B9417C1176526 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_ClearCustomHeader(webView);
		intptr_t L_3 = __this->___webView_24;
		WebViewObject__CWebViewPlugin_ClearCustomHeader_m1855C7017F63969B5EED4506FED160CB771DFC2A(L_3, NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::ClearCookies()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_ClearCookies_m8304C6FA463D3DDE1183566D1E9805B87615B075 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_ClearCookies();
		WebViewObject__CWebViewPlugin_ClearCookies_m7FCC8210626A0F022BC8D6F11B2A423A7C300FD4(NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::SaveCookies()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_SaveCookies_m6EF4334EC21D455C0C6A70B4B353F9942A92F5E1 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_SaveCookies();
		WebViewObject__CWebViewPlugin_SaveCookies_m6E9EE5F72F0B403A199A52502AF346782636A6F4(NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::GetCookies(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_GetCookies_mFFC33687D38A31E6D9F55F41D5805595AC1EF2D2 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, String_t* ___0_url, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_GetCookies(webView, url);
		intptr_t L_3 = __this->___webView_24;
		String_t* L_4 = ___0_url;
		WebViewObject__CWebViewPlugin_GetCookies_mEBE776075CD1290CE067D39F7C6C75A4F8676399(L_3, L_4, NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::SetBasicAuthInfo(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_SetBasicAuthInfo_mBF5E57E10F5D6DF10BF91E880B00FDE162951E11 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, String_t* ___0_userName, String_t* ___1_password, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_SetBasicAuthInfo(webView, userName, password);
		intptr_t L_3 = __this->___webView_24;
		String_t* L_4 = ___0_userName;
		String_t* L_5 = ___1_password;
		WebViewObject__CWebViewPlugin_SetBasicAuthInfo_m51AE24FCA7CEBC77FA27DD901157649EFAB1BF9A(L_3, L_4, L_5, NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::ClearCache(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_ClearCache_m1365D809C7A51F3A5EFAC16581E17E38FAA54D33 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, bool ___0_includeDiskFiles, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (webView == IntPtr.Zero)
		intptr_t L_0 = __this->___webView_24;
		intptr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->___Zero_1;
		bool L_2;
		L_2 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// _CWebViewPlugin_ClearCache(webView, includeDiskFiles);
		intptr_t L_3 = __this->___webView_24;
		bool L_4 = ___0_includeDiskFiles;
		WebViewObject__CWebViewPlugin_ClearCache_m3E0823A7B9908EDB08951A8858A5782D882988D7(L_3, L_4, NULL);
		// }
		return;
	}
}
// System.Void WebViewObject::SetTextZoom(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject_SetTextZoom_mE2F4060CAECDBFB2B90F73FA9FE11D38F1311BD1 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, int32_t ___0_textZoom, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void WebViewObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebViewObject__ctor_mCF74D7A0FF5875B0B98D3200379AD046B0B95F81 (WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___0_obj, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___0_obj, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
