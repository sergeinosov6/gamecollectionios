﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif





// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_tE8693FF0E67CDBA52BAFB211BFF1844D076ABAFB;
// System.Action`1<System.Boolean>
struct Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C;
// System.Action`1<UnityEngine.Camera>
struct Action_1_t268986DA4CF361AC17B40338506A83AFB35832EA;
// System.Action`1<UnityEngine.Font>
struct Action_1_tD91E4D0ED3C2E385D3BDD4B3EA48B5F99D39F1DC;
// System.Action`1<UnityEngine.XR.InputDevice>
struct Action_1_tFAB0B519749BBE2B7AAD946105FAE8116636A8BC;
// System.Action`1<System.Int32>
struct Action_1_tD69A6DC9FBE94131E52F5A73B2A9D4AB51EEC404;
// System.Action`1<System.IntPtr>
struct Action_1_t2DF1ED40E3084E997390FF52F462390882271FE2;
// System.Action`1<UnityEngineInternal.Input.NativeInputUpdateType>
struct Action_1_t7797D4D8783204B10C3D28B96B049C48276C3B1B;
// System.Action`1<UnityEngine.Playables.PlayableDirector>
struct Action_1_tB645F646DB079054A9500B09427CB02A88372D3F;
// System.Action`1<System.String>
struct Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A;
// System.Action`1<UnityEngine.XR.XRInputSubsystem>
struct Action_1_tC867D66471C553CFFF8707FF2C59FB7AAB03086A;
// System.Action`1<UnityEngine.XR.XRNodeState>
struct Action_1_t290119641EBA3C1EAC8AF78274C63CE01C3046D8;
// System.Action`2<System.Boolean,System.String>
struct Action_2_t8EADE87044ADE97906736D729EA2E3EF97F34F3D;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_tD7438462601D3939500ED67463331FE00CFFBDB8;
// System.Action`2<System.Int32,System.String>
struct Action_2_t6AAF2E215E74E16A4EEF0A0749A4A325D99F5BA6;
// System.Action`2<UnityEngine.PhysicsScene,Unity.Collections.NativeArray`1<UnityEngine.ModifiableContactPair>>
struct Action_2_t70E17A6F8F03189031C560482454FE2D87F496F2;
// System.Action`3<System.Boolean,System.Boolean,System.Int32>
struct Action_3_t4730167C8E7EB19F1E0034580790A915D549F6CB;
// System.Action`3<UnityEngine.Timeline.TimelineClip,UnityEngine.GameObject,UnityEngine.Playables.Playable>
struct Action_3_t3638A0A401CA68AF6FECFB956B602BBF7B9EFA72;
// System.Action`3<UnityEngine.Timeline.TrackAsset,UnityEngine.GameObject,UnityEngine.Playables.Playable>
struct Action_3_t8A9161BC98843636E3BF066B37CBCC15C593B73E;
// System.Action`4<DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform>
struct Action_4_t9AF66ACF00E5AEB0F9B4A06FAEFEA18B2F266BDB;
// System.Dynamic.Utils.CacheDict`2<System.Type,System.Reflection.MethodInfo>
struct CacheDict_2_tB695739D50653F4D4C3DA03BCF07CC868196FB15;
// System.Comparison`1<UnityEngine.Timeline.TimelineClip>
struct Comparison_1_t72A7B2EACD3F8F0C3D6A14C88E2B3C719E89C50E;
// System.Comparison`1<UnityEngine.Timeline.TimeNotificationBehaviour/NotificationEntry>
struct Comparison_1_tE4F2B745C928DCA6C67CEA5FC784D025346A6CCA;
// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Linq.Expressions.Expression,System.Linq.Expressions.Expression/ExtensionInfo>
struct ConditionalWeakTable_2_t0F3FDA57EE333DF8B8C1F3FB944E4E19C5DDCFC7;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Vector3>
struct Dictionary_2_t352A6559C587A201E648C2AD14035DFDD2403AE0;
// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t6AC338B3CEB934A66B363042F19213FE666F6818;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.TextCore.Text.FontAsset>
struct Dictionary_2_tC20B3D6AE4370C892734F670EF4D1FB9CE91F371;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_tABE19B9C5C52F1DE14F0D3287B2696E7D7419180;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material>
struct Dictionary_2_tBF325E0F09BEEDF7AC6E6CB85841301637FC6E90;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.TextCore.Text.SpriteAsset>
struct Dictionary_2_t30736317A47AFC36A7F4FEE0E6487EDC529FB11E;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.TextCore.Text.TextColorGradient>
struct Dictionary_2_t3ECC0A0D579183A347F8AE00758620AA008C715A;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.TextCore.Text.TextStyle>
struct Dictionary_2_tD24879A9B92D2B2D486110818CD29C513B42AF62;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
struct Dictionary_2_tD74A089D3CFE69E54B1617003276B07F5B82B598;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.TextCore.Text.TextResourceManager/FontAssetRef>
struct Dictionary_2_t4B29EB34D6BDD7CF67CE77D60AC718EB61186713;
// System.Collections.Generic.Dictionary`2<System.Int64,UnityEngine.TextCore.Text.FontAsset>
struct Dictionary_2_t6A8DB1A82203EE1CADD418706BFDC84FED020B64;
// System.Collections.Generic.Dictionary`2<System.Int64,UnityEngine.Material>
struct Dictionary_2_tDBB219D9459E13F073641D0B84B8AB9AF3839287;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_tEFC8016EC28460E6CE058A5F413FAB656883AA5F;
// System.Collections.Generic.Dictionary`2<UnityEngine.Timeline.TrackAsset,UnityEngine.Playables.Playable>
struct Dictionary_2_t7F8A452CAC89438BACFC42CC989FA36151E8577E;
// System.Collections.Generic.Dictionary`2<DG.Tweening.Tween,DG.Tweening.Core.TweenLink>
struct Dictionary_2_tFD00E28E4EE499B3B2F468CCB593BD9ACCE80357;
// System.Collections.Generic.Dictionary`2<System.Type,DG.Tweening.Plugins.Core.ITweenPlugin>
struct Dictionary_2_tF4CB0FB5DF80F436652EC8BDB03A82C708969A88;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute>
struct Dictionary_2_tF0368534E8881FC0469B58E4901741C5B0CC1D79;
// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Text.Character>
struct Dictionary_2_t93CDF0F4011A5A3024EB73A492F9512E3046EACB;
// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Glyph>
struct Dictionary_2_tC61348D10610A6B3D7B65102D82AC3467D59EAA7;
// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord>
struct Dictionary_2_tDD72F78A572F94ECEDBDA75C3D17C3ED05C167E0;
// System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32>
struct Dictionary_2_t1A4804CA9724B6CE01D6ECABE81CE0848CBA80B4;
// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Text.SpriteCharacter>
struct Dictionary_2_tD4154357CA320908C5A7A35ED81FA2A9856C28D9;
// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Text.SpriteGlyph>
struct Dictionary_2_tDC0461D8CBB2E6B52DD2C421114EDE7C1C70DE73;
// System.Func`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Vector2>>
struct Func_1_tF5F7F5DCF1679E08B2536581A6E1EEF5529155C9;
// System.Func`1<System.Boolean>
struct Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457;
// System.Func`2<UnityEngine.TextCore.Text.Character,System.UInt32>
struct Func_2_t1F4F442815B8C6944A17E87D7B00D2830ECD1600;
// System.Func`2<System.Exception,System.Boolean>
struct Func_2_tDDBE08B46BEFDD869DE0B97D023CB9C89674FED6;
// System.Func`2<UnityEngine.TextCore.Glyph,System.UInt32>
struct Func_2_tBA43006BE5B44011173C435E32D4BC18730623FB;
// System.Func`2<UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord,System.UInt32>
struct Func_2_tEDCDCD7BE3F7A4F5A742A5FD711EA63155BC825E;
// System.Func`2<UnityEngineInternal.Input.NativeInputUpdateType,System.Boolean>
struct Func_2_t880CA675AE5D39E081BEEF14DC092D82674DE4F2;
// System.Func`2<UnityEngine.TextCore.Text.SpriteCharacter,System.UInt32>
struct Func_2_tF0A06FF312E9CFC173E3513FCD90DA99BA52B5B1;
// System.Func`2<UnityEngine.TextCore.Text.SpriteGlyph,System.UInt32>
struct Func_2_t12C2A18A794EA750752F8735CC29CD839D6B4B84;
// System.Func`3<System.Int32,System.IntPtr,System.Boolean>
struct Func_3_t2376B3D8C7A437FC32F21C4C4E4B3E7D2302007C;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t4A2F2B74276D0AD3ED0F873045BD61E9504ECAE2;
// System.Collections.Generic.HashSet`1<System.UInt32>
struct HashSet_1_t5DD20B42149A11AEBF12A75505306E6EFC34943A;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t349E66EC5F09B881A8E52EE40A1AB9EC60E08E44;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Timeline.TrackAsset>
struct IEnumerable_1_tCF360FA8155395D7F2E3092E355BE18C4A37F7E0;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Playables.PlayableBinding>
struct IEnumerator_1_t22AE37A791011C389F4D3E3ACD25F7336D5E2D5D;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Timeline.TrackAsset>
struct IEnumerator_1_t7C55B8BC383F7FAC09A311AB9528E677C60F1F41;
// UnityEngine.Timeline.IntervalTree`1<UnityEngine.Timeline.RuntimeElement>
struct IntervalTree_1_t56942FFEDE9739CBDEB79B0B42BE1F89DA07314B;
// System.Collections.Generic.List`1<UnityEngine.Camera>
struct List_1_tD2FA3273746E404D72561E8324608D18B52B533E;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7;
// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.Character>
struct List_1_tFED0F30EE65D995591571D3CD2C10F22439CB317;
// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.FontAsset>
struct List_1_t55B85B981AC5FD6A5358491F90FE354F78BB97DE;
// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
struct List_1_tA5BCD116CC751A5F35C7D3D7B96DC3A5D22B9C82;
// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
struct List_1_tC0F62C6753434D94B7A8CFEB0E642E533349DD30;
// System.Collections.Generic.List`1<UnityEngine.TextCore.Glyph>
struct List_1_t95DB74B8EE315F8F92B7B96D93C901C8C3F6FE2C;
// System.Collections.Generic.List`1<UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord>
struct List_1_t3CA8EA3609B406A4099002CBD02BB599F3B1D5DB;
// System.Collections.Generic.List`1<UnityEngine.TextCore.GlyphRect>
struct List_1_t425D3A455811E316D2DF73E46CF9CD90A4341C1B;
// System.Collections.Generic.List`1<UnityEngine.Timeline.IMarker>
struct List_1_tB481045C42962DD282E8A89B2AF0246A4042EADF;
// System.Collections.Generic.List`1<UnityEngine.Timeline.ITimelineEvaluateCallback>
struct List_1_t66D70AC14455298A9DB791AD2621759C701785A9;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73;
// System.Collections.Generic.List`1<UnityEngine.IntegratedSubsystem>
struct List_1_t78E7232867D713AA9907E71F6C5B19B226F0B180;
// System.Collections.Generic.List`1<UnityEngine.IntegratedSubsystemDescriptor>
struct List_1_tACFC79734710927A89702FFC38900223BB85B5A6;
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
struct List_1_tCD5F926D25FC8BFAF39E4BE6F879C1FA11501C76;
// System.Collections.Generic.List`1<UnityEngine.Timeline.RuntimeElement>
struct List_1_t10B46D1B2917EFB9569F790FA432CF6C1C534BEF;
// System.Collections.Generic.List`1<UnityEngine.ScriptableObject>
struct List_1_tF941E9C3FEB6F1C2D20E73A90AA7F6319EB3F828;
// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.SpriteAsset>
struct List_1_t3EE59C28A34FCD5060EF6B6BAFA85F2C9D01D320;
// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.SpriteCharacter>
struct List_1_t7DA088250C54C07AF1211AE132355AD2D343EE51;
// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.SpriteGlyph>
struct List_1_t063B87D3CFDC3AEE80E33EFBDA1410C697D71AD6;
// System.Collections.Generic.List`1<System.String>
struct List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD;
// System.Collections.Generic.List`1<UnityEngine.Subsystem>
struct List_1_t9E8CCD70A25458CE30A64503B35F06ECA62E3052;
// System.Collections.Generic.List`1<UnityEngine.SubsystemDescriptor>
struct List_1_t15AD773D34D3739AFB67421B6DFFACEA7638F64E;
// System.Collections.Generic.List`1<UnityEngine.SubsystemsImplementation.SubsystemDescriptorWithProvider>
struct List_1_t2D19D6F759F401FE6C5460698E5B8249E470E044;
// System.Collections.Generic.List`1<UnityEngine.SubsystemsImplementation.SubsystemWithProvider>
struct List_1_tD834E8FB7FDC0D4243FBCF922D7FE4E3C707AAC3;
// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.TextStyle>
struct List_1_t2AFC9C88B8DEFBC5C4C13A94CD97E65EF9AA29C1;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t0F231C3F13EBA1FF9081BD61489D01AA3CBE59D4;
// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip>
struct List_1_tD78196B4DE777C4B74ADAD24051A9978F5191506;
// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset>
struct List_1_t6908BEEFB57470CB30420983896AA06BFB8796F0;
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_tDA2C18E15C40590123A37DABB6D0D9AEB77A3BBD;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t67A1600A303BB89506DFD21B59687088B7E0675B;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t9209B29AC606399207E97BDCD817DEA5B6C63CA5;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t09F8990ACE8783E311B473B0090859BA9C00FC2A;
// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t9B68833848E4C4D7F623C05F6B77F0449396354A;
// System.Collections.Generic.List`1<UnityEngine.Yoga.YogaNode>
struct List_1_t84B666107A8A3ECB0F5A24B0243137D056DA9165;
// System.Collections.Generic.List`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>
struct List_1_t34937A123BB6F1345F58E1BE04611AC43FA110C2;
// System.Collections.Generic.List`1<CartoonFX.CFXR_Effect/CameraShake>
struct List_1_t195AE9539E927EB5BCA43B11B0FC84CC5A780A06;
// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.TextSettings/FontReferenceMap>
struct List_1_tA1547550E5FBA50050B20DA74245C38434654EE8;
// System.Collections.Generic.List`1<UnityEngine.Timeline.TimeNotificationBehaviour/NotificationEntry>
struct List_1_t2C68AC15B44F139479D9C8C55F4D093DE5F74CD2;
// System.Collections.Generic.Queue`1<UnityEngine.Transform>
struct Queue_1_tED3A82CB4024BD256422C3EF148E0EDAFDFC852A;
// System.Collections.Generic.Stack`1<DG.Tweening.Tween>
struct Stack_1_t5BFE231094D4C21C56F26E6EE0BC9A194AD9A5B6;
// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>
struct Task_1_tE41CFF640EB7C045550D9D0D92BE67533B084C17;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_tF0ADCA0C226C9C243ACB55E67D852E4BB53AEB67;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
struct AchievementDescriptionU5BU5D_t6B3ED222FB06DD89115602C955A2CD98E000CCC5;
// System.Reflection.Assembly[]
struct AssemblyU5BU5D_t97B7B4E3FD4DA4944A4BFAA4DC484EA7D990B339;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// UnityEngine.Camera[]
struct CameraU5BU5D_t1506EBA524A07AD1066D6DD4D7DFC6721F1AC26B;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// UnityEngine.Color32[]
struct Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259;
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411;
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t427621BF8902AE33C86E7BF384D9B2B5B781F949;
// DG.Tweening.Plugins.Core.PathCore.ControlPoint[]
struct ControlPointU5BU5D_t52F9D1EC70E441ED3915E30FFB75F9B95AD56C59;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// UnityEngine.TextCore.Text.FontWeightPair[]
struct FontWeightPairU5BU5D_t76E8DB55C81EEBEFA2E6D1D3E3B3EA1FB4C4954F;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t1BA4BCF4D4D32DF07E9B84F1750D964DF33B0FEC;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF;
// UnityEngine.TextCore.Glyph[]
struct GlyphU5BU5D_t345CEC8703A6C650639C40DB7D35269A2D467FC5;
// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct[]
struct GlyphMarshallingStructU5BU5D_t9424A4B1FAAD615472A9346208026B1B9E22069E;
// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord[]
struct GlyphPairAdjustmentRecordU5BU5D_tD5DD2A739A4CA745E7F28ECCB2CD0BD0A65A38F7;
// UnityEngine.TextCore.GlyphRect[]
struct GlyphRectU5BU5D_t494B690215E3F3F42B6F216930A461256CE2CC70;
// UnityEngine.HumanBone[]
struct HumanBoneU5BU5D_t443B81D55400778CBB921DF04BE932ABF14BAA52;
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t72B1FC43A0166FFFA30AF4E10BCA837E34A6B042;
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t0179D2FF9BD9F78A4E0A10AE350DC1F19E5FCB43;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// UnityEngine.TextCore.Text.LineInfo[]
struct LineInfoU5BU5D_t37598F2175B291797270D1161DC29B6296FB169D;
// UnityEngine.TextCore.Text.LinkInfo[]
struct LinkInfoU5BU5D_tB7EB23E47AF29CCBEC884F9D0DB95BC97F62AE51;
// UnityEngine.TextCore.Text.MaterialReference[]
struct MaterialReferenceU5BU5D_t4A9B88114E223BD96CE5121053664023CE2DE07E;
// UnityEngine.TextCore.Text.MeshInfo[]
struct MeshInfoU5BU5D_t3DF8B75BF4A213334EED197AD25E432212894AC6;
// UnityEngine.TextCore.Text.PageInfo[]
struct PageInfoU5BU5D_tFEA2CF88695491CFC2F2A2EF6BDCC56E52B0A6D4;
// UnityEngine.Playables.PlayableBinding[]
struct PlayableBindingU5BU5D_tC50C3F27A8E4246488F7A5998CAABAC4811A92CD;
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t9C963C4B9AAD862BBE402147E82F7BEBF699F6A6;
// UnityEngine.TextCore.Text.RichTextTagAttribute[]
struct RichTextTagAttributeU5BU5D_tEE9D071B3246F23742DBF4226567620BCBB24A14;
// System.Single[]
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C;
// UnityEngine.SkeletonBone[]
struct SkeletonBoneU5BU5D_t84722CE071EF9295326D5A84492716DBC7A15A85;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// UnityEngine.TextCore.Text.TextAlignment[]
struct TextAlignmentU5BU5D_t756DC2D672145699CB9718DDBA5982ED51A95F49;
// UnityEngine.TextCore.Text.TextColorGradient[]
struct TextColorGradientU5BU5D_tA27A5E49640CF01334A10DBDBC959903AFBD941A;
// UnityEngine.TextCore.Text.TextElementInfo[]
struct TextElementInfoU5BU5D_tEC28C9B72883EE21AA798913497C69E179A15C4E;
// UnityEngine.TextCore.Text.TextFontWeight[]
struct TextFontWeightU5BU5D_t3DE32809AEE657255C8333897D61F2EA5279D43F;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t05332F1E3F7D4493E304C702201F9BE4F9236191;
// UnityEngine.Timeline.TimelineClip[]
struct TimelineClipU5BU5D_t37945156A55BC896C442C4FE59198216769A4E64;
// UnityEngine.Timeline.TrackAsset[]
struct TrackAssetU5BU5D_tE6935AFD32D0BE4B0C69D1CCE96B55D383BCF88C;
// DG.Tweening.Tween[]
struct TweenU5BU5D_t6E5F58C51FA0DA64C990C1C7E02EE9B70220C2F7;
// System.UInt32[]
struct UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_tCE4194A0D6665FFF7943DDC3B0B9301D57F84A6A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
// UnityEngine.TextCore.Text.WordInfo[]
struct WordInfoU5BU5D_tAD74C9720883D7BB229A20FFAE9EFD2CF9963F7B;
// UnityEngine.TextCore.Text.XmlTagAttribute[]
struct XmlTagAttributeU5BU5D_tEDFE75BDDC81D11CEA2F2A12583516D3BFB309B2;
// CartoonFX.CFXR_Effect/AnimatedLight[]
struct AnimatedLightU5BU5D_t18D44AAF10E0A23939730E3057A03CE2E81E4879;
// CartoonFX.CFXR_ParticleTextFontAsset/Kerning[]
struct KerningU5BU5D_t71F41AB873624E12E9C1447A4C794D52A931B19A;
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_tDAE7DF0D2B0BE3EB2FD25FB4418704E27A2BF1D5;
// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder
struct ABSPathDecoder_t6B479550CEF6C183ACCA13F11E29E019270AB61C;
// System.Action
struct Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07;
// UnityEngine.Animation
struct Animation_t6593B06C39E3B139808B19F2C719C860F3F61040;
// UnityEngine.AnimationClip
struct AnimationClip_t00BD2F131D308A4AD2C6B0BF66644FC25FECE712;
// UnityEngine.AnimationCurve
struct AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354;
// UnityEngine.Timeline.AnimationPlayableAsset
struct AnimationPlayableAsset_t381BF46F8FEBECFD107EC61C257C60D9438FDF33;
// UnityEngine.AnimationState
struct AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE;
// UnityEngine.Timeline.AnimationTrack
struct AnimationTrack_tBE37C239976A84F0A2E6BA3B77263B4F44BB1359;
// System.Collections.ArrayList
struct ArrayList_t7A8E5AF0C4378015B5731ABE2BED8F2782FEEF8A;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_tE2E8D79241711CDF9AB428C7FB96A35D80E40B04;
// UnityEngine.Experimental.Audio.AudioSampleProvider
struct AudioSampleProvider_t602353124A2F6F2AEC38E56C3C21932344F712E2;
// UnityEngine.AudioSource
struct AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299;
// UnityEngine.AvatarMask
struct AvatarMask_tC1D777FFB77C952502ECF6D80FAFAD16B27B02AF;
// UnityEngine.Yoga.BaselineFunction
struct BaselineFunction_t13AFADEF52F63320B2159C237635948AEB801679;
// Unity.Burst.BurstCompilerOptions
struct BurstCompilerOptions_t5F93118F305E1B0C950C6F9AF8BCA74033DA01C9;
// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3;
// CartoonFX.CFXR_ParticleTextFontAsset
struct CFXR_ParticleTextFontAsset_t19D1F73819A2F60E0FA1B17ECA99FB3FAAF756ED;
// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184;
// UnityEngine.CanvasGroup
struct CanvasGroup_t048C1461B14628CFAEBE6E7353093ADB04EBC094;
// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder
struct CatmullRomDecoder_tBC93937ED94DB6355B974915EE9885854F1A5EB0;
// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804;
// UnityEngine.TextCore.Text.Character
struct Character_t9B671B493FAC8D43638C69AF6AE92CBD103D80EC;
// UnityEngine.CharacterController
struct CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A;
// UnityEngine.Collider
struct Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// DG.Tweening.Plugins.Core.PathCore.CubicBezierDecoder
struct CubicBezierDecoder_t58382D9354F3F75F8D6A235E945C013EACD3CC1D;
// System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB;
// DG.Tweening.EaseFunction
struct EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04;
// System.Linq.Expressions.Expression
struct Expression_t70AA908ECBD33E94249BF235E4EBB0F831AD8785;
// UnityEngine.Font
struct Font_tC95270EA3198038970422D78B74A7F2E218A96B6;
// UnityEngine.TextCore.Text.FontAsset
struct FontAsset_t61A6446D934E582651044E33D250EA8D306AB958;
// UnityEngine.TextCore.Text.FontFeatureTable
struct FontFeatureTable_t992E0493CD7E9D7834DF204E0198237F0D25B3B7;
// UnityEngine.GUIContent
struct GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F;
// UnityEngine.GUILayoutGroup
struct GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D;
// UnityEngine.GUISettings
struct GUISettings_tF2CA7E8B9F62F1FC013BFF053B5FA2709EBA3847;
// UnityEngine.GUISkin
struct GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9;
// UnityEngine.GUIStyle
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580;
// UnityEngine.GUIStyleState
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// UnityEngineInternal.GenericStack
struct GenericStack_t1FB49AB7D847C97ABAA97AB232CA416CABD24C49;
// UnityEngine.TextCore.Glyph
struct Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F;
// UnityEngine.Gradient
struct Gradient_tA7FEBE2FDB4929FFF6C997134841046F713DAC1E;
// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_t0680C7F905C553076B552D5A1A6E39E2F0F36AA2;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// UnityEngine.Playables.INotification
struct INotification_tEF630287442F0A66470493068A5D158E3C2D3C6B;
// UnityEngine.SocialPlatforms.IScore
struct IScore_t95B1CFEB3094570AFF240C7CDD61A3A5D0169E86;
// UnityEngine.ISubsystemDescriptor
struct ISubsystemDescriptor_tEF29944D579CC7D70F52CB883150735991D54E6E;
// DG.Tweening.Plugins.Core.ITweenPlugin
struct ITweenPlugin_t6CC45CF7D7F91626F1BD45D75418FF1AC2DC5166;
// UnityEngine.UI.Image
struct Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E;
// UnityEngine.UI.LayoutElement
struct LayoutElement_tB1F24CC11AF4AA87015C8D8EE06D22349C5BF40A;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
struct Leaderboard_tBDB34CC6F79318BE6D7761015C70C8A5CC64EC71;
// UnityEngine.Light
struct Light_t1E68479B7782AF2050FAA02A5DC612FD034F18F3;
// UnityEngine.LineRenderer
struct LineRenderer_tEFEF960672DB69CB14B6D181FAE6292F0CF8B63D;
// DG.Tweening.Plugins.Core.PathCore.LinearDecoder
struct LinearDecoder_tC7C53176BBF58227DC1855AFDBA3FAFF19860B15;
// UnityEngine.SocialPlatforms.Impl.LocalUser
struct LocalUser_t55C68E98993F86B6FBB7A25F28EB989CD7E6A3AD;
// UnityEngine.Yoga.Logger
struct Logger_t092B1218ED93DD47180692D5761559B2054234A0;
// UnityEngine.Timeline.MarkerTrack
struct MarkerTrack_tE18594CE52CCC412606B1B5A147DD3A4F7D056C2;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D;
// UnityEngine.Yoga.MeasureFunction
struct MeasureFunction_t60EBED1328F5328D4FA7E26335967E59E73B4D09;
// UnityEngine.Mesh
struct Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4;
// UnityEngine.MeshCollider
struct MeshCollider_tB525E4DDE383252364ED0BDD32CF2B53914EE455;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// UnityEngineInternal.Input.NativeUpdateCallback
struct NativeUpdateCallback_tC5CA5A9117B79251968A4DA3758552EFE1D37495;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// UnityEngine.UI.Outline
struct Outline_t9CF146E077DC65F441EDEC463AA6710374108084;
// UnityEngine.ParticleSystem
struct ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1;
// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t6964211C3DAE503FEEDD04089ED6B962945D271E;
// UnityEngine.RectOffset
struct RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5;
// UnityEngine.RectTransform
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5;
// System.Text.RegularExpressions.Regex
struct Regex_tE773142C2BE45C5D362B0F815AFF831707A51772;
// UnityEngine.RenderTexture
struct RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27;
// UnityEngine.Renderer
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF;
// UnityEngine.Rigidbody
struct Rigidbody_t268697F5A994213ED97393309870968BC1C7393C;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t17D2F2939CA8953110180DF53164CFC3DC88D70E;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t5C292A12062F24027A98492F52ECFE9802AA6F0E;
// DG.Tweening.Sequence
struct Sequence_tEADBE56D6ED2E9EE8FB2E5459C3E57131EC0545C;
// UnityEngine.Shader
struct Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692;
// UnityEngine.UI.Slider
struct Slider_t87EA570E3D6556CABF57456C2F3873FFD86E652F;
// UnityEngine.Sprite
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99;
// UnityEngine.TextCore.Text.SpriteAsset
struct SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62;
// UnityEngine.TextAsset
struct TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69;
// UnityEngine.TextCore.Text.TextAsset
struct TextAsset_tB28F1843A877CCA74B89DC4F63EA532618B049B8;
// UnityEngine.TextCore.Text.TextColorGradient
struct TextColorGradient_t22D94E441E8E8CD772B966C167E5C0AEB0919D70;
// UnityEngine.TextCore.Text.TextElement
struct TextElement_tCEF567A8810788262275B39DC39CBA6EBE7472DA;
// UnityEngine.TextCore.Text.TextInfo
struct TextInfo_t27E58E62A7552C66D38C175AF9D22622365F5D09;
// UnityEngine.TextCore.Text.TextSettings
struct TextSettings_tB7F55685AFFD4A96F714427BCACFD6958E357D64;
// UnityEngine.TextCore.Text.TextStyleSheet
struct TextStyleSheet_t86A0FA5523897465F371A2ABC17DFA3558C8D15E;
// UnityEngine.Texture
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700;
// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
// UnityEngine.Timeline.TimelineAsset
struct TimelineAsset_tE400C944B07CA9D1349BAD84545E24075ADB3496;
// UnityEngine.Timeline.TimelineClip
struct TimelineClip_t003008F08E56A75F3A47FD9ADE7C066988A3371D;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_tE87B78A3DAED69816B44C99270A734682E093E7A;
// UnityEngine.Timeline.TrackAsset
struct TrackAsset_t31E19BE900C90F6616C0D337652C8614CD833B96;
// UnityEngine.TrailRenderer
struct TrailRenderer_tF7E185EF383CF4300DFC3E1DE59FA6100B93CA39;
// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
// DG.Tweening.Tween
struct Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C;
// DG.Tweening.TweenCallback
struct TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24;
// System.Type
struct Type_t;
// UnityEngine.TextCore.Text.UnicodeLineBreakingRules
struct UnicodeLineBreakingRules_t80BE36F5E16AE48FE7B6DE1C91D36B1142B4EC0E;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F;
// UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6;
// System.Uri
struct Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// System.Threading.WaitCallback
struct WaitCallback_tFB2C7FD58D024BBC2B0333DC7A4CB63B8DEBD5D3;
// System.WeakReference
struct WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E;
// UnityEngine.Yoga.YogaConfig
struct YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345;
// UnityEngine.Yoga.YogaNode
struct YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA;
// UnityEngine.Analytics.AnalyticsSessionInfo/IdentityTokenChanged
struct IdentityTokenChanged_tE8CB0DAB5F6E640A847803F582E6CB6237742395;
// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct SessionStateChanged_t1180FB66E702B635CAD9316DC661D931277B2A0C;
// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback
struct OnOverrideControllerDirtyCallback_tDC67F7661A27502AD804BDE0B696955AFD4A44D5;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t3396D9613664F0AFF65FB91018FD0F901CC16F1E;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t8D7135A2FB40647CAEC93F5254AD59E18DEB6072;
// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler
struct SampleFramesHandler_tFE84FF9BBCEFB880D46227188F375BEF680AAA30;
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_tE071B0CBA3B3A77D3E41F5FCB65B4017885B3177;
// CartoonFX.CFXR_Effect/CameraShake
struct CameraShake_tF662E3447A81E0F4B337C630E8DDF9758C02DFD0;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC;
// DG.Tweening.Core.DOTweenSettings/ModulesSetup
struct ModulesSetup_t888D98957DAB1E84C63C5F866C838A0E714F7236;
// DG.Tweening.Core.DOTweenSettings/SafeModeOptions
struct SafeModeOptions_tA3C410AFB34C85C70704176B558984B1BA8AD880;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t76D5E172DF8AA57E67763D453AAC40F0961D09B1;
// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_tF844B2FAD6933B78FD5EFEBDE0529BCBAC19BA60;
// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_tA6D456E853D58AD2EF8A599F543C7E5BA8E94B98;
// UnityEngine.Playables.PlayableBinding/CreateOutputMethod
struct CreateOutputMethod_tD18AFE3B69E6DDD913D82D5FA1D5D909CEEC8509;
// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_tB0D5A5BA322FE093894992C29DCF51E7E12579C4;
// UnityEngine.Timeline.TimelineAsset/EditorSettings
struct EditorSettings_t3A8D02A80F57944B75911D9692FECDF6B7081DFF;

struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_com;
struct Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B;
struct ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801;
struct ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com;
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke;
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_com;
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_pinvoke;
struct HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_com;
struct HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_pinvoke;
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com;
struct RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com;
struct SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_com;
struct SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_pinvoke;
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_com;
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_pinvoke;
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com;
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7;
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tF062866229C4952B8051AD32AB6E9D931142CC95 
{
};

// <Module>
struct U3CModuleU3E_tACA7C44D47EE520DBE4B854B783CBE378B317C8B 
{
};

// <Module>
struct U3CModuleU3E_t61602430437A8A096CA205928842E5E8AA51729D 
{
};

// <Module>
struct U3CModuleU3E_t5C873E33121FECD351F7CB74331B5D342A4C978F 
{
};

// <Module>
struct U3CModuleU3E_t90149EF90407715CC46EB5A9704669888393F1DE 
{
};

// <Module>
struct U3CModuleU3E_t030FC11D37663E6AAF04929A5E099A4974BAB334 
{
};

// <Module>
struct U3CModuleU3E_tDA285F13E9413BF3B79A99D6E310BE9AF3444EEB 
{
};

// <Module>
struct U3CModuleU3E_tA3942657997767549ED3B944EB77AFA183BBF4B9 
{
};

// <Module>
struct U3CModuleU3E_t4791F64F4B6411D4D033A002CAD365D597AA2451 
{
};

// <Module>
struct U3CModuleU3E_t7A78175E99B61C7B4022EA3D1E12E92F7F669089 
{
};

// <Module>
struct U3CModuleU3E_t9A99FA938A0D410D25805C4E4D63978454163723 
{
};

// <Module>
struct U3CModuleU3E_tBF95EDE7AC72079FEC40B1A3B57D72418FF75C99 
{
};

// <Module>
struct U3CModuleU3E_tFD9C433EC5744AFEE387384BF931F3B170BC806B 
{
};

// <Module>
struct U3CModuleU3E_t853A105E2E1595E463CC860AFEE0FB13A177A12C 
{
};

// <Module>
struct U3CModuleU3E_t8B36B9B16FF72CF5A0EBA03D2FA162E77C86534C 
{
};

// <Module>
struct U3CModuleU3E_t462BCCFB9B78348533823E0754F65F52A5348F89 
{
};

// <Module>
struct U3CModuleU3E_t30BE97772842985A3AF723FA1759387A3E79C563 
{
};

// <Module>
struct U3CModuleU3E_t48670A4A557D138A8C338D0B799AEE0AB575434C 
{
};

// <Module>
struct U3CModuleU3E_t2C7BF608494A5C8FB8C8C4D318FB27BCF6CE322A 
{
};

// <Module>
struct U3CModuleU3E_tB500DDB8D220627F9BFAF448763790F027856F11 
{
};

// <Module>
struct U3CModuleU3E_t941B0EB06FD57B79F043CCA70C8AA4C0B3FB68E7 
{
};

// <Module>
struct U3CModuleU3E_tE7BE147157D59625477C35248C6A6C59EA2900FB 
{
};

// <Module>
struct U3CModuleU3E_t2F9091E403B25A5364AE8A6B2C249E31D405E3F4 
{
};

// <Module>
struct U3CModuleU3E_tD4D8152B1CC10B76FF3BD3BF122F926B6BF0D3EE 
{
};

// <Module>
struct U3CModuleU3E_t0643977EA9107777E6F2E30DC5F5326A467F5F6B 
{
};

// <Module>
struct U3CModuleU3E_tCFCF033B61CFCC76C69180CF9A7B07EED67725EA 
{
};

// <Module>
struct U3CModuleU3E_t416C1B54F702B9F0B5C7C848BFDFA85A9E90F443 
{
};

// <Module>
struct U3CModuleU3E_t4BC86AB59C63F2CC6EB51BE560C15CFCAE821BC7 
{
};

// <Module>
struct U3CModuleU3E_tCDD16073F88F09BB7B50158A053DD15949D8ADB6 
{
};

// <Module>
struct U3CModuleU3E_t3B74AF9E7E84B3C57D4687184E31363228069DF2 
{
};

// <Module>
struct U3CModuleU3E_tCCE5947EACBB46966D34E517FB03602C848AA9A0 
{
};

// <Module>
struct U3CModuleU3E_t806C4A82D63BA5BEE007D75772441609D967BADA 
{
};

// <Module>
struct U3CModuleU3E_t5E8190EE43F4DF5D80E8A6651A0469A8FD445F94 
{
};

// <Module>
struct U3CModuleU3E_tCFDAF3CE34E8117DEABC58BB3EBDB7B80EA66F5A 
{
};

// <Module>
struct U3CModuleU3E_t7A1E3DF1BFD27FA828B031F2A96909F13C3F170B 
{
};

// <Module>
struct U3CModuleU3E_t6B4A7D64487421A1C7A9ACB5578F8A35510E2A0C 
{
};

// <Module>
struct U3CModuleU3E_t42F165DEA2597BD5AB2C914FCF80349ECF878162 
{
};

// <Module>
struct U3CModuleU3E_t0F6AB019D77D717D42BE5AD848FFBD032B14CFFC 
{
};

// <Module>
struct U3CModuleU3E_t98AB86DBD4B6990BFAA7E2230BDA19430302AC99 
{
};

// <Module>
struct U3CModuleU3E_tB4B8BFF6E6672C9C5CA86F52CA248539A4A5120B 
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct ABSTweenPlugin_3_tC26278B0CA053BAF4547345679D3E8D1F748AC12  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
struct ABSTweenPlugin_3_t2B071FD0D9C7410BEDE633CDB14F13654FC2A973  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t7A4F79038C7079DFB69340991577802A9439A59D  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t79D91C3E0DB82A874E072CF1EF486B5F78D7F8DE  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t7341CD6BAA9CD38815B6A9E3CFF47989DE634ABF  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t56BEDD6B006DC2E8D499101DE8A2339425AE6A10  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct ABSTweenPlugin_3_t3823C0F935A3168B9E48DC90ABD9A0CED3D7BB82  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct ABSTweenPlugin_3_tA51581359A1B697A7A0A8F2FFDE00794FA1E67F4  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_tEDADD90C5C3ECECC20453D4CCA444C32237B4DA6  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct ABSTweenPlugin_3_t60F4DE5120CFD5986925189A0E775FAEAB4C59B9  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>
struct ABSTweenPlugin_3_tE7EF56C4695649256BA2077D8E447E739C869256  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>
struct ABSTweenPlugin_3_t76C40FFA153363CF88C31D954E2AE740916B5B22  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t303ADD886C8A7272DE43A8D065EEB864F5E03BF4  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.CircleOptions>
struct ABSTweenPlugin_3_tF3514EEC2BD29A12B4641076F59405DFB6106DAE  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t4082710A2BB933E2D055E454B3EFAC4C0A319444  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct ABSTweenPlugin_3_tAD778405CFD8B0E13EF7423622294E3E3913479B  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>
struct ABSTweenPlugin_3_t08B14BED068ACE348E543E45725D6C6BFFA60143  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_tE5A78BE46D046C07A6356B8AB596B2D00F9295E7  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t9F8F8099624B165B75CB1E0C53FA9762CB2815FC  : public RuntimeObject
{
};

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_tCA0A4120E1B13462A402E739CE2DD9CA72BAC713  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder
struct ABSPathDecoder_t6B479550CEF6C183ACCA13F11E29E019270AB61C  : public RuntimeObject
{
};

// DG.Tweening.Core.ABSSequentiable
struct ABSSequentiable_t05DF85FC63E3650D2D4CF6ABBA0F43263EB8CE89  : public RuntimeObject
{
	// DG.Tweening.TweenType DG.Tweening.Core.ABSSequentiable::tweenType
	int32_t ___tweenType_0;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedPosition
	float ___sequencedPosition_1;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedEndPosition
	float ___sequencedEndPosition_2;
	// DG.Tweening.TweenCallback DG.Tweening.Core.ABSSequentiable::onStart
	TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24* ___onStart_3;
};

// Mono.Security.ASN1
struct ASN1_t33549D58797C9C33AA83F13AD184EAA00C584A6F  : public RuntimeObject
{
	// System.Byte Mono.Security.ASN1::m_nTag
	uint8_t ___m_nTag_0;
	// System.Byte[] Mono.Security.ASN1::m_aValue
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___m_aValue_1;
	// System.Collections.ArrayList Mono.Security.ASN1::elist
	ArrayList_t7A8E5AF0C4378015B5731ABE2BED8F2782FEEF8A* ___elist_2;
};

// Mono.Security.ASN1Convert
struct ASN1Convert_tDA6D2B7710D7868F3D559D5BE7F2C7816BB50AB6  : public RuntimeObject
{
};

// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t0D2306DF6EE55C872DB06E6855D7B1AE0E6DDEF9  : public RuntimeObject
{
	// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::m_Title
	String_t* ___m_Title_0;
	// UnityEngine.Texture2D UnityEngine.SocialPlatforms.Impl.AchievementDescription::m_Image
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___m_Image_1;
	// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::m_AchievedDescription
	String_t* ___m_AchievedDescription_2;
	// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::m_UnachievedDescription
	String_t* ___m_UnachievedDescription_3;
	// System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::m_Hidden
	bool ___m_Hidden_4;
	// System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::m_Points
	int32_t ___m_Points_5;
	// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_6;
};

// UnityEngine.Analytics.AnalyticsSessionInfo
struct AnalyticsSessionInfo_tDE8F7A9E13EF9723E2D975F76E916753DA61AD76  : public RuntimeObject
{
};

// UnityEngine.Animations.AnimationPlayableBinding
struct AnimationPlayableBinding_t44AB912C26E38CBC1F5027F5C96A321C3B1C1612  : public RuntimeObject
{
};

// UnityEngine.Animations.AnimationPlayableExtensions
struct AnimationPlayableExtensions_t3F93F70729DACA99F423992CFC1B380C1942AD4D  : public RuntimeObject
{
};

// UnityEngine.Animations.AnimationPlayableGraphExtensions
struct AnimationPlayableGraphExtensions_tF833C072961F30409DB4D2A1B8B1B5BAE53221B3  : public RuntimeObject
{
};

// System.Attribute
struct Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA  : public RuntimeObject
{
};

// UnityEngine.Experimental.Audio.AudioSampleProvider
struct AudioSampleProvider_t602353124A2F6F2AEC38E56C3C21932344F712E2  : public RuntimeObject
{
	// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler UnityEngine.Experimental.Audio.AudioSampleProvider::sampleFramesAvailable
	SampleFramesHandler_tFE84FF9BBCEFB880D46227188F375BEF680AAA30* ___sampleFramesAvailable_0;
	// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler UnityEngine.Experimental.Audio.AudioSampleProvider::sampleFramesOverflow
	SampleFramesHandler_tFE84FF9BBCEFB880D46227188F375BEF680AAA30* ___sampleFramesOverflow_1;
};

// UnityEngine.AudioSettings
struct AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD  : public RuntimeObject
{
};

// Mono.Security.BitConverterLE
struct BitConverterLE_tB6EF365ED05024FCC12DA3939B10FDEBDB29E1BD  : public RuntimeObject
{
};

// DG.Tweening.Core.Easing.Bounce
struct Bounce_t0AEC0E86192AFD2DF3F551A099C4902C113A26E9  : public RuntimeObject
{
};

// Unity.Burst.BurstCompiler
struct BurstCompiler_t2715484E1FF256726FC4D4D8E17C35A4C8DFA2B8  : public RuntimeObject
{
};

// Unity.Burst.BurstCompilerOptions
struct BurstCompilerOptions_t5F93118F305E1B0C950C6F9AF8BCA74033DA01C9  : public RuntimeObject
{
	// System.Boolean Unity.Burst.BurstCompilerOptions::_enableBurstCompilation
	bool ____enableBurstCompilation_3;
	// System.Boolean Unity.Burst.BurstCompilerOptions::_enableBurstSafetyChecks
	bool ____enableBurstSafetyChecks_4;
	// System.Boolean Unity.Burst.BurstCompilerOptions::<IsGlobal>k__BackingField
	bool ___U3CIsGlobalU3Ek__BackingField_5;
	// System.Action Unity.Burst.BurstCompilerOptions::<OptionsChanged>k__BackingField
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___U3COptionsChangedU3Ek__BackingField_6;
};

// Unity.Burst.BurstRuntime
struct BurstRuntime_tA87CEB6EE77F6DA708C87C3DAEC7862E3A1B0EA1  : public RuntimeObject
{
};

// Unity.Burst.BurstString
struct BurstString_tD6AF700FD5AF48728FC90C6CA2AA2E48C6472AF1  : public RuntimeObject
{
};

// UnityEngine.CameraRaycastHelper
struct CameraRaycastHelper_tEF8B5EE50B6F5141652EAAF44A77E8B3621FE455  : public RuntimeObject
{
};

// UnityEngine.TextCore.Text.ColorUtilities
struct ColorUtilities_tC9ADAE7FE6DAC43204E7B087D23F6BA85A111D1D  : public RuntimeObject
{
};

// System.Configuration.ConfigurationElement
struct ConfigurationElement_tAE3EE71C256825472831FFBB7F491275DFAF089E  : public RuntimeObject
{
};

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t1DEB95D3283BB11A46B862E9D13710ED698B6C93  : public RuntimeObject
{
};

// System.Configuration.ConfigurationSectionGroup
struct ConfigurationSectionGroup_tE7948C2D31B193F4BA8828947ED3094B952C7863  : public RuntimeObject
{
};

// UnityEngine.Analytics.ContinuousEvent
struct ContinuousEvent_t71122F6F65BF7EA8490EA664A55D5C03790CB6CF  : public RuntimeObject
{
};

// System.Dynamic.Utils.ContractUtils
struct ContractUtils_tFD5BFE68866F22438D49EF2D69CC0BE6FFF726EC  : public RuntimeObject
{
};

// Mono.Security.Cryptography.CryptoConvert
struct CryptoConvert_t676AC22DA6332E9936696ECC97197AB7B1BC7252  : public RuntimeObject
{
};

// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617  : public RuntimeObject
{
};

// DG.Tweening.DOTweenCYInstruction
struct DOTweenCYInstruction_tCB7C6C1E61447CF998FF1A85EDB5949ED08C303B  : public RuntimeObject
{
};

// DG.Tweening.Core.DOTweenExternalCommand
struct DOTweenExternalCommand_tAD5F307461CED759CA3533F555E48B09C354C49D  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModuleAudio
struct DOTweenModuleAudio_tCF19013F0C086697BB9D373975D877E26BD2CE09  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModulePhysics
struct DOTweenModulePhysics_t288D427B27A061E96FAD8BB322D3640CA9EC1C14  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModulePhysics2D
struct DOTweenModulePhysics2D_t9F08692BD09A9DC4646782951FD80D8A529905F4  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModuleSprite
struct DOTweenModuleSprite_tF147E9559811CF3DC449BEEBFF0EF43A5DB9BCD7  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModuleUI
struct DOTweenModuleUI_t4988700FDB42C34ACA64236BD53E96D3049D86FE  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModuleUnityVersion
struct DOTweenModuleUnityVersion_t028EE91988A2A48FC2EEACA97E9BA03468B748F6  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModuleUtils
struct DOTweenModuleUtils_t5554865584F951A4A4E5DD282E6EBC60F5CEC6E9  : public RuntimeObject
{
};

// DG.Tweening.Core.DOTweenUtils
struct DOTweenUtils_t88FB5E3E9A98EB0C75054BC17E200352D2D658D1  : public RuntimeObject
{
};

// DG.Tweening.Core.Debugger
struct Debugger_tCF42DBFBF81B46CDEE59CA397F2860F3427FE1F0  : public RuntimeObject
{
};

// DG.Tweening.Core.Easing.EaseCurve
struct EaseCurve_t36BD7EAD5E58656A06E0D71596FF781803BE9C5F  : public RuntimeObject
{
	// UnityEngine.AnimationCurve DG.Tweening.Core.Easing.EaseCurve::_animCurve
	AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* ____animCurve_0;
};

// DG.Tweening.Core.Easing.EaseManager
struct EaseManager_t7E8B166DF6DFCEE3386DAB1524EC423E3523EB3C  : public RuntimeObject
{
};

// System.Linq.Enumerable
struct Enumerable_t372195206D92B3F390693F9449282C31FD564C09  : public RuntimeObject
{
};

// System.Linq.Error
struct Error_tCE0C9D928B2D2CC69DDEC1A0ECF05131938959DB  : public RuntimeObject
{
};

// System.Linq.Expressions.Error
struct Error_t2E8AD85278E48F4074983610DC3BEE8A3F84454B  : public RuntimeObject
{
};

// System.Linq.Expressions.Expression
struct Expression_t70AA908ECBD33E94249BF235E4EBB0F831AD8785  : public RuntimeObject
{
};

// DG.Tweening.Core.Extensions
struct Extensions_tB285E4629D501497029ADC7BAD9B6FACC6930625  : public RuntimeObject
{
};

// UnityEngine.Timeline.Extrapolation
struct Extrapolation_t4258D23965C0DA9509062D44F7A2AACA99A59DFE  : public RuntimeObject
{
};

// DG.Tweening.Core.Easing.Flash
struct Flash_tBDBDE9EDB27F061B0BB2F196A8A1A9C502F684DF  : public RuntimeObject
{
};

// UnityEngine.TextCore.Text.FontAssetUtilities
struct FontAssetUtilities_t791E501B3F727200A3751332E495071520D393A6  : public RuntimeObject
{
};

// UnityEngine.TextCore.LowLevel.FontEngine
struct FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A  : public RuntimeObject
{
};

// UnityEngine.TextCore.Text.FontFeatureTable
struct FontFeatureTable_t992E0493CD7E9D7834DF204E0198237F0D25B3B7  : public RuntimeObject
{
	// System.Collections.Generic.List`1<UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord> UnityEngine.TextCore.Text.FontFeatureTable::m_GlyphPairAdjustmentRecords
	List_1_t3CA8EA3609B406A4099002CBD02BB599F3B1D5DB* ___m_GlyphPairAdjustmentRecords_0;
	// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord> UnityEngine.TextCore.Text.FontFeatureTable::m_GlyphPairAdjustmentRecordLookup
	Dictionary_2_tDD72F78A572F94ECEDBDA75C3D17C3ED05C167E0* ___m_GlyphPairAdjustmentRecordLookup_1;
};

// UnityEngine.GUIClip
struct GUIClip_t6049AB1B245065014011639ADCF204EE3668221B  : public RuntimeObject
{
};

// UnityEngine.GUIContent
struct GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2  : public RuntimeObject
{
	// System.String UnityEngine.GUIContent::m_Text
	String_t* ___m_Text_0;
	// UnityEngine.Texture UnityEngine.GUIContent::m_Image
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___m_Image_1;
	// System.String UnityEngine.GUIContent::m_Tooltip
	String_t* ___m_Tooltip_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.GUIContent
struct GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2_marshaled_pinvoke
{
	char* ___m_Text_0;
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___m_Image_1;
	char* ___m_Tooltip_2;
};
// Native definition for COM marshalling of UnityEngine.GUIContent
struct GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2_marshaled_com
{
	Il2CppChar* ___m_Text_0;
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___m_Image_1;
	Il2CppChar* ___m_Tooltip_2;
};

// UnityEngine.GUILayout
struct GUILayout_tB26F0D6938B9B2AD04633B1DF56A1E52F3E6D177  : public RuntimeObject
{
};

// UnityEngine.GUILayoutOption
struct GUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14  : public RuntimeObject
{
	// UnityEngine.GUILayoutOption/Type UnityEngine.GUILayoutOption::type
	int32_t ___type_0;
	// System.Object UnityEngine.GUILayoutOption::value
	RuntimeObject* ___value_1;
};

// UnityEngine.GUIUtility
struct GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A  : public RuntimeObject
{
};

// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
struct GameCenterPlatform_tF95E5DA7B46424313E1812B6B80DE102ABB1F96F  : public RuntimeObject
{
};

// UnityEngine.XR.HashCodeHelper
struct HashCodeHelper_tC1D27B890F89E7B1158911DDBE91E869D2087387  : public RuntimeObject
{
};

// UnityEngine.Timeline.HashUtility
struct HashUtility_tE6FFABA5BA93FFF277F35828FFBC704E3CC1910D  : public RuntimeObject
{
};

// UnityEngine.Input
struct Input_t47D83E2A50E6AF7F8A47AA06FBEF9EBE6BBC22BB  : public RuntimeObject
{
};

// UnityEngine.XR.InputDevices
struct InputDevices_t02B79FC19CEA9AC29A9945F5CDA6D790730FBF34  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.InputDevices
struct InputDevices_t02B79FC19CEA9AC29A9945F5CDA6D790730FBF34_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.XR.InputDevices
struct InputDevices_t02B79FC19CEA9AC29A9945F5CDA6D790730FBF34_marshaled_com
{
};

// UnityEngine.XR.InputTracking
struct InputTracking_tA4F34D4D5EC8E560B56ED295177C040D9C9815F1  : public RuntimeObject
{
};

// UnityEngine.Internal_SubsystemDescriptors
struct Internal_SubsystemDescriptors_t087D53EE6F9D7AAEA9E38D42AF436C952DF7936F  : public RuntimeObject
{
};

// UnityEngine.JsonUtility
struct JsonUtility_t731013D97E03B7EDAE6186D6D6826A53B85F7197  : public RuntimeObject
{
};

// UnityEngine.Timeline.MatchTargetFieldConstants
struct MatchTargetFieldConstants_t07C2FAD424736E6B40B0764A78AC93A64983D89B  : public RuntimeObject
{
};

// UnityEngine.TextCore.Text.MaterialManager
struct MaterialManager_t104D2897F78BE83C3377323E18BEB5B8F0704D9B  : public RuntimeObject
{
};

// UnityEngine.TextCore.Text.MaterialReferenceManager
struct MaterialReferenceManager_t04E59E4B24B4F971D73124195671473AEFA6DFA9  : public RuntimeObject
{
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material> UnityEngine.TextCore.Text.MaterialReferenceManager::m_FontMaterialReferenceLookup
	Dictionary_2_tBF325E0F09BEEDF7AC6E6CB85841301637FC6E90* ___m_FontMaterialReferenceLookup_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.TextCore.Text.FontAsset> UnityEngine.TextCore.Text.MaterialReferenceManager::m_FontAssetReferenceLookup
	Dictionary_2_tC20B3D6AE4370C892734F670EF4D1FB9CE91F371* ___m_FontAssetReferenceLookup_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.TextCore.Text.SpriteAsset> UnityEngine.TextCore.Text.MaterialReferenceManager::m_SpriteAssetReferenceLookup
	Dictionary_2_t30736317A47AFC36A7F4FEE0E6487EDC529FB11E* ___m_SpriteAssetReferenceLookup_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.TextCore.Text.TextColorGradient> UnityEngine.TextCore.Text.MaterialReferenceManager::m_ColorGradientReferenceLookup
	Dictionary_2_t3ECC0A0D579183A347F8AE00758620AA008C715A* ___m_ColorGradientReferenceLookup_4;
};

// UnityEngine.Yoga.MeasureOutput
struct MeasureOutput_t6C4FCF151309F81DF23561CF3FF1777445FBD84E  : public RuntimeObject
{
};

// UnityEngine.Yoga.Native
struct Native_t97ADC11284398663A27E9214C13A84F868A25614  : public RuntimeObject
{
};

// UnityEngineInternal.Input.NativeInputSystem
struct NativeInputSystem_tCFE5554EBC0D3EE1DAD80FC55CE0DE38A3DDC5EE  : public RuntimeObject
{
};

// UnityEngine.Timeline.NotificationUtilities
struct NotificationUtilities_t9B189DA39E0036BAC0636501BB2C82CF7F20C3CD  : public RuntimeObject
{
};

// UnityEngine.Physics
struct Physics_t1244C2983AEAFA149425AFFC3DF53BC91C18ED56  : public RuntimeObject
{
};

// UnityEngine.Physics2D
struct Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D  : public RuntimeObject
{
};

// UnityEngine.Playables.PlayableBehaviour
struct PlayableBehaviour_tCCFE023F2CAB1769A3EAB176BD5B0416C54C22E2  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.PluginsManager
struct PluginsManager_tB3018BC34957F354DA1466C4B00F76EF354BBFA7  : public RuntimeObject
{
};

// UnityEngine.RectTransformUtility
struct RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244  : public RuntimeObject
{
};

// UnityEngine.RemoteConfigSettingsHelper
struct RemoteConfigSettingsHelper_t29B2673892F8181388B45FFEEE354B3773629588  : public RuntimeObject
{
};

// UnityEngine.RemoteSettings
struct RemoteSettings_t9DFFC747AB3E7A39DF4527F245B529A407427250  : public RuntimeObject
{
};

// UnityEngine.Timeline.RuntimeElement
struct RuntimeElement_tB6282BAA538AFD75E138BED88A14DFA2665737B2  : public RuntimeObject
{
	// System.Int32 UnityEngine.Timeline.RuntimeElement::<intervalBit>k__BackingField
	int32_t ___U3CintervalBitU3Ek__BackingField_0;
};

// SR
struct SR_tF79CE2856F7D3AF3AE5D28C5C52C986CAFBE262F  : public RuntimeObject
{
};

// UnityEngine.ScrollViewState
struct ScrollViewState_t004FCCBFB6795BD76582385D6D308D8F9ECF41B6  : public RuntimeObject
{
};

// UnityEngine.Rendering.Universal.ShaderInput
struct ShaderInput_t048F7464F457883E25851701A5BBF15ADDDABAA3  : public RuntimeObject
{
};

// Unity.Burst.SharedStatic
struct SharedStatic_t83F4045688B6DB97142DC2BCAE88140D165FFE35  : public RuntimeObject
{
};

// DG.Tweening.ShortcutExtensions
struct ShortcutExtensions_t673F7F210C73AF07B020A2F8D6C360C96C470B1A  : public RuntimeObject
{
};

// UnityEngine.SliderState
struct SliderState_t7BBFAEF918BAA1EE6116C3979993E4EC7DC54FC8  : public RuntimeObject
{
};

// DG.Tweening.Plugins.Core.SpecialPluginsUtils
struct SpecialPluginsUtils_t3B0BE28310DF18FF4349B72BEF68F3A4AB3EC952  : public RuntimeObject
{
};

// DG.Tweening.Plugins.StringPluginExtensions
struct StringPluginExtensions_tA36E0339AE94CFDB282B13B80AD5E10EDB969B8F  : public RuntimeObject
{
};

// System.Linq.Expressions.Strings
struct Strings_t5E2898117DA2D8EC4D672719556FD56C9A4C6D6B  : public RuntimeObject
{
};

// UnityEngine.Subsystem
struct Subsystem_t5E67EE95D848FB950AD5D76325BF8959A6F7C7D7  : public RuntimeObject
{
};

// UnityEngine.SubsystemDescriptor
struct SubsystemDescriptor_tF417D2751C69A8B0DD86162EBCE55F84D3493A71  : public RuntimeObject
{
	// System.String UnityEngine.SubsystemDescriptor::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
};

// UnityEngine.SubsystemDescriptorBindings
struct SubsystemDescriptorBindings_t12C0380442BAE5AD9760662561CAD0AE7B41FFFE  : public RuntimeObject
{
};

// UnityEngine.SubsystemsImplementation.SubsystemDescriptorStore
struct SubsystemDescriptorStore_tEF3761B84B8C25EA4B93F94A487551820B268250  : public RuntimeObject
{
};

// UnityEngine.SubsystemsImplementation.SubsystemDescriptorWithProvider
struct SubsystemDescriptorWithProvider_t2A61A2C951A4A179E898CF207726BF6B5AF474D5  : public RuntimeObject
{
	// System.String UnityEngine.SubsystemsImplementation.SubsystemDescriptorWithProvider::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
};

// UnityEngine.SubsystemManager
struct SubsystemManager_t9A7261E4D0B53B996F04B8707D8E1C33AB65E824  : public RuntimeObject
{
};

// UnityEngine.SubsystemsImplementation.SubsystemWithProvider
struct SubsystemWithProvider_tC72E35EE2D413A4B0635B058154BABF265F31242  : public RuntimeObject
{
};

// UnityEngine.TextCore.Text.TextElement
struct TextElement_tCEF567A8810788262275B39DC39CBA6EBE7472DA  : public RuntimeObject
{
	// UnityEngine.TextCore.Text.TextElementType UnityEngine.TextCore.Text.TextElement::m_ElementType
	uint8_t ___m_ElementType_0;
	// System.UInt32 UnityEngine.TextCore.Text.TextElement::m_Unicode
	uint32_t ___m_Unicode_1;
	// UnityEngine.TextCore.Text.TextAsset UnityEngine.TextCore.Text.TextElement::m_TextAsset
	TextAsset_tB28F1843A877CCA74B89DC4F63EA532618B049B8* ___m_TextAsset_2;
	// UnityEngine.TextCore.Glyph UnityEngine.TextCore.Text.TextElement::m_Glyph
	Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F* ___m_Glyph_3;
	// System.UInt32 UnityEngine.TextCore.Text.TextElement::m_GlyphIndex
	uint32_t ___m_GlyphIndex_4;
	// System.Single UnityEngine.TextCore.Text.TextElement::m_Scale
	float ___m_Scale_5;
};

// UnityEngine.UIElements.TextNative
struct TextNative_t463AA48470CE96DB270F55A6F73EF2D90401C00C  : public RuntimeObject
{
};

// UnityEngine.TextCore.Text.TextResourceManager
struct TextResourceManager_t10194AE8FB3DE5B9938C916F107713F6BDD3CD88  : public RuntimeObject
{
};

// UnityEngine.TextCore.Text.TextShaderUtilities
struct TextShaderUtilities_t47B400695C5D96E7B04FEF9D132468B3A1799692  : public RuntimeObject
{
};

// UnityEngine.TextCore.Text.TextStyle
struct TextStyle_tD9287057EB15E73ED76AC925AC21A889D64CDAAE  : public RuntimeObject
{
	// System.String UnityEngine.TextCore.Text.TextStyle::m_Name
	String_t* ___m_Name_1;
	// System.Int32 UnityEngine.TextCore.Text.TextStyle::m_HashCode
	int32_t ___m_HashCode_2;
	// System.String UnityEngine.TextCore.Text.TextStyle::m_OpeningDefinition
	String_t* ___m_OpeningDefinition_3;
	// System.String UnityEngine.TextCore.Text.TextStyle::m_ClosingDefinition
	String_t* ___m_ClosingDefinition_4;
	// System.Int32[] UnityEngine.TextCore.Text.TextStyle::m_OpeningTagArray
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___m_OpeningTagArray_5;
	// System.Int32[] UnityEngine.TextCore.Text.TextStyle::m_ClosingTagArray
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___m_ClosingTagArray_6;
	// System.UInt32[] UnityEngine.TextCore.Text.TextStyle::m_OpeningTagUnicodeArray
	UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA* ___m_OpeningTagUnicodeArray_7;
	// System.UInt32[] UnityEngine.TextCore.Text.TextStyle::m_ClosingTagUnicodeArray
	UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA* ___m_ClosingTagUnicodeArray_8;
};

// UnityEngine.TextCore.Text.TextUtilities
struct TextUtilities_t2A52211BCD70DAC88C65B8FA51B350BECB96FD30  : public RuntimeObject
{
};

// UnityEngine.Timeline.TimeUtility
struct TimeUtility_t815D4EA395D12E4F662C6AB493F3CE5A0006AD26  : public RuntimeObject
{
};

// UnityEngine.Timeline.TimelineClip
struct TimelineClip_t003008F08E56A75F3A47FD9ADE7C066988A3371D  : public RuntimeObject
{
	// System.Int32 UnityEngine.Timeline.TimelineClip::m_Version
	int32_t ___m_Version_1;
	// System.Double UnityEngine.Timeline.TimelineClip::m_Start
	double ___m_Start_9;
	// System.Double UnityEngine.Timeline.TimelineClip::m_ClipIn
	double ___m_ClipIn_10;
	// UnityEngine.Object UnityEngine.Timeline.TimelineClip::m_Asset
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___m_Asset_11;
	// System.Double UnityEngine.Timeline.TimelineClip::m_Duration
	double ___m_Duration_12;
	// System.Double UnityEngine.Timeline.TimelineClip::m_TimeScale
	double ___m_TimeScale_13;
	// UnityEngine.Timeline.TrackAsset UnityEngine.Timeline.TimelineClip::m_ParentTrack
	TrackAsset_t31E19BE900C90F6616C0D337652C8614CD833B96* ___m_ParentTrack_14;
	// System.Double UnityEngine.Timeline.TimelineClip::m_EaseInDuration
	double ___m_EaseInDuration_15;
	// System.Double UnityEngine.Timeline.TimelineClip::m_EaseOutDuration
	double ___m_EaseOutDuration_16;
	// System.Double UnityEngine.Timeline.TimelineClip::m_BlendInDuration
	double ___m_BlendInDuration_17;
	// System.Double UnityEngine.Timeline.TimelineClip::m_BlendOutDuration
	double ___m_BlendOutDuration_18;
	// UnityEngine.AnimationCurve UnityEngine.Timeline.TimelineClip::m_MixInCurve
	AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* ___m_MixInCurve_19;
	// UnityEngine.AnimationCurve UnityEngine.Timeline.TimelineClip::m_MixOutCurve
	AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* ___m_MixOutCurve_20;
	// UnityEngine.Timeline.TimelineClip/BlendCurveMode UnityEngine.Timeline.TimelineClip::m_BlendInCurveMode
	int32_t ___m_BlendInCurveMode_21;
	// UnityEngine.Timeline.TimelineClip/BlendCurveMode UnityEngine.Timeline.TimelineClip::m_BlendOutCurveMode
	int32_t ___m_BlendOutCurveMode_22;
	// System.Collections.Generic.List`1<System.String> UnityEngine.Timeline.TimelineClip::m_ExposedParameterNames
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___m_ExposedParameterNames_23;
	// UnityEngine.AnimationClip UnityEngine.Timeline.TimelineClip::m_AnimationCurves
	AnimationClip_t00BD2F131D308A4AD2C6B0BF66644FC25FECE712* ___m_AnimationCurves_24;
	// System.Boolean UnityEngine.Timeline.TimelineClip::m_Recordable
	bool ___m_Recordable_25;
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.TimelineClip::m_PostExtrapolationMode
	int32_t ___m_PostExtrapolationMode_26;
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.TimelineClip::m_PreExtrapolationMode
	int32_t ___m_PreExtrapolationMode_27;
	// System.Double UnityEngine.Timeline.TimelineClip::m_PostExtrapolationTime
	double ___m_PostExtrapolationTime_28;
	// System.Double UnityEngine.Timeline.TimelineClip::m_PreExtrapolationTime
	double ___m_PreExtrapolationTime_29;
	// System.String UnityEngine.Timeline.TimelineClip::m_DisplayName
	String_t* ___m_DisplayName_30;
};

// UnityEngine.Timeline.TimelineClipCapsExtensions
struct TimelineClipCapsExtensions_tE7B52704E1D67E0F7DC021AA8AB9D9B84557A618  : public RuntimeObject
{
};

// UnityEngine.Timeline.TimelineCreateUtilities
struct TimelineCreateUtilities_tA54637927CEAAC7695569051E983F9062A303E0D  : public RuntimeObject
{
};

// UnityEngine.Timeline.TimelineUndo
struct TimelineUndo_tDC9CE9EF28D32B1EF1CE8D0A7D5948C637B78B61  : public RuntimeObject
{
};

// DG.Tweening.Core.TweenLink
struct TweenLink_t19BFBE652927BC2D2D5C39CE7F3AB1ECC7315CEE  : public RuntimeObject
{
	// UnityEngine.GameObject DG.Tweening.Core.TweenLink::target
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___target_0;
	// DG.Tweening.LinkBehaviour DG.Tweening.Core.TweenLink::behaviour
	int32_t ___behaviour_1;
	// System.Boolean DG.Tweening.Core.TweenLink::lastSeenActive
	bool ___lastSeenActive_2;
};

// DG.Tweening.Core.TweenManager
struct TweenManager_t3DFB74C661BA08421AF4EB9F62D6DB3B8F7351F1  : public RuntimeObject
{
};

// DG.Tweening.TweenParams
struct TweenParams_t0BF9382130164497B9A5A19076149D1CEDC697DA  : public RuntimeObject
{
	// System.Object DG.Tweening.TweenParams::id
	RuntimeObject* ___id_1;
	// System.String DG.Tweening.TweenParams::stringId
	String_t* ___stringId_2;
	// System.Int32 DG.Tweening.TweenParams::intId
	int32_t ___intId_3;
	// System.Object DG.Tweening.TweenParams::target
	RuntimeObject* ___target_4;
	// DG.Tweening.UpdateType DG.Tweening.TweenParams::updateType
	int32_t ___updateType_5;
	// System.Boolean DG.Tweening.TweenParams::isIndependentUpdate
	bool ___isIndependentUpdate_6;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onStart
	TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24* ___onStart_7;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onPlay
	TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24* ___onPlay_8;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onRewind
	TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24* ___onRewind_9;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onUpdate
	TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24* ___onUpdate_10;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onStepComplete
	TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24* ___onStepComplete_11;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onComplete
	TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24* ___onComplete_12;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onKill
	TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24* ___onKill_13;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.TweenParams::onWaypointChange
	TweenCallback_1_tF0ADCA0C226C9C243ACB55E67D852E4BB53AEB67* ___onWaypointChange_14;
	// System.Boolean DG.Tweening.TweenParams::isRecyclable
	bool ___isRecyclable_15;
	// System.Boolean DG.Tweening.TweenParams::isSpeedBased
	bool ___isSpeedBased_16;
	// System.Boolean DG.Tweening.TweenParams::autoKill
	bool ___autoKill_17;
	// System.Int32 DG.Tweening.TweenParams::loops
	int32_t ___loops_18;
	// DG.Tweening.LoopType DG.Tweening.TweenParams::loopType
	int32_t ___loopType_19;
	// System.Single DG.Tweening.TweenParams::delay
	float ___delay_20;
	// System.Boolean DG.Tweening.TweenParams::isRelative
	bool ___isRelative_21;
	// DG.Tweening.Ease DG.Tweening.TweenParams::easeType
	int32_t ___easeType_22;
	// DG.Tweening.EaseFunction DG.Tweening.TweenParams::customEase
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___customEase_23;
	// System.Single DG.Tweening.TweenParams::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_24;
	// System.Single DG.Tweening.TweenParams::easePeriod
	float ___easePeriod_25;
};

// DG.Tweening.TweenSettingsExtensions
struct TweenSettingsExtensions_t633F1D7D5B2C6D63B9331B5E0B63A14326F4F426  : public RuntimeObject
{
};

// UnityEngine.UIElements.UIElementsRuntimeUtilityNative
struct UIElementsRuntimeUtilityNative_t9DE2C23158D553BB693212D0D8AEAE8594E75938  : public RuntimeObject
{
};

// UnityEngine.UISystemProfilerApi
struct UISystemProfilerApi_t891AC4E16D3C12EAFD2748AE04F7A070F632396A  : public RuntimeObject
{
};

// UnityEngine.TextCore.Text.UnicodeLineBreakingRules
struct UnicodeLineBreakingRules_t80BE36F5E16AE48FE7B6DE1C91D36B1142B4EC0E  : public RuntimeObject
{
	// UnityEngine.TextAsset UnityEngine.TextCore.Text.UnicodeLineBreakingRules::m_UnicodeLineBreakingRules
	TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69* ___m_UnicodeLineBreakingRules_1;
	// UnityEngine.TextAsset UnityEngine.TextCore.Text.UnicodeLineBreakingRules::m_LeadingCharacters
	TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69* ___m_LeadingCharacters_2;
	// UnityEngine.TextAsset UnityEngine.TextCore.Text.UnicodeLineBreakingRules::m_FollowingCharacters
	TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69* ___m_FollowingCharacters_3;
	// System.Boolean UnityEngine.TextCore.Text.UnicodeLineBreakingRules::m_UseModernHangulLineBreakingRules
	bool ___m_UseModernHangulLineBreakingRules_4;
};

// UnityEngine.UnityString
struct UnityString_tEB81DAFE75C642A9472D9FEDA7C2EC19A7B672B6  : public RuntimeObject
{
};

// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t3EF35349E23201EF9F3C5956C44384FA45C1EF29  : public RuntimeObject
{
	// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::m_UserName
	String_t* ___m_UserName_0;
	// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::m_ID
	String_t* ___m_ID_1;
	// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::m_legacyID
	String_t* ___m_legacyID_2;
	// System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::m_IsFriend
	bool ___m_IsFriend_3;
	// UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::m_State
	int32_t ___m_State_4;
	// UnityEngine.Texture2D UnityEngine.SocialPlatforms.Impl.UserProfile::m_Image
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___m_Image_5;
	// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::m_gameID
	String_t* ___m_gameID_6;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// UnityEngine.WWWForm
struct WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045  : public RuntimeObject
{
};

// UnityEngine.WWWTranscoder
struct WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B  : public RuntimeObject
{
};

// UnityEngineInternal.WebRequestUtils
struct WebRequestUtils_t23F1FB533DBFDA3BE5624D901D535B4C6EFAD443  : public RuntimeObject
{
};

// UnityEngine.Timeline.WeightUtility
struct WeightUtility_tB748A8522E941F6EC55488967F280E34D4D4E410  : public RuntimeObject
{
};

// UnityEngine.XR.XRDevice
struct XRDevice_tD076A68EFE413B3EEEEA362BE0364A488B58F194  : public RuntimeObject
{
};

// UnityEngine.XR.XRSettings
struct XRSettings_t783533FF87B79D6D0C6A47FA8EC9B17EC0820D97  : public RuntimeObject
{
};

// System.Xml.XmlNode
struct XmlNode_t3180B9B3D5C36CD58F5327D9F13458E3B3F030AF  : public RuntimeObject
{
};

// System.Xml.XmlReader
struct XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD  : public RuntimeObject
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
};

// UnityEngine.Yoga.YogaConstants
struct YogaConstants_tE52AB48288567AEF285EDE0C8884AFD803AD9D3C  : public RuntimeObject
{
};

// System.__Il2CppComDelegate
struct __Il2CppComDelegate_tD0DD2BBA6AC8F151D32B6DFD02F6BDA339F8DC4D  : public Il2CppComObject
{
};

// Unity.Mathematics.math
struct math_t084A7021C0897F87DB209393BE7FBDFD6A7FBFA6  : public RuntimeObject
{
};

// UnityEngine.Animation/Enumerator
struct Enumerator_t81434F7D5603121F3D7BD6DB916FE1C755307530  : public RuntimeObject
{
	// UnityEngine.Animation UnityEngine.Animation/Enumerator::m_Outer
	Animation_t6593B06C39E3B139808B19F2C719C860F3F61040* ___m_Outer_0;
	// System.Int32 UnityEngine.Animation/Enumerator::m_CurrentIndex
	int32_t ___m_CurrentIndex_1;
};

// UnityEngine.Timeline.AnimationPlayableAsset/AnimationPlayableAssetUpgrade
struct AnimationPlayableAssetUpgrade_t03DC883418FF5A1A5EFF90307DE53090C17F94F7  : public RuntimeObject
{
};

// UnityEngine.Timeline.AnimationTrack/AnimationTrackUpgrade
struct AnimationTrackUpgrade_t7D42F92CD765334066B52C341F5EEF005BB800F0  : public RuntimeObject
{
};

// UnityEngine.AudioSettings/Mobile
struct Mobile_t304A73480DF447472BDB16BA19A9E4FE2C8CB2DD  : public RuntimeObject
{
};

// Unity.Burst.BurstCompiler/FakeDelegate
struct FakeDelegate_t659588AB379C77AF08088ED8B2E2A5ECFF38CE16  : public RuntimeObject
{
	// System.Reflection.MethodInfo Unity.Burst.BurstCompiler/FakeDelegate::<Method>k__BackingField
	MethodInfo_t* ___U3CMethodU3Ek__BackingField_0;
};

// CartoonFX.CFXR_Effect/AnimatedLight
struct AnimatedLight_t452FBE1EDE1B867DB67C7383966C9992DC6A9C59  : public RuntimeObject
{
	// UnityEngine.Light CartoonFX.CFXR_Effect/AnimatedLight::light
	Light_t1E68479B7782AF2050FAA02A5DC612FD034F18F3* ___light_1;
	// System.Boolean CartoonFX.CFXR_Effect/AnimatedLight::loop
	bool ___loop_2;
	// System.Boolean CartoonFX.CFXR_Effect/AnimatedLight::animateIntensity
	bool ___animateIntensity_3;
	// System.Single CartoonFX.CFXR_Effect/AnimatedLight::intensityStart
	float ___intensityStart_4;
	// System.Single CartoonFX.CFXR_Effect/AnimatedLight::intensityEnd
	float ___intensityEnd_5;
	// System.Single CartoonFX.CFXR_Effect/AnimatedLight::intensityDuration
	float ___intensityDuration_6;
	// UnityEngine.AnimationCurve CartoonFX.CFXR_Effect/AnimatedLight::intensityCurve
	AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* ___intensityCurve_7;
	// System.Boolean CartoonFX.CFXR_Effect/AnimatedLight::perlinIntensity
	bool ___perlinIntensity_8;
	// System.Single CartoonFX.CFXR_Effect/AnimatedLight::perlinIntensitySpeed
	float ___perlinIntensitySpeed_9;
	// System.Boolean CartoonFX.CFXR_Effect/AnimatedLight::fadeIn
	bool ___fadeIn_10;
	// System.Single CartoonFX.CFXR_Effect/AnimatedLight::fadeInDuration
	float ___fadeInDuration_11;
	// System.Boolean CartoonFX.CFXR_Effect/AnimatedLight::fadeOut
	bool ___fadeOut_12;
	// System.Single CartoonFX.CFXR_Effect/AnimatedLight::fadeOutDuration
	float ___fadeOutDuration_13;
	// System.Boolean CartoonFX.CFXR_Effect/AnimatedLight::animateRange
	bool ___animateRange_14;
	// System.Single CartoonFX.CFXR_Effect/AnimatedLight::rangeStart
	float ___rangeStart_15;
	// System.Single CartoonFX.CFXR_Effect/AnimatedLight::rangeEnd
	float ___rangeEnd_16;
	// System.Single CartoonFX.CFXR_Effect/AnimatedLight::rangeDuration
	float ___rangeDuration_17;
	// UnityEngine.AnimationCurve CartoonFX.CFXR_Effect/AnimatedLight::rangeCurve
	AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* ___rangeCurve_18;
	// System.Boolean CartoonFX.CFXR_Effect/AnimatedLight::perlinRange
	bool ___perlinRange_19;
	// System.Single CartoonFX.CFXR_Effect/AnimatedLight::perlinRangeSpeed
	float ___perlinRangeSpeed_20;
	// System.Boolean CartoonFX.CFXR_Effect/AnimatedLight::animateColor
	bool ___animateColor_21;
	// UnityEngine.Gradient CartoonFX.CFXR_Effect/AnimatedLight::colorGradient
	Gradient_tA7FEBE2FDB4929FFF6C997134841046F713DAC1E* ___colorGradient_22;
	// System.Single CartoonFX.CFXR_Effect/AnimatedLight::colorDuration
	float ___colorDuration_23;
	// UnityEngine.AnimationCurve CartoonFX.CFXR_Effect/AnimatedLight::colorCurve
	AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* ___colorCurve_24;
	// System.Boolean CartoonFX.CFXR_Effect/AnimatedLight::perlinColor
	bool ___perlinColor_25;
	// System.Single CartoonFX.CFXR_Effect/AnimatedLight::perlinColorSpeed
	float ___perlinColorSpeed_26;
};

// CartoonFX.CFXR_ParticleTextFontAsset/Kerning
struct Kerning_t5359477AFC231ACCA7CB4DAE0513897AABCA7500  : public RuntimeObject
{
	// System.String CartoonFX.CFXR_ParticleTextFontAsset/Kerning::name
	String_t* ___name_0;
	// System.Single CartoonFX.CFXR_ParticleTextFontAsset/Kerning::pre
	float ___pre_1;
	// System.Single CartoonFX.CFXR_ParticleTextFontAsset/Kerning::post
	float ___post_2;
};

// DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__17
struct U3CWaitForCompletionU3Ed__17_tC4B4474C8297E45D0D46677FD6B631561BB82815  : public RuntimeObject
{
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__17::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__17::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
};

// DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__20
struct U3CWaitForElapsedLoopsU3Ed__20_tC6EDF6D2101E0B45832ABB43A0A631C2A9417351  : public RuntimeObject
{
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__20::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__20::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__20::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__20::elapsedLoops
	int32_t ___elapsedLoops_3;
};

// DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__19
struct U3CWaitForKillU3Ed__19_tCD266A012639DB20A94A837CBCC7542219D3D041  : public RuntimeObject
{
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__19::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__19::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
};

// DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__21
struct U3CWaitForPositionU3Ed__21_t278402B3AE14CDA519C1B7F48D327F9D22FA3337  : public RuntimeObject
{
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__21::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__21::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
	// System.Single DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__21::position
	float ___position_3;
};

// DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__18
struct U3CWaitForRewindU3Ed__18_t55F75B6C49D9C6AA69B9620D932C482C2070A4C7  : public RuntimeObject
{
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__18::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__18::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
};

// DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__22
struct U3CWaitForStartU3Ed__22_t87727B5B55905440293FA75A84473AD1A582252D  : public RuntimeObject
{
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__22::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__22::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__22::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
};

// DG.Tweening.DOTweenModuleAudio/cy
struct cy_t1F5EB8CC9D026AD38C320A7D19A1E07CDCE8F4C8  : public RuntimeObject
{
	// UnityEngine.AudioSource DG.Tweening.DOTweenModuleAudio/cy::target
	AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* ___target_0;
};

// DG.Tweening.DOTweenModuleAudio/cz
struct cz_t85A5FCD7FAA576091E8B4BFBD57625052E1B917D  : public RuntimeObject
{
	// UnityEngine.AudioSource DG.Tweening.DOTweenModuleAudio/cz::target
	AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* ___target_0;
};

// DG.Tweening.DOTweenModuleAudio/da
struct da_t3FC532477FC26B8C8B109F722ED5DD96CA6B97A4  : public RuntimeObject
{
	// UnityEngine.Audio.AudioMixer DG.Tweening.DOTweenModuleAudio/da::target
	AudioMixer_tE2E8D79241711CDF9AB428C7FB96A35D80E40B04* ___target_0;
	// System.String DG.Tweening.DOTweenModuleAudio/da::floatName
	String_t* ___floatName_1;
};

// DG.Tweening.DOTweenModulePhysics/db
struct db_t04FA8C1C83849880285627B0AEB66AA6254AC896  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/db::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics/dc
struct dc_t8560375365CB794B1B1EB7D8112D328CB79166C1  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/dc::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics/dd
struct dd_tC04F9BEFD21B87A979BB1BF151883FC26DBA2079  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/dd::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics/de
struct de_t1957E3969214286A4AAF6BD98BF61899A2EFAEE7  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/de::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics/df
struct df_tDE0CD18A0ACFB761CF7EBB92C73EE6CCC8C46E03  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/df::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics/dg
struct dg_t74AFC493747D3773884E56C4134EFBB4395889D2  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/dg::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics/di
struct di_t14426C7A9EA6883BB2AB39FBA5D1FD29A09B0D34  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/di::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics/dj
struct dj_tD6C8ED0CAB3386298320D2B1EB661292B31BA050  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics/dj::trans
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___trans_0;
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/dj::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_1;
};

// DG.Tweening.DOTweenModulePhysics/dk
struct dk_t91215D2D2D582240CE8B8C76634C7F17546E9F8C  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/dk::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics/dl
struct dl_t006ECC3270CE6AAA83F3E5944E2CEBAF7DD6C029  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics/dl::trans
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___trans_0;
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/dl::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_1;
};

// DG.Tweening.DOTweenModulePhysics2D/dm
struct dm_t0EE8E4DE2AF72F874B566797C004695394DBD262  : public RuntimeObject
{
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/dm::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics2D/dn
struct dn_tEF51A22D307B0EE60DE9E282A8BF4C2D65CC2936  : public RuntimeObject
{
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/dn::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics2D/do
struct do_t76311BB32294D3739B4D88A6C988945DC5E8E1D5  : public RuntimeObject
{
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/do::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics2D/dp
struct dp_tCCE1A32DE0623F9BAB888D6174CC9CCE901E4E1B  : public RuntimeObject
{
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/dp::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics2D/dr
struct dr_t08A6E7547D64506AB0E3E9640BEAAC9BA0412603  : public RuntimeObject
{
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/dr::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics2D/ds
struct ds_t5AF5C01D4BFDAACE64367BAD31E184B019D352AE  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics2D/ds::trans
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___trans_0;
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/ds::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_1;
};

// DG.Tweening.DOTweenModulePhysics2D/dt
struct dt_tBC8E4BE4DB21DACC0A81470D7B1CFA10561D6708  : public RuntimeObject
{
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/dt::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_0;
};

// DG.Tweening.DOTweenModulePhysics2D/du
struct du_t9C8F1B18CE5A2B107C056E597C78AA21A5434FB2  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics2D/du::trans
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___trans_0;
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/du::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_1;
};

// DG.Tweening.DOTweenModuleSprite/dv
struct dv_t9E694C9B76B01D9846D951BE9A564F3E8F6E1516  : public RuntimeObject
{
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite/dv::target
	SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* ___target_0;
};

// DG.Tweening.DOTweenModuleSprite/dw
struct dw_tC4D9A5FEC7AD046EF807C3556A574E383CEB910A  : public RuntimeObject
{
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite/dw::target
	SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/Utils
struct Utils_t3635A7E5B7FAC073A571339320CB18B71E25ABD8  : public RuntimeObject
{
};

// DG.Tweening.DOTweenModuleUI/dy
struct dy_t1B3448FA1815934EFB53E0016E8DEC994A5A3F43  : public RuntimeObject
{
	// UnityEngine.CanvasGroup DG.Tweening.DOTweenModuleUI/dy::target
	CanvasGroup_t048C1461B14628CFAEBE6E7353093ADB04EBC094* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/dz
struct dz_tCD2E9ADEA68AAAE78D012D85BAF1E26DBCE1C013  : public RuntimeObject
{
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI/dz::target
	Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/ea
struct ea_t6AF4F7D4D540E9E527BBEF98DE636F94FDFE4EF5  : public RuntimeObject
{
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI/ea::target
	Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/eb
struct eb_t498E9E971601541D493F2B143425AA1F3698C0E6  : public RuntimeObject
{
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/eb::target
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/ec
struct ec_t89E50EEB2476BAAAFBE7F85CA814453569BAEBAD  : public RuntimeObject
{
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/ec::target
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/ed
struct ed_t9B95C38962839D948422357292F158321AB448F7  : public RuntimeObject
{
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/ed::target
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/ee
struct ee_tA8F87539F6BACC3848548656974C06447BF67223  : public RuntimeObject
{
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI/ee::target
	LayoutElement_tB1F24CC11AF4AA87015C8D8EE06D22349C5BF40A* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/ef
struct ef_t4E8CB78C298D36AADD1937E072FC6EE278481E63  : public RuntimeObject
{
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI/ef::target
	LayoutElement_tB1F24CC11AF4AA87015C8D8EE06D22349C5BF40A* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/eg
struct eg_t65BC07AE5FEB82B0D5A6EE2E611C09EAFD4ED670  : public RuntimeObject
{
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI/eg::target
	LayoutElement_tB1F24CC11AF4AA87015C8D8EE06D22349C5BF40A* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/eh
struct eh_t4FF1C1056500F999D804DD2BEF86B3031CF3C99C  : public RuntimeObject
{
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI/eh::target
	Outline_t9CF146E077DC65F441EDEC463AA6710374108084* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/ei
struct ei_t919D83BA42C49173A18766EA39B67C3475E87882  : public RuntimeObject
{
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI/ei::target
	Outline_t9CF146E077DC65F441EDEC463AA6710374108084* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/ej
struct ej_t18DAE8A30B51C3D34DB0D5964798929EB157C69E  : public RuntimeObject
{
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI/ej::target
	Outline_t9CF146E077DC65F441EDEC463AA6710374108084* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/ek
struct ek_tC37AA180EDF0809193B38B3D0F3F18D34218D826  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/ek::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/el
struct el_t0758C333352A74222C7B402D194635113221F29A  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/el::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/em
struct em_t91CA10C9D401DCC0190BF122CA9F8EEF4226062A  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/em::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/en
struct en_tCA269384E666E2437B547C86A2E0970E16308DD0  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/en::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/eo
struct eo_t8875602888E0CF53F1EED6B2E926D1FCE27DE675  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/eo::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/ep
struct ep_t7791CE01C7D9FA2C28622B377FF67F39E0519164  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/ep::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/eq
struct eq_tE0C83491E29F03693C9EC2186D11F157085F7937  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/eq::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/er
struct er_tDDD5288BB2DE0469770BBCB2F90A03BCCB8ADD57  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/er::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/es
struct es_t90FDC1C658DF920F5BFD9CE46FE88B1D53C01A24  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/es::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/et
struct et_tD92CD2DF5BE38E15AE6FDC4755C577DC0CBE8CED  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/et::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/eu
struct eu_t4FC045D2551FCCAD17DEC212F2674FFD75A8897A  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/eu::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/ev
struct ev_tD9C81DAA29A760A09241CA2E3C38AFA2C06525A8  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/ev::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/ew
struct ew_t18930CB32FD7F8692597DCECD783AC3E84AD8261  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/ew::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/ex
struct ex_tC12B7079FD9F7105096B33C00E1CF128475F8E9E  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/ex::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/ey
struct ey_tC4291BD4B8DCE22D43E92D20815C5DA73742AA6D  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/ey::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/ez
struct ez_t0A5814C37E1C2B25CD984443BFDB76E68575C728  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/ez::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/fb
struct fb_tB4BE092DF664B4D9040B9317EF21AA57490F0BD5  : public RuntimeObject
{
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI/fb::target
	ScrollRect_t17D2F2939CA8953110180DF53164CFC3DC88D70E* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/fc
struct fc_tD2FB882F61BF34D867EB605780126FD5C2F22F0B  : public RuntimeObject
{
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI/fc::target
	ScrollRect_t17D2F2939CA8953110180DF53164CFC3DC88D70E* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/fd
struct fd_tDA5E47505A25D3A62E1B713157CF0F03EBE72B89  : public RuntimeObject
{
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI/fd::target
	ScrollRect_t17D2F2939CA8953110180DF53164CFC3DC88D70E* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/fe
struct fe_tC5A76D8DF980C583FCD405FFBE38B890744CBB17  : public RuntimeObject
{
	// UnityEngine.UI.Slider DG.Tweening.DOTweenModuleUI/fe::target
	Slider_t87EA570E3D6556CABF57456C2F3873FFD86E652F* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/ff
struct ff_tA8CB63F594A70D0B4F555E8527ACEDED35E01430  : public RuntimeObject
{
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/ff::target
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/fg
struct fg_tC31619D49CF72755A4AA12ABACF4DAE7FE5842FB  : public RuntimeObject
{
	// System.Int32 DG.Tweening.DOTweenModuleUI/fg::v
	int32_t ___v_0;
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/fg::target
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___target_1;
	// System.Boolean DG.Tweening.DOTweenModuleUI/fg::addThousandsSeparator
	bool ___addThousandsSeparator_2;
	// System.Globalization.CultureInfo DG.Tweening.DOTweenModuleUI/fg::cInfo
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___cInfo_3;
};

// DG.Tweening.DOTweenModuleUI/fh
struct fh_tB10B99DBBF95F5138B3D2270FE4365F8119776B6  : public RuntimeObject
{
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/fh::target
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/fi
struct fi_t649FEB4657A71F350E6BF0A93C5A6C934D1A9CE5  : public RuntimeObject
{
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/fi::target
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___target_0;
};

// DG.Tweening.DOTweenModuleUI/fm
struct fm_t51C9D72D64BCC8261AC65B4321A2D38C142CC78A  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/fm::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
};

// DG.Tweening.DOTweenModuleUnityVersion/fn
struct fn_tF788F79558E8A188448411AA4B2DCD839531E4B0  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.DOTweenModuleUnityVersion/fn::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/fn::propertyID
	int32_t ___propertyID_1;
};

// DG.Tweening.DOTweenModuleUnityVersion/fo
struct fo_t3C207075334909DF25777631046D3574EB73D523  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.DOTweenModuleUnityVersion/fo::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/fo::propertyID
	int32_t ___propertyID_1;
};

// DG.Tweening.DOTweenModuleUtils/Physics
struct Physics_t835AE7329FBFA61BA3A2271950FD3F7F6A18770A  : public RuntimeObject
{
};

// DG.Tweening.Core.DOTweenSettings/ModulesSetup
struct ModulesSetup_t888D98957DAB1E84C63C5F866C838A0E714F7236  : public RuntimeObject
{
	// System.Boolean DG.Tweening.Core.DOTweenSettings/ModulesSetup::showPanel
	bool ___showPanel_0;
	// System.Boolean DG.Tweening.Core.DOTweenSettings/ModulesSetup::audioEnabled
	bool ___audioEnabled_1;
	// System.Boolean DG.Tweening.Core.DOTweenSettings/ModulesSetup::physicsEnabled
	bool ___physicsEnabled_2;
	// System.Boolean DG.Tweening.Core.DOTweenSettings/ModulesSetup::physics2DEnabled
	bool ___physics2DEnabled_3;
	// System.Boolean DG.Tweening.Core.DOTweenSettings/ModulesSetup::spriteEnabled
	bool ___spriteEnabled_4;
	// System.Boolean DG.Tweening.Core.DOTweenSettings/ModulesSetup::uiEnabled
	bool ___uiEnabled_5;
	// System.Boolean DG.Tweening.Core.DOTweenSettings/ModulesSetup::textMeshProEnabled
	bool ___textMeshProEnabled_6;
	// System.Boolean DG.Tweening.Core.DOTweenSettings/ModulesSetup::tk2DEnabled
	bool ___tk2DEnabled_7;
	// System.Boolean DG.Tweening.Core.DOTweenSettings/ModulesSetup::deAudioEnabled
	bool ___deAudioEnabled_8;
	// System.Boolean DG.Tweening.Core.DOTweenSettings/ModulesSetup::deUnityExtendedEnabled
	bool ___deUnityExtendedEnabled_9;
	// System.Boolean DG.Tweening.Core.DOTweenSettings/ModulesSetup::epoOutlineEnabled
	bool ___epoOutlineEnabled_10;
};

// DG.Tweening.Core.DOTweenSettings/SafeModeOptions
struct SafeModeOptions_tA3C410AFB34C85C70704176B558984B1BA8AD880  : public RuntimeObject
{
	// DG.Tweening.Core.Enums.SafeModeLogBehaviour DG.Tweening.Core.DOTweenSettings/SafeModeOptions::logBehaviour
	int32_t ___logBehaviour_0;
	// DG.Tweening.Core.Enums.NestedTweenFailureBehaviour DG.Tweening.Core.DOTweenSettings/SafeModeOptions::nestedTweenFailureBehaviour
	int32_t ___nestedTweenFailureBehaviour_1;
};

// DG.Tweening.Core.Debugger/Sequence
struct Sequence_t084DF9B3D58AA78E1F4F77949DD157CA9E3CAD02  : public RuntimeObject
{
};

// DG.Tweening.Core.Easing.EaseManager/<>c
struct U3CU3Ec_t124AFA44553F835AFE37BF9E66B435E4CA7DB8B4  : public RuntimeObject
{
};

// System.Linq.Expressions.Expression/ExtensionInfo
struct ExtensionInfo_tCE0B03041E5780D311D03EACAA1F672E4C70C3E7  : public RuntimeObject
{
	// System.Linq.Expressions.ExpressionType System.Linq.Expressions.Expression/ExtensionInfo::NodeType
	int32_t ___NodeType_0;
};

// System.Linq.Expressions.Expression/LambdaExpressionProxy
struct LambdaExpressionProxy_t19CC47CCDC0090FFF30367A402CD6127C8A580DE  : public RuntimeObject
{
};

// System.Linq.Expressions.Expression/MemberExpressionProxy
struct MemberExpressionProxy_t6B52A63F5D0C957F9B833F873B05E89527B49484  : public RuntimeObject
{
};

// System.Linq.Expressions.Expression/UnaryExpressionProxy
struct UnaryExpressionProxy_tA78B15C1B517EF474E3F9AE481010961D2919D23  : public RuntimeObject
{
};

// UnityEngine.Timeline.Extrapolation/<>c
struct U3CU3Ec_tB74E998116FFDE2919C1EF6E8B35AB3798461A28  : public RuntimeObject
{
};

// UnityEngine.TextCore.Text.FontAsset/<>c
struct U3CU3Ec_tE3434A696CF5060D63A69C93A84379DBF90E9948  : public RuntimeObject
{
};

// UnityEngine.TextCore.Text.FontFeatureTable/<>c
struct U3CU3Ec_tCA6A4D073378D45745D0E81D226E721969C3BE80  : public RuntimeObject
{
};

// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_tF844B2FAD6933B78FD5EFEBDE0529BCBAC19BA60  : public RuntimeObject
{
	// System.Int32 UnityEngine.GUILayoutUtility/LayoutCache::<id>k__BackingField
	int32_t ___U3CidU3Ek__BackingField_0;
	// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility/LayoutCache::topLevel
	GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D* ___topLevel_1;
	// UnityEngineInternal.GenericStack UnityEngine.GUILayoutUtility/LayoutCache::layoutGroups
	GenericStack_t1FB49AB7D847C97ABAA97AB232CA416CABD24C49* ___layoutGroups_2;
	// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility/LayoutCache::windows
	GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D* ___windows_3;
};

// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform/<>c__DisplayClass21_0
struct U3CU3Ec__DisplayClass21_0_t9AE48FD34643B3E83D69BD45882795E81C13E0A1  : public RuntimeObject
{
	// System.Action`1<System.Boolean> UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform/<>c__DisplayClass21_0::callback
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* ___callback_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass11_0
struct U3CU3Ec__DisplayClass11_0_t4CD563957CF59D29FF34DCB41ACF32E7E6C56B4D  : public RuntimeObject
{
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass11_0::target
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass12_0
struct U3CU3Ec__DisplayClass12_0_t760D335F2D2DF176146C98FA8EA2BF2383C78C2A  : public RuntimeObject
{
	// UnityEngine.Light DG.Tweening.ShortcutExtensions/<>c__DisplayClass12_0::target
	Light_t1E68479B7782AF2050FAA02A5DC612FD034F18F3* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass13_0
struct U3CU3Ec__DisplayClass13_0_t494528D70729B93DB7D601F3D47E3C71DEAC39E2  : public RuntimeObject
{
	// UnityEngine.Light DG.Tweening.ShortcutExtensions/<>c__DisplayClass13_0::target
	Light_t1E68479B7782AF2050FAA02A5DC612FD034F18F3* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass14_0
struct U3CU3Ec__DisplayClass14_0_tE6E24BB0E6CAFBB2DFDE32E8CDD1FE9BB244B067  : public RuntimeObject
{
	// UnityEngine.Light DG.Tweening.ShortcutExtensions/<>c__DisplayClass14_0::target
	Light_t1E68479B7782AF2050FAA02A5DC612FD034F18F3* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass16_0
struct U3CU3Ec__DisplayClass16_0_t9217B09EF7ABF88C8F99653F97FCB2F6E0772ED4  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass16_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass17_0
struct U3CU3Ec__DisplayClass17_0_t70C4222F705AACA1CB3A9601C9A33FA726E42C4A  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass17_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
	// System.String DG.Tweening.ShortcutExtensions/<>c__DisplayClass17_0::property
	String_t* ___property_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass18_0
struct U3CU3Ec__DisplayClass18_0_t1BFF276984245961583498AE53143E78820EA151  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass18_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
	// System.Int32 DG.Tweening.ShortcutExtensions/<>c__DisplayClass18_0::propertyID
	int32_t ___propertyID_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass19_0
struct U3CU3Ec__DisplayClass19_0_t9CD6F9D8EBF541A8BE42A4A3EA84533FA311AA88  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass19_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass20_0
struct U3CU3Ec__DisplayClass20_0_t9421CA0B6EB7CDFCF453B9E22D9BE4D3918441D6  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass20_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
	// System.String DG.Tweening.ShortcutExtensions/<>c__DisplayClass20_0::property
	String_t* ___property_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass21_0
struct U3CU3Ec__DisplayClass21_0_tDE07ED974C04029C53C41D2EE832E0FE4FBF980C  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass21_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
	// System.Int32 DG.Tweening.ShortcutExtensions/<>c__DisplayClass21_0::propertyID
	int32_t ___propertyID_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass22_0
struct U3CU3Ec__DisplayClass22_0_tEE82B8B9BC498710AEE99211D2D5BAC8B8F309E8  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass22_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
	// System.String DG.Tweening.ShortcutExtensions/<>c__DisplayClass22_0::property
	String_t* ___property_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass23_0
struct U3CU3Ec__DisplayClass23_0_t6A4C1EA019470975DCF72E56D5743016A84CDAA5  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass23_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
	// System.Int32 DG.Tweening.ShortcutExtensions/<>c__DisplayClass23_0::propertyID
	int32_t ___propertyID_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass24_0
struct U3CU3Ec__DisplayClass24_0_t4849CCC37129ED28E5DF2BE13A9A1706DE9E8CB8  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass24_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass25_0
struct U3CU3Ec__DisplayClass25_0_t7B41616A17E6850A9081163473AE90592E4F83D5  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass25_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
	// System.String DG.Tweening.ShortcutExtensions/<>c__DisplayClass25_0::property
	String_t* ___property_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass26_0
struct U3CU3Ec__DisplayClass26_0_t129A7FC4A0B467E5E0984FB0FE76F5585A87CC06  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass26_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass27_0
struct U3CU3Ec__DisplayClass27_0_t2F9F7E733BD3F950E7C2EC3025D006C9BCE148E2  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass27_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
	// System.String DG.Tweening.ShortcutExtensions/<>c__DisplayClass27_0::property
	String_t* ___property_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass28_0
struct U3CU3Ec__DisplayClass28_0_tDCD0401BBCE832C8F66F464E01377A8E110A385F  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass28_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
	// System.String DG.Tweening.ShortcutExtensions/<>c__DisplayClass28_0::property
	String_t* ___property_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass29_0
struct U3CU3Ec__DisplayClass29_0_tD7C964849E2B7EEC73ED948CF7170E4D28FB0F50  : public RuntimeObject
{
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass29_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_0;
	// System.Int32 DG.Tweening.ShortcutExtensions/<>c__DisplayClass29_0::propertyID
	int32_t ___propertyID_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass30_0
struct U3CU3Ec__DisplayClass30_0_t83315C238D477F94424D41BF191800DB803D011F  : public RuntimeObject
{
	// UnityEngine.TrailRenderer DG.Tweening.ShortcutExtensions/<>c__DisplayClass30_0::target
	TrailRenderer_tF7E185EF383CF4300DFC3E1DE59FA6100B93CA39* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass31_0
struct U3CU3Ec__DisplayClass31_0_t81F2950D9BCBF314ADFDCB2FA5B271713E0842F4  : public RuntimeObject
{
	// UnityEngine.TrailRenderer DG.Tweening.ShortcutExtensions/<>c__DisplayClass31_0::target
	TrailRenderer_tF7E185EF383CF4300DFC3E1DE59FA6100B93CA39* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass32_0
struct U3CU3Ec__DisplayClass32_0_t8DA65445AF764D116F09828EC2F6DBF80A63E879  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass32_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass33_0
struct U3CU3Ec__DisplayClass33_0_t5CD3D4AA9D130B81CF111DEF984D6A9840A20BA4  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass33_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass34_0
struct U3CU3Ec__DisplayClass34_0_t057A7B0C01B1A49D0E916492D42C5FB2344E78CC  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass34_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass35_0
struct U3CU3Ec__DisplayClass35_0_tBA3B73DAB15E09DFF5EFE1ECD34340A1D0D659A3  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass35_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass36_0
struct U3CU3Ec__DisplayClass36_0_t44A8B5E8AF1C9E0594DC5D085B18E274188AB497  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass36_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass37_0
struct U3CU3Ec__DisplayClass37_0_tF05A09D7E18C84C1EADFDADCCC1A63DEF495DE84  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass37_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass38_0
struct U3CU3Ec__DisplayClass38_0_t1EC88301378528B419721DCE1B10D9EE3D91094B  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass38_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass39_0
struct U3CU3Ec__DisplayClass39_0_t56A4FBF5B980B30B8E41C437AE6D105DE2B1A370  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass39_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass40_0
struct U3CU3Ec__DisplayClass40_0_tF6F5F6C34FB0526FD16534810E499BCE4E40F7B6  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass40_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass41_0
struct U3CU3Ec__DisplayClass41_0_t1FAF26807ABB609E6A8D218B75F41954FC6136F4  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass41_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass42_0
struct U3CU3Ec__DisplayClass42_0_tBD69DF3FE195AEBF1AB253930C48AA48497C09CD  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass42_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass43_0
struct U3CU3Ec__DisplayClass43_0_tB3BA33D79526265D7D313B58B739311DF8AE3AED  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass43_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass44_0
struct U3CU3Ec__DisplayClass44_0_t2C0E5B7393A966223081AB8116B88BBF6F883D97  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass44_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass45_0
struct U3CU3Ec__DisplayClass45_0_t5F584F3B77663C369DC9227301F305414AFF12A9  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass45_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass46_0
struct U3CU3Ec__DisplayClass46_0_t948EE002DF5B4A1E595F7676DAA65C87F9D2BDFD  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass46_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass47_0
struct U3CU3Ec__DisplayClass47_0_t8B75D0151AE83F371BCB74DD8B9FA4EFD7319F47  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass47_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass48_0
struct U3CU3Ec__DisplayClass48_0_tE3DCC90A5A9C02495A4A3EC7B0645CF014851AEC  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass48_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass51_0
struct U3CU3Ec__DisplayClass51_0_tAC1502125C96CAA86E45C54D8389C1ECD8206A06  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass51_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass52_0
struct U3CU3Ec__DisplayClass52_0_tAB2245D9274F656343DA107BC28107F74438862A  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass52_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass53_0
struct U3CU3Ec__DisplayClass53_0_t44A1404C6DAC368C0CDDE88D4A061E65A58748F3  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass53_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass54_0
struct U3CU3Ec__DisplayClass54_0_tD6DF8B814EBA95239B92C195AE986EA5FE25B408  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass54_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass55_0
struct U3CU3Ec__DisplayClass55_0_t1D8D264CC436BF750C2CF019FD62CB19CDA71FF4  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass55_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass56_0
struct U3CU3Ec__DisplayClass56_0_t50FE7F2A56F77E22BFCCFCB376F7C0FB46745CA5  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass56_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass57_0
struct U3CU3Ec__DisplayClass57_0_tF946A93343ACD693F2BCAD0CB38445C844B28748  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass57_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass58_0
struct U3CU3Ec__DisplayClass58_0_t5CF14C7C14CD8658948E8DCD5488900127C1287C  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass58_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass59_0
struct U3CU3Ec__DisplayClass59_0_tB90A49433476CF808EEF96ED606D607868A1D80F  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass59_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0
struct U3CU3Ec__DisplayClass60_0_t17FC0A48346790190DEA104ACEA77B33ECE0662F  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass63_0
struct U3CU3Ec__DisplayClass63_0_t6ED6F5F7C2A008395097910C532090BDB4233C19  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass63_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass64_0
struct U3CU3Ec__DisplayClass64_0_tAE3DFABCC8670C2C5167F242B7E1B7D334581BC7  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass64_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass65_0
struct U3CU3Ec__DisplayClass65_0_tE76C72BE81B1FB69195AB05068181B97608FBC14  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass65_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass66_0
struct U3CU3Ec__DisplayClass66_0_tDD5A3F15C0731E215A01EA78331D5470F79AE88B  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass66_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass67_0
struct U3CU3Ec__DisplayClass67_0_t0CA35EFB06B7B01102C6AA20221F04713017B66F  : public RuntimeObject
{
	// DG.Tweening.Tween DG.Tweening.ShortcutExtensions/<>c__DisplayClass67_0::target
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___target_0;
};

// UnityEngine.TextCore.Text.SpriteAsset/<>c
struct U3CU3Ec_t04E77827D54CC7D69F04FDFC5A84C3F7392F9A76  : public RuntimeObject
{
};

// UnityEngine.Timeline.TimeNotificationBehaviour/<>c
struct U3CU3Ec_t008379627E11F153BBF661B1F74241272206C3D2  : public RuntimeObject
{
};

// UnityEngine.Timeline.TimelineAsset/EditorSettings
struct EditorSettings_t3A8D02A80F57944B75911D9692FECDF6B7081DFF  : public RuntimeObject
{
	// System.Double UnityEngine.Timeline.TimelineAsset/EditorSettings::m_Framerate
	double ___m_Framerate_3;
	// System.Boolean UnityEngine.Timeline.TimelineAsset/EditorSettings::m_ScenePreview
	bool ___m_ScenePreview_4;
};

// UnityEngine.Timeline.TimelineClip/TimelineClipUpgrade
struct TimelineClipUpgrade_t69B67BF242D4B5D087908D2D3726619897DAA620  : public RuntimeObject
{
};

// UnityEngine.Timeline.TimelineCreateUtilities/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_tEEFDBD8392308CD42F270E5772842108CCBA2AD6  : public RuntimeObject
{
	// System.String UnityEngine.Timeline.TimelineCreateUtilities/<>c__DisplayClass0_0::name
	String_t* ___name_0;
};

// UnityEngine.Timeline.TimelineCreateUtilities/<>c__DisplayClass0_1
struct U3CU3Ec__DisplayClass0_1_t6C3CFF368A6F745558E6345BDEEF00CC8B033591  : public RuntimeObject
{
	// System.String UnityEngine.Timeline.TimelineCreateUtilities/<>c__DisplayClass0_1::result
	String_t* ___result_0;
};

// UnityEngine.Timeline.TrackAsset/<>c
struct U3CU3Ec_t893DC1C51582BE5AFCDF9E5AE64A900EB53B344A  : public RuntimeObject
{
};

// Unity.Mathematics.float2/DebuggerProxy
struct DebuggerProxy_t2F3C70F890B53139614C348B6F97B31367817B27  : public RuntimeObject
{
};

// Unity.Mathematics.float3/DebuggerProxy
struct DebuggerProxy_t7ADF52C52717A46ECA0C565057AE4DE54B2C69B8  : public RuntimeObject
{
};

// Unity.Mathematics.float4/DebuggerProxy
struct DebuggerProxy_tD5F63FE661BF3F780BEFBE721E36E7A87B310121  : public RuntimeObject
{
};

// Unity.Mathematics.int2/DebuggerProxy
struct DebuggerProxy_tE4ABD062E1F1465B4A2601D1A2B183D9A691F3E7  : public RuntimeObject
{
};

// Unity.Mathematics.uint2/DebuggerProxy
struct DebuggerProxy_t834C4C5984734C3563C7BCAA0995DB56453E04DB  : public RuntimeObject
{
};

// Unity.Mathematics.uint3/DebuggerProxy
struct DebuggerProxy_tFD3C8C80DBBCC5A0DBFB6C74F3A6C2BDFD1F362A  : public RuntimeObject
{
};

// Unity.Mathematics.uint4/DebuggerProxy
struct DebuggerProxy_t3A8A5A2398C653D7A869D7B07919E08EB23696E8  : public RuntimeObject
{
};

// Unity.Collections.NativeArray`1<System.Byte>
struct NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF 
{
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;
};

// System.Nullable`1<System.Boolean>
struct Nullable_1_t78F453FADB4A9F50F267A4E349019C34410D1A01 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	bool ___value_1;
};

// UnityEngine.TextCore.Text.TextProcessingStack`1<System.Int32>
struct TextProcessingStack_1_t27834AAB14D26DC6519558C4C2574BA9C190D8E8 
{
	// T[] UnityEngine.TextCore.Text.TextProcessingStack`1::itemStack
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___itemStack_0;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::index
	int32_t ___index_1;
	// T UnityEngine.TextCore.Text.TextProcessingStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_2;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// UnityEngine.TextCore.Text.TextProcessingStack`1<System.Single>
struct TextProcessingStack_1_t9FC06E35259ADD291ED640FE7554D8C03EA5F555 
{
	// T[] UnityEngine.TextCore.Text.TextProcessingStack`1::itemStack
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___itemStack_0;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::index
	int32_t ___index_1;
	// T UnityEngine.TextCore.Text.TextProcessingStack`1::m_DefaultItem
	float ___m_DefaultItem_2;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.TextCore.Text.TextAlignment>
struct TextProcessingStack_1_tE63296B08A4C34E38D7EF3FFFA3470076B9E3A0F 
{
	// T[] UnityEngine.TextCore.Text.TextProcessingStack`1::itemStack
	TextAlignmentU5BU5D_t756DC2D672145699CB9718DDBA5982ED51A95F49* ___itemStack_0;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::index
	int32_t ___index_1;
	// T UnityEngine.TextCore.Text.TextProcessingStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_2;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.TextCore.Text.TextColorGradient>
struct TextProcessingStack_1_t0F39F088E8F8F6E18C3C463B2998ADC5B7A0513E 
{
	// T[] UnityEngine.TextCore.Text.TextProcessingStack`1::itemStack
	TextColorGradientU5BU5D_tA27A5E49640CF01334A10DBDBC959903AFBD941A* ___itemStack_0;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::index
	int32_t ___index_1;
	// T UnityEngine.TextCore.Text.TextProcessingStack`1::m_DefaultItem
	TextColorGradient_t22D94E441E8E8CD772B966C167E5C0AEB0919D70* ___m_DefaultItem_2;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.TextCore.Text.TextFontWeight>
struct TextProcessingStack_1_t698B87CDD968C2046F57134BB3AB807EBFFD7790 
{
	// T[] UnityEngine.TextCore.Text.TextProcessingStack`1::itemStack
	TextFontWeightU5BU5D_t3DE32809AEE657255C8333897D61F2EA5279D43F* ___itemStack_0;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::index
	int32_t ___index_1;
	// T UnityEngine.TextCore.Text.TextProcessingStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_2;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// UnityEngine.AnimatorClipInfo
struct AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03 
{
	// System.Int32 UnityEngine.AnimatorClipInfo::m_ClipInstanceID
	int32_t ___m_ClipInstanceID_0;
	// System.Single UnityEngine.AnimatorClipInfo::m_Weight
	float ___m_Weight_1;
};

// UnityEngine.AnimatorStateInfo
struct AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2 
{
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;
};

// UnityEngine.AnimatorTransitionInfo
struct AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD 
{
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_FullPath
	int32_t ___m_FullPath_0;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_UserName
	int32_t ___m_UserName_1;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_Name
	int32_t ___m_Name_2;
	// System.Boolean UnityEngine.AnimatorTransitionInfo::m_HasFixedDuration
	bool ___m_HasFixedDuration_3;
	// System.Single UnityEngine.AnimatorTransitionInfo::m_Duration
	float ___m_Duration_4;
	// System.Single UnityEngine.AnimatorTransitionInfo::m_NormalizedTime
	float ___m_NormalizedTime_5;
	// System.Boolean UnityEngine.AnimatorTransitionInfo::m_AnyState
	bool ___m_AnyState_6;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_TransitionType
	int32_t ___m_TransitionType_7;
};
// Native definition for P/Invoke marshalling of UnityEngine.AnimatorTransitionInfo
struct AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD_marshaled_pinvoke
{
	int32_t ___m_FullPath_0;
	int32_t ___m_UserName_1;
	int32_t ___m_Name_2;
	int32_t ___m_HasFixedDuration_3;
	float ___m_Duration_4;
	float ___m_NormalizedTime_5;
	int32_t ___m_AnyState_6;
	int32_t ___m_TransitionType_7;
};
// Native definition for COM marshalling of UnityEngine.AnimatorTransitionInfo
struct AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD_marshaled_com
{
	int32_t ___m_FullPath_0;
	int32_t ___m_UserName_1;
	int32_t ___m_Name_2;
	int32_t ___m_HasFixedDuration_3;
	float ___m_Duration_4;
	float ___m_NormalizedTime_5;
	int32_t ___m_AnyState_6;
	int32_t ___m_TransitionType_7;
};

// UnityEngine.AssetFileNameExtensionAttribute
struct AssetFileNameExtensionAttribute_tEA86B663DC42BB5C4F9A2A081CD7D28845D9D056  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.AssetFileNameExtensionAttribute::<preferredExtension>k__BackingField
	String_t* ___U3CpreferredExtensionU3Ek__BackingField_0;
	// System.Collections.Generic.IEnumerable`1<System.String> UnityEngine.AssetFileNameExtensionAttribute::<otherExtensions>k__BackingField
	RuntimeObject* ___U3CotherExtensionsU3Ek__BackingField_1;
};

// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_tD5ABB3A2536319A3345B32A5481E37E23DD8CEDF 
{
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___m_defaultContextAction_1;
};
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_tD5ABB3A2536319A3345B32A5481E37E23DD8CEDF_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_tD5ABB3A2536319A3345B32A5481E37E23DD8CEDF_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};

// UnityEngine.XR.Bone
struct Bone_t2558B1DD1E4F405EA4D76A3B8D5149CA16011975 
{
	// System.UInt64 UnityEngine.XR.Bone::m_DeviceId
	uint64_t ___m_DeviceId_0;
	// System.UInt32 UnityEngine.XR.Bone::m_FeatureIndex
	uint32_t ___m_FeatureIndex_1;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// Unity.Burst.BurstCompileAttribute
struct BurstCompileAttribute_t35957F7418CF3B99A40C9E1C66CD3C56094A2C9D  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder
struct CatmullRomDecoder_tBC93937ED94DB6355B974915EE9885854F1A5EB0  : public ABSPathDecoder_t6B479550CEF6C183ACCA13F11E29E019270AB61C
{
};

// UnityEngine.TextCore.Text.Character
struct Character_t9B671B493FAC8D43638C69AF6AE92CBD103D80EC  : public TextElement_tCEF567A8810788262275B39DC39CBA6EBE7472DA
{
};

// DG.Tweening.Plugins.CirclePlugin
struct CirclePlugin_t448312372C0575D793130F5E7EE5EA0C5D32B5C1  : public ABSTweenPlugin_3_tF3514EEC2BD29A12B4641076F59405DFB6106DAE
{
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// DG.Tweening.Plugins.Color2Plugin
struct Color2Plugin_tEC0AF600F0A8B7ED84B98B30EC22D064A1BD65E0  : public ABSTweenPlugin_3_t2B071FD0D9C7410BEDE633CDB14F13654FC2A973
{
};

// UnityEngine.Color32
struct Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};
};

// DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t9F2151E3A21F3FE2A41BEEF7D288D670C2685F39 
{
	// System.Boolean DG.Tweening.Plugins.Options.ColorOptions::alphaOnly
	bool ___alphaOnly_0;
};
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t9F2151E3A21F3FE2A41BEEF7D288D670C2685F39_marshaled_pinvoke
{
	int32_t ___alphaOnly_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t9F2151E3A21F3FE2A41BEEF7D288D670C2685F39_marshaled_com
{
	int32_t ___alphaOnly_0;
};

// DG.Tweening.Plugins.ColorPlugin
struct ColorPlugin_t12D5DF9AD61036BFA78330F7123998B5EE1C8954  : public ABSTweenPlugin_3_tC26278B0CA053BAF4547345679D3E8D1F748AC12
{
};

// System.Configuration.ConfigurationCollectionAttribute
struct ConfigurationCollectionAttribute_t1D7DBAAB4908B6B8F26EA1C66106A67BDE949558  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// System.Configuration.ConfigurationElementCollection
struct ConfigurationElementCollection_t56E8398661A85A59616301BADF13026FB1492606  : public ConfigurationElement_tAE3EE71C256825472831FFBB7F491275DFAF089E
{
};

// System.Configuration.ConfigurationSection
struct ConfigurationSection_t0BC609F0151B160A4FAB8226679B62AF22539C3E  : public ConfigurationElement_tAE3EE71C256825472831FFBB7F491275DFAF089E
{
};

// DG.Tweening.Plugins.Core.PathCore.CubicBezierDecoder
struct CubicBezierDecoder_t58382D9354F3F75F8D6A235E945C013EACD3CC1D  : public ABSPathDecoder_t6B479550CEF6C183ACCA13F11E29E019270AB61C
{
};

// System.DateTime
struct DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D 
{
	// System.UInt64 System.DateTime::_dateData
	uint64_t ____dateData_46;
};

// UnityEngine.Animations.DiscreteEvaluationAttribute
struct DiscreteEvaluationAttribute_tF23FCB5AB01B394BF5BD84623364A965C90F8BB9  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.Timeline.DiscreteTime
struct DiscreteTime_t1598D60B0B2432F702E2A6120D04369EE54600A6 
{
	// System.Int64 UnityEngine.Timeline.DiscreteTime::m_DiscreteTime
	int64_t ___m_DiscreteTime_2;
};

// OPS.Obfuscator.Attribute.DoNotObfuscateClassAttribute
struct DoNotObfuscateClassAttribute_t28D5BEC72DA2A88F118DB9764424391DB2F9312B  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// OPS.Obfuscator.Attribute.DoNotObfuscateMethodBodyAttribute
struct DoNotObfuscateMethodBodyAttribute_t0DCC53A255BEBEF9BBAE0F007FC7A9D4BA547868  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// OPS.Obfuscator.Attribute.DoNotRenameAttribute
struct DoNotRenameAttribute_tDF0E0CDAF447C0119529B7A7C87DFDAABCFA0653  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// OPS.Obfuscator.Attribute.DoNotUseClassForFakeCodeAttribute
struct DoNotUseClassForFakeCodeAttribute_t198754EECF12C9E58F495144BD1336F77995FEB9  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// DG.Tweening.Plugins.DoublePlugin
struct DoublePlugin_tF3C7CBCD2DFE818D53C038EA61FEE4838F62138F  : public ABSTweenPlugin_3_t7A4F79038C7079DFB69340991577802A9439A59D
{
};

// UnityEngine.UIElements.UIR.DrawBufferRange
struct DrawBufferRange_t684F255F5C954760B12F6689F84E78811040C7A4 
{
	// System.Int32 UnityEngine.UIElements.UIR.DrawBufferRange::firstIndex
	int32_t ___firstIndex_0;
	// System.Int32 UnityEngine.UIElements.UIR.DrawBufferRange::indexCount
	int32_t ___indexCount_1;
	// System.Int32 UnityEngine.UIElements.UIR.DrawBufferRange::minIndexVal
	int32_t ___minIndexVal_2;
	// System.Int32 UnityEngine.UIElements.UIR.DrawBufferRange::vertsReferenced
	int32_t ___vertsReferenced_3;
};

// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_t4F9AD0F906A21B3318CFA9466B076A467E16305F  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_tE12941E90BE7D41A61A8FF0507EC86D12F1C2ACC  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.EventInterests
struct EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8 
{
	// System.Boolean UnityEngine.EventInterests::<wantsMouseMove>k__BackingField
	bool ___U3CwantsMouseMoveU3Ek__BackingField_0;
	// System.Boolean UnityEngine.EventInterests::<wantsMouseEnterLeaveWindow>k__BackingField
	bool ___U3CwantsMouseEnterLeaveWindowU3Ek__BackingField_1;
	// System.Boolean UnityEngine.EventInterests::<wantsLessLayoutEvents>k__BackingField
	bool ___U3CwantsLessLayoutEventsU3Ek__BackingField_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.EventInterests
struct EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8_marshaled_pinvoke
{
	int32_t ___U3CwantsMouseMoveU3Ek__BackingField_0;
	int32_t ___U3CwantsMouseEnterLeaveWindowU3Ek__BackingField_1;
	int32_t ___U3CwantsLessLayoutEventsU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.EventInterests
struct EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8_marshaled_com
{
	int32_t ___U3CwantsMouseMoveU3Ek__BackingField_0;
	int32_t ___U3CwantsMouseEnterLeaveWindowU3Ek__BackingField_1;
	int32_t ___U3CwantsLessLayoutEventsU3Ek__BackingField_2;
};

// UnityEngine.XR.Eyes
struct Eyes_t9FD3821575977E294F11E0EB92D2A4CA509ED8C7 
{
	// System.UInt64 UnityEngine.XR.Eyes::m_DeviceId
	uint64_t ___m_DeviceId_0;
	// System.UInt32 UnityEngine.XR.Eyes::m_FeatureIndex
	uint32_t ___m_FeatureIndex_1;
};

// UnityEngine.TextCore.FaceInfo
struct FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756 
{
	// System.Int32 UnityEngine.TextCore.FaceInfo::m_FaceIndex
	int32_t ___m_FaceIndex_0;
	// System.String UnityEngine.TextCore.FaceInfo::m_FamilyName
	String_t* ___m_FamilyName_1;
	// System.String UnityEngine.TextCore.FaceInfo::m_StyleName
	String_t* ___m_StyleName_2;
	// System.Int32 UnityEngine.TextCore.FaceInfo::m_PointSize
	int32_t ___m_PointSize_3;
	// System.Single UnityEngine.TextCore.FaceInfo::m_Scale
	float ___m_Scale_4;
	// System.Int32 UnityEngine.TextCore.FaceInfo::m_UnitsPerEM
	int32_t ___m_UnitsPerEM_5;
	// System.Single UnityEngine.TextCore.FaceInfo::m_LineHeight
	float ___m_LineHeight_6;
	// System.Single UnityEngine.TextCore.FaceInfo::m_AscentLine
	float ___m_AscentLine_7;
	// System.Single UnityEngine.TextCore.FaceInfo::m_CapLine
	float ___m_CapLine_8;
	// System.Single UnityEngine.TextCore.FaceInfo::m_MeanLine
	float ___m_MeanLine_9;
	// System.Single UnityEngine.TextCore.FaceInfo::m_Baseline
	float ___m_Baseline_10;
	// System.Single UnityEngine.TextCore.FaceInfo::m_DescentLine
	float ___m_DescentLine_11;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SuperscriptOffset
	float ___m_SuperscriptOffset_12;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SuperscriptSize
	float ___m_SuperscriptSize_13;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SubscriptOffset
	float ___m_SubscriptOffset_14;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SubscriptSize
	float ___m_SubscriptSize_15;
	// System.Single UnityEngine.TextCore.FaceInfo::m_UnderlineOffset
	float ___m_UnderlineOffset_16;
	// System.Single UnityEngine.TextCore.FaceInfo::m_UnderlineThickness
	float ___m_UnderlineThickness_17;
	// System.Single UnityEngine.TextCore.FaceInfo::m_StrikethroughOffset
	float ___m_StrikethroughOffset_18;
	// System.Single UnityEngine.TextCore.FaceInfo::m_StrikethroughThickness
	float ___m_StrikethroughThickness_19;
	// System.Single UnityEngine.TextCore.FaceInfo::m_TabWidth
	float ___m_TabWidth_20;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.FaceInfo
struct FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756_marshaled_pinvoke
{
	int32_t ___m_FaceIndex_0;
	char* ___m_FamilyName_1;
	char* ___m_StyleName_2;
	int32_t ___m_PointSize_3;
	float ___m_Scale_4;
	int32_t ___m_UnitsPerEM_5;
	float ___m_LineHeight_6;
	float ___m_AscentLine_7;
	float ___m_CapLine_8;
	float ___m_MeanLine_9;
	float ___m_Baseline_10;
	float ___m_DescentLine_11;
	float ___m_SuperscriptOffset_12;
	float ___m_SuperscriptSize_13;
	float ___m_SubscriptOffset_14;
	float ___m_SubscriptSize_15;
	float ___m_UnderlineOffset_16;
	float ___m_UnderlineThickness_17;
	float ___m_StrikethroughOffset_18;
	float ___m_StrikethroughThickness_19;
	float ___m_TabWidth_20;
};
// Native definition for COM marshalling of UnityEngine.TextCore.FaceInfo
struct FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756_marshaled_com
{
	int32_t ___m_FaceIndex_0;
	Il2CppChar* ___m_FamilyName_1;
	Il2CppChar* ___m_StyleName_2;
	int32_t ___m_PointSize_3;
	float ___m_Scale_4;
	int32_t ___m_UnitsPerEM_5;
	float ___m_LineHeight_6;
	float ___m_AscentLine_7;
	float ___m_CapLine_8;
	float ___m_MeanLine_9;
	float ___m_Baseline_10;
	float ___m_DescentLine_11;
	float ___m_SuperscriptOffset_12;
	float ___m_SuperscriptSize_13;
	float ___m_SubscriptOffset_14;
	float ___m_SubscriptSize_15;
	float ___m_UnderlineOffset_16;
	float ___m_UnderlineThickness_17;
	float ___m_StrikethroughOffset_18;
	float ___m_StrikethroughThickness_19;
	float ___m_TabWidth_20;
};

// DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t8A9B05DB7CF6CC90A27F300C2977D91A48B3FEF5 
{
	// System.Boolean DG.Tweening.Plugins.Options.FloatOptions::snapping
	bool ___snapping_0;
};
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t8A9B05DB7CF6CC90A27F300C2977D91A48B3FEF5_marshaled_pinvoke
{
	int32_t ___snapping_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t8A9B05DB7CF6CC90A27F300C2977D91A48B3FEF5_marshaled_com
{
	int32_t ___snapping_0;
};

// DG.Tweening.Plugins.FloatPlugin
struct FloatPlugin_tC1B73BD6C186ED983EE92FAFA44996E106521602  : public ABSTweenPlugin_3_t60F4DE5120CFD5986925189A0E775FAEAB4C59B9
{
};

// UnityEngine.TextCore.Text.FontAssetCreationEditorSettings
struct FontAssetCreationEditorSettings_t0FF28D2E78F090105C63C81F9E438A7B09E3EA52 
{
	// System.String UnityEngine.TextCore.Text.FontAssetCreationEditorSettings::sourceFontFileGUID
	String_t* ___sourceFontFileGUID_0;
	// System.Int32 UnityEngine.TextCore.Text.FontAssetCreationEditorSettings::faceIndex
	int32_t ___faceIndex_1;
	// System.Int32 UnityEngine.TextCore.Text.FontAssetCreationEditorSettings::pointSizeSamplingMode
	int32_t ___pointSizeSamplingMode_2;
	// System.Int32 UnityEngine.TextCore.Text.FontAssetCreationEditorSettings::pointSize
	int32_t ___pointSize_3;
	// System.Int32 UnityEngine.TextCore.Text.FontAssetCreationEditorSettings::padding
	int32_t ___padding_4;
	// System.Int32 UnityEngine.TextCore.Text.FontAssetCreationEditorSettings::packingMode
	int32_t ___packingMode_5;
	// System.Int32 UnityEngine.TextCore.Text.FontAssetCreationEditorSettings::atlasWidth
	int32_t ___atlasWidth_6;
	// System.Int32 UnityEngine.TextCore.Text.FontAssetCreationEditorSettings::atlasHeight
	int32_t ___atlasHeight_7;
	// System.Int32 UnityEngine.TextCore.Text.FontAssetCreationEditorSettings::characterSetSelectionMode
	int32_t ___characterSetSelectionMode_8;
	// System.String UnityEngine.TextCore.Text.FontAssetCreationEditorSettings::characterSequence
	String_t* ___characterSequence_9;
	// System.String UnityEngine.TextCore.Text.FontAssetCreationEditorSettings::referencedFontAssetGUID
	String_t* ___referencedFontAssetGUID_10;
	// System.String UnityEngine.TextCore.Text.FontAssetCreationEditorSettings::referencedTextAssetGUID
	String_t* ___referencedTextAssetGUID_11;
	// System.Int32 UnityEngine.TextCore.Text.FontAssetCreationEditorSettings::fontStyle
	int32_t ___fontStyle_12;
	// System.Single UnityEngine.TextCore.Text.FontAssetCreationEditorSettings::fontStyleModifier
	float ___fontStyleModifier_13;
	// System.Int32 UnityEngine.TextCore.Text.FontAssetCreationEditorSettings::renderMode
	int32_t ___renderMode_14;
	// System.Boolean UnityEngine.TextCore.Text.FontAssetCreationEditorSettings::includeFontFeatures
	bool ___includeFontFeatures_15;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.Text.FontAssetCreationEditorSettings
struct FontAssetCreationEditorSettings_t0FF28D2E78F090105C63C81F9E438A7B09E3EA52_marshaled_pinvoke
{
	char* ___sourceFontFileGUID_0;
	int32_t ___faceIndex_1;
	int32_t ___pointSizeSamplingMode_2;
	int32_t ___pointSize_3;
	int32_t ___padding_4;
	int32_t ___packingMode_5;
	int32_t ___atlasWidth_6;
	int32_t ___atlasHeight_7;
	int32_t ___characterSetSelectionMode_8;
	char* ___characterSequence_9;
	char* ___referencedFontAssetGUID_10;
	char* ___referencedTextAssetGUID_11;
	int32_t ___fontStyle_12;
	float ___fontStyleModifier_13;
	int32_t ___renderMode_14;
	int32_t ___includeFontFeatures_15;
};
// Native definition for COM marshalling of UnityEngine.TextCore.Text.FontAssetCreationEditorSettings
struct FontAssetCreationEditorSettings_t0FF28D2E78F090105C63C81F9E438A7B09E3EA52_marshaled_com
{
	Il2CppChar* ___sourceFontFileGUID_0;
	int32_t ___faceIndex_1;
	int32_t ___pointSizeSamplingMode_2;
	int32_t ___pointSize_3;
	int32_t ___padding_4;
	int32_t ___packingMode_5;
	int32_t ___atlasWidth_6;
	int32_t ___atlasHeight_7;
	int32_t ___characterSetSelectionMode_8;
	Il2CppChar* ___characterSequence_9;
	Il2CppChar* ___referencedFontAssetGUID_10;
	Il2CppChar* ___referencedTextAssetGUID_11;
	int32_t ___fontStyle_12;
	float ___fontStyleModifier_13;
	int32_t ___renderMode_14;
	int32_t ___includeFontFeatures_15;
};

// UnityEngine.TextCore.LowLevel.FontEngineUtilities
struct FontEngineUtilities_t08D8707F6F929B42407961E303FD339A793E5BBB 
{
	union
	{
		struct
		{
		};
		uint8_t FontEngineUtilities_t08D8707F6F929B42407961E303FD339A793E5BBB__padding[1];
	};
};

// UnityEngine.TextCore.LowLevel.FontReference
struct FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172 
{
	// System.String UnityEngine.TextCore.LowLevel.FontReference::familyName
	String_t* ___familyName_0;
	// System.String UnityEngine.TextCore.LowLevel.FontReference::styleName
	String_t* ___styleName_1;
	// System.Int32 UnityEngine.TextCore.LowLevel.FontReference::faceIndex
	int32_t ___faceIndex_2;
	// System.String UnityEngine.TextCore.LowLevel.FontReference::filePath
	String_t* ___filePath_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.LowLevel.FontReference
struct FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172_marshaled_pinvoke
{
	char* ___familyName_0;
	char* ___styleName_1;
	int32_t ___faceIndex_2;
	char* ___filePath_3;
};
// Native definition for COM marshalling of UnityEngine.TextCore.LowLevel.FontReference
struct FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172_marshaled_com
{
	Il2CppChar* ___familyName_0;
	Il2CppChar* ___styleName_1;
	int32_t ___faceIndex_2;
	Il2CppChar* ___filePath_3;
};

// UnityEngine.TextCore.Text.FontStyleStack
struct FontStyleStack_t63C77495F068E6DF762D6AF063A817E3709659A7 
{
	// System.Byte UnityEngine.TextCore.Text.FontStyleStack::bold
	uint8_t ___bold_0;
	// System.Byte UnityEngine.TextCore.Text.FontStyleStack::italic
	uint8_t ___italic_1;
	// System.Byte UnityEngine.TextCore.Text.FontStyleStack::underline
	uint8_t ___underline_2;
	// System.Byte UnityEngine.TextCore.Text.FontStyleStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte UnityEngine.TextCore.Text.FontStyleStack::highlight
	uint8_t ___highlight_4;
	// System.Byte UnityEngine.TextCore.Text.FontStyleStack::superscript
	uint8_t ___superscript_5;
	// System.Byte UnityEngine.TextCore.Text.FontStyleStack::subscript
	uint8_t ___subscript_6;
	// System.Byte UnityEngine.TextCore.Text.FontStyleStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte UnityEngine.TextCore.Text.FontStyleStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte UnityEngine.TextCore.Text.FontStyleStack::smallcaps
	uint8_t ___smallcaps_9;
};

// UnityEngine.TextCore.Text.FontWeightPair
struct FontWeightPair_tB94169BD86D0DFC25D651F890B15991A3E0ADA42 
{
	// UnityEngine.TextCore.Text.FontAsset UnityEngine.TextCore.Text.FontWeightPair::regularTypeface
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___regularTypeface_0;
	// UnityEngine.TextCore.Text.FontAsset UnityEngine.TextCore.Text.FontWeightPair::italicTypeface
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___italicTypeface_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.Text.FontWeightPair
struct FontWeightPair_tB94169BD86D0DFC25D651F890B15991A3E0ADA42_marshaled_pinvoke
{
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___regularTypeface_0;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___italicTypeface_1;
};
// Native definition for COM marshalling of UnityEngine.TextCore.Text.FontWeightPair
struct FontWeightPair_tB94169BD86D0DFC25D651F890B15991A3E0ADA42_marshaled_com
{
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___regularTypeface_0;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___italicTypeface_1;
};

// UnityEngine.GUITargetAttribute
struct GUITargetAttribute_t3F08CE7E00D79CB555B94AA7FA1BCB4B144B922B  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Int32 UnityEngine.GUITargetAttribute::displayMask
	int32_t ___displayMask_0;
};

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
struct GcAchievementData_tBA953A0889E78C0050ED548758B7AF25F12799D4 
{
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::m_Identifier
	String_t* ___m_Identifier_0;
	// System.Double UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::m_PercentCompleted
	double ___m_PercentCompleted_1;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::m_Completed
	int32_t ___m_Completed_2;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::m_Hidden
	int32_t ___m_Hidden_3;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::m_LastReportedDate
	int32_t ___m_LastReportedDate_4;
};
// Native definition for P/Invoke marshalling of UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
struct GcAchievementData_tBA953A0889E78C0050ED548758B7AF25F12799D4_marshaled_pinvoke
{
	char* ___m_Identifier_0;
	double ___m_PercentCompleted_1;
	int32_t ___m_Completed_2;
	int32_t ___m_Hidden_3;
	int32_t ___m_LastReportedDate_4;
};
// Native definition for COM marshalling of UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
struct GcAchievementData_tBA953A0889E78C0050ED548758B7AF25F12799D4_marshaled_com
{
	Il2CppChar* ___m_Identifier_0;
	double ___m_PercentCompleted_1;
	int32_t ___m_Completed_2;
	int32_t ___m_Hidden_3;
	int32_t ___m_LastReportedDate_4;
};

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
struct GcAchievementDescriptionData_t9C5BBAB764F0088FE40698EB33FE79D5173B2086 
{
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::m_Identifier
	String_t* ___m_Identifier_0;
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::m_Title
	String_t* ___m_Title_1;
	// UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::m_Image
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___m_Image_2;
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::m_AchievedDescription
	String_t* ___m_AchievedDescription_3;
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::m_UnachievedDescription
	String_t* ___m_UnachievedDescription_4;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::m_Hidden
	int32_t ___m_Hidden_5;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::m_Points
	int32_t ___m_Points_6;
};
// Native definition for P/Invoke marshalling of UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
struct GcAchievementDescriptionData_t9C5BBAB764F0088FE40698EB33FE79D5173B2086_marshaled_pinvoke
{
	char* ___m_Identifier_0;
	char* ___m_Title_1;
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___m_Image_2;
	char* ___m_AchievedDescription_3;
	char* ___m_UnachievedDescription_4;
	int32_t ___m_Hidden_5;
	int32_t ___m_Points_6;
};
// Native definition for COM marshalling of UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
struct GcAchievementDescriptionData_t9C5BBAB764F0088FE40698EB33FE79D5173B2086_marshaled_com
{
	Il2CppChar* ___m_Identifier_0;
	Il2CppChar* ___m_Title_1;
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___m_Image_2;
	Il2CppChar* ___m_AchievedDescription_3;
	Il2CppChar* ___m_UnachievedDescription_4;
	int32_t ___m_Hidden_5;
	int32_t ___m_Points_6;
};

// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
struct GcScoreData_t90ADB5BBE4EF7B4B1E0503E9E0934EA2ED254F0F 
{
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcScoreData::m_Category
	String_t* ___m_Category_0;
	// System.UInt32 UnityEngine.SocialPlatforms.GameCenter.GcScoreData::m_ValueLow
	uint32_t ___m_ValueLow_1;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcScoreData::m_ValueHigh
	int32_t ___m_ValueHigh_2;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcScoreData::m_Date
	int32_t ___m_Date_3;
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcScoreData::m_FormattedValue
	String_t* ___m_FormattedValue_4;
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcScoreData::m_PlayerID
	String_t* ___m_PlayerID_5;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcScoreData::m_Rank
	int32_t ___m_Rank_6;
};
// Native definition for P/Invoke marshalling of UnityEngine.SocialPlatforms.GameCenter.GcScoreData
struct GcScoreData_t90ADB5BBE4EF7B4B1E0503E9E0934EA2ED254F0F_marshaled_pinvoke
{
	char* ___m_Category_0;
	uint32_t ___m_ValueLow_1;
	int32_t ___m_ValueHigh_2;
	int32_t ___m_Date_3;
	char* ___m_FormattedValue_4;
	char* ___m_PlayerID_5;
	int32_t ___m_Rank_6;
};
// Native definition for COM marshalling of UnityEngine.SocialPlatforms.GameCenter.GcScoreData
struct GcScoreData_t90ADB5BBE4EF7B4B1E0503E9E0934EA2ED254F0F_marshaled_com
{
	Il2CppChar* ___m_Category_0;
	uint32_t ___m_ValueLow_1;
	int32_t ___m_ValueHigh_2;
	int32_t ___m_Date_3;
	Il2CppChar* ___m_FormattedValue_4;
	Il2CppChar* ___m_PlayerID_5;
	int32_t ___m_Rank_6;
};

// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
struct GcUserProfileData_tCDEF4010D44CB370077CE47055C89CD9E808A535 
{
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::userName
	String_t* ___userName_0;
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::teamID
	String_t* ___teamID_1;
	// System.String UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::gameID
	String_t* ___gameID_2;
	// System.Int32 UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::isFriend
	int32_t ___isFriend_3;
	// UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::image
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___image_4;
};
// Native definition for P/Invoke marshalling of UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
struct GcUserProfileData_tCDEF4010D44CB370077CE47055C89CD9E808A535_marshaled_pinvoke
{
	char* ___userName_0;
	char* ___teamID_1;
	char* ___gameID_2;
	int32_t ___isFriend_3;
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___image_4;
};
// Native definition for COM marshalling of UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
struct GcUserProfileData_tCDEF4010D44CB370077CE47055C89CD9E808A535_marshaled_com
{
	Il2CppChar* ___userName_0;
	Il2CppChar* ___teamID_1;
	Il2CppChar* ___gameID_2;
	int32_t ___isFriend_3;
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___image_4;
};

// UnityEngine.TextCore.GlyphMetrics
struct GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A 
{
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_Width
	float ___m_Width_0;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_Height
	float ___m_Height_1;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalBearingX
	float ___m_HorizontalBearingX_2;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalBearingY
	float ___m_HorizontalBearingY_3;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalAdvance
	float ___m_HorizontalAdvance_4;
};

// UnityEngine.TextCore.GlyphRect
struct GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D 
{
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Y
	int32_t ___m_Y_1;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Width
	int32_t ___m_Width_2;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Height
	int32_t ___m_Height_3;
};

// UnityEngine.TextCore.LowLevel.GlyphValueRecord
struct GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E 
{
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_XPlacement
	float ___m_XPlacement_0;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_YPlacement
	float ___m_YPlacement_1;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_XAdvance
	float ___m_XAdvance_2;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_YAdvance
	float ___m_YAdvance_3;
};

// UnityEngine.XR.Hand
struct Hand_t67B90BC0D36CBC92DF7E38BD15463B925CB5912C 
{
	// System.UInt64 UnityEngine.XR.Hand::m_DeviceId
	uint64_t ___m_DeviceId_0;
	// System.UInt32 UnityEngine.XR.Hand::m_FeatureIndex
	uint32_t ___m_FeatureIndex_1;
};

// UnityEngine.Timeline.HideInMenuAttribute
struct HideInMenuAttribute_t8F5AEDC2C84031EBD6C3F7A2C2B39DC4115B9DEC  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.HumanDescription
struct HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44 
{
	// UnityEngine.HumanBone[] UnityEngine.HumanDescription::human
	HumanBoneU5BU5D_t443B81D55400778CBB921DF04BE932ABF14BAA52* ___human_0;
	// UnityEngine.SkeletonBone[] UnityEngine.HumanDescription::skeleton
	SkeletonBoneU5BU5D_t84722CE071EF9295326D5A84492716DBC7A15A85* ___skeleton_1;
	// System.Single UnityEngine.HumanDescription::m_ArmTwist
	float ___m_ArmTwist_2;
	// System.Single UnityEngine.HumanDescription::m_ForeArmTwist
	float ___m_ForeArmTwist_3;
	// System.Single UnityEngine.HumanDescription::m_UpperLegTwist
	float ___m_UpperLegTwist_4;
	// System.Single UnityEngine.HumanDescription::m_LegTwist
	float ___m_LegTwist_5;
	// System.Single UnityEngine.HumanDescription::m_ArmStretch
	float ___m_ArmStretch_6;
	// System.Single UnityEngine.HumanDescription::m_LegStretch
	float ___m_LegStretch_7;
	// System.Single UnityEngine.HumanDescription::m_FeetSpacing
	float ___m_FeetSpacing_8;
	// System.Single UnityEngine.HumanDescription::m_GlobalScale
	float ___m_GlobalScale_9;
	// System.String UnityEngine.HumanDescription::m_RootMotionBoneName
	String_t* ___m_RootMotionBoneName_10;
	// System.Boolean UnityEngine.HumanDescription::m_HasTranslationDoF
	bool ___m_HasTranslationDoF_11;
	// System.Boolean UnityEngine.HumanDescription::m_HasExtraRoot
	bool ___m_HasExtraRoot_12;
	// System.Boolean UnityEngine.HumanDescription::m_SkeletonHasParents
	bool ___m_SkeletonHasParents_13;
};
// Native definition for P/Invoke marshalling of UnityEngine.HumanDescription
struct HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44_marshaled_pinvoke
{
	HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_pinvoke* ___human_0;
	SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_pinvoke* ___skeleton_1;
	float ___m_ArmTwist_2;
	float ___m_ForeArmTwist_3;
	float ___m_UpperLegTwist_4;
	float ___m_LegTwist_5;
	float ___m_ArmStretch_6;
	float ___m_LegStretch_7;
	float ___m_FeetSpacing_8;
	float ___m_GlobalScale_9;
	char* ___m_RootMotionBoneName_10;
	int32_t ___m_HasTranslationDoF_11;
	int32_t ___m_HasExtraRoot_12;
	int32_t ___m_SkeletonHasParents_13;
};
// Native definition for COM marshalling of UnityEngine.HumanDescription
struct HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44_marshaled_com
{
	HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_com* ___human_0;
	SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_com* ___skeleton_1;
	float ___m_ArmTwist_2;
	float ___m_ForeArmTwist_3;
	float ___m_UpperLegTwist_4;
	float ___m_LegTwist_5;
	float ___m_ArmStretch_6;
	float ___m_LegStretch_7;
	float ___m_FeetSpacing_8;
	float ___m_GlobalScale_9;
	Il2CppChar* ___m_RootMotionBoneName_10;
	int32_t ___m_HasTranslationDoF_11;
	int32_t ___m_HasExtraRoot_12;
	int32_t ___m_SkeletonHasParents_13;
};

// UnityEngine.Bindings.IgnoreAttribute
struct IgnoreAttribute_tAB3F6C4808BA16CD585D60A6353B3E0599DFCE4D  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Boolean UnityEngine.Bindings.IgnoreAttribute::<DoesNotContributeToSize>k__BackingField
	bool ___U3CDoesNotContributeToSizeU3Ek__BackingField_0;
};

// UnityEngine.Timeline.IgnoreOnPlayableTrackAttribute
struct IgnoreOnPlayableTrackAttribute_tD17B1A4900010C52505414888355F8BCA3FC6049  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// Unity.IL2CPP.CompilerServices.Il2CppEagerStaticClassConstructionAttribute
struct Il2CppEagerStaticClassConstructionAttribute_t01F60DE85CC3427802A892466A27D5245AEA1BAF  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.XR.InputDevice
struct InputDevice_t882EE3EE8A71D8F5F38BA3F9356A49F24510E8BD 
{
	// System.UInt64 UnityEngine.XR.InputDevice::m_DeviceId
	uint64_t ___m_DeviceId_0;
	// System.Boolean UnityEngine.XR.InputDevice::m_Initialized
	bool ___m_Initialized_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.InputDevice
struct InputDevice_t882EE3EE8A71D8F5F38BA3F9356A49F24510E8BD_marshaled_pinvoke
{
	uint64_t ___m_DeviceId_0;
	int32_t ___m_Initialized_1;
};
// Native definition for COM marshalling of UnityEngine.XR.InputDevice
struct InputDevice_t882EE3EE8A71D8F5F38BA3F9356A49F24510E8BD_marshaled_com
{
	uint64_t ___m_DeviceId_0;
	int32_t ___m_Initialized_1;
};

// UnityEngine.XR.InputFeatureUsage
struct InputFeatureUsage_t1E251DC4F8CD697080F0F5D98388955AF8B87599 
{
	// System.String UnityEngine.XR.InputFeatureUsage::m_Name
	String_t* ___m_Name_0;
	// UnityEngine.XR.InputFeatureType UnityEngine.XR.InputFeatureUsage::m_InternalType
	uint32_t ___m_InternalType_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage
struct InputFeatureUsage_t1E251DC4F8CD697080F0F5D98388955AF8B87599_marshaled_pinvoke
{
	char* ___m_Name_0;
	uint32_t ___m_InternalType_1;
};
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage
struct InputFeatureUsage_t1E251DC4F8CD697080F0F5D98388955AF8B87599_marshaled_com
{
	Il2CppChar* ___m_Name_0;
	uint32_t ___m_InternalType_1;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.Int64
struct Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3 
{
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;
};

// DG.Tweening.Plugins.IntPlugin
struct IntPlugin_tF9F1AFC1C3A6BDD2443ED3E14E9F78EF89EC348A  : public ABSTweenPlugin_3_t79D91C3E0DB82A874E072CF1EF486B5F78D7F8DE
{
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// UnityEngine.Timeline.IntervalTreeNode
struct IntervalTreeNode_tDAA7D63276D62CD178C91CC7DF932C97896332EC 
{
	// System.Int64 UnityEngine.Timeline.IntervalTreeNode::center
	int64_t ___center_0;
	// System.Int32 UnityEngine.Timeline.IntervalTreeNode::first
	int32_t ___first_1;
	// System.Int32 UnityEngine.Timeline.IntervalTreeNode::last
	int32_t ___last_2;
	// System.Int32 UnityEngine.Timeline.IntervalTreeNode::left
	int32_t ___left_3;
	// System.Int32 UnityEngine.Timeline.IntervalTreeNode::right
	int32_t ___right_4;
};

// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_t537BA5EEEDB652AA60A00E4CEA6B73710FBDD806  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_t2C6085B49D0BEB030B26D149806E40189CACCA6F  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// System.Linq.Expressions.LambdaExpression
struct LambdaExpression_tD26FB6AEAD01B2EBB668CDEAFAAFA4948697300E  : public Expression_t70AA908ECBD33E94249BF235E4EBB0F831AD8785
{
	// System.Linq.Expressions.Expression System.Linq.Expressions.LambdaExpression::_body
	Expression_t70AA908ECBD33E94249BF235E4EBB0F831AD8785* ____body_2;
};

// UnityEngine.LayerMask
struct LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB 
{
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;
};

// DG.Tweening.Plugins.Core.PathCore.LinearDecoder
struct LinearDecoder_tC7C53176BBF58227DC1855AFDBA3FAFF19860B15  : public ABSPathDecoder_t6B479550CEF6C183ACCA13F11E29E019270AB61C
{
};

// UnityEngine.TextCore.Text.LinkInfo
struct LinkInfo_tE85DDAFDFBDA635E6405C88EE4FD5941A9243DD8 
{
	// System.Int32 UnityEngine.TextCore.Text.LinkInfo::hashCode
	int32_t ___hashCode_0;
	// System.Int32 UnityEngine.TextCore.Text.LinkInfo::linkIdFirstCharacterIndex
	int32_t ___linkIdFirstCharacterIndex_1;
	// System.Int32 UnityEngine.TextCore.Text.LinkInfo::linkIdLength
	int32_t ___linkIdLength_2;
	// System.Int32 UnityEngine.TextCore.Text.LinkInfo::linkTextfirstCharacterIndex
	int32_t ___linkTextfirstCharacterIndex_3;
	// System.Int32 UnityEngine.TextCore.Text.LinkInfo::linkTextLength
	int32_t ___linkTextLength_4;
	// System.Char[] UnityEngine.TextCore.Text.LinkInfo::linkId
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___linkId_5;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.Text.LinkInfo
struct LinkInfo_tE85DDAFDFBDA635E6405C88EE4FD5941A9243DD8_marshaled_pinvoke
{
	int32_t ___hashCode_0;
	int32_t ___linkIdFirstCharacterIndex_1;
	int32_t ___linkIdLength_2;
	int32_t ___linkTextfirstCharacterIndex_3;
	int32_t ___linkTextLength_4;
	uint8_t* ___linkId_5;
};
// Native definition for COM marshalling of UnityEngine.TextCore.Text.LinkInfo
struct LinkInfo_tE85DDAFDFBDA635E6405C88EE4FD5941A9243DD8_marshaled_com
{
	int32_t ___hashCode_0;
	int32_t ___linkIdFirstCharacterIndex_1;
	int32_t ___linkIdLength_2;
	int32_t ___linkTextfirstCharacterIndex_3;
	int32_t ___linkTextLength_4;
	uint8_t* ___linkId_5;
};

// UnityEngine.SocialPlatforms.Impl.LocalUser
struct LocalUser_t55C68E98993F86B6FBB7A25F28EB989CD7E6A3AD  : public UserProfile_t3EF35349E23201EF9F3C5956C44384FA45C1EF29
{
	// UnityEngine.SocialPlatforms.IUserProfile[] UnityEngine.SocialPlatforms.Impl.LocalUser::m_Friends
	IUserProfileU5BU5D_t0179D2FF9BD9F78A4E0A10AE350DC1F19E5FCB43* ___m_Friends_7;
	// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::m_Authenticated
	bool ___m_Authenticated_8;
	// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::m_Underage
	bool ___m_Underage_9;
};

// DG.Tweening.Plugins.LongPlugin
struct LongPlugin_t383C336429D2F3189B963651F9FF0609978A3B3B  : public ABSTweenPlugin_3_t7341CD6BAA9CD38815B6A9E3CFF47989DE634ABF
{
};

// UnityEngine.Timeline.MarkerList
struct MarkerList_tD4B632EBA98CE678EB8D108A1AF559F734FA7698 
{
	// System.Collections.Generic.List`1<UnityEngine.ScriptableObject> UnityEngine.Timeline.MarkerList::m_Objects
	List_1_tF941E9C3FEB6F1C2D20E73A90AA7F6319EB3F828* ___m_Objects_0;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.IMarker> UnityEngine.Timeline.MarkerList::m_Cache
	List_1_tB481045C42962DD282E8A89B2AF0246A4042EADF* ___m_Cache_1;
	// System.Boolean UnityEngine.Timeline.MarkerList::m_CacheDirty
	bool ___m_CacheDirty_2;
	// System.Boolean UnityEngine.Timeline.MarkerList::m_HasNotifications
	bool ___m_HasNotifications_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.Timeline.MarkerList
struct MarkerList_tD4B632EBA98CE678EB8D108A1AF559F734FA7698_marshaled_pinvoke
{
	List_1_tF941E9C3FEB6F1C2D20E73A90AA7F6319EB3F828* ___m_Objects_0;
	List_1_tB481045C42962DD282E8A89B2AF0246A4042EADF* ___m_Cache_1;
	int32_t ___m_CacheDirty_2;
	int32_t ___m_HasNotifications_3;
};
// Native definition for COM marshalling of UnityEngine.Timeline.MarkerList
struct MarkerList_tD4B632EBA98CE678EB8D108A1AF559F734FA7698_marshaled_com
{
	List_1_tF941E9C3FEB6F1C2D20E73A90AA7F6319EB3F828* ___m_Objects_0;
	List_1_tB481045C42962DD282E8A89B2AF0246A4042EADF* ___m_Cache_1;
	int32_t ___m_CacheDirty_2;
	int32_t ___m_HasNotifications_3;
};

// UnityEngine.TextCore.Text.MaterialReference
struct MaterialReference_t86DB0799D5C82869D4FF0A4F59624AED6910FD26 
{
	// System.Int32 UnityEngine.TextCore.Text.MaterialReference::index
	int32_t ___index_0;
	// UnityEngine.TextCore.Text.FontAsset UnityEngine.TextCore.Text.MaterialReference::fontAsset
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___fontAsset_1;
	// UnityEngine.TextCore.Text.SpriteAsset UnityEngine.TextCore.Text.MaterialReference::spriteAsset
	SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313* ___spriteAsset_2;
	// UnityEngine.Material UnityEngine.TextCore.Text.MaterialReference::material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_3;
	// System.Boolean UnityEngine.TextCore.Text.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean UnityEngine.TextCore.Text.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material UnityEngine.TextCore.Text.MaterialReference::fallbackMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___fallbackMaterial_6;
	// System.Single UnityEngine.TextCore.Text.MaterialReference::padding
	float ___padding_7;
	// System.Int32 UnityEngine.TextCore.Text.MaterialReference::referenceCount
	int32_t ___referenceCount_8;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.Text.MaterialReference
struct MaterialReference_t86DB0799D5C82869D4FF0A4F59624AED6910FD26_marshaled_pinvoke
{
	int32_t ___index_0;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___fontAsset_1;
	SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313* ___spriteAsset_2;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of UnityEngine.TextCore.Text.MaterialReference
struct MaterialReference_t86DB0799D5C82869D4FF0A4F59624AED6910FD26_marshaled_com
{
	int32_t ___index_0;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___fontAsset_1;
	SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313* ___spriteAsset_2;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};

// UnityEngine.Matrix4x4
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 
{
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;
};

// System.Linq.Expressions.MemberExpression
struct MemberExpression_t133C12A9CE765EF02D622D660CE80E146B15EF89  : public Expression_t70AA908ECBD33E94249BF235E4EBB0F831AD8785
{
};

// UnityEngine.XR.MeshId
struct MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC 
{
	// System.UInt64 UnityEngine.XR.MeshId::m_SubId1
	uint64_t ___m_SubId1_1;
	// System.UInt64 UnityEngine.XR.MeshId::m_SubId2
	uint64_t ___m_SubId2_2;
};

// UnityEngine.Bindings.NativeAsStructAttribute
struct NativeAsStructAttribute_t48549F0E2D38CC0251B7BF2780E434EA141DF2D8  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.NativeClassAttribute
struct NativeClassAttribute_t774C48B9F745C9B0FD2FA82F9B42D4A18E162FA7  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.NativeClassAttribute::<QualifiedNativeName>k__BackingField
	String_t* ___U3CQualifiedNativeNameU3Ek__BackingField_0;
	// System.String UnityEngine.NativeClassAttribute::<Declaration>k__BackingField
	String_t* ___U3CDeclarationU3Ek__BackingField_1;
};

// UnityEngine.Bindings.NativeConditionalAttribute
struct NativeConditionalAttribute_tB722B3ED350E82853F8CEFF672A6CDC4B6B362CA  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeConditionalAttribute::<Condition>k__BackingField
	String_t* ___U3CConditionU3Ek__BackingField_0;
	// System.String UnityEngine.Bindings.NativeConditionalAttribute::<StubReturnStatement>k__BackingField
	String_t* ___U3CStubReturnStatementU3Ek__BackingField_1;
	// System.Boolean UnityEngine.Bindings.NativeConditionalAttribute::<Enabled>k__BackingField
	bool ___U3CEnabledU3Ek__BackingField_2;
};

// UnityEngine.Bindings.NativeHeaderAttribute
struct NativeHeaderAttribute_t35DDAA41C31EEE4C94D2586F33D3EB26C0EA6F51  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeHeaderAttribute::<Header>k__BackingField
	String_t* ___U3CHeaderU3Ek__BackingField_0;
};

// UnityEngineInternal.Input.NativeInputEvent
struct NativeInputEvent_tDE7DE9A48ACA442A8D37E2920836D00C26408CB8 
{
	union
	{
		struct
		{
			union
			{
				#pragma pack(push, tp, 1)
				struct
				{
					// UnityEngineInternal.Input.NativeInputEventType UnityEngineInternal.Input.NativeInputEvent::type
					int32_t ___type_1;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					int32_t ___type_1_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___sizeInBytes_2_OffsetPadding[4];
					// System.UInt16 UnityEngineInternal.Input.NativeInputEvent::sizeInBytes
					uint16_t ___sizeInBytes_2;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___sizeInBytes_2_OffsetPadding_forAlignmentOnly[4];
					uint16_t ___sizeInBytes_2_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___deviceId_3_OffsetPadding[6];
					// System.UInt16 UnityEngineInternal.Input.NativeInputEvent::deviceId
					uint16_t ___deviceId_3;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___deviceId_3_OffsetPadding_forAlignmentOnly[6];
					uint16_t ___deviceId_3_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___time_4_OffsetPadding[8];
					// System.Double UnityEngineInternal.Input.NativeInputEvent::time
					double ___time_4;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___time_4_OffsetPadding_forAlignmentOnly[8];
					double ___time_4_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___eventId_5_OffsetPadding[16];
					// System.Int32 UnityEngineInternal.Input.NativeInputEvent::eventId
					int32_t ___eventId_5;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___eventId_5_OffsetPadding_forAlignmentOnly[16];
					int32_t ___eventId_5_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
			};
		};
		uint8_t NativeInputEvent_tDE7DE9A48ACA442A8D37E2920836D00C26408CB8__padding[20];
	};
};

// UnityEngineInternal.Input.NativeInputEventBuffer
struct NativeInputEventBuffer_t4EE5873AD7998E0E83C9F8585C338AB14C9101FD 
{
	union
	{
		struct
		{
			union
			{
				#pragma pack(push, tp, 1)
				struct
				{
					// System.Void* UnityEngineInternal.Input.NativeInputEventBuffer::eventBuffer
					void* ___eventBuffer_0;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					void* ___eventBuffer_0_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___eventCount_1_OffsetPadding[8];
					// System.Int32 UnityEngineInternal.Input.NativeInputEventBuffer::eventCount
					int32_t ___eventCount_1;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___eventCount_1_OffsetPadding_forAlignmentOnly[8];
					int32_t ___eventCount_1_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___sizeInBytes_2_OffsetPadding[12];
					// System.Int32 UnityEngineInternal.Input.NativeInputEventBuffer::sizeInBytes
					int32_t ___sizeInBytes_2;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___sizeInBytes_2_OffsetPadding_forAlignmentOnly[12];
					int32_t ___sizeInBytes_2_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___capacityInBytes_3_OffsetPadding[16];
					// System.Int32 UnityEngineInternal.Input.NativeInputEventBuffer::capacityInBytes
					int32_t ___capacityInBytes_3;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___capacityInBytes_3_OffsetPadding_forAlignmentOnly[16];
					int32_t ___capacityInBytes_3_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
			};
		};
		uint8_t NativeInputEventBuffer_t4EE5873AD7998E0E83C9F8585C338AB14C9101FD__padding[20];
	};
};

// UnityEngine.Bindings.NativeMethodAttribute
struct NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeMethodAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Bindings.NativeMethodAttribute::<IsThreadSafe>k__BackingField
	bool ___U3CIsThreadSafeU3Ek__BackingField_1;
	// System.Boolean UnityEngine.Bindings.NativeMethodAttribute::<IsFreeFunction>k__BackingField
	bool ___U3CIsFreeFunctionU3Ek__BackingField_2;
	// System.Boolean UnityEngine.Bindings.NativeMethodAttribute::<ThrowsException>k__BackingField
	bool ___U3CThrowsExceptionU3Ek__BackingField_3;
	// System.Boolean UnityEngine.Bindings.NativeMethodAttribute::<HasExplicitThis>k__BackingField
	bool ___U3CHasExplicitThisU3Ek__BackingField_4;
};

// UnityEngine.Bindings.NativeNameAttribute
struct NativeNameAttribute_t222751782B5418807DFE2A88CA0B24CA691B8621  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeNameAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
};

// UnityEngine.Bindings.NativeThrowsAttribute
struct NativeThrowsAttribute_t211CE8D047A8D45676C9ED399D5AA3B4A2C3E625  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Boolean UnityEngine.Bindings.NativeThrowsAttribute::<ThrowsException>k__BackingField
	bool ___U3CThrowsExceptionU3Ek__BackingField_0;
};

// UnityEngine.Bindings.NativeTypeAttribute
struct NativeTypeAttribute_tB60F1675F1F20B6CB1B871FDDD067D672F75B8D1  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeTypeAttribute::<Header>k__BackingField
	String_t* ___U3CHeaderU3Ek__BackingField_0;
	// System.String UnityEngine.Bindings.NativeTypeAttribute::<IntermediateScriptingStructName>k__BackingField
	String_t* ___U3CIntermediateScriptingStructNameU3Ek__BackingField_1;
	// UnityEngine.Bindings.CodegenOptions UnityEngine.Bindings.NativeTypeAttribute::<CodegenOptions>k__BackingField
	int32_t ___U3CCodegenOptionsU3Ek__BackingField_2;
};

// UnityEngine.Bindings.NativeWritableSelfAttribute
struct NativeWritableSelfAttribute_t2ABC353836DDC2F15B1FBED9C0CF2E5ED0D1686C  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Boolean UnityEngine.Bindings.NativeWritableSelfAttribute::<WritableSelf>k__BackingField
	bool ___U3CWritableSelfU3Ek__BackingField_0;
};

// DG.Tweening.Plugins.Options.NoOptions
struct NoOptions_t2B4A2CA3C472B5AC37AACC090B1D0B27BCF4307E 
{
	union
	{
		struct
		{
		};
		uint8_t NoOptions_t2B4A2CA3C472B5AC37AACC090B1D0B27BCF4307E__padding[1];
	};
};

// UnityEngine.Animations.NotKeyableAttribute
struct NotKeyableAttribute_tDDB6B25B26F649E3CED893EE1E63B6DE66844483  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.Timeline.NotKeyableAttribute
struct NotKeyableAttribute_tB601DB1128DFF4962D64BC486C0637230E819C14  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.Bindings.NotNullAttribute
struct NotNullAttribute_t2E29B7802E8ED55CEA04EC4A6C254C6B60272DF7  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NotNullAttribute::<Exception>k__BackingField
	String_t* ___U3CExceptionU3Ek__BackingField_0;
};

// OPS.Obfuscator.Attribute.NotObfuscatedCauseAttribute
struct NotObfuscatedCauseAttribute_tFB6D7D82C7F2ABE6CE218ACB6CE3E849DC9C2865  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// OPS.Obfuscator.Attribute.ObfuscateAnywayAttribute
struct ObfuscateAnywayAttribute_tFF38D6CED6FAC08B8615C58AECEDEA379854B579  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String OPS.Obfuscator.Attribute.ObfuscateAnywayAttribute::obfuscateTo
	String_t* ___obfuscateTo_0;
};

// UnityEngine.TextCore.Text.PageInfo
struct PageInfo_tFFF6B289E9A37E4D69353B32F941421180DA5909 
{
	// System.Int32 UnityEngine.TextCore.Text.PageInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_0;
	// System.Int32 UnityEngine.TextCore.Text.PageInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_1;
	// System.Single UnityEngine.TextCore.Text.PageInfo::ascender
	float ___ascender_2;
	// System.Single UnityEngine.TextCore.Text.PageInfo::baseLine
	float ___baseLine_3;
	// System.Single UnityEngine.TextCore.Text.PageInfo::descender
	float ___descender_4;
};

// DG.Tweening.Plugins.PathPlugin
struct PathPlugin_t8E755C6DF62D7C06030DCE6562860239008777A6  : public ABSTweenPlugin_3_t08B14BED068ACE348E543E45725D6C6BFFA60143
{
};

// UnityEngine.PhysicsScene
struct PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE 
{
	// System.Int32 UnityEngine.PhysicsScene::m_Handle
	int32_t ___m_Handle_0;
};

// UnityEngine.PhysicsScene2D
struct PhysicsScene2D_t550D023B9E77BE6844564BB4F9FA291EEA10FDC9 
{
	// System.Int32 UnityEngine.PhysicsScene2D::m_Handle
	int32_t ___m_Handle_0;
};

// UnityEngine.Bindings.PreventReadOnlyInstanceModificationAttribute
struct PreventReadOnlyInstanceModificationAttribute_t7FBCFCBA855C80F9E87486C8A6B4DDBA47B78415  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.PropertyAttribute
struct PropertyAttribute_t5E0CB5A6CDA6E24CBD4FF26DE3B0C29D8BB54BF0  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// DG.Tweening.CustomPlugins.PureQuaternionPlugin
struct PureQuaternionPlugin_t657F176265AD5B16DAC19DB4055CED610D0E7728  : public ABSTweenPlugin_3_t56BEDD6B006DC2E8D499101DE8A2339425AE6A10
{
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

// DG.Tweening.Plugins.QuaternionPlugin
struct QuaternionPlugin_t1A56047AD40DAFAADC979A7D1060705C6B452494  : public ABSTweenPlugin_3_t3823C0F935A3168B9E48DC90ABD9A0CED3D7BB82
{
};

// UnityEngine.SocialPlatforms.Range
struct Range_tDDBAD7CBDC5DD273DA4330F4E9CDF24C62F16ED6 
{
	// System.Int32 UnityEngine.SocialPlatforms.Range::from
	int32_t ___from_0;
	// System.Int32 UnityEngine.SocialPlatforms.Range::count
	int32_t ___count_1;
};

// UnityEngine.Rect
struct Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D 
{
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;
};

// DG.Tweening.Plugins.RectOffsetPlugin
struct RectOffsetPlugin_tACF5D3CEB5F225A9D3A1DFDBAD171F71B674E845  : public ABSTweenPlugin_3_tEDADD90C5C3ECECC20453D4CCA444C32237B4DA6
{
};

// DG.Tweening.Plugins.Options.RectOptions
struct RectOptions_tCE72F3B850FF80059E448A0ED2AA3FF16EE5EDAC 
{
	// System.Boolean DG.Tweening.Plugins.Options.RectOptions::snapping
	bool ___snapping_0;
};
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.RectOptions
struct RectOptions_tCE72F3B850FF80059E448A0ED2AA3FF16EE5EDAC_marshaled_pinvoke
{
	int32_t ___snapping_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.RectOptions
struct RectOptions_tCE72F3B850FF80059E448A0ED2AA3FF16EE5EDAC_marshaled_com
{
	int32_t ___snapping_0;
};

// DG.Tweening.Plugins.RectPlugin
struct RectPlugin_t798CAB602477A5FEC0C26CAF494979B1F2803E8D  : public ABSTweenPlugin_3_tA51581359A1B697A7A0A8F2FFDE00794FA1E67F4
{
};

// UnityEngine.RenderTextureDescriptor
struct RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 
{
	// System.Int32 UnityEngine.RenderTextureDescriptor::<width>k__BackingField
	int32_t ___U3CwidthU3Ek__BackingField_0;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<height>k__BackingField
	int32_t ___U3CheightU3Ek__BackingField_1;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<msaaSamples>k__BackingField
	int32_t ___U3CmsaaSamplesU3Ek__BackingField_2;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<volumeDepth>k__BackingField
	int32_t ___U3CvolumeDepthU3Ek__BackingField_3;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<mipCount>k__BackingField
	int32_t ___U3CmipCountU3Ek__BackingField_4;
	// UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTextureDescriptor::_graphicsFormat
	int32_t ____graphicsFormat_5;
	// UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTextureDescriptor::<stencilFormat>k__BackingField
	int32_t ___U3CstencilFormatU3Ek__BackingField_6;
	// UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTextureDescriptor::<depthStencilFormat>k__BackingField
	int32_t ___U3CdepthStencilFormatU3Ek__BackingField_7;
	// UnityEngine.Rendering.TextureDimension UnityEngine.RenderTextureDescriptor::<dimension>k__BackingField
	int32_t ___U3CdimensionU3Ek__BackingField_8;
	// UnityEngine.Rendering.ShadowSamplingMode UnityEngine.RenderTextureDescriptor::<shadowSamplingMode>k__BackingField
	int32_t ___U3CshadowSamplingModeU3Ek__BackingField_9;
	// UnityEngine.VRTextureUsage UnityEngine.RenderTextureDescriptor::<vrUsage>k__BackingField
	int32_t ___U3CvrUsageU3Ek__BackingField_10;
	// UnityEngine.RenderTextureCreationFlags UnityEngine.RenderTextureDescriptor::_flags
	int32_t ____flags_11;
	// UnityEngine.RenderTextureMemoryless UnityEngine.RenderTextureDescriptor::<memoryless>k__BackingField
	int32_t ___U3CmemorylessU3Ek__BackingField_12;
};

// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct RequiredByNativeCodeAttribute_t86B11F2BA12BB463CE3258E64E16B43484014FCA  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Scripting.RequiredByNativeCodeAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Scripting.RequiredByNativeCodeAttribute::<Optional>k__BackingField
	bool ___U3COptionalU3Ek__BackingField_1;
	// System.Boolean UnityEngine.Scripting.RequiredByNativeCodeAttribute::<GenerateProxy>k__BackingField
	bool ___U3CGenerateProxyU3Ek__BackingField_2;
};

// UnityEngine.TextCore.Text.RichTextTagAttribute
struct RichTextTagAttribute_t0600951F833208392F1D8EE3E6A58AD5C797F9DA 
{
	// System.Int32 UnityEngine.TextCore.Text.RichTextTagAttribute::nameHashCode
	int32_t ___nameHashCode_0;
	// System.Int32 UnityEngine.TextCore.Text.RichTextTagAttribute::valueHashCode
	int32_t ___valueHashCode_1;
	// UnityEngine.TextCore.Text.TagValueType UnityEngine.TextCore.Text.RichTextTagAttribute::valueType
	int32_t ___valueType_2;
	// System.Int32 UnityEngine.TextCore.Text.RichTextTagAttribute::valueStartIndex
	int32_t ___valueStartIndex_3;
	// System.Int32 UnityEngine.TextCore.Text.RichTextTagAttribute::valueLength
	int32_t ___valueLength_4;
};

// UnityEngine.Timeline.RuntimeClipBase
struct RuntimeClipBase_t7F5FBDC61E8219FDDF0F9DEB7B6E3864BC57FDD7  : public RuntimeElement_tB6282BAA538AFD75E138BED88A14DFA2665737B2
{
};

// DG.Tweening.Core.SafeModeReport
struct SafeModeReport_t36C3527D96D574F81DB1748D1E856C7A539A25B9 
{
	// System.Int32 DG.Tweening.Core.SafeModeReport::<totMissingTargetOrFieldErrors>k__BackingField
	int32_t ___U3CtotMissingTargetOrFieldErrorsU3Ek__BackingField_0;
	// System.Int32 DG.Tweening.Core.SafeModeReport::<totCallbackErrors>k__BackingField
	int32_t ___U3CtotCallbackErrorsU3Ek__BackingField_1;
	// System.Int32 DG.Tweening.Core.SafeModeReport::<totStartupErrors>k__BackingField
	int32_t ___U3CtotStartupErrorsU3Ek__BackingField_2;
	// System.Int32 DG.Tweening.Core.SafeModeReport::<totUnsetErrors>k__BackingField
	int32_t ___U3CtotUnsetErrorsU3Ek__BackingField_3;
};

// DG.Tweening.Core.SequenceCallback
struct SequenceCallback_tF04229A385B0898870CB2A68EB10408A723489E8  : public ABSSequentiable_t05DF85FC63E3650D2D4CF6ABBA0F43263EB8CE89
{
};

// UnityEngine.SharedBetweenAnimatorsAttribute
struct SharedBetweenAnimatorsAttribute_t44FFD5D3B5AEBB394182D66E2198FA398087449C  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.TextCore.Text.SpriteCharacter
struct SpriteCharacter_tB3516A25DBFA0AD68DD8E1432752D503FD1F40F5  : public TextElement_tCEF567A8810788262275B39DC39CBA6EBE7472DA
{
	// System.String UnityEngine.TextCore.Text.SpriteCharacter::m_Name
	String_t* ___m_Name_6;
	// System.Int32 UnityEngine.TextCore.Text.SpriteCharacter::m_HashCode
	int32_t ___m_HashCode_7;
};

// UnityEngine.Bindings.StaticAccessorAttribute
struct StaticAccessorAttribute_tDE194716AED7A414D473DC570B2E0035A5CE130A  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.StaticAccessorAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// UnityEngine.Bindings.StaticAccessorType UnityEngine.Bindings.StaticAccessorAttribute::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_1;
};

// DG.Tweening.Plugins.Options.StringOptions
struct StringOptions_tC70D70DB6854CE62E6BBC3AA066517835919E9FA 
{
	// System.Boolean DG.Tweening.Plugins.Options.StringOptions::richTextEnabled
	bool ___richTextEnabled_0;
	// DG.Tweening.ScrambleMode DG.Tweening.Plugins.Options.StringOptions::scrambleMode
	int32_t ___scrambleMode_1;
	// System.Char[] DG.Tweening.Plugins.Options.StringOptions::scrambledChars
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___scrambledChars_2;
	// System.Int32 DG.Tweening.Plugins.Options.StringOptions::startValueStrippedLength
	int32_t ___startValueStrippedLength_3;
	// System.Int32 DG.Tweening.Plugins.Options.StringOptions::changeValueStrippedLength
	int32_t ___changeValueStrippedLength_4;
};
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.StringOptions
struct StringOptions_tC70D70DB6854CE62E6BBC3AA066517835919E9FA_marshaled_pinvoke
{
	int32_t ___richTextEnabled_0;
	int32_t ___scrambleMode_1;
	uint8_t* ___scrambledChars_2;
	int32_t ___startValueStrippedLength_3;
	int32_t ___changeValueStrippedLength_4;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.StringOptions
struct StringOptions_tC70D70DB6854CE62E6BBC3AA066517835919E9FA_marshaled_com
{
	int32_t ___richTextEnabled_0;
	int32_t ___scrambleMode_1;
	uint8_t* ___scrambledChars_2;
	int32_t ___startValueStrippedLength_3;
	int32_t ___changeValueStrippedLength_4;
};

// DG.Tweening.Plugins.StringPlugin
struct StringPlugin_tFB74E02BC676FC582B73F3505B2D35B9C66FE603  : public ABSTweenPlugin_3_tE7EF56C4695649256BA2077D8E447E739C869256
{
};

// UnityEngine.Timeline.SupportsChildTracksAttribute
struct SupportsChildTracksAttribute_tAA48787F5A6F2DDBC4FFB428C54C57CA21EF30E8  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Type UnityEngine.Timeline.SupportsChildTracksAttribute::childType
	Type_t* ___childType_0;
	// System.Int32 UnityEngine.Timeline.SupportsChildTracksAttribute::levels
	int32_t ___levels_1;
};

// UnityEngine.ThreadAndSerializationSafeAttribute
struct ThreadAndSerializationSafeAttribute_t819C12E8106F42E7493B11DDA93C36F6FB864357  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.Timeline.TimelinePlayable
struct TimelinePlayable_t01A93A4FCD1222A9B8A7BE65E5FEB187D8D99BBD  : public PlayableBehaviour_tCCFE023F2CAB1769A3EAB176BD5B0416C54C22E2
{
	// UnityEngine.Timeline.IntervalTree`1<UnityEngine.Timeline.RuntimeElement> UnityEngine.Timeline.TimelinePlayable::m_IntervalTree
	IntervalTree_1_t56942FFEDE9739CBDEB79B0B42BE1F89DA07314B* ___m_IntervalTree_0;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.RuntimeElement> UnityEngine.Timeline.TimelinePlayable::m_ActiveClips
	List_1_t10B46D1B2917EFB9569F790FA432CF6C1C534BEF* ___m_ActiveClips_1;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.RuntimeElement> UnityEngine.Timeline.TimelinePlayable::m_CurrentListOfActiveClips
	List_1_t10B46D1B2917EFB9569F790FA432CF6C1C534BEF* ___m_CurrentListOfActiveClips_2;
	// System.Int32 UnityEngine.Timeline.TimelinePlayable::m_ActiveBit
	int32_t ___m_ActiveBit_3;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.ITimelineEvaluateCallback> UnityEngine.Timeline.TimelinePlayable::m_EvaluateCallbacks
	List_1_t66D70AC14455298A9DB791AD2621759C701785A9* ___m_EvaluateCallbacks_4;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Timeline.TrackAsset,UnityEngine.Playables.Playable> UnityEngine.Timeline.TimelinePlayable::m_PlayableCache
	Dictionary_2_t7F8A452CAC89438BACFC42CC989FA36151E8577E* ___m_PlayableCache_5;
};

// UnityEngine.Timeline.TrackBindingTypeAttribute
struct TrackBindingTypeAttribute_t7A2B3C3F8994B073A3354E00CAEF06FC1D7721E2  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Type UnityEngine.Timeline.TrackBindingTypeAttribute::type
	Type_t* ___type_0;
	// UnityEngine.Timeline.TrackBindingFlags UnityEngine.Timeline.TrackBindingTypeAttribute::flags
	int32_t ___flags_1;
};

// UnityEngine.Timeline.TrackClipTypeAttribute
struct TrackClipTypeAttribute_tCB240AD2A704583CF65CFEEBC7B9F71A1148B6E7  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Type UnityEngine.Timeline.TrackClipTypeAttribute::inspectedType
	Type_t* ___inspectedType_0;
	// System.Boolean UnityEngine.Timeline.TrackClipTypeAttribute::allowAutoCreate
	bool ___allowAutoCreate_1;
};

// DG.Tweening.Tween
struct Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C  : public ABSSequentiable_t05DF85FC63E3650D2D4CF6ABBA0F43263EB8CE89
{
	// System.Single DG.Tweening.Tween::timeScale
	float ___timeScale_4;
	// System.Boolean DG.Tweening.Tween::isBackwards
	bool ___isBackwards_5;
	// System.Boolean DG.Tweening.Tween::isInverted
	bool ___isInverted_6;
	// System.Object DG.Tweening.Tween::id
	RuntimeObject* ___id_7;
	// System.String DG.Tweening.Tween::stringId
	String_t* ___stringId_8;
	// System.Int32 DG.Tweening.Tween::intId
	int32_t ___intId_9;
	// System.Object DG.Tweening.Tween::target
	RuntimeObject* ___target_10;
	// DG.Tweening.UpdateType DG.Tweening.Tween::updateType
	int32_t ___updateType_11;
	// System.Boolean DG.Tweening.Tween::isIndependentUpdate
	bool ___isIndependentUpdate_12;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPlay
	TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24* ___onPlay_13;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPause
	TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24* ___onPause_14;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onRewind
	TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24* ___onRewind_15;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onUpdate
	TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24* ___onUpdate_16;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onStepComplete
	TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24* ___onStepComplete_17;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onComplete
	TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24* ___onComplete_18;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onKill
	TweenCallback_t7C8B8A38E7B30905FF1B83C943256EF23617BB24* ___onKill_19;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.Tween::onWaypointChange
	TweenCallback_1_tF0ADCA0C226C9C243ACB55E67D852E4BB53AEB67* ___onWaypointChange_20;
	// System.Boolean DG.Tweening.Tween::isFrom
	bool ___isFrom_21;
	// System.Boolean DG.Tweening.Tween::isBlendable
	bool ___isBlendable_22;
	// System.Boolean DG.Tweening.Tween::isRecyclable
	bool ___isRecyclable_23;
	// System.Boolean DG.Tweening.Tween::isSpeedBased
	bool ___isSpeedBased_24;
	// System.Boolean DG.Tweening.Tween::autoKill
	bool ___autoKill_25;
	// System.Single DG.Tweening.Tween::duration
	float ___duration_26;
	// System.Int32 DG.Tweening.Tween::loops
	int32_t ___loops_27;
	// DG.Tweening.LoopType DG.Tweening.Tween::loopType
	int32_t ___loopType_28;
	// System.Single DG.Tweening.Tween::delay
	float ___delay_29;
	// System.Boolean DG.Tweening.Tween::<isRelative>k__BackingField
	bool ___U3CisRelativeU3Ek__BackingField_30;
	// DG.Tweening.Ease DG.Tweening.Tween::easeType
	int32_t ___easeType_31;
	// DG.Tweening.EaseFunction DG.Tweening.Tween::customEase
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___customEase_32;
	// System.Single DG.Tweening.Tween::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_33;
	// System.Single DG.Tweening.Tween::easePeriod
	float ___easePeriod_34;
	// System.String DG.Tweening.Tween::debugTargetId
	String_t* ___debugTargetId_35;
	// System.Type DG.Tweening.Tween::typeofT1
	Type_t* ___typeofT1_36;
	// System.Type DG.Tweening.Tween::typeofT2
	Type_t* ___typeofT2_37;
	// System.Type DG.Tweening.Tween::typeofTPlugOptions
	Type_t* ___typeofTPlugOptions_38;
	// System.Boolean DG.Tweening.Tween::<active>k__BackingField
	bool ___U3CactiveU3Ek__BackingField_39;
	// System.Boolean DG.Tweening.Tween::isSequenced
	bool ___isSequenced_40;
	// DG.Tweening.Sequence DG.Tweening.Tween::sequenceParent
	Sequence_tEADBE56D6ED2E9EE8FB2E5459C3E57131EC0545C* ___sequenceParent_41;
	// System.Int32 DG.Tweening.Tween::activeId
	int32_t ___activeId_42;
	// DG.Tweening.Core.Enums.SpecialStartupMode DG.Tweening.Tween::specialStartupMode
	int32_t ___specialStartupMode_43;
	// System.Boolean DG.Tweening.Tween::creationLocked
	bool ___creationLocked_44;
	// System.Boolean DG.Tweening.Tween::startupDone
	bool ___startupDone_45;
	// System.Boolean DG.Tweening.Tween::<playedOnce>k__BackingField
	bool ___U3CplayedOnceU3Ek__BackingField_46;
	// System.Single DG.Tweening.Tween::<position>k__BackingField
	float ___U3CpositionU3Ek__BackingField_47;
	// System.Single DG.Tweening.Tween::fullDuration
	float ___fullDuration_48;
	// System.Int32 DG.Tweening.Tween::completedLoops
	int32_t ___completedLoops_49;
	// System.Boolean DG.Tweening.Tween::isPlaying
	bool ___isPlaying_50;
	// System.Boolean DG.Tweening.Tween::isComplete
	bool ___isComplete_51;
	// System.Single DG.Tweening.Tween::elapsedDelay
	float ___elapsedDelay_52;
	// System.Boolean DG.Tweening.Tween::delayComplete
	bool ___delayComplete_53;
	// System.Int32 DG.Tweening.Tween::miscInt
	int32_t ___miscInt_54;
};

// UnityEngine.UILineInfo
struct UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC 
{
	// System.Int32 UnityEngine.UILineInfo::startCharIdx
	int32_t ___startCharIdx_0;
	// System.Int32 UnityEngine.UILineInfo::height
	int32_t ___height_1;
	// System.Single UnityEngine.UILineInfo::topY
	float ___topY_2;
	// System.Single UnityEngine.UILineInfo::leading
	float ___leading_3;
};

// System.UInt32
struct UInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B 
{
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;
};

// System.UIntPtr
struct UIntPtr_t 
{
	// System.Void* System.UIntPtr::_pointer
	void* ____pointer_1;
};

// DG.Tweening.Plugins.Options.UintOptions
struct UintOptions_t06BB035A1F0801FF6D4721F6F98F36DDD93E53A3 
{
	// System.Boolean DG.Tweening.Plugins.Options.UintOptions::isNegativeChangeValue
	bool ___isNegativeChangeValue_0;
};
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.UintOptions
struct UintOptions_t06BB035A1F0801FF6D4721F6F98F36DDD93E53A3_marshaled_pinvoke
{
	int32_t ___isNegativeChangeValue_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.UintOptions
struct UintOptions_t06BB035A1F0801FF6D4721F6F98F36DDD93E53A3_marshaled_com
{
	int32_t ___isNegativeChangeValue_0;
};

// DG.Tweening.Plugins.UintPlugin
struct UintPlugin_t8AD83D61CE5C42CC008163B8DB1AF61837487931  : public ABSTweenPlugin_3_t76C40FFA153363CF88C31D954E2AE740916B5B22
{
};

// DG.Tweening.Plugins.UlongPlugin
struct UlongPlugin_t201AE77FD5B0C39AD2EE340B411C70A2019A8A6E  : public ABSTweenPlugin_3_t303ADD886C8A7272DE43A8D065EEB864F5E03BF4
{
};

// System.Linq.Expressions.UnaryExpression
struct UnaryExpression_tFB4F40A211A2FF9B58F1A86E0EDB474121867B96  : public Expression_t70AA908ECBD33E94249BF235E4EBB0F831AD8785
{
	// System.Linq.Expressions.Expression System.Linq.Expressions.UnaryExpression::<Operand>k__BackingField
	Expression_t70AA908ECBD33E94249BF235E4EBB0F831AD8785* ___U3COperandU3Ek__BackingField_2;
};

// UnityEngine.UnityEngineModuleAssembly
struct UnityEngineModuleAssembly_tB6587DA5BA2569921894019758C4D69095012710  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.Bindings.UnmarshalledAttribute
struct UnmarshalledAttribute_t3D645C3393EF99EED2893026413D4F5B489CD13B  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct UsedByNativeCodeAttribute_t3FE9A7CDCC6A3A4122D8BF44F1D0A37BB38894C1  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Scripting.UsedByNativeCodeAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

// DG.Tweening.Plugins.Vector2Plugin
struct Vector2Plugin_t49FFF2365699F66F30188BB37F03AA461129DD2D  : public ABSTweenPlugin_3_t4082710A2BB933E2D055E454B3EFAC4C0A319444
{
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

// DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct Vector3ArrayOptions_t0F50A7A54A860E4604486511E285C952557C6644 
{
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.Vector3ArrayOptions::axisConstraint
	int32_t ___axisConstraint_0;
	// System.Boolean DG.Tweening.Plugins.Options.Vector3ArrayOptions::snapping
	bool ___snapping_1;
	// System.Single[] DG.Tweening.Plugins.Options.Vector3ArrayOptions::durations
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___durations_2;
};
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct Vector3ArrayOptions_t0F50A7A54A860E4604486511E285C952557C6644_marshaled_pinvoke
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
	Il2CppSafeArray/*NONE*/* ___durations_2;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct Vector3ArrayOptions_t0F50A7A54A860E4604486511E285C952557C6644_marshaled_com
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
	Il2CppSafeArray/*NONE*/* ___durations_2;
};

// DG.Tweening.Plugins.Vector3ArrayPlugin
struct Vector3ArrayPlugin_tF94D793B537B01F6CB2ECC6E51F85705FF224A8F  : public ABSTweenPlugin_3_tAD778405CFD8B0E13EF7423622294E3E3913479B
{
};

// DG.Tweening.Plugins.Vector3Plugin
struct Vector3Plugin_t8DB704648AD26DB2C1B603081274AF3225B8BFAD  : public ABSTweenPlugin_3_tE5A78BE46D046C07A6356B8AB596B2D00F9295E7
{
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

// DG.Tweening.Plugins.Vector4Plugin
struct Vector4Plugin_tF067D35176F54CB1A4BE2D7C2DE7229C3DB22CE5  : public ABSTweenPlugin_3_t9F8F8099624B165B75CB1E0C53FA9762CB2815FC
{
};

// DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t2814CC842518C92C9DFC5DE6F7A73824758D3EF9 
{
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.VectorOptions::axisConstraint
	int32_t ___axisConstraint_0;
	// System.Boolean DG.Tweening.Plugins.Options.VectorOptions::snapping
	bool ___snapping_1;
};
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t2814CC842518C92C9DFC5DE6F7A73824758D3EF9_marshaled_pinvoke
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t2814CC842518C92C9DFC5DE6F7A73824758D3EF9_marshaled_com
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};

// UnityEngine.Bindings.VisibleToOtherModulesAttribute
struct VisibleToOtherModulesAttribute_tE7803AC6A0462A18B7EEF17C4A1036DEE993B489  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// UnityEngine.TextCore.Text.WordInfo
struct WordInfo_tA466206097891A5A2590896EE164AFC406EB060D 
{
	// System.Int32 UnityEngine.TextCore.Text.WordInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_0;
	// System.Int32 UnityEngine.TextCore.Text.WordInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_1;
	// System.Int32 UnityEngine.TextCore.Text.WordInfo::characterCount
	int32_t ___characterCount_2;
};

// UnityEngine.WritableAttribute
struct WritableAttribute_t7D85DADDFD6751C94E2E9594E562AD281A3B6E7B  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.TextCore.Text.XmlTagAttribute
struct XmlTagAttribute_t2F0EE4823C3BB8B9ED6844183CA998A44EA1AB59 
{
	// System.Int32 UnityEngine.TextCore.Text.XmlTagAttribute::nameHashCode
	int32_t ___nameHashCode_0;
	// UnityEngine.TextCore.Text.TagValueType UnityEngine.TextCore.Text.XmlTagAttribute::valueType
	int32_t ___valueType_1;
	// System.Int32 UnityEngine.TextCore.Text.XmlTagAttribute::valueStartIndex
	int32_t ___valueStartIndex_2;
	// System.Int32 UnityEngine.TextCore.Text.XmlTagAttribute::valueLength
	int32_t ___valueLength_3;
	// System.Int32 UnityEngine.TextCore.Text.XmlTagAttribute::valueHashCode
	int32_t ___valueHashCode_4;
};

// UnityEngine.Yoga.YogaSize
struct YogaSize_tA276812CB1E90E7AA2028A9474EA6EA46B3B38EA 
{
	// System.Single UnityEngine.Yoga.YogaSize::width
	float ___width_0;
	// System.Single UnityEngine.Yoga.YogaSize::height
	float ___height_1;
};

// UnityEngine.Yoga.YogaValue
struct YogaValue_t9066126971BFC18D9B4A8AB11435557F19598F8C 
{
	// System.Single UnityEngine.Yoga.YogaValue::value
	float ___value_0;
	// UnityEngine.Yoga.YogaUnit UnityEngine.Yoga.YogaValue::unit
	int32_t ___unit_1;
};

// Unity.Mathematics.float2
struct float2_t24AA5C0F612B0672315EDAFEC9D9E7F1C4A5B0BA 
{
	// System.Single Unity.Mathematics.float2::x
	float ___x_0;
	// System.Single Unity.Mathematics.float2::y
	float ___y_1;
};

// Unity.Mathematics.float3
struct float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E 
{
	// System.Single Unity.Mathematics.float3::x
	float ___x_0;
	// System.Single Unity.Mathematics.float3::y
	float ___y_1;
	// System.Single Unity.Mathematics.float3::z
	float ___z_2;
};

// Unity.Mathematics.float4
struct float4_t89D9A294E7A79BD81BFBDD18654508532958555E 
{
	// System.Single Unity.Mathematics.float4::x
	float ___x_0;
	// System.Single Unity.Mathematics.float4::y
	float ___y_1;
	// System.Single Unity.Mathematics.float4::z
	float ___z_2;
	// System.Single Unity.Mathematics.float4::w
	float ___w_3;
};

// Unity.Mathematics.half
struct half_tE8A6792149696F478D662DD4B736964C0FF018AF 
{
	// System.UInt16 Unity.Mathematics.half::value
	uint16_t ___value_0;
};

// Unity.Mathematics.int2
struct int2_tF4AC25F87943DC0B2BB3456B0B919B3B42A9432A 
{
	// System.Int32 Unity.Mathematics.int2::x
	int32_t ___x_0;
	// System.Int32 Unity.Mathematics.int2::y
	int32_t ___y_1;
};

// Unity.Mathematics.uint2
struct uint2_t157753816C23B82EB918C3D3AFCFDDE06A04C05F 
{
	// System.UInt32 Unity.Mathematics.uint2::x
	uint32_t ___x_0;
	// System.UInt32 Unity.Mathematics.uint2::y
	uint32_t ___y_1;
};

// Unity.Mathematics.uint3
struct uint3_tC1C1C817DB46ED2E6A6C7390716FDDD565917F7B 
{
	// System.UInt32 Unity.Mathematics.uint3::x
	uint32_t ___x_0;
	// System.UInt32 Unity.Mathematics.uint3::y
	uint32_t ___y_1;
	// System.UInt32 Unity.Mathematics.uint3::z
	uint32_t ___z_2;
};

// Unity.Mathematics.uint4
struct uint4_t6C69CBFAE9BF0F727D52B68779D4A3F0DBA8D5C9 
{
	// System.UInt32 Unity.Mathematics.uint4::x
	uint32_t ___x_0;
	// System.UInt32 Unity.Mathematics.uint4::y
	uint32_t ___y_1;
	// System.UInt32 Unity.Mathematics.uint4::z
	uint32_t ___z_2;
	// System.UInt32 Unity.Mathematics.uint4::w
	uint32_t ___w_3;
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120
struct __StaticArrayInitTypeSizeU3D120_t782DC2F9FE322240B16562C43775CC0C2E85C81F 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_t782DC2F9FE322240B16562C43775CC0C2E85C81F__padding[120];
	};
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20
struct __StaticArrayInitTypeSizeU3D20_t2BA535C65B6076A7713D41FD8D81F2C4C9018986 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_t2BA535C65B6076A7713D41FD8D81F2C4C9018986__padding[20];
	};
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=50
struct __StaticArrayInitTypeSizeU3D50_t10CD55D757224F40CC808D5B7F260E34FD4B2E0F 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D50_t10CD55D757224F40CC808D5B7F260E34FD4B2E0F__padding[50];
	};
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256
struct __StaticArrayInitTypeSizeU3D256_tFFE4CE163BD2DCEAA09662C2BCC33B3C37AB0D22 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D256_tFFE4CE163BD2DCEAA09662C2BCC33B3C37AB0D22__padding[256];
	};
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3
struct __StaticArrayInitTypeSizeU3D3_t2857C07F0A23FB025DA0D81FCD2BE07B4ADCC026 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D3_t2857C07F0A23FB025DA0D81FCD2BE07B4ADCC026__padding[3];
	};
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32
struct __StaticArrayInitTypeSizeU3D32_tF5E240ACF4B30B5A5F8C77E9E49CC2F8559D76D9 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_tF5E240ACF4B30B5A5F8C77E9E49CC2F8559D76D9__padding[32];
	};
};

// Unity.Burst.BurstRuntime/PreserveAttribute
struct PreserveAttribute_tA1799B67558808CC16DE11D04CC1D42AAA569133  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// Unity.Burst.BurstString/FormatOptions
struct FormatOptions_tBD49C0C9CC14282D1249620565FC537D4D4AFB84 
{
	// Unity.Burst.BurstString/NumberFormatKind Unity.Burst.BurstString/FormatOptions::Kind
	uint8_t ___Kind_0;
	// System.SByte Unity.Burst.BurstString/FormatOptions::AlignAndSize
	int8_t ___AlignAndSize_1;
	// System.Byte Unity.Burst.BurstString/FormatOptions::Specifier
	uint8_t ___Specifier_2;
	// System.Boolean Unity.Burst.BurstString/FormatOptions::Lowercase
	bool ___Lowercase_3;
};
// Native definition for P/Invoke marshalling of Unity.Burst.BurstString/FormatOptions
struct FormatOptions_tBD49C0C9CC14282D1249620565FC537D4D4AFB84_marshaled_pinvoke
{
	uint8_t ___Kind_0;
	int8_t ___AlignAndSize_1;
	uint8_t ___Specifier_2;
	int32_t ___Lowercase_3;
};
// Native definition for COM marshalling of Unity.Burst.BurstString/FormatOptions
struct FormatOptions_tBD49C0C9CC14282D1249620565FC537D4D4AFB84_marshaled_com
{
	uint8_t ___Kind_0;
	int8_t ___AlignAndSize_1;
	uint8_t ___Specifier_2;
	int32_t ___Lowercase_3;
};

// Unity.Burst.BurstString/NumberBuffer
struct NumberBuffer_tF09E8463D840202ECA50F50BE6D57729C18213B4 
{
	// System.Byte* Unity.Burst.BurstString/NumberBuffer::_buffer
	uint8_t* ____buffer_0;
	// Unity.Burst.BurstString/NumberBufferKind Unity.Burst.BurstString/NumberBuffer::Kind
	int32_t ___Kind_1;
	// System.Int32 Unity.Burst.BurstString/NumberBuffer::DigitsCount
	int32_t ___DigitsCount_2;
	// System.Int32 Unity.Burst.BurstString/NumberBuffer::Scale
	int32_t ___Scale_3;
	// System.Boolean Unity.Burst.BurstString/NumberBuffer::IsNegative
	bool ___IsNegative_4;
};
// Native definition for P/Invoke marshalling of Unity.Burst.BurstString/NumberBuffer
struct NumberBuffer_tF09E8463D840202ECA50F50BE6D57729C18213B4_marshaled_pinvoke
{
	uint8_t* ____buffer_0;
	int32_t ___Kind_1;
	int32_t ___DigitsCount_2;
	int32_t ___Scale_3;
	int32_t ___IsNegative_4;
};
// Native definition for COM marshalling of Unity.Burst.BurstString/NumberBuffer
struct NumberBuffer_tF09E8463D840202ECA50F50BE6D57729C18213B4_marshaled_com
{
	uint8_t* ____buffer_0;
	int32_t ___Kind_1;
	int32_t ___DigitsCount_2;
	int32_t ___Scale_3;
	int32_t ___IsNegative_4;
};

// Unity.Burst.BurstString/PreserveAttribute
struct PreserveAttribute_t54BBA699FC0C1DD99BED77D21CADC33A352E1999  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// Unity.Burst.BurstString/tFloatUnion32
struct tFloatUnion32_t1140001CA96F869F598FBC16C082BC2BA85AB2CA 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Single Unity.Burst.BurstString/tFloatUnion32::m_floatingPoint
			float ___m_floatingPoint_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___m_floatingPoint_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt32 Unity.Burst.BurstString/tFloatUnion32::m_integer
			uint32_t ___m_integer_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t ___m_integer_1_forAlignmentOnly;
		};
	};
};

// Unity.Burst.BurstString/tFloatUnion64
struct tFloatUnion64_t737111FBE1FD2D4509E72C45FE6389106B60B2FC 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Double Unity.Burst.BurstString/tFloatUnion64::m_floatingPoint
			double ___m_floatingPoint_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			double ___m_floatingPoint_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt64 Unity.Burst.BurstString/tFloatUnion64::m_integer
			uint64_t ___m_integer_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint64_t ___m_integer_1_forAlignmentOnly;
		};
	};
};

// DG.Tweening.DOTweenCYInstruction/WaitForCompletion
struct WaitForCompletion_tC84400E0FA4E28B95AA56DF28973D5FFDA16AFA0  : public CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617
{
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForCompletion::ciz
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___ciz_0;
};

// DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops
struct WaitForElapsedLoops_t24C0691B408798B4BE5CCC92DC8B4D40430717BC  : public CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617
{
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::cjc
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___cjc_0;
	// System.Int32 DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::cjd
	int32_t ___cjd_1;
};

// DG.Tweening.DOTweenCYInstruction/WaitForKill
struct WaitForKill_t532BDFE32D7C3892E01BF80054F95A9A5C1C24DE  : public CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617
{
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForKill::cjb
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___cjb_0;
};

// DG.Tweening.DOTweenCYInstruction/WaitForPosition
struct WaitForPosition_t302B4F4C6FC89426E08DDC65543F45785B20B84B  : public CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617
{
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForPosition::cje
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___cje_0;
	// System.Single DG.Tweening.DOTweenCYInstruction/WaitForPosition::cjf
	float ___cjf_1;
};

// DG.Tweening.DOTweenCYInstruction/WaitForRewind
struct WaitForRewind_t2ABB006386A81D361C36B476044786442726743D  : public CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617
{
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForRewind::cja
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___cja_0;
};

// DG.Tweening.DOTweenCYInstruction/WaitForStart
struct WaitForStart_t4448F8E46F59EE599CA8DCEF52FC706221093F30  : public CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617
{
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForStart::cjg
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___cjg_0;
};

// UnityEngine.GUIClip/ParentClipScope
struct ParentClipScope_tDAB1300C623213518730D926A970098BECFD9C52 
{
	// System.Boolean UnityEngine.GUIClip/ParentClipScope::m_Disposed
	bool ___m_Disposed_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.GUIClip/ParentClipScope
struct ParentClipScope_tDAB1300C623213518730D926A970098BECFD9C52_marshaled_pinvoke
{
	int32_t ___m_Disposed_0;
};
// Native definition for COM marshalling of UnityEngine.GUIClip/ParentClipScope
struct ParentClipScope_tDAB1300C623213518730D926A970098BECFD9C52_marshaled_com
{
	int32_t ___m_Disposed_0;
};

// UnityEngine.ParticleSystem/CustomDataModule
struct CustomDataModule_t44BDC4DC2B7E66B5298D766D86667C063E8595C4 
{
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/CustomDataModule::m_ParticleSystem
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___m_ParticleSystem_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/CustomDataModule
struct CustomDataModule_t44BDC4DC2B7E66B5298D766D86667C063E8595C4_marshaled_pinvoke
{
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/CustomDataModule
struct CustomDataModule_t44BDC4DC2B7E66B5298D766D86667C063E8595C4_marshaled_com
{
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/MainModule
struct MainModule_tC7ECD8330C14B0808478A748048988A6085CE2A9 
{
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/MainModule::m_ParticleSystem
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___m_ParticleSystem_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_tC7ECD8330C14B0808478A748048988A6085CE2A9_marshaled_pinvoke
{
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_tC7ECD8330C14B0808478A748048988A6085CE2A9_marshaled_com
{
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/MinMaxCurve
struct MinMaxCurve_t812D571297EED6498776CC58949A42B172E60E23 
{
	// UnityEngine.ParticleSystemCurveMode UnityEngine.ParticleSystem/MinMaxCurve::m_Mode
	int32_t ___m_Mode_0;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMultiplier
	float ___m_CurveMultiplier_1;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMin
	AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* ___m_CurveMin_2;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMax
	AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* ___m_CurveMax_3;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_ConstantMin
	float ___m_ConstantMin_4;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_ConstantMax
	float ___m_ConstantMax_5;
};

// UnityEngine.ParticleSystem/TextureSheetAnimationModule
struct TextureSheetAnimationModule_tB53F451F252E24ACC3EF80D770DB4FBE1A13D1F6 
{
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/TextureSheetAnimationModule::m_ParticleSystem
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___m_ParticleSystem_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/TextureSheetAnimationModule
struct TextureSheetAnimationModule_tB53F451F252E24ACC3EF80D770DB4FBE1A13D1F6_marshaled_pinvoke
{
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/TextureSheetAnimationModule
struct TextureSheetAnimationModule_tB53F451F252E24ACC3EF80D770DB4FBE1A13D1F6_marshaled_com
{
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___m_ParticleSystem_0;
};

// UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314 
{
	// UnityEngine.GameObject UnityEngine.SendMouseEvents/HitInfo::target
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___target_0;
	// UnityEngine.Camera UnityEngine.SendMouseEvents/HitInfo::camera
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___camera_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314_marshaled_pinvoke
{
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___target_0;
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___camera_1;
};
// Native definition for COM marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314_marshaled_com
{
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___target_0;
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___camera_1;
};

// Unity.Burst.SharedStatic/PreserveAttribute
struct PreserveAttribute_tDEA15EF9DCAB8AC4428ED72A2A1377384FE7C27B  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.TextCore.Text.TextGenerator/SpecialCharacter
struct SpecialCharacter_t869F8BE65A7FE32AFD4196118258F49A63D8E2BD 
{
	// UnityEngine.TextCore.Text.Character UnityEngine.TextCore.Text.TextGenerator/SpecialCharacter::character
	Character_t9B671B493FAC8D43638C69AF6AE92CBD103D80EC* ___character_0;
	// UnityEngine.TextCore.Text.FontAsset UnityEngine.TextCore.Text.TextGenerator/SpecialCharacter::fontAsset
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___fontAsset_1;
	// UnityEngine.Material UnityEngine.TextCore.Text.TextGenerator/SpecialCharacter::material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_2;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator/SpecialCharacter::materialIndex
	int32_t ___materialIndex_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.Text.TextGenerator/SpecialCharacter
struct SpecialCharacter_t869F8BE65A7FE32AFD4196118258F49A63D8E2BD_marshaled_pinvoke
{
	Character_t9B671B493FAC8D43638C69AF6AE92CBD103D80EC* ___character_0;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___fontAsset_1;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_2;
	int32_t ___materialIndex_3;
};
// Native definition for COM marshalling of UnityEngine.TextCore.Text.TextGenerator/SpecialCharacter
struct SpecialCharacter_t869F8BE65A7FE32AFD4196118258F49A63D8E2BD_marshaled_com
{
	Character_t9B671B493FAC8D43638C69AF6AE92CBD103D80EC* ___character_0;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___fontAsset_1;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_2;
	int32_t ___materialIndex_3;
};

// UnityEngine.TextCore.Text.TextResourceManager/FontAssetRef
struct FontAssetRef_t7B8E634754BC5683F1E6601D7CD0061285A28FF3 
{
	// System.Int32 UnityEngine.TextCore.Text.TextResourceManager/FontAssetRef::nameHashCode
	int32_t ___nameHashCode_0;
	// System.Int32 UnityEngine.TextCore.Text.TextResourceManager/FontAssetRef::familyNameHashCode
	int32_t ___familyNameHashCode_1;
	// System.Int32 UnityEngine.TextCore.Text.TextResourceManager/FontAssetRef::styleNameHashCode
	int32_t ___styleNameHashCode_2;
	// System.Int64 UnityEngine.TextCore.Text.TextResourceManager/FontAssetRef::familyNameAndStyleHashCode
	int64_t ___familyNameAndStyleHashCode_3;
	// UnityEngine.TextCore.Text.FontAsset UnityEngine.TextCore.Text.TextResourceManager/FontAssetRef::fontAsset
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___fontAsset_4;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.Text.TextResourceManager/FontAssetRef
struct FontAssetRef_t7B8E634754BC5683F1E6601D7CD0061285A28FF3_marshaled_pinvoke
{
	int32_t ___nameHashCode_0;
	int32_t ___familyNameHashCode_1;
	int32_t ___styleNameHashCode_2;
	int64_t ___familyNameAndStyleHashCode_3;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___fontAsset_4;
};
// Native definition for COM marshalling of UnityEngine.TextCore.Text.TextResourceManager/FontAssetRef
struct FontAssetRef_t7B8E634754BC5683F1E6601D7CD0061285A28FF3_marshaled_com
{
	int32_t ___nameHashCode_0;
	int32_t ___familyNameHashCode_1;
	int32_t ___styleNameHashCode_2;
	int64_t ___familyNameAndStyleHashCode_3;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___fontAsset_4;
};

// UnityEngine.TextCore.Text.TextSettings/FontReferenceMap
struct FontReferenceMap_t1C0CECF3F0F650BE4A881A50A25EFB26965E7831 
{
	// UnityEngine.Font UnityEngine.TextCore.Text.TextSettings/FontReferenceMap::font
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_0;
	// UnityEngine.TextCore.Text.FontAsset UnityEngine.TextCore.Text.TextSettings/FontReferenceMap::fontAsset
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___fontAsset_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.Text.TextSettings/FontReferenceMap
struct FontReferenceMap_t1C0CECF3F0F650BE4A881A50A25EFB26965E7831_marshaled_pinvoke
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_0;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___fontAsset_1;
};
// Native definition for COM marshalling of UnityEngine.TextCore.Text.TextSettings/FontReferenceMap
struct FontReferenceMap_t1C0CECF3F0F650BE4A881A50A25EFB26965E7831_marshaled_com
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_0;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___fontAsset_1;
};

// UnityEngine.Timeline.TimeNotificationBehaviour/NotificationEntry
struct NotificationEntry_tBBA39A8ACD63E90360DB0FFC4835E8702DFC2E62 
{
	// System.Double UnityEngine.Timeline.TimeNotificationBehaviour/NotificationEntry::time
	double ___time_0;
	// UnityEngine.Playables.INotification UnityEngine.Timeline.TimeNotificationBehaviour/NotificationEntry::payload
	RuntimeObject* ___payload_1;
	// System.Boolean UnityEngine.Timeline.TimeNotificationBehaviour/NotificationEntry::notificationFired
	bool ___notificationFired_2;
	// UnityEngine.Timeline.NotificationFlags UnityEngine.Timeline.TimeNotificationBehaviour/NotificationEntry::flags
	int16_t ___flags_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.Timeline.TimeNotificationBehaviour/NotificationEntry
struct NotificationEntry_tBBA39A8ACD63E90360DB0FFC4835E8702DFC2E62_marshaled_pinvoke
{
	double ___time_0;
	RuntimeObject* ___payload_1;
	int32_t ___notificationFired_2;
	int16_t ___flags_3;
};
// Native definition for COM marshalling of UnityEngine.Timeline.TimeNotificationBehaviour/NotificationEntry
struct NotificationEntry_tBBA39A8ACD63E90360DB0FFC4835E8702DFC2E62_marshaled_com
{
	double ___time_0;
	RuntimeObject* ___payload_1;
	int32_t ___notificationFired_2;
	int16_t ___flags_3;
};

// UnityEngine.Timeline.TrackAsset/TransientBuildData
struct TransientBuildData_t3BE8EF6B5113561AEE7D53FDF3DB331D39BE194F 
{
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TrackAsset/TransientBuildData::trackList
	List_1_t6908BEEFB57470CB30420983896AA06BFB8796F0* ___trackList_0;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset/TransientBuildData::clipList
	List_1_tD78196B4DE777C4B74ADAD24051A9978F5191506* ___clipList_1;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.IMarker> UnityEngine.Timeline.TrackAsset/TransientBuildData::markerList
	List_1_tB481045C42962DD282E8A89B2AF0246A4042EADF* ___markerList_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.Timeline.TrackAsset/TransientBuildData
struct TransientBuildData_t3BE8EF6B5113561AEE7D53FDF3DB331D39BE194F_marshaled_pinvoke
{
	List_1_t6908BEEFB57470CB30420983896AA06BFB8796F0* ___trackList_0;
	List_1_tD78196B4DE777C4B74ADAD24051A9978F5191506* ___clipList_1;
	List_1_tB481045C42962DD282E8A89B2AF0246A4042EADF* ___markerList_2;
};
// Native definition for COM marshalling of UnityEngine.Timeline.TrackAsset/TransientBuildData
struct TransientBuildData_t3BE8EF6B5113561AEE7D53FDF3DB331D39BE194F_marshaled_com
{
	List_1_t6908BEEFB57470CB30420983896AA06BFB8796F0* ___trackList_0;
	List_1_tD78196B4DE777C4B74ADAD24051A9978F5191506* ___clipList_1;
	List_1_tB481045C42962DD282E8A89B2AF0246A4042EADF* ___markerList_2;
};

// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter
struct YieldAwaiter_t5F0A81DC85227C01FFC38D53139B5C19D920B52A 
{
	union
	{
		struct
		{
		};
		uint8_t YieldAwaiter_t5F0A81DC85227C01FFC38D53139B5C19D920B52A__padding[1];
	};
};

// Unity.Mathematics.math/IntFloatUnion
struct IntFloatUnion_t549256A9DD754252DD18383D9CE7EA55EBBD6D96 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 Unity.Mathematics.math/IntFloatUnion::intValue
			int32_t ___intValue_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___intValue_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Single Unity.Mathematics.math/IntFloatUnion::floatValue
			float ___floatValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___floatValue_1_forAlignmentOnly;
		};
	};
};

// Unity.Burst.BurstString/tBigInt/<m_blocks>e__FixedBuffer
struct U3Cm_blocksU3Ee__FixedBuffer_tBBE20C4EF7009465021F0375E2128D5DCFF59F7C 
{
	union
	{
		struct
		{
			// System.UInt32 Unity.Burst.BurstString/tBigInt/<m_blocks>e__FixedBuffer::FixedElementField
			uint32_t ___FixedElementField_0;
		};
		uint8_t U3Cm_blocksU3Ee__FixedBuffer_tBBE20C4EF7009465021F0375E2128D5DCFF59F7C__padding[140];
	};
};

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult>
struct AsyncTaskMethodBuilder_1_tE88892A6B2F97B5D44B7C3EE2DBEED85743412AC 
{
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_tD5ABB3A2536319A3345B32A5481E37E23DD8CEDF ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_tE41CFF640EB7C045550D9D0D92BE67533B084C17* ___m_task_2;
};

// System.Nullable`1<UnityEngine.Vector3>
struct Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value_1;
};

// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.Color32>
struct TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 
{
	// T[] UnityEngine.TextCore.Text.TextProcessingStack`1::itemStack
	Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___itemStack_0;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::index
	int32_t ___index_1;
	// T UnityEngine.TextCore.Text.TextProcessingStack`1::m_DefaultItem
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_DefaultItem_2;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.TextCore.Text.MaterialReference>
struct TextProcessingStack_1_t0C74606C1B6C7817CA95F0DCA46B219CF6FB35CA 
{
	// T[] UnityEngine.TextCore.Text.TextProcessingStack`1::itemStack
	MaterialReferenceU5BU5D_t4A9B88114E223BD96CE5121053664023CE2DE07E* ___itemStack_0;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::index
	int32_t ___index_1;
	// T UnityEngine.TextCore.Text.TextProcessingStack`1::m_DefaultItem
	MaterialReference_t86DB0799D5C82869D4FF0A4F59624AED6910FD26 ___m_DefaultItem_2;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 UnityEngine.TextCore.Text.TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;
};

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t9A1F8C029FA1B33060F0C3AC9DD83EEA394A7808  : public RuntimeObject
{
};

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t2CADAF0D55AC9D0785A6F7B80D4772CF1220C48F  : public RuntimeObject
{
};

// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t723EE724DCEBFAE9555CDC909FDA84F71EB5719B  : public RuntimeObject
{
	// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::m_Completed
	bool ___m_Completed_0;
	// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::m_Hidden
	bool ___m_Hidden_1;
	// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::m_LastReportedDate
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___m_LastReportedDate_2;
	// System.String UnityEngine.SocialPlatforms.Impl.Achievement::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_3;
	// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::<percentCompleted>k__BackingField
	double ___U3CpercentCompletedU3Ek__BackingField_4;
};

// UnityEngine.Animations.AnimationHumanStream
struct AnimationHumanStream_t31E8EAD3F7C2C29CAE7B4EFB87AA84ECC6DCC6EC 
{
	// System.IntPtr UnityEngine.Animations.AnimationHumanStream::stream
	intptr_t ___stream_0;
};

// UnityEngine.Animations.AnimationStream
struct AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A 
{
	// System.UInt32 UnityEngine.Animations.AnimationStream::m_AnimatorBindingsVersion
	uint32_t ___m_AnimatorBindingsVersion_0;
	// System.IntPtr UnityEngine.Animations.AnimationStream::constant
	intptr_t ___constant_1;
	// System.IntPtr UnityEngine.Animations.AnimationStream::input
	intptr_t ___input_2;
	// System.IntPtr UnityEngine.Animations.AnimationStream::output
	intptr_t ___output_3;
	// System.IntPtr UnityEngine.Animations.AnimationStream::workspace
	intptr_t ___workspace_4;
	// System.IntPtr UnityEngine.Animations.AnimationStream::inputStreamAccessor
	intptr_t ___inputStreamAccessor_5;
	// System.IntPtr UnityEngine.Animations.AnimationStream::animationHandleBinder
	intptr_t ___animationHandleBinder_6;
};

// UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_tE8693FF0E67CDBA52BAFB211BFF1844D076ABAFB* ___m_completeCallback_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};

// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.CertificateHandler::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// DG.Tweening.Plugins.CircleOptions
struct CircleOptions_tB7B463413662FD10ECA7DF439BF0B36AC7685008 
{
	// System.Single DG.Tweening.Plugins.CircleOptions::endValueDegrees
	float ___endValueDegrees_0;
	// System.Boolean DG.Tweening.Plugins.CircleOptions::relativeCenter
	bool ___relativeCenter_1;
	// System.Boolean DG.Tweening.Plugins.CircleOptions::snapping
	bool ___snapping_2;
	// UnityEngine.Vector2 DG.Tweening.Plugins.CircleOptions::center
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___center_3;
	// System.Single DG.Tweening.Plugins.CircleOptions::radius
	float ___radius_4;
	// System.Single DG.Tweening.Plugins.CircleOptions::startValueDegrees
	float ___startValueDegrees_5;
	// System.Boolean DG.Tweening.Plugins.CircleOptions::initialized
	bool ___initialized_6;
};
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.CircleOptions
struct CircleOptions_tB7B463413662FD10ECA7DF439BF0B36AC7685008_marshaled_pinvoke
{
	float ___endValueDegrees_0;
	int32_t ___relativeCenter_1;
	int32_t ___snapping_2;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___center_3;
	float ___radius_4;
	float ___startValueDegrees_5;
	int32_t ___initialized_6;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.CircleOptions
struct CircleOptions_tB7B463413662FD10ECA7DF439BF0B36AC7685008_marshaled_com
{
	float ___endValueDegrees_0;
	int32_t ___relativeCenter_1;
	int32_t ___snapping_2;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___center_3;
	float ___radius_4;
	float ___startValueDegrees_5;
	int32_t ___initialized_6;
};

// UnityEngine.Collision
struct Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0  : public RuntimeObject
{
	// UnityEngine.Vector3 UnityEngine.Collision::m_Impulse
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Impulse_0;
	// UnityEngine.Vector3 UnityEngine.Collision::m_RelativeVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_RelativeVelocity_1;
	// UnityEngine.Component UnityEngine.Collision::m_Body
	Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* ___m_Body_2;
	// UnityEngine.Collider UnityEngine.Collision::m_Collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_3;
	// System.Int32 UnityEngine.Collision::m_ContactCount
	int32_t ___m_ContactCount_4;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_ReusedContacts
	ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* ___m_ReusedContacts_5;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_LegacyContacts
	ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* ___m_LegacyContacts_6;
};
// Native definition for P/Invoke marshalling of UnityEngine.Collision
struct Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0_marshaled_pinvoke
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Impulse_0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_RelativeVelocity_1;
	Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* ___m_Body_2;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_3;
	int32_t ___m_ContactCount_4;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_ReusedContacts_5;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_LegacyContacts_6;
};
// Native definition for COM marshalling of UnityEngine.Collision
struct Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0_marshaled_com
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Impulse_0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_RelativeVelocity_1;
	Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* ___m_Body_2;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_3;
	int32_t ___m_ContactCount_4;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_ReusedContacts_5;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_LegacyContacts_6;
};

// UnityEngine.Collision2D
struct Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B  : public RuntimeObject
{
	// System.Int32 UnityEngine.Collision2D::m_Collider
	int32_t ___m_Collider_0;
	// System.Int32 UnityEngine.Collision2D::m_OtherCollider
	int32_t ___m_OtherCollider_1;
	// System.Int32 UnityEngine.Collision2D::m_Rigidbody
	int32_t ___m_Rigidbody_2;
	// System.Int32 UnityEngine.Collision2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_3;
	// UnityEngine.Vector2 UnityEngine.Collision2D::m_RelativeVelocity
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RelativeVelocity_4;
	// System.Int32 UnityEngine.Collision2D::m_Enabled
	int32_t ___m_Enabled_5;
	// System.Int32 UnityEngine.Collision2D::m_ContactCount
	int32_t ___m_ContactCount_6;
	// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::m_ReusedContacts
	ContactPoint2DU5BU5D_t427621BF8902AE33C86E7BF384D9B2B5B781F949* ___m_ReusedContacts_7;
	// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::m_LegacyContacts
	ContactPoint2DU5BU5D_t427621BF8902AE33C86E7BF384D9B2B5B781F949* ___m_LegacyContacts_8;
};
// Native definition for P/Invoke marshalling of UnityEngine.Collision2D
struct Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B_marshaled_pinvoke
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RelativeVelocity_4;
	int32_t ___m_Enabled_5;
	int32_t ___m_ContactCount_6;
	ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801* ___m_ReusedContacts_7;
	ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801* ___m_LegacyContacts_8;
};
// Native definition for COM marshalling of UnityEngine.Collision2D
struct Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B_marshaled_com
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RelativeVelocity_4;
	int32_t ___m_Enabled_5;
	int32_t ___m_ContactCount_6;
	ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801* ___m_ReusedContacts_7;
	ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801* ___m_LegacyContacts_8;
};

// DG.Tweening.Color2
struct Color2_t35316BB1AA7A5F82C686D69DA763B9E99A72EEAA 
{
	// UnityEngine.Color DG.Tweening.Color2::ca
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___ca_0;
	// UnityEngine.Color DG.Tweening.Color2::cb
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___cb_1;
};

// UnityEngine.ContactFilter2D
struct ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14 
{
	// System.Boolean UnityEngine.ContactFilter2D::useTriggers
	bool ___useTriggers_0;
	// System.Boolean UnityEngine.ContactFilter2D::useLayerMask
	bool ___useLayerMask_1;
	// System.Boolean UnityEngine.ContactFilter2D::useDepth
	bool ___useDepth_2;
	// System.Boolean UnityEngine.ContactFilter2D::useOutsideDepth
	bool ___useOutsideDepth_3;
	// System.Boolean UnityEngine.ContactFilter2D::useNormalAngle
	bool ___useNormalAngle_4;
	// System.Boolean UnityEngine.ContactFilter2D::useOutsideNormalAngle
	bool ___useOutsideNormalAngle_5;
	// UnityEngine.LayerMask UnityEngine.ContactFilter2D::layerMask
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___layerMask_6;
	// System.Single UnityEngine.ContactFilter2D::minDepth
	float ___minDepth_7;
	// System.Single UnityEngine.ContactFilter2D::maxDepth
	float ___maxDepth_8;
	// System.Single UnityEngine.ContactFilter2D::minNormalAngle
	float ___minNormalAngle_9;
	// System.Single UnityEngine.ContactFilter2D::maxNormalAngle
	float ___maxNormalAngle_10;
};
// Native definition for P/Invoke marshalling of UnityEngine.ContactFilter2D
struct ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14_marshaled_pinvoke
{
	int32_t ___useTriggers_0;
	int32_t ___useLayerMask_1;
	int32_t ___useDepth_2;
	int32_t ___useOutsideDepth_3;
	int32_t ___useNormalAngle_4;
	int32_t ___useOutsideNormalAngle_5;
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___layerMask_6;
	float ___minDepth_7;
	float ___maxDepth_8;
	float ___minNormalAngle_9;
	float ___maxNormalAngle_10;
};
// Native definition for COM marshalling of UnityEngine.ContactFilter2D
struct ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14_marshaled_com
{
	int32_t ___useTriggers_0;
	int32_t ___useLayerMask_1;
	int32_t ___useDepth_2;
	int32_t ___useOutsideDepth_3;
	int32_t ___useNormalAngle_4;
	int32_t ___useOutsideNormalAngle_5;
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___layerMask_6;
	float ___minDepth_7;
	float ___maxDepth_8;
	float ___minNormalAngle_9;
	float ___maxNormalAngle_10;
};

// UnityEngine.ContactPoint
struct ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9 
{
	// UnityEngine.Vector3 UnityEngine.ContactPoint::m_Point
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.ContactPoint::m_Normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_1;
	// System.Int32 UnityEngine.ContactPoint::m_ThisColliderInstanceID
	int32_t ___m_ThisColliderInstanceID_2;
	// System.Int32 UnityEngine.ContactPoint::m_OtherColliderInstanceID
	int32_t ___m_OtherColliderInstanceID_3;
	// System.Single UnityEngine.ContactPoint::m_Separation
	float ___m_Separation_4;
};

// UnityEngine.ContactPoint2D
struct ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801 
{
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_Point
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Point_0;
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_Normal
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Normal_1;
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_RelativeVelocity
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RelativeVelocity_2;
	// System.Single UnityEngine.ContactPoint2D::m_Separation
	float ___m_Separation_3;
	// System.Single UnityEngine.ContactPoint2D::m_NormalImpulse
	float ___m_NormalImpulse_4;
	// System.Single UnityEngine.ContactPoint2D::m_TangentImpulse
	float ___m_TangentImpulse_5;
	// System.Int32 UnityEngine.ContactPoint2D::m_Collider
	int32_t ___m_Collider_6;
	// System.Int32 UnityEngine.ContactPoint2D::m_OtherCollider
	int32_t ___m_OtherCollider_7;
	// System.Int32 UnityEngine.ContactPoint2D::m_Rigidbody
	int32_t ___m_Rigidbody_8;
	// System.Int32 UnityEngine.ContactPoint2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_9;
	// System.Int32 UnityEngine.ContactPoint2D::m_Enabled
	int32_t ___m_Enabled_10;
};

// DG.Tweening.Plugins.Core.PathCore.ControlPoint
struct ControlPoint_tCAFE592FCADA75007AF24FEB842AE63DAFA9B8D1 
{
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.ControlPoint::a
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a_0;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.ControlPoint::b
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___b_1;
};

// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92  : public RuntimeObject
{
	// UnityEngine.CharacterController UnityEngine.ControllerColliderHit::m_Controller
	CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* ___m_Controller_0;
	// UnityEngine.Collider UnityEngine.ControllerColliderHit::m_Collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_1;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Point
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_2;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_3;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_MoveDirection
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_MoveDirection_4;
	// System.Single UnityEngine.ControllerColliderHit::m_MoveLength
	float ___m_MoveLength_5;
	// System.Int32 UnityEngine.ControllerColliderHit::m_Push
	int32_t ___m_Push_6;
};
// Native definition for P/Invoke marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92_marshaled_pinvoke
{
	CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* ___m_Controller_0;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_2;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};
// Native definition for COM marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92_marshaled_com
{
	CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* ___m_Controller_0;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_2;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Event
struct Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Event::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Event
struct Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Event
struct Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// UnityEngine.TextCore.Text.Extents
struct Extents_t369FB2B84521A0229C2FA3D4C8592B14E07CEFE6 
{
	// UnityEngine.Vector2 UnityEngine.TextCore.Text.Extents::min
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___min_0;
	// UnityEngine.Vector2 UnityEngine.TextCore.Text.Extents::max
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___max_1;
};

// UnityEngine.Timeline.FrameRateFieldAttribute
struct FrameRateFieldAttribute_t14E8B29573FB61BADE0F2AE9ED93C60AF16FBC07  : public PropertyAttribute_t5E0CB5A6CDA6E24CBD4FF26DE3B0C29D8BB54BF0
{
};

// UnityEngine.Bindings.FreeFunctionAttribute
struct FreeFunctionAttribute_t1200571BEDF64167E58F976FB7374AEA5D9BCBB6  : public NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270
{
};

// UnityEngine.GUI
struct GUI_tA9CDB3D69DB13D51AD83ABDB587EF95947EC2D2A  : public RuntimeObject
{
};

// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F  : public RuntimeObject
{
	// System.Single UnityEngine.GUILayoutEntry::minWidth
	float ___minWidth_0;
	// System.Single UnityEngine.GUILayoutEntry::maxWidth
	float ___maxWidth_1;
	// System.Single UnityEngine.GUILayoutEntry::minHeight
	float ___minHeight_2;
	// System.Single UnityEngine.GUILayoutEntry::maxHeight
	float ___maxHeight_3;
	// UnityEngine.Rect UnityEngine.GUILayoutEntry::rect
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___rect_4;
	// System.Int32 UnityEngine.GUILayoutEntry::stretchWidth
	int32_t ___stretchWidth_5;
	// System.Int32 UnityEngine.GUILayoutEntry::stretchHeight
	int32_t ___stretchHeight_6;
	// System.Boolean UnityEngine.GUILayoutEntry::consideredForMargin
	bool ___consideredForMargin_7;
	// UnityEngine.GUIStyle UnityEngine.GUILayoutEntry::m_Style
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_Style_8;
};

// UnityEngine.GUILayoutUtility
struct GUILayoutUtility_t48D00CD11CFC1E09E8EC2E51D59E735F5D24836F  : public RuntimeObject
{
};

// UnityEngine.GUISettings
struct GUISettings_tF2CA7E8B9F62F1FC013BFF053B5FA2709EBA3847  : public RuntimeObject
{
	// System.Boolean UnityEngine.GUISettings::m_DoubleClickSelectsWord
	bool ___m_DoubleClickSelectsWord_0;
	// System.Boolean UnityEngine.GUISettings::m_TripleClickSelectsLine
	bool ___m_TripleClickSelectsLine_1;
	// UnityEngine.Color UnityEngine.GUISettings::m_CursorColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_CursorColor_2;
	// System.Single UnityEngine.GUISettings::m_CursorFlashSpeed
	float ___m_CursorFlashSpeed_3;
	// UnityEngine.Color UnityEngine.GUISettings::m_SelectionColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_SelectionColor_4;
};

// UnityEngine.GUIStyleState
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95  : public RuntimeObject
{
	// System.IntPtr UnityEngine.GUIStyleState::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyle UnityEngine.GUIStyleState::m_SourceStyle
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_SourceStyle_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyleState
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_pinvoke* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.GUIStyleState
struct GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_com* ___m_SourceStyle_1;
};

// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t4C8666E37C725723182101E6F36DFAB20D581E9D  : public RuntimeObject
{
	// System.IntPtr UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::m_InternalLeaderboard
	intptr_t ___m_InternalLeaderboard_0;
	// UnityEngine.SocialPlatforms.Impl.Leaderboard UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::m_GenericLeaderboard
	Leaderboard_tBDB34CC6F79318BE6D7761015C70C8A5CC64EC71* ___m_GenericLeaderboard_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t4C8666E37C725723182101E6F36DFAB20D581E9D_marshaled_pinvoke
{
	intptr_t ___m_InternalLeaderboard_0;
	Leaderboard_tBDB34CC6F79318BE6D7761015C70C8A5CC64EC71* ___m_GenericLeaderboard_1;
};
// Native definition for COM marshalling of UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t4C8666E37C725723182101E6F36DFAB20D581E9D_marshaled_com
{
	intptr_t ___m_InternalLeaderboard_0;
	Leaderboard_tBDB34CC6F79318BE6D7761015C70C8A5CC64EC71* ___m_GenericLeaderboard_1;
};

// UnityEngine.UIElements.UIR.GfxUpdateBufferRange
struct GfxUpdateBufferRange_tC47258BCB472B0727B4FCE21A2A53506644C1A97 
{
	// System.UInt32 UnityEngine.UIElements.UIR.GfxUpdateBufferRange::offsetFromWriteStart
	uint32_t ___offsetFromWriteStart_0;
	// System.UInt32 UnityEngine.UIElements.UIR.GfxUpdateBufferRange::size
	uint32_t ___size_1;
	// System.UIntPtr UnityEngine.UIElements.UIR.GfxUpdateBufferRange::source
	uintptr_t ___source_2;
};

// UnityEngine.TextCore.Glyph
struct Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F  : public RuntimeObject
{
	// System.UInt32 UnityEngine.TextCore.Glyph::m_Index
	uint32_t ___m_Index_0;
	// UnityEngine.TextCore.GlyphMetrics UnityEngine.TextCore.Glyph::m_Metrics
	GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A ___m_Metrics_1;
	// UnityEngine.TextCore.GlyphRect UnityEngine.TextCore.Glyph::m_GlyphRect
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___m_GlyphRect_2;
	// System.Single UnityEngine.TextCore.Glyph::m_Scale
	float ___m_Scale_3;
	// System.Int32 UnityEngine.TextCore.Glyph::m_AtlasIndex
	int32_t ___m_AtlasIndex_4;
	// UnityEngine.TextCore.GlyphClassDefinitionType UnityEngine.TextCore.Glyph::m_ClassDefinitionType
	int32_t ___m_ClassDefinitionType_5;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.Glyph
struct Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F_marshaled_pinvoke
{
	uint32_t ___m_Index_0;
	GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A ___m_Metrics_1;
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___m_GlyphRect_2;
	float ___m_Scale_3;
	int32_t ___m_AtlasIndex_4;
	int32_t ___m_ClassDefinitionType_5;
};
// Native definition for COM marshalling of UnityEngine.TextCore.Glyph
struct Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F_marshaled_com
{
	uint32_t ___m_Index_0;
	GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A ___m_Metrics_1;
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___m_GlyphRect_2;
	float ___m_Scale_3;
	int32_t ___m_AtlasIndex_4;
	int32_t ___m_ClassDefinitionType_5;
};

// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord
struct GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7 
{
	// System.UInt32 UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord::m_GlyphIndex
	uint32_t ___m_GlyphIndex_0;
	// UnityEngine.TextCore.LowLevel.GlyphValueRecord UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord::m_GlyphValueRecord
	GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E ___m_GlyphValueRecord_1;
};

// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct
struct GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C 
{
	// System.UInt32 UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::index
	uint32_t ___index_0;
	// UnityEngine.TextCore.GlyphMetrics UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::metrics
	GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A ___metrics_1;
	// UnityEngine.TextCore.GlyphRect UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::glyphRect
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___glyphRect_2;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::scale
	float ___scale_3;
	// System.Int32 UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::atlasIndex
	int32_t ___atlasIndex_4;
	// UnityEngine.TextCore.GlyphClassDefinitionType UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::classDefinitionType
	int32_t ___classDefinitionType_5;
};

// UnityEngine.HumanLimit
struct HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E 
{
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Min
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Min_0;
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Max
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Max_1;
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Center
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Center_2;
	// System.Single UnityEngine.HumanLimit::m_AxisLength
	float ___m_AxisLength_3;
	// System.Int32 UnityEngine.HumanLimit::m_UseDefaultValues
	int32_t ___m_UseDefaultValues_4;
};

// System.Configuration.IgnoreSection
struct IgnoreSection_t43A7C33C0083D18639AA3CC3D75DD93FCF1C5D97  : public ConfigurationSection_t0BC609F0151B160A4FAB8226679B62AF22539C3E
{
};

// UnityEngine.IntegratedSubsystem
struct IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3  : public RuntimeObject
{
	// System.IntPtr UnityEngine.IntegratedSubsystem::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.ISubsystemDescriptor UnityEngine.IntegratedSubsystem::m_SubsystemDescriptor
	RuntimeObject* ___m_SubsystemDescriptor_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.IntegratedSubsystem
struct IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	RuntimeObject* ___m_SubsystemDescriptor_1;
};
// Native definition for COM marshalling of UnityEngine.IntegratedSubsystem
struct IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3_marshaled_com
{
	intptr_t ___m_Ptr_0;
	RuntimeObject* ___m_SubsystemDescriptor_1;
};

// UnityEngine.IntegratedSubsystemDescriptor
struct IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D  : public RuntimeObject
{
	// System.IntPtr UnityEngine.IntegratedSubsystemDescriptor::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.IntegratedSubsystemDescriptor
struct IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.IntegratedSubsystemDescriptor
struct IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.SocialPlatforms.Impl.Leaderboard
struct Leaderboard_tBDB34CC6F79318BE6D7761015C70C8A5CC64EC71  : public RuntimeObject
{
	// System.Boolean UnityEngine.SocialPlatforms.Impl.Leaderboard::m_Loading
	bool ___m_Loading_0;
	// UnityEngine.SocialPlatforms.IScore UnityEngine.SocialPlatforms.Impl.Leaderboard::m_LocalUserScore
	RuntimeObject* ___m_LocalUserScore_1;
	// System.UInt32 UnityEngine.SocialPlatforms.Impl.Leaderboard::m_MaxRange
	uint32_t ___m_MaxRange_2;
	// UnityEngine.SocialPlatforms.IScore[] UnityEngine.SocialPlatforms.Impl.Leaderboard::m_Scores
	IScoreU5BU5D_t72B1FC43A0166FFFA30AF4E10BCA837E34A6B042* ___m_Scores_3;
	// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::m_Title
	String_t* ___m_Title_4;
	// System.String[] UnityEngine.SocialPlatforms.Impl.Leaderboard::m_UserIDs
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___m_UserIDs_5;
	// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_6;
	// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::<userScope>k__BackingField
	int32_t ___U3CuserScopeU3Ek__BackingField_7;
	// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::<range>k__BackingField
	Range_tDDBAD7CBDC5DD273DA4330F4E9CDF24C62F16ED6 ___U3CrangeU3Ek__BackingField_8;
	// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::<timeScope>k__BackingField
	int32_t ___U3CtimeScopeU3Ek__BackingField_9;
};

// UnityEngine.XR.MeshGenerationResult
struct MeshGenerationResult_tD5A6D639B2CF1A3F855AFB41861DEC48DC0D3A9C 
{
	// UnityEngine.XR.MeshId UnityEngine.XR.MeshGenerationResult::<MeshId>k__BackingField
	MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC ___U3CMeshIdU3Ek__BackingField_0;
	// UnityEngine.Mesh UnityEngine.XR.MeshGenerationResult::<Mesh>k__BackingField
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___U3CMeshU3Ek__BackingField_1;
	// UnityEngine.MeshCollider UnityEngine.XR.MeshGenerationResult::<MeshCollider>k__BackingField
	MeshCollider_tB525E4DDE383252364ED0BDD32CF2B53914EE455* ___U3CMeshColliderU3Ek__BackingField_2;
	// UnityEngine.XR.MeshGenerationStatus UnityEngine.XR.MeshGenerationResult::<Status>k__BackingField
	int32_t ___U3CStatusU3Ek__BackingField_3;
	// UnityEngine.XR.MeshVertexAttributes UnityEngine.XR.MeshGenerationResult::<Attributes>k__BackingField
	int32_t ___U3CAttributesU3Ek__BackingField_4;
	// System.UInt64 UnityEngine.XR.MeshGenerationResult::<Timestamp>k__BackingField
	uint64_t ___U3CTimestampU3Ek__BackingField_5;
	// UnityEngine.Vector3 UnityEngine.XR.MeshGenerationResult::<Position>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CPositionU3Ek__BackingField_6;
	// UnityEngine.Quaternion UnityEngine.XR.MeshGenerationResult::<Rotation>k__BackingField
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___U3CRotationU3Ek__BackingField_7;
	// UnityEngine.Vector3 UnityEngine.XR.MeshGenerationResult::<Scale>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CScaleU3Ek__BackingField_8;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.MeshGenerationResult
struct MeshGenerationResult_tD5A6D639B2CF1A3F855AFB41861DEC48DC0D3A9C_marshaled_pinvoke
{
	MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC ___U3CMeshIdU3Ek__BackingField_0;
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___U3CMeshU3Ek__BackingField_1;
	MeshCollider_tB525E4DDE383252364ED0BDD32CF2B53914EE455* ___U3CMeshColliderU3Ek__BackingField_2;
	int32_t ___U3CStatusU3Ek__BackingField_3;
	int32_t ___U3CAttributesU3Ek__BackingField_4;
	uint64_t ___U3CTimestampU3Ek__BackingField_5;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CPositionU3Ek__BackingField_6;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___U3CRotationU3Ek__BackingField_7;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CScaleU3Ek__BackingField_8;
};
// Native definition for COM marshalling of UnityEngine.XR.MeshGenerationResult
struct MeshGenerationResult_tD5A6D639B2CF1A3F855AFB41861DEC48DC0D3A9C_marshaled_com
{
	MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC ___U3CMeshIdU3Ek__BackingField_0;
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___U3CMeshU3Ek__BackingField_1;
	MeshCollider_tB525E4DDE383252364ED0BDD32CF2B53914EE455* ___U3CMeshColliderU3Ek__BackingField_2;
	int32_t ___U3CStatusU3Ek__BackingField_3;
	int32_t ___U3CAttributesU3Ek__BackingField_4;
	uint64_t ___U3CTimestampU3Ek__BackingField_5;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CPositionU3Ek__BackingField_6;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___U3CRotationU3Ek__BackingField_7;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CScaleU3Ek__BackingField_8;
};

// UnityEngine.TextCore.Text.MeshInfo
struct MeshInfo_tE55C4A8846CC2C399CCC3FE989476D987B86AB2F 
{
	// System.Int32 UnityEngine.TextCore.Text.MeshInfo::vertexCount
	int32_t ___vertexCount_1;
	// UnityEngine.Vector3[] UnityEngine.TextCore.Text.MeshInfo::vertices
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___vertices_2;
	// UnityEngine.Vector2[] UnityEngine.TextCore.Text.MeshInfo::uvs0
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___uvs0_3;
	// UnityEngine.Vector2[] UnityEngine.TextCore.Text.MeshInfo::uvs2
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___uvs2_4;
	// UnityEngine.Color32[] UnityEngine.TextCore.Text.MeshInfo::colors32
	Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___colors32_5;
	// System.Int32[] UnityEngine.TextCore.Text.MeshInfo::triangles
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___triangles_6;
	// UnityEngine.Material UnityEngine.TextCore.Text.MeshInfo::material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_7;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.Text.MeshInfo
struct MeshInfo_tE55C4A8846CC2C399CCC3FE989476D987B86AB2F_marshaled_pinvoke
{
	int32_t ___vertexCount_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___vertices_2;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* ___uvs0_3;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* ___uvs2_4;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* ___colors32_5;
	Il2CppSafeArray/*NONE*/* ___triangles_6;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_7;
};
// Native definition for COM marshalling of UnityEngine.TextCore.Text.MeshInfo
struct MeshInfo_tE55C4A8846CC2C399CCC3FE989476D987B86AB2F_marshaled_com
{
	int32_t ___vertexCount_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___vertices_2;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* ___uvs0_3;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* ___uvs2_4;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* ___colors32_5;
	Il2CppSafeArray/*NONE*/* ___triangles_6;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_7;
};

// UnityEngine.ModifiableContactPair
struct ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960 
{
	// System.IntPtr UnityEngine.ModifiableContactPair::actor
	intptr_t ___actor_0;
	// System.IntPtr UnityEngine.ModifiableContactPair::otherActor
	intptr_t ___otherActor_1;
	// System.IntPtr UnityEngine.ModifiableContactPair::shape
	intptr_t ___shape_2;
	// System.IntPtr UnityEngine.ModifiableContactPair::otherShape
	intptr_t ___otherShape_3;
	// UnityEngine.Quaternion UnityEngine.ModifiableContactPair::rotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_4;
	// UnityEngine.Vector3 UnityEngine.ModifiableContactPair::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_5;
	// UnityEngine.Quaternion UnityEngine.ModifiableContactPair::otherRotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___otherRotation_6;
	// UnityEngine.Vector3 UnityEngine.ModifiableContactPair::otherPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___otherPosition_7;
	// System.Int32 UnityEngine.ModifiableContactPair::numContacts
	int32_t ___numContacts_8;
	// System.IntPtr UnityEngine.ModifiableContactPair::contacts
	intptr_t ___contacts_9;
};

// UnityEngine.Bindings.NativePropertyAttribute
struct NativePropertyAttribute_tAF7FB03BF7FFE9E8AB0E75B0F842FC0AA22AE607  : public NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270
{
	// UnityEngine.Bindings.TargetType UnityEngine.Bindings.NativePropertyAttribute::<TargetType>k__BackingField
	int32_t ___U3CTargetTypeU3Ek__BackingField_5;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.ObjectGUIState
struct ObjectGUIState_t7BE88DC8B9C7187A77D63BBCBE9DB7B674863C15  : public RuntimeObject
{
	// System.IntPtr UnityEngine.ObjectGUIState::m_Ptr
	intptr_t ___m_Ptr_0;
};

// DG.Tweening.Plugins.Options.PathOptions
struct PathOptions_t76F1CBAC082454349D530B799121EB15BFA4CB3A 
{
	// DG.Tweening.PathMode DG.Tweening.Plugins.Options.PathOptions::mode
	int32_t ___mode_0;
	// DG.Tweening.Plugins.Options.OrientType DG.Tweening.Plugins.Options.PathOptions::orientType
	int32_t ___orientType_1;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.PathOptions::lockPositionAxis
	int32_t ___lockPositionAxis_2;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.PathOptions::lockRotationAxis
	int32_t ___lockRotationAxis_3;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::isClosedPath
	bool ___isClosedPath_4;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Options.PathOptions::lookAtPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___lookAtPosition_5;
	// UnityEngine.Transform DG.Tweening.Plugins.Options.PathOptions::lookAtTransform
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___lookAtTransform_6;
	// System.Single DG.Tweening.Plugins.Options.PathOptions::lookAhead
	float ___lookAhead_7;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::hasCustomForwardDirection
	bool ___hasCustomForwardDirection_8;
	// UnityEngine.Quaternion DG.Tweening.Plugins.Options.PathOptions::forward
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___forward_9;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::useLocalPosition
	bool ___useLocalPosition_10;
	// UnityEngine.Transform DG.Tweening.Plugins.Options.PathOptions::parent
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___parent_11;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::isRigidbody
	bool ___isRigidbody_12;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::isRigidbody2D
	bool ___isRigidbody2D_13;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::stableZRotation
	bool ___stableZRotation_14;
	// UnityEngine.Quaternion DG.Tweening.Plugins.Options.PathOptions::startupRot
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___startupRot_15;
	// System.Single DG.Tweening.Plugins.Options.PathOptions::startupZRot
	float ___startupZRot_16;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::addedExtraStartWp
	bool ___addedExtraStartWp_17;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::addedExtraEndWp
	bool ___addedExtraEndWp_18;
};
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.PathOptions
struct PathOptions_t76F1CBAC082454349D530B799121EB15BFA4CB3A_marshaled_pinvoke
{
	int32_t ___mode_0;
	int32_t ___orientType_1;
	int32_t ___lockPositionAxis_2;
	int32_t ___lockRotationAxis_3;
	int32_t ___isClosedPath_4;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___lookAtPosition_5;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___lookAtTransform_6;
	float ___lookAhead_7;
	int32_t ___hasCustomForwardDirection_8;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___forward_9;
	int32_t ___useLocalPosition_10;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___parent_11;
	int32_t ___isRigidbody_12;
	int32_t ___isRigidbody2D_13;
	int32_t ___stableZRotation_14;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___startupRot_15;
	float ___startupZRot_16;
	int32_t ___addedExtraStartWp_17;
	int32_t ___addedExtraEndWp_18;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.PathOptions
struct PathOptions_t76F1CBAC082454349D530B799121EB15BFA4CB3A_marshaled_com
{
	int32_t ___mode_0;
	int32_t ___orientType_1;
	int32_t ___lockPositionAxis_2;
	int32_t ___lockRotationAxis_3;
	int32_t ___isClosedPath_4;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___lookAtPosition_5;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___lookAtTransform_6;
	float ___lookAhead_7;
	int32_t ___hasCustomForwardDirection_8;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___forward_9;
	int32_t ___useLocalPosition_10;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___parent_11;
	int32_t ___isRigidbody_12;
	int32_t ___isRigidbody2D_13;
	int32_t ___stableZRotation_14;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___startupRot_15;
	float ___startupZRot_16;
	int32_t ___addedExtraStartWp_17;
	int32_t ___addedExtraEndWp_18;
};

// UnityEngine.Playables.PlayableHandle
struct PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 
{
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableHandle::m_Version
	uint32_t ___m_Version_1;
};

// UnityEngine.Playables.PlayableOutputHandle
struct PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 
{
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	uint32_t ___m_Version_1;
};

// Unity.Profiling.ProfilerMarker
struct ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD 
{
	// System.IntPtr Unity.Profiling.ProfilerMarker::m_Ptr
	intptr_t ___m_Ptr_0;
};

// DG.Tweening.Plugins.Options.QuaternionOptions
struct QuaternionOptions_t1B83700718F7417854E4B6FB0E1726E183F69718 
{
	// DG.Tweening.RotateMode DG.Tweening.Plugins.Options.QuaternionOptions::rotateMode
	int32_t ___rotateMode_0;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.QuaternionOptions::axisConstraint
	int32_t ___axisConstraint_1;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Options.QuaternionOptions::up
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___up_2;
	// System.Boolean DG.Tweening.Plugins.Options.QuaternionOptions::dynamicLookAt
	bool ___dynamicLookAt_3;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Options.QuaternionOptions::dynamicLookAtWorldPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___dynamicLookAtWorldPosition_4;
};
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.QuaternionOptions
struct QuaternionOptions_t1B83700718F7417854E4B6FB0E1726E183F69718_marshaled_pinvoke
{
	int32_t ___rotateMode_0;
	int32_t ___axisConstraint_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___up_2;
	int32_t ___dynamicLookAt_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___dynamicLookAtWorldPosition_4;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.QuaternionOptions
struct QuaternionOptions_t1B83700718F7417854E4B6FB0E1726E183F69718_marshaled_com
{
	int32_t ___rotateMode_0;
	int32_t ___axisConstraint_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___up_2;
	int32_t ___dynamicLookAt_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___dynamicLookAtWorldPosition_4;
};

// UnityEngine.RaycastHit
struct RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5 
{
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;
};

// UnityEngine.RaycastHit2D
struct RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA 
{
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;
};

// UnityEngine.RectOffset
struct RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5  : public RuntimeObject
{
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject* ___m_SourceStyle_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};

// UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_tC979947EE51355162B3241B9F80D95A8FD25FE52  : public RuntimeObject
{
	// System.IntPtr UnityEngine.RemoteConfigSettings::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<System.Boolean> UnityEngine.RemoteConfigSettings::Updated
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* ___Updated_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_tC979947EE51355162B3241B9F80D95A8FD25FE52_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___Updated_1;
};
// Native definition for COM marshalling of UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_tC979947EE51355162B3241B9F80D95A8FD25FE52_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___Updated_1;
};

// UnityEngine.Rendering.RenderTargetIdentifier
struct RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B 
{
	// UnityEngine.Rendering.BuiltinRenderTextureType UnityEngine.Rendering.RenderTargetIdentifier::m_Type
	int32_t ___m_Type_1;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_NameID
	int32_t ___m_NameID_2;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_InstanceID
	int32_t ___m_InstanceID_3;
	// System.IntPtr UnityEngine.Rendering.RenderTargetIdentifier::m_BufferPointer
	intptr_t ___m_BufferPointer_4;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_MipLevel
	int32_t ___m_MipLevel_5;
	// UnityEngine.CubemapFace UnityEngine.Rendering.RenderTargetIdentifier::m_CubeFace
	int32_t ___m_CubeFace_6;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_DepthSlice
	int32_t ___m_DepthSlice_7;
};

// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t9ED78BAAA0A342F85A3473CCF95CE31E6BF03D53  : public RuntimeObject
{
	// System.DateTime UnityEngine.SocialPlatforms.Impl.Score::m_Date
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___m_Date_0;
	// System.String UnityEngine.SocialPlatforms.Impl.Score::m_FormattedValue
	String_t* ___m_FormattedValue_1;
	// System.String UnityEngine.SocialPlatforms.Impl.Score::m_UserID
	String_t* ___m_UserID_2;
	// System.Int32 UnityEngine.SocialPlatforms.Impl.Score::m_Rank
	int32_t ___m_Rank_3;
	// System.String UnityEngine.SocialPlatforms.Impl.Score::<leaderboardID>k__BackingField
	String_t* ___U3CleaderboardIDU3Ek__BackingField_4;
	// System.Int64 UnityEngine.SocialPlatforms.Impl.Score::<value>k__BackingField
	int64_t ___U3CvalueU3Ek__BackingField_5;
};

// UnityEngine.SendMouseEvents
struct SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39  : public RuntimeObject
{
};

// UnityEngine.SkeletonBone
struct SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126 
{
	// System.String UnityEngine.SkeletonBone::name
	String_t* ___name_0;
	// System.String UnityEngine.SkeletonBone::parentName
	String_t* ___parentName_1;
	// UnityEngine.Vector3 UnityEngine.SkeletonBone::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_2;
	// UnityEngine.Quaternion UnityEngine.SkeletonBone::rotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_3;
	// UnityEngine.Vector3 UnityEngine.SkeletonBone::scale
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale_4;
};
// Native definition for P/Invoke marshalling of UnityEngine.SkeletonBone
struct SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_pinvoke
{
	char* ___name_0;
	char* ___parentName_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_2;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale_4;
};
// Native definition for COM marshalling of UnityEngine.SkeletonBone
struct SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___parentName_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_2;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale_4;
};

// UnityEngine.TextEditor
struct TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27  : public RuntimeObject
{
	// UnityEngine.TouchScreenKeyboard UnityEngine.TextEditor::keyboardOnScreen
	TouchScreenKeyboard_tE87B78A3DAED69816B44C99270A734682E093E7A* ___keyboardOnScreen_0;
	// System.Int32 UnityEngine.TextEditor::controlID
	int32_t ___controlID_1;
	// UnityEngine.GUIStyle UnityEngine.TextEditor::style
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___style_2;
	// System.Boolean UnityEngine.TextEditor::multiline
	bool ___multiline_3;
	// System.Boolean UnityEngine.TextEditor::hasHorizontalCursorPos
	bool ___hasHorizontalCursorPos_4;
	// System.Boolean UnityEngine.TextEditor::isPasswordField
	bool ___isPasswordField_5;
	// System.Boolean UnityEngine.TextEditor::m_HasFocus
	bool ___m_HasFocus_6;
	// UnityEngine.Vector2 UnityEngine.TextEditor::scrollOffset
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___scrollOffset_7;
	// UnityEngine.GUIContent UnityEngine.TextEditor::m_Content
	GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2* ___m_Content_8;
	// UnityEngine.Rect UnityEngine.TextEditor::m_Position
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___m_Position_9;
	// System.Int32 UnityEngine.TextEditor::m_CursorIndex
	int32_t ___m_CursorIndex_10;
	// System.Int32 UnityEngine.TextEditor::m_SelectIndex
	int32_t ___m_SelectIndex_11;
	// System.Boolean UnityEngine.TextEditor::m_RevealCursor
	bool ___m_RevealCursor_12;
	// UnityEngine.Vector2 UnityEngine.TextEditor::graphicalCursorPos
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___graphicalCursorPos_13;
	// UnityEngine.Vector2 UnityEngine.TextEditor::graphicalSelectCursorPos
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___graphicalSelectCursorPos_14;
	// System.Boolean UnityEngine.TextEditor::m_MouseDragSelectsWholeWords
	bool ___m_MouseDragSelectsWholeWords_15;
	// System.Int32 UnityEngine.TextEditor::m_DblClickInitPos
	int32_t ___m_DblClickInitPos_16;
	// UnityEngine.TextEditor/DblClickSnapping UnityEngine.TextEditor::m_DblClickSnap
	uint8_t ___m_DblClickSnap_17;
	// System.Boolean UnityEngine.TextEditor::m_bJustSelected
	bool ___m_bJustSelected_18;
	// System.Int32 UnityEngine.TextEditor::m_iAltCursorPos
	int32_t ___m_iAltCursorPos_19;
	// System.String UnityEngine.TextEditor::oldText
	String_t* ___oldText_20;
	// System.Int32 UnityEngine.TextEditor::oldPos
	int32_t ___oldPos_21;
	// System.Int32 UnityEngine.TextEditor::oldSelectPos
	int32_t ___oldSelectPos_22;
};

// UnityEngine.TextGenerationSettings
struct TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3 
{
	// UnityEngine.Font UnityEngine.TextGenerationSettings::font
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_0;
	// UnityEngine.Color UnityEngine.TextGenerationSettings::color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_1;
	// System.Int32 UnityEngine.TextGenerationSettings::fontSize
	int32_t ___fontSize_2;
	// System.Single UnityEngine.TextGenerationSettings::lineSpacing
	float ___lineSpacing_3;
	// System.Boolean UnityEngine.TextGenerationSettings::richText
	bool ___richText_4;
	// System.Single UnityEngine.TextGenerationSettings::scaleFactor
	float ___scaleFactor_5;
	// UnityEngine.FontStyle UnityEngine.TextGenerationSettings::fontStyle
	int32_t ___fontStyle_6;
	// UnityEngine.TextAnchor UnityEngine.TextGenerationSettings::textAnchor
	int32_t ___textAnchor_7;
	// System.Boolean UnityEngine.TextGenerationSettings::alignByGeometry
	bool ___alignByGeometry_8;
	// System.Boolean UnityEngine.TextGenerationSettings::resizeTextForBestFit
	bool ___resizeTextForBestFit_9;
	// System.Int32 UnityEngine.TextGenerationSettings::resizeTextMinSize
	int32_t ___resizeTextMinSize_10;
	// System.Int32 UnityEngine.TextGenerationSettings::resizeTextMaxSize
	int32_t ___resizeTextMaxSize_11;
	// System.Boolean UnityEngine.TextGenerationSettings::updateBounds
	bool ___updateBounds_12;
	// UnityEngine.VerticalWrapMode UnityEngine.TextGenerationSettings::verticalOverflow
	int32_t ___verticalOverflow_13;
	// UnityEngine.HorizontalWrapMode UnityEngine.TextGenerationSettings::horizontalOverflow
	int32_t ___horizontalOverflow_14;
	// UnityEngine.Vector2 UnityEngine.TextGenerationSettings::generationExtents
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___generationExtents_15;
	// UnityEngine.Vector2 UnityEngine.TextGenerationSettings::pivot
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___pivot_16;
	// System.Boolean UnityEngine.TextGenerationSettings::generateOutOfBounds
	bool ___generateOutOfBounds_17;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextGenerationSettings
struct TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3_marshaled_pinvoke
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_0;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_1;
	int32_t ___fontSize_2;
	float ___lineSpacing_3;
	int32_t ___richText_4;
	float ___scaleFactor_5;
	int32_t ___fontStyle_6;
	int32_t ___textAnchor_7;
	int32_t ___alignByGeometry_8;
	int32_t ___resizeTextForBestFit_9;
	int32_t ___resizeTextMinSize_10;
	int32_t ___resizeTextMaxSize_11;
	int32_t ___updateBounds_12;
	int32_t ___verticalOverflow_13;
	int32_t ___horizontalOverflow_14;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___generationExtents_15;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___pivot_16;
	int32_t ___generateOutOfBounds_17;
};
// Native definition for COM marshalling of UnityEngine.TextGenerationSettings
struct TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3_marshaled_com
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_0;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_1;
	int32_t ___fontSize_2;
	float ___lineSpacing_3;
	int32_t ___richText_4;
	float ___scaleFactor_5;
	int32_t ___fontStyle_6;
	int32_t ___textAnchor_7;
	int32_t ___alignByGeometry_8;
	int32_t ___resizeTextForBestFit_9;
	int32_t ___resizeTextMinSize_10;
	int32_t ___resizeTextMaxSize_11;
	int32_t ___updateBounds_12;
	int32_t ___verticalOverflow_13;
	int32_t ___horizontalOverflow_14;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___generationExtents_15;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___pivot_16;
	int32_t ___generateOutOfBounds_17;
};

// UnityEngine.TextCore.Text.TextGenerationSettings
struct TextGenerationSettings_t3E75DB1D14DF53934AF76C9ACB1CD94A344A92A2  : public RuntimeObject
{
	// System.String UnityEngine.TextCore.Text.TextGenerationSettings::text
	String_t* ___text_0;
	// UnityEngine.Rect UnityEngine.TextCore.Text.TextGenerationSettings::screenRect
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___screenRect_1;
	// UnityEngine.Vector4 UnityEngine.TextCore.Text.TextGenerationSettings::margins
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___margins_2;
	// System.Single UnityEngine.TextCore.Text.TextGenerationSettings::scale
	float ___scale_3;
	// UnityEngine.TextCore.Text.FontAsset UnityEngine.TextCore.Text.TextGenerationSettings::fontAsset
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___fontAsset_4;
	// UnityEngine.Material UnityEngine.TextCore.Text.TextGenerationSettings::material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_5;
	// UnityEngine.TextCore.Text.SpriteAsset UnityEngine.TextCore.Text.TextGenerationSettings::spriteAsset
	SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313* ___spriteAsset_6;
	// UnityEngine.TextCore.Text.TextStyleSheet UnityEngine.TextCore.Text.TextGenerationSettings::styleSheet
	TextStyleSheet_t86A0FA5523897465F371A2ABC17DFA3558C8D15E* ___styleSheet_7;
	// UnityEngine.TextCore.Text.FontStyles UnityEngine.TextCore.Text.TextGenerationSettings::fontStyle
	int32_t ___fontStyle_8;
	// UnityEngine.TextCore.Text.TextSettings UnityEngine.TextCore.Text.TextGenerationSettings::textSettings
	TextSettings_tB7F55685AFFD4A96F714427BCACFD6958E357D64* ___textSettings_9;
	// UnityEngine.TextCore.Text.TextAlignment UnityEngine.TextCore.Text.TextGenerationSettings::textAlignment
	int32_t ___textAlignment_10;
	// UnityEngine.TextCore.Text.TextOverflowMode UnityEngine.TextCore.Text.TextGenerationSettings::overflowMode
	int32_t ___overflowMode_11;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerationSettings::wordWrap
	bool ___wordWrap_12;
	// System.Single UnityEngine.TextCore.Text.TextGenerationSettings::wordWrappingRatio
	float ___wordWrappingRatio_13;
	// UnityEngine.Color UnityEngine.TextCore.Text.TextGenerationSettings::color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_14;
	// UnityEngine.TextCore.Text.TextColorGradient UnityEngine.TextCore.Text.TextGenerationSettings::fontColorGradient
	TextColorGradient_t22D94E441E8E8CD772B966C167E5C0AEB0919D70* ___fontColorGradient_15;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerationSettings::tintSprites
	bool ___tintSprites_16;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerationSettings::overrideRichTextColors
	bool ___overrideRichTextColors_17;
	// System.Single UnityEngine.TextCore.Text.TextGenerationSettings::fontSize
	float ___fontSize_18;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerationSettings::autoSize
	bool ___autoSize_19;
	// System.Single UnityEngine.TextCore.Text.TextGenerationSettings::fontSizeMin
	float ___fontSizeMin_20;
	// System.Single UnityEngine.TextCore.Text.TextGenerationSettings::fontSizeMax
	float ___fontSizeMax_21;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerationSettings::enableKerning
	bool ___enableKerning_22;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerationSettings::richText
	bool ___richText_23;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerationSettings::isRightToLeft
	bool ___isRightToLeft_24;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerationSettings::extraPadding
	bool ___extraPadding_25;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerationSettings::parseControlCharacters
	bool ___parseControlCharacters_26;
	// System.Single UnityEngine.TextCore.Text.TextGenerationSettings::characterSpacing
	float ___characterSpacing_27;
	// System.Single UnityEngine.TextCore.Text.TextGenerationSettings::wordSpacing
	float ___wordSpacing_28;
	// System.Single UnityEngine.TextCore.Text.TextGenerationSettings::lineSpacing
	float ___lineSpacing_29;
	// System.Single UnityEngine.TextCore.Text.TextGenerationSettings::paragraphSpacing
	float ___paragraphSpacing_30;
	// System.Single UnityEngine.TextCore.Text.TextGenerationSettings::lineSpacingMax
	float ___lineSpacingMax_31;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerationSettings::maxVisibleCharacters
	int32_t ___maxVisibleCharacters_32;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerationSettings::maxVisibleWords
	int32_t ___maxVisibleWords_33;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerationSettings::maxVisibleLines
	int32_t ___maxVisibleLines_34;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerationSettings::firstVisibleCharacter
	int32_t ___firstVisibleCharacter_35;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerationSettings::useMaxVisibleDescender
	bool ___useMaxVisibleDescender_36;
	// UnityEngine.TextCore.Text.TextFontWeight UnityEngine.TextCore.Text.TextGenerationSettings::fontWeight
	int32_t ___fontWeight_37;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerationSettings::pageToDisplay
	int32_t ___pageToDisplay_38;
	// UnityEngine.TextCore.Text.TextureMapping UnityEngine.TextCore.Text.TextGenerationSettings::horizontalMapping
	int32_t ___horizontalMapping_39;
	// UnityEngine.TextCore.Text.TextureMapping UnityEngine.TextCore.Text.TextGenerationSettings::verticalMapping
	int32_t ___verticalMapping_40;
	// System.Single UnityEngine.TextCore.Text.TextGenerationSettings::uvLineOffset
	float ___uvLineOffset_41;
	// UnityEngine.TextCore.Text.VertexSortingOrder UnityEngine.TextCore.Text.TextGenerationSettings::geometrySortingOrder
	int32_t ___geometrySortingOrder_42;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerationSettings::inverseYAxis
	bool ___inverseYAxis_43;
	// System.Single UnityEngine.TextCore.Text.TextGenerationSettings::charWidthMaxAdj
	float ___charWidthMaxAdj_44;
};

// UnityEngine.TextCore.Text.TextGeneratorUtilities
struct TextGeneratorUtilities_tAD0F329B1A5C7CC27CF63086C11FE092B43FED53  : public RuntimeObject
{
};

// UnityEngine.TextCore.Text.TextInfo
struct TextInfo_t27E58E62A7552C66D38C175AF9D22622365F5D09  : public RuntimeObject
{
	// System.Int32 UnityEngine.TextCore.Text.TextInfo::characterCount
	int32_t ___characterCount_2;
	// System.Int32 UnityEngine.TextCore.Text.TextInfo::spriteCount
	int32_t ___spriteCount_3;
	// System.Int32 UnityEngine.TextCore.Text.TextInfo::spaceCount
	int32_t ___spaceCount_4;
	// System.Int32 UnityEngine.TextCore.Text.TextInfo::wordCount
	int32_t ___wordCount_5;
	// System.Int32 UnityEngine.TextCore.Text.TextInfo::linkCount
	int32_t ___linkCount_6;
	// System.Int32 UnityEngine.TextCore.Text.TextInfo::lineCount
	int32_t ___lineCount_7;
	// System.Int32 UnityEngine.TextCore.Text.TextInfo::pageCount
	int32_t ___pageCount_8;
	// System.Int32 UnityEngine.TextCore.Text.TextInfo::materialCount
	int32_t ___materialCount_9;
	// UnityEngine.TextCore.Text.TextElementInfo[] UnityEngine.TextCore.Text.TextInfo::textElementInfo
	TextElementInfoU5BU5D_tEC28C9B72883EE21AA798913497C69E179A15C4E* ___textElementInfo_10;
	// UnityEngine.TextCore.Text.WordInfo[] UnityEngine.TextCore.Text.TextInfo::wordInfo
	WordInfoU5BU5D_tAD74C9720883D7BB229A20FFAE9EFD2CF9963F7B* ___wordInfo_11;
	// UnityEngine.TextCore.Text.LinkInfo[] UnityEngine.TextCore.Text.TextInfo::linkInfo
	LinkInfoU5BU5D_tB7EB23E47AF29CCBEC884F9D0DB95BC97F62AE51* ___linkInfo_12;
	// UnityEngine.TextCore.Text.LineInfo[] UnityEngine.TextCore.Text.TextInfo::lineInfo
	LineInfoU5BU5D_t37598F2175B291797270D1161DC29B6296FB169D* ___lineInfo_13;
	// UnityEngine.TextCore.Text.PageInfo[] UnityEngine.TextCore.Text.TextInfo::pageInfo
	PageInfoU5BU5D_tFEA2CF88695491CFC2F2A2EF6BDCC56E52B0A6D4* ___pageInfo_14;
	// UnityEngine.TextCore.Text.MeshInfo[] UnityEngine.TextCore.Text.TextInfo::meshInfo
	MeshInfoU5BU5D_t3DF8B75BF4A213334EED197AD25E432212894AC6* ___meshInfo_15;
	// System.Boolean UnityEngine.TextCore.Text.TextInfo::isDirty
	bool ___isDirty_16;
};

// UnityEngine.UIElements.TextNativeSettings
struct TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062 
{
	// System.String UnityEngine.UIElements.TextNativeSettings::text
	String_t* ___text_0;
	// UnityEngine.Font UnityEngine.UIElements.TextNativeSettings::font
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_1;
	// System.Int32 UnityEngine.UIElements.TextNativeSettings::size
	int32_t ___size_2;
	// System.Single UnityEngine.UIElements.TextNativeSettings::scaling
	float ___scaling_3;
	// UnityEngine.FontStyle UnityEngine.UIElements.TextNativeSettings::style
	int32_t ___style_4;
	// UnityEngine.Color UnityEngine.UIElements.TextNativeSettings::color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_5;
	// UnityEngine.TextAnchor UnityEngine.UIElements.TextNativeSettings::anchor
	int32_t ___anchor_6;
	// System.Boolean UnityEngine.UIElements.TextNativeSettings::wordWrap
	bool ___wordWrap_7;
	// System.Single UnityEngine.UIElements.TextNativeSettings::wordWrapWidth
	float ___wordWrapWidth_8;
	// System.Boolean UnityEngine.UIElements.TextNativeSettings::richText
	bool ___richText_9;
};
// Native definition for P/Invoke marshalling of UnityEngine.UIElements.TextNativeSettings
struct TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062_marshaled_pinvoke
{
	char* ___text_0;
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_1;
	int32_t ___size_2;
	float ___scaling_3;
	int32_t ___style_4;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_5;
	int32_t ___anchor_6;
	int32_t ___wordWrap_7;
	float ___wordWrapWidth_8;
	int32_t ___richText_9;
};
// Native definition for COM marshalling of UnityEngine.UIElements.TextNativeSettings
struct TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062_marshaled_com
{
	Il2CppChar* ___text_0;
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_1;
	int32_t ___size_2;
	float ___scaling_3;
	int32_t ___style_4;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_5;
	int32_t ___anchor_6;
	int32_t ___wordWrap_7;
	float ___wordWrapWidth_8;
	int32_t ___richText_9;
};

// UnityEngine.TextCore.Text.TextVertex
struct TextVertex_tF030A16DC67EAF3F6C9C9C0564D4B88758B173A9 
{
	// UnityEngine.Vector3 UnityEngine.TextCore.Text.TextVertex::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_0;
	// UnityEngine.Vector2 UnityEngine.TextCore.Text.TextVertex::uv
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___uv_1;
	// UnityEngine.Vector2 UnityEngine.TextCore.Text.TextVertex::uv2
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___uv2_2;
	// UnityEngine.Vector2 UnityEngine.TextCore.Text.TextVertex::uv4
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___uv4_3;
	// UnityEngine.Color32 UnityEngine.TextCore.Text.TextVertex::color
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color_4;
};

// UnityEngine.UIElements.TextVertex
struct TextVertex_tF56662BA585F7DD34D71971F1AA1D2E767946CF3 
{
	// UnityEngine.Vector3 UnityEngine.UIElements.TextVertex::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_0;
	// UnityEngine.Color32 UnityEngine.UIElements.TextVertex::color
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color_1;
	// UnityEngine.Vector2 UnityEngine.UIElements.TextVertex::uv0
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___uv0_2;
};

// UnityEngine.Bindings.ThreadSafeAttribute
struct ThreadSafeAttribute_t2535A209D57BDA2FF398C4CA766059277FC349FE  : public NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270
{
};

// UnityEngine.Touch
struct Touch_t03E51455ED508492B3F278903A0114FA0E87B417 
{
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;
};

// UnityEngine.Timeline.TrackColorAttribute
struct TrackColorAttribute_t7A7D452B7CD382F0F9BE71B0CDECE67BD6BF532C  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// UnityEngine.Color UnityEngine.Timeline.TrackColorAttribute::m_Color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_Color_0;
};

// UnityEngine.TrackedReference
struct TrackedReference_tF35FF4FB6E89ACD81C24469FAF0CA6FFF29262A2  : public RuntimeObject
{
	// System.IntPtr UnityEngine.TrackedReference::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.TrackedReference
struct TrackedReference_tF35FF4FB6E89ACD81C24469FAF0CA6FFF29262A2_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.TrackedReference
struct TrackedReference_tF35FF4FB6E89ACD81C24469FAF0CA6FFF29262A2_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// DG.Tweening.Tweener
struct Tweener_tD38633F1A42EDF47A73CE3BF1894D946E830E140  : public Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C
{
	// System.Boolean DG.Tweening.Tweener::hasManuallySetStartValue
	bool ___hasManuallySetStartValue_55;
	// System.Boolean DG.Tweening.Tweener::isFromAllowed
	bool ___isFromAllowed_56;
};

// UnityEngine.UICharInfo
struct UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD 
{
	// UnityEngine.Vector2 UnityEngine.UICharInfo::cursorPos
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___cursorPos_0;
	// System.Single UnityEngine.UICharInfo::charWidth
	float ___charWidth_1;
};

// UnityEngine.UIVertex
struct UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207 
{
	// UnityEngine.Vector3 UnityEngine.UIVertex::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_0;
	// UnityEngine.Vector3 UnityEngine.UIVertex::normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___normal_1;
	// UnityEngine.Vector4 UnityEngine.UIVertex::tangent
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___tangent_2;
	// UnityEngine.Color32 UnityEngine.UIVertex::color
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color_3;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv0
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv0_4;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv1
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv1_5;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv2
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv2_6;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv3
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv3_7;
};

// UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.UploadHandler::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.XR.XRNodeState
struct XRNodeState_t683158812A1D80A6BC73DB97405BB0B795A9111A 
{
	// UnityEngine.XR.XRNode UnityEngine.XR.XRNodeState::m_Type
	int32_t ___m_Type_0;
	// UnityEngine.XR.AvailableTrackingData UnityEngine.XR.XRNodeState::m_AvailableFields
	int32_t ___m_AvailableFields_1;
	// UnityEngine.Vector3 UnityEngine.XR.XRNodeState::m_Position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Position_2;
	// UnityEngine.Quaternion UnityEngine.XR.XRNodeState::m_Rotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___m_Rotation_3;
	// UnityEngine.Vector3 UnityEngine.XR.XRNodeState::m_Velocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Velocity_4;
	// UnityEngine.Vector3 UnityEngine.XR.XRNodeState::m_AngularVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_AngularVelocity_5;
	// UnityEngine.Vector3 UnityEngine.XR.XRNodeState::m_Acceleration
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Acceleration_6;
	// UnityEngine.Vector3 UnityEngine.XR.XRNodeState::m_AngularAcceleration
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_AngularAcceleration_7;
	// System.Int32 UnityEngine.XR.XRNodeState::m_Tracked
	int32_t ___m_Tracked_8;
	// System.UInt64 UnityEngine.XR.XRNodeState::m_UniqueID
	uint64_t ___m_UniqueID_9;
};

// UnityEngine.Yoga.YogaConfig
struct YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Yoga.YogaConfig::_ygConfig
	intptr_t ____ygConfig_1;
	// UnityEngine.Yoga.Logger UnityEngine.Yoga.YogaConfig::_logger
	Logger_t092B1218ED93DD47180692D5761559B2054234A0* ____logger_2;
};

// UnityEngine.Yoga.YogaNode
struct YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Yoga.YogaNode::_ygNode
	intptr_t ____ygNode_0;
	// UnityEngine.Yoga.YogaConfig UnityEngine.Yoga.YogaNode::_config
	YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345* ____config_1;
	// System.WeakReference UnityEngine.Yoga.YogaNode::_parent
	WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E* ____parent_2;
	// System.Collections.Generic.List`1<UnityEngine.Yoga.YogaNode> UnityEngine.Yoga.YogaNode::_children
	List_1_t84B666107A8A3ECB0F5A24B0243137D056DA9165* ____children_3;
	// UnityEngine.Yoga.MeasureFunction UnityEngine.Yoga.YogaNode::_measureFunction
	MeasureFunction_t60EBED1328F5328D4FA7E26335967E59E73B4D09* ____measureFunction_4;
	// UnityEngine.Yoga.BaselineFunction UnityEngine.Yoga.YogaNode::_baselineFunction
	BaselineFunction_t13AFADEF52F63320B2159C237635948AEB801679* ____baselineFunction_5;
	// System.Object UnityEngine.Yoga.YogaNode::_data
	RuntimeObject* ____data_6;
};

// Unity.Mathematics.float3x3
struct float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 
{
	// Unity.Mathematics.float3 Unity.Mathematics.float3x3::c0
	float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___c0_0;
	// Unity.Mathematics.float3 Unity.Mathematics.float3x3::c1
	float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___c1_1;
	// Unity.Mathematics.float3 Unity.Mathematics.float3x3::c2
	float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___c2_2;
};

// Unity.Mathematics.float4x4
struct float4x4_t7EDD16F7F57DC7F61A6302535F7C19FB97915DF2 
{
	// Unity.Mathematics.float4 Unity.Mathematics.float4x4::c0
	float4_t89D9A294E7A79BD81BFBDD18654508532958555E ___c0_0;
	// Unity.Mathematics.float4 Unity.Mathematics.float4x4::c1
	float4_t89D9A294E7A79BD81BFBDD18654508532958555E ___c1_1;
	// Unity.Mathematics.float4 Unity.Mathematics.float4x4::c2
	float4_t89D9A294E7A79BD81BFBDD18654508532958555E ___c2_2;
	// Unity.Mathematics.float4 Unity.Mathematics.float4x4::c3
	float4_t89D9A294E7A79BD81BFBDD18654508532958555E ___c3_3;
};

// Unity.Mathematics.quaternion
struct quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4 
{
	// Unity.Mathematics.float4 Unity.Mathematics.quaternion::value
	float4_t89D9A294E7A79BD81BFBDD18654508532958555E ___value_0;
};

// Unity.Burst.BurstString/tBigInt
struct tBigInt_t6A436AD3913A2950571338A5018B48B299987358 
{
	// System.Int32 Unity.Burst.BurstString/tBigInt::m_length
	int32_t ___m_length_0;
	// Unity.Burst.BurstString/tBigInt/<m_blocks>e__FixedBuffer Unity.Burst.BurstString/tBigInt::m_blocks
	U3Cm_blocksU3Ee__FixedBuffer_tBBE20C4EF7009465021F0375E2128D5DCFF59F7C ___m_blocks_1;
};

// CartoonFX.CFXR_Effect/CameraShake
struct CameraShake_tF662E3447A81E0F4B337C630E8DDF9758C02DFD0  : public RuntimeObject
{
	// System.Boolean CartoonFX.CFXR_Effect/CameraShake::enabled
	bool ___enabled_1;
	// System.Boolean CartoonFX.CFXR_Effect/CameraShake::useMainCamera
	bool ___useMainCamera_2;
	// System.Collections.Generic.List`1<UnityEngine.Camera> CartoonFX.CFXR_Effect/CameraShake::cameras
	List_1_tD2FA3273746E404D72561E8324608D18B52B533E* ___cameras_3;
	// System.Single CartoonFX.CFXR_Effect/CameraShake::delay
	float ___delay_4;
	// System.Single CartoonFX.CFXR_Effect/CameraShake::duration
	float ___duration_5;
	// CartoonFX.CFXR_Effect/CameraShake/ShakeSpace CartoonFX.CFXR_Effect/CameraShake::shakeSpace
	int32_t ___shakeSpace_6;
	// UnityEngine.Vector3 CartoonFX.CFXR_Effect/CameraShake::shakeStrength
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___shakeStrength_7;
	// UnityEngine.AnimationCurve CartoonFX.CFXR_Effect/CameraShake::shakeCurve
	AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* ___shakeCurve_8;
	// System.Single CartoonFX.CFXR_Effect/CameraShake::shakesDelay
	float ___shakesDelay_9;
	// System.Boolean CartoonFX.CFXR_Effect/CameraShake::isShaking
	bool ___isShaking_10;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Vector3> CartoonFX.CFXR_Effect/CameraShake::cjr
	Dictionary_2_t352A6559C587A201E648C2AD14035DFDD2403AE0* ___cjr_11;
	// UnityEngine.Vector3 CartoonFX.CFXR_Effect/CameraShake::cjs
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___cjs_12;
	// System.Single CartoonFX.CFXR_Effect/CameraShake::cjt
	float ___cjt_13;
};

// DG.Tweening.DOTweenModulePhysics/dh
struct dh_t39B924AB8F118682E4493B650D6FBF5B8D0F9EE7  : public RuntimeObject
{
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/dh::target
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___target_0;
	// System.Single DG.Tweening.DOTweenModulePhysics/dh::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModulePhysics/dh::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.DOTweenModulePhysics/dh::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics/dh::s
	Sequence_tEADBE56D6ED2E9EE8FB2E5459C3E57131EC0545C* ___s_4;
	// UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/dh::endValue
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___endValue_5;
	// DG.Tweening.Tween DG.Tweening.DOTweenModulePhysics/dh::yTween
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___yTween_6;
};

// DG.Tweening.DOTweenModulePhysics2D/dq
struct dq_t56D790A1C052919142A84179CA25C65BBCCE0DD7  : public RuntimeObject
{
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/dq::target
	Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F* ___target_0;
	// System.Single DG.Tweening.DOTweenModulePhysics2D/dq::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModulePhysics2D/dq::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.DOTweenModulePhysics2D/dq::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics2D/dq::s
	Sequence_tEADBE56D6ED2E9EE8FB2E5459C3E57131EC0545C* ___s_4;
	// UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/dq::endValue
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___endValue_5;
	// DG.Tweening.Tween DG.Tweening.DOTweenModulePhysics2D/dq::yTween
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___yTween_6;
};

// DG.Tweening.DOTweenModuleSprite/dx
struct dx_tF1865E98745F606AB4614C0666E92C7BB46B48B4  : public RuntimeObject
{
	// UnityEngine.Color DG.Tweening.DOTweenModuleSprite/dx::to
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___to_0;
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite/dx::target
	SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* ___target_1;
};

// DG.Tweening.DOTweenModuleUI/fa
struct fa_tE9B1832901B936CFA7EBAEA5B94E8A5F2C77BD5D  : public RuntimeObject
{
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/fa::target
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___target_0;
	// System.Single DG.Tweening.DOTweenModuleUI/fa::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModuleUI/fa::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.DOTweenModuleUI/fa::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI/fa::s
	Sequence_tEADBE56D6ED2E9EE8FB2E5459C3E57131EC0545C* ___s_4;
	// UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/fa::endValue
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___endValue_5;
};

// DG.Tweening.DOTweenModuleUI/fj
struct fj_tD4EA578918F20DF40CC055CE3290963BF22105A8  : public RuntimeObject
{
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI/fj::to
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___to_0;
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI/fj::target
	Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* ___target_1;
};

// DG.Tweening.DOTweenModuleUI/fk
struct fk_t0B3E21AA1C3FBA9811CFEFC4AB67465CC86610A2  : public RuntimeObject
{
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI/fk::to
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___to_0;
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/fk::target
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___target_1;
};

// DG.Tweening.DOTweenModuleUI/fl
struct fl_t1487064AB5A0B201B00A9D9DAE3743391E7F882C  : public RuntimeObject
{
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI/fl::to
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___to_0;
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/fl::target
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___target_1;
};

// UnityEngine.ParticleSystem/MinMaxGradient
struct MinMaxGradient_tFF31B8EC2855D0074AB86E8B37BEA6609070AC69 
{
	// UnityEngine.ParticleSystemGradientMode UnityEngine.ParticleSystem/MinMaxGradient::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.Gradient UnityEngine.ParticleSystem/MinMaxGradient::m_GradientMin
	Gradient_tA7FEBE2FDB4929FFF6C997134841046F713DAC1E* ___m_GradientMin_1;
	// UnityEngine.Gradient UnityEngine.ParticleSystem/MinMaxGradient::m_GradientMax
	Gradient_tA7FEBE2FDB4929FFF6C997134841046F713DAC1E* ___m_GradientMax_2;
	// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::m_ColorMin
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_ColorMin_3;
	// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::m_ColorMax
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_ColorMax_4;
};

// UnityEngine.ParticleSystem/Particle
struct Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D 
{
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Position_0;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Velocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Velocity_1;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AnimatedVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_AnimatedVelocity_2;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_InitialVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_InitialVelocity_3;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AxisOfRotation
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_AxisOfRotation_4;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Rotation
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Rotation_5;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AngularVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_AngularVelocity_6;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_StartSize
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_StartSize_7;
	// UnityEngine.Color32 UnityEngine.ParticleSystem/Particle::m_StartColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_StartColor_8;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_RandomSeed
	uint32_t ___m_RandomSeed_9;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_ParentRandomSeed
	uint32_t ___m_ParentRandomSeed_10;
	// System.Single UnityEngine.ParticleSystem/Particle::m_Lifetime
	float ___m_Lifetime_11;
	// System.Single UnityEngine.ParticleSystem/Particle::m_StartLifetime
	float ___m_StartLifetime_12;
	// System.Int32 UnityEngine.ParticleSystem/Particle::m_MeshIndex
	int32_t ___m_MeshIndex_13;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator0
	float ___m_EmitAccumulator0_14;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator1
	float ___m_EmitAccumulator1_15;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_Flags
	uint32_t ___m_Flags_16;
};

// UnityEngine.Rendering.Universal.ShaderInput/LightData
struct LightData_tAC4023737E9903DE3F96B993AA323E062ABCB9ED 
{
	// UnityEngine.Vector4 UnityEngine.Rendering.Universal.ShaderInput/LightData::position
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___position_0;
	// UnityEngine.Vector4 UnityEngine.Rendering.Universal.ShaderInput/LightData::color
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___color_1;
	// UnityEngine.Vector4 UnityEngine.Rendering.Universal.ShaderInput/LightData::attenuation
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___attenuation_2;
	// UnityEngine.Vector4 UnityEngine.Rendering.Universal.ShaderInput/LightData::spotDirection
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___spotDirection_3;
	// UnityEngine.Vector4 UnityEngine.Rendering.Universal.ShaderInput/LightData::occlusionProbeChannels
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___occlusionProbeChannels_4;
	// System.UInt32 UnityEngine.Rendering.Universal.ShaderInput/LightData::layerMask
	uint32_t ___layerMask_5;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0
struct U3CU3Ec__DisplayClass61_0_t1DDEF1BD80CEADC19DCEEB7FCC30C90CC2013BC9  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0::s
	Sequence_tEADBE56D6ED2E9EE8FB2E5459C3E57131EC0545C* ___s_4;
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0::endValue
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___endValue_5;
	// DG.Tweening.Tween DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0::yTween
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___yTween_6;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0
struct U3CU3Ec__DisplayClass62_0_t57FD1DA46F208298B66B340BF2DFDF55DBFEA05E  : public RuntimeObject
{
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_0;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0::s
	Sequence_tEADBE56D6ED2E9EE8FB2E5459C3E57131EC0545C* ___s_4;
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0::endValue
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___endValue_5;
	// DG.Tweening.Tween DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0::yTween
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___yTween_6;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0
struct U3CU3Ec__DisplayClass68_0_tDB8CAB5A1E37C025A29F8F2995410319626E4800  : public RuntimeObject
{
	// UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::to
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___to_0;
	// UnityEngine.Light DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::target
	Light_t1E68479B7782AF2050FAA02A5DC612FD034F18F3* ___target_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0
struct U3CU3Ec__DisplayClass69_0_tFE24BA7AB38BFBD74C8E70CEA97273EF9B6FFAF7  : public RuntimeObject
{
	// UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::to
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___to_0;
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0
struct U3CU3Ec__DisplayClass70_0_tB2447C0E75B7AB47789EC0AE2C5D7CDCB11CD89E  : public RuntimeObject
{
	// UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0::to
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___to_0;
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_1;
	// System.String DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0::property
	String_t* ___property_2;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass71_0
struct U3CU3Ec__DisplayClass71_0_tD24AB956CC7388BA7A4EB5D71C62A66A84F1693E  : public RuntimeObject
{
	// UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass71_0::to
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___to_0;
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass71_0::target
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___target_1;
	// System.Int32 DG.Tweening.ShortcutExtensions/<>c__DisplayClass71_0::propertyID
	int32_t ___propertyID_2;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0
struct U3CU3Ec__DisplayClass72_0_t53244858FA69DF933AF4D7258FCAC1FA41F61EFB  : public RuntimeObject
{
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0::to
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass73_0
struct U3CU3Ec__DisplayClass73_0_tEC070381D793BF76FDF71D5C7BD804835ED51A75  : public RuntimeObject
{
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass73_0::to
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass73_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass74_0
struct U3CU3Ec__DisplayClass74_0_tA32299450196DE927349F74CEF3DAFB9A6AD3B98  : public RuntimeObject
{
	// UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass74_0::to
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass74_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0
struct U3CU3Ec__DisplayClass75_0_t0D7AC3252650CC5E98C4E309BB3816AB9EF60FB0  : public RuntimeObject
{
	// UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0::to
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0
struct U3CU3Ec__DisplayClass76_0_t45CD5765778EB0A4FA81C848AF6D3B62E9B62B75  : public RuntimeObject
{
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0::to
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_1;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0
struct U3CU3Ec__DisplayClass77_0_tFEF93DDADD61E664BA439598ED08588F2F927544  : public RuntimeObject
{
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0::to
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0::target
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___target_1;
};

// UnityEngine.XR.XRDisplaySubsystem/XRBlitParams
struct XRBlitParams_tB6F9130166CF43540F2547163E8542CFC7266CD9 
{
	// UnityEngine.RenderTexture UnityEngine.XR.XRDisplaySubsystem/XRBlitParams::srcTex
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___srcTex_0;
	// System.Int32 UnityEngine.XR.XRDisplaySubsystem/XRBlitParams::srcTexArraySlice
	int32_t ___srcTexArraySlice_1;
	// UnityEngine.Rect UnityEngine.XR.XRDisplaySubsystem/XRBlitParams::srcRect
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___srcRect_2;
	// UnityEngine.Rect UnityEngine.XR.XRDisplaySubsystem/XRBlitParams::destRect
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___destRect_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.XRDisplaySubsystem/XRBlitParams
struct XRBlitParams_tB6F9130166CF43540F2547163E8542CFC7266CD9_marshaled_pinvoke
{
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___srcTex_0;
	int32_t ___srcTexArraySlice_1;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___srcRect_2;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___destRect_3;
};
// Native definition for COM marshalling of UnityEngine.XR.XRDisplaySubsystem/XRBlitParams
struct XRBlitParams_tB6F9130166CF43540F2547163E8542CFC7266CD9_marshaled_com
{
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___srcTex_0;
	int32_t ___srcTexArraySlice_1;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___srcRect_2;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___destRect_3;
};

// UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc
struct XRMirrorViewBlitDesc_tC913B2856BA2160BC3AD99F0C67293850E2162E0 
{
	// System.IntPtr UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc::displaySubsystemInstance
	intptr_t ___displaySubsystemInstance_0;
	// System.Boolean UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc::nativeBlitAvailable
	bool ___nativeBlitAvailable_1;
	// System.Boolean UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc::nativeBlitInvalidStates
	bool ___nativeBlitInvalidStates_2;
	// System.Int32 UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc::blitParamsCount
	int32_t ___blitParamsCount_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc
struct XRMirrorViewBlitDesc_tC913B2856BA2160BC3AD99F0C67293850E2162E0_marshaled_pinvoke
{
	intptr_t ___displaySubsystemInstance_0;
	int32_t ___nativeBlitAvailable_1;
	int32_t ___nativeBlitInvalidStates_2;
	int32_t ___blitParamsCount_3;
};
// Native definition for COM marshalling of UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc
struct XRMirrorViewBlitDesc_tC913B2856BA2160BC3AD99F0C67293850E2162E0_marshaled_com
{
	intptr_t ___displaySubsystemInstance_0;
	int32_t ___nativeBlitAvailable_1;
	int32_t ___nativeBlitInvalidStates_2;
	int32_t ___blitParamsCount_3;
};

// UnityEngine.XR.XRDisplaySubsystem/XRRenderParameter
struct XRRenderParameter_t0C786C9DBEFFCAD6204039BD181B412B69F95260 
{
	// UnityEngine.Matrix4x4 UnityEngine.XR.XRDisplaySubsystem/XRRenderParameter::view
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___view_0;
	// UnityEngine.Matrix4x4 UnityEngine.XR.XRDisplaySubsystem/XRRenderParameter::projection
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___projection_1;
	// UnityEngine.Rect UnityEngine.XR.XRDisplaySubsystem/XRRenderParameter::viewport
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___viewport_2;
	// UnityEngine.Mesh UnityEngine.XR.XRDisplaySubsystem/XRRenderParameter::occlusionMesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___occlusionMesh_3;
	// System.Int32 UnityEngine.XR.XRDisplaySubsystem/XRRenderParameter::textureArraySlice
	int32_t ___textureArraySlice_4;
	// UnityEngine.Matrix4x4 UnityEngine.XR.XRDisplaySubsystem/XRRenderParameter::previousView
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___previousView_5;
	// System.Boolean UnityEngine.XR.XRDisplaySubsystem/XRRenderParameter::isPreviousViewValid
	bool ___isPreviousViewValid_6;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.XRDisplaySubsystem/XRRenderParameter
struct XRRenderParameter_t0C786C9DBEFFCAD6204039BD181B412B69F95260_marshaled_pinvoke
{
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___view_0;
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___projection_1;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___viewport_2;
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___occlusionMesh_3;
	int32_t ___textureArraySlice_4;
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___previousView_5;
	int32_t ___isPreviousViewValid_6;
};
// Native definition for COM marshalling of UnityEngine.XR.XRDisplaySubsystem/XRRenderParameter
struct XRRenderParameter_t0C786C9DBEFFCAD6204039BD181B412B69F95260_marshaled_com
{
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___view_0;
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___projection_1;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___viewport_2;
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___occlusionMesh_3;
	int32_t ___textureArraySlice_4;
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___previousView_5;
	int32_t ___isPreviousViewValid_6;
};

// UnityEngine.XR.XRMeshSubsystem/MeshTransformList
struct MeshTransformList_t761D725D4B30CFD7DDF57B3725004994FB3B561F 
{
	// System.IntPtr UnityEngine.XR.XRMeshSubsystem/MeshTransformList::m_Self
	intptr_t ___m_Self_0;
};

// UnityEngine.IntegratedSubsystemDescriptor`1<UnityEngine.XR.XRDisplaySubsystem>
struct IntegratedSubsystemDescriptor_1_t7261AA0914165CB589AD41C4F9B463D44E333D7C  : public IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D
{
};
// Native definition for P/Invoke marshalling of UnityEngine.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_pinvoke_define
#define IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_pinvoke_define
struct IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_pinvoke : public IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D_marshaled_pinvoke
{
};
#endif
// Native definition for COM marshalling of UnityEngine.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_com_define
#define IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_com_define
struct IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_com : public IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D_marshaled_com
{
};
#endif

// UnityEngine.IntegratedSubsystemDescriptor`1<UnityEngine.XR.XRInputSubsystem>
struct IntegratedSubsystemDescriptor_1_t440D086A98F968B9FC279854F552698F319A32F7  : public IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D
{
};
// Native definition for P/Invoke marshalling of UnityEngine.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_pinvoke_define
#define IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_pinvoke_define
struct IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_pinvoke : public IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D_marshaled_pinvoke
{
};
#endif
// Native definition for COM marshalling of UnityEngine.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_com_define
#define IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_com_define
struct IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_com : public IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D_marshaled_com
{
};
#endif

// UnityEngine.IntegratedSubsystemDescriptor`1<UnityEngine.XR.XRMeshSubsystem>
struct IntegratedSubsystemDescriptor_1_tBFE1680F4FA360D4E9AB19FDE2CCBD959E436513  : public IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D
{
};
// Native definition for P/Invoke marshalling of UnityEngine.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_pinvoke_define
#define IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_pinvoke_define
struct IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_pinvoke : public IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D_marshaled_pinvoke
{
};
#endif
// Native definition for COM marshalling of UnityEngine.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_com_define
#define IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_com_define
struct IntegratedSubsystemDescriptor_1_tC541D17A8306FA1C3A608A1328A6DBFDA3264671_marshaled_com : public IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D_marshaled_com
{
};
#endif

// UnityEngine.IntegratedSubsystem`1<UnityEngine.XR.XRDisplaySubsystemDescriptor>
struct IntegratedSubsystem_1_t8312865F01EEA1EDE4B24A973E47ADD526616848  : public IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3
{
};

// UnityEngine.IntegratedSubsystem`1<UnityEngine.XR.XRInputSubsystemDescriptor>
struct IntegratedSubsystem_1_tF93BC76362E85BDD215312162457BE510FC76D3B  : public IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3
{
};

// UnityEngine.IntegratedSubsystem`1<UnityEngine.XR.XRMeshSubsystemDescriptor>
struct IntegratedSubsystem_1_t3AB3A9BAB10A4D1420E63EEA50D40FEE65F41C03  : public IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3
{
};

// UnityEngine.Animations.AnimationClipPlayable
struct AnimationClipPlayable_t54CEA0DD315B1674C2BD49E681005C4271D73969 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationClipPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.AnimationEvent
struct AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174  : public RuntimeObject
{
	// System.Single UnityEngine.AnimationEvent::m_Time
	float ___m_Time_0;
	// System.String UnityEngine.AnimationEvent::m_FunctionName
	String_t* ___m_FunctionName_1;
	// System.String UnityEngine.AnimationEvent::m_StringParameter
	String_t* ___m_StringParameter_2;
	// UnityEngine.Object UnityEngine.AnimationEvent::m_ObjectReferenceParameter
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___m_ObjectReferenceParameter_3;
	// System.Single UnityEngine.AnimationEvent::m_FloatParameter
	float ___m_FloatParameter_4;
	// System.Int32 UnityEngine.AnimationEvent::m_IntParameter
	int32_t ___m_IntParameter_5;
	// System.Int32 UnityEngine.AnimationEvent::m_MessageOptions
	int32_t ___m_MessageOptions_6;
	// UnityEngine.AnimationEventSource UnityEngine.AnimationEvent::m_Source
	int32_t ___m_Source_7;
	// UnityEngine.AnimationState UnityEngine.AnimationEvent::m_StateSender
	AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE* ___m_StateSender_8;
	// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::m_AnimatorStateInfo
	AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2 ___m_AnimatorStateInfo_9;
	// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::m_AnimatorClipInfo
	AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03 ___m_AnimatorClipInfo_10;
};
// Native definition for P/Invoke marshalling of UnityEngine.AnimationEvent
struct AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174_marshaled_pinvoke
{
	float ___m_Time_0;
	char* ___m_FunctionName_1;
	char* ___m_StringParameter_2;
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke ___m_ObjectReferenceParameter_3;
	float ___m_FloatParameter_4;
	int32_t ___m_IntParameter_5;
	int32_t ___m_MessageOptions_6;
	int32_t ___m_Source_7;
	AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE* ___m_StateSender_8;
	AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2 ___m_AnimatorStateInfo_9;
	AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03 ___m_AnimatorClipInfo_10;
};
// Native definition for COM marshalling of UnityEngine.AnimationEvent
struct AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174_marshaled_com
{
	float ___m_Time_0;
	Il2CppChar* ___m_FunctionName_1;
	Il2CppChar* ___m_StringParameter_2;
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com* ___m_ObjectReferenceParameter_3;
	float ___m_FloatParameter_4;
	int32_t ___m_IntParameter_5;
	int32_t ___m_MessageOptions_6;
	int32_t ___m_Source_7;
	AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE* ___m_StateSender_8;
	AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2 ___m_AnimatorStateInfo_9;
	AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03 ___m_AnimatorClipInfo_10;
};

// UnityEngine.Animations.AnimationLayerMixerPlayable
struct AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationMixerPlayable
struct AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMixerPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationMotionXToDeltaPlayable
struct AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMotionXToDeltaPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationOffsetPlayable
struct AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationOffsetPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationPlayableOutput
struct AnimationPlayableOutput_t753AC95DC826789BC537D18449E93114777DDF4E 
{
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Animations.AnimationPlayableOutput::m_Handle
	PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationPosePlayable
struct AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationPosePlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationRemoveScalePlayable
struct AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationRemoveScalePlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationScriptPlayable
struct AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationScriptPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.AnimationState
struct AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE  : public TrackedReference_tF35FF4FB6E89ACD81C24469FAF0CA6FFF29262A2
{
};

// UnityEngine.Animations.AnimatorControllerPlayable
struct AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimatorControllerPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06 
{
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::m_builder
	AsyncTaskMethodBuilder_1_tE88892A6B2F97B5D44B7C3EE2DBEED85743412AC ___m_builder_1;
};
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06_marshaled_pinvoke
{
	AsyncTaskMethodBuilder_1_tE88892A6B2F97B5D44B7C3EE2DBEED85743412AC ___m_builder_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06_marshaled_com
{
	AsyncTaskMethodBuilder_1_tE88892A6B2F97B5D44B7C3EE2DBEED85743412AC ___m_builder_1;
};

// UnityEngine.AudioClip
struct AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t3396D9613664F0AFF65FB91018FD0F901CC16F1E* ___m_PCMReaderCallback_4;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t8D7135A2FB40647CAEC93F5254AD59E18DEB6072* ___m_PCMSetPositionCallback_5;
};

// UnityEngine.Audio.AudioClipPlayable
struct AudioClipPlayable_tD4B758E68CAE03CB0CD31F90C8A3E603B97143A0 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Audio.AudioMixer
struct AudioMixer_tE2E8D79241711CDF9AB428C7FB96A35D80E40B04  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Audio.AudioMixerPlayable
struct AudioMixerPlayable_t6AADDF0C53DF1B4C17969EC24B3B4E4975F3A56C 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Audio.AudioPlayableOutput
struct AudioPlayableOutput_tC3DFF8095F429D90129A367EAB98A24F6D6ADF20 
{
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Audio.AudioPlayableOutput::m_Handle
	PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 ___m_Handle_0;
};

// UnityEngine.Avatar
struct Avatar_t7861E57EEE2CF8CC61BD63C09737BA22F7ABCA0F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.AvatarMask
struct AvatarMask_tC1D777FFB77C952502ECF6D80FAFAD16B27B02AF  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974  : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB
{
	// Unity.Collections.NativeArray`1<System.Byte> UnityEngine.Networking.DownloadHandlerBuffer::m_NativeData
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974_marshaled_pinvoke : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974_marshaled_com : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
};

// UnityEngine.ExitGUIException
struct ExitGUIException_tFF2EEEBACD9E5684D6112478EEF754B74D154549  : public Exception_t
{
};

// UnityEngine.Font
struct Font_tC95270EA3198038970422D78B74A7F2E218A96B6  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t76D5E172DF8AA57E67763D453AAC40F0961D09B1* ___m_FontTextureRebuildCallback_5;
};

// UnityEngine.GUILayoutGroup
struct GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D  : public GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F
{
	// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry> UnityEngine.GUILayoutGroup::entries
	List_1_tA5BCD116CC751A5F35C7D3D7B96DC3A5D22B9C82* ___entries_11;
	// System.Boolean UnityEngine.GUILayoutGroup::isVertical
	bool ___isVertical_12;
	// System.Boolean UnityEngine.GUILayoutGroup::resetCoords
	bool ___resetCoords_13;
	// System.Single UnityEngine.GUILayoutGroup::spacing
	float ___spacing_14;
	// System.Boolean UnityEngine.GUILayoutGroup::sameSize
	bool ___sameSize_15;
	// System.Boolean UnityEngine.GUILayoutGroup::isWindow
	bool ___isWindow_16;
	// System.Int32 UnityEngine.GUILayoutGroup::windowID
	int32_t ___windowID_17;
	// System.Int32 UnityEngine.GUILayoutGroup::m_Cursor
	int32_t ___m_Cursor_18;
	// System.Int32 UnityEngine.GUILayoutGroup::m_StretchableCountX
	int32_t ___m_StretchableCountX_19;
	// System.Int32 UnityEngine.GUILayoutGroup::m_StretchableCountY
	int32_t ___m_StretchableCountY_20;
	// System.Boolean UnityEngine.GUILayoutGroup::m_UserSpecifiedWidth
	bool ___m_UserSpecifiedWidth_21;
	// System.Boolean UnityEngine.GUILayoutGroup::m_UserSpecifiedHeight
	bool ___m_UserSpecifiedHeight_22;
	// System.Single UnityEngine.GUILayoutGroup::m_ChildMinWidth
	float ___m_ChildMinWidth_23;
	// System.Single UnityEngine.GUILayoutGroup::m_ChildMaxWidth
	float ___m_ChildMaxWidth_24;
	// System.Single UnityEngine.GUILayoutGroup::m_ChildMinHeight
	float ___m_ChildMinHeight_25;
	// System.Single UnityEngine.GUILayoutGroup::m_ChildMaxHeight
	float ___m_ChildMaxHeight_26;
	// System.Int32 UnityEngine.GUILayoutGroup::m_MarginLeft
	int32_t ___m_MarginLeft_27;
	// System.Int32 UnityEngine.GUILayoutGroup::m_MarginRight
	int32_t ___m_MarginRight_28;
	// System.Int32 UnityEngine.GUILayoutGroup::m_MarginTop
	int32_t ___m_MarginTop_29;
	// System.Int32 UnityEngine.GUILayoutGroup::m_MarginBottom
	int32_t ___m_MarginBottom_30;
};

// UnityEngine.GUIStyle
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580  : public RuntimeObject
{
	// System.IntPtr UnityEngine.GUIStyle::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Normal
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_Normal_1;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Hover
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_Hover_2;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Active
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_Active_3;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Focused
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_Focused_4;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnNormal
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_OnNormal_5;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnHover
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_OnHover_6;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnActive
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_OnActive_7;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnFocused
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95* ___m_OnFocused_8;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Border
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5* ___m_Border_9;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Padding
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5* ___m_Padding_10;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Margin
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5* ___m_Margin_11;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Overflow
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5* ___m_Overflow_12;
	// System.String UnityEngine.GUIStyle::m_Name
	String_t* ___m_Name_13;
};
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyle
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_Normal_1;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_Hover_2;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_Active_3;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_Focused_4;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_OnNormal_5;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_OnHover_6;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_OnActive_7;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_pinvoke* ___m_OnFocused_8;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_pinvoke ___m_Border_9;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_pinvoke ___m_Padding_10;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_pinvoke ___m_Margin_11;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_pinvoke ___m_Overflow_12;
	char* ___m_Name_13;
};
// Native definition for COM marshalling of UnityEngine.GUIStyle
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_Normal_1;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_Hover_2;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_Active_3;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_Focused_4;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_OnNormal_5;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_OnHover_6;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_OnActive_7;
	GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95_marshaled_com* ___m_OnFocused_8;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com* ___m_Border_9;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com* ___m_Padding_10;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com* ___m_Margin_11;
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5_marshaled_com* ___m_Overflow_12;
	Il2CppChar* ___m_Name_13;
};

// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord
struct GlyphPairAdjustmentRecord_t6E4295094D349DBF22BC59116FBC8F22EA55420E 
{
	// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord::m_FirstAdjustmentRecord
	GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7 ___m_FirstAdjustmentRecord_0;
	// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord::m_SecondAdjustmentRecord
	GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7 ___m_SecondAdjustmentRecord_1;
	// UnityEngine.TextCore.LowLevel.FontFeatureLookupFlags UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord::m_FeatureLookupFlags
	int32_t ___m_FeatureLookupFlags_2;
};

// UnityEngine.HumanBone
struct HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8 
{
	// System.String UnityEngine.HumanBone::m_BoneName
	String_t* ___m_BoneName_0;
	// System.String UnityEngine.HumanBone::m_HumanName
	String_t* ___m_HumanName_1;
	// UnityEngine.HumanLimit UnityEngine.HumanBone::limit
	HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E ___limit_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.HumanBone
struct HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_pinvoke
{
	char* ___m_BoneName_0;
	char* ___m_HumanName_1;
	HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E ___limit_2;
};
// Native definition for COM marshalling of UnityEngine.HumanBone
struct HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_com
{
	Il2CppChar* ___m_BoneName_0;
	Il2CppChar* ___m_HumanName_1;
	HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E ___limit_2;
};

// UnityEngine.TextCore.Text.LineInfo
struct LineInfo_t2BBD461B330C46ACA45596A8E72FEA4172F88CF5 
{
	// System.Int32 UnityEngine.TextCore.Text.LineInfo::controlCharacterCount
	int32_t ___controlCharacterCount_0;
	// System.Int32 UnityEngine.TextCore.Text.LineInfo::characterCount
	int32_t ___characterCount_1;
	// System.Int32 UnityEngine.TextCore.Text.LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 UnityEngine.TextCore.Text.LineInfo::spaceCount
	int32_t ___spaceCount_3;
	// System.Int32 UnityEngine.TextCore.Text.LineInfo::visibleSpaceCount
	int32_t ___visibleSpaceCount_4;
	// System.Int32 UnityEngine.TextCore.Text.LineInfo::wordCount
	int32_t ___wordCount_5;
	// System.Int32 UnityEngine.TextCore.Text.LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_6;
	// System.Int32 UnityEngine.TextCore.Text.LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_7;
	// System.Int32 UnityEngine.TextCore.Text.LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_8;
	// System.Int32 UnityEngine.TextCore.Text.LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_9;
	// System.Single UnityEngine.TextCore.Text.LineInfo::length
	float ___length_10;
	// System.Single UnityEngine.TextCore.Text.LineInfo::lineHeight
	float ___lineHeight_11;
	// System.Single UnityEngine.TextCore.Text.LineInfo::ascender
	float ___ascender_12;
	// System.Single UnityEngine.TextCore.Text.LineInfo::baseline
	float ___baseline_13;
	// System.Single UnityEngine.TextCore.Text.LineInfo::descender
	float ___descender_14;
	// System.Single UnityEngine.TextCore.Text.LineInfo::maxAdvance
	float ___maxAdvance_15;
	// System.Single UnityEngine.TextCore.Text.LineInfo::width
	float ___width_16;
	// System.Single UnityEngine.TextCore.Text.LineInfo::marginLeft
	float ___marginLeft_17;
	// System.Single UnityEngine.TextCore.Text.LineInfo::marginRight
	float ___marginRight_18;
	// UnityEngine.TextCore.Text.TextAlignment UnityEngine.TextCore.Text.LineInfo::alignment
	int32_t ___alignment_19;
	// UnityEngine.TextCore.Text.Extents UnityEngine.TextCore.Text.LineInfo::lineExtents
	Extents_t369FB2B84521A0229C2FA3D4C8592B14E07CEFE6 ___lineExtents_20;
};

// UnityEngine.Motion
struct Motion_tBCD49FBF5608AD21FC03B63C8182FABCEF2707AC  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
	// System.Boolean UnityEngine.Motion::<isAnimatorMotion>k__BackingField
	bool ___U3CisAnimatorMotionU3Ek__BackingField_4;
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// DG.Tweening.Plugins.Core.PathCore.Path
struct Path_t6EC35555EF601CAFED947AC467DEBA7C1496A0C3  : public RuntimeObject
{
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::wpLengths
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___wpLengths_3;
	// UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.Path::wps
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___wps_4;
	// DG.Tweening.PathType DG.Tweening.Plugins.Core.PathCore.Path::type
	int32_t ___type_5;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::subdivisionsXSegment
	int32_t ___subdivisionsXSegment_6;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::subdivisions
	int32_t ___subdivisions_7;
	// DG.Tweening.Plugins.Core.PathCore.ControlPoint[] DG.Tweening.Plugins.Core.PathCore.Path::controlPoints
	ControlPointU5BU5D_t52F9D1EC70E441ED3915E30FFB75F9B95AD56C59* ___controlPoints_8;
	// System.Single DG.Tweening.Plugins.Core.PathCore.Path::length
	float ___length_9;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::isFinalized
	bool ___isFinalized_10;
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::timesTable
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___timesTable_11;
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::lengthsTable
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___lengthsTable_12;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::linearWPIndex
	int32_t ___linearWPIndex_13;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::addedExtraStartWp
	bool ___addedExtraStartWp_14;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::addedExtraEndWp
	bool ___addedExtraEndWp_15;
	// DG.Tweening.Plugins.Options.PathOptions DG.Tweening.Plugins.Core.PathCore.Path::plugOptions
	PathOptions_t76F1CBAC082454349D530B799121EB15BFA4CB3A ___plugOptions_16;
	// DG.Tweening.Plugins.Core.PathCore.Path DG.Tweening.Plugins.Core.PathCore.Path::_incrementalClone
	Path_t6EC35555EF601CAFED947AC467DEBA7C1496A0C3* ____incrementalClone_17;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::_incrementalIndex
	int32_t ____incrementalIndex_18;
	// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder DG.Tweening.Plugins.Core.PathCore.Path::_decoder
	ABSPathDecoder_t6B479550CEF6C183ACCA13F11E29E019270AB61C* ____decoder_19;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::_changed
	bool ____changed_20;
	// UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.Path::nonLinearDrawWps
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___nonLinearDrawWps_21;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.Path::targetPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___targetPosition_22;
	// System.Nullable`1<UnityEngine.Vector3> DG.Tweening.Plugins.Core.PathCore.Path::lookAtPosition
	Nullable_1_t9C51B084784B716FFF4ED4575C63CFD8A71A86FE ___lookAtPosition_23;
	// UnityEngine.Color DG.Tweening.Plugins.Core.PathCore.Path::gizmoColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___gizmoColor_24;
};

// UnityEngine.PhysicsMaterial2D
struct PhysicsMaterial2D_t20AD48FB40C1BED4689A8135E81015703323C065  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Playables.Playable
struct Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Playables.PlayableBinding
struct PlayableBinding_tB68B3BAC47F4F4C559640472174D5BEF93CB6AB4 
{
	// System.String UnityEngine.Playables.PlayableBinding::m_StreamName
	String_t* ___m_StreamName_0;
	// UnityEngine.Object UnityEngine.Playables.PlayableBinding::m_SourceObject
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___m_SourceObject_1;
	// System.Type UnityEngine.Playables.PlayableBinding::m_SourceBindingType
	Type_t* ___m_SourceBindingType_2;
	// UnityEngine.Playables.PlayableBinding/CreateOutputMethod UnityEngine.Playables.PlayableBinding::m_CreateOutputMethod
	CreateOutputMethod_tD18AFE3B69E6DDD913D82D5FA1D5D909CEEC8509* ___m_CreateOutputMethod_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_tB68B3BAC47F4F4C559640472174D5BEF93CB6AB4_marshaled_pinvoke
{
	char* ___m_StreamName_0;
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke ___m_SourceObject_1;
	Type_t* ___m_SourceBindingType_2;
	Il2CppMethodPointer ___m_CreateOutputMethod_3;
};
// Native definition for COM marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_tB68B3BAC47F4F4C559640472174D5BEF93CB6AB4_marshaled_com
{
	Il2CppChar* ___m_StreamName_0;
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com* ___m_SourceObject_1;
	Type_t* ___m_SourceBindingType_2;
	Il2CppMethodPointer ___m_CreateOutputMethod_3;
};

// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t6F7C753402B42EC23C163099CF935C5E0D7A7254  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_pinvoke : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_com : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
};

// UnityEngine.TextCore.Text.SpriteGlyph
struct SpriteGlyph_t0BD62F6EB8D19B2C4B246BC436A8F4BF2E0ACA1A  : public Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F
{
	// UnityEngine.Sprite UnityEngine.TextCore.Text.SpriteGlyph::sprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___sprite_6;
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// UnityEngine.TextCore.Text.TextElementInfo
struct TextElementInfo_tDD7A12E319505510E0B350E342BD55F32AB5F976 
{
	// System.Char UnityEngine.TextCore.Text.TextElementInfo::character
	Il2CppChar ___character_0;
	// System.Int32 UnityEngine.TextCore.Text.TextElementInfo::index
	int32_t ___index_1;
	// UnityEngine.TextCore.Text.TextElementType UnityEngine.TextCore.Text.TextElementInfo::elementType
	uint8_t ___elementType_2;
	// UnityEngine.TextCore.Text.TextElement UnityEngine.TextCore.Text.TextElementInfo::textElement
	TextElement_tCEF567A8810788262275B39DC39CBA6EBE7472DA* ___textElement_3;
	// UnityEngine.TextCore.Text.FontAsset UnityEngine.TextCore.Text.TextElementInfo::fontAsset
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___fontAsset_4;
	// UnityEngine.TextCore.Text.SpriteAsset UnityEngine.TextCore.Text.TextElementInfo::spriteAsset
	SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313* ___spriteAsset_5;
	// System.Int32 UnityEngine.TextCore.Text.TextElementInfo::spriteIndex
	int32_t ___spriteIndex_6;
	// UnityEngine.Material UnityEngine.TextCore.Text.TextElementInfo::material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_7;
	// System.Int32 UnityEngine.TextCore.Text.TextElementInfo::materialReferenceIndex
	int32_t ___materialReferenceIndex_8;
	// System.Boolean UnityEngine.TextCore.Text.TextElementInfo::isUsingAlternateTypeface
	bool ___isUsingAlternateTypeface_9;
	// System.Single UnityEngine.TextCore.Text.TextElementInfo::pointSize
	float ___pointSize_10;
	// System.Int32 UnityEngine.TextCore.Text.TextElementInfo::lineNumber
	int32_t ___lineNumber_11;
	// System.Int32 UnityEngine.TextCore.Text.TextElementInfo::pageNumber
	int32_t ___pageNumber_12;
	// System.Int32 UnityEngine.TextCore.Text.TextElementInfo::vertexIndex
	int32_t ___vertexIndex_13;
	// UnityEngine.TextCore.Text.TextVertex UnityEngine.TextCore.Text.TextElementInfo::vertexTopLeft
	TextVertex_tF030A16DC67EAF3F6C9C9C0564D4B88758B173A9 ___vertexTopLeft_14;
	// UnityEngine.TextCore.Text.TextVertex UnityEngine.TextCore.Text.TextElementInfo::vertexBottomLeft
	TextVertex_tF030A16DC67EAF3F6C9C9C0564D4B88758B173A9 ___vertexBottomLeft_15;
	// UnityEngine.TextCore.Text.TextVertex UnityEngine.TextCore.Text.TextElementInfo::vertexTopRight
	TextVertex_tF030A16DC67EAF3F6C9C9C0564D4B88758B173A9 ___vertexTopRight_16;
	// UnityEngine.TextCore.Text.TextVertex UnityEngine.TextCore.Text.TextElementInfo::vertexBottomRight
	TextVertex_tF030A16DC67EAF3F6C9C9C0564D4B88758B173A9 ___vertexBottomRight_17;
	// UnityEngine.Vector3 UnityEngine.TextCore.Text.TextElementInfo::topLeft
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___topLeft_18;
	// UnityEngine.Vector3 UnityEngine.TextCore.Text.TextElementInfo::bottomLeft
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___bottomLeft_19;
	// UnityEngine.Vector3 UnityEngine.TextCore.Text.TextElementInfo::topRight
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___topRight_20;
	// UnityEngine.Vector3 UnityEngine.TextCore.Text.TextElementInfo::bottomRight
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___bottomRight_21;
	// System.Single UnityEngine.TextCore.Text.TextElementInfo::origin
	float ___origin_22;
	// System.Single UnityEngine.TextCore.Text.TextElementInfo::ascender
	float ___ascender_23;
	// System.Single UnityEngine.TextCore.Text.TextElementInfo::baseLine
	float ___baseLine_24;
	// System.Single UnityEngine.TextCore.Text.TextElementInfo::descender
	float ___descender_25;
	// System.Single UnityEngine.TextCore.Text.TextElementInfo::xAdvance
	float ___xAdvance_26;
	// System.Single UnityEngine.TextCore.Text.TextElementInfo::aspectRatio
	float ___aspectRatio_27;
	// System.Single UnityEngine.TextCore.Text.TextElementInfo::scale
	float ___scale_28;
	// UnityEngine.Color32 UnityEngine.TextCore.Text.TextElementInfo::color
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color_29;
	// UnityEngine.Color32 UnityEngine.TextCore.Text.TextElementInfo::underlineColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___underlineColor_30;
	// UnityEngine.Color32 UnityEngine.TextCore.Text.TextElementInfo::strikethroughColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___strikethroughColor_31;
	// UnityEngine.Color32 UnityEngine.TextCore.Text.TextElementInfo::highlightColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___highlightColor_32;
	// UnityEngine.TextCore.Text.FontStyles UnityEngine.TextCore.Text.TextElementInfo::style
	int32_t ___style_33;
	// System.Boolean UnityEngine.TextCore.Text.TextElementInfo::isVisible
	bool ___isVisible_34;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.Text.TextElementInfo
struct TextElementInfo_tDD7A12E319505510E0B350E342BD55F32AB5F976_marshaled_pinvoke
{
	uint8_t ___character_0;
	int32_t ___index_1;
	uint8_t ___elementType_2;
	TextElement_tCEF567A8810788262275B39DC39CBA6EBE7472DA* ___textElement_3;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___fontAsset_4;
	SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313* ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TextVertex_tF030A16DC67EAF3F6C9C9C0564D4B88758B173A9 ___vertexTopLeft_14;
	TextVertex_tF030A16DC67EAF3F6C9C9C0564D4B88758B173A9 ___vertexBottomLeft_15;
	TextVertex_tF030A16DC67EAF3F6C9C9C0564D4B88758B173A9 ___vertexTopRight_16;
	TextVertex_tF030A16DC67EAF3F6C9C9C0564D4B88758B173A9 ___vertexBottomRight_17;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___topLeft_18;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___bottomLeft_19;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___topRight_20;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color_29;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___underlineColor_30;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___strikethroughColor_31;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
// Native definition for COM marshalling of UnityEngine.TextCore.Text.TextElementInfo
struct TextElementInfo_tDD7A12E319505510E0B350E342BD55F32AB5F976_marshaled_com
{
	uint8_t ___character_0;
	int32_t ___index_1;
	uint8_t ___elementType_2;
	TextElement_tCEF567A8810788262275B39DC39CBA6EBE7472DA* ___textElement_3;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___fontAsset_4;
	SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313* ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TextVertex_tF030A16DC67EAF3F6C9C9C0564D4B88758B173A9 ___vertexTopLeft_14;
	TextVertex_tF030A16DC67EAF3F6C9C9C0564D4B88758B173A9 ___vertexBottomLeft_15;
	TextVertex_tF030A16DC67EAF3F6C9C9C0564D4B88758B173A9 ___vertexTopRight_16;
	TextVertex_tF030A16DC67EAF3F6C9C9C0564D4B88758B173A9 ___vertexBottomRight_17;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___topLeft_18;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___bottomLeft_19;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___topRight_20;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color_29;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___underlineColor_30;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___strikethroughColor_31;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};

// UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC  : public RuntimeObject
{
	// System.IntPtr UnityEngine.TextGenerator::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.String UnityEngine.TextGenerator::m_LastString
	String_t* ___m_LastString_1;
	// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::m_LastSettings
	TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3 ___m_LastSettings_2;
	// System.Boolean UnityEngine.TextGenerator::m_HasGenerated
	bool ___m_HasGenerated_3;
	// UnityEngine.TextGenerationError UnityEngine.TextGenerator::m_LastValid
	int32_t ___m_LastValid_4;
	// System.Collections.Generic.List`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::m_Verts
	List_1_t09F8990ACE8783E311B473B0090859BA9C00FC2A* ___m_Verts_5;
	// System.Collections.Generic.List`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::m_Characters
	List_1_t67A1600A303BB89506DFD21B59687088B7E0675B* ___m_Characters_6;
	// System.Collections.Generic.List`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::m_Lines
	List_1_t9209B29AC606399207E97BDCD817DEA5B6C63CA5* ___m_Lines_7;
	// System.Boolean UnityEngine.TextGenerator::m_CachedVerts
	bool ___m_CachedVerts_8;
	// System.Boolean UnityEngine.TextGenerator::m_CachedCharacters
	bool ___m_CachedCharacters_9;
	// System.Boolean UnityEngine.TextGenerator::m_CachedLines
	bool ___m_CachedLines_10;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	char* ___m_LastString_1;
	TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3_marshaled_pinvoke ___m_LastSettings_2;
	int32_t ___m_HasGenerated_3;
	int32_t ___m_LastValid_4;
	List_1_t09F8990ACE8783E311B473B0090859BA9C00FC2A* ___m_Verts_5;
	List_1_t67A1600A303BB89506DFD21B59687088B7E0675B* ___m_Characters_6;
	List_1_t9209B29AC606399207E97BDCD817DEA5B6C63CA5* ___m_Lines_7;
	int32_t ___m_CachedVerts_8;
	int32_t ___m_CachedCharacters_9;
	int32_t ___m_CachedLines_10;
};
// Native definition for COM marshalling of UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppChar* ___m_LastString_1;
	TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3_marshaled_com ___m_LastSettings_2;
	int32_t ___m_HasGenerated_3;
	int32_t ___m_LastValid_4;
	List_1_t09F8990ACE8783E311B473B0090859BA9C00FC2A* ___m_Verts_5;
	List_1_t67A1600A303BB89506DFD21B59687088B7E0675B* ___m_Characters_6;
	List_1_t9209B29AC606399207E97BDCD817DEA5B6C63CA5* ___m_Lines_7;
	int32_t ___m_CachedVerts_8;
	int32_t ___m_CachedCharacters_9;
	int32_t ___m_CachedLines_10;
};

// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.UnityWebRequest::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::m_DownloadHandler
	DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB* ___m_DownloadHandler_1;
	// UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::m_UploadHandler
	UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6* ___m_UploadHandler_2;
	// UnityEngine.Networking.CertificateHandler UnityEngine.Networking.UnityWebRequest::m_CertificateHandler
	CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804* ___m_CertificateHandler_3;
	// System.Uri UnityEngine.Networking.UnityWebRequest::m_Uri
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___m_Uri_4;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeCertificateHandlerOnDispose>k__BackingField
	bool ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeDownloadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeUploadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke ___m_DownloadHandler_1;
	UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_pinvoke ___m_UploadHandler_2;
	CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_pinvoke ___m_CertificateHandler_3;
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_com
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com* ___m_DownloadHandler_1;
	UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com* ___m_UploadHandler_2;
	CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_com* ___m_CertificateHandler_3;
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};

// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C  : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C
{
	// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAsyncOperation::<webRequest>k__BackingField
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* ___U3CwebRequestU3Ek__BackingField_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C_marshaled_pinvoke : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_pinvoke
{
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_pinvoke* ___U3CwebRequestU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C_marshaled_com : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_com
{
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_com* ___U3CwebRequestU3Ek__BackingField_2;
};

// UnityEngine.UIElements.UIR.Utility
struct Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD  : public RuntimeObject
{
};

// UnityEngine.ParticleSystem/EmitParams
struct EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0 
{
	// UnityEngine.ParticleSystem/Particle UnityEngine.ParticleSystem/EmitParams::m_Particle
	Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D ___m_Particle_0;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_PositionSet
	bool ___m_PositionSet_1;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_VelocitySet
	bool ___m_VelocitySet_2;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_AxisOfRotationSet
	bool ___m_AxisOfRotationSet_3;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_RotationSet
	bool ___m_RotationSet_4;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_AngularVelocitySet
	bool ___m_AngularVelocitySet_5;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartSizeSet
	bool ___m_StartSizeSet_6;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartColorSet
	bool ___m_StartColorSet_7;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_RandomSeedSet
	bool ___m_RandomSeedSet_8;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartLifetimeSet
	bool ___m_StartLifetimeSet_9;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_MeshIndexSet
	bool ___m_MeshIndexSet_10;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_ApplyShapeToPosition
	bool ___m_ApplyShapeToPosition_11;
};
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/EmitParams
struct EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0_marshaled_pinvoke
{
	Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D ___m_Particle_0;
	int32_t ___m_PositionSet_1;
	int32_t ___m_VelocitySet_2;
	int32_t ___m_AxisOfRotationSet_3;
	int32_t ___m_RotationSet_4;
	int32_t ___m_AngularVelocitySet_5;
	int32_t ___m_StartSizeSet_6;
	int32_t ___m_StartColorSet_7;
	int32_t ___m_RandomSeedSet_8;
	int32_t ___m_StartLifetimeSet_9;
	int32_t ___m_MeshIndexSet_10;
	int32_t ___m_ApplyShapeToPosition_11;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/EmitParams
struct EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0_marshaled_com
{
	Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D ___m_Particle_0;
	int32_t ___m_PositionSet_1;
	int32_t ___m_VelocitySet_2;
	int32_t ___m_AxisOfRotationSet_3;
	int32_t ___m_RotationSet_4;
	int32_t ___m_AngularVelocitySet_5;
	int32_t ___m_StartSizeSet_6;
	int32_t ___m_StartColorSet_7;
	int32_t ___m_RandomSeedSet_8;
	int32_t ___m_StartLifetimeSet_9;
	int32_t ___m_MeshIndexSet_10;
	int32_t ___m_ApplyShapeToPosition_11;
};

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass15_0
struct U3CU3Ec__DisplayClass15_0_t5ECC8CA166122F19F1A179FEF026908B3EAEBDBC  : public RuntimeObject
{
	// DG.Tweening.Color2 DG.Tweening.ShortcutExtensions/<>c__DisplayClass15_0::startValue
	Color2_t35316BB1AA7A5F82C686D69DA763B9E99A72EEAA ___startValue_0;
	// UnityEngine.LineRenderer DG.Tweening.ShortcutExtensions/<>c__DisplayClass15_0::target
	LineRenderer_tEFEF960672DB69CB14B6D181FAE6292F0CF8B63D* ___target_1;
};

// UnityEngine.XR.XRDisplaySubsystem/XRRenderPass
struct XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8 
{
	// System.IntPtr UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::displaySubsystemInstance
	intptr_t ___displaySubsystemInstance_0;
	// System.Int32 UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::renderPassIndex
	int32_t ___renderPassIndex_1;
	// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::renderTarget
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___renderTarget_2;
	// UnityEngine.RenderTextureDescriptor UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::renderTargetDesc
	RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 ___renderTargetDesc_3;
	// System.Boolean UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::hasMotionVectorPass
	bool ___hasMotionVectorPass_4;
	// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::motionVectorRenderTarget
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___motionVectorRenderTarget_5;
	// UnityEngine.RenderTextureDescriptor UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::motionVectorRenderTargetDesc
	RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 ___motionVectorRenderTargetDesc_6;
	// System.Boolean UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::shouldFillOutDepth
	bool ___shouldFillOutDepth_7;
	// System.Int32 UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::cullingPassIndex
	int32_t ___cullingPassIndex_8;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.XRDisplaySubsystem/XRRenderPass
struct XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8_marshaled_pinvoke
{
	intptr_t ___displaySubsystemInstance_0;
	int32_t ___renderPassIndex_1;
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___renderTarget_2;
	RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 ___renderTargetDesc_3;
	int32_t ___hasMotionVectorPass_4;
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___motionVectorRenderTarget_5;
	RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 ___motionVectorRenderTargetDesc_6;
	int32_t ___shouldFillOutDepth_7;
	int32_t ___cullingPassIndex_8;
};
// Native definition for COM marshalling of UnityEngine.XR.XRDisplaySubsystem/XRRenderPass
struct XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8_marshaled_com
{
	intptr_t ___displaySubsystemInstance_0;
	int32_t ___renderPassIndex_1;
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___renderTarget_2;
	RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 ___renderTargetDesc_3;
	int32_t ___hasMotionVectorPass_4;
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___motionVectorRenderTarget_5;
	RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 ___motionVectorRenderTargetDesc_6;
	int32_t ___shouldFillOutDepth_7;
	int32_t ___cullingPassIndex_8;
};

// UnityEngine.AnimationClip
struct AnimationClip_t00BD2F131D308A4AD2C6B0BF66644FC25FECE712  : public Motion_tBCD49FBF5608AD21FC03B63C8182FABCEF2707AC
{
};

// UnityEngine.Timeline.AnimationOutputWeightProcessor
struct AnimationOutputWeightProcessor_tA79B0F7A640A2BE35E2B5DD009E84CFB293E1EFE  : public RuntimeObject
{
	// UnityEngine.Animations.AnimationPlayableOutput UnityEngine.Timeline.AnimationOutputWeightProcessor::m_Output
	AnimationPlayableOutput_t753AC95DC826789BC537D18449E93114777DDF4E ___m_Output_0;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo> UnityEngine.Timeline.AnimationOutputWeightProcessor::m_Mixers
	List_1_t34937A123BB6F1345F58E1BE04611AC43FA110C2* ___m_Mixers_1;
};

// UnityEngine.AnimatorOverrideController
struct AnimatorOverrideController_tF78BD58B30BB0D767E7A96F8428EA66F2DFD5493  : public RuntimeAnimatorController_t6F7C753402B42EC23C163099CF935C5E0D7A7254
{
	// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback UnityEngine.AnimatorOverrideController::OnOverrideControllerDirty
	OnOverrideControllerDirtyCallback_tDC67F7661A27502AD804BDE0B696955AFD4A44D5* ___OnOverrideControllerDirty_4;
};

// UnityEngine.Yoga.BaselineFunction
struct BaselineFunction_t13AFADEF52F63320B2159C237635948AEB801679  : public MulticastDelegate_t
{
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// CartoonFX.CFXR_ParticleTextFontAsset
struct CFXR_ParticleTextFontAsset_t19D1F73819A2F60E0FA1B17ECA99FB3FAAF756ED  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// System.String CartoonFX.CFXR_ParticleTextFontAsset::CharSequence
	String_t* ___CharSequence_4;
	// CartoonFX.CFXR_ParticleTextFontAsset/LetterCase CartoonFX.CFXR_ParticleTextFontAsset::letterCase
	int32_t ___letterCase_5;
	// UnityEngine.Sprite[] CartoonFX.CFXR_ParticleTextFontAsset::CharSprites
	SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* ___CharSprites_6;
	// CartoonFX.CFXR_ParticleTextFontAsset/Kerning[] CartoonFX.CFXR_ParticleTextFontAsset::CharKerningOffsets
	KerningU5BU5D_t71F41AB873624E12E9C1447A4C794D52A931B19A* ___CharKerningOffsets_7;
};

// UnityEngine.CanvasRenderer
struct CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
	// System.Boolean UnityEngine.CanvasRenderer::<isMask>k__BackingField
	bool ___U3CisMaskU3Ek__BackingField_4;
};

// UnityEngine.Collider
struct Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// DG.Tweening.Core.DOTweenSettings
struct DOTweenSettings_tC357F09CCC21098056247CCF9445D9E519ADD4EB  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// System.Boolean DG.Tweening.Core.DOTweenSettings::useSafeMode
	bool ___useSafeMode_6;
	// DG.Tweening.Core.DOTweenSettings/SafeModeOptions DG.Tweening.Core.DOTweenSettings::safeModeOptions
	SafeModeOptions_tA3C410AFB34C85C70704176B558984B1BA8AD880* ___safeModeOptions_7;
	// System.Single DG.Tweening.Core.DOTweenSettings::timeScale
	float ___timeScale_8;
	// System.Single DG.Tweening.Core.DOTweenSettings::unscaledTimeScale
	float ___unscaledTimeScale_9;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::useSmoothDeltaTime
	bool ___useSmoothDeltaTime_10;
	// System.Single DG.Tweening.Core.DOTweenSettings::maxSmoothUnscaledTime
	float ___maxSmoothUnscaledTime_11;
	// DG.Tweening.Core.Enums.RewindCallbackMode DG.Tweening.Core.DOTweenSettings::rewindCallbackMode
	int32_t ___rewindCallbackMode_12;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::showUnityEditorReport
	bool ___showUnityEditorReport_13;
	// DG.Tweening.LogBehaviour DG.Tweening.Core.DOTweenSettings::logBehaviour
	int32_t ___logBehaviour_14;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::drawGizmos
	bool ___drawGizmos_15;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::defaultRecyclable
	bool ___defaultRecyclable_16;
	// DG.Tweening.AutoPlay DG.Tweening.Core.DOTweenSettings::defaultAutoPlay
	int32_t ___defaultAutoPlay_17;
	// DG.Tweening.UpdateType DG.Tweening.Core.DOTweenSettings::defaultUpdateType
	int32_t ___defaultUpdateType_18;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::defaultTimeScaleIndependent
	bool ___defaultTimeScaleIndependent_19;
	// DG.Tweening.Ease DG.Tweening.Core.DOTweenSettings::defaultEaseType
	int32_t ___defaultEaseType_20;
	// System.Single DG.Tweening.Core.DOTweenSettings::defaultEaseOvershootOrAmplitude
	float ___defaultEaseOvershootOrAmplitude_21;
	// System.Single DG.Tweening.Core.DOTweenSettings::defaultEasePeriod
	float ___defaultEasePeriod_22;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::defaultAutoKill
	bool ___defaultAutoKill_23;
	// DG.Tweening.LoopType DG.Tweening.Core.DOTweenSettings::defaultLoopType
	int32_t ___defaultLoopType_24;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::debugMode
	bool ___debugMode_25;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::debugStoreTargetId
	bool ___debugStoreTargetId_26;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::showPreviewPanel
	bool ___showPreviewPanel_27;
	// DG.Tweening.Core.DOTweenSettings/SettingsLocation DG.Tweening.Core.DOTweenSettings::storeSettingsLocation
	int32_t ___storeSettingsLocation_28;
	// DG.Tweening.Core.DOTweenSettings/ModulesSetup DG.Tweening.Core.DOTweenSettings::modules
	ModulesSetup_t888D98957DAB1E84C63C5F866C838A0E714F7236* ___modules_29;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::createASMDEF
	bool ___createASMDEF_30;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::showPlayingTweens
	bool ___showPlayingTweens_31;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::showPausedTweens
	bool ___showPausedTweens_32;
};

// UnityEngine.GUIScrollGroup
struct GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5  : public GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D
{
	// System.Single UnityEngine.GUIScrollGroup::calcMinWidth
	float ___calcMinWidth_32;
	// System.Single UnityEngine.GUIScrollGroup::calcMaxWidth
	float ___calcMaxWidth_33;
	// System.Single UnityEngine.GUIScrollGroup::calcMinHeight
	float ___calcMinHeight_34;
	// System.Single UnityEngine.GUIScrollGroup::calcMaxHeight
	float ___calcMaxHeight_35;
	// System.Single UnityEngine.GUIScrollGroup::clientWidth
	float ___clientWidth_36;
	// System.Single UnityEngine.GUIScrollGroup::clientHeight
	float ___clientHeight_37;
	// System.Boolean UnityEngine.GUIScrollGroup::allowHorizontalScroll
	bool ___allowHorizontalScroll_38;
	// System.Boolean UnityEngine.GUIScrollGroup::allowVerticalScroll
	bool ___allowVerticalScroll_39;
	// System.Boolean UnityEngine.GUIScrollGroup::needsHorizontalScrollbar
	bool ___needsHorizontalScrollbar_40;
	// System.Boolean UnityEngine.GUIScrollGroup::needsVerticalScrollbar
	bool ___needsVerticalScrollbar_41;
	// UnityEngine.GUIStyle UnityEngine.GUIScrollGroup::horizontalScrollbar
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___horizontalScrollbar_42;
	// UnityEngine.GUIStyle UnityEngine.GUIScrollGroup::verticalScrollbar
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___verticalScrollbar_43;
};

// UnityEngine.GUISkin
struct GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// UnityEngine.Font UnityEngine.GUISkin::m_Font
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___m_Font_4;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_box
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_box_5;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_button
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_button_6;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_toggle
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_toggle_7;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_label
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_label_8;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textField
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_textField_9;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textArea
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_textArea_10;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_window
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_window_11;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSlider
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalSlider_12;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSliderThumb
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalSliderThumb_13;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSliderThumbExtent
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalSliderThumbExtent_14;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSlider
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalSlider_15;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSliderThumb
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalSliderThumb_16;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSliderThumbExtent
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalSliderThumbExtent_17;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_SliderMixed
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_SliderMixed_18;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbar
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalScrollbar_19;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarThumb
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalScrollbarThumb_20;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarLeftButton
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalScrollbarLeftButton_21;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarRightButton
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_horizontalScrollbarRightButton_22;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbar
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalScrollbar_23;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarThumb
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalScrollbarThumb_24;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarUpButton
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalScrollbarUpButton_25;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarDownButton
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_verticalScrollbarDownButton_26;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_ScrollView
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___m_ScrollView_27;
	// UnityEngine.GUIStyle[] UnityEngine.GUISkin::m_CustomStyles
	GUIStyleU5BU5D_t1BA4BCF4D4D32DF07E9B84F1750D964DF33B0FEC* ___m_CustomStyles_28;
	// UnityEngine.GUISettings UnityEngine.GUISkin::m_Settings
	GUISettings_tF2CA7E8B9F62F1FC013BFF053B5FA2709EBA3847* ___m_Settings_29;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle> UnityEngine.GUISkin::m_Styles
	Dictionary_2_tEFC8016EC28460E6CE058A5F413FAB656883AA5F* ___m_Styles_31;
};

// UnityEngine.Timeline.InfiniteRuntimeClip
struct InfiniteRuntimeClip_t4421729DA047CB7D239F88EBD2B8103D06686D79  : public RuntimeElement_tB6282BAA538AFD75E138BED88A14DFA2665737B2
{
	// UnityEngine.Playables.Playable UnityEngine.Timeline.InfiniteRuntimeClip::m_Playable
	Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F ___m_Playable_1;
};

// System.InvalidOperationException
struct InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// UnityEngine.Yoga.Logger
struct Logger_t092B1218ED93DD47180692D5761559B2054234A0  : public MulticastDelegate_t
{
};

// UnityEngine.Yoga.MeasureFunction
struct MeasureFunction_t60EBED1328F5328D4FA7E26335967E59E73B4D09  : public MulticastDelegate_t
{
};

// UnityEngineInternal.Input.NativeUpdateCallback
struct NativeUpdateCallback_tC5CA5A9117B79251968A4DA3758552EFE1D37495  : public MulticastDelegate_t
{
};

// UnityEngine.ParticleSystem
struct ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t6964211C3DAE503FEEDD04089ED6B962945D271E  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
};

// UnityEngine.Renderer
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Rigidbody
struct Rigidbody_t268697F5A994213ED97393309870968BC1C7393C  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Timeline.RuntimeClip
struct RuntimeClip_t60AA28A7FF08E7EB8A2F524A979CEAC3F32A00AC  : public RuntimeClipBase_t7F5FBDC61E8219FDDF0F9DEB7B6E3864BC57FDD7
{
	// UnityEngine.Timeline.TimelineClip UnityEngine.Timeline.RuntimeClip::m_Clip
	TimelineClip_t003008F08E56A75F3A47FD9ADE7C066988A3371D* ___m_Clip_1;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.RuntimeClip::m_Playable
	Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F ___m_Playable_2;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.RuntimeClip::m_ParentMixer
	Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F ___m_ParentMixer_3;
};

// UnityEngine.StateMachineBehaviour
struct StateMachineBehaviour_t59C5685227B06222F5AF7027E2DA530AB99AFDF7  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
};

// UnityEngine.TextCore.Text.TextAsset
struct TextAsset_tB28F1843A877CCA74B89DC4F63EA532618B049B8  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// System.String UnityEngine.TextCore.Text.TextAsset::m_Version
	String_t* ___m_Version_4;
	// System.Int32 UnityEngine.TextCore.Text.TextAsset::m_InstanceID
	int32_t ___m_InstanceID_5;
	// System.Int32 UnityEngine.TextCore.Text.TextAsset::m_HashCode
	int32_t ___m_HashCode_6;
	// UnityEngine.Material UnityEngine.TextCore.Text.TextAsset::m_Material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_Material_7;
	// System.Int32 UnityEngine.TextCore.Text.TextAsset::m_MaterialHashCode
	int32_t ___m_MaterialHashCode_8;
};

// UnityEngine.TextCore.Text.TextColorGradient
struct TextColorGradient_t22D94E441E8E8CD772B966C167E5C0AEB0919D70  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// UnityEngine.TextCore.Text.ColorGradientMode UnityEngine.TextCore.Text.TextColorGradient::colorMode
	int32_t ___colorMode_4;
	// UnityEngine.Color UnityEngine.TextCore.Text.TextColorGradient::topLeft
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___topLeft_5;
	// UnityEngine.Color UnityEngine.TextCore.Text.TextColorGradient::topRight
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___topRight_6;
	// UnityEngine.Color UnityEngine.TextCore.Text.TextColorGradient::bottomLeft
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___bottomLeft_7;
	// UnityEngine.Color UnityEngine.TextCore.Text.TextColorGradient::bottomRight
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___bottomRight_8;
};

// UnityEngine.TextMesh
struct TextMesh_t7E1981C7B03E50D5CA5A3AD5B0D9BB0AB6EE91F8  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.TextCore.Text.TextSettings
struct TextSettings_tB7F55685AFFD4A96F714427BCACFD6958E357D64  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// System.String UnityEngine.TextCore.Text.TextSettings::m_Version
	String_t* ___m_Version_4;
	// UnityEngine.TextCore.Text.FontAsset UnityEngine.TextCore.Text.TextSettings::m_DefaultFontAsset
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___m_DefaultFontAsset_5;
	// System.String UnityEngine.TextCore.Text.TextSettings::m_DefaultFontAssetPath
	String_t* ___m_DefaultFontAssetPath_6;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.FontAsset> UnityEngine.TextCore.Text.TextSettings::m_FallbackFontAssets
	List_1_t55B85B981AC5FD6A5358491F90FE354F78BB97DE* ___m_FallbackFontAssets_7;
	// System.Boolean UnityEngine.TextCore.Text.TextSettings::m_MatchMaterialPreset
	bool ___m_MatchMaterialPreset_8;
	// System.Int32 UnityEngine.TextCore.Text.TextSettings::m_MissingCharacterUnicode
	int32_t ___m_MissingCharacterUnicode_9;
	// System.Boolean UnityEngine.TextCore.Text.TextSettings::m_ClearDynamicDataOnBuild
	bool ___m_ClearDynamicDataOnBuild_10;
	// UnityEngine.TextCore.Text.SpriteAsset UnityEngine.TextCore.Text.TextSettings::m_DefaultSpriteAsset
	SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313* ___m_DefaultSpriteAsset_11;
	// System.String UnityEngine.TextCore.Text.TextSettings::m_DefaultSpriteAssetPath
	String_t* ___m_DefaultSpriteAssetPath_12;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.SpriteAsset> UnityEngine.TextCore.Text.TextSettings::m_FallbackSpriteAssets
	List_1_t3EE59C28A34FCD5060EF6B6BAFA85F2C9D01D320* ___m_FallbackSpriteAssets_13;
	// System.UInt32 UnityEngine.TextCore.Text.TextSettings::m_MissingSpriteCharacterUnicode
	uint32_t ___m_MissingSpriteCharacterUnicode_14;
	// UnityEngine.TextCore.Text.TextStyleSheet UnityEngine.TextCore.Text.TextSettings::m_DefaultStyleSheet
	TextStyleSheet_t86A0FA5523897465F371A2ABC17DFA3558C8D15E* ___m_DefaultStyleSheet_15;
	// System.String UnityEngine.TextCore.Text.TextSettings::m_StyleSheetsResourcePath
	String_t* ___m_StyleSheetsResourcePath_16;
	// System.String UnityEngine.TextCore.Text.TextSettings::m_DefaultColorGradientPresetsPath
	String_t* ___m_DefaultColorGradientPresetsPath_17;
	// UnityEngine.TextCore.Text.UnicodeLineBreakingRules UnityEngine.TextCore.Text.TextSettings::m_UnicodeLineBreakingRules
	UnicodeLineBreakingRules_t80BE36F5E16AE48FE7B6DE1C91D36B1142B4EC0E* ___m_UnicodeLineBreakingRules_18;
	// System.Boolean UnityEngine.TextCore.Text.TextSettings::m_DisplayWarnings
	bool ___m_DisplayWarnings_19;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.TextCore.Text.FontAsset> UnityEngine.TextCore.Text.TextSettings::m_FontLookup
	Dictionary_2_tC20B3D6AE4370C892734F670EF4D1FB9CE91F371* ___m_FontLookup_20;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.TextSettings/FontReferenceMap> UnityEngine.TextCore.Text.TextSettings::m_FontReferences
	List_1_tA1547550E5FBA50050B20DA74245C38434654EE8* ___m_FontReferences_21;
};

// UnityEngine.TextCore.Text.TextStyleSheet
struct TextStyleSheet_t86A0FA5523897465F371A2ABC17DFA3558C8D15E  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.TextStyle> UnityEngine.TextCore.Text.TextStyleSheet::m_StyleList
	List_1_t2AFC9C88B8DEFBC5C4C13A94CD97E65EF9AA29C1* ___m_StyleList_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.TextCore.Text.TextStyle> UnityEngine.TextCore.Text.TextStyleSheet::m_StyleLookupDictionary
	Dictionary_2_tD24879A9B92D2B2D486110818CD29C513B42AF62* ___m_StyleLookupDictionary_5;
};

// UnityEngine.Timeline.TimeNotificationBehaviour
struct TimeNotificationBehaviour_tE17AD7EF43D3F48E5BB2C35250F82AB5F9B47535  : public PlayableBehaviour_tCCFE023F2CAB1769A3EAB176BD5B0416C54C22E2
{
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TimeNotificationBehaviour/NotificationEntry> UnityEngine.Timeline.TimeNotificationBehaviour::m_Notifications
	List_1_t2C68AC15B44F139479D9C8C55F4D093DE5F74CD2* ___m_Notifications_0;
	// System.Double UnityEngine.Timeline.TimeNotificationBehaviour::m_PreviousTime
	double ___m_PreviousTime_1;
	// System.Boolean UnityEngine.Timeline.TimeNotificationBehaviour::m_NeedSortNotifications
	bool ___m_NeedSortNotifications_2;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.TimeNotificationBehaviour::m_TimeSource
	Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F ___m_TimeSource_3;
};

// UnityEngine.TextCore.Text.WordWrapState
struct WordWrapState_tD71131CF008362DB9562FB9794AE9D9225D8F123 
{
	// System.Int32 UnityEngine.TextCore.Text.WordWrapState::previousWordBreak
	int32_t ___previousWordBreak_0;
	// System.Int32 UnityEngine.TextCore.Text.WordWrapState::totalCharacterCount
	int32_t ___totalCharacterCount_1;
	// System.Int32 UnityEngine.TextCore.Text.WordWrapState::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 UnityEngine.TextCore.Text.WordWrapState::visibleSpriteCount
	int32_t ___visibleSpriteCount_3;
	// System.Int32 UnityEngine.TextCore.Text.WordWrapState::visibleLinkCount
	int32_t ___visibleLinkCount_4;
	// System.Int32 UnityEngine.TextCore.Text.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 UnityEngine.TextCore.Text.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 UnityEngine.TextCore.Text.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 UnityEngine.TextCore.Text.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 UnityEngine.TextCore.Text.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single UnityEngine.TextCore.Text.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single UnityEngine.TextCore.Text.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single UnityEngine.TextCore.Text.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single UnityEngine.TextCore.Text.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single UnityEngine.TextCore.Text.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single UnityEngine.TextCore.Text.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single UnityEngine.TextCore.Text.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single UnityEngine.TextCore.Text.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single UnityEngine.TextCore.Text.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single UnityEngine.TextCore.Text.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 UnityEngine.TextCore.Text.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// UnityEngine.TextCore.Text.FontStyles UnityEngine.TextCore.Text.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single UnityEngine.TextCore.Text.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single UnityEngine.TextCore.Text.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single UnityEngine.TextCore.Text.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single UnityEngine.TextCore.Text.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single UnityEngine.TextCore.Text.WordWrapState::lineOffset
	float ___lineOffset_26;
	// UnityEngine.TextCore.Text.TextInfo UnityEngine.TextCore.Text.WordWrapState::textInfo
	TextInfo_t27E58E62A7552C66D38C175AF9D22622365F5D09* ___textInfo_27;
	// UnityEngine.TextCore.Text.LineInfo UnityEngine.TextCore.Text.WordWrapState::lineInfo
	LineInfo_t2BBD461B330C46ACA45596A8E72FEA4172F88CF5 ___lineInfo_28;
	// UnityEngine.Color32 UnityEngine.TextCore.Text.WordWrapState::vertexColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___vertexColor_29;
	// UnityEngine.Color32 UnityEngine.TextCore.Text.WordWrapState::underlineColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___underlineColor_30;
	// UnityEngine.Color32 UnityEngine.TextCore.Text.WordWrapState::strikethroughColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___strikethroughColor_31;
	// UnityEngine.Color32 UnityEngine.TextCore.Text.WordWrapState::highlightColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___highlightColor_32;
	// UnityEngine.TextCore.Text.FontStyleStack UnityEngine.TextCore.Text.WordWrapState::basicStyleStack
	FontStyleStack_t63C77495F068E6DF762D6AF063A817E3709659A7 ___basicStyleStack_33;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.Color32> UnityEngine.TextCore.Text.WordWrapState::colorStack
	TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 ___colorStack_34;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.Color32> UnityEngine.TextCore.Text.WordWrapState::underlineColorStack
	TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 ___underlineColorStack_35;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.Color32> UnityEngine.TextCore.Text.WordWrapState::strikethroughColorStack
	TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 ___strikethroughColorStack_36;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.Color32> UnityEngine.TextCore.Text.WordWrapState::highlightColorStack
	TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 ___highlightColorStack_37;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.TextCore.Text.TextColorGradient> UnityEngine.TextCore.Text.WordWrapState::colorGradientStack
	TextProcessingStack_1_t0F39F088E8F8F6E18C3C463B2998ADC5B7A0513E ___colorGradientStack_38;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<System.Single> UnityEngine.TextCore.Text.WordWrapState::sizeStack
	TextProcessingStack_1_t9FC06E35259ADD291ED640FE7554D8C03EA5F555 ___sizeStack_39;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<System.Single> UnityEngine.TextCore.Text.WordWrapState::indentStack
	TextProcessingStack_1_t9FC06E35259ADD291ED640FE7554D8C03EA5F555 ___indentStack_40;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.TextCore.Text.TextFontWeight> UnityEngine.TextCore.Text.WordWrapState::fontWeightStack
	TextProcessingStack_1_t698B87CDD968C2046F57134BB3AB807EBFFD7790 ___fontWeightStack_41;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<System.Int32> UnityEngine.TextCore.Text.WordWrapState::styleStack
	TextProcessingStack_1_t27834AAB14D26DC6519558C4C2574BA9C190D8E8 ___styleStack_42;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<System.Single> UnityEngine.TextCore.Text.WordWrapState::baselineStack
	TextProcessingStack_1_t9FC06E35259ADD291ED640FE7554D8C03EA5F555 ___baselineStack_43;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<System.Int32> UnityEngine.TextCore.Text.WordWrapState::actionStack
	TextProcessingStack_1_t27834AAB14D26DC6519558C4C2574BA9C190D8E8 ___actionStack_44;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.TextCore.Text.MaterialReference> UnityEngine.TextCore.Text.WordWrapState::materialReferenceStack
	TextProcessingStack_1_t0C74606C1B6C7817CA95F0DCA46B219CF6FB35CA ___materialReferenceStack_45;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.TextCore.Text.TextAlignment> UnityEngine.TextCore.Text.WordWrapState::lineJustificationStack
	TextProcessingStack_1_tE63296B08A4C34E38D7EF3FFFA3470076B9E3A0F ___lineJustificationStack_46;
	// System.Int32 UnityEngine.TextCore.Text.WordWrapState::spriteAnimationId
	int32_t ___spriteAnimationId_47;
	// UnityEngine.TextCore.Text.FontAsset UnityEngine.TextCore.Text.WordWrapState::currentFontAsset
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___currentFontAsset_48;
	// UnityEngine.TextCore.Text.SpriteAsset UnityEngine.TextCore.Text.WordWrapState::currentSpriteAsset
	SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313* ___currentSpriteAsset_49;
	// UnityEngine.Material UnityEngine.TextCore.Text.WordWrapState::currentMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___currentMaterial_50;
	// System.Int32 UnityEngine.TextCore.Text.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// UnityEngine.TextCore.Text.Extents UnityEngine.TextCore.Text.WordWrapState::meshExtents
	Extents_t369FB2B84521A0229C2FA3D4C8592B14E07CEFE6 ___meshExtents_52;
	// System.Boolean UnityEngine.TextCore.Text.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean UnityEngine.TextCore.Text.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.Text.WordWrapState
struct WordWrapState_tD71131CF008362DB9562FB9794AE9D9225D8F123_marshaled_pinvoke
{
	int32_t ___previousWordBreak_0;
	int32_t ___totalCharacterCount_1;
	int32_t ___visibleCharacterCount_2;
	int32_t ___visibleSpriteCount_3;
	int32_t ___visibleLinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TextInfo_t27E58E62A7552C66D38C175AF9D22622365F5D09* ___textInfo_27;
	LineInfo_t2BBD461B330C46ACA45596A8E72FEA4172F88CF5 ___lineInfo_28;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___vertexColor_29;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___underlineColor_30;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___strikethroughColor_31;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___highlightColor_32;
	FontStyleStack_t63C77495F068E6DF762D6AF063A817E3709659A7 ___basicStyleStack_33;
	TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 ___colorStack_34;
	TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 ___underlineColorStack_35;
	TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 ___strikethroughColorStack_36;
	TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 ___highlightColorStack_37;
	TextProcessingStack_1_t0F39F088E8F8F6E18C3C463B2998ADC5B7A0513E ___colorGradientStack_38;
	TextProcessingStack_1_t9FC06E35259ADD291ED640FE7554D8C03EA5F555 ___sizeStack_39;
	TextProcessingStack_1_t9FC06E35259ADD291ED640FE7554D8C03EA5F555 ___indentStack_40;
	TextProcessingStack_1_t698B87CDD968C2046F57134BB3AB807EBFFD7790 ___fontWeightStack_41;
	TextProcessingStack_1_t27834AAB14D26DC6519558C4C2574BA9C190D8E8 ___styleStack_42;
	TextProcessingStack_1_t9FC06E35259ADD291ED640FE7554D8C03EA5F555 ___baselineStack_43;
	TextProcessingStack_1_t27834AAB14D26DC6519558C4C2574BA9C190D8E8 ___actionStack_44;
	TextProcessingStack_1_t0C74606C1B6C7817CA95F0DCA46B219CF6FB35CA ___materialReferenceStack_45;
	TextProcessingStack_1_tE63296B08A4C34E38D7EF3FFFA3470076B9E3A0F ___lineJustificationStack_46;
	int32_t ___spriteAnimationId_47;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___currentFontAsset_48;
	SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313* ___currentSpriteAsset_49;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t369FB2B84521A0229C2FA3D4C8592B14E07CEFE6 ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of UnityEngine.TextCore.Text.WordWrapState
struct WordWrapState_tD71131CF008362DB9562FB9794AE9D9225D8F123_marshaled_com
{
	int32_t ___previousWordBreak_0;
	int32_t ___totalCharacterCount_1;
	int32_t ___visibleCharacterCount_2;
	int32_t ___visibleSpriteCount_3;
	int32_t ___visibleLinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TextInfo_t27E58E62A7552C66D38C175AF9D22622365F5D09* ___textInfo_27;
	LineInfo_t2BBD461B330C46ACA45596A8E72FEA4172F88CF5 ___lineInfo_28;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___vertexColor_29;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___underlineColor_30;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___strikethroughColor_31;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___highlightColor_32;
	FontStyleStack_t63C77495F068E6DF762D6AF063A817E3709659A7 ___basicStyleStack_33;
	TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 ___colorStack_34;
	TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 ___underlineColorStack_35;
	TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 ___strikethroughColorStack_36;
	TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 ___highlightColorStack_37;
	TextProcessingStack_1_t0F39F088E8F8F6E18C3C463B2998ADC5B7A0513E ___colorGradientStack_38;
	TextProcessingStack_1_t9FC06E35259ADD291ED640FE7554D8C03EA5F555 ___sizeStack_39;
	TextProcessingStack_1_t9FC06E35259ADD291ED640FE7554D8C03EA5F555 ___indentStack_40;
	TextProcessingStack_1_t698B87CDD968C2046F57134BB3AB807EBFFD7790 ___fontWeightStack_41;
	TextProcessingStack_1_t27834AAB14D26DC6519558C4C2574BA9C190D8E8 ___styleStack_42;
	TextProcessingStack_1_t9FC06E35259ADD291ED640FE7554D8C03EA5F555 ___baselineStack_43;
	TextProcessingStack_1_t27834AAB14D26DC6519558C4C2574BA9C190D8E8 ___actionStack_44;
	TextProcessingStack_1_t0C74606C1B6C7817CA95F0DCA46B219CF6FB35CA ___materialReferenceStack_45;
	TextProcessingStack_1_tE63296B08A4C34E38D7EF3FFFA3470076B9E3A0F ___lineJustificationStack_46;
	int32_t ___spriteAnimationId_47;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___currentFontAsset_48;
	SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313* ___currentSpriteAsset_49;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t369FB2B84521A0229C2FA3D4C8592B14E07CEFE6 ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};

// UnityEngine.XR.XRDisplaySubsystem
struct XRDisplaySubsystem_t4B00B0BF1894A039ACFA8DDC2C2EB9301118C1F1  : public IntegratedSubsystem_1_t8312865F01EEA1EDE4B24A973E47ADD526616848
{
	// System.Action`1<System.Boolean> UnityEngine.XR.XRDisplaySubsystem::displayFocusChanged
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* ___displayFocusChanged_2;
};

// UnityEngine.XR.XRDisplaySubsystemDescriptor
struct XRDisplaySubsystemDescriptor_t72DD88EE9094488AE723A495F48884BA4EA8311A  : public IntegratedSubsystemDescriptor_1_t7261AA0914165CB589AD41C4F9B463D44E333D7C
{
};

// UnityEngine.XR.XRInputSubsystem
struct XRInputSubsystem_tFECE6683FCAEBF05BAD05E5D612690095D8BAD34  : public IntegratedSubsystem_1_tF93BC76362E85BDD215312162457BE510FC76D3B
{
	// System.Action`1<UnityEngine.XR.XRInputSubsystem> UnityEngine.XR.XRInputSubsystem::trackingOriginUpdated
	Action_1_tC867D66471C553CFFF8707FF2C59FB7AAB03086A* ___trackingOriginUpdated_2;
	// System.Action`1<UnityEngine.XR.XRInputSubsystem> UnityEngine.XR.XRInputSubsystem::boundaryChanged
	Action_1_tC867D66471C553CFFF8707FF2C59FB7AAB03086A* ___boundaryChanged_3;
};

// UnityEngine.XR.XRInputSubsystemDescriptor
struct XRInputSubsystemDescriptor_t42088DD6542C0BDD27C2951B911E4F69DD1F917D  : public IntegratedSubsystemDescriptor_1_t440D086A98F968B9FC279854F552698F319A32F7
{
};

// UnityEngine.XR.XRMeshSubsystem
struct XRMeshSubsystem_tDDC31EC10D4F0517542F9EB296428A0F7EC2C3B2  : public IntegratedSubsystem_1_t3AB3A9BAB10A4D1420E63EEA50D40FEE65F41C03
{
};

// UnityEngine.XR.XRMeshSubsystemDescriptor
struct XRMeshSubsystemDescriptor_tD9814661B8661C69D5A0DBB76C9AF61778B9CEC1  : public IntegratedSubsystemDescriptor_1_tBFE1680F4FA360D4E9AB19FDE2CCBD959E436513
{
};

// UnityEngine.Analytics.AnalyticsSessionInfo/IdentityTokenChanged
struct IdentityTokenChanged_tE8CB0DAB5F6E640A847803F582E6CB6237742395  : public MulticastDelegate_t
{
};

// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct SessionStateChanged_t1180FB66E702B635CAD9316DC661D931277B2A0C  : public MulticastDelegate_t
{
};

// UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo
struct WeightInfo_t9942B0D2C77A00A5C9824732AEAA0AB0A55620B0 
{
	// UnityEngine.Playables.Playable UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::mixer
	Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F ___mixer_0;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::parentMixer
	Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F ___parentMixer_1;
	// System.Int32 UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::port
	int32_t ___port_2;
};

// UnityEngine.Timeline.AnimationPlayableAsset/<get_outputs>d__45
struct U3Cget_outputsU3Ed__45_tA1B097E5833BFEF217CC8402D897A89E32BF2928  : public RuntimeObject
{
	// System.Int32 UnityEngine.Timeline.AnimationPlayableAsset/<get_outputs>d__45::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AnimationPlayableAsset/<get_outputs>d__45::<>2__current
	PlayableBinding_tB68B3BAC47F4F4C559640472174D5BEF93CB6AB4 ___U3CU3E2__current_1;
	// System.Int32 UnityEngine.Timeline.AnimationPlayableAsset/<get_outputs>d__45::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Timeline.AnimationPlayableAsset UnityEngine.Timeline.AnimationPlayableAsset/<get_outputs>d__45::<>4__this
	AnimationPlayableAsset_t381BF46F8FEBECFD107EC61C257C60D9438FDF33* ___U3CU3E4__this_3;
};

// UnityEngine.Timeline.AnimationTrack/<get_outputs>d__49
struct U3Cget_outputsU3Ed__49_t590B0A0FEE820B5A8A1F548908337BBCD7AF6A21  : public RuntimeObject
{
	// System.Int32 UnityEngine.Timeline.AnimationTrack/<get_outputs>d__49::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AnimationTrack/<get_outputs>d__49::<>2__current
	PlayableBinding_tB68B3BAC47F4F4C559640472174D5BEF93CB6AB4 ___U3CU3E2__current_1;
	// System.Int32 UnityEngine.Timeline.AnimationTrack/<get_outputs>d__49::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Timeline.AnimationTrack UnityEngine.Timeline.AnimationTrack/<get_outputs>d__49::<>4__this
	AnimationTrack_tBE37C239976A84F0A2E6BA3B77263B4F44BB1359* ___U3CU3E4__this_3;
};

// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback
struct OnOverrideControllerDirtyCallback_tDC67F7661A27502AD804BDE0B696955AFD4A44D5  : public MulticastDelegate_t
{
};

// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t3396D9613664F0AFF65FB91018FD0F901CC16F1E  : public MulticastDelegate_t
{
};

// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t8D7135A2FB40647CAEC93F5254AD59E18DEB6072  : public MulticastDelegate_t
{
};

// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler
struct SampleFramesHandler_tFE84FF9BBCEFB880D46227188F375BEF680AAA30  : public MulticastDelegate_t
{
};

// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_tE071B0CBA3B3A77D3E41F5FCB65B4017885B3177  : public MulticastDelegate_t
{
};

// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC  : public MulticastDelegate_t
{
};

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10
struct U3CAsyncWaitForCompletionU3Ed__10_tC84049D47EAD23B14384BDEF646D532785ECBF0E 
{
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::<>t__builder
	AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06 ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::cit
	YieldAwaiter_t5F0A81DC85227C01FFC38D53139B5C19D920B52A ___cit_3;
};

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13
struct U3CAsyncWaitForElapsedLoopsU3Ed__13_tC7B431C2393096ACD1A6BA0EAFEA84EE62DAF825 
{
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::<>t__builder
	AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06 ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::elapsedLoops
	int32_t ___elapsedLoops_3;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::ciw
	YieldAwaiter_t5F0A81DC85227C01FFC38D53139B5C19D920B52A ___ciw_4;
};

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12
struct U3CAsyncWaitForKillU3Ed__12_t6EA9E2438625431E39D01AB7EEBB4501D6B5E54E 
{
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::<>t__builder
	AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06 ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::civ
	YieldAwaiter_t5F0A81DC85227C01FFC38D53139B5C19D920B52A ___civ_3;
};

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14
struct U3CAsyncWaitForPositionU3Ed__14_tA6006769EC53BBEBA0665ECA79096B606FDA8A4A 
{
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::<>t__builder
	AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06 ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
	// System.Single DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::position
	float ___position_3;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::cix
	YieldAwaiter_t5F0A81DC85227C01FFC38D53139B5C19D920B52A ___cix_4;
};

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11
struct U3CAsyncWaitForRewindU3Ed__11_tC8D7C20224797881A037D09DA8079ECCC3E518FE 
{
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::<>t__builder
	AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06 ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::ciu
	YieldAwaiter_t5F0A81DC85227C01FFC38D53139B5C19D920B52A ___ciu_3;
};

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15
struct U3CAsyncWaitForStartU3Ed__15_tB4B1CE199FE822B67BCF87301159986D9D50961B 
{
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::<>t__builder
	AsyncTaskMethodBuilder_t7A5128C134547B5918EB1AA24FE47ED4C1DF3F06 ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::t
	Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C* ___t_2;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::ciy
	YieldAwaiter_t5F0A81DC85227C01FFC38D53139B5C19D920B52A ___ciy_3;
};

// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t76D5E172DF8AA57E67763D453AAC40F0961D09B1  : public MulticastDelegate_t
{
};

// UnityEngine.GUI/WindowFunction
struct WindowFunction_t0067B6F174FD5BEC3E869A38C2319BA8EE85D550  : public MulticastDelegate_t
{
};

// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_tA6D456E853D58AD2EF8A599F543C7E5BA8E94B98  : public MulticastDelegate_t
{
};

// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_tB0D5A5BA322FE093894992C29DCF51E7E12579C4  : public MulticastDelegate_t
{
};

// UnityEngine.Timeline.TimelineAsset/<get_outputs>d__27
struct U3Cget_outputsU3Ed__27_tC9898A06DC21CA0BE6B27E1948531195AEBAD3C5  : public RuntimeObject
{
	// System.Int32 UnityEngine.Timeline.TimelineAsset/<get_outputs>d__27::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.TimelineAsset/<get_outputs>d__27::<>2__current
	PlayableBinding_tB68B3BAC47F4F4C559640472174D5BEF93CB6AB4 ___U3CU3E2__current_1;
	// System.Int32 UnityEngine.Timeline.TimelineAsset/<get_outputs>d__27::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Timeline.TimelineAsset UnityEngine.Timeline.TimelineAsset/<get_outputs>d__27::<>4__this
	TimelineAsset_tE400C944B07CA9D1349BAD84545E24075ADB3496* ___U3CU3E4__this_3;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TimelineAsset/<get_outputs>d__27::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_4;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.Playables.PlayableBinding> UnityEngine.Timeline.TimelineAsset/<get_outputs>d__27::<>7__wrap2
	RuntimeObject* ___U3CU3E7__wrap2_5;
};

// UnityEngine.Timeline.TrackAsset/<get_outputs>d__65
struct U3Cget_outputsU3Ed__65_tE61187F3860B492601D3B49624D0E518A1D10F46  : public RuntimeObject
{
	// System.Int32 UnityEngine.Timeline.TrackAsset/<get_outputs>d__65::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.TrackAsset/<get_outputs>d__65::<>2__current
	PlayableBinding_tB68B3BAC47F4F4C559640472174D5BEF93CB6AB4 ___U3CU3E2__current_1;
	// System.Int32 UnityEngine.Timeline.TrackAsset/<get_outputs>d__65::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Timeline.TrackAsset UnityEngine.Timeline.TrackAsset/<get_outputs>d__65::<>4__this
	TrackAsset_t31E19BE900C90F6616C0D337652C8614CD833B96* ___U3CU3E4__this_3;
};

// UnityEngine.Animation
struct Animation_t6593B06C39E3B139808B19F2C719C860F3F61040  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.Timeline.AnimationPlayableAsset
struct AnimationPlayableAsset_t381BF46F8FEBECFD107EC61C257C60D9438FDF33  : public PlayableAsset_t6964211C3DAE503FEEDD04089ED6B962945D271E
{
	// UnityEngine.AnimationClip UnityEngine.Timeline.AnimationPlayableAsset::m_Clip
	AnimationClip_t00BD2F131D308A4AD2C6B0BF66644FC25FECE712* ___m_Clip_4;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationPlayableAsset::m_Position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Position_5;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationPlayableAsset::m_EulerAngles
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_EulerAngles_6;
	// System.Boolean UnityEngine.Timeline.AnimationPlayableAsset::m_UseTrackMatchFields
	bool ___m_UseTrackMatchFields_7;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.AnimationPlayableAsset::m_MatchTargetFields
	int32_t ___m_MatchTargetFields_8;
	// System.Boolean UnityEngine.Timeline.AnimationPlayableAsset::m_RemoveStartOffset
	bool ___m_RemoveStartOffset_9;
	// System.Boolean UnityEngine.Timeline.AnimationPlayableAsset::m_ApplyFootIK
	bool ___m_ApplyFootIK_10;
	// UnityEngine.Timeline.AnimationPlayableAsset/LoopMode UnityEngine.Timeline.AnimationPlayableAsset::m_Loop
	int32_t ___m_Loop_11;
	// UnityEngine.Timeline.AppliedOffsetMode UnityEngine.Timeline.AnimationPlayableAsset::<appliedOffsetMode>k__BackingField
	int32_t ___U3CappliedOffsetModeU3Ek__BackingField_12;
	// System.Int32 UnityEngine.Timeline.AnimationPlayableAsset::m_Version
	int32_t ___m_Version_14;
	// UnityEngine.Quaternion UnityEngine.Timeline.AnimationPlayableAsset::m_Rotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___m_Rotation_15;
};

// UnityEngine.Animator
struct Animator_t8A52E42AE54F76681838FE9E632683EF3952E883  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.AudioBehaviour
struct AudioBehaviour_t2DC0BEF7B020C952F3D2DA5AAAC88501C7EEB941  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.BoxCollider
struct BoxCollider_tFA5D239388334D6DE0B8FFDAD6825C5B03786E23  : public Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76
{
};

// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.CanvasGroup
struct CanvasGroup_t048C1461B14628CFAEBE6E7353093ADB04EBC094  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.CapsuleCollider
struct CapsuleCollider_t3A1671C74F0836ABEF5D01A7470B5B2BE290A808  : public Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76
{
};

// UnityEngine.CharacterController
struct CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A  : public Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76
{
};

// UnityEngine.Collider2D
struct Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.TextCore.Text.FontAsset
struct FontAsset_t61A6446D934E582651044E33D250EA8D306AB958  : public TextAsset_tB28F1843A877CCA74B89DC4F63EA532618B049B8
{
	// System.String UnityEngine.TextCore.Text.FontAsset::m_SourceFontFileGUID
	String_t* ___m_SourceFontFileGUID_9;
	// UnityEngine.Font UnityEngine.TextCore.Text.FontAsset::m_SourceFontFile
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___m_SourceFontFile_10;
	// UnityEngine.TextCore.Text.AtlasPopulationMode UnityEngine.TextCore.Text.FontAsset::m_AtlasPopulationMode
	int32_t ___m_AtlasPopulationMode_11;
	// System.Boolean UnityEngine.TextCore.Text.FontAsset::InternalDynamicOS
	bool ___InternalDynamicOS_12;
	// UnityEngine.TextCore.FaceInfo UnityEngine.TextCore.Text.FontAsset::m_FaceInfo
	FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756 ___m_FaceInfo_13;
	// System.Int32 UnityEngine.TextCore.Text.FontAsset::m_FamilyNameHashCode
	int32_t ___m_FamilyNameHashCode_14;
	// System.Int32 UnityEngine.TextCore.Text.FontAsset::m_StyleNameHashCode
	int32_t ___m_StyleNameHashCode_15;
	// UnityEngine.TextCore.Text.FontWeightPair[] UnityEngine.TextCore.Text.FontAsset::m_FontWeightTable
	FontWeightPairU5BU5D_t76E8DB55C81EEBEFA2E6D1D3E3B3EA1FB4C4954F* ___m_FontWeightTable_16;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Glyph> UnityEngine.TextCore.Text.FontAsset::m_GlyphTable
	List_1_t95DB74B8EE315F8F92B7B96D93C901C8C3F6FE2C* ___m_GlyphTable_17;
	// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Glyph> UnityEngine.TextCore.Text.FontAsset::m_GlyphLookupDictionary
	Dictionary_2_tC61348D10610A6B3D7B65102D82AC3467D59EAA7* ___m_GlyphLookupDictionary_18;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.Character> UnityEngine.TextCore.Text.FontAsset::m_CharacterTable
	List_1_tFED0F30EE65D995591571D3CD2C10F22439CB317* ___m_CharacterTable_19;
	// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Text.Character> UnityEngine.TextCore.Text.FontAsset::m_CharacterLookupDictionary
	Dictionary_2_t93CDF0F4011A5A3024EB73A492F9512E3046EACB* ___m_CharacterLookupDictionary_20;
	// UnityEngine.Texture2D UnityEngine.TextCore.Text.FontAsset::m_AtlasTexture
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___m_AtlasTexture_21;
	// UnityEngine.Texture2D[] UnityEngine.TextCore.Text.FontAsset::m_AtlasTextures
	Texture2DU5BU5D_t05332F1E3F7D4493E304C702201F9BE4F9236191* ___m_AtlasTextures_22;
	// System.Int32 UnityEngine.TextCore.Text.FontAsset::m_AtlasTextureIndex
	int32_t ___m_AtlasTextureIndex_23;
	// System.Boolean UnityEngine.TextCore.Text.FontAsset::m_IsMultiAtlasTexturesEnabled
	bool ___m_IsMultiAtlasTexturesEnabled_24;
	// System.Boolean UnityEngine.TextCore.Text.FontAsset::m_ClearDynamicDataOnBuild
	bool ___m_ClearDynamicDataOnBuild_25;
	// System.Int32 UnityEngine.TextCore.Text.FontAsset::m_AtlasWidth
	int32_t ___m_AtlasWidth_26;
	// System.Int32 UnityEngine.TextCore.Text.FontAsset::m_AtlasHeight
	int32_t ___m_AtlasHeight_27;
	// System.Int32 UnityEngine.TextCore.Text.FontAsset::m_AtlasPadding
	int32_t ___m_AtlasPadding_28;
	// UnityEngine.TextCore.LowLevel.GlyphRenderMode UnityEngine.TextCore.Text.FontAsset::m_AtlasRenderMode
	int32_t ___m_AtlasRenderMode_29;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.GlyphRect> UnityEngine.TextCore.Text.FontAsset::m_UsedGlyphRects
	List_1_t425D3A455811E316D2DF73E46CF9CD90A4341C1B* ___m_UsedGlyphRects_30;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.GlyphRect> UnityEngine.TextCore.Text.FontAsset::m_FreeGlyphRects
	List_1_t425D3A455811E316D2DF73E46CF9CD90A4341C1B* ___m_FreeGlyphRects_31;
	// UnityEngine.TextCore.Text.FontFeatureTable UnityEngine.TextCore.Text.FontAsset::m_FontFeatureTable
	FontFeatureTable_t992E0493CD7E9D7834DF204E0198237F0D25B3B7* ___m_FontFeatureTable_32;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.FontAsset> UnityEngine.TextCore.Text.FontAsset::m_FallbackFontAssetTable
	List_1_t55B85B981AC5FD6A5358491F90FE354F78BB97DE* ___m_FallbackFontAssetTable_33;
	// UnityEngine.TextCore.Text.FontAssetCreationEditorSettings UnityEngine.TextCore.Text.FontAsset::m_fontAssetCreationEditorSettings
	FontAssetCreationEditorSettings_t0FF28D2E78F090105C63C81F9E438A7B09E3EA52 ___m_fontAssetCreationEditorSettings_34;
	// System.Single UnityEngine.TextCore.Text.FontAsset::m_RegularStyleWeight
	float ___m_RegularStyleWeight_35;
	// System.Single UnityEngine.TextCore.Text.FontAsset::m_RegularStyleSpacing
	float ___m_RegularStyleSpacing_36;
	// System.Single UnityEngine.TextCore.Text.FontAsset::m_BoldStyleWeight
	float ___m_BoldStyleWeight_37;
	// System.Single UnityEngine.TextCore.Text.FontAsset::m_BoldStyleSpacing
	float ___m_BoldStyleSpacing_38;
	// System.Byte UnityEngine.TextCore.Text.FontAsset::m_ItalicStyleSlant
	uint8_t ___m_ItalicStyleSlant_39;
	// System.Byte UnityEngine.TextCore.Text.FontAsset::m_TabMultiple
	uint8_t ___m_TabMultiple_40;
	// System.Boolean UnityEngine.TextCore.Text.FontAsset::IsFontAssetLookupTablesDirty
	bool ___IsFontAssetLookupTablesDirty_41;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Glyph> UnityEngine.TextCore.Text.FontAsset::m_GlyphsToRender
	List_1_t95DB74B8EE315F8F92B7B96D93C901C8C3F6FE2C* ___m_GlyphsToRender_55;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Glyph> UnityEngine.TextCore.Text.FontAsset::m_GlyphsRendered
	List_1_t95DB74B8EE315F8F92B7B96D93C901C8C3F6FE2C* ___m_GlyphsRendered_56;
	// System.Collections.Generic.List`1<System.UInt32> UnityEngine.TextCore.Text.FontAsset::m_GlyphIndexList
	List_1_t9B68833848E4C4D7F623C05F6B77F0449396354A* ___m_GlyphIndexList_57;
	// System.Collections.Generic.List`1<System.UInt32> UnityEngine.TextCore.Text.FontAsset::m_GlyphIndexListNewlyAdded
	List_1_t9B68833848E4C4D7F623C05F6B77F0449396354A* ___m_GlyphIndexListNewlyAdded_58;
	// System.Collections.Generic.List`1<System.UInt32> UnityEngine.TextCore.Text.FontAsset::m_GlyphsToAdd
	List_1_t9B68833848E4C4D7F623C05F6B77F0449396354A* ___m_GlyphsToAdd_59;
	// System.Collections.Generic.HashSet`1<System.UInt32> UnityEngine.TextCore.Text.FontAsset::m_GlyphsToAddLookup
	HashSet_1_t5DD20B42149A11AEBF12A75505306E6EFC34943A* ___m_GlyphsToAddLookup_60;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.Character> UnityEngine.TextCore.Text.FontAsset::m_CharactersToAdd
	List_1_tFED0F30EE65D995591571D3CD2C10F22439CB317* ___m_CharactersToAdd_61;
	// System.Collections.Generic.HashSet`1<System.UInt32> UnityEngine.TextCore.Text.FontAsset::m_CharactersToAddLookup
	HashSet_1_t5DD20B42149A11AEBF12A75505306E6EFC34943A* ___m_CharactersToAddLookup_62;
	// System.Collections.Generic.List`1<System.UInt32> UnityEngine.TextCore.Text.FontAsset::s_MissingCharacterList
	List_1_t9B68833848E4C4D7F623C05F6B77F0449396354A* ___s_MissingCharacterList_63;
	// System.Collections.Generic.HashSet`1<System.UInt32> UnityEngine.TextCore.Text.FontAsset::m_MissingUnicodesFromFontFile
	HashSet_1_t5DD20B42149A11AEBF12A75505306E6EFC34943A* ___m_MissingUnicodesFromFontFile_64;
};

// UnityEngine.GridLayout
struct GridLayout_tAD661B1E1E57C16BE21C8C13432EA04FE1F0418B  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.MeshCollider
struct MeshCollider_tB525E4DDE383252364ED0BDD32CF2B53914EE455  : public Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// System.ObjectDisposedException
struct ObjectDisposedException_tC5FB29E8E980E2010A2F6A5B9B791089419F89EB  : public InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB
{
	// System.String System.ObjectDisposedException::_objectName
	String_t* ____objectName_18;
};

// UnityEngine.ParticleSystemRenderer
struct ParticleSystemRenderer_t576C271A363A738A6C576D4C6AEFB3B5B23E46C4  : public Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF
{
};

// UnityEngine.Playables.PlayableDirector
struct PlayableDirector_t895D7BC3CFBFFD823278F438EAC4AA91DBFEC475  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
	// System.Action`1<UnityEngine.Playables.PlayableDirector> UnityEngine.Playables.PlayableDirector::played
	Action_1_tB645F646DB079054A9500B09427CB02A88372D3F* ___played_4;
	// System.Action`1<UnityEngine.Playables.PlayableDirector> UnityEngine.Playables.PlayableDirector::paused
	Action_1_tB645F646DB079054A9500B09427CB02A88372D3F* ___paused_5;
	// System.Action`1<UnityEngine.Playables.PlayableDirector> UnityEngine.Playables.PlayableDirector::stopped
	Action_1_tB645F646DB079054A9500B09427CB02A88372D3F* ___stopped_6;
};

// UnityEngine.SphereCollider
struct SphereCollider_tBA111C542CE97F6873DE742757213D6265C7D275  : public Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76
{
};

// UnityEngine.TextCore.Text.SpriteAsset
struct SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313  : public TextAsset_tB28F1843A877CCA74B89DC4F63EA532618B049B8
{
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> UnityEngine.TextCore.Text.SpriteAsset::m_NameLookup
	Dictionary_2_tABE19B9C5C52F1DE14F0D3287B2696E7D7419180* ___m_NameLookup_9;
	// System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32> UnityEngine.TextCore.Text.SpriteAsset::m_GlyphIndexLookup
	Dictionary_2_t1A4804CA9724B6CE01D6ECABE81CE0848CBA80B4* ___m_GlyphIndexLookup_10;
	// UnityEngine.TextCore.FaceInfo UnityEngine.TextCore.Text.SpriteAsset::m_FaceInfo
	FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756 ___m_FaceInfo_11;
	// UnityEngine.Texture UnityEngine.TextCore.Text.SpriteAsset::m_SpriteAtlasTexture
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___m_SpriteAtlasTexture_12;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.SpriteCharacter> UnityEngine.TextCore.Text.SpriteAsset::m_SpriteCharacterTable
	List_1_t7DA088250C54C07AF1211AE132355AD2D343EE51* ___m_SpriteCharacterTable_13;
	// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Text.SpriteCharacter> UnityEngine.TextCore.Text.SpriteAsset::m_SpriteCharacterLookup
	Dictionary_2_tD4154357CA320908C5A7A35ED81FA2A9856C28D9* ___m_SpriteCharacterLookup_14;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.SpriteGlyph> UnityEngine.TextCore.Text.SpriteAsset::m_SpriteGlyphTable
	List_1_t063B87D3CFDC3AEE80E33EFBDA1410C697D71AD6* ___m_SpriteGlyphTable_15;
	// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Text.SpriteGlyph> UnityEngine.TextCore.Text.SpriteAsset::m_SpriteGlyphLookup
	Dictionary_2_tDC0461D8CBB2E6B52DD2C421114EDE7C1C70DE73* ___m_SpriteGlyphLookup_16;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.SpriteAsset> UnityEngine.TextCore.Text.SpriteAsset::fallbackSpriteAssets
	List_1_t3EE59C28A34FCD5060EF6B6BAFA85F2C9D01D320* ___fallbackSpriteAssets_17;
	// System.Boolean UnityEngine.TextCore.Text.SpriteAsset::m_IsSpriteAssetLookupTablesDirty
	bool ___m_IsSpriteAssetLookupTablesDirty_18;
};

// UnityEngine.U2D.SpriteShapeRenderer
struct SpriteShapeRenderer_tE998BB73CF661079736CCC23617E597AB230A4AC  : public Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF
{
};

// UnityEngine.TextCore.Text.TextGenerator
struct TextGenerator_t6B84DC798596D3A9944DC346DD453C075EE62366  : public RuntimeObject
{
	// UnityEngine.Vector3[] UnityEngine.TextCore.Text.TextGenerator::m_RectTransformCorners
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___m_RectTransformCorners_1;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_MarginWidth
	float ___m_MarginWidth_2;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_MarginHeight
	float ___m_MarginHeight_3;
	// System.Int32[] UnityEngine.TextCore.Text.TextGenerator::m_CharBuffer
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___m_CharBuffer_4;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_PreferredWidth
	float ___m_PreferredWidth_5;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_PreferredHeight
	float ___m_PreferredHeight_6;
	// UnityEngine.TextCore.Text.FontAsset UnityEngine.TextCore.Text.TextGenerator::m_CurrentFontAsset
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___m_CurrentFontAsset_7;
	// UnityEngine.Material UnityEngine.TextCore.Text.TextGenerator::m_CurrentMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_CurrentMaterial_8;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator::m_CurrentMaterialIndex
	int32_t ___m_CurrentMaterialIndex_9;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.TextCore.Text.MaterialReference> UnityEngine.TextCore.Text.TextGenerator::m_MaterialReferenceStack
	TextProcessingStack_1_t0C74606C1B6C7817CA95F0DCA46B219CF6FB35CA ___m_MaterialReferenceStack_10;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_Padding
	float ___m_Padding_11;
	// UnityEngine.TextCore.Text.SpriteAsset UnityEngine.TextCore.Text.TextGenerator::m_CurrentSpriteAsset
	SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313* ___m_CurrentSpriteAsset_12;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator::m_TotalCharacterCount
	int32_t ___m_TotalCharacterCount_13;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_FontScale
	float ___m_FontScale_14;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_FontSize
	float ___m_FontSize_15;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_FontScaleMultiplier
	float ___m_FontScaleMultiplier_16;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_CurrentFontSize
	float ___m_CurrentFontSize_17;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<System.Single> UnityEngine.TextCore.Text.TextGenerator::m_SizeStack
	TextProcessingStack_1_t9FC06E35259ADD291ED640FE7554D8C03EA5F555 ___m_SizeStack_18;
	// UnityEngine.TextCore.Text.FontStyles UnityEngine.TextCore.Text.TextGenerator::m_FontStyleInternal
	int32_t ___m_FontStyleInternal_19;
	// UnityEngine.TextCore.Text.FontStyleStack UnityEngine.TextCore.Text.TextGenerator::m_FontStyleStack
	FontStyleStack_t63C77495F068E6DF762D6AF063A817E3709659A7 ___m_FontStyleStack_20;
	// UnityEngine.TextCore.Text.TextFontWeight UnityEngine.TextCore.Text.TextGenerator::m_FontWeightInternal
	int32_t ___m_FontWeightInternal_21;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.TextCore.Text.TextFontWeight> UnityEngine.TextCore.Text.TextGenerator::m_FontWeightStack
	TextProcessingStack_1_t698B87CDD968C2046F57134BB3AB807EBFFD7790 ___m_FontWeightStack_22;
	// UnityEngine.TextCore.Text.TextAlignment UnityEngine.TextCore.Text.TextGenerator::m_LineJustification
	int32_t ___m_LineJustification_23;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.TextCore.Text.TextAlignment> UnityEngine.TextCore.Text.TextGenerator::m_LineJustificationStack
	TextProcessingStack_1_tE63296B08A4C34E38D7EF3FFFA3470076B9E3A0F ___m_LineJustificationStack_24;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_BaselineOffset
	float ___m_BaselineOffset_25;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<System.Single> UnityEngine.TextCore.Text.TextGenerator::m_BaselineOffsetStack
	TextProcessingStack_1_t9FC06E35259ADD291ED640FE7554D8C03EA5F555 ___m_BaselineOffsetStack_26;
	// UnityEngine.Color32 UnityEngine.TextCore.Text.TextGenerator::m_FontColor32
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_FontColor32_27;
	// UnityEngine.Color32 UnityEngine.TextCore.Text.TextGenerator::m_HtmlColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_HtmlColor_28;
	// UnityEngine.Color32 UnityEngine.TextCore.Text.TextGenerator::m_UnderlineColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_UnderlineColor_29;
	// UnityEngine.Color32 UnityEngine.TextCore.Text.TextGenerator::m_StrikethroughColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_StrikethroughColor_30;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.Color32> UnityEngine.TextCore.Text.TextGenerator::m_ColorStack
	TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 ___m_ColorStack_31;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.Color32> UnityEngine.TextCore.Text.TextGenerator::m_UnderlineColorStack
	TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 ___m_UnderlineColorStack_32;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.Color32> UnityEngine.TextCore.Text.TextGenerator::m_StrikethroughColorStack
	TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 ___m_StrikethroughColorStack_33;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.Color32> UnityEngine.TextCore.Text.TextGenerator::m_HighlightColorStack
	TextProcessingStack_1_t7868E818AC1E1B5FED21B76D5C309C9A04380B63 ___m_HighlightColorStack_34;
	// UnityEngine.TextCore.Text.TextColorGradient UnityEngine.TextCore.Text.TextGenerator::m_ColorGradientPreset
	TextColorGradient_t22D94E441E8E8CD772B966C167E5C0AEB0919D70* ___m_ColorGradientPreset_35;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<UnityEngine.TextCore.Text.TextColorGradient> UnityEngine.TextCore.Text.TextGenerator::m_ColorGradientStack
	TextProcessingStack_1_t0F39F088E8F8F6E18C3C463B2998ADC5B7A0513E ___m_ColorGradientStack_36;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<System.Int32> UnityEngine.TextCore.Text.TextGenerator::m_ActionStack
	TextProcessingStack_1_t27834AAB14D26DC6519558C4C2574BA9C190D8E8 ___m_ActionStack_37;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerator::m_IsFxMatrixSet
	bool ___m_IsFxMatrixSet_38;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_LineOffset
	float ___m_LineOffset_39;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_LineHeight
	float ___m_LineHeight_40;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_CSpacing
	float ___m_CSpacing_41;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_MonoSpacing
	float ___m_MonoSpacing_42;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_XAdvance
	float ___m_XAdvance_43;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_TagLineIndent
	float ___m_TagLineIndent_44;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_TagIndent
	float ___m_TagIndent_45;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<System.Single> UnityEngine.TextCore.Text.TextGenerator::m_IndentStack
	TextProcessingStack_1_t9FC06E35259ADD291ED640FE7554D8C03EA5F555 ___m_IndentStack_46;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerator::m_TagNoParsing
	bool ___m_TagNoParsing_47;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator::m_CharacterCount
	int32_t ___m_CharacterCount_48;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator::m_FirstCharacterOfLine
	int32_t ___m_FirstCharacterOfLine_49;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator::m_LastCharacterOfLine
	int32_t ___m_LastCharacterOfLine_50;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator::m_FirstVisibleCharacterOfLine
	int32_t ___m_FirstVisibleCharacterOfLine_51;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator::m_LastVisibleCharacterOfLine
	int32_t ___m_LastVisibleCharacterOfLine_52;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_MaxLineAscender
	float ___m_MaxLineAscender_53;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_MaxLineDescender
	float ___m_MaxLineDescender_54;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator::m_LineNumber
	int32_t ___m_LineNumber_55;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator::m_LineVisibleCharacterCount
	int32_t ___m_LineVisibleCharacterCount_56;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator::m_FirstOverflowCharacterIndex
	int32_t ___m_FirstOverflowCharacterIndex_57;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator::m_PageNumber
	int32_t ___m_PageNumber_58;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_MarginLeft
	float ___m_MarginLeft_59;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_MarginRight
	float ___m_MarginRight_60;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_Width
	float ___m_Width_61;
	// UnityEngine.TextCore.Text.Extents UnityEngine.TextCore.Text.TextGenerator::m_MeshExtents
	Extents_t369FB2B84521A0229C2FA3D4C8592B14E07CEFE6 ___m_MeshExtents_62;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_MaxCapHeight
	float ___m_MaxCapHeight_63;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_MaxAscender
	float ___m_MaxAscender_64;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_MaxDescender
	float ___m_MaxDescender_65;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerator::m_IsNewPage
	bool ___m_IsNewPage_66;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerator::m_IsNonBreakingSpace
	bool ___m_IsNonBreakingSpace_67;
	// UnityEngine.TextCore.Text.WordWrapState UnityEngine.TextCore.Text.TextGenerator::m_SavedWordWrapState
	WordWrapState_tD71131CF008362DB9562FB9794AE9D9225D8F123 ___m_SavedWordWrapState_68;
	// UnityEngine.TextCore.Text.WordWrapState UnityEngine.TextCore.Text.TextGenerator::m_SavedLineState
	WordWrapState_tD71131CF008362DB9562FB9794AE9D9225D8F123 ___m_SavedLineState_69;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator::m_LoopCountA
	int32_t ___m_LoopCountA_70;
	// UnityEngine.TextCore.Text.TextElementType UnityEngine.TextCore.Text.TextGenerator::m_TextElementType
	uint8_t ___m_TextElementType_71;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerator::m_IsParsingText
	bool ___m_IsParsingText_72;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator::m_SpriteIndex
	int32_t ___m_SpriteIndex_73;
	// UnityEngine.Color32 UnityEngine.TextCore.Text.TextGenerator::m_SpriteColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_SpriteColor_74;
	// UnityEngine.TextCore.Text.TextElement UnityEngine.TextCore.Text.TextGenerator::m_CachedTextElement
	TextElement_tCEF567A8810788262275B39DC39CBA6EBE7472DA* ___m_CachedTextElement_75;
	// UnityEngine.Color32 UnityEngine.TextCore.Text.TextGenerator::m_HighlightColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_HighlightColor_76;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_CharWidthAdjDelta
	float ___m_CharWidthAdjDelta_77;
	// UnityEngine.Matrix4x4 UnityEngine.TextCore.Text.TextGenerator::m_FxMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___m_FxMatrix_78;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_MaxFontSize
	float ___m_MaxFontSize_79;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_MinFontSize
	float ___m_MinFontSize_80;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerator::m_IsCharacterWrappingEnabled
	bool ___m_IsCharacterWrappingEnabled_81;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_StartOfLineAscender
	float ___m_StartOfLineAscender_82;
	// System.Single UnityEngine.TextCore.Text.TextGenerator::m_LineSpacingDelta
	float ___m_LineSpacingDelta_83;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerator::m_IsMaskingEnabled
	bool ___m_IsMaskingEnabled_84;
	// UnityEngine.TextCore.Text.MaterialReference[] UnityEngine.TextCore.Text.TextGenerator::m_MaterialReferences
	MaterialReferenceU5BU5D_t4A9B88114E223BD96CE5121053664023CE2DE07E* ___m_MaterialReferences_85;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator::m_SpriteCount
	int32_t ___m_SpriteCount_86;
	// UnityEngine.TextCore.Text.TextProcessingStack`1<System.Int32> UnityEngine.TextCore.Text.TextGenerator::m_StyleStack
	TextProcessingStack_1_t27834AAB14D26DC6519558C4C2574BA9C190D8E8 ___m_StyleStack_87;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator::m_SpriteAnimationId
	int32_t ___m_SpriteAnimationId_88;
	// System.UInt32[] UnityEngine.TextCore.Text.TextGenerator::m_InternalTextParsingBuffer
	UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA* ___m_InternalTextParsingBuffer_89;
	// UnityEngine.TextCore.Text.RichTextTagAttribute[] UnityEngine.TextCore.Text.TextGenerator::m_Attributes
	RichTextTagAttributeU5BU5D_tEE9D071B3246F23742DBF4226567620BCBB24A14* ___m_Attributes_90;
	// UnityEngine.TextCore.Text.XmlTagAttribute[] UnityEngine.TextCore.Text.TextGenerator::m_XmlAttribute
	XmlTagAttributeU5BU5D_tEDFE75BDDC81D11CEA2F2A12583516D3BFB309B2* ___m_XmlAttribute_91;
	// System.Char[] UnityEngine.TextCore.Text.TextGenerator::m_RichTextTag
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___m_RichTextTag_92;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> UnityEngine.TextCore.Text.TextGenerator::m_MaterialReferenceIndexLookup
	Dictionary_2_tABE19B9C5C52F1DE14F0D3287B2696E7D7419180* ___m_MaterialReferenceIndexLookup_93;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerator::m_IsCalculatingPreferredValues
	bool ___m_IsCalculatingPreferredValues_94;
	// UnityEngine.TextCore.Text.SpriteAsset UnityEngine.TextCore.Text.TextGenerator::m_DefaultSpriteAsset
	SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313* ___m_DefaultSpriteAsset_95;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerator::m_TintSprite
	bool ___m_TintSprite_96;
	// UnityEngine.TextCore.Text.TextGenerator/SpecialCharacter UnityEngine.TextCore.Text.TextGenerator::m_Ellipsis
	SpecialCharacter_t869F8BE65A7FE32AFD4196118258F49A63D8E2BD ___m_Ellipsis_97;
	// UnityEngine.TextCore.Text.TextGenerator/SpecialCharacter UnityEngine.TextCore.Text.TextGenerator::m_Underline
	SpecialCharacter_t869F8BE65A7FE32AFD4196118258F49A63D8E2BD ___m_Underline_98;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerator::m_IsUsingBold
	bool ___m_IsUsingBold_99;
	// System.Boolean UnityEngine.TextCore.Text.TextGenerator::m_IsSdfShader
	bool ___m_IsSdfShader_100;
	// UnityEngine.TextCore.Text.TextElementInfo[] UnityEngine.TextCore.Text.TextGenerator::m_InternalTextElementInfo
	TextElementInfoU5BU5D_tEC28C9B72883EE21AA798913497C69E179A15C4E* ___m_InternalTextElementInfo_101;
	// System.Int32 UnityEngine.TextCore.Text.TextGenerator::m_RecursiveCount
	int32_t ___m_RecursiveCount_102;
};

// UnityEngine.Tilemaps.TilemapRenderer
struct TilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB  : public Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF
{
};

// UnityEngine.Timeline.TimelineAsset
struct TimelineAsset_tE400C944B07CA9D1349BAD84545E24075ADB3496  : public PlayableAsset_t6964211C3DAE503FEEDD04089ED6B962945D271E
{
	// System.Int32 UnityEngine.Timeline.TimelineAsset::m_Version
	int32_t ___m_Version_5;
	// System.Collections.Generic.List`1<UnityEngine.ScriptableObject> UnityEngine.Timeline.TimelineAsset::m_Tracks
	List_1_tF941E9C3FEB6F1C2D20E73A90AA7F6319EB3F828* ___m_Tracks_6;
	// System.Double UnityEngine.Timeline.TimelineAsset::m_FixedDuration
	double ___m_FixedDuration_7;
	// UnityEngine.Timeline.TrackAsset[] UnityEngine.Timeline.TimelineAsset::m_CacheOutputTracks
	TrackAssetU5BU5D_tE6935AFD32D0BE4B0C69D1CCE96B55D383BCF88C* ___m_CacheOutputTracks_8;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TimelineAsset::m_CacheRootTracks
	List_1_t6908BEEFB57470CB30420983896AA06BFB8796F0* ___m_CacheRootTracks_9;
	// UnityEngine.Timeline.TrackAsset[] UnityEngine.Timeline.TimelineAsset::m_CacheFlattenedTracks
	TrackAssetU5BU5D_tE6935AFD32D0BE4B0C69D1CCE96B55D383BCF88C* ___m_CacheFlattenedTracks_10;
	// UnityEngine.Timeline.TimelineAsset/EditorSettings UnityEngine.Timeline.TimelineAsset::m_EditorSettings
	EditorSettings_t3A8D02A80F57944B75911D9692FECDF6B7081DFF* ___m_EditorSettings_11;
	// UnityEngine.Timeline.TimelineAsset/DurationMode UnityEngine.Timeline.TimelineAsset::m_DurationMode
	int32_t ___m_DurationMode_12;
	// UnityEngine.Timeline.MarkerTrack UnityEngine.Timeline.TimelineAsset::m_MarkerTrack
	MarkerTrack_tE18594CE52CCC412606B1B5A147DD3A4F7D056C2* ___m_MarkerTrack_13;
};

// UnityEngine.Timeline.TrackAsset
struct TrackAsset_t31E19BE900C90F6616C0D337652C8614CD833B96  : public PlayableAsset_t6964211C3DAE503FEEDD04089ED6B962945D271E
{
	// System.Int32 UnityEngine.Timeline.TrackAsset::m_Version
	int32_t ___m_Version_5;
	// UnityEngine.AnimationClip UnityEngine.Timeline.TrackAsset::m_AnimClip
	AnimationClip_t00BD2F131D308A4AD2C6B0BF66644FC25FECE712* ___m_AnimClip_6;
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Locked
	bool ___m_Locked_11;
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Muted
	bool ___m_Muted_12;
	// System.String UnityEngine.Timeline.TrackAsset::m_CustomPlayableFullTypename
	String_t* ___m_CustomPlayableFullTypename_13;
	// UnityEngine.AnimationClip UnityEngine.Timeline.TrackAsset::m_Curves
	AnimationClip_t00BD2F131D308A4AD2C6B0BF66644FC25FECE712* ___m_Curves_14;
	// UnityEngine.Playables.PlayableAsset UnityEngine.Timeline.TrackAsset::m_Parent
	PlayableAsset_t6964211C3DAE503FEEDD04089ED6B962945D271E* ___m_Parent_15;
	// System.Collections.Generic.List`1<UnityEngine.ScriptableObject> UnityEngine.Timeline.TrackAsset::m_Children
	List_1_tF941E9C3FEB6F1C2D20E73A90AA7F6319EB3F828* ___m_Children_16;
	// System.Int32 UnityEngine.Timeline.TrackAsset::m_ItemsHash
	int32_t ___m_ItemsHash_17;
	// UnityEngine.Timeline.TimelineClip[] UnityEngine.Timeline.TrackAsset::m_ClipsCache
	TimelineClipU5BU5D_t37945156A55BC896C442C4FE59198216769A4E64* ___m_ClipsCache_18;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_Start
	DiscreteTime_t1598D60B0B2432F702E2A6120D04369EE54600A6 ___m_Start_19;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_End
	DiscreteTime_t1598D60B0B2432F702E2A6120D04369EE54600A6 ___m_End_20;
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_CacheSorted
	bool ___m_CacheSorted_21;
	// System.Nullable`1<System.Boolean> UnityEngine.Timeline.TrackAsset::m_SupportsNotifications
	Nullable_1_t78F453FADB4A9F50F267A4E349019C34410D1A01 ___m_SupportsNotifications_22;
	// System.Collections.Generic.IEnumerable`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TrackAsset::m_ChildTrackCache
	RuntimeObject* ___m_ChildTrackCache_24;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::m_Clips
	List_1_tD78196B4DE777C4B74ADAD24051A9978F5191506* ___m_Clips_26;
	// UnityEngine.Timeline.MarkerList UnityEngine.Timeline.TrackAsset::m_Markers
	MarkerList_tD4B632EBA98CE678EB8D108A1AF559F734FA7698 ___m_Markers_27;
};

// UnityEngine.Timeline.AnimationTrack
struct AnimationTrack_tBE37C239976A84F0A2E6BA3B77263B4F44BB1359  : public TrackAsset_t31E19BE900C90F6616C0D337652C8614CD833B96
{
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.AnimationTrack::m_InfiniteClipPreExtrapolation
	int32_t ___m_InfiniteClipPreExtrapolation_30;
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.AnimationTrack::m_InfiniteClipPostExtrapolation
	int32_t ___m_InfiniteClipPostExtrapolation_31;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationTrack::m_InfiniteClipOffsetPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_InfiniteClipOffsetPosition_32;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationTrack::m_InfiniteClipOffsetEulerAngles
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_InfiniteClipOffsetEulerAngles_33;
	// System.Double UnityEngine.Timeline.AnimationTrack::m_InfiniteClipTimeOffset
	double ___m_InfiniteClipTimeOffset_34;
	// System.Boolean UnityEngine.Timeline.AnimationTrack::m_InfiniteClipRemoveOffset
	bool ___m_InfiniteClipRemoveOffset_35;
	// System.Boolean UnityEngine.Timeline.AnimationTrack::m_InfiniteClipApplyFootIK
	bool ___m_InfiniteClipApplyFootIK_36;
	// UnityEngine.Timeline.AnimationPlayableAsset/LoopMode UnityEngine.Timeline.AnimationTrack::mInfiniteClipLoop
	int32_t ___mInfiniteClipLoop_37;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.AnimationTrack::m_MatchTargetFields
	int32_t ___m_MatchTargetFields_38;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationTrack::m_Position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Position_39;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationTrack::m_EulerAngles
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_EulerAngles_40;
	// UnityEngine.AvatarMask UnityEngine.Timeline.AnimationTrack::m_AvatarMask
	AvatarMask_tC1D777FFB77C952502ECF6D80FAFAD16B27B02AF* ___m_AvatarMask_41;
	// System.Boolean UnityEngine.Timeline.AnimationTrack::m_ApplyAvatarMask
	bool ___m_ApplyAvatarMask_42;
	// UnityEngine.Timeline.TrackOffset UnityEngine.Timeline.AnimationTrack::m_TrackOffset
	int32_t ___m_TrackOffset_43;
	// UnityEngine.AnimationClip UnityEngine.Timeline.AnimationTrack::m_InfiniteClip
	AnimationClip_t00BD2F131D308A4AD2C6B0BF66644FC25FECE712* ___m_InfiniteClip_44;
	// UnityEngine.Quaternion UnityEngine.Timeline.AnimationTrack::m_OpenClipOffsetRotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___m_OpenClipOffsetRotation_46;
	// UnityEngine.Quaternion UnityEngine.Timeline.AnimationTrack::m_Rotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___m_Rotation_47;
	// System.Boolean UnityEngine.Timeline.AnimationTrack::m_ApplyOffsets
	bool ___m_ApplyOffsets_48;
};

// UnityEngine.AudioListener
struct AudioListener_t1D629CE9BC079C8ECDE8F822616E8A8E319EAE35  : public AudioBehaviour_t2DC0BEF7B020C952F3D2DA5AAAC88501C7EEB941
{
};

// UnityEngine.AudioSource
struct AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299  : public AudioBehaviour_t2DC0BEF7B020C952F3D2DA5AAAC88501C7EEB941
{
};

// Kino.Bloom
struct Bloom_tA14A4ADA32420926093CB5242D06D17DA1A9BB1E  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single Kino.Bloom::_threshold
	float ____threshold_4;
	// System.Single Kino.Bloom::_softKnee
	float ____softKnee_5;
	// System.Single Kino.Bloom::_radius
	float ____radius_6;
	// System.Single Kino.Bloom::_intensity
	float ____intensity_7;
	// System.Boolean Kino.Bloom::_highQuality
	bool ____highQuality_8;
	// System.Boolean Kino.Bloom::_antiFlicker
	bool ____antiFlicker_9;
	// UnityEngine.Shader Kino.Bloom::_shader
	Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* ____shader_10;
	// UnityEngine.Material Kino.Bloom::ckn
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___ckn_11;
	// UnityEngine.RenderTexture[] Kino.Bloom::ckp
	RenderTextureU5BU5D_t9C963C4B9AAD862BBE402147E82F7BEBF699F6A6* ___ckp_13;
	// UnityEngine.RenderTexture[] Kino.Bloom::ckq
	RenderTextureU5BU5D_t9C963C4B9AAD862BBE402147E82F7BEBF699F6A6* ___ckq_14;
};

// UnityEngine.BoxCollider2D
struct BoxCollider2D_tF860C7737FFB062CEC06577E0CD8364EEC1D4EDA  : public Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52
{
};

// CartoonFX.CFXR_Demo
struct CFXR_Demo_tC798392D488F2E202DFEB3464B0A5EFEC019BE6C  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.UI.Image CartoonFX.CFXR_Demo::btnSlowMotion
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___btnSlowMotion_4;
	// UnityEngine.UI.Text CartoonFX.CFXR_Demo::lblSlowMotion
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___lblSlowMotion_5;
	// UnityEngine.UI.Image CartoonFX.CFXR_Demo::btnCameraRotation
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___btnCameraRotation_6;
	// UnityEngine.UI.Text CartoonFX.CFXR_Demo::lblCameraRotation
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___lblCameraRotation_7;
	// UnityEngine.UI.Image CartoonFX.CFXR_Demo::btnShowGround
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___btnShowGround_8;
	// UnityEngine.UI.Text CartoonFX.CFXR_Demo::lblShowGround
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___lblShowGround_9;
	// UnityEngine.UI.Image CartoonFX.CFXR_Demo::btnCamShake
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___btnCamShake_10;
	// UnityEngine.UI.Text CartoonFX.CFXR_Demo::lblCamShake
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___lblCamShake_11;
	// UnityEngine.UI.Image CartoonFX.CFXR_Demo::btnLights
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___btnLights_12;
	// UnityEngine.UI.Text CartoonFX.CFXR_Demo::lblLights
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___lblLights_13;
	// UnityEngine.UI.Image CartoonFX.CFXR_Demo::btnBloom
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___btnBloom_14;
	// UnityEngine.UI.Text CartoonFX.CFXR_Demo::lblBloom
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___lblBloom_15;
	// UnityEngine.UI.Text CartoonFX.CFXR_Demo::labelEffect
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___labelEffect_16;
	// UnityEngine.UI.Text CartoonFX.CFXR_Demo::labelIndex
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___labelIndex_17;
	// UnityEngine.GameObject CartoonFX.CFXR_Demo::ground
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ground_18;
	// UnityEngine.Collider CartoonFX.CFXR_Demo::groundCollider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___groundCollider_19;
	// UnityEngine.Transform CartoonFX.CFXR_Demo::demoCamera
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___demoCamera_20;
	// UnityEngine.MonoBehaviour CartoonFX.CFXR_Demo::bloom
	MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* ___bloom_21;
	// System.Single CartoonFX.CFXR_Demo::rotationSpeed
	float ___rotationSpeed_22;
	// System.Single CartoonFX.CFXR_Demo::zoomFactor
	float ___zoomFactor_23;
	// System.Boolean CartoonFX.CFXR_Demo::cji
	bool ___cji_24;
	// System.Boolean CartoonFX.CFXR_Demo::cjj
	bool ___cjj_25;
	// System.Boolean CartoonFX.CFXR_Demo::cjk
	bool ___cjk_26;
	// UnityEngine.GameObject CartoonFX.CFXR_Demo::currentEffect
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___currentEffect_27;
	// UnityEngine.GameObject[] CartoonFX.CFXR_Demo::cjl
	GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* ___cjl_28;
	// System.Int32 CartoonFX.CFXR_Demo::cjm
	int32_t ___cjm_29;
	// UnityEngine.Vector3 CartoonFX.CFXR_Demo::cjn
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___cjn_30;
	// UnityEngine.Quaternion CartoonFX.CFXR_Demo::cjo
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___cjo_31;
};

// CartoonFX.CFXR_Demo_Rotate
struct CFXR_Demo_Rotate_tC9BF71B92F879B0DC058EC49386E2B596A3021F2  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Vector3 CartoonFX.CFXR_Demo_Rotate::axis
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___axis_4;
	// UnityEngine.Vector3 CartoonFX.CFXR_Demo_Rotate::center
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___center_5;
	// System.Single CartoonFX.CFXR_Demo_Rotate::speed
	float ___speed_6;
};

// CartoonFX.CFXR_Demo_Translate
struct CFXR_Demo_Translate_t95CB6CDBAE93F397B9CA608E9B233BAB8C7FB891  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Vector3 CartoonFX.CFXR_Demo_Translate::direction
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___direction_4;
	// System.Boolean CartoonFX.CFXR_Demo_Translate::randomRotation
	bool ___randomRotation_5;
	// System.Boolean CartoonFX.CFXR_Demo_Translate::cjp
	bool ___cjp_6;
	// UnityEngine.Vector3 CartoonFX.CFXR_Demo_Translate::cjq
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___cjq_7;
};

// CartoonFX.CFXR_Effect
struct CFXR_Effect_t3017EDCEABB0C70663A82AAA961EE597C0AF743A  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// CartoonFX.CFXR_Effect/ClearBehavior CartoonFX.CFXR_Effect::clearBehavior
	int32_t ___clearBehavior_7;
	// CartoonFX.CFXR_Effect/CameraShake CartoonFX.CFXR_Effect::cameraShake
	CameraShake_tF662E3447A81E0F4B337C630E8DDF9758C02DFD0* ___cameraShake_8;
	// CartoonFX.CFXR_Effect/AnimatedLight[] CartoonFX.CFXR_Effect::animatedLights
	AnimatedLightU5BU5D_t18D44AAF10E0A23939730E3057A03CE2E81E4879* ___animatedLights_9;
	// UnityEngine.ParticleSystem CartoonFX.CFXR_Effect::fadeOutReference
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___fadeOutReference_10;
	// System.Single CartoonFX.CFXR_Effect::cjx
	float ___cjx_11;
	// UnityEngine.ParticleSystem CartoonFX.CFXR_Effect::cjy
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___cjy_12;
	// UnityEngine.MaterialPropertyBlock CartoonFX.CFXR_Effect::cjz
	MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* ___cjz_13;
	// UnityEngine.Renderer CartoonFX.CFXR_Effect::cka
	Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* ___cka_14;
	// System.Int32 CartoonFX.CFXR_Effect::ckd
	int32_t ___ckd_17;
	// System.Boolean CartoonFX.CFXR_Effect::cke
	bool ___cke_18;
	// System.Single CartoonFX.CFXR_Effect::ckf
	float ___ckf_19;
};

// CartoonFX.CFXR_EmissionBySurface
struct CFXR_EmissionBySurface_t0512AB30B6428D57F6153E5928283CD14E1DFDD7  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Boolean CartoonFX.CFXR_EmissionBySurface::active
	bool ___active_4;
	// System.Single CartoonFX.CFXR_EmissionBySurface::particlesPerUnit
	float ___particlesPerUnit_5;
	// System.Single CartoonFX.CFXR_EmissionBySurface::maxEmissionRate
	float ___maxEmissionRate_6;
	// System.Single CartoonFX.CFXR_EmissionBySurface::density
	float ___density_7;
	// System.Boolean CartoonFX.CFXR_EmissionBySurface::ckg
	bool ___ckg_8;
	// UnityEngine.ParticleSystem CartoonFX.CFXR_EmissionBySurface::ckh
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___ckh_9;
};

// CartoonFX.CFXR_ParticleText
struct CFXR_ParticleText_t4E5797B5E79E3513E6901E6C7CD8ACB5891EA1D3  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Boolean CartoonFX.CFXR_ParticleText::isDynamic
	bool ___isDynamic_4;
	// System.String CartoonFX.CFXR_ParticleText::text
	String_t* ___text_5;
	// System.Single CartoonFX.CFXR_ParticleText::size
	float ___size_6;
	// System.Single CartoonFX.CFXR_ParticleText::letterSpacing
	float ___letterSpacing_7;
	// UnityEngine.Color CartoonFX.CFXR_ParticleText::backgroundColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___backgroundColor_8;
	// UnityEngine.Color CartoonFX.CFXR_ParticleText::color1
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color1_9;
	// UnityEngine.Color CartoonFX.CFXR_ParticleText::color2
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color2_10;
	// System.Single CartoonFX.CFXR_ParticleText::delay
	float ___delay_11;
	// System.Boolean CartoonFX.CFXR_ParticleText::cumulativeDelay
	bool ___cumulativeDelay_12;
	// System.Single CartoonFX.CFXR_ParticleText::compensateLifetime
	float ___compensateLifetime_13;
	// System.Single CartoonFX.CFXR_ParticleText::lifetimeMultiplier
	float ___lifetimeMultiplier_14;
	// System.Single CartoonFX.CFXR_ParticleText::rotation
	float ___rotation_15;
	// System.Single CartoonFX.CFXR_ParticleText::sortingFudgeOffset
	float ___sortingFudgeOffset_16;
	// CartoonFX.CFXR_ParticleTextFontAsset CartoonFX.CFXR_ParticleText::font
	CFXR_ParticleTextFontAsset_t19D1F73819A2F60E0FA1B17ECA99FB3FAAF756ED* ___font_17;
	// System.Single CartoonFX.CFXR_ParticleText::cki
	float ___cki_18;
	// System.Single CartoonFX.CFXR_ParticleText::ckj
	float ___ckj_19;
	// System.Single CartoonFX.CFXR_ParticleText::ckk
	float ___ckk_20;
	// System.Single CartoonFX.CFXR_ParticleText::ckl
	float ___ckl_21;
	// UnityEngine.Vector3 CartoonFX.CFXR_ParticleText::ckm
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___ckm_22;
};

// UnityEngine.CircleCollider2D
struct CircleCollider2D_t5D665D58EACA966EA4033BCF0EE91E198552E786  : public Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52
{
};

// UnityEngine.CompositeCollider2D
struct CompositeCollider2D_t58511A535241FD7BDA84C6F3DF2C38220D4079D8  : public Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52
{
};

// DG.Tweening.Core.DOTweenComponent
struct DOTweenComponent_tEA6C5A1520B40681AE6FA1703529F60EBC3691DC  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 DG.Tweening.Core.DOTweenComponent::inspectorUpdater
	int32_t ___inspectorUpdater_4;
	// System.Single DG.Tweening.Core.DOTweenComponent::_unscaledTime
	float ____unscaledTime_5;
	// System.Single DG.Tweening.Core.DOTweenComponent::_unscaledDeltaTime
	float ____unscaledDeltaTime_6;
	// System.Boolean DG.Tweening.Core.DOTweenComponent::_paused
	bool ____paused_7;
	// System.Single DG.Tweening.Core.DOTweenComponent::_pausedTime
	float ____pausedTime_8;
	// System.Boolean DG.Tweening.Core.DOTweenComponent::_isQuitting
	bool ____isQuitting_9;
	// System.Boolean DG.Tweening.Core.DOTweenComponent::_duplicateToDestroy
	bool ____duplicateToDestroy_10;
};

// UnityEngine.Timeline.GroupTrack
struct GroupTrack_tF8FE0FA9A74536ECFBF0C328194D206A7CAA5A5D  : public TrackAsset_t31E19BE900C90F6616C0D337652C8614CD833B96
{
};

// UnityEngine.Timeline.MarkerTrack
struct MarkerTrack_tE18594CE52CCC412606B1B5A147DD3A4F7D056C2  : public TrackAsset_t31E19BE900C90F6616C0D337652C8614CD833B96
{
};

// UnityEngine.Timeline.PlayableTrack
struct PlayableTrack_t26BA2795391A45BA8C114EAF95EDCACC2EC25B8F  : public TrackAsset_t31E19BE900C90F6616C0D337652C8614CD833B96
{
};

// UnityEngine.PolygonCollider2D
struct PolygonCollider2D_t7CEFFFEE6522175436B408712B052D236889C89E  : public Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52
{
};

// Unity.ThrowStub
struct ThrowStub_t9161280E38728A40D9B1A975AEE62E89C379E400  : public ObjectDisposedException_tC5FB29E8E980E2010A2F6A5B9B791089419F89EB
{
};

// UnityEngine.Tilemaps.Tilemap
struct Tilemap_t18C4166D0AC702D5BFC0C411FA73C4B61D9D1751  : public GridLayout_tAD661B1E1E57C16BE21C8C13432EA04FE1F0418B
{
};

// WebViewObject
struct WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Action`1<System.String> WebViewObject::onJS
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onJS_4;
	// System.Action`1<System.String> WebViewObject::onError
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onError_5;
	// System.Action`1<System.String> WebViewObject::onHttpError
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onHttpError_6;
	// System.Action`1<System.String> WebViewObject::onStarted
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onStarted_7;
	// System.Action`1<System.String> WebViewObject::onLoaded
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onLoaded_8;
	// System.Action`1<System.String> WebViewObject::onHooked
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onHooked_9;
	// System.Action`1<System.String> WebViewObject::onCookies
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onCookies_10;
	// System.Boolean WebViewObject::visibility
	bool ___visibility_11;
	// System.Boolean WebViewObject::alertDialogEnabled
	bool ___alertDialogEnabled_12;
	// System.Boolean WebViewObject::scrollBounceEnabled
	bool ___scrollBounceEnabled_13;
	// System.Int32 WebViewObject::mMarginLeft
	int32_t ___mMarginLeft_14;
	// System.Int32 WebViewObject::mMarginTop
	int32_t ___mMarginTop_15;
	// System.Int32 WebViewObject::mMarginRight
	int32_t ___mMarginRight_16;
	// System.Int32 WebViewObject::mMarginBottom
	int32_t ___mMarginBottom_17;
	// System.Boolean WebViewObject::mMarginRelative
	bool ___mMarginRelative_18;
	// System.Single WebViewObject::mMarginLeftComputed
	float ___mMarginLeftComputed_19;
	// System.Single WebViewObject::mMarginTopComputed
	float ___mMarginTopComputed_20;
	// System.Single WebViewObject::mMarginRightComputed
	float ___mMarginRightComputed_21;
	// System.Single WebViewObject::mMarginBottomComputed
	float ___mMarginBottomComputed_22;
	// System.Boolean WebViewObject::mMarginRelativeComputed
	bool ___mMarginRelativeComputed_23;
	// System.IntPtr WebViewObject::webView
	intptr_t ___webView_24;
};

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <Module>

// <PrivateImplementationDetails>

// <PrivateImplementationDetails>

// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder

// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder

// DG.Tweening.Core.ABSSequentiable

// DG.Tweening.Core.ABSSequentiable

// Mono.Security.ASN1

// Mono.Security.ASN1

// Mono.Security.ASN1Convert

// Mono.Security.ASN1Convert

// UnityEngine.SocialPlatforms.Impl.AchievementDescription

// UnityEngine.SocialPlatforms.Impl.AchievementDescription

// UnityEngine.Analytics.AnalyticsSessionInfo
struct AnalyticsSessionInfo_tDE8F7A9E13EF9723E2D975F76E916753DA61AD76_StaticFields
{
	// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged UnityEngine.Analytics.AnalyticsSessionInfo::sessionStateChanged
	SessionStateChanged_t1180FB66E702B635CAD9316DC661D931277B2A0C* ___sessionStateChanged_0;
	// UnityEngine.Analytics.AnalyticsSessionInfo/IdentityTokenChanged UnityEngine.Analytics.AnalyticsSessionInfo::identityTokenChanged
	IdentityTokenChanged_tE8CB0DAB5F6E640A847803F582E6CB6237742395* ___identityTokenChanged_1;
};

// UnityEngine.Analytics.AnalyticsSessionInfo

// UnityEngine.Animations.AnimationPlayableBinding

// UnityEngine.Animations.AnimationPlayableBinding

// UnityEngine.Animations.AnimationPlayableExtensions

// UnityEngine.Animations.AnimationPlayableExtensions

// UnityEngine.Animations.AnimationPlayableGraphExtensions

// UnityEngine.Animations.AnimationPlayableGraphExtensions

// UnityEngine.Experimental.Audio.AudioSampleProvider

// UnityEngine.Experimental.Audio.AudioSampleProvider

// UnityEngine.AudioSettings
struct AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD_StaticFields
{
	// UnityEngine.AudioSettings/AudioConfigurationChangeHandler UnityEngine.AudioSettings::OnAudioConfigurationChanged
	AudioConfigurationChangeHandler_tE071B0CBA3B3A77D3E41F5FCB65B4017885B3177* ___OnAudioConfigurationChanged_0;
	// System.Action UnityEngine.AudioSettings::OnAudioSystemShuttingDown
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___OnAudioSystemShuttingDown_1;
	// System.Action UnityEngine.AudioSettings::OnAudioSystemStartedUp
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___OnAudioSystemStartedUp_2;
};

// UnityEngine.AudioSettings

// Mono.Security.BitConverterLE

// Mono.Security.BitConverterLE

// DG.Tweening.Core.Easing.Bounce

// DG.Tweening.Core.Easing.Bounce

// Unity.Burst.BurstCompiler
struct BurstCompiler_t2715484E1FF256726FC4D4D8E17C35A4C8DFA2B8_StaticFields
{
	// System.Boolean Unity.Burst.BurstCompiler::_IsEnabled
	bool ____IsEnabled_0;
	// Unity.Burst.BurstCompilerOptions Unity.Burst.BurstCompiler::Options
	BurstCompilerOptions_t5F93118F305E1B0C950C6F9AF8BCA74033DA01C9* ___Options_1;
	// System.Reflection.MethodInfo Unity.Burst.BurstCompiler::DummyMethodInfo
	MethodInfo_t* ___DummyMethodInfo_2;
};

// Unity.Burst.BurstCompiler

// Unity.Burst.BurstCompilerOptions
struct BurstCompilerOptions_t5F93118F305E1B0C950C6F9AF8BCA74033DA01C9_StaticFields
{
	// System.Boolean Unity.Burst.BurstCompilerOptions::ForceDisableBurstCompilation
	bool ___ForceDisableBurstCompilation_0;
	// System.Boolean Unity.Burst.BurstCompilerOptions::ForceBurstCompilationSynchronously
	bool ___ForceBurstCompilationSynchronously_1;
	// System.Boolean Unity.Burst.BurstCompilerOptions::IsSecondaryUnityProcess
	bool ___IsSecondaryUnityProcess_2;
};

// Unity.Burst.BurstCompilerOptions

// Unity.Burst.BurstRuntime

// Unity.Burst.BurstRuntime

// Unity.Burst.BurstString
struct BurstString_tD6AF700FD5AF48728FC90C6CA2AA2E48C6472AF1_StaticFields
{
	// System.Char[] Unity.Burst.BurstString::SplitByColon
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___SplitByColon_0;
	// System.Byte[] Unity.Burst.BurstString::logTable
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___logTable_1;
	// System.UInt32[] Unity.Burst.BurstString::g_PowerOf10_U32
	UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA* ___g_PowerOf10_U32_2;
	// System.Byte[] Unity.Burst.BurstString::InfinityString
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___InfinityString_3;
	// System.Byte[] Unity.Burst.BurstString::NanString
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___NanString_4;
};

// Unity.Burst.BurstString

// UnityEngine.CameraRaycastHelper

// UnityEngine.CameraRaycastHelper

// UnityEngine.TextCore.Text.ColorUtilities

// UnityEngine.TextCore.Text.ColorUtilities

// System.Configuration.ConfigurationElement

// System.Configuration.ConfigurationElement

// System.Configuration.ConfigurationPropertyCollection

// System.Configuration.ConfigurationPropertyCollection

// System.Configuration.ConfigurationSectionGroup

// System.Configuration.ConfigurationSectionGroup

// UnityEngine.Analytics.ContinuousEvent

// UnityEngine.Analytics.ContinuousEvent

// System.Dynamic.Utils.ContractUtils

// System.Dynamic.Utils.ContractUtils

// Mono.Security.Cryptography.CryptoConvert

// Mono.Security.Cryptography.CryptoConvert

// DG.Tweening.DOTweenCYInstruction

// DG.Tweening.DOTweenCYInstruction

// DG.Tweening.Core.DOTweenExternalCommand
struct DOTweenExternalCommand_tAD5F307461CED759CA3533F555E48B09C354C49D_StaticFields
{
	// System.Action`4<DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform> DG.Tweening.Core.DOTweenExternalCommand::SetOrientationOnPath
	Action_4_t9AF66ACF00E5AEB0F9B4A06FAEFEA18B2F266BDB* ___SetOrientationOnPath_0;
};

// DG.Tweening.Core.DOTweenExternalCommand

// DG.Tweening.DOTweenModuleAudio

// DG.Tweening.DOTweenModuleAudio

// DG.Tweening.DOTweenModulePhysics

// DG.Tweening.DOTweenModulePhysics

// DG.Tweening.DOTweenModulePhysics2D

// DG.Tweening.DOTweenModulePhysics2D

// DG.Tweening.DOTweenModuleSprite

// DG.Tweening.DOTweenModuleSprite

// DG.Tweening.DOTweenModuleUI

// DG.Tweening.DOTweenModuleUI

// DG.Tweening.DOTweenModuleUnityVersion

// DG.Tweening.DOTweenModuleUnityVersion

// DG.Tweening.DOTweenModuleUtils
struct DOTweenModuleUtils_t5554865584F951A4A4E5DD282E6EBC60F5CEC6E9_StaticFields
{
	// System.Boolean DG.Tweening.DOTweenModuleUtils::cjh
	bool ___cjh_0;
};

// DG.Tweening.DOTweenModuleUtils

// DG.Tweening.Core.DOTweenUtils
struct DOTweenUtils_t88FB5E3E9A98EB0C75054BC17E200352D2D658D1_StaticFields
{
	// System.Reflection.Assembly[] DG.Tweening.Core.DOTweenUtils::_loadedAssemblies
	AssemblyU5BU5D_t97B7B4E3FD4DA4944A4BFAA4DC484EA7D990B339* ____loadedAssemblies_0;
	// System.String[] DG.Tweening.Core.DOTweenUtils::_defAssembliesToQuery
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ____defAssembliesToQuery_1;
};

// DG.Tweening.Core.DOTweenUtils

// DG.Tweening.Core.Debugger
struct Debugger_tCF42DBFBF81B46CDEE59CA397F2860F3427FE1F0_StaticFields
{
	// System.Int32 DG.Tweening.Core.Debugger::_logPriority
	int32_t ____logPriority_0;
};

// DG.Tweening.Core.Debugger

// DG.Tweening.Core.Easing.EaseCurve

// DG.Tweening.Core.Easing.EaseCurve

// DG.Tweening.Core.Easing.EaseManager

// DG.Tweening.Core.Easing.EaseManager

// System.Linq.Enumerable

// System.Linq.Enumerable

// System.Linq.Error

// System.Linq.Error

// System.Linq.Expressions.Error

// System.Linq.Expressions.Error

// System.Linq.Expressions.Expression
struct Expression_t70AA908ECBD33E94249BF235E4EBB0F831AD8785_StaticFields
{
	// System.Dynamic.Utils.CacheDict`2<System.Type,System.Reflection.MethodInfo> System.Linq.Expressions.Expression::s_lambdaDelegateCache
	CacheDict_2_tB695739D50653F4D4C3DA03BCF07CC868196FB15* ___s_lambdaDelegateCache_0;
	// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Linq.Expressions.Expression,System.Linq.Expressions.Expression/ExtensionInfo> System.Linq.Expressions.Expression::s_legacyCtorSupportTable
	ConditionalWeakTable_2_t0F3FDA57EE333DF8B8C1F3FB944E4E19C5DDCFC7* ___s_legacyCtorSupportTable_1;
};

// System.Linq.Expressions.Expression

// DG.Tweening.Core.Extensions

// DG.Tweening.Core.Extensions

// UnityEngine.Timeline.Extrapolation
struct Extrapolation_t4258D23965C0DA9509062D44F7A2AACA99A59DFE_StaticFields
{
	// System.Double UnityEngine.Timeline.Extrapolation::kMinExtrapolationTime
	double ___kMinExtrapolationTime_0;
};

// UnityEngine.Timeline.Extrapolation

// DG.Tweening.Core.Easing.Flash

// DG.Tweening.Core.Easing.Flash

// UnityEngine.TextCore.Text.FontAssetUtilities
struct FontAssetUtilities_t791E501B3F727200A3751332E495071520D393A6_StaticFields
{
	// System.Collections.Generic.HashSet`1<System.Int32> UnityEngine.TextCore.Text.FontAssetUtilities::k_SearchedAssets
	HashSet_1_t4A2F2B74276D0AD3ED0F873045BD61E9504ECAE2* ___k_SearchedAssets_0;
};

// UnityEngine.TextCore.Text.FontAssetUtilities

// UnityEngine.TextCore.LowLevel.FontEngine
struct FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields
{
	// UnityEngine.TextCore.Glyph[] UnityEngine.TextCore.LowLevel.FontEngine::s_Glyphs
	GlyphU5BU5D_t345CEC8703A6C650639C40DB7D35269A2D467FC5* ___s_Glyphs_0;
	// System.UInt32[] UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphIndexes_MarshallingArray_A
	UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA* ___s_GlyphIndexes_MarshallingArray_A_1;
	// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct[] UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphMarshallingStruct_IN
	GlyphMarshallingStructU5BU5D_t9424A4B1FAAD615472A9346208026B1B9E22069E* ___s_GlyphMarshallingStruct_IN_2;
	// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct[] UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphMarshallingStruct_OUT
	GlyphMarshallingStructU5BU5D_t9424A4B1FAAD615472A9346208026B1B9E22069E* ___s_GlyphMarshallingStruct_OUT_3;
	// UnityEngine.TextCore.GlyphRect[] UnityEngine.TextCore.LowLevel.FontEngine::s_FreeGlyphRects
	GlyphRectU5BU5D_t494B690215E3F3F42B6F216930A461256CE2CC70* ___s_FreeGlyphRects_4;
	// UnityEngine.TextCore.GlyphRect[] UnityEngine.TextCore.LowLevel.FontEngine::s_UsedGlyphRects
	GlyphRectU5BU5D_t494B690215E3F3F42B6F216930A461256CE2CC70* ___s_UsedGlyphRects_5;
	// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord[] UnityEngine.TextCore.LowLevel.FontEngine::s_PairAdjustmentRecords_MarshallingArray
	GlyphPairAdjustmentRecordU5BU5D_tD5DD2A739A4CA745E7F28ECCB2CD0BD0A65A38F7* ___s_PairAdjustmentRecords_MarshallingArray_6;
	// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Glyph> UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphLookupDictionary
	Dictionary_2_tC61348D10610A6B3D7B65102D82AC3467D59EAA7* ___s_GlyphLookupDictionary_7;
};

// UnityEngine.TextCore.LowLevel.FontEngine

// UnityEngine.TextCore.Text.FontFeatureTable

// UnityEngine.TextCore.Text.FontFeatureTable

// UnityEngine.GUIClip

// UnityEngine.GUIClip

// UnityEngine.GUIContent
struct GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2_StaticFields
{
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_Text
	GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2* ___s_Text_3;
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_Image
	GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2* ___s_Image_4;
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_TextImage
	GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2* ___s_TextImage_5;
	// UnityEngine.GUIContent UnityEngine.GUIContent::none
	GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2* ___none_6;
};

// UnityEngine.GUIContent

// UnityEngine.GUILayout

// UnityEngine.GUILayout

// UnityEngine.GUILayoutOption

// UnityEngine.GUILayoutOption

// UnityEngine.GUIUtility
struct GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A_StaticFields
{
	// System.Int32 UnityEngine.GUIUtility::s_ControlCount
	int32_t ___s_ControlCount_0;
	// System.Int32 UnityEngine.GUIUtility::s_SkinMode
	int32_t ___s_SkinMode_1;
	// System.Int32 UnityEngine.GUIUtility::s_OriginalID
	int32_t ___s_OriginalID_2;
	// System.Action UnityEngine.GUIUtility::takeCapture
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___takeCapture_3;
	// System.Action UnityEngine.GUIUtility::releaseCapture
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___releaseCapture_4;
	// System.Func`3<System.Int32,System.IntPtr,System.Boolean> UnityEngine.GUIUtility::processEvent
	Func_3_t2376B3D8C7A437FC32F21C4C4E4B3E7D2302007C* ___processEvent_5;
	// System.Action UnityEngine.GUIUtility::cleanupRoots
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___cleanupRoots_6;
	// System.Func`2<System.Exception,System.Boolean> UnityEngine.GUIUtility::endContainerGUIFromException
	Func_2_tDDBE08B46BEFDD869DE0B97D023CB9C89674FED6* ___endContainerGUIFromException_7;
	// System.Action UnityEngine.GUIUtility::guiChanged
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___guiChanged_8;
	// System.Boolean UnityEngine.GUIUtility::<guiIsExiting>k__BackingField
	bool ___U3CguiIsExitingU3Ek__BackingField_9;
	// System.Func`1<System.Boolean> UnityEngine.GUIUtility::s_HasCurrentWindowKeyFocusFunc
	Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* ___s_HasCurrentWindowKeyFocusFunc_10;
};

// UnityEngine.GUIUtility

// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
struct GameCenterPlatform_tF95E5DA7B46424313E1812B6B80DE102ABB1F96F_StaticFields
{
	// System.Action`2<System.Boolean,System.String> UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::s_AuthenticateCallback
	Action_2_t8EADE87044ADE97906736D729EA2E3EF97F34F3D* ___s_AuthenticateCallback_0;
	// UnityEngine.SocialPlatforms.Impl.AchievementDescription[] UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::s_adCache
	AchievementDescriptionU5BU5D_t6B3ED222FB06DD89115602C955A2CD98E000CCC5* ___s_adCache_1;
	// UnityEngine.SocialPlatforms.Impl.UserProfile[] UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::s_friends
	UserProfileU5BU5D_tCE4194A0D6665FFF7943DDC3B0B9301D57F84A6A* ___s_friends_2;
	// UnityEngine.SocialPlatforms.Impl.UserProfile[] UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::s_users
	UserProfileU5BU5D_tCE4194A0D6665FFF7943DDC3B0B9301D57F84A6A* ___s_users_3;
	// System.Action`1<System.Boolean> UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::s_ResetAchievements
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* ___s_ResetAchievements_4;
	// UnityEngine.SocialPlatforms.Impl.LocalUser UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::m_LocalUser
	LocalUser_t55C68E98993F86B6FBB7A25F28EB989CD7E6A3AD* ___m_LocalUser_5;
	// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard> UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::m_GcBoards
	List_1_tC0F62C6753434D94B7A8CFEB0E642E533349DD30* ___m_GcBoards_6;
};

// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform

// UnityEngine.XR.HashCodeHelper

// UnityEngine.XR.HashCodeHelper

// UnityEngine.Timeline.HashUtility

// UnityEngine.Timeline.HashUtility

// UnityEngine.Input

// UnityEngine.Input

// UnityEngine.XR.InputDevices
struct InputDevices_t02B79FC19CEA9AC29A9945F5CDA6D790730FBF34_StaticFields
{
	// System.Action`1<UnityEngine.XR.InputDevice> UnityEngine.XR.InputDevices::deviceConnected
	Action_1_tFAB0B519749BBE2B7AAD946105FAE8116636A8BC* ___deviceConnected_0;
	// System.Action`1<UnityEngine.XR.InputDevice> UnityEngine.XR.InputDevices::deviceDisconnected
	Action_1_tFAB0B519749BBE2B7AAD946105FAE8116636A8BC* ___deviceDisconnected_1;
	// System.Action`1<UnityEngine.XR.InputDevice> UnityEngine.XR.InputDevices::deviceConfigChanged
	Action_1_tFAB0B519749BBE2B7AAD946105FAE8116636A8BC* ___deviceConfigChanged_2;
};

// UnityEngine.XR.InputDevices

// UnityEngine.XR.InputTracking
struct InputTracking_tA4F34D4D5EC8E560B56ED295177C040D9C9815F1_StaticFields
{
	// System.Action`1<UnityEngine.XR.XRNodeState> UnityEngine.XR.InputTracking::trackingAcquired
	Action_1_t290119641EBA3C1EAC8AF78274C63CE01C3046D8* ___trackingAcquired_0;
	// System.Action`1<UnityEngine.XR.XRNodeState> UnityEngine.XR.InputTracking::trackingLost
	Action_1_t290119641EBA3C1EAC8AF78274C63CE01C3046D8* ___trackingLost_1;
	// System.Action`1<UnityEngine.XR.XRNodeState> UnityEngine.XR.InputTracking::nodeAdded
	Action_1_t290119641EBA3C1EAC8AF78274C63CE01C3046D8* ___nodeAdded_2;
	// System.Action`1<UnityEngine.XR.XRNodeState> UnityEngine.XR.InputTracking::nodeRemoved
	Action_1_t290119641EBA3C1EAC8AF78274C63CE01C3046D8* ___nodeRemoved_3;
};

// UnityEngine.XR.InputTracking

// UnityEngine.Internal_SubsystemDescriptors

// UnityEngine.Internal_SubsystemDescriptors

// UnityEngine.JsonUtility

// UnityEngine.JsonUtility

// UnityEngine.Timeline.MatchTargetFieldConstants
struct MatchTargetFieldConstants_t07C2FAD424736E6B40B0764A78AC93A64983D89B_StaticFields
{
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::All
	int32_t ___All_0;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::None
	int32_t ___None_1;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::Position
	int32_t ___Position_2;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::Rotation
	int32_t ___Rotation_3;
};

// UnityEngine.Timeline.MatchTargetFieldConstants

// UnityEngine.TextCore.Text.MaterialManager
struct MaterialManager_t104D2897F78BE83C3377323E18BEB5B8F0704D9B_StaticFields
{
	// System.Collections.Generic.Dictionary`2<System.Int64,UnityEngine.Material> UnityEngine.TextCore.Text.MaterialManager::s_FallbackMaterials
	Dictionary_2_tDBB219D9459E13F073641D0B84B8AB9AF3839287* ___s_FallbackMaterials_0;
};

// UnityEngine.TextCore.Text.MaterialManager

// UnityEngine.TextCore.Text.MaterialReferenceManager
struct MaterialReferenceManager_t04E59E4B24B4F971D73124195671473AEFA6DFA9_StaticFields
{
	// UnityEngine.TextCore.Text.MaterialReferenceManager UnityEngine.TextCore.Text.MaterialReferenceManager::s_Instance
	MaterialReferenceManager_t04E59E4B24B4F971D73124195671473AEFA6DFA9* ___s_Instance_0;
};

// UnityEngine.TextCore.Text.MaterialReferenceManager

// UnityEngine.Yoga.MeasureOutput

// UnityEngine.Yoga.MeasureOutput

// UnityEngine.Yoga.Native

// UnityEngine.Yoga.Native

// UnityEngineInternal.Input.NativeInputSystem
struct NativeInputSystem_tCFE5554EBC0D3EE1DAD80FC55CE0DE38A3DDC5EE_StaticFields
{
	// UnityEngineInternal.Input.NativeUpdateCallback UnityEngineInternal.Input.NativeInputSystem::onUpdate
	NativeUpdateCallback_tC5CA5A9117B79251968A4DA3758552EFE1D37495* ___onUpdate_0;
	// System.Action`1<UnityEngineInternal.Input.NativeInputUpdateType> UnityEngineInternal.Input.NativeInputSystem::onBeforeUpdate
	Action_1_t7797D4D8783204B10C3D28B96B049C48276C3B1B* ___onBeforeUpdate_1;
	// System.Func`2<UnityEngineInternal.Input.NativeInputUpdateType,System.Boolean> UnityEngineInternal.Input.NativeInputSystem::onShouldRunUpdate
	Func_2_t880CA675AE5D39E081BEEF14DC092D82674DE4F2* ___onShouldRunUpdate_2;
	// System.Action`2<System.Int32,System.String> UnityEngineInternal.Input.NativeInputSystem::s_OnDeviceDiscoveredCallback
	Action_2_t6AAF2E215E74E16A4EEF0A0749A4A325D99F5BA6* ___s_OnDeviceDiscoveredCallback_3;
};

// UnityEngineInternal.Input.NativeInputSystem

// UnityEngine.Timeline.NotificationUtilities

// UnityEngine.Timeline.NotificationUtilities

// UnityEngine.Physics
struct Physics_t1244C2983AEAFA149425AFFC3DF53BC91C18ED56_StaticFields
{
	// System.Action`2<UnityEngine.PhysicsScene,Unity.Collections.NativeArray`1<UnityEngine.ModifiableContactPair>> UnityEngine.Physics::ContactModifyEvent
	Action_2_t70E17A6F8F03189031C560482454FE2D87F496F2* ___ContactModifyEvent_0;
	// System.Action`2<UnityEngine.PhysicsScene,Unity.Collections.NativeArray`1<UnityEngine.ModifiableContactPair>> UnityEngine.Physics::ContactModifyEventCCD
	Action_2_t70E17A6F8F03189031C560482454FE2D87F496F2* ___ContactModifyEventCCD_1;
};

// UnityEngine.Physics

// UnityEngine.Physics2D
struct Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D_StaticFields
{
	// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D> UnityEngine.Physics2D::m_LastDisabledRigidbody2D
	List_1_tCD5F926D25FC8BFAF39E4BE6F879C1FA11501C76* ___m_LastDisabledRigidbody2D_0;
};

// UnityEngine.Physics2D

// DG.Tweening.Plugins.Core.PluginsManager
struct PluginsManager_tB3018BC34957F354DA1466C4B00F76EF354BBFA7_StaticFields
{
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_floatPlugin
	RuntimeObject* ____floatPlugin_0;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_doublePlugin
	RuntimeObject* ____doublePlugin_1;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_intPlugin
	RuntimeObject* ____intPlugin_2;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_uintPlugin
	RuntimeObject* ____uintPlugin_3;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_longPlugin
	RuntimeObject* ____longPlugin_4;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_ulongPlugin
	RuntimeObject* ____ulongPlugin_5;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector2Plugin
	RuntimeObject* ____vector2Plugin_6;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector3Plugin
	RuntimeObject* ____vector3Plugin_7;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector4Plugin
	RuntimeObject* ____vector4Plugin_8;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_quaternionPlugin
	RuntimeObject* ____quaternionPlugin_9;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_colorPlugin
	RuntimeObject* ____colorPlugin_10;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_rectPlugin
	RuntimeObject* ____rectPlugin_11;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_rectOffsetPlugin
	RuntimeObject* ____rectOffsetPlugin_12;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_stringPlugin
	RuntimeObject* ____stringPlugin_13;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector3ArrayPlugin
	RuntimeObject* ____vector3ArrayPlugin_14;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_color2Plugin
	RuntimeObject* ____color2Plugin_15;
	// System.Collections.Generic.Dictionary`2<System.Type,DG.Tweening.Plugins.Core.ITweenPlugin> DG.Tweening.Plugins.Core.PluginsManager::_customPlugins
	Dictionary_2_tF4CB0FB5DF80F436652EC8BDB03A82C708969A88* ____customPlugins_17;
};

// DG.Tweening.Plugins.Core.PluginsManager

// UnityEngine.RectTransformUtility
struct RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244_StaticFields
{
	// UnityEngine.Vector3[] UnityEngine.RectTransformUtility::s_Corners
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___s_Corners_0;
};

// UnityEngine.RectTransformUtility

// UnityEngine.RemoteConfigSettingsHelper

// UnityEngine.RemoteConfigSettingsHelper

// UnityEngine.RemoteSettings
struct RemoteSettings_t9DFFC747AB3E7A39DF4527F245B529A407427250_StaticFields
{
	// UnityEngine.RemoteSettings/UpdatedEventHandler UnityEngine.RemoteSettings::Updated
	UpdatedEventHandler_tB0D5A5BA322FE093894992C29DCF51E7E12579C4* ___Updated_0;
	// System.Action UnityEngine.RemoteSettings::BeforeFetchFromServer
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___BeforeFetchFromServer_1;
	// System.Action`3<System.Boolean,System.Boolean,System.Int32> UnityEngine.RemoteSettings::Completed
	Action_3_t4730167C8E7EB19F1E0034580790A915D549F6CB* ___Completed_2;
};

// UnityEngine.RemoteSettings

// UnityEngine.Timeline.RuntimeElement

// UnityEngine.Timeline.RuntimeElement

// SR

// SR

// UnityEngine.ScrollViewState

// UnityEngine.ScrollViewState

// UnityEngine.Rendering.Universal.ShaderInput

// UnityEngine.Rendering.Universal.ShaderInput

// Unity.Burst.SharedStatic

// Unity.Burst.SharedStatic

// DG.Tweening.ShortcutExtensions

// DG.Tweening.ShortcutExtensions

// UnityEngine.SliderState

// UnityEngine.SliderState

// DG.Tweening.Plugins.Core.SpecialPluginsUtils

// DG.Tweening.Plugins.Core.SpecialPluginsUtils

// DG.Tweening.Plugins.StringPluginExtensions
struct StringPluginExtensions_tA36E0339AE94CFDB282B13B80AD5E10EDB969B8F_StaticFields
{
	// System.Char[] DG.Tweening.Plugins.StringPluginExtensions::ScrambledCharsAll
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___ScrambledCharsAll_0;
	// System.Char[] DG.Tweening.Plugins.StringPluginExtensions::ScrambledCharsUppercase
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___ScrambledCharsUppercase_1;
	// System.Char[] DG.Tweening.Plugins.StringPluginExtensions::ScrambledCharsLowercase
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___ScrambledCharsLowercase_2;
	// System.Char[] DG.Tweening.Plugins.StringPluginExtensions::ScrambledCharsNumerals
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___ScrambledCharsNumerals_3;
	// System.Int32 DG.Tweening.Plugins.StringPluginExtensions::_lastRndSeed
	int32_t ____lastRndSeed_4;
};

// DG.Tweening.Plugins.StringPluginExtensions

// System.Linq.Expressions.Strings

// System.Linq.Expressions.Strings

// UnityEngine.Subsystem

// UnityEngine.Subsystem

// UnityEngine.SubsystemDescriptor

// UnityEngine.SubsystemDescriptor

// UnityEngine.SubsystemDescriptorBindings

// UnityEngine.SubsystemDescriptorBindings

// UnityEngine.SubsystemsImplementation.SubsystemDescriptorStore
struct SubsystemDescriptorStore_tEF3761B84B8C25EA4B93F94A487551820B268250_StaticFields
{
	// System.Collections.Generic.List`1<UnityEngine.IntegratedSubsystemDescriptor> UnityEngine.SubsystemsImplementation.SubsystemDescriptorStore::s_IntegratedDescriptors
	List_1_tACFC79734710927A89702FFC38900223BB85B5A6* ___s_IntegratedDescriptors_0;
	// System.Collections.Generic.List`1<UnityEngine.SubsystemsImplementation.SubsystemDescriptorWithProvider> UnityEngine.SubsystemsImplementation.SubsystemDescriptorStore::s_StandaloneDescriptors
	List_1_t2D19D6F759F401FE6C5460698E5B8249E470E044* ___s_StandaloneDescriptors_1;
	// System.Collections.Generic.List`1<UnityEngine.SubsystemDescriptor> UnityEngine.SubsystemsImplementation.SubsystemDescriptorStore::s_DeprecatedDescriptors
	List_1_t15AD773D34D3739AFB67421B6DFFACEA7638F64E* ___s_DeprecatedDescriptors_2;
};

// UnityEngine.SubsystemsImplementation.SubsystemDescriptorStore

// UnityEngine.SubsystemsImplementation.SubsystemDescriptorWithProvider

// UnityEngine.SubsystemsImplementation.SubsystemDescriptorWithProvider

// UnityEngine.SubsystemManager
struct SubsystemManager_t9A7261E4D0B53B996F04B8707D8E1C33AB65E824_StaticFields
{
	// System.Action UnityEngine.SubsystemManager::beforeReloadSubsystems
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___beforeReloadSubsystems_0;
	// System.Action UnityEngine.SubsystemManager::afterReloadSubsystems
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___afterReloadSubsystems_1;
	// System.Collections.Generic.List`1<UnityEngine.IntegratedSubsystem> UnityEngine.SubsystemManager::s_IntegratedSubsystems
	List_1_t78E7232867D713AA9907E71F6C5B19B226F0B180* ___s_IntegratedSubsystems_2;
	// System.Collections.Generic.List`1<UnityEngine.SubsystemsImplementation.SubsystemWithProvider> UnityEngine.SubsystemManager::s_StandaloneSubsystems
	List_1_tD834E8FB7FDC0D4243FBCF922D7FE4E3C707AAC3* ___s_StandaloneSubsystems_3;
	// System.Collections.Generic.List`1<UnityEngine.Subsystem> UnityEngine.SubsystemManager::s_DeprecatedSubsystems
	List_1_t9E8CCD70A25458CE30A64503B35F06ECA62E3052* ___s_DeprecatedSubsystems_4;
	// System.Action UnityEngine.SubsystemManager::reloadSubsytemsStarted
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___reloadSubsytemsStarted_5;
	// System.Action UnityEngine.SubsystemManager::reloadSubsytemsCompleted
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___reloadSubsytemsCompleted_6;
};

// UnityEngine.SubsystemManager

// UnityEngine.SubsystemsImplementation.SubsystemWithProvider

// UnityEngine.SubsystemsImplementation.SubsystemWithProvider

// UnityEngine.TextCore.Text.TextElement

// UnityEngine.TextCore.Text.TextElement

// UnityEngine.UIElements.TextNative

// UnityEngine.UIElements.TextNative

// UnityEngine.TextCore.Text.TextResourceManager
struct TextResourceManager_t10194AE8FB3DE5B9938C916F107713F6BDD3CD88_StaticFields
{
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.TextCore.Text.TextResourceManager/FontAssetRef> UnityEngine.TextCore.Text.TextResourceManager::s_FontAssetReferences
	Dictionary_2_t4B29EB34D6BDD7CF67CE77D60AC718EB61186713* ___s_FontAssetReferences_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.TextCore.Text.FontAsset> UnityEngine.TextCore.Text.TextResourceManager::s_FontAssetNameReferenceLookup
	Dictionary_2_tC20B3D6AE4370C892734F670EF4D1FB9CE91F371* ___s_FontAssetNameReferenceLookup_1;
	// System.Collections.Generic.Dictionary`2<System.Int64,UnityEngine.TextCore.Text.FontAsset> UnityEngine.TextCore.Text.TextResourceManager::s_FontAssetFamilyNameAndStyleReferenceLookup
	Dictionary_2_t6A8DB1A82203EE1CADD418706BFDC84FED020B64* ___s_FontAssetFamilyNameAndStyleReferenceLookup_2;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.TextCore.Text.TextResourceManager::s_FontAssetRemovalList
	List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* ___s_FontAssetRemovalList_3;
	// System.Int32 UnityEngine.TextCore.Text.TextResourceManager::k_RegularStyleHashCode
	int32_t ___k_RegularStyleHashCode_4;
};

// UnityEngine.TextCore.Text.TextResourceManager

// UnityEngine.TextCore.Text.TextShaderUtilities
struct TextShaderUtilities_t47B400695C5D96E7B04FEF9D132468B3A1799692_StaticFields
{
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_MainTex
	int32_t ___ID_MainTex_0;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_FaceTex
	int32_t ___ID_FaceTex_1;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_FaceColor
	int32_t ___ID_FaceColor_2;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_FaceDilate
	int32_t ___ID_FaceDilate_3;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_Shininess
	int32_t ___ID_Shininess_4;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_UnderlayColor
	int32_t ___ID_UnderlayColor_5;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_UnderlayOffsetX
	int32_t ___ID_UnderlayOffsetX_6;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_UnderlayOffsetY
	int32_t ___ID_UnderlayOffsetY_7;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_UnderlayDilate
	int32_t ___ID_UnderlayDilate_8;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_UnderlaySoftness
	int32_t ___ID_UnderlaySoftness_9;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_WeightNormal
	int32_t ___ID_WeightNormal_10;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_WeightBold
	int32_t ___ID_WeightBold_11;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_OutlineTex
	int32_t ___ID_OutlineTex_12;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_OutlineWidth
	int32_t ___ID_OutlineWidth_13;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_OutlineSoftness
	int32_t ___ID_OutlineSoftness_14;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_OutlineColor
	int32_t ___ID_OutlineColor_15;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_Outline2Color
	int32_t ___ID_Outline2Color_16;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_Outline2Width
	int32_t ___ID_Outline2Width_17;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_Padding
	int32_t ___ID_Padding_18;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_GradientScale
	int32_t ___ID_GradientScale_19;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_ScaleX
	int32_t ___ID_ScaleX_20;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_ScaleY
	int32_t ___ID_ScaleY_21;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_PerspectiveFilter
	int32_t ___ID_PerspectiveFilter_22;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_Sharpness
	int32_t ___ID_Sharpness_23;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_TextureWidth
	int32_t ___ID_TextureWidth_24;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_TextureHeight
	int32_t ___ID_TextureHeight_25;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_BevelAmount
	int32_t ___ID_BevelAmount_26;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_GlowColor
	int32_t ___ID_GlowColor_27;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_GlowOffset
	int32_t ___ID_GlowOffset_28;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_GlowPower
	int32_t ___ID_GlowPower_29;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_GlowOuter
	int32_t ___ID_GlowOuter_30;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_GlowInner
	int32_t ___ID_GlowInner_31;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_LightAngle
	int32_t ___ID_LightAngle_32;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_EnvMap
	int32_t ___ID_EnvMap_33;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_EnvMatrix
	int32_t ___ID_EnvMatrix_34;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_EnvMatrixRotation
	int32_t ___ID_EnvMatrixRotation_35;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_MaskCoord
	int32_t ___ID_MaskCoord_36;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_ClipRect
	int32_t ___ID_ClipRect_37;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_MaskSoftnessX
	int32_t ___ID_MaskSoftnessX_38;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_MaskSoftnessY
	int32_t ___ID_MaskSoftnessY_39;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_VertexOffsetX
	int32_t ___ID_VertexOffsetX_40;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_VertexOffsetY
	int32_t ___ID_VertexOffsetY_41;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_UseClipRect
	int32_t ___ID_UseClipRect_42;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_StencilID
	int32_t ___ID_StencilID_43;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_StencilOp
	int32_t ___ID_StencilOp_44;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_StencilComp
	int32_t ___ID_StencilComp_45;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_StencilReadMask
	int32_t ___ID_StencilReadMask_46;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_StencilWriteMask
	int32_t ___ID_StencilWriteMask_47;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_ShaderFlags
	int32_t ___ID_ShaderFlags_48;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_ScaleRatio_A
	int32_t ___ID_ScaleRatio_A_49;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_ScaleRatio_B
	int32_t ___ID_ScaleRatio_B_50;
	// System.Int32 UnityEngine.TextCore.Text.TextShaderUtilities::ID_ScaleRatio_C
	int32_t ___ID_ScaleRatio_C_51;
	// System.String UnityEngine.TextCore.Text.TextShaderUtilities::Keyword_Bevel
	String_t* ___Keyword_Bevel_52;
	// System.String UnityEngine.TextCore.Text.TextShaderUtilities::Keyword_Glow
	String_t* ___Keyword_Glow_53;
	// System.String UnityEngine.TextCore.Text.TextShaderUtilities::Keyword_Underlay
	String_t* ___Keyword_Underlay_54;
	// System.String UnityEngine.TextCore.Text.TextShaderUtilities::Keyword_Ratios
	String_t* ___Keyword_Ratios_55;
	// System.String UnityEngine.TextCore.Text.TextShaderUtilities::Keyword_MASK_SOFT
	String_t* ___Keyword_MASK_SOFT_56;
	// System.String UnityEngine.TextCore.Text.TextShaderUtilities::Keyword_MASK_HARD
	String_t* ___Keyword_MASK_HARD_57;
	// System.String UnityEngine.TextCore.Text.TextShaderUtilities::Keyword_MASK_TEX
	String_t* ___Keyword_MASK_TEX_58;
	// System.String UnityEngine.TextCore.Text.TextShaderUtilities::Keyword_Outline
	String_t* ___Keyword_Outline_59;
	// System.String UnityEngine.TextCore.Text.TextShaderUtilities::ShaderTag_ZTestMode
	String_t* ___ShaderTag_ZTestMode_60;
	// System.String UnityEngine.TextCore.Text.TextShaderUtilities::ShaderTag_CullMode
	String_t* ___ShaderTag_CullMode_61;
	// System.Single UnityEngine.TextCore.Text.TextShaderUtilities::m_clamp
	float ___m_clamp_62;
	// System.Boolean UnityEngine.TextCore.Text.TextShaderUtilities::isInitialized
	bool ___isInitialized_63;
	// UnityEngine.Shader UnityEngine.TextCore.Text.TextShaderUtilities::k_ShaderRef_MobileSDF
	Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* ___k_ShaderRef_MobileSDF_64;
	// UnityEngine.Shader UnityEngine.TextCore.Text.TextShaderUtilities::k_ShaderRef_MobileBitmap
	Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* ___k_ShaderRef_MobileBitmap_65;
};

// UnityEngine.TextCore.Text.TextShaderUtilities

// UnityEngine.TextCore.Text.TextStyle
struct TextStyle_tD9287057EB15E73ED76AC925AC21A889D64CDAAE_StaticFields
{
	// UnityEngine.TextCore.Text.TextStyle UnityEngine.TextCore.Text.TextStyle::k_NormalStyle
	TextStyle_tD9287057EB15E73ED76AC925AC21A889D64CDAAE* ___k_NormalStyle_0;
};

// UnityEngine.TextCore.Text.TextStyle

// UnityEngine.TextCore.Text.TextUtilities

// UnityEngine.TextCore.Text.TextUtilities

// UnityEngine.Timeline.TimeUtility
struct TimeUtility_t815D4EA395D12E4F662C6AB493F3CE5A0006AD26_StaticFields
{
	// System.Double UnityEngine.Timeline.TimeUtility::kTimeEpsilon
	double ___kTimeEpsilon_0;
	// System.Double UnityEngine.Timeline.TimeUtility::kFrameRateEpsilon
	double ___kFrameRateEpsilon_1;
	// System.Double UnityEngine.Timeline.TimeUtility::k_MaxTimelineDurationInSeconds
	double ___k_MaxTimelineDurationInSeconds_2;
	// System.Double UnityEngine.Timeline.TimeUtility::kFrameRateRounding
	double ___kFrameRateRounding_3;
};

// UnityEngine.Timeline.TimeUtility

// UnityEngine.Timeline.TimelineClip
struct TimelineClip_t003008F08E56A75F3A47FD9ADE7C066988A3371D_StaticFields
{
	// UnityEngine.Timeline.ClipCaps UnityEngine.Timeline.TimelineClip::kDefaultClipCaps
	int32_t ___kDefaultClipCaps_2;
	// System.Single UnityEngine.Timeline.TimelineClip::kDefaultClipDurationInSeconds
	float ___kDefaultClipDurationInSeconds_3;
	// System.Double UnityEngine.Timeline.TimelineClip::kTimeScaleMin
	double ___kTimeScaleMin_4;
	// System.Double UnityEngine.Timeline.TimelineClip::kTimeScaleMax
	double ___kTimeScaleMax_5;
	// System.String UnityEngine.Timeline.TimelineClip::kDefaultCurvesName
	String_t* ___kDefaultCurvesName_6;
	// System.Double UnityEngine.Timeline.TimelineClip::kMinDuration
	double ___kMinDuration_7;
	// System.Double UnityEngine.Timeline.TimelineClip::kMaxTimeValue
	double ___kMaxTimeValue_8;
};

// UnityEngine.Timeline.TimelineClip

// UnityEngine.Timeline.TimelineClipCapsExtensions

// UnityEngine.Timeline.TimelineClipCapsExtensions

// UnityEngine.Timeline.TimelineCreateUtilities

// UnityEngine.Timeline.TimelineCreateUtilities

// UnityEngine.Timeline.TimelineUndo

// UnityEngine.Timeline.TimelineUndo

// DG.Tweening.Core.TweenLink

// DG.Tweening.Core.TweenLink

// DG.Tweening.Core.TweenManager
struct TweenManager_t3DFB74C661BA08421AF4EB9F62D6DB3B8F7351F1_StaticFields
{
	// System.Boolean DG.Tweening.Core.TweenManager::isUnityEditor
	bool ___isUnityEditor_4;
	// System.Boolean DG.Tweening.Core.TweenManager::isDebugBuild
	bool ___isDebugBuild_5;
	// System.Int32 DG.Tweening.Core.TweenManager::maxActive
	int32_t ___maxActive_6;
	// System.Int32 DG.Tweening.Core.TweenManager::maxTweeners
	int32_t ___maxTweeners_7;
	// System.Int32 DG.Tweening.Core.TweenManager::maxSequences
	int32_t ___maxSequences_8;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveTweens
	bool ___hasActiveTweens_9;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveDefaultTweens
	bool ___hasActiveDefaultTweens_10;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveLateTweens
	bool ___hasActiveLateTweens_11;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveFixedTweens
	bool ___hasActiveFixedTweens_12;
	// System.Boolean DG.Tweening.Core.TweenManager::hasActiveManualTweens
	bool ___hasActiveManualTweens_13;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveTweens
	int32_t ___totActiveTweens_14;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveDefaultTweens
	int32_t ___totActiveDefaultTweens_15;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveLateTweens
	int32_t ___totActiveLateTweens_16;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveFixedTweens
	int32_t ___totActiveFixedTweens_17;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveManualTweens
	int32_t ___totActiveManualTweens_18;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveTweeners
	int32_t ___totActiveTweeners_19;
	// System.Int32 DG.Tweening.Core.TweenManager::totActiveSequences
	int32_t ___totActiveSequences_20;
	// System.Int32 DG.Tweening.Core.TweenManager::totPooledTweeners
	int32_t ___totPooledTweeners_21;
	// System.Int32 DG.Tweening.Core.TweenManager::totPooledSequences
	int32_t ___totPooledSequences_22;
	// System.Int32 DG.Tweening.Core.TweenManager::totTweeners
	int32_t ___totTweeners_23;
	// System.Int32 DG.Tweening.Core.TweenManager::totSequences
	int32_t ___totSequences_24;
	// System.Boolean DG.Tweening.Core.TweenManager::isUpdateLoop
	bool ___isUpdateLoop_25;
	// DG.Tweening.Tween[] DG.Tweening.Core.TweenManager::_activeTweens
	TweenU5BU5D_t6E5F58C51FA0DA64C990C1C7E02EE9B70220C2F7* ____activeTweens_26;
	// DG.Tweening.Tween[] DG.Tweening.Core.TweenManager::_pooledTweeners
	TweenU5BU5D_t6E5F58C51FA0DA64C990C1C7E02EE9B70220C2F7* ____pooledTweeners_27;
	// System.Collections.Generic.Stack`1<DG.Tweening.Tween> DG.Tweening.Core.TweenManager::_PooledSequences
	Stack_1_t5BFE231094D4C21C56F26E6EE0BC9A194AD9A5B6* ____PooledSequences_28;
	// System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.Core.TweenManager::_KillList
	List_1_tDA2C18E15C40590123A37DABB6D0D9AEB77A3BBD* ____KillList_29;
	// System.Collections.Generic.Dictionary`2<DG.Tweening.Tween,DG.Tweening.Core.TweenLink> DG.Tweening.Core.TweenManager::_TweenLinks
	Dictionary_2_tFD00E28E4EE499B3B2F468CCB593BD9ACCE80357* ____TweenLinks_30;
	// System.Int32 DG.Tweening.Core.TweenManager::_totTweenLinks
	int32_t ____totTweenLinks_31;
	// System.Int32 DG.Tweening.Core.TweenManager::_maxActiveLookupId
	int32_t ____maxActiveLookupId_32;
	// System.Boolean DG.Tweening.Core.TweenManager::_requiresActiveReorganization
	bool ____requiresActiveReorganization_33;
	// System.Int32 DG.Tweening.Core.TweenManager::_reorganizeFromId
	int32_t ____reorganizeFromId_34;
	// System.Int32 DG.Tweening.Core.TweenManager::_minPooledTweenerId
	int32_t ____minPooledTweenerId_35;
	// System.Int32 DG.Tweening.Core.TweenManager::_maxPooledTweenerId
	int32_t ____maxPooledTweenerId_36;
	// System.Boolean DG.Tweening.Core.TweenManager::_despawnAllCalledFromUpdateLoopCallback
	bool ____despawnAllCalledFromUpdateLoopCallback_37;
};

// DG.Tweening.Core.TweenManager

// DG.Tweening.TweenParams
struct TweenParams_t0BF9382130164497B9A5A19076149D1CEDC697DA_StaticFields
{
	// DG.Tweening.TweenParams DG.Tweening.TweenParams::Params
	TweenParams_t0BF9382130164497B9A5A19076149D1CEDC697DA* ___Params_0;
};

// DG.Tweening.TweenParams

// DG.Tweening.TweenSettingsExtensions

// DG.Tweening.TweenSettingsExtensions

// UnityEngine.UIElements.UIElementsRuntimeUtilityNative
struct UIElementsRuntimeUtilityNative_t9DE2C23158D553BB693212D0D8AEAE8594E75938_StaticFields
{
	// System.Action UnityEngine.UIElements.UIElementsRuntimeUtilityNative::RepaintOverlayPanelsCallback
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___RepaintOverlayPanelsCallback_0;
	// System.Action UnityEngine.UIElements.UIElementsRuntimeUtilityNative::UpdateRuntimePanelsCallback
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___UpdateRuntimePanelsCallback_1;
};

// UnityEngine.UIElements.UIElementsRuntimeUtilityNative

// UnityEngine.UISystemProfilerApi

// UnityEngine.UISystemProfilerApi

// UnityEngine.TextCore.Text.UnicodeLineBreakingRules
struct UnicodeLineBreakingRules_t80BE36F5E16AE48FE7B6DE1C91D36B1142B4EC0E_StaticFields
{
	// UnityEngine.TextCore.Text.UnicodeLineBreakingRules UnityEngine.TextCore.Text.UnicodeLineBreakingRules::s_Instance
	UnicodeLineBreakingRules_t80BE36F5E16AE48FE7B6DE1C91D36B1142B4EC0E* ___s_Instance_0;
	// System.Collections.Generic.HashSet`1<System.UInt32> UnityEngine.TextCore.Text.UnicodeLineBreakingRules::s_LeadingCharactersLookup
	HashSet_1_t5DD20B42149A11AEBF12A75505306E6EFC34943A* ___s_LeadingCharactersLookup_5;
	// System.Collections.Generic.HashSet`1<System.UInt32> UnityEngine.TextCore.Text.UnicodeLineBreakingRules::s_FollowingCharactersLookup
	HashSet_1_t5DD20B42149A11AEBF12A75505306E6EFC34943A* ___s_FollowingCharactersLookup_6;
};

// UnityEngine.TextCore.Text.UnicodeLineBreakingRules

// UnityEngine.UnityString

// UnityEngine.UnityString

// UnityEngine.SocialPlatforms.Impl.UserProfile

// UnityEngine.SocialPlatforms.Impl.UserProfile

// UnityEngine.WWWForm
struct WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_StaticFields
{
	// System.Byte[] UnityEngine.WWWForm::dDash
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___dDash_0;
	// System.Byte[] UnityEngine.WWWForm::crlf
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___crlf_1;
	// System.Byte[] UnityEngine.WWWForm::contentTypeHeader
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___contentTypeHeader_2;
	// System.Byte[] UnityEngine.WWWForm::dispositionHeader
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___dispositionHeader_3;
	// System.Byte[] UnityEngine.WWWForm::endQuote
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___endQuote_4;
	// System.Byte[] UnityEngine.WWWForm::fileNameField
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___fileNameField_5;
	// System.Byte[] UnityEngine.WWWForm::ampersand
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___ampersand_6;
	// System.Byte[] UnityEngine.WWWForm::equal
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___equal_7;
};

// UnityEngine.WWWForm

// UnityEngine.WWWTranscoder
struct WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B_StaticFields
{
	// System.Byte[] UnityEngine.WWWTranscoder::ucHexChars
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___ucHexChars_0;
	// System.Byte[] UnityEngine.WWWTranscoder::lcHexChars
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___lcHexChars_1;
	// System.Byte UnityEngine.WWWTranscoder::urlEscapeChar
	uint8_t ___urlEscapeChar_2;
	// System.Byte[] UnityEngine.WWWTranscoder::urlSpace
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___urlSpace_3;
	// System.Byte[] UnityEngine.WWWTranscoder::dataSpace
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___dataSpace_4;
	// System.Byte[] UnityEngine.WWWTranscoder::urlForbidden
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___urlForbidden_5;
	// System.Byte UnityEngine.WWWTranscoder::qpEscapeChar
	uint8_t ___qpEscapeChar_6;
	// System.Byte[] UnityEngine.WWWTranscoder::qpSpace
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___qpSpace_7;
	// System.Byte[] UnityEngine.WWWTranscoder::qpForbidden
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___qpForbidden_8;
};

// UnityEngine.WWWTranscoder

// UnityEngineInternal.WebRequestUtils
struct WebRequestUtils_t23F1FB533DBFDA3BE5624D901D535B4C6EFAD443_StaticFields
{
	// System.Text.RegularExpressions.Regex UnityEngineInternal.WebRequestUtils::domainRegex
	Regex_tE773142C2BE45C5D362B0F815AFF831707A51772* ___domainRegex_0;
};

// UnityEngineInternal.WebRequestUtils

// UnityEngine.Timeline.WeightUtility

// UnityEngine.Timeline.WeightUtility

// UnityEngine.XR.XRDevice
struct XRDevice_tD076A68EFE413B3EEEEA362BE0364A488B58F194_StaticFields
{
	// System.Action`1<System.String> UnityEngine.XR.XRDevice::deviceLoaded
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___deviceLoaded_0;
};

// UnityEngine.XR.XRDevice

// UnityEngine.XR.XRSettings

// UnityEngine.XR.XRSettings

// System.Xml.XmlNode

// System.Xml.XmlNode

// System.Xml.XmlReader
struct XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD_StaticFields
{
	// System.UInt32 System.Xml.XmlReader::IsTextualNodeBitmap
	uint32_t ___IsTextualNodeBitmap_0;
	// System.UInt32 System.Xml.XmlReader::CanReadContentAsBitmap
	uint32_t ___CanReadContentAsBitmap_1;
	// System.UInt32 System.Xml.XmlReader::HasValueBitmap
	uint32_t ___HasValueBitmap_2;
};

// System.Xml.XmlReader

// UnityEngine.Yoga.YogaConstants

// UnityEngine.Yoga.YogaConstants

// System.__Il2CppComDelegate

// System.__Il2CppComDelegate

// Unity.Mathematics.math

// Unity.Mathematics.math

// UnityEngine.Animation/Enumerator

// UnityEngine.Animation/Enumerator

// UnityEngine.Timeline.AnimationPlayableAsset/AnimationPlayableAssetUpgrade

// UnityEngine.Timeline.AnimationPlayableAsset/AnimationPlayableAssetUpgrade

// UnityEngine.Timeline.AnimationTrack/AnimationTrackUpgrade

// UnityEngine.Timeline.AnimationTrack/AnimationTrackUpgrade

// UnityEngine.AudioSettings/Mobile
struct Mobile_t304A73480DF447472BDB16BA19A9E4FE2C8CB2DD_StaticFields
{
	// System.Boolean UnityEngine.AudioSettings/Mobile::<muteState>k__BackingField
	bool ___U3CmuteStateU3Ek__BackingField_0;
	// System.Boolean UnityEngine.AudioSettings/Mobile::_stopAudioOutputOnMute
	bool ____stopAudioOutputOnMute_1;
	// System.Action`1<System.Boolean> UnityEngine.AudioSettings/Mobile::OnMuteStateChanged
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* ___OnMuteStateChanged_2;
};

// UnityEngine.AudioSettings/Mobile

// Unity.Burst.BurstCompiler/FakeDelegate

// Unity.Burst.BurstCompiler/FakeDelegate

// CartoonFX.CFXR_Effect/AnimatedLight
struct AnimatedLight_t452FBE1EDE1B867DB67C7383966C9992DC6A9C59_StaticFields
{
	// System.Boolean CartoonFX.CFXR_Effect/AnimatedLight::editorPreview
	bool ___editorPreview_0;
};

// CartoonFX.CFXR_Effect/AnimatedLight

// CartoonFX.CFXR_ParticleTextFontAsset/Kerning

// CartoonFX.CFXR_ParticleTextFontAsset/Kerning

// DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__17

// DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__17

// DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__20

// DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__20

// DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__19

// DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__19

// DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__21

// DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__21

// DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__18

// DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__18

// DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__22

// DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__22

// DG.Tweening.DOTweenModuleAudio/cy

// DG.Tweening.DOTweenModuleAudio/cy

// DG.Tweening.DOTweenModuleAudio/cz

// DG.Tweening.DOTweenModuleAudio/cz

// DG.Tweening.DOTweenModuleAudio/da

// DG.Tweening.DOTweenModuleAudio/da

// DG.Tweening.DOTweenModulePhysics/db

// DG.Tweening.DOTweenModulePhysics/db

// DG.Tweening.DOTweenModulePhysics/dc

// DG.Tweening.DOTweenModulePhysics/dc

// DG.Tweening.DOTweenModulePhysics/dd

// DG.Tweening.DOTweenModulePhysics/dd

// DG.Tweening.DOTweenModulePhysics/de

// DG.Tweening.DOTweenModulePhysics/de

// DG.Tweening.DOTweenModulePhysics/df

// DG.Tweening.DOTweenModulePhysics/df

// DG.Tweening.DOTweenModulePhysics/dg

// DG.Tweening.DOTweenModulePhysics/dg

// DG.Tweening.DOTweenModulePhysics/di

// DG.Tweening.DOTweenModulePhysics/di

// DG.Tweening.DOTweenModulePhysics/dj

// DG.Tweening.DOTweenModulePhysics/dj

// DG.Tweening.DOTweenModulePhysics/dk

// DG.Tweening.DOTweenModulePhysics/dk

// DG.Tweening.DOTweenModulePhysics/dl

// DG.Tweening.DOTweenModulePhysics/dl

// DG.Tweening.DOTweenModulePhysics2D/dm

// DG.Tweening.DOTweenModulePhysics2D/dm

// DG.Tweening.DOTweenModulePhysics2D/dn

// DG.Tweening.DOTweenModulePhysics2D/dn

// DG.Tweening.DOTweenModulePhysics2D/do

// DG.Tweening.DOTweenModulePhysics2D/do

// DG.Tweening.DOTweenModulePhysics2D/dp

// DG.Tweening.DOTweenModulePhysics2D/dp

// DG.Tweening.DOTweenModulePhysics2D/dr

// DG.Tweening.DOTweenModulePhysics2D/dr

// DG.Tweening.DOTweenModulePhysics2D/ds

// DG.Tweening.DOTweenModulePhysics2D/ds

// DG.Tweening.DOTweenModulePhysics2D/dt

// DG.Tweening.DOTweenModulePhysics2D/dt

// DG.Tweening.DOTweenModulePhysics2D/du

// DG.Tweening.DOTweenModulePhysics2D/du

// DG.Tweening.DOTweenModuleSprite/dv

// DG.Tweening.DOTweenModuleSprite/dv

// DG.Tweening.DOTweenModuleSprite/dw

// DG.Tweening.DOTweenModuleSprite/dw

// DG.Tweening.DOTweenModuleUI/Utils

// DG.Tweening.DOTweenModuleUI/Utils

// DG.Tweening.DOTweenModuleUI/dy

// DG.Tweening.DOTweenModuleUI/dy

// DG.Tweening.DOTweenModuleUI/dz

// DG.Tweening.DOTweenModuleUI/dz

// DG.Tweening.DOTweenModuleUI/ea

// DG.Tweening.DOTweenModuleUI/ea

// DG.Tweening.DOTweenModuleUI/eb

// DG.Tweening.DOTweenModuleUI/eb

// DG.Tweening.DOTweenModuleUI/ec

// DG.Tweening.DOTweenModuleUI/ec

// DG.Tweening.DOTweenModuleUI/ed

// DG.Tweening.DOTweenModuleUI/ed

// DG.Tweening.DOTweenModuleUI/ee

// DG.Tweening.DOTweenModuleUI/ee

// DG.Tweening.DOTweenModuleUI/ef

// DG.Tweening.DOTweenModuleUI/ef

// DG.Tweening.DOTweenModuleUI/eg

// DG.Tweening.DOTweenModuleUI/eg

// DG.Tweening.DOTweenModuleUI/eh

// DG.Tweening.DOTweenModuleUI/eh

// DG.Tweening.DOTweenModuleUI/ei

// DG.Tweening.DOTweenModuleUI/ei

// DG.Tweening.DOTweenModuleUI/ej

// DG.Tweening.DOTweenModuleUI/ej

// DG.Tweening.DOTweenModuleUI/ek

// DG.Tweening.DOTweenModuleUI/ek

// DG.Tweening.DOTweenModuleUI/el

// DG.Tweening.DOTweenModuleUI/el

// DG.Tweening.DOTweenModuleUI/em

// DG.Tweening.DOTweenModuleUI/em

// DG.Tweening.DOTweenModuleUI/en

// DG.Tweening.DOTweenModuleUI/en

// DG.Tweening.DOTweenModuleUI/eo

// DG.Tweening.DOTweenModuleUI/eo

// DG.Tweening.DOTweenModuleUI/ep

// DG.Tweening.DOTweenModuleUI/ep

// DG.Tweening.DOTweenModuleUI/eq

// DG.Tweening.DOTweenModuleUI/eq

// DG.Tweening.DOTweenModuleUI/er

// DG.Tweening.DOTweenModuleUI/er

// DG.Tweening.DOTweenModuleUI/es

// DG.Tweening.DOTweenModuleUI/es

// DG.Tweening.DOTweenModuleUI/et

// DG.Tweening.DOTweenModuleUI/et

// DG.Tweening.DOTweenModuleUI/eu

// DG.Tweening.DOTweenModuleUI/eu

// DG.Tweening.DOTweenModuleUI/ev

// DG.Tweening.DOTweenModuleUI/ev

// DG.Tweening.DOTweenModuleUI/ew

// DG.Tweening.DOTweenModuleUI/ew

// DG.Tweening.DOTweenModuleUI/ex

// DG.Tweening.DOTweenModuleUI/ex

// DG.Tweening.DOTweenModuleUI/ey

// DG.Tweening.DOTweenModuleUI/ey

// DG.Tweening.DOTweenModuleUI/ez

// DG.Tweening.DOTweenModuleUI/ez

// DG.Tweening.DOTweenModuleUI/fb

// DG.Tweening.DOTweenModuleUI/fb

// DG.Tweening.DOTweenModuleUI/fc

// DG.Tweening.DOTweenModuleUI/fc

// DG.Tweening.DOTweenModuleUI/fd

// DG.Tweening.DOTweenModuleUI/fd

// DG.Tweening.DOTweenModuleUI/fe

// DG.Tweening.DOTweenModuleUI/fe

// DG.Tweening.DOTweenModuleUI/ff

// DG.Tweening.DOTweenModuleUI/ff

// DG.Tweening.DOTweenModuleUI/fg

// DG.Tweening.DOTweenModuleUI/fg

// DG.Tweening.DOTweenModuleUI/fh

// DG.Tweening.DOTweenModuleUI/fh

// DG.Tweening.DOTweenModuleUI/fi

// DG.Tweening.DOTweenModuleUI/fi

// DG.Tweening.DOTweenModuleUI/fm

// DG.Tweening.DOTweenModuleUI/fm

// DG.Tweening.DOTweenModuleUnityVersion/fn

// DG.Tweening.DOTweenModuleUnityVersion/fn

// DG.Tweening.DOTweenModuleUnityVersion/fo

// DG.Tweening.DOTweenModuleUnityVersion/fo

// DG.Tweening.DOTweenModuleUtils/Physics

// DG.Tweening.DOTweenModuleUtils/Physics

// DG.Tweening.Core.DOTweenSettings/ModulesSetup

// DG.Tweening.Core.DOTweenSettings/ModulesSetup

// DG.Tweening.Core.DOTweenSettings/SafeModeOptions

// DG.Tweening.Core.DOTweenSettings/SafeModeOptions

// DG.Tweening.Core.Debugger/Sequence

// DG.Tweening.Core.Debugger/Sequence

// DG.Tweening.Core.Easing.EaseManager/<>c
struct U3CU3Ec_t124AFA44553F835AFE37BF9E66B435E4CA7DB8B4_StaticFields
{
	// DG.Tweening.Core.Easing.EaseManager/<>c DG.Tweening.Core.Easing.EaseManager/<>c::<>9
	U3CU3Ec_t124AFA44553F835AFE37BF9E66B435E4CA7DB8B4* ___U3CU3E9_0;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_0
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_0_1;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_1
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_1_2;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_2
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_2_3;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_3
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_3_4;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_4
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_4_5;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_5
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_5_6;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_6
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_6_7;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_7
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_7_8;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_8
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_8_9;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_9
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_9_10;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_10
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_10_11;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_11
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_11_12;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_12
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_12_13;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_13
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_13_14;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_14
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_14_15;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_15
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_15_16;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_16
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_16_17;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_17
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_17_18;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_18
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_18_19;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_19
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_19_20;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_20
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_20_21;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_21
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_21_22;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_22
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_22_23;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_23
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_23_24;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_24
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_24_25;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_25
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_25_26;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_26
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_26_27;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_27
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_27_28;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_28
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_28_29;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_29
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_29_30;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_30
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_30_31;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_31
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_31_32;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_32
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_32_33;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_33
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_33_34;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_34
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_34_35;
	// DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager/<>c::<>9__4_35
	EaseFunction_t0F945D9D726B0915C5FBF30862E987EC3AC12A04* ___U3CU3E9__4_35_36;
};

// DG.Tweening.Core.Easing.EaseManager/<>c

// System.Linq.Expressions.Expression/ExtensionInfo

// System.Linq.Expressions.Expression/ExtensionInfo

// System.Linq.Expressions.Expression/LambdaExpressionProxy

// System.Linq.Expressions.Expression/LambdaExpressionProxy

// System.Linq.Expressions.Expression/MemberExpressionProxy

// System.Linq.Expressions.Expression/MemberExpressionProxy

// System.Linq.Expressions.Expression/UnaryExpressionProxy

// System.Linq.Expressions.Expression/UnaryExpressionProxy

// UnityEngine.Timeline.Extrapolation/<>c
struct U3CU3Ec_tB74E998116FFDE2919C1EF6E8B35AB3798461A28_StaticFields
{
	// UnityEngine.Timeline.Extrapolation/<>c UnityEngine.Timeline.Extrapolation/<>c::<>9
	U3CU3Ec_tB74E998116FFDE2919C1EF6E8B35AB3798461A28* ___U3CU3E9_0;
	// System.Comparison`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.Extrapolation/<>c::<>9__2_0
	Comparison_1_t72A7B2EACD3F8F0C3D6A14C88E2B3C719E89C50E* ___U3CU3E9__2_0_1;
};

// UnityEngine.Timeline.Extrapolation/<>c

// UnityEngine.TextCore.Text.FontAsset/<>c
struct U3CU3Ec_tE3434A696CF5060D63A69C93A84379DBF90E9948_StaticFields
{
	// UnityEngine.TextCore.Text.FontAsset/<>c UnityEngine.TextCore.Text.FontAsset/<>c::<>9
	U3CU3Ec_tE3434A696CF5060D63A69C93A84379DBF90E9948* ___U3CU3E9_0;
	// System.Func`2<UnityEngine.TextCore.Text.Character,System.UInt32> UnityEngine.TextCore.Text.FontAsset/<>c::<>9__144_0
	Func_2_t1F4F442815B8C6944A17E87D7B00D2830ECD1600* ___U3CU3E9__144_0_1;
	// System.Func`2<UnityEngine.TextCore.Glyph,System.UInt32> UnityEngine.TextCore.Text.FontAsset/<>c::<>9__145_0
	Func_2_tBA43006BE5B44011173C435E32D4BC18730623FB* ___U3CU3E9__145_0_2;
};

// UnityEngine.TextCore.Text.FontAsset/<>c

// UnityEngine.TextCore.Text.FontFeatureTable/<>c
struct U3CU3Ec_tCA6A4D073378D45745D0E81D226E721969C3BE80_StaticFields
{
	// UnityEngine.TextCore.Text.FontFeatureTable/<>c UnityEngine.TextCore.Text.FontFeatureTable/<>c::<>9
	U3CU3Ec_tCA6A4D073378D45745D0E81D226E721969C3BE80* ___U3CU3E9_0;
	// System.Func`2<UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord,System.UInt32> UnityEngine.TextCore.Text.FontFeatureTable/<>c::<>9__6_0
	Func_2_tEDCDCD7BE3F7A4F5A742A5FD711EA63155BC825E* ___U3CU3E9__6_0_1;
	// System.Func`2<UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord,System.UInt32> UnityEngine.TextCore.Text.FontFeatureTable/<>c::<>9__6_1
	Func_2_tEDCDCD7BE3F7A4F5A742A5FD711EA63155BC825E* ___U3CU3E9__6_1_2;
};

// UnityEngine.TextCore.Text.FontFeatureTable/<>c

// UnityEngine.GUILayoutUtility/LayoutCache

// UnityEngine.GUILayoutUtility/LayoutCache

// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform/<>c__DisplayClass21_0

// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform/<>c__DisplayClass21_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass11_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass11_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass12_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass12_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass13_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass13_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass14_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass14_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass16_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass16_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass17_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass17_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass18_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass18_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass19_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass19_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass20_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass20_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass21_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass21_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass22_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass22_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass23_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass23_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass24_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass24_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass25_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass25_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass26_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass26_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass27_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass27_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass28_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass28_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass29_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass29_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass30_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass30_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass31_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass31_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass32_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass32_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass33_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass33_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass34_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass34_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass35_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass35_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass36_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass36_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass37_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass37_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass38_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass38_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass39_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass39_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass40_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass40_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass41_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass41_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass42_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass42_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass43_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass43_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass44_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass44_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass45_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass45_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass46_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass46_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass47_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass47_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass48_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass48_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass51_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass51_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass52_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass52_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass53_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass53_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass54_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass54_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass55_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass55_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass56_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass56_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass57_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass57_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass58_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass58_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass59_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass59_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass63_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass63_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass64_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass64_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass65_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass65_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass66_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass66_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass67_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass67_0

// UnityEngine.TextCore.Text.SpriteAsset/<>c
struct U3CU3Ec_t04E77827D54CC7D69F04FDFC5A84C3F7392F9A76_StaticFields
{
	// UnityEngine.TextCore.Text.SpriteAsset/<>c UnityEngine.TextCore.Text.SpriteAsset/<>c::<>9
	U3CU3Ec_t04E77827D54CC7D69F04FDFC5A84C3F7392F9A76* ___U3CU3E9_0;
	// System.Func`2<UnityEngine.TextCore.Text.SpriteGlyph,System.UInt32> UnityEngine.TextCore.Text.SpriteAsset/<>c::<>9__37_0
	Func_2_t12C2A18A794EA750752F8735CC29CD839D6B4B84* ___U3CU3E9__37_0_1;
	// System.Func`2<UnityEngine.TextCore.Text.SpriteCharacter,System.UInt32> UnityEngine.TextCore.Text.SpriteAsset/<>c::<>9__38_0
	Func_2_tF0A06FF312E9CFC173E3513FCD90DA99BA52B5B1* ___U3CU3E9__38_0_2;
};

// UnityEngine.TextCore.Text.SpriteAsset/<>c

// UnityEngine.Timeline.TimeNotificationBehaviour/<>c
struct U3CU3Ec_t008379627E11F153BBF661B1F74241272206C3D2_StaticFields
{
	// UnityEngine.Timeline.TimeNotificationBehaviour/<>c UnityEngine.Timeline.TimeNotificationBehaviour/<>c::<>9
	U3CU3Ec_t008379627E11F153BBF661B1F74241272206C3D2* ___U3CU3E9_0;
	// System.Comparison`1<UnityEngine.Timeline.TimeNotificationBehaviour/NotificationEntry> UnityEngine.Timeline.TimeNotificationBehaviour/<>c::<>9__12_0
	Comparison_1_tE4F2B745C928DCA6C67CEA5FC784D025346A6CCA* ___U3CU3E9__12_0_1;
};

// UnityEngine.Timeline.TimeNotificationBehaviour/<>c

// UnityEngine.Timeline.TimelineAsset/EditorSettings
struct EditorSettings_t3A8D02A80F57944B75911D9692FECDF6B7081DFF_StaticFields
{
	// System.Double UnityEngine.Timeline.TimelineAsset/EditorSettings::kMinFrameRate
	double ___kMinFrameRate_0;
	// System.Double UnityEngine.Timeline.TimelineAsset/EditorSettings::kMaxFrameRate
	double ___kMaxFrameRate_1;
	// System.Double UnityEngine.Timeline.TimelineAsset/EditorSettings::kDefaultFrameRate
	double ___kDefaultFrameRate_2;
};

// UnityEngine.Timeline.TimelineAsset/EditorSettings

// UnityEngine.Timeline.TimelineClip/TimelineClipUpgrade

// UnityEngine.Timeline.TimelineClip/TimelineClipUpgrade

// UnityEngine.Timeline.TimelineCreateUtilities/<>c__DisplayClass0_0

// UnityEngine.Timeline.TimelineCreateUtilities/<>c__DisplayClass0_0

// UnityEngine.Timeline.TimelineCreateUtilities/<>c__DisplayClass0_1

// UnityEngine.Timeline.TimelineCreateUtilities/<>c__DisplayClass0_1

// UnityEngine.Timeline.TrackAsset/<>c
struct U3CU3Ec_t893DC1C51582BE5AFCDF9E5AE64A900EB53B344A_StaticFields
{
	// UnityEngine.Timeline.TrackAsset/<>c UnityEngine.Timeline.TrackAsset/<>c::<>9
	U3CU3Ec_t893DC1C51582BE5AFCDF9E5AE64A900EB53B344A* ___U3CU3E9_0;
	// System.Comparison`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset/<>c::<>9__121_0
	Comparison_1_t72A7B2EACD3F8F0C3D6A14C88E2B3C719E89C50E* ___U3CU3E9__121_0_1;
};

// UnityEngine.Timeline.TrackAsset/<>c

// Unity.Mathematics.float2/DebuggerProxy

// Unity.Mathematics.float2/DebuggerProxy

// Unity.Mathematics.float3/DebuggerProxy

// Unity.Mathematics.float3/DebuggerProxy

// Unity.Mathematics.float4/DebuggerProxy

// Unity.Mathematics.float4/DebuggerProxy

// Unity.Mathematics.int2/DebuggerProxy

// Unity.Mathematics.int2/DebuggerProxy

// Unity.Mathematics.uint2/DebuggerProxy

// Unity.Mathematics.uint2/DebuggerProxy

// Unity.Mathematics.uint3/DebuggerProxy

// Unity.Mathematics.uint3/DebuggerProxy

// Unity.Mathematics.uint4/DebuggerProxy

// Unity.Mathematics.uint4/DebuggerProxy

// UnityEngine.AnimatorClipInfo

// UnityEngine.AnimatorClipInfo

// UnityEngine.AnimatorStateInfo

// UnityEngine.AnimatorStateInfo

// UnityEngine.AnimatorTransitionInfo

// UnityEngine.AnimatorTransitionInfo

// UnityEngine.AssetFileNameExtensionAttribute

// UnityEngine.AssetFileNameExtensionAttribute

// UnityEngine.XR.Bone

// UnityEngine.XR.Bone

// Unity.Burst.BurstCompileAttribute

// Unity.Burst.BurstCompileAttribute

// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder
struct CatmullRomDecoder_tBC93937ED94DB6355B974915EE9885854F1A5EB0_StaticFields
{
	// DG.Tweening.Plugins.Core.PathCore.ControlPoint[] DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder::_PartialControlPs
	ControlPointU5BU5D_t52F9D1EC70E441ED3915E30FFB75F9B95AD56C59* ____PartialControlPs_0;
	// UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder::_PartialWps
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ____PartialWps_1;
};

// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder

// UnityEngine.TextCore.Text.Character

// UnityEngine.TextCore.Text.Character

// DG.Tweening.Plugins.CirclePlugin

// DG.Tweening.Plugins.CirclePlugin

// DG.Tweening.Plugins.Color2Plugin

// DG.Tweening.Plugins.Color2Plugin

// DG.Tweening.Plugins.Options.ColorOptions

// DG.Tweening.Plugins.Options.ColorOptions

// DG.Tweening.Plugins.ColorPlugin

// DG.Tweening.Plugins.ColorPlugin

// System.Configuration.ConfigurationCollectionAttribute

// System.Configuration.ConfigurationCollectionAttribute

// System.Configuration.ConfigurationElementCollection

// System.Configuration.ConfigurationElementCollection

// System.Configuration.ConfigurationSection

// System.Configuration.ConfigurationSection

// DG.Tweening.Plugins.Core.PathCore.CubicBezierDecoder
struct CubicBezierDecoder_t58382D9354F3F75F8D6A235E945C013EACD3CC1D_StaticFields
{
	// DG.Tweening.Plugins.Core.PathCore.ControlPoint[] DG.Tweening.Plugins.Core.PathCore.CubicBezierDecoder::_PartialControlPs
	ControlPointU5BU5D_t52F9D1EC70E441ED3915E30FFB75F9B95AD56C59* ____PartialControlPs_0;
	// UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.CubicBezierDecoder::_PartialWps
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ____PartialWps_1;
};

// DG.Tweening.Plugins.Core.PathCore.CubicBezierDecoder

// UnityEngine.Animations.DiscreteEvaluationAttribute

// UnityEngine.Animations.DiscreteEvaluationAttribute

// UnityEngine.Timeline.DiscreteTime
struct DiscreteTime_t1598D60B0B2432F702E2A6120D04369EE54600A6_StaticFields
{
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.DiscreteTime::kMaxTime
	DiscreteTime_t1598D60B0B2432F702E2A6120D04369EE54600A6 ___kMaxTime_1;
};

// UnityEngine.Timeline.DiscreteTime

// OPS.Obfuscator.Attribute.DoNotObfuscateClassAttribute

// OPS.Obfuscator.Attribute.DoNotObfuscateClassAttribute

// OPS.Obfuscator.Attribute.DoNotObfuscateMethodBodyAttribute

// OPS.Obfuscator.Attribute.DoNotObfuscateMethodBodyAttribute

// OPS.Obfuscator.Attribute.DoNotRenameAttribute

// OPS.Obfuscator.Attribute.DoNotRenameAttribute

// OPS.Obfuscator.Attribute.DoNotUseClassForFakeCodeAttribute

// OPS.Obfuscator.Attribute.DoNotUseClassForFakeCodeAttribute

// DG.Tweening.Plugins.DoublePlugin

// DG.Tweening.Plugins.DoublePlugin

// UnityEngine.UIElements.UIR.DrawBufferRange

// UnityEngine.UIElements.UIR.DrawBufferRange

// Microsoft.CodeAnalysis.EmbeddedAttribute

// Microsoft.CodeAnalysis.EmbeddedAttribute

// Microsoft.CodeAnalysis.EmbeddedAttribute

// Microsoft.CodeAnalysis.EmbeddedAttribute

// UnityEngine.EventInterests

// UnityEngine.EventInterests

// UnityEngine.XR.Eyes

// UnityEngine.XR.Eyes

// UnityEngine.TextCore.FaceInfo

// UnityEngine.TextCore.FaceInfo

// DG.Tweening.Plugins.Options.FloatOptions

// DG.Tweening.Plugins.Options.FloatOptions

// DG.Tweening.Plugins.FloatPlugin

// DG.Tweening.Plugins.FloatPlugin

// UnityEngine.TextCore.Text.FontAssetCreationEditorSettings

// UnityEngine.TextCore.Text.FontAssetCreationEditorSettings

// UnityEngine.TextCore.LowLevel.FontEngineUtilities

// UnityEngine.TextCore.LowLevel.FontEngineUtilities

// UnityEngine.TextCore.LowLevel.FontReference

// UnityEngine.TextCore.LowLevel.FontReference

// UnityEngine.TextCore.Text.FontStyleStack

// UnityEngine.TextCore.Text.FontStyleStack

// UnityEngine.TextCore.Text.FontWeightPair

// UnityEngine.TextCore.Text.FontWeightPair

// UnityEngine.GUITargetAttribute

// UnityEngine.GUITargetAttribute

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData

// UnityEngine.SocialPlatforms.GameCenter.GcScoreData

// UnityEngine.SocialPlatforms.GameCenter.GcScoreData

// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData

// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData

// UnityEngine.TextCore.GlyphMetrics

// UnityEngine.TextCore.GlyphMetrics

// UnityEngine.TextCore.GlyphRect
struct GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D_StaticFields
{
	// UnityEngine.TextCore.GlyphRect UnityEngine.TextCore.GlyphRect::s_ZeroGlyphRect
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___s_ZeroGlyphRect_4;
};

// UnityEngine.TextCore.GlyphRect

// UnityEngine.TextCore.LowLevel.GlyphValueRecord

// UnityEngine.TextCore.LowLevel.GlyphValueRecord

// UnityEngine.XR.Hand

// UnityEngine.XR.Hand

// UnityEngine.Timeline.HideInMenuAttribute

// UnityEngine.Timeline.HideInMenuAttribute

// UnityEngine.HumanDescription

// UnityEngine.HumanDescription

// UnityEngine.Bindings.IgnoreAttribute

// UnityEngine.Bindings.IgnoreAttribute

// UnityEngine.Timeline.IgnoreOnPlayableTrackAttribute

// UnityEngine.Timeline.IgnoreOnPlayableTrackAttribute

// Unity.IL2CPP.CompilerServices.Il2CppEagerStaticClassConstructionAttribute

// Unity.IL2CPP.CompilerServices.Il2CppEagerStaticClassConstructionAttribute

// UnityEngine.XR.InputDevice

// UnityEngine.XR.InputDevice

// UnityEngine.XR.InputFeatureUsage

// UnityEngine.XR.InputFeatureUsage

// DG.Tweening.Plugins.IntPlugin

// DG.Tweening.Plugins.IntPlugin

// UnityEngine.Timeline.IntervalTreeNode

// UnityEngine.Timeline.IntervalTreeNode

// System.Runtime.CompilerServices.IsReadOnlyAttribute

// System.Runtime.CompilerServices.IsReadOnlyAttribute

// System.Runtime.CompilerServices.IsReadOnlyAttribute

// System.Runtime.CompilerServices.IsReadOnlyAttribute

// System.Linq.Expressions.LambdaExpression

// System.Linq.Expressions.LambdaExpression

// DG.Tweening.Plugins.Core.PathCore.LinearDecoder

// DG.Tweening.Plugins.Core.PathCore.LinearDecoder

// UnityEngine.TextCore.Text.LinkInfo

// UnityEngine.TextCore.Text.LinkInfo

// UnityEngine.SocialPlatforms.Impl.LocalUser

// UnityEngine.SocialPlatforms.Impl.LocalUser

// DG.Tweening.Plugins.LongPlugin

// DG.Tweening.Plugins.LongPlugin

// UnityEngine.Timeline.MarkerList

// UnityEngine.Timeline.MarkerList

// UnityEngine.TextCore.Text.MaterialReference

// UnityEngine.TextCore.Text.MaterialReference

// System.Linq.Expressions.MemberExpression

// System.Linq.Expressions.MemberExpression

// UnityEngine.XR.MeshId
struct MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC_StaticFields
{
	// UnityEngine.XR.MeshId UnityEngine.XR.MeshId::s_InvalidId
	MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC ___s_InvalidId_0;
};

// UnityEngine.XR.MeshId

// UnityEngine.Bindings.NativeAsStructAttribute

// UnityEngine.Bindings.NativeAsStructAttribute

// UnityEngine.NativeClassAttribute

// UnityEngine.NativeClassAttribute

// UnityEngine.Bindings.NativeConditionalAttribute

// UnityEngine.Bindings.NativeConditionalAttribute

// UnityEngine.Bindings.NativeHeaderAttribute

// UnityEngine.Bindings.NativeHeaderAttribute

// UnityEngineInternal.Input.NativeInputEvent

// UnityEngineInternal.Input.NativeInputEvent

// UnityEngineInternal.Input.NativeInputEventBuffer

// UnityEngineInternal.Input.NativeInputEventBuffer

// UnityEngine.Bindings.NativeMethodAttribute

// UnityEngine.Bindings.NativeMethodAttribute

// UnityEngine.Bindings.NativeNameAttribute

// UnityEngine.Bindings.NativeNameAttribute

// UnityEngine.Bindings.NativeThrowsAttribute

// UnityEngine.Bindings.NativeThrowsAttribute

// UnityEngine.Bindings.NativeTypeAttribute

// UnityEngine.Bindings.NativeTypeAttribute

// UnityEngine.Bindings.NativeWritableSelfAttribute

// UnityEngine.Bindings.NativeWritableSelfAttribute

// DG.Tweening.Plugins.Options.NoOptions

// DG.Tweening.Plugins.Options.NoOptions

// UnityEngine.Animations.NotKeyableAttribute

// UnityEngine.Animations.NotKeyableAttribute

// UnityEngine.Timeline.NotKeyableAttribute

// UnityEngine.Timeline.NotKeyableAttribute

// UnityEngine.Bindings.NotNullAttribute

// UnityEngine.Bindings.NotNullAttribute

// OPS.Obfuscator.Attribute.NotObfuscatedCauseAttribute

// OPS.Obfuscator.Attribute.NotObfuscatedCauseAttribute

// OPS.Obfuscator.Attribute.ObfuscateAnywayAttribute

// OPS.Obfuscator.Attribute.ObfuscateAnywayAttribute

// UnityEngine.TextCore.Text.PageInfo

// UnityEngine.TextCore.Text.PageInfo

// DG.Tweening.Plugins.PathPlugin

// DG.Tweening.Plugins.PathPlugin

// UnityEngine.PhysicsScene

// UnityEngine.PhysicsScene

// UnityEngine.PhysicsScene2D

// UnityEngine.PhysicsScene2D

// UnityEngine.Bindings.PreventReadOnlyInstanceModificationAttribute

// UnityEngine.Bindings.PreventReadOnlyInstanceModificationAttribute

// DG.Tweening.CustomPlugins.PureQuaternionPlugin
struct PureQuaternionPlugin_t657F176265AD5B16DAC19DB4055CED610D0E7728_StaticFields
{
	// DG.Tweening.CustomPlugins.PureQuaternionPlugin DG.Tweening.CustomPlugins.PureQuaternionPlugin::_plug
	PureQuaternionPlugin_t657F176265AD5B16DAC19DB4055CED610D0E7728* ____plug_0;
};

// DG.Tweening.CustomPlugins.PureQuaternionPlugin

// DG.Tweening.Plugins.QuaternionPlugin

// DG.Tweening.Plugins.QuaternionPlugin

// UnityEngine.SocialPlatforms.Range

// UnityEngine.SocialPlatforms.Range

// DG.Tweening.Plugins.RectOffsetPlugin
struct RectOffsetPlugin_tACF5D3CEB5F225A9D3A1DFDBAD171F71B674E845_StaticFields
{
	// UnityEngine.RectOffset DG.Tweening.Plugins.RectOffsetPlugin::_r
	RectOffset_t6358774A0DEEABA4586840CB9BC7DC88B39660B5* ____r_0;
};

// DG.Tweening.Plugins.RectOffsetPlugin

// DG.Tweening.Plugins.Options.RectOptions

// DG.Tweening.Plugins.Options.RectOptions

// DG.Tweening.Plugins.RectPlugin

// DG.Tweening.Plugins.RectPlugin

// UnityEngine.Scripting.RequiredByNativeCodeAttribute

// UnityEngine.Scripting.RequiredByNativeCodeAttribute

// UnityEngine.TextCore.Text.RichTextTagAttribute

// UnityEngine.TextCore.Text.RichTextTagAttribute

// UnityEngine.Timeline.RuntimeClipBase

// UnityEngine.Timeline.RuntimeClipBase

// DG.Tweening.Core.SafeModeReport

// DG.Tweening.Core.SafeModeReport

// DG.Tweening.Core.SequenceCallback

// DG.Tweening.Core.SequenceCallback

// UnityEngine.SharedBetweenAnimatorsAttribute

// UnityEngine.SharedBetweenAnimatorsAttribute

// UnityEngine.TextCore.Text.SpriteCharacter

// UnityEngine.TextCore.Text.SpriteCharacter

// UnityEngine.Bindings.StaticAccessorAttribute

// UnityEngine.Bindings.StaticAccessorAttribute

// DG.Tweening.Plugins.Options.StringOptions

// DG.Tweening.Plugins.Options.StringOptions

// DG.Tweening.Plugins.StringPlugin
struct StringPlugin_tFB74E02BC676FC582B73F3505B2D35B9C66FE603_StaticFields
{
	// System.Text.StringBuilder DG.Tweening.Plugins.StringPlugin::_Buffer
	StringBuilder_t* ____Buffer_0;
	// System.Collections.Generic.List`1<System.Char> DG.Tweening.Plugins.StringPlugin::_OpenedTags
	List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7* ____OpenedTags_1;
};

// DG.Tweening.Plugins.StringPlugin

// UnityEngine.Timeline.SupportsChildTracksAttribute

// UnityEngine.Timeline.SupportsChildTracksAttribute

// UnityEngine.ThreadAndSerializationSafeAttribute

// UnityEngine.ThreadAndSerializationSafeAttribute

// UnityEngine.Timeline.TimelinePlayable
struct TimelinePlayable_t01A93A4FCD1222A9B8A7BE65E5FEB187D8D99BBD_StaticFields
{
	// System.Boolean UnityEngine.Timeline.TimelinePlayable::muteAudioScrubbing
	bool ___muteAudioScrubbing_6;
};

// UnityEngine.Timeline.TimelinePlayable

// UnityEngine.Timeline.TrackBindingTypeAttribute

// UnityEngine.Timeline.TrackBindingTypeAttribute

// UnityEngine.Timeline.TrackClipTypeAttribute

// UnityEngine.Timeline.TrackClipTypeAttribute

// DG.Tweening.Tween

// DG.Tweening.Tween

// UnityEngine.UILineInfo

// UnityEngine.UILineInfo

// DG.Tweening.Plugins.Options.UintOptions

// DG.Tweening.Plugins.Options.UintOptions

// DG.Tweening.Plugins.UintPlugin

// DG.Tweening.Plugins.UintPlugin

// DG.Tweening.Plugins.UlongPlugin

// DG.Tweening.Plugins.UlongPlugin

// System.Linq.Expressions.UnaryExpression

// System.Linq.Expressions.UnaryExpression

// UnityEngine.UnityEngineModuleAssembly

// UnityEngine.UnityEngineModuleAssembly

// UnityEngine.Bindings.UnmarshalledAttribute

// UnityEngine.Bindings.UnmarshalledAttribute

// UnityEngine.Scripting.UsedByNativeCodeAttribute

// UnityEngine.Scripting.UsedByNativeCodeAttribute

// DG.Tweening.Plugins.Vector2Plugin

// DG.Tweening.Plugins.Vector2Plugin

// DG.Tweening.Plugins.Options.Vector3ArrayOptions

// DG.Tweening.Plugins.Options.Vector3ArrayOptions

// DG.Tweening.Plugins.Vector3ArrayPlugin

// DG.Tweening.Plugins.Vector3ArrayPlugin

// DG.Tweening.Plugins.Vector3Plugin

// DG.Tweening.Plugins.Vector3Plugin

// DG.Tweening.Plugins.Vector4Plugin

// DG.Tweening.Plugins.Vector4Plugin

// DG.Tweening.Plugins.Options.VectorOptions

// DG.Tweening.Plugins.Options.VectorOptions

// UnityEngine.Bindings.VisibleToOtherModulesAttribute

// UnityEngine.Bindings.VisibleToOtherModulesAttribute

// UnityEngine.TextCore.Text.WordInfo

// UnityEngine.TextCore.Text.WordInfo

// UnityEngine.WritableAttribute

// UnityEngine.WritableAttribute

// UnityEngine.TextCore.Text.XmlTagAttribute

// UnityEngine.TextCore.Text.XmlTagAttribute

// UnityEngine.Yoga.YogaSize

// UnityEngine.Yoga.YogaSize

// UnityEngine.Yoga.YogaValue

// UnityEngine.Yoga.YogaValue

// Unity.Mathematics.float2
struct float2_t24AA5C0F612B0672315EDAFEC9D9E7F1C4A5B0BA_StaticFields
{
	// Unity.Mathematics.float2 Unity.Mathematics.float2::zero
	float2_t24AA5C0F612B0672315EDAFEC9D9E7F1C4A5B0BA ___zero_2;
};

// Unity.Mathematics.float2

// Unity.Mathematics.float3
struct float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E_StaticFields
{
	// Unity.Mathematics.float3 Unity.Mathematics.float3::zero
	float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___zero_3;
};

// Unity.Mathematics.float3

// Unity.Mathematics.float4
struct float4_t89D9A294E7A79BD81BFBDD18654508532958555E_StaticFields
{
	// Unity.Mathematics.float4 Unity.Mathematics.float4::zero
	float4_t89D9A294E7A79BD81BFBDD18654508532958555E ___zero_4;
};

// Unity.Mathematics.float4

// Unity.Mathematics.half

// Unity.Mathematics.half

// Unity.Mathematics.int2

// Unity.Mathematics.int2

// Unity.Mathematics.uint2

// Unity.Mathematics.uint2

// Unity.Mathematics.uint3

// Unity.Mathematics.uint3

// Unity.Mathematics.uint4

// Unity.Mathematics.uint4

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=50

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=50

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32

// Unity.Burst.BurstRuntime/PreserveAttribute

// Unity.Burst.BurstRuntime/PreserveAttribute

// Unity.Burst.BurstString/FormatOptions

// Unity.Burst.BurstString/FormatOptions

// Unity.Burst.BurstString/NumberBuffer

// Unity.Burst.BurstString/NumberBuffer

// Unity.Burst.BurstString/PreserveAttribute

// Unity.Burst.BurstString/PreserveAttribute

// Unity.Burst.BurstString/tFloatUnion32

// Unity.Burst.BurstString/tFloatUnion32

// Unity.Burst.BurstString/tFloatUnion64

// Unity.Burst.BurstString/tFloatUnion64

// DG.Tweening.DOTweenCYInstruction/WaitForCompletion

// DG.Tweening.DOTweenCYInstruction/WaitForCompletion

// DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops

// DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops

// DG.Tweening.DOTweenCYInstruction/WaitForKill

// DG.Tweening.DOTweenCYInstruction/WaitForKill

// DG.Tweening.DOTweenCYInstruction/WaitForPosition

// DG.Tweening.DOTweenCYInstruction/WaitForPosition

// DG.Tweening.DOTweenCYInstruction/WaitForRewind

// DG.Tweening.DOTweenCYInstruction/WaitForRewind

// DG.Tweening.DOTweenCYInstruction/WaitForStart

// DG.Tweening.DOTweenCYInstruction/WaitForStart

// UnityEngine.GUIClip/ParentClipScope

// UnityEngine.GUIClip/ParentClipScope

// UnityEngine.ParticleSystem/CustomDataModule

// UnityEngine.ParticleSystem/CustomDataModule

// UnityEngine.ParticleSystem/MainModule

// UnityEngine.ParticleSystem/MainModule

// UnityEngine.ParticleSystem/MinMaxCurve

// UnityEngine.ParticleSystem/MinMaxCurve

// UnityEngine.ParticleSystem/TextureSheetAnimationModule

// UnityEngine.ParticleSystem/TextureSheetAnimationModule

// UnityEngine.SendMouseEvents/HitInfo

// UnityEngine.SendMouseEvents/HitInfo

// Unity.Burst.SharedStatic/PreserveAttribute

// Unity.Burst.SharedStatic/PreserveAttribute

// UnityEngine.TextCore.Text.TextGenerator/SpecialCharacter

// UnityEngine.TextCore.Text.TextGenerator/SpecialCharacter

// UnityEngine.TextCore.Text.TextResourceManager/FontAssetRef

// UnityEngine.TextCore.Text.TextResourceManager/FontAssetRef

// UnityEngine.TextCore.Text.TextSettings/FontReferenceMap

// UnityEngine.TextCore.Text.TextSettings/FontReferenceMap

// UnityEngine.Timeline.TimeNotificationBehaviour/NotificationEntry

// UnityEngine.Timeline.TimeNotificationBehaviour/NotificationEntry

// UnityEngine.Timeline.TrackAsset/TransientBuildData

// UnityEngine.Timeline.TrackAsset/TransientBuildData

// Unity.Mathematics.math/IntFloatUnion

// Unity.Mathematics.math/IntFloatUnion

// Unity.Burst.BurstString/tBigInt/<m_blocks>e__FixedBuffer

// Unity.Burst.BurstString/tBigInt/<m_blocks>e__FixedBuffer

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t9A1F8C029FA1B33060F0C3AC9DD83EEA394A7808_StaticFields
{
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=50 <PrivateImplementationDetails>::4EF0676A416B9CCF117C378BDCB6C5CA0EA9505154E3F0F7CC8570285DB79663
	__StaticArrayInitTypeSizeU3D50_t10CD55D757224F40CC808D5B7F260E34FD4B2E0F ___4EF0676A416B9CCF117C378BDCB6C5CA0EA9505154E3F0F7CC8570285DB79663_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=20 <PrivateImplementationDetails>::921D53955C220A863C9C5D33DD45329C6F74471C522EA5D46EA838D8D4838055
	__StaticArrayInitTypeSizeU3D20_t2BA535C65B6076A7713D41FD8D81F2C4C9018986 ___921D53955C220A863C9C5D33DD45329C6F74471C522EA5D46EA838D8D4838055_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::A203B1199E78DE3BB75B28FC520ED2F86ADB2749BFC52E3ACA275A3BE2587678
	__StaticArrayInitTypeSizeU3D120_t782DC2F9FE322240B16562C43775CC0C2E85C81F ___A203B1199E78DE3BB75B28FC520ED2F86ADB2749BFC52E3ACA275A3BE2587678_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=50 <PrivateImplementationDetails>::C845A807AA8EC73757DC1B07EA49311061FEDECA35AFFE91C5474E0814925020
	__StaticArrayInitTypeSizeU3D50_t10CD55D757224F40CC808D5B7F260E34FD4B2E0F ___C845A807AA8EC73757DC1B07EA49311061FEDECA35AFFE91C5474E0814925020_3;
};

// <PrivateImplementationDetails>

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t2CADAF0D55AC9D0785A6F7B80D4772CF1220C48F_StaticFields
{
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256 <PrivateImplementationDetails>::07DB995E8ED2CFB0AB71EBA69F3A3EC07D5C6AC10C0C64F33E94ED2949B348AA
	__StaticArrayInitTypeSizeU3D256_tFFE4CE163BD2DCEAA09662C2BCC33B3C37AB0D22 ___07DB995E8ED2CFB0AB71EBA69F3A3EC07D5C6AC10C0C64F33E94ED2949B348AA_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::C69994AC61B52FBCEA582D6CCCD595C12E00BDB18F0C6F593FB6B393CAEDB08C
	__StaticArrayInitTypeSizeU3D32_tF5E240ACF4B30B5A5F8C77E9E49CC2F8559D76D9 ___C69994AC61B52FBCEA582D6CCCD595C12E00BDB18F0C6F593FB6B393CAEDB08C_1;
	// System.Int64 <PrivateImplementationDetails>::D0067CAD9A63E0813759A2BB841051CA73570C0DA2E08E840A8EB45DB6A7A010
	int64_t ___D0067CAD9A63E0813759A2BB841051CA73570C0DA2E08E840A8EB45DB6A7A010_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=3 <PrivateImplementationDetails>::D5B592C05DC25B5032553F1B27F4139BE95E881F73DB33B02B05AB20C3F9981E
	__StaticArrayInitTypeSizeU3D3_t2857C07F0A23FB025DA0D81FCD2BE07B4ADCC026 ___D5B592C05DC25B5032553F1B27F4139BE95E881F73DB33B02B05AB20C3F9981E_3;
};

// <PrivateImplementationDetails>

// UnityEngine.SocialPlatforms.Impl.Achievement

// UnityEngine.SocialPlatforms.Impl.Achievement

// UnityEngine.Animations.AnimationHumanStream

// UnityEngine.Animations.AnimationHumanStream

// UnityEngine.Animations.AnimationStream

// UnityEngine.Animations.AnimationStream

// UnityEngine.Networking.CertificateHandler

// UnityEngine.Networking.CertificateHandler

// DG.Tweening.Plugins.CircleOptions

// DG.Tweening.Plugins.CircleOptions

// UnityEngine.Collision

// UnityEngine.Collision

// UnityEngine.Collision2D

// UnityEngine.Collision2D

// UnityEngine.ContactFilter2D

// UnityEngine.ContactFilter2D

// UnityEngine.ContactPoint

// UnityEngine.ContactPoint

// UnityEngine.ContactPoint2D

// UnityEngine.ContactPoint2D

// DG.Tweening.Plugins.Core.PathCore.ControlPoint

// DG.Tweening.Plugins.Core.PathCore.ControlPoint

// UnityEngine.ControllerColliderHit

// UnityEngine.ControllerColliderHit

// UnityEngine.Networking.DownloadHandler

// UnityEngine.Networking.DownloadHandler

// UnityEngine.Event
struct Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB_StaticFields
{
	// UnityEngine.Event UnityEngine.Event::s_Current
	Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB* ___s_Current_1;
	// UnityEngine.Event UnityEngine.Event::s_MasterEvent
	Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB* ___s_MasterEvent_2;
};

// UnityEngine.Event

// UnityEngine.TextCore.Text.Extents

// UnityEngine.TextCore.Text.Extents

// UnityEngine.Timeline.FrameRateFieldAttribute

// UnityEngine.Timeline.FrameRateFieldAttribute

// UnityEngine.Bindings.FreeFunctionAttribute

// UnityEngine.Bindings.FreeFunctionAttribute

// UnityEngine.GUI
struct GUI_tA9CDB3D69DB13D51AD83ABDB587EF95947EC2D2A_StaticFields
{
	// System.Int32 UnityEngine.GUI::s_HotTextField
	int32_t ___s_HotTextField_0;
	// System.Int32 UnityEngine.GUI::s_BoxHash
	int32_t ___s_BoxHash_1;
	// System.Int32 UnityEngine.GUI::s_ButonHash
	int32_t ___s_ButonHash_2;
	// System.Int32 UnityEngine.GUI::s_RepeatButtonHash
	int32_t ___s_RepeatButtonHash_3;
	// System.Int32 UnityEngine.GUI::s_ToggleHash
	int32_t ___s_ToggleHash_4;
	// System.Int32 UnityEngine.GUI::s_ButtonGridHash
	int32_t ___s_ButtonGridHash_5;
	// System.Int32 UnityEngine.GUI::s_SliderHash
	int32_t ___s_SliderHash_6;
	// System.Int32 UnityEngine.GUI::s_BeginGroupHash
	int32_t ___s_BeginGroupHash_7;
	// System.Int32 UnityEngine.GUI::s_ScrollviewHash
	int32_t ___s_ScrollviewHash_8;
	// System.DateTime UnityEngine.GUI::<nextScrollStepTime>k__BackingField
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___U3CnextScrollStepTimeU3Ek__BackingField_9;
	// UnityEngine.GUISkin UnityEngine.GUI::s_Skin
	GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9* ___s_Skin_10;
	// UnityEngine.Rect UnityEngine.GUI::s_ToolTipRect
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___s_ToolTipRect_11;
	// UnityEngineInternal.GenericStack UnityEngine.GUI::<scrollViewStates>k__BackingField
	GenericStack_t1FB49AB7D847C97ABAA97AB232CA416CABD24C49* ___U3CscrollViewStatesU3Ek__BackingField_12;
};

// UnityEngine.GUI

// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F_StaticFields
{
	// UnityEngine.Rect UnityEngine.GUILayoutEntry::kDummyRect
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___kDummyRect_9;
	// System.Int32 UnityEngine.GUILayoutEntry::indent
	int32_t ___indent_10;
};

// UnityEngine.GUILayoutEntry

// UnityEngine.GUILayoutUtility
struct GUILayoutUtility_t48D00CD11CFC1E09E8EC2E51D59E735F5D24836F_StaticFields
{
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache> UnityEngine.GUILayoutUtility::s_StoredLayouts
	Dictionary_2_tD74A089D3CFE69E54B1617003276B07F5B82B598* ___s_StoredLayouts_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache> UnityEngine.GUILayoutUtility::s_StoredWindows
	Dictionary_2_tD74A089D3CFE69E54B1617003276B07F5B82B598* ___s_StoredWindows_1;
	// UnityEngine.GUILayoutUtility/LayoutCache UnityEngine.GUILayoutUtility::current
	LayoutCache_tF844B2FAD6933B78FD5EFEBDE0529BCBAC19BA60* ___current_2;
	// UnityEngine.Rect UnityEngine.GUILayoutUtility::kDummyRect
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___kDummyRect_3;
};

// UnityEngine.GUILayoutUtility

// UnityEngine.GUISettings

// UnityEngine.GUISettings

// UnityEngine.GUIStyleState

// UnityEngine.GUIStyleState

// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard

// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard

// UnityEngine.UIElements.UIR.GfxUpdateBufferRange

// UnityEngine.UIElements.UIR.GfxUpdateBufferRange

// UnityEngine.TextCore.Glyph

// UnityEngine.TextCore.Glyph

// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord

// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord

// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct

// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct

// UnityEngine.HumanLimit

// UnityEngine.HumanLimit

// System.Configuration.IgnoreSection

// System.Configuration.IgnoreSection

// UnityEngine.IntegratedSubsystem

// UnityEngine.IntegratedSubsystem

// UnityEngine.IntegratedSubsystemDescriptor

// UnityEngine.IntegratedSubsystemDescriptor

// UnityEngine.SocialPlatforms.Impl.Leaderboard

// UnityEngine.SocialPlatforms.Impl.Leaderboard

// UnityEngine.XR.MeshGenerationResult

// UnityEngine.XR.MeshGenerationResult

// UnityEngine.TextCore.Text.MeshInfo
struct MeshInfo_tE55C4A8846CC2C399CCC3FE989476D987B86AB2F_StaticFields
{
	// UnityEngine.Color32 UnityEngine.TextCore.Text.MeshInfo::k_DefaultColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___k_DefaultColor_0;
};

// UnityEngine.TextCore.Text.MeshInfo

// UnityEngine.ModifiableContactPair

// UnityEngine.ModifiableContactPair

// UnityEngine.Bindings.NativePropertyAttribute

// UnityEngine.Bindings.NativePropertyAttribute

// UnityEngine.ObjectGUIState

// UnityEngine.ObjectGUIState

// DG.Tweening.Plugins.Options.PathOptions

// DG.Tweening.Plugins.Options.PathOptions

// DG.Tweening.Plugins.Options.QuaternionOptions

// DG.Tweening.Plugins.Options.QuaternionOptions

// UnityEngine.RaycastHit

// UnityEngine.RaycastHit

// UnityEngine.RaycastHit2D

// UnityEngine.RaycastHit2D

// UnityEngine.RemoteConfigSettings

// UnityEngine.RemoteConfigSettings

// UnityEngine.SocialPlatforms.Impl.Score

// UnityEngine.SocialPlatforms.Impl.Score

// UnityEngine.SendMouseEvents
struct SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields
{
	// System.Boolean UnityEngine.SendMouseEvents::s_MouseUsed
	bool ___s_MouseUsed_0;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_LastHit
	HitInfoU5BU5D_tDAE7DF0D2B0BE3EB2FD25FB4418704E27A2BF1D5* ___m_LastHit_1;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_MouseDownHit
	HitInfoU5BU5D_tDAE7DF0D2B0BE3EB2FD25FB4418704E27A2BF1D5* ___m_MouseDownHit_2;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_CurrentHit
	HitInfoU5BU5D_tDAE7DF0D2B0BE3EB2FD25FB4418704E27A2BF1D5* ___m_CurrentHit_3;
	// UnityEngine.Camera[] UnityEngine.SendMouseEvents::m_Cameras
	CameraU5BU5D_t1506EBA524A07AD1066D6DD4D7DFC6721F1AC26B* ___m_Cameras_4;
	// System.Func`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Vector2>> UnityEngine.SendMouseEvents::s_GetMouseState
	Func_1_tF5F7F5DCF1679E08B2536581A6E1EEF5529155C9* ___s_GetMouseState_5;
	// UnityEngine.Vector2 UnityEngine.SendMouseEvents::s_MousePosition
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___s_MousePosition_6;
	// System.Boolean UnityEngine.SendMouseEvents::s_MouseButtonPressedThisFrame
	bool ___s_MouseButtonPressedThisFrame_7;
	// System.Boolean UnityEngine.SendMouseEvents::s_MouseButtonIsPressed
	bool ___s_MouseButtonIsPressed_8;
};

// UnityEngine.SendMouseEvents

// UnityEngine.SkeletonBone

// UnityEngine.SkeletonBone

// UnityEngine.TextEditor
struct TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27_StaticFields
{
	// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp> UnityEngine.TextEditor::s_Keyactions
	Dictionary_2_t6AC338B3CEB934A66B363042F19213FE666F6818* ___s_Keyactions_23;
};

// UnityEngine.TextEditor

// UnityEngine.TextGenerationSettings

// UnityEngine.TextGenerationSettings

// UnityEngine.TextCore.Text.TextGenerationSettings

// UnityEngine.TextCore.Text.TextGenerationSettings

// UnityEngine.TextCore.Text.TextGeneratorUtilities
struct TextGeneratorUtilities_tAD0F329B1A5C7CC27CF63086C11FE092B43FED53_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.TextCore.Text.TextGeneratorUtilities::largePositiveVector2
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___largePositiveVector2_0;
	// UnityEngine.Vector2 UnityEngine.TextCore.Text.TextGeneratorUtilities::largeNegativeVector2
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___largeNegativeVector2_1;
};

// UnityEngine.TextCore.Text.TextGeneratorUtilities

// UnityEngine.TextCore.Text.TextInfo
struct TextInfo_t27E58E62A7552C66D38C175AF9D22622365F5D09_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.TextCore.Text.TextInfo::s_InfinityVectorPositive
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___s_InfinityVectorPositive_0;
	// UnityEngine.Vector2 UnityEngine.TextCore.Text.TextInfo::s_InfinityVectorNegative
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___s_InfinityVectorNegative_1;
};

// UnityEngine.TextCore.Text.TextInfo

// UnityEngine.UIElements.TextNativeSettings

// UnityEngine.UIElements.TextNativeSettings

// UnityEngine.TextCore.Text.TextVertex

// UnityEngine.TextCore.Text.TextVertex

// UnityEngine.UIElements.TextVertex

// UnityEngine.UIElements.TextVertex

// UnityEngine.Bindings.ThreadSafeAttribute

// UnityEngine.Bindings.ThreadSafeAttribute

// UnityEngine.Touch

// UnityEngine.Touch

// UnityEngine.Timeline.TrackColorAttribute

// UnityEngine.Timeline.TrackColorAttribute

// DG.Tweening.Tweener

// DG.Tweening.Tweener

// UnityEngine.UICharInfo

// UnityEngine.UICharInfo

// UnityEngine.UIVertex
struct UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207_StaticFields
{
	// UnityEngine.Color32 UnityEngine.UIVertex::s_DefaultColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___s_DefaultColor_8;
	// UnityEngine.Vector4 UnityEngine.UIVertex::s_DefaultTangent
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___s_DefaultTangent_9;
	// UnityEngine.UIVertex UnityEngine.UIVertex::simpleVert
	UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207 ___simpleVert_10;
};

// UnityEngine.UIVertex

// UnityEngine.Networking.UploadHandler

// UnityEngine.Networking.UploadHandler

// UnityEngine.XR.XRNodeState

// UnityEngine.XR.XRNodeState

// UnityEngine.Yoga.YogaConfig
struct YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345_StaticFields
{
	// UnityEngine.Yoga.YogaConfig UnityEngine.Yoga.YogaConfig::Default
	YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345* ___Default_0;
};

// UnityEngine.Yoga.YogaConfig

// UnityEngine.Yoga.YogaNode

// UnityEngine.Yoga.YogaNode

// Unity.Mathematics.float3x3
struct float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79_StaticFields
{
	// Unity.Mathematics.float3x3 Unity.Mathematics.float3x3::identity
	float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 ___identity_3;
};

// Unity.Mathematics.float3x3

// Unity.Mathematics.float4x4
struct float4x4_t7EDD16F7F57DC7F61A6302535F7C19FB97915DF2_StaticFields
{
	// Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::identity
	float4x4_t7EDD16F7F57DC7F61A6302535F7C19FB97915DF2 ___identity_4;
	// Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::zero
	float4x4_t7EDD16F7F57DC7F61A6302535F7C19FB97915DF2 ___zero_5;
};

// Unity.Mathematics.float4x4

// Unity.Mathematics.quaternion
struct quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4_StaticFields
{
	// Unity.Mathematics.quaternion Unity.Mathematics.quaternion::identity
	quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4 ___identity_1;
};

// Unity.Mathematics.quaternion

// Unity.Burst.BurstString/tBigInt

// Unity.Burst.BurstString/tBigInt

// CartoonFX.CFXR_Effect/CameraShake
struct CameraShake_tF662E3447A81E0F4B337C630E8DDF9758C02DFD0_StaticFields
{
	// System.Boolean CartoonFX.CFXR_Effect/CameraShake::editorPreview
	bool ___editorPreview_0;
	// System.Boolean CartoonFX.CFXR_Effect/CameraShake::cju
	bool ___cju_14;
	// System.Collections.Generic.List`1<CartoonFX.CFXR_Effect/CameraShake> CartoonFX.CFXR_Effect/CameraShake::cjv
	List_1_t195AE9539E927EB5BCA43B11B0FC84CC5A780A06* ___cjv_15;
};

// CartoonFX.CFXR_Effect/CameraShake

// DG.Tweening.DOTweenModulePhysics/dh

// DG.Tweening.DOTweenModulePhysics/dh

// DG.Tweening.DOTweenModulePhysics2D/dq

// DG.Tweening.DOTweenModulePhysics2D/dq

// DG.Tweening.DOTweenModuleSprite/dx

// DG.Tweening.DOTweenModuleSprite/dx

// DG.Tweening.DOTweenModuleUI/fa

// DG.Tweening.DOTweenModuleUI/fa

// DG.Tweening.DOTweenModuleUI/fj

// DG.Tweening.DOTweenModuleUI/fj

// DG.Tweening.DOTweenModuleUI/fk

// DG.Tweening.DOTweenModuleUI/fk

// DG.Tweening.DOTweenModuleUI/fl

// DG.Tweening.DOTweenModuleUI/fl

// UnityEngine.ParticleSystem/MinMaxGradient

// UnityEngine.ParticleSystem/MinMaxGradient

// UnityEngine.ParticleSystem/Particle

// UnityEngine.ParticleSystem/Particle

// UnityEngine.Rendering.Universal.ShaderInput/LightData

// UnityEngine.Rendering.Universal.ShaderInput/LightData

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass71_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass71_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass73_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass73_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass74_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass74_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0

// UnityEngine.XR.XRDisplaySubsystem/XRBlitParams

// UnityEngine.XR.XRDisplaySubsystem/XRBlitParams

// UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc

// UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc

// UnityEngine.XR.XRDisplaySubsystem/XRRenderParameter

// UnityEngine.XR.XRDisplaySubsystem/XRRenderParameter

// UnityEngine.XR.XRMeshSubsystem/MeshTransformList

// UnityEngine.XR.XRMeshSubsystem/MeshTransformList

// UnityEngine.Animations.AnimationClipPlayable

// UnityEngine.Animations.AnimationClipPlayable

// UnityEngine.AnimationEvent

// UnityEngine.AnimationEvent

// UnityEngine.Animations.AnimationLayerMixerPlayable
struct AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D_StaticFields
{
	// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Animations.AnimationLayerMixerPlayable::m_NullPlayable
	AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationLayerMixerPlayable

// UnityEngine.Animations.AnimationMixerPlayable
struct AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0_StaticFields
{
	// UnityEngine.Animations.AnimationMixerPlayable UnityEngine.Animations.AnimationMixerPlayable::m_NullPlayable
	AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0 ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationMixerPlayable

// UnityEngine.Animations.AnimationMotionXToDeltaPlayable
struct AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18_StaticFields
{
	// UnityEngine.Animations.AnimationMotionXToDeltaPlayable UnityEngine.Animations.AnimationMotionXToDeltaPlayable::m_NullPlayable
	AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18 ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationMotionXToDeltaPlayable

// UnityEngine.Animations.AnimationOffsetPlayable
struct AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4_StaticFields
{
	// UnityEngine.Animations.AnimationOffsetPlayable UnityEngine.Animations.AnimationOffsetPlayable::m_NullPlayable
	AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4 ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationOffsetPlayable

// UnityEngine.Animations.AnimationPlayableOutput

// UnityEngine.Animations.AnimationPlayableOutput

// UnityEngine.Animations.AnimationPosePlayable
struct AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C_StaticFields
{
	// UnityEngine.Animations.AnimationPosePlayable UnityEngine.Animations.AnimationPosePlayable::m_NullPlayable
	AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationPosePlayable

// UnityEngine.Animations.AnimationRemoveScalePlayable
struct AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD_StaticFields
{
	// UnityEngine.Animations.AnimationRemoveScalePlayable UnityEngine.Animations.AnimationRemoveScalePlayable::m_NullPlayable
	AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationRemoveScalePlayable

// UnityEngine.Animations.AnimationScriptPlayable
struct AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127_StaticFields
{
	// UnityEngine.Animations.AnimationScriptPlayable UnityEngine.Animations.AnimationScriptPlayable::m_NullPlayable
	AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127 ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationScriptPlayable

// UnityEngine.AnimationState

// UnityEngine.AnimationState

// UnityEngine.Animations.AnimatorControllerPlayable
struct AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A_StaticFields
{
	// UnityEngine.Animations.AnimatorControllerPlayable UnityEngine.Animations.AnimatorControllerPlayable::m_NullPlayable
	AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimatorControllerPlayable

// UnityEngine.AudioClip

// UnityEngine.AudioClip

// UnityEngine.Audio.AudioClipPlayable

// UnityEngine.Audio.AudioClipPlayable

// UnityEngine.Audio.AudioMixer

// UnityEngine.Audio.AudioMixer

// UnityEngine.Audio.AudioMixerPlayable

// UnityEngine.Audio.AudioMixerPlayable

// UnityEngine.Audio.AudioPlayableOutput

// UnityEngine.Audio.AudioPlayableOutput

// UnityEngine.Avatar

// UnityEngine.Avatar

// UnityEngine.AvatarMask

// UnityEngine.AvatarMask

// UnityEngine.Networking.DownloadHandlerBuffer

// UnityEngine.Networking.DownloadHandlerBuffer

// UnityEngine.ExitGUIException

// UnityEngine.ExitGUIException

// UnityEngine.Font
struct Font_tC95270EA3198038970422D78B74A7F2E218A96B6_StaticFields
{
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_tD91E4D0ED3C2E385D3BDD4B3EA48B5F99D39F1DC* ___textureRebuilt_4;
};

// UnityEngine.Font

// UnityEngine.GUILayoutGroup
struct GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D_StaticFields
{
	// UnityEngine.GUILayoutEntry UnityEngine.GUILayoutGroup::none
	GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F* ___none_31;
};

// UnityEngine.GUILayoutGroup

// UnityEngine.GUIStyle
struct GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_StaticFields
{
	// System.Boolean UnityEngine.GUIStyle::showKeyboardFocus
	bool ___showKeyboardFocus_14;
	// UnityEngine.GUIStyle UnityEngine.GUIStyle::s_None
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___s_None_15;
};

// UnityEngine.GUIStyle

// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord

// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord

// UnityEngine.HumanBone

// UnityEngine.HumanBone

// UnityEngine.TextCore.Text.LineInfo

// UnityEngine.TextCore.Text.LineInfo

// UnityEngine.Motion

// UnityEngine.Motion

// DG.Tweening.Plugins.Core.PathCore.Path
struct Path_t6EC35555EF601CAFED947AC467DEBA7C1496A0C3_StaticFields
{
	// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder DG.Tweening.Plugins.Core.PathCore.Path::_catmullRomDecoder
	CatmullRomDecoder_tBC93937ED94DB6355B974915EE9885854F1A5EB0* ____catmullRomDecoder_0;
	// DG.Tweening.Plugins.Core.PathCore.LinearDecoder DG.Tweening.Plugins.Core.PathCore.Path::_linearDecoder
	LinearDecoder_tC7C53176BBF58227DC1855AFDBA3FAFF19860B15* ____linearDecoder_1;
	// DG.Tweening.Plugins.Core.PathCore.CubicBezierDecoder DG.Tweening.Plugins.Core.PathCore.Path::_cubicBezierDecoder
	CubicBezierDecoder_t58382D9354F3F75F8D6A235E945C013EACD3CC1D* ____cubicBezierDecoder_2;
};

// DG.Tweening.Plugins.Core.PathCore.Path

// UnityEngine.PhysicsMaterial2D

// UnityEngine.PhysicsMaterial2D

// UnityEngine.RuntimeAnimatorController

// UnityEngine.RuntimeAnimatorController

// UnityEngine.TextCore.Text.SpriteGlyph

// UnityEngine.TextCore.Text.SpriteGlyph

// UnityEngine.TextCore.Text.TextElementInfo

// UnityEngine.TextCore.Text.TextElementInfo

// UnityEngine.TextGenerator

// UnityEngine.TextGenerator

// UnityEngine.Networking.UnityWebRequest

// UnityEngine.Networking.UnityWebRequest

// UnityEngine.Networking.UnityWebRequestAsyncOperation

// UnityEngine.Networking.UnityWebRequestAsyncOperation

// UnityEngine.UIElements.UIR.Utility
struct Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields
{
	// System.Action`1<System.Boolean> UnityEngine.UIElements.UIR.Utility::GraphicsResourcesRecreate
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* ___GraphicsResourcesRecreate_0;
	// System.Action UnityEngine.UIElements.UIR.Utility::EngineUpdate
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___EngineUpdate_1;
	// System.Action UnityEngine.UIElements.UIR.Utility::FlushPendingResources
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___FlushPendingResources_2;
	// System.Action`1<UnityEngine.Camera> UnityEngine.UIElements.UIR.Utility::RegisterIntermediateRenderers
	Action_1_t268986DA4CF361AC17B40338506A83AFB35832EA* ___RegisterIntermediateRenderers_3;
	// System.Action`1<System.IntPtr> UnityEngine.UIElements.UIR.Utility::RenderNodeAdd
	Action_1_t2DF1ED40E3084E997390FF52F462390882271FE2* ___RenderNodeAdd_4;
	// System.Action`1<System.IntPtr> UnityEngine.UIElements.UIR.Utility::RenderNodeExecute
	Action_1_t2DF1ED40E3084E997390FF52F462390882271FE2* ___RenderNodeExecute_5;
	// System.Action`1<System.IntPtr> UnityEngine.UIElements.UIR.Utility::RenderNodeCleanup
	Action_1_t2DF1ED40E3084E997390FF52F462390882271FE2* ___RenderNodeCleanup_6;
	// Unity.Profiling.ProfilerMarker UnityEngine.UIElements.UIR.Utility::s_MarkerRaiseEngineUpdate
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD ___s_MarkerRaiseEngineUpdate_7;
};

// UnityEngine.UIElements.UIR.Utility

// UnityEngine.ParticleSystem/EmitParams

// UnityEngine.ParticleSystem/EmitParams

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass15_0

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass15_0

// UnityEngine.XR.XRDisplaySubsystem/XRRenderPass

// UnityEngine.XR.XRDisplaySubsystem/XRRenderPass

// UnityEngine.AnimationClip

// UnityEngine.AnimationClip

// UnityEngine.Timeline.AnimationOutputWeightProcessor

// UnityEngine.Timeline.AnimationOutputWeightProcessor

// UnityEngine.AnimatorOverrideController

// UnityEngine.AnimatorOverrideController

// UnityEngine.Yoga.BaselineFunction

// UnityEngine.Yoga.BaselineFunction

// CartoonFX.CFXR_ParticleTextFontAsset

// CartoonFX.CFXR_ParticleTextFontAsset

// UnityEngine.CanvasRenderer

// UnityEngine.CanvasRenderer

// UnityEngine.Collider

// UnityEngine.Collider

// DG.Tweening.Core.DOTweenSettings

// DG.Tweening.Core.DOTweenSettings

// UnityEngine.GUIScrollGroup

// UnityEngine.GUIScrollGroup

// UnityEngine.GUISkin
struct GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9_StaticFields
{
	// UnityEngine.GUIStyle UnityEngine.GUISkin::ms_Error
	GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580* ___ms_Error_30;
	// UnityEngine.GUISkin/SkinChangedDelegate UnityEngine.GUISkin::m_SkinChanged
	SkinChangedDelegate_tA6D456E853D58AD2EF8A599F543C7E5BA8E94B98* ___m_SkinChanged_32;
	// UnityEngine.GUISkin UnityEngine.GUISkin::current
	GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9* ___current_33;
};

// UnityEngine.GUISkin

// UnityEngine.Timeline.InfiniteRuntimeClip
struct InfiniteRuntimeClip_t4421729DA047CB7D239F88EBD2B8103D06686D79_StaticFields
{
	// System.Int64 UnityEngine.Timeline.InfiniteRuntimeClip::kIntervalEnd
	int64_t ___kIntervalEnd_2;
};

// UnityEngine.Timeline.InfiniteRuntimeClip

// UnityEngine.Yoga.Logger

// UnityEngine.Yoga.Logger

// UnityEngine.Yoga.MeasureFunction

// UnityEngine.Yoga.MeasureFunction

// UnityEngineInternal.Input.NativeUpdateCallback

// UnityEngineInternal.Input.NativeUpdateCallback

// UnityEngine.ParticleSystem

// UnityEngine.ParticleSystem

// UnityEngine.Rigidbody

// UnityEngine.Rigidbody

// UnityEngine.Rigidbody2D

// UnityEngine.Rigidbody2D

// UnityEngine.Timeline.RuntimeClip

// UnityEngine.Timeline.RuntimeClip

// UnityEngine.StateMachineBehaviour

// UnityEngine.StateMachineBehaviour

// UnityEngine.TextCore.Text.TextAsset

// UnityEngine.TextCore.Text.TextAsset

// UnityEngine.TextCore.Text.TextColorGradient
struct TextColorGradient_t22D94E441E8E8CD772B966C167E5C0AEB0919D70_StaticFields
{
	// UnityEngine.Color UnityEngine.TextCore.Text.TextColorGradient::k_DefaultColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___k_DefaultColor_10;
};

// UnityEngine.TextCore.Text.TextColorGradient

// UnityEngine.TextMesh

// UnityEngine.TextMesh

// UnityEngine.TextCore.Text.TextSettings

// UnityEngine.TextCore.Text.TextSettings

// UnityEngine.TextCore.Text.TextStyleSheet

// UnityEngine.TextCore.Text.TextStyleSheet

// UnityEngine.Timeline.TimeNotificationBehaviour

// UnityEngine.Timeline.TimeNotificationBehaviour

// UnityEngine.TextCore.Text.WordWrapState

// UnityEngine.TextCore.Text.WordWrapState

// UnityEngine.XR.XRDisplaySubsystem

// UnityEngine.XR.XRDisplaySubsystem

// UnityEngine.XR.XRDisplaySubsystemDescriptor

// UnityEngine.XR.XRDisplaySubsystemDescriptor

// UnityEngine.XR.XRInputSubsystem

// UnityEngine.XR.XRInputSubsystem

// UnityEngine.XR.XRInputSubsystemDescriptor

// UnityEngine.XR.XRInputSubsystemDescriptor

// UnityEngine.XR.XRMeshSubsystem

// UnityEngine.XR.XRMeshSubsystem

// UnityEngine.XR.XRMeshSubsystemDescriptor

// UnityEngine.XR.XRMeshSubsystemDescriptor

// UnityEngine.Analytics.AnalyticsSessionInfo/IdentityTokenChanged

// UnityEngine.Analytics.AnalyticsSessionInfo/IdentityTokenChanged

// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged

// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged

// UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo

// UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo

// UnityEngine.Timeline.AnimationPlayableAsset/<get_outputs>d__45

// UnityEngine.Timeline.AnimationPlayableAsset/<get_outputs>d__45

// UnityEngine.Timeline.AnimationTrack/<get_outputs>d__49

// UnityEngine.Timeline.AnimationTrack/<get_outputs>d__49

// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback

// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback

// UnityEngine.AudioClip/PCMReaderCallback

// UnityEngine.AudioClip/PCMReaderCallback

// UnityEngine.AudioClip/PCMSetPositionCallback

// UnityEngine.AudioClip/PCMSetPositionCallback

// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler

// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler

// UnityEngine.AudioSettings/AudioConfigurationChangeHandler

// UnityEngine.AudioSettings/AudioConfigurationChangeHandler

// UnityEngine.Canvas/WillRenderCanvases

// UnityEngine.Canvas/WillRenderCanvases

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15

// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15

// UnityEngine.Font/FontTextureRebuildCallback

// UnityEngine.Font/FontTextureRebuildCallback

// UnityEngine.GUI/WindowFunction

// UnityEngine.GUI/WindowFunction

// UnityEngine.GUISkin/SkinChangedDelegate

// UnityEngine.GUISkin/SkinChangedDelegate

// UnityEngine.RemoteSettings/UpdatedEventHandler

// UnityEngine.RemoteSettings/UpdatedEventHandler

// UnityEngine.Timeline.TimelineAsset/<get_outputs>d__27

// UnityEngine.Timeline.TimelineAsset/<get_outputs>d__27

// UnityEngine.Timeline.TrackAsset/<get_outputs>d__65

// UnityEngine.Timeline.TrackAsset/<get_outputs>d__65

// UnityEngine.Animation

// UnityEngine.Animation

// UnityEngine.Timeline.AnimationPlayableAsset
struct AnimationPlayableAsset_t381BF46F8FEBECFD107EC61C257C60D9438FDF33_StaticFields
{
	// System.Int32 UnityEngine.Timeline.AnimationPlayableAsset::k_LatestVersion
	int32_t ___k_LatestVersion_13;
};

// UnityEngine.Timeline.AnimationPlayableAsset

// UnityEngine.Animator

// UnityEngine.Animator

// UnityEngine.AudioBehaviour

// UnityEngine.AudioBehaviour

// UnityEngine.BoxCollider

// UnityEngine.BoxCollider

// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields
{
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::preWillRenderCanvases
	WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC* ___preWillRenderCanvases_4;
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC* ___willRenderCanvases_5;
	// System.Action`1<System.Int32> UnityEngine.Canvas::<externBeginRenderOverlays>k__BackingField
	Action_1_tD69A6DC9FBE94131E52F5A73B2A9D4AB51EEC404* ___U3CexternBeginRenderOverlaysU3Ek__BackingField_6;
	// System.Action`2<System.Int32,System.Int32> UnityEngine.Canvas::<externRenderOverlaysBefore>k__BackingField
	Action_2_tD7438462601D3939500ED67463331FE00CFFBDB8* ___U3CexternRenderOverlaysBeforeU3Ek__BackingField_7;
	// System.Action`1<System.Int32> UnityEngine.Canvas::<externEndRenderOverlays>k__BackingField
	Action_1_tD69A6DC9FBE94131E52F5A73B2A9D4AB51EEC404* ___U3CexternEndRenderOverlaysU3Ek__BackingField_8;
};

// UnityEngine.Canvas

// UnityEngine.CanvasGroup

// UnityEngine.CanvasGroup

// UnityEngine.CapsuleCollider

// UnityEngine.CapsuleCollider

// UnityEngine.CharacterController

// UnityEngine.CharacterController

// UnityEngine.Collider2D

// UnityEngine.Collider2D

// UnityEngine.TextCore.Text.FontAsset
struct FontAsset_t61A6446D934E582651044E33D250EA8D306AB958_StaticFields
{
	// Unity.Profiling.ProfilerMarker UnityEngine.TextCore.Text.FontAsset::k_ReadFontAssetDefinitionMarker
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD ___k_ReadFontAssetDefinitionMarker_42;
	// Unity.Profiling.ProfilerMarker UnityEngine.TextCore.Text.FontAsset::k_AddSynthesizedCharactersMarker
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD ___k_AddSynthesizedCharactersMarker_43;
	// Unity.Profiling.ProfilerMarker UnityEngine.TextCore.Text.FontAsset::k_TryAddCharacterMarker
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD ___k_TryAddCharacterMarker_44;
	// Unity.Profiling.ProfilerMarker UnityEngine.TextCore.Text.FontAsset::k_TryAddCharactersMarker
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD ___k_TryAddCharactersMarker_45;
	// Unity.Profiling.ProfilerMarker UnityEngine.TextCore.Text.FontAsset::k_UpdateGlyphAdjustmentRecordsMarker
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD ___k_UpdateGlyphAdjustmentRecordsMarker_46;
	// Unity.Profiling.ProfilerMarker UnityEngine.TextCore.Text.FontAsset::k_ClearFontAssetDataMarker
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD ___k_ClearFontAssetDataMarker_47;
	// Unity.Profiling.ProfilerMarker UnityEngine.TextCore.Text.FontAsset::k_UpdateFontAssetDataMarker
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD ___k_UpdateFontAssetDataMarker_48;
	// System.String UnityEngine.TextCore.Text.FontAsset::s_DefaultMaterialSuffix
	String_t* ___s_DefaultMaterialSuffix_49;
	// System.Collections.Generic.HashSet`1<System.Int32> UnityEngine.TextCore.Text.FontAsset::k_SearchedFontAssetLookup
	HashSet_1_t4A2F2B74276D0AD3ED0F873045BD61E9504ECAE2* ___k_SearchedFontAssetLookup_50;
	// System.Collections.Generic.List`1<UnityEngine.TextCore.Text.FontAsset> UnityEngine.TextCore.Text.FontAsset::k_FontAssets_FontFeaturesUpdateQueue
	List_1_t55B85B981AC5FD6A5358491F90FE354F78BB97DE* ___k_FontAssets_FontFeaturesUpdateQueue_51;
	// System.Collections.Generic.HashSet`1<System.Int32> UnityEngine.TextCore.Text.FontAsset::k_FontAssets_FontFeaturesUpdateQueueLookup
	HashSet_1_t4A2F2B74276D0AD3ED0F873045BD61E9504ECAE2* ___k_FontAssets_FontFeaturesUpdateQueueLookup_52;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> UnityEngine.TextCore.Text.FontAsset::k_FontAssets_AtlasTexturesUpdateQueue
	List_1_t0F231C3F13EBA1FF9081BD61489D01AA3CBE59D4* ___k_FontAssets_AtlasTexturesUpdateQueue_53;
	// System.Collections.Generic.HashSet`1<System.Int32> UnityEngine.TextCore.Text.FontAsset::k_FontAssets_AtlasTexturesUpdateQueueLookup
	HashSet_1_t4A2F2B74276D0AD3ED0F873045BD61E9504ECAE2* ___k_FontAssets_AtlasTexturesUpdateQueueLookup_54;
	// System.UInt32[] UnityEngine.TextCore.Text.FontAsset::k_GlyphIndexArray
	UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA* ___k_GlyphIndexArray_65;
};

// UnityEngine.TextCore.Text.FontAsset

// UnityEngine.GridLayout

// UnityEngine.GridLayout

// UnityEngine.MeshCollider

// UnityEngine.MeshCollider

// UnityEngine.ParticleSystemRenderer

// UnityEngine.ParticleSystemRenderer

// UnityEngine.Playables.PlayableDirector

// UnityEngine.Playables.PlayableDirector

// UnityEngine.SphereCollider

// UnityEngine.SphereCollider

// UnityEngine.TextCore.Text.SpriteAsset
struct SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313_StaticFields
{
	// System.Collections.Generic.HashSet`1<System.Int32> UnityEngine.TextCore.Text.SpriteAsset::k_searchedSpriteAssets
	HashSet_1_t4A2F2B74276D0AD3ED0F873045BD61E9504ECAE2* ___k_searchedSpriteAssets_19;
};

// UnityEngine.TextCore.Text.SpriteAsset

// UnityEngine.U2D.SpriteShapeRenderer

// UnityEngine.U2D.SpriteShapeRenderer

// UnityEngine.TextCore.Text.TextGenerator
struct TextGenerator_t6B84DC798596D3A9944DC346DD453C075EE62366_StaticFields
{
	// UnityEngine.TextCore.Text.TextGenerator UnityEngine.TextCore.Text.TextGenerator::s_TextGenerator
	TextGenerator_t6B84DC798596D3A9944DC346DD453C075EE62366* ___s_TextGenerator_0;
};

// UnityEngine.TextCore.Text.TextGenerator

// UnityEngine.Tilemaps.TilemapRenderer

// UnityEngine.Tilemaps.TilemapRenderer

// UnityEngine.Timeline.TimelineAsset

// UnityEngine.Timeline.TimelineAsset

// UnityEngine.Timeline.TrackAsset
struct TrackAsset_t31E19BE900C90F6616C0D337652C8614CD833B96_StaticFields
{
	// UnityEngine.Timeline.TrackAsset/TransientBuildData UnityEngine.Timeline.TrackAsset::s_BuildData
	TransientBuildData_t3BE8EF6B5113561AEE7D53FDF3DB331D39BE194F ___s_BuildData_7;
	// System.Action`3<UnityEngine.Timeline.TimelineClip,UnityEngine.GameObject,UnityEngine.Playables.Playable> UnityEngine.Timeline.TrackAsset::OnClipPlayableCreate
	Action_3_t3638A0A401CA68AF6FECFB956B602BBF7B9EFA72* ___OnClipPlayableCreate_9;
	// System.Action`3<UnityEngine.Timeline.TrackAsset,UnityEngine.GameObject,UnityEngine.Playables.Playable> UnityEngine.Timeline.TrackAsset::OnTrackAnimationPlayableCreate
	Action_3_t8A9161BC98843636E3BF066B37CBCC15C593B73E* ___OnTrackAnimationPlayableCreate_10;
	// UnityEngine.Timeline.TrackAsset[] UnityEngine.Timeline.TrackAsset::s_EmptyCache
	TrackAssetU5BU5D_tE6935AFD32D0BE4B0C69D1CCE96B55D383BCF88C* ___s_EmptyCache_23;
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute> UnityEngine.Timeline.TrackAsset::s_TrackBindingTypeAttributeCache
	Dictionary_2_tF0368534E8881FC0469B58E4901741C5B0CC1D79* ___s_TrackBindingTypeAttributeCache_25;
};

// UnityEngine.Timeline.TrackAsset

// UnityEngine.Timeline.AnimationTrack
struct AnimationTrack_tBE37C239976A84F0A2E6BA3B77263B4F44BB1359_StaticFields
{
	// System.Collections.Generic.Queue`1<UnityEngine.Transform> UnityEngine.Timeline.AnimationTrack::s_CachedQueue
	Queue_1_tED3A82CB4024BD256422C3EF148E0EDAFDFC852A* ___s_CachedQueue_45;
};

// UnityEngine.Timeline.AnimationTrack

// UnityEngine.AudioListener

// UnityEngine.AudioListener

// UnityEngine.AudioSource

// UnityEngine.AudioSource

// Kino.Bloom

// Kino.Bloom

// UnityEngine.BoxCollider2D

// UnityEngine.BoxCollider2D

// CartoonFX.CFXR_Demo

// CartoonFX.CFXR_Demo

// CartoonFX.CFXR_Demo_Rotate

// CartoonFX.CFXR_Demo_Rotate

// CartoonFX.CFXR_Demo_Translate

// CartoonFX.CFXR_Demo_Translate

// CartoonFX.CFXR_Effect
struct CFXR_Effect_t3017EDCEABB0C70663A82AAA961EE597C0AF743A_StaticFields
{
	// System.Boolean CartoonFX.CFXR_Effect::GlobalDisableCameraShake
	bool ___GlobalDisableCameraShake_5;
	// System.Boolean CartoonFX.CFXR_Effect::GlobalDisableLights
	bool ___GlobalDisableLights_6;
	// System.Int32 CartoonFX.CFXR_Effect::ckc
	int32_t ___ckc_16;
};

// CartoonFX.CFXR_Effect

// CartoonFX.CFXR_EmissionBySurface

// CartoonFX.CFXR_EmissionBySurface

// CartoonFX.CFXR_ParticleText

// CartoonFX.CFXR_ParticleText

// UnityEngine.CircleCollider2D

// UnityEngine.CircleCollider2D

// UnityEngine.CompositeCollider2D

// UnityEngine.CompositeCollider2D

// DG.Tweening.Core.DOTweenComponent

// DG.Tweening.Core.DOTweenComponent

// UnityEngine.Timeline.GroupTrack

// UnityEngine.Timeline.GroupTrack

// UnityEngine.Timeline.MarkerTrack

// UnityEngine.Timeline.MarkerTrack

// UnityEngine.Timeline.PlayableTrack

// UnityEngine.Timeline.PlayableTrack

// UnityEngine.PolygonCollider2D

// UnityEngine.PolygonCollider2D

// Unity.ThrowStub

// Unity.ThrowStub

// UnityEngine.Tilemaps.Tilemap

// UnityEngine.Tilemaps.Tilemap

// WebViewObject

// WebViewObject
#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5400;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5400 = { sizeof(U3CU3Ec__DisplayClass11_0_t4CD563957CF59D29FF34DCB41ACF32E7E6C56B4D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5401;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5401 = { sizeof(U3CU3Ec__DisplayClass12_0_t760D335F2D2DF176146C98FA8EA2BF2383C78C2A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5402;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5402 = { sizeof(U3CU3Ec__DisplayClass13_0_t494528D70729B93DB7D601F3D47E3C71DEAC39E2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5403;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5403 = { sizeof(U3CU3Ec__DisplayClass14_0_tE6E24BB0E6CAFBB2DFDE32E8CDD1FE9BB244B067), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5404;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5404 = { sizeof(U3CU3Ec__DisplayClass15_0_t5ECC8CA166122F19F1A179FEF026908B3EAEBDBC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5405;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5405 = { sizeof(U3CU3Ec__DisplayClass16_0_t9217B09EF7ABF88C8F99653F97FCB2F6E0772ED4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5406;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5406 = { sizeof(U3CU3Ec__DisplayClass17_0_t70C4222F705AACA1CB3A9601C9A33FA726E42C4A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5407;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5407 = { sizeof(U3CU3Ec__DisplayClass18_0_t1BFF276984245961583498AE53143E78820EA151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5408;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5408 = { sizeof(U3CU3Ec__DisplayClass19_0_t9CD6F9D8EBF541A8BE42A4A3EA84533FA311AA88), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5409;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5409 = { sizeof(U3CU3Ec__DisplayClass20_0_t9421CA0B6EB7CDFCF453B9E22D9BE4D3918441D6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5410;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5410 = { sizeof(U3CU3Ec__DisplayClass21_0_tDE07ED974C04029C53C41D2EE832E0FE4FBF980C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5411;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5411 = { sizeof(U3CU3Ec__DisplayClass22_0_tEE82B8B9BC498710AEE99211D2D5BAC8B8F309E8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5412;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5412 = { sizeof(U3CU3Ec__DisplayClass23_0_t6A4C1EA019470975DCF72E56D5743016A84CDAA5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5413;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5413 = { sizeof(U3CU3Ec__DisplayClass24_0_t4849CCC37129ED28E5DF2BE13A9A1706DE9E8CB8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5414;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5414 = { sizeof(U3CU3Ec__DisplayClass25_0_t7B41616A17E6850A9081163473AE90592E4F83D5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5415;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5415 = { sizeof(U3CU3Ec__DisplayClass26_0_t129A7FC4A0B467E5E0984FB0FE76F5585A87CC06), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5416;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5416 = { sizeof(U3CU3Ec__DisplayClass27_0_t2F9F7E733BD3F950E7C2EC3025D006C9BCE148E2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5417;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5417 = { sizeof(U3CU3Ec__DisplayClass28_0_tDCD0401BBCE832C8F66F464E01377A8E110A385F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5418;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5418 = { sizeof(U3CU3Ec__DisplayClass29_0_tD7C964849E2B7EEC73ED948CF7170E4D28FB0F50), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5419;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5419 = { sizeof(U3CU3Ec__DisplayClass30_0_t83315C238D477F94424D41BF191800DB803D011F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5420;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5420 = { sizeof(U3CU3Ec__DisplayClass31_0_t81F2950D9BCBF314ADFDCB2FA5B271713E0842F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5421;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5421 = { sizeof(U3CU3Ec__DisplayClass32_0_t8DA65445AF764D116F09828EC2F6DBF80A63E879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5422;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5422 = { sizeof(U3CU3Ec__DisplayClass33_0_t5CD3D4AA9D130B81CF111DEF984D6A9840A20BA4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5423;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5423 = { sizeof(U3CU3Ec__DisplayClass34_0_t057A7B0C01B1A49D0E916492D42C5FB2344E78CC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5424;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5424 = { sizeof(U3CU3Ec__DisplayClass35_0_tBA3B73DAB15E09DFF5EFE1ECD34340A1D0D659A3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5425;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5425 = { sizeof(U3CU3Ec__DisplayClass36_0_t44A8B5E8AF1C9E0594DC5D085B18E274188AB497), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5426;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5426 = { sizeof(U3CU3Ec__DisplayClass37_0_tF05A09D7E18C84C1EADFDADCCC1A63DEF495DE84), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5427;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5427 = { sizeof(U3CU3Ec__DisplayClass38_0_t1EC88301378528B419721DCE1B10D9EE3D91094B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5428;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5428 = { sizeof(U3CU3Ec__DisplayClass39_0_t56A4FBF5B980B30B8E41C437AE6D105DE2B1A370), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5429;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5429 = { sizeof(U3CU3Ec__DisplayClass40_0_tF6F5F6C34FB0526FD16534810E499BCE4E40F7B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5430;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5430 = { sizeof(U3CU3Ec__DisplayClass41_0_t1FAF26807ABB609E6A8D218B75F41954FC6136F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5431;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5431 = { sizeof(U3CU3Ec__DisplayClass42_0_tBD69DF3FE195AEBF1AB253930C48AA48497C09CD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5432;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5432 = { sizeof(U3CU3Ec__DisplayClass43_0_tB3BA33D79526265D7D313B58B739311DF8AE3AED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5433;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5433 = { sizeof(U3CU3Ec__DisplayClass44_0_t2C0E5B7393A966223081AB8116B88BBF6F883D97), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5434;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5434 = { sizeof(U3CU3Ec__DisplayClass45_0_t5F584F3B77663C369DC9227301F305414AFF12A9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5435;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5435 = { sizeof(U3CU3Ec__DisplayClass46_0_t948EE002DF5B4A1E595F7676DAA65C87F9D2BDFD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5436;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5436 = { sizeof(U3CU3Ec__DisplayClass47_0_t8B75D0151AE83F371BCB74DD8B9FA4EFD7319F47), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5437;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5437 = { sizeof(U3CU3Ec__DisplayClass48_0_tE3DCC90A5A9C02495A4A3EC7B0645CF014851AEC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5438;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5438 = { sizeof(U3CU3Ec__DisplayClass51_0_tAC1502125C96CAA86E45C54D8389C1ECD8206A06), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5439;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5439 = { sizeof(U3CU3Ec__DisplayClass52_0_tAB2245D9274F656343DA107BC28107F74438862A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5440;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5440 = { sizeof(U3CU3Ec__DisplayClass53_0_t44A1404C6DAC368C0CDDE88D4A061E65A58748F3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5441;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5441 = { sizeof(U3CU3Ec__DisplayClass54_0_tD6DF8B814EBA95239B92C195AE986EA5FE25B408), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5442;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5442 = { sizeof(U3CU3Ec__DisplayClass55_0_t1D8D264CC436BF750C2CF019FD62CB19CDA71FF4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5443;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5443 = { sizeof(U3CU3Ec__DisplayClass56_0_t50FE7F2A56F77E22BFCCFCB376F7C0FB46745CA5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5444;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5444 = { sizeof(U3CU3Ec__DisplayClass57_0_tF946A93343ACD693F2BCAD0CB38445C844B28748), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5445;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5445 = { sizeof(U3CU3Ec__DisplayClass58_0_t5CF14C7C14CD8658948E8DCD5488900127C1287C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5446;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5446 = { sizeof(U3CU3Ec__DisplayClass59_0_tB90A49433476CF808EEF96ED606D607868A1D80F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5447;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5447 = { sizeof(U3CU3Ec__DisplayClass60_0_t17FC0A48346790190DEA104ACEA77B33ECE0662F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5448;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5448 = { sizeof(U3CU3Ec__DisplayClass61_0_t1DDEF1BD80CEADC19DCEEB7FCC30C90CC2013BC9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5449;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5449 = { sizeof(U3CU3Ec__DisplayClass62_0_t57FD1DA46F208298B66B340BF2DFDF55DBFEA05E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5450;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5450 = { sizeof(U3CU3Ec__DisplayClass63_0_t6ED6F5F7C2A008395097910C532090BDB4233C19), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5451;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5451 = { sizeof(U3CU3Ec__DisplayClass64_0_tAE3DFABCC8670C2C5167F242B7E1B7D334581BC7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5452;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5452 = { sizeof(U3CU3Ec__DisplayClass65_0_tE76C72BE81B1FB69195AB05068181B97608FBC14), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5453;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5453 = { sizeof(U3CU3Ec__DisplayClass66_0_tDD5A3F15C0731E215A01EA78331D5470F79AE88B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5454;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5454 = { sizeof(U3CU3Ec__DisplayClass67_0_t0CA35EFB06B7B01102C6AA20221F04713017B66F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5455;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5455 = { sizeof(U3CU3Ec__DisplayClass68_0_tDB8CAB5A1E37C025A29F8F2995410319626E4800), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5456;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5456 = { sizeof(U3CU3Ec__DisplayClass69_0_tFE24BA7AB38BFBD74C8E70CEA97273EF9B6FFAF7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5457;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5457 = { sizeof(U3CU3Ec__DisplayClass70_0_tB2447C0E75B7AB47789EC0AE2C5D7CDCB11CD89E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5458;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5458 = { sizeof(U3CU3Ec__DisplayClass71_0_tD24AB956CC7388BA7A4EB5D71C62A66A84F1693E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5459;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5459 = { sizeof(U3CU3Ec__DisplayClass72_0_t53244858FA69DF933AF4D7258FCAC1FA41F61EFB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5460;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5460 = { sizeof(U3CU3Ec__DisplayClass73_0_tEC070381D793BF76FDF71D5C7BD804835ED51A75), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5461;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5461 = { sizeof(U3CU3Ec__DisplayClass74_0_tA32299450196DE927349F74CEF3DAFB9A6AD3B98), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5462;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5462 = { sizeof(U3CU3Ec__DisplayClass75_0_t0D7AC3252650CC5E98C4E309BB3816AB9EF60FB0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5463;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5463 = { sizeof(U3CU3Ec__DisplayClass76_0_t45CD5765778EB0A4FA81C848AF6D3B62E9B62B75), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5464;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5464 = { sizeof(U3CU3Ec__DisplayClass77_0_tFEF93DDADD61E664BA439598ED08588F2F927544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5465;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5465 = { sizeof(ShortcutExtensions_t673F7F210C73AF07B020A2F8D6C360C96C470B1A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5466;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5466 = { sizeof(TweenParams_t0BF9382130164497B9A5A19076149D1CEDC697DA), -1, sizeof(TweenParams_t0BF9382130164497B9A5A19076149D1CEDC697DA_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5467;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5467 = { sizeof(TweenSettingsExtensions_t633F1D7D5B2C6D63B9331B5E0B63A14326F4F426), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5468;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5468 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5469;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5469 = { sizeof(Tween_t8CB06EBC48A5B6F5065C490E4F4909C18CE7983C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5470;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5470 = { sizeof(Tweener_tD38633F1A42EDF47A73CE3BF1894D946E830E140), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5471;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5471 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5472;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5472 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5473;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5473 = { sizeof(CircleOptions_tB7B463413662FD10ECA7DF439BF0B36AC7685008)+ sizeof(RuntimeObject), sizeof(CircleOptions_tB7B463413662FD10ECA7DF439BF0B36AC7685008_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5474;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5474 = { sizeof(CirclePlugin_t448312372C0575D793130F5E7EE5EA0C5D32B5C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5475;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5475 = { sizeof(Color2Plugin_tEC0AF600F0A8B7ED84B98B30EC22D064A1BD65E0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5476;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5476 = { sizeof(DoublePlugin_tF3C7CBCD2DFE818D53C038EA61FEE4838F62138F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5477;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5477 = { sizeof(LongPlugin_t383C336429D2F3189B963651F9FF0609978A3B3B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5478;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5478 = { sizeof(UlongPlugin_t201AE77FD5B0C39AD2EE340B411C70A2019A8A6E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5479;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5479 = { sizeof(Vector3ArrayPlugin_tF94D793B537B01F6CB2ECC6E51F85705FF224A8F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5480;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5480 = { sizeof(PathPlugin_t8E755C6DF62D7C06030DCE6562860239008777A6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5481;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5481 = { sizeof(ColorPlugin_t12D5DF9AD61036BFA78330F7123998B5EE1C8954), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5482;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5482 = { sizeof(IntPlugin_tF9F1AFC1C3A6BDD2443ED3E14E9F78EF89EC348A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5483;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5483 = { sizeof(QuaternionPlugin_t1A56047AD40DAFAADC979A7D1060705C6B452494), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5484;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5484 = { sizeof(RectOffsetPlugin_tACF5D3CEB5F225A9D3A1DFDBAD171F71B674E845), -1, sizeof(RectOffsetPlugin_tACF5D3CEB5F225A9D3A1DFDBAD171F71B674E845_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5485;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5485 = { sizeof(RectPlugin_t798CAB602477A5FEC0C26CAF494979B1F2803E8D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5486;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5486 = { sizeof(UintPlugin_t8AD83D61CE5C42CC008163B8DB1AF61837487931), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5487;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5487 = { sizeof(Vector2Plugin_t49FFF2365699F66F30188BB37F03AA461129DD2D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5488;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5488 = { sizeof(Vector4Plugin_tF067D35176F54CB1A4BE2D7C2DE7229C3DB22CE5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5489;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5489 = { sizeof(StringPlugin_tFB74E02BC676FC582B73F3505B2D35B9C66FE603), -1, sizeof(StringPlugin_tFB74E02BC676FC582B73F3505B2D35B9C66FE603_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5490;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5490 = { sizeof(StringPluginExtensions_tA36E0339AE94CFDB282B13B80AD5E10EDB969B8F), -1, sizeof(StringPluginExtensions_tA36E0339AE94CFDB282B13B80AD5E10EDB969B8F_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5491;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5491 = { sizeof(FloatPlugin_tC1B73BD6C186ED983EE92FAFA44996E106521602), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5492;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5492 = { sizeof(Vector3Plugin_t8DB704648AD26DB2C1B603081274AF3225B8BFAD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5493;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5493 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5494;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5494 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5495;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5495 = { sizeof(PathOptions_t76F1CBAC082454349D530B799121EB15BFA4CB3A)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5496;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5496 = { sizeof(QuaternionOptions_t1B83700718F7417854E4B6FB0E1726E183F69718)+ sizeof(RuntimeObject), sizeof(QuaternionOptions_t1B83700718F7417854E4B6FB0E1726E183F69718_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5497;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5497 = { sizeof(UintOptions_t06BB035A1F0801FF6D4721F6F98F36DDD93E53A3)+ sizeof(RuntimeObject), sizeof(UintOptions_t06BB035A1F0801FF6D4721F6F98F36DDD93E53A3_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5498;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5498 = { sizeof(Vector3ArrayOptions_t0F50A7A54A860E4604486511E285C952557C6644)+ sizeof(RuntimeObject), sizeof(Vector3ArrayOptions_t0F50A7A54A860E4604486511E285C952557C6644_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5499;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5499 = { sizeof(NoOptions_t2B4A2CA3C472B5AC37AACC090B1D0B27BCF4307E)+ sizeof(RuntimeObject), sizeof(NoOptions_t2B4A2CA3C472B5AC37AACC090B1D0B27BCF4307E), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5500;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5500 = { sizeof(ColorOptions_t9F2151E3A21F3FE2A41BEEF7D288D670C2685F39)+ sizeof(RuntimeObject), sizeof(ColorOptions_t9F2151E3A21F3FE2A41BEEF7D288D670C2685F39_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5501;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5501 = { sizeof(FloatOptions_t8A9B05DB7CF6CC90A27F300C2977D91A48B3FEF5)+ sizeof(RuntimeObject), sizeof(FloatOptions_t8A9B05DB7CF6CC90A27F300C2977D91A48B3FEF5_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5502;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5502 = { sizeof(RectOptions_tCE72F3B850FF80059E448A0ED2AA3FF16EE5EDAC)+ sizeof(RuntimeObject), sizeof(RectOptions_tCE72F3B850FF80059E448A0ED2AA3FF16EE5EDAC_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5503;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5503 = { sizeof(StringOptions_tC70D70DB6854CE62E6BBC3AA066517835919E9FA)+ sizeof(RuntimeObject), sizeof(StringOptions_tC70D70DB6854CE62E6BBC3AA066517835919E9FA_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5504;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5504 = { sizeof(VectorOptions_t2814CC842518C92C9DFC5DE6F7A73824758D3EF9)+ sizeof(RuntimeObject), sizeof(VectorOptions_t2814CC842518C92C9DFC5DE6F7A73824758D3EF9_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5505;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5505 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5506;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5506 = { sizeof(SpecialPluginsUtils_t3B0BE28310DF18FF4349B72BEF68F3A4AB3EC952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5507;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5507 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5508;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5508 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5509;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5509 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5510;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5510 = { sizeof(PluginsManager_tB3018BC34957F354DA1466C4B00F76EF354BBFA7), -1, sizeof(PluginsManager_tB3018BC34957F354DA1466C4B00F76EF354BBFA7_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5511;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5511 = { sizeof(CubicBezierDecoder_t58382D9354F3F75F8D6A235E945C013EACD3CC1D), -1, sizeof(CubicBezierDecoder_t58382D9354F3F75F8D6A235E945C013EACD3CC1D_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5512;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5512 = { sizeof(ControlPoint_tCAFE592FCADA75007AF24FEB842AE63DAFA9B8D1)+ sizeof(RuntimeObject), sizeof(ControlPoint_tCAFE592FCADA75007AF24FEB842AE63DAFA9B8D1), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5513;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5513 = { sizeof(ABSPathDecoder_t6B479550CEF6C183ACCA13F11E29E019270AB61C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5514;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5514 = { sizeof(CatmullRomDecoder_tBC93937ED94DB6355B974915EE9885854F1A5EB0), -1, sizeof(CatmullRomDecoder_tBC93937ED94DB6355B974915EE9885854F1A5EB0_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5515;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5515 = { sizeof(LinearDecoder_tC7C53176BBF58227DC1855AFDBA3FAFF19860B15), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5516;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5516 = { sizeof(Path_t6EC35555EF601CAFED947AC467DEBA7C1496A0C3), -1, sizeof(Path_t6EC35555EF601CAFED947AC467DEBA7C1496A0C3_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5517;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5517 = { sizeof(PureQuaternionPlugin_t657F176265AD5B16DAC19DB4055CED610D0E7728), -1, sizeof(PureQuaternionPlugin_t657F176265AD5B16DAC19DB4055CED610D0E7728_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5518;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5518 = { sizeof(ABSSequentiable_t05DF85FC63E3650D2D4CF6ABBA0F43263EB8CE89), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5519;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5519 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5520;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5520 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5521;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5521 = { sizeof(Sequence_t084DF9B3D58AA78E1F4F77949DD157CA9E3CAD02), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5522;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5522 = { sizeof(Debugger_tCF42DBFBF81B46CDEE59CA397F2860F3427FE1F0), -1, sizeof(Debugger_tCF42DBFBF81B46CDEE59CA397F2860F3427FE1F0_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5523;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5523 = { sizeof(U3CWaitForCompletionU3Ed__17_tC4B4474C8297E45D0D46677FD6B631561BB82815), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5524;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5524 = { sizeof(U3CWaitForRewindU3Ed__18_t55F75B6C49D9C6AA69B9620D932C482C2070A4C7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5525;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5525 = { sizeof(U3CWaitForKillU3Ed__19_tCD266A012639DB20A94A837CBCC7542219D3D041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5526;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5526 = { sizeof(U3CWaitForElapsedLoopsU3Ed__20_tC6EDF6D2101E0B45832ABB43A0A631C2A9417351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5527;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5527 = { sizeof(U3CWaitForPositionU3Ed__21_t278402B3AE14CDA519C1B7F48D327F9D22FA3337), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5528;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5528 = { sizeof(U3CWaitForStartU3Ed__22_t87727B5B55905440293FA75A84473AD1A582252D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5529;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5529 = { sizeof(DOTweenComponent_tEA6C5A1520B40681AE6FA1703529F60EBC3691DC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5530;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5530 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5531;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5531 = { sizeof(SafeModeOptions_tA3C410AFB34C85C70704176B558984B1BA8AD880), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5532;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5532 = { sizeof(ModulesSetup_t888D98957DAB1E84C63C5F866C838A0E714F7236), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5533;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5533 = { sizeof(DOTweenSettings_tC357F09CCC21098056247CCF9445D9E519ADD4EB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5534;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5534 = { sizeof(Extensions_tB285E4629D501497029ADC7BAD9B6FACC6930625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5535;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5535 = { sizeof(DOTweenExternalCommand_tAD5F307461CED759CA3533F555E48B09C354C49D), -1, sizeof(DOTweenExternalCommand_tAD5F307461CED759CA3533F555E48B09C354C49D_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5536;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5536 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5537;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5537 = { sizeof(SafeModeReport_t36C3527D96D574F81DB1748D1E856C7A539A25B9)+ sizeof(RuntimeObject), sizeof(SafeModeReport_t36C3527D96D574F81DB1748D1E856C7A539A25B9), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5538;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5538 = { sizeof(SequenceCallback_tF04229A385B0898870CB2A68EB10408A723489E8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5539;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5539 = { sizeof(TweenLink_t19BFBE652927BC2D2D5C39CE7F3AB1ECC7315CEE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5540;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5540 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5541;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5541 = { sizeof(TweenManager_t3DFB74C661BA08421AF4EB9F62D6DB3B8F7351F1), -1, sizeof(TweenManager_t3DFB74C661BA08421AF4EB9F62D6DB3B8F7351F1_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5542;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5542 = { sizeof(DOTweenUtils_t88FB5E3E9A98EB0C75054BC17E200352D2D658D1), -1, sizeof(DOTweenUtils_t88FB5E3E9A98EB0C75054BC17E200352D2D658D1_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5543;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5543 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5544;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5544 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5545;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5545 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5546;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5546 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5547;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5547 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5548;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5548 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5549;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5549 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5550;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5550 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5551;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5551 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5552;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5552 = { sizeof(Bounce_t0AEC0E86192AFD2DF3F551A099C4902C113A26E9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5553;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5553 = { sizeof(U3CU3Ec_t124AFA44553F835AFE37BF9E66B435E4CA7DB8B4), -1, sizeof(U3CU3Ec_t124AFA44553F835AFE37BF9E66B435E4CA7DB8B4_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5554;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5554 = { sizeof(EaseManager_t7E8B166DF6DFCEE3386DAB1524EC423E3523EB3C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5555;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5555 = { sizeof(EaseCurve_t36BD7EAD5E58656A06E0D71596FF781803BE9C5F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5556;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5556 = { sizeof(Flash_tBDBDE9EDB27F061B0BB2F196A8A1A9C502F684DF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5557;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5557 = { sizeof(__StaticArrayInitTypeSizeU3D20_t2BA535C65B6076A7713D41FD8D81F2C4C9018986)+ sizeof(RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D20_t2BA535C65B6076A7713D41FD8D81F2C4C9018986), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5558;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5558 = { sizeof(__StaticArrayInitTypeSizeU3D50_t10CD55D757224F40CC808D5B7F260E34FD4B2E0F)+ sizeof(RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D50_t10CD55D757224F40CC808D5B7F260E34FD4B2E0F), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5559;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5559 = { sizeof(__StaticArrayInitTypeSizeU3D120_t782DC2F9FE322240B16562C43775CC0C2E85C81F)+ sizeof(RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D120_t782DC2F9FE322240B16562C43775CC0C2E85C81F), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5560;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5560 = { sizeof(U3CPrivateImplementationDetailsU3E_t9A1F8C029FA1B33060F0C3AC9DD83EEA394A7808), -1, sizeof(U3CPrivateImplementationDetailsU3E_t9A1F8C029FA1B33060F0C3AC9DD83EEA394A7808_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5561;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5561 = { sizeof(U3CModuleU3E_tCCE5947EACBB46966D34E517FB03602C848AA9A0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5562;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5562 = { sizeof(Character_t9B671B493FAC8D43638C69AF6AE92CBD103D80EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5563;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5563 = { sizeof(ColorUtilities_tC9ADAE7FE6DAC43204E7B087D23F6BA85A111D1D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5564;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5564 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5565;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5565 = { sizeof(FontWeightPair_tB94169BD86D0DFC25D651F890B15991A3E0ADA42)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5566;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5566 = { sizeof(FontAssetCreationEditorSettings_t0FF28D2E78F090105C63C81F9E438A7B09E3EA52)+ sizeof(RuntimeObject), sizeof(FontAssetCreationEditorSettings_t0FF28D2E78F090105C63C81F9E438A7B09E3EA52_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5567;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5567 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5568;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5568 = { sizeof(U3CU3Ec_tE3434A696CF5060D63A69C93A84379DBF90E9948), -1, sizeof(U3CU3Ec_tE3434A696CF5060D63A69C93A84379DBF90E9948_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5569;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5569 = { sizeof(FontAsset_t61A6446D934E582651044E33D250EA8D306AB958), -1, sizeof(FontAsset_t61A6446D934E582651044E33D250EA8D306AB958_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5570;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5570 = { sizeof(FontAssetUtilities_t791E501B3F727200A3751332E495071520D393A6), -1, sizeof(FontAssetUtilities_t791E501B3F727200A3751332E495071520D393A6_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5571;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5571 = { sizeof(U3CU3Ec_tCA6A4D073378D45745D0E81D226E721969C3BE80), -1, sizeof(U3CU3Ec_tCA6A4D073378D45745D0E81D226E721969C3BE80_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5572;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5572 = { sizeof(FontFeatureTable_t992E0493CD7E9D7834DF204E0198237F0D25B3B7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5573;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5573 = { sizeof(Extents_t369FB2B84521A0229C2FA3D4C8592B14E07CEFE6)+ sizeof(RuntimeObject), sizeof(Extents_t369FB2B84521A0229C2FA3D4C8592B14E07CEFE6), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5574;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5574 = { sizeof(LineInfo_t2BBD461B330C46ACA45596A8E72FEA4172F88CF5)+ sizeof(RuntimeObject), sizeof(LineInfo_t2BBD461B330C46ACA45596A8E72FEA4172F88CF5), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5575;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5575 = { sizeof(LinkInfo_tE85DDAFDFBDA635E6405C88EE4FD5941A9243DD8)+ sizeof(RuntimeObject), sizeof(LinkInfo_tE85DDAFDFBDA635E6405C88EE4FD5941A9243DD8_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5576;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5576 = { sizeof(MaterialManager_t104D2897F78BE83C3377323E18BEB5B8F0704D9B), -1, sizeof(MaterialManager_t104D2897F78BE83C3377323E18BEB5B8F0704D9B_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5577;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5577 = { sizeof(MaterialReference_t86DB0799D5C82869D4FF0A4F59624AED6910FD26)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5578;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5578 = { sizeof(MaterialReferenceManager_t04E59E4B24B4F971D73124195671473AEFA6DFA9), -1, sizeof(MaterialReferenceManager_t04E59E4B24B4F971D73124195671473AEFA6DFA9_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5579;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5579 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5580;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5580 = { sizeof(MeshInfo_tE55C4A8846CC2C399CCC3FE989476D987B86AB2F)+ sizeof(RuntimeObject), -1, sizeof(MeshInfo_tE55C4A8846CC2C399CCC3FE989476D987B86AB2F_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5581;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5581 = { sizeof(U3CU3Ec_t04E77827D54CC7D69F04FDFC5A84C3F7392F9A76), -1, sizeof(U3CU3Ec_t04E77827D54CC7D69F04FDFC5A84C3F7392F9A76_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5582;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5582 = { sizeof(SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313), -1, sizeof(SpriteAsset_t1D3CF1D9DC350A4690CB09DE228A8B59F2F02313_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5583;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5583 = { sizeof(SpriteCharacter_tB3516A25DBFA0AD68DD8E1432752D503FD1F40F5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5584;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5584 = { sizeof(SpriteGlyph_t0BD62F6EB8D19B2C4B246BC436A8F4BF2E0ACA1A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5585;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5585 = { sizeof(TextAsset_tB28F1843A877CCA74B89DC4F63EA532618B049B8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5586;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5586 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5587;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5587 = { sizeof(TextColorGradient_t22D94E441E8E8CD772B966C167E5C0AEB0919D70), -1, sizeof(TextColorGradient_t22D94E441E8E8CD772B966C167E5C0AEB0919D70_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5588;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5588 = { sizeof(uint8_t)+ sizeof(RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5589;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5589 = { sizeof(TextElement_tCEF567A8810788262275B39DC39CBA6EBE7472DA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5590;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5590 = { sizeof(TextVertex_tF030A16DC67EAF3F6C9C9C0564D4B88758B173A9)+ sizeof(RuntimeObject), sizeof(TextVertex_tF030A16DC67EAF3F6C9C9C0564D4B88758B173A9), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5591;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5591 = { sizeof(TextElementInfo_tDD7A12E319505510E0B350E342BD55F32AB5F976)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5592;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5592 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5593;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5593 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5594;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5594 = { sizeof(TextGenerationSettings_t3E75DB1D14DF53934AF76C9ACB1CD94A344A92A2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5595;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5595 = { sizeof(SpecialCharacter_t869F8BE65A7FE32AFD4196118258F49A63D8E2BD)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5596;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5596 = { sizeof(TextGenerator_t6B84DC798596D3A9944DC346DD453C075EE62366), -1, sizeof(TextGenerator_t6B84DC798596D3A9944DC346DD453C075EE62366_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5597;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5597 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5598;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5598 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5599;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5599 = { sizeof(XmlTagAttribute_t2F0EE4823C3BB8B9ED6844183CA998A44EA1AB59)+ sizeof(RuntimeObject), sizeof(XmlTagAttribute_t2F0EE4823C3BB8B9ED6844183CA998A44EA1AB59), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5600;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5600 = { sizeof(RichTextTagAttribute_t0600951F833208392F1D8EE3E6A58AD5C797F9DA)+ sizeof(RuntimeObject), sizeof(RichTextTagAttribute_t0600951F833208392F1D8EE3E6A58AD5C797F9DA), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5601;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5601 = { sizeof(WordWrapState_tD71131CF008362DB9562FB9794AE9D9225D8F123)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5602;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5602 = { sizeof(TextGeneratorUtilities_tAD0F329B1A5C7CC27CF63086C11FE092B43FED53), -1, sizeof(TextGeneratorUtilities_tAD0F329B1A5C7CC27CF63086C11FE092B43FED53_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5603;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5603 = { sizeof(PageInfo_tFFF6B289E9A37E4D69353B32F941421180DA5909)+ sizeof(RuntimeObject), sizeof(PageInfo_tFFF6B289E9A37E4D69353B32F941421180DA5909), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5604;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5604 = { sizeof(WordInfo_tA466206097891A5A2590896EE164AFC406EB060D)+ sizeof(RuntimeObject), sizeof(WordInfo_tA466206097891A5A2590896EE164AFC406EB060D), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5605;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5605 = { sizeof(TextInfo_t27E58E62A7552C66D38C175AF9D22622365F5D09), -1, sizeof(TextInfo_t27E58E62A7552C66D38C175AF9D22622365F5D09_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5606;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5606 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5607;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5607 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5608;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5608 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5609;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5609 = { sizeof(FontStyleStack_t63C77495F068E6DF762D6AF063A817E3709659A7)+ sizeof(RuntimeObject), sizeof(FontStyleStack_t63C77495F068E6DF762D6AF063A817E3709659A7), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5610;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5610 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5611;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5611 = { sizeof(FontAssetRef_t7B8E634754BC5683F1E6601D7CD0061285A28FF3)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5612;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5612 = { sizeof(TextResourceManager_t10194AE8FB3DE5B9938C916F107713F6BDD3CD88), -1, sizeof(TextResourceManager_t10194AE8FB3DE5B9938C916F107713F6BDD3CD88_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5613;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5613 = { sizeof(FontReferenceMap_t1C0CECF3F0F650BE4A881A50A25EFB26965E7831)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5614;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5614 = { sizeof(TextSettings_tB7F55685AFFD4A96F714427BCACFD6958E357D64), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5615;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5615 = { sizeof(TextShaderUtilities_t47B400695C5D96E7B04FEF9D132468B3A1799692), -1, sizeof(TextShaderUtilities_t47B400695C5D96E7B04FEF9D132468B3A1799692_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5616;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5616 = { sizeof(TextStyle_tD9287057EB15E73ED76AC925AC21A889D64CDAAE), -1, sizeof(TextStyle_tD9287057EB15E73ED76AC925AC21A889D64CDAAE_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5617;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5617 = { sizeof(TextStyleSheet_t86A0FA5523897465F371A2ABC17DFA3558C8D15E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5618;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5618 = { sizeof(TextUtilities_t2A52211BCD70DAC88C65B8FA51B350BECB96FD30), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5619;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5619 = { sizeof(UnicodeLineBreakingRules_t80BE36F5E16AE48FE7B6DE1C91D36B1142B4EC0E), -1, sizeof(UnicodeLineBreakingRules_t80BE36F5E16AE48FE7B6DE1C91D36B1142B4EC0E_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5620;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5620 = { sizeof(U3CModuleU3E_tB500DDB8D220627F9BFAF448763790F027856F11), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5621;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5621 = { sizeof(EmbeddedAttribute_t4F9AD0F906A21B3318CFA9466B076A467E16305F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5622;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5622 = { sizeof(IsReadOnlyAttribute_t537BA5EEEDB652AA60A00E4CEA6B73710FBDD806), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5623;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5623 = { sizeof(Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB), sizeof(Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB_marshaled_pinvoke), sizeof(Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5624;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5624 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5625;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5625 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5626;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5626 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5627;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5627 = { sizeof(EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8)+ sizeof(RuntimeObject), sizeof(EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5628;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5628 = { sizeof(WindowFunction_t0067B6F174FD5BEC3E869A38C2319BA8EE85D550), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5629;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5629 = { sizeof(GUI_tA9CDB3D69DB13D51AD83ABDB587EF95947EC2D2A), -1, sizeof(GUI_tA9CDB3D69DB13D51AD83ABDB587EF95947EC2D2A_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5630;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5630 = { sizeof(ParentClipScope_tDAB1300C623213518730D926A970098BECFD9C52)+ sizeof(RuntimeObject), sizeof(ParentClipScope_tDAB1300C623213518730D926A970098BECFD9C52_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5631;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5631 = { sizeof(GUIClip_t6049AB1B245065014011639ADCF204EE3668221B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5632;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5632 = { sizeof(GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2), -1, sizeof(GUIContent_t15E48D4BEB1E6B6044F7DEB5E350800F511C2ED2_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5633;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5633 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5634;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5634 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5635;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5635 = { sizeof(GUILayout_tB26F0D6938B9B2AD04633B1DF56A1E52F3E6D177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5636;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5636 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5637;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5637 = { sizeof(GUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5638;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5638 = { sizeof(LayoutCache_tF844B2FAD6933B78FD5EFEBDE0529BCBAC19BA60), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5639;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5639 = { sizeof(GUILayoutUtility_t48D00CD11CFC1E09E8EC2E51D59E735F5D24836F), -1, sizeof(GUILayoutUtility_t48D00CD11CFC1E09E8EC2E51D59E735F5D24836F_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5640;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5640 = { sizeof(GUISettings_tF2CA7E8B9F62F1FC013BFF053B5FA2709EBA3847), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5641;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5641 = { sizeof(SkinChangedDelegate_tA6D456E853D58AD2EF8A599F543C7E5BA8E94B98), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5642;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5642 = { sizeof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9), -1, sizeof(GUISkin_t8C65CE1424D4B5D8D73022E266BDAD3BDE8612D9_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5643;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5643 = { sizeof(GUIStyleState_t7A948723D9DCDFD8EE4F418B6EC909C18E023F95), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5644;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5644 = { sizeof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580), -1, sizeof(GUIStyle_t20BA2F9F3FE9D13AAA607EEEBE5547835A6F6580_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5645;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5645 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5646;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5646 = { sizeof(GUITargetAttribute_t3F08CE7E00D79CB555B94AA7FA1BCB4B144B922B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5647;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5647 = { sizeof(GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A), -1, sizeof(GUIUtility_tA20863F7281628086EFC61CF90CB52D20E1FD76A_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5648;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5648 = { sizeof(ExitGUIException_tFF2EEEBACD9E5684D6112478EEF754B74D154549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5649;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5649 = { sizeof(GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F), -1, sizeof(GUILayoutEntry_tDF59F19DD000820F64B356D5092C4BEDFE109D5F_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5650;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5650 = { sizeof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D), -1, sizeof(GUILayoutGroup_tD08496E80F283C290B5B90D7BFB3C9C7CC33CD8D_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5651;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5651 = { sizeof(GUIScrollGroup_t4D7230655A7D01ED9BD95916958E34AF09B21FE5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5652;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5652 = { sizeof(ObjectGUIState_t7BE88DC8B9C7187A77D63BBCBE9DB7B674863C15), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5653;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5653 = { sizeof(ScrollViewState_t004FCCBFB6795BD76582385D6D308D8F9ECF41B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5654;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5654 = { sizeof(SliderState_t7BBFAEF918BAA1EE6116C3979993E4EC7DC54FC8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5655;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5655 = { sizeof(uint8_t)+ sizeof(RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5656;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5656 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5657;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5657 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5658;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5658 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5659;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5659 = { sizeof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27), -1, sizeof(TextEditor_t45128DFCB4C388BF7B8153693C9342D5D2358B27_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5660;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5660 = { sizeof(U3CPrivateImplementationDetailsU3E_tCA0A4120E1B13462A402E739CE2DD9CA72BAC713), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5661;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5661 = { sizeof(U3CModuleU3E_tFD9C433EC5744AFEE387384BF931F3B170BC806B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5662;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5662 = { sizeof(WeightInfo_t9942B0D2C77A00A5C9824732AEAA0AB0A55620B0)+ sizeof(RuntimeObject), sizeof(WeightInfo_t9942B0D2C77A00A5C9824732AEAA0AB0A55620B0), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5663;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5663 = { sizeof(AnimationOutputWeightProcessor_tA79B0F7A640A2BE35E2B5DD009E84CFB293E1EFE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5664;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5664 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5665;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5665 = { sizeof(AnimationPlayableAssetUpgrade_t03DC883418FF5A1A5EFF90307DE53090C17F94F7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5666;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5666 = { sizeof(U3Cget_outputsU3Ed__45_tA1B097E5833BFEF217CC8402D897A89E32BF2928), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5667;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5667 = { sizeof(AnimationPlayableAsset_t381BF46F8FEBECFD107EC61C257C60D9438FDF33), -1, sizeof(AnimationPlayableAsset_t381BF46F8FEBECFD107EC61C257C60D9438FDF33_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5668;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5668 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5669;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5669 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5670;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5670 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5671;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5671 = { sizeof(MatchTargetFieldConstants_t07C2FAD424736E6B40B0764A78AC93A64983D89B), -1, sizeof(MatchTargetFieldConstants_t07C2FAD424736E6B40B0764A78AC93A64983D89B_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5672;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5672 = { sizeof(AnimationTrackUpgrade_t7D42F92CD765334066B52C341F5EEF005BB800F0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5673;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5673 = { sizeof(U3Cget_outputsU3Ed__49_t590B0A0FEE820B5A8A1F548908337BBCD7AF6A21), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5674;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5674 = { sizeof(AnimationTrack_tBE37C239976A84F0A2E6BA3B77263B4F44BB1359), -1, sizeof(AnimationTrack_tBE37C239976A84F0A2E6BA3B77263B4F44BB1359_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5675;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5675 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5676;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5676 = { sizeof(TimelineClipUpgrade_t69B67BF242D4B5D087908D2D3726619897DAA620), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5677;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5677 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5678;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5678 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5679;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5679 = { sizeof(TimelineClip_t003008F08E56A75F3A47FD9ADE7C066988A3371D), -1, sizeof(TimelineClip_t003008F08E56A75F3A47FD9ADE7C066988A3371D_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5680;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5680 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5681;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5681 = { sizeof(EditorSettings_t3A8D02A80F57944B75911D9692FECDF6B7081DFF), -1, sizeof(EditorSettings_t3A8D02A80F57944B75911D9692FECDF6B7081DFF_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5682;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5682 = { sizeof(U3Cget_outputsU3Ed__27_tC9898A06DC21CA0BE6B27E1948531195AEBAD3C5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5683;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5683 = { sizeof(TimelineAsset_tE400C944B07CA9D1349BAD84545E24075ADB3496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5684;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5684 = { sizeof(TransientBuildData_t3BE8EF6B5113561AEE7D53FDF3DB331D39BE194F)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5685;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5685 = { sizeof(U3Cget_outputsU3Ed__65_tE61187F3860B492601D3B49624D0E518A1D10F46), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5686;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5686 = { sizeof(U3CU3Ec_t893DC1C51582BE5AFCDF9E5AE64A900EB53B344A), -1, sizeof(U3CU3Ec_t893DC1C51582BE5AFCDF9E5AE64A900EB53B344A_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5687;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5687 = { sizeof(TrackAsset_t31E19BE900C90F6616C0D337652C8614CD833B96), -1, sizeof(TrackAsset_t31E19BE900C90F6616C0D337652C8614CD833B96_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5688;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5688 = { sizeof(TrackColorAttribute_t7A7D452B7CD382F0F9BE71B0CDECE67BD6BF532C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5689;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5689 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5690;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5690 = { sizeof(TimelineClipCapsExtensions_tE7B52704E1D67E0F7DC021AA8AB9D9B84557A618), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5691;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5691 = { sizeof(DiscreteTime_t1598D60B0B2432F702E2A6120D04369EE54600A6)+ sizeof(RuntimeObject), sizeof(DiscreteTime_t1598D60B0B2432F702E2A6120D04369EE54600A6), sizeof(DiscreteTime_t1598D60B0B2432F702E2A6120D04369EE54600A6_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5692;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5692 = { sizeof(InfiniteRuntimeClip_t4421729DA047CB7D239F88EBD2B8103D06686D79), -1, sizeof(InfiniteRuntimeClip_t4421729DA047CB7D239F88EBD2B8103D06686D79_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5693;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5693 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5694;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5694 = { sizeof(IntervalTreeNode_tDAA7D63276D62CD178C91CC7DF932C97896332EC)+ sizeof(RuntimeObject), sizeof(IntervalTreeNode_tDAA7D63276D62CD178C91CC7DF932C97896332EC), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5695;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5695 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5696;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5696 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5697;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5697 = { sizeof(RuntimeClip_t60AA28A7FF08E7EB8A2F524A979CEAC3F32A00AC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5698;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5698 = { sizeof(RuntimeClipBase_t7F5FBDC61E8219FDDF0F9DEB7B6E3864BC57FDD7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5699;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5699 = { sizeof(RuntimeElement_tB6282BAA538AFD75E138BED88A14DFA2665737B2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5700;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5700 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5701;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5701 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5702;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5702 = { sizeof(MarkerList_tD4B632EBA98CE678EB8D108A1AF559F734FA7698)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5703;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5703 = { sizeof(MarkerTrack_tE18594CE52CCC412606B1B5A147DD3A4F7D056C2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5704;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5704 = { sizeof(GroupTrack_tF8FE0FA9A74536ECFBF0C328194D206A7CAA5A5D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5705;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5705 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5706;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5706 = { sizeof(int16_t)+ sizeof(RuntimeObject), sizeof(int16_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5707;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5707 = { sizeof(NotificationEntry_tBBA39A8ACD63E90360DB0FFC4835E8702DFC2E62)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5708;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5708 = { sizeof(U3CU3Ec_t008379627E11F153BBF661B1F74241272206C3D2), -1, sizeof(U3CU3Ec_t008379627E11F153BBF661B1F74241272206C3D2_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5709;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5709 = { sizeof(TimeNotificationBehaviour_tE17AD7EF43D3F48E5BB2C35250F82AB5F9B47535), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5710;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5710 = { sizeof(PlayableTrack_t26BA2795391A45BA8C114EAF95EDCACC2EC25B8F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5711;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5711 = { sizeof(TrackClipTypeAttribute_tCB240AD2A704583CF65CFEEBC7B9F71A1148B6E7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5712;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5712 = { sizeof(NotKeyableAttribute_tB601DB1128DFF4962D64BC486C0637230E819C14), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5713;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5713 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5714;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5714 = { sizeof(TrackBindingTypeAttribute_t7A2B3C3F8994B073A3354E00CAEF06FC1D7721E2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5715;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5715 = { sizeof(SupportsChildTracksAttribute_tAA48787F5A6F2DDBC4FFB428C54C57CA21EF30E8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5716;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5716 = { sizeof(IgnoreOnPlayableTrackAttribute_tD17B1A4900010C52505414888355F8BCA3FC6049), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5717;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5717 = { sizeof(FrameRateFieldAttribute_t14E8B29573FB61BADE0F2AE9ED93C60AF16FBC07), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5718;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5718 = { sizeof(HideInMenuAttribute_t8F5AEDC2C84031EBD6C3F7A2C2B39DC4115B9DEC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5719;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5719 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5720;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5720 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5721;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5721 = { sizeof(TimelinePlayable_t01A93A4FCD1222A9B8A7BE65E5FEB187D8D99BBD), -1, sizeof(TimelinePlayable_t01A93A4FCD1222A9B8A7BE65E5FEB187D8D99BBD_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5722;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5722 = { sizeof(U3CU3Ec_tB74E998116FFDE2919C1EF6E8B35AB3798461A28), -1, sizeof(U3CU3Ec_tB74E998116FFDE2919C1EF6E8B35AB3798461A28_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5723;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5723 = { sizeof(Extrapolation_t4258D23965C0DA9509062D44F7A2AACA99A59DFE), -1, sizeof(Extrapolation_t4258D23965C0DA9509062D44F7A2AACA99A59DFE_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5724;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5724 = { sizeof(HashUtility_tE6FFABA5BA93FFF277F35828FFBC704E3CC1910D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5725;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5725 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5726;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5726 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5727;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5727 = { sizeof(NotificationUtilities_t9B189DA39E0036BAC0636501BB2C82CF7F20C3CD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5728;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5728 = { sizeof(U3CU3Ec__DisplayClass0_0_tEEFDBD8392308CD42F270E5772842108CCBA2AD6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5729;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5729 = { sizeof(U3CU3Ec__DisplayClass0_1_t6C3CFF368A6F745558E6345BDEEF00CC8B033591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5730;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5730 = { sizeof(TimelineCreateUtilities_tA54637927CEAAC7695569051E983F9062A303E0D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5731;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5731 = { sizeof(TimelineUndo_tDC9CE9EF28D32B1EF1CE8D0A7D5948C637B78B61), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5732;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5732 = { sizeof(TimeUtility_t815D4EA395D12E4F662C6AB493F3CE5A0006AD26), -1, sizeof(TimeUtility_t815D4EA395D12E4F662C6AB493F3CE5A0006AD26_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5733;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5733 = { sizeof(WeightUtility_tB748A8522E941F6EC55488967F280E34D4D4E410), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5734;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5734 = { sizeof(U3CModuleU3E_t853A105E2E1595E463CC860AFEE0FB13A177A12C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5735;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5735 = { sizeof(U3CModuleU3E_tF062866229C4952B8051AD32AB6E9D931142CC95), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5736;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5736 = { sizeof(cy_t1F5EB8CC9D026AD38C320A7D19A1E07CDCE8F4C8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5737;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5737 = { sizeof(cz_t85A5FCD7FAA576091E8B4BFBD57625052E1B917D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5738;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5738 = { sizeof(da_t3FC532477FC26B8C8B109F722ED5DD96CA6B97A4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5739;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5739 = { sizeof(DOTweenModuleAudio_tCF19013F0C086697BB9D373975D877E26BD2CE09), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5740;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5740 = { sizeof(db_t04FA8C1C83849880285627B0AEB66AA6254AC896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5741;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5741 = { sizeof(dc_t8560375365CB794B1B1EB7D8112D328CB79166C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5742;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5742 = { sizeof(dd_tC04F9BEFD21B87A979BB1BF151883FC26DBA2079), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5743;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5743 = { sizeof(de_t1957E3969214286A4AAF6BD98BF61899A2EFAEE7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5744;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5744 = { sizeof(df_tDE0CD18A0ACFB761CF7EBB92C73EE6CCC8C46E03), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5745;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5745 = { sizeof(dg_t74AFC493747D3773884E56C4134EFBB4395889D2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5746;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5746 = { sizeof(dh_t39B924AB8F118682E4493B650D6FBF5B8D0F9EE7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5747;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5747 = { sizeof(di_t14426C7A9EA6883BB2AB39FBA5D1FD29A09B0D34), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5748;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5748 = { sizeof(dj_tD6C8ED0CAB3386298320D2B1EB661292B31BA050), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5749;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5749 = { sizeof(dk_t91215D2D2D582240CE8B8C76634C7F17546E9F8C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5750;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5750 = { sizeof(dl_t006ECC3270CE6AAA83F3E5944E2CEBAF7DD6C029), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5751;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5751 = { sizeof(DOTweenModulePhysics_t288D427B27A061E96FAD8BB322D3640CA9EC1C14), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5752;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5752 = { sizeof(dm_t0EE8E4DE2AF72F874B566797C004695394DBD262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5753;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5753 = { sizeof(dn_tEF51A22D307B0EE60DE9E282A8BF4C2D65CC2936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5754;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5754 = { sizeof(do_t76311BB32294D3739B4D88A6C988945DC5E8E1D5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5755;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5755 = { sizeof(dp_tCCE1A32DE0623F9BAB888D6174CC9CCE901E4E1B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5756;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5756 = { sizeof(dq_t56D790A1C052919142A84179CA25C65BBCCE0DD7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5757;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5757 = { sizeof(dr_t08A6E7547D64506AB0E3E9640BEAAC9BA0412603), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5758;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5758 = { sizeof(ds_t5AF5C01D4BFDAACE64367BAD31E184B019D352AE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5759;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5759 = { sizeof(dt_tBC8E4BE4DB21DACC0A81470D7B1CFA10561D6708), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5760;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5760 = { sizeof(du_t9C8F1B18CE5A2B107C056E597C78AA21A5434FB2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5761;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5761 = { sizeof(DOTweenModulePhysics2D_t9F08692BD09A9DC4646782951FD80D8A529905F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5762;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5762 = { sizeof(dv_t9E694C9B76B01D9846D951BE9A564F3E8F6E1516), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5763;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5763 = { sizeof(dw_tC4D9A5FEC7AD046EF807C3556A574E383CEB910A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5764;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5764 = { sizeof(dx_tF1865E98745F606AB4614C0666E92C7BB46B48B4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5765;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5765 = { sizeof(DOTweenModuleSprite_tF147E9559811CF3DC449BEEBFF0EF43A5DB9BCD7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5766;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5766 = { sizeof(Utils_t3635A7E5B7FAC073A571339320CB18B71E25ABD8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5767;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5767 = { sizeof(dy_t1B3448FA1815934EFB53E0016E8DEC994A5A3F43), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5768;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5768 = { sizeof(dz_tCD2E9ADEA68AAAE78D012D85BAF1E26DBCE1C013), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5769;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5769 = { sizeof(ea_t6AF4F7D4D540E9E527BBEF98DE636F94FDFE4EF5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5770;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5770 = { sizeof(eb_t498E9E971601541D493F2B143425AA1F3698C0E6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5771;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5771 = { sizeof(ec_t89E50EEB2476BAAAFBE7F85CA814453569BAEBAD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5772;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5772 = { sizeof(ed_t9B95C38962839D948422357292F158321AB448F7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5773;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5773 = { sizeof(ee_tA8F87539F6BACC3848548656974C06447BF67223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5774;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5774 = { sizeof(ef_t4E8CB78C298D36AADD1937E072FC6EE278481E63), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5775;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5775 = { sizeof(eg_t65BC07AE5FEB82B0D5A6EE2E611C09EAFD4ED670), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5776;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5776 = { sizeof(eh_t4FF1C1056500F999D804DD2BEF86B3031CF3C99C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5777;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5777 = { sizeof(ei_t919D83BA42C49173A18766EA39B67C3475E87882), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5778;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5778 = { sizeof(ej_t18DAE8A30B51C3D34DB0D5964798929EB157C69E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5779;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5779 = { sizeof(ek_tC37AA180EDF0809193B38B3D0F3F18D34218D826), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5780;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5780 = { sizeof(el_t0758C333352A74222C7B402D194635113221F29A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5781;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5781 = { sizeof(em_t91CA10C9D401DCC0190BF122CA9F8EEF4226062A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5782;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5782 = { sizeof(en_tCA269384E666E2437B547C86A2E0970E16308DD0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5783;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5783 = { sizeof(eo_t8875602888E0CF53F1EED6B2E926D1FCE27DE675), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5784;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5784 = { sizeof(ep_t7791CE01C7D9FA2C28622B377FF67F39E0519164), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5785;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5785 = { sizeof(eq_tE0C83491E29F03693C9EC2186D11F157085F7937), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5786;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5786 = { sizeof(er_tDDD5288BB2DE0469770BBCB2F90A03BCCB8ADD57), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5787;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5787 = { sizeof(es_t90FDC1C658DF920F5BFD9CE46FE88B1D53C01A24), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5788;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5788 = { sizeof(et_tD92CD2DF5BE38E15AE6FDC4755C577DC0CBE8CED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5789;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5789 = { sizeof(eu_t4FC045D2551FCCAD17DEC212F2674FFD75A8897A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5790;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5790 = { sizeof(ev_tD9C81DAA29A760A09241CA2E3C38AFA2C06525A8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5791;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5791 = { sizeof(ew_t18930CB32FD7F8692597DCECD783AC3E84AD8261), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5792;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5792 = { sizeof(ex_tC12B7079FD9F7105096B33C00E1CF128475F8E9E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5793;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5793 = { sizeof(ey_tC4291BD4B8DCE22D43E92D20815C5DA73742AA6D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5794;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5794 = { sizeof(ez_t0A5814C37E1C2B25CD984443BFDB76E68575C728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5795;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5795 = { sizeof(fa_tE9B1832901B936CFA7EBAEA5B94E8A5F2C77BD5D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5796;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5796 = { sizeof(fb_tB4BE092DF664B4D9040B9317EF21AA57490F0BD5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5797;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5797 = { sizeof(fc_tD2FB882F61BF34D867EB605780126FD5C2F22F0B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5798;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5798 = { sizeof(fd_tDA5E47505A25D3A62E1B713157CF0F03EBE72B89), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5799;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5799 = { sizeof(fe_tC5A76D8DF980C583FCD405FFBE38B890744CBB17), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5800;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5800 = { sizeof(ff_tA8CB63F594A70D0B4F555E8527ACEDED35E01430), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5801;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5801 = { sizeof(fg_tC31619D49CF72755A4AA12ABACF4DAE7FE5842FB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5802;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5802 = { sizeof(fh_tB10B99DBBF95F5138B3D2270FE4365F8119776B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5803;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5803 = { sizeof(fi_t649FEB4657A71F350E6BF0A93C5A6C934D1A9CE5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5804;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5804 = { sizeof(fj_tD4EA578918F20DF40CC055CE3290963BF22105A8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5805;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5805 = { sizeof(fk_t0B3E21AA1C3FBA9811CFEFC4AB67465CC86610A2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5806;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5806 = { sizeof(fl_t1487064AB5A0B201B00A9D9DAE3743391E7F882C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5807;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5807 = { sizeof(fm_t51C9D72D64BCC8261AC65B4321A2D38C142CC78A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5808;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5808 = { sizeof(DOTweenModuleUI_t4988700FDB42C34ACA64236BD53E96D3049D86FE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5809;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5809 = { sizeof(fn_tF788F79558E8A188448411AA4B2DCD839531E4B0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5810;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5810 = { sizeof(fo_t3C207075334909DF25777631046D3574EB73D523), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5811;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5811 = { sizeof(U3CAsyncWaitForCompletionU3Ed__10_tC84049D47EAD23B14384BDEF646D532785ECBF0E)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5812;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5812 = { sizeof(U3CAsyncWaitForRewindU3Ed__11_tC8D7C20224797881A037D09DA8079ECCC3E518FE)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5813;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5813 = { sizeof(U3CAsyncWaitForKillU3Ed__12_t6EA9E2438625431E39D01AB7EEBB4501D6B5E54E)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5814;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5814 = { sizeof(U3CAsyncWaitForElapsedLoopsU3Ed__13_tC7B431C2393096ACD1A6BA0EAFEA84EE62DAF825)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5815;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5815 = { sizeof(U3CAsyncWaitForPositionU3Ed__14_tA6006769EC53BBEBA0665ECA79096B606FDA8A4A)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5816;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5816 = { sizeof(U3CAsyncWaitForStartU3Ed__15_tB4B1CE199FE822B67BCF87301159986D9D50961B)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5817;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5817 = { sizeof(DOTweenModuleUnityVersion_t028EE91988A2A48FC2EEACA97E9BA03468B748F6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5818;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5818 = { sizeof(WaitForCompletion_tC84400E0FA4E28B95AA56DF28973D5FFDA16AFA0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5819;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5819 = { sizeof(WaitForRewind_t2ABB006386A81D361C36B476044786442726743D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5820;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5820 = { sizeof(WaitForKill_t532BDFE32D7C3892E01BF80054F95A9A5C1C24DE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5821;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5821 = { sizeof(WaitForElapsedLoops_t24C0691B408798B4BE5CCC92DC8B4D40430717BC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5822;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5822 = { sizeof(WaitForPosition_t302B4F4C6FC89426E08DDC65543F45785B20B84B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5823;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5823 = { sizeof(WaitForStart_t4448F8E46F59EE599CA8DCEF52FC706221093F30), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5824;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5824 = { sizeof(DOTweenCYInstruction_tCB7C6C1E61447CF998FF1A85EDB5949ED08C303B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5825;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5825 = { sizeof(Physics_t835AE7329FBFA61BA3A2271950FD3F7F6A18770A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5826;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5826 = { sizeof(DOTweenModuleUtils_t5554865584F951A4A4E5DD282E6EBC60F5CEC6E9), -1, sizeof(DOTweenModuleUtils_t5554865584F951A4A4E5DD282E6EBC60F5CEC6E9_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5827;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5827 = { sizeof(U3CModuleU3E_tA3942657997767549ED3B944EB77AFA183BBF4B9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5828;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5828 = { sizeof(SR_tF79CE2856F7D3AF3AE5D28C5C52C986CAFBE262F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5829;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5829 = { sizeof(Error_tCE0C9D928B2D2CC69DDEC1A0ECF05131938959DB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5830;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5830 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5831;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5831 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5832;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5832 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5833;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5833 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5834;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5834 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5835;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5835 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5836;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5836 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5837;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5837 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5838;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5838 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5839;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5839 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5840;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5840 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5841;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5841 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5842;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5842 = { sizeof(Enumerable_t372195206D92B3F390693F9449282C31FD564C09), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5843;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5843 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5844;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5844 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5845;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5845 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5846;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5846 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5847;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5847 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5848;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5848 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5849;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5849 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5850;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5850 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5851;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5851 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5852;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5852 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5853;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5853 = { sizeof(LambdaExpressionProxy_t19CC47CCDC0090FFF30367A402CD6127C8A580DE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5854;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5854 = { sizeof(MemberExpressionProxy_t6B52A63F5D0C957F9B833F873B05E89527B49484), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5855;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5855 = { sizeof(UnaryExpressionProxy_tA78B15C1B517EF474E3F9AE481010961D2919D23), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5856;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5856 = { sizeof(ExtensionInfo_tCE0B03041E5780D311D03EACAA1F672E4C70C3E7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5857;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5857 = { sizeof(Expression_t70AA908ECBD33E94249BF235E4EBB0F831AD8785), -1, sizeof(Expression_t70AA908ECBD33E94249BF235E4EBB0F831AD8785_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5858;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5858 = { sizeof(Error_t2E8AD85278E48F4074983610DC3BEE8A3F84454B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5859;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5859 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5860;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5860 = { sizeof(LambdaExpression_tD26FB6AEAD01B2EBB668CDEAFAAFA4948697300E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5861;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5861 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5862;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5862 = { sizeof(MemberExpression_t133C12A9CE765EF02D622D660CE80E146B15EF89), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5863;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5863 = { sizeof(Strings_t5E2898117DA2D8EC4D672719556FD56C9A4C6D6B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5864;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5864 = { sizeof(UnaryExpression_tFB4F40A211A2FF9B58F1A86E0EDB474121867B96), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5865;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5865 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5866;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5866 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5867;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5867 = { sizeof(ContractUtils_tFD5BFE68866F22438D49EF2D69CC0BE6FFF726EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5868;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5868 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5869;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5869 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5870;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5870 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5871;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5871 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5872;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5872 = { sizeof(U3CModuleU3E_tCFDAF3CE34E8117DEABC58BB3EBDB7B80EA66F5A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5873;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5873 = { sizeof(BaselineFunction_t13AFADEF52F63320B2159C237635948AEB801679), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5874;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5874 = { sizeof(Logger_t092B1218ED93DD47180692D5761559B2054234A0), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5875;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5875 = { sizeof(MeasureFunction_t60EBED1328F5328D4FA7E26335967E59E73B4D09), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5876;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5876 = { sizeof(MeasureOutput_t6C4FCF151309F81DF23561CF3FF1777445FBD84E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5877;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5877 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5878;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5878 = { sizeof(YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345), -1, sizeof(YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5879;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5879 = { sizeof(YogaConstants_tE52AB48288567AEF285EDE0C8884AFD803AD9D3C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5880;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5880 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5881;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5881 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5882;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5882 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5883;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5883 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5884;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5884 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5885;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5885 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5886;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5886 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5887;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5887 = { sizeof(Native_t97ADC11284398663A27E9214C13A84F868A25614), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5888;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5888 = { sizeof(YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5889;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5889 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5890;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5890 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5891;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5891 = { sizeof(YogaSize_tA276812CB1E90E7AA2028A9474EA6EA46B3B38EA)+ sizeof(RuntimeObject), sizeof(YogaSize_tA276812CB1E90E7AA2028A9474EA6EA46B3B38EA), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5892;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5892 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5893;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5893 = { sizeof(YogaValue_t9066126971BFC18D9B4A8AB11435557F19598F8C)+ sizeof(RuntimeObject), sizeof(YogaValue_t9066126971BFC18D9B4A8AB11435557F19598F8C), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5894;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5894 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5895;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5895 = { sizeof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5896;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5896 = { sizeof(TextVertex_tF56662BA585F7DD34D71971F1AA1D2E767946CF3)+ sizeof(RuntimeObject), sizeof(TextVertex_tF56662BA585F7DD34D71971F1AA1D2E767946CF3), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5897;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5897 = { sizeof(TextNative_t463AA48470CE96DB270F55A6F73EF2D90401C00C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5898;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5898 = { sizeof(UIElementsRuntimeUtilityNative_t9DE2C23158D553BB693212D0D8AEAE8594E75938), -1, sizeof(UIElementsRuntimeUtilityNative_t9DE2C23158D553BB693212D0D8AEAE8594E75938_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5899;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5899 = { sizeof(GfxUpdateBufferRange_tC47258BCB472B0727B4FCE21A2A53506644C1A97)+ sizeof(RuntimeObject), sizeof(GfxUpdateBufferRange_tC47258BCB472B0727B4FCE21A2A53506644C1A97), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5900;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5900 = { sizeof(DrawBufferRange_t684F255F5C954760B12F6689F84E78811040C7A4)+ sizeof(RuntimeObject), sizeof(DrawBufferRange_t684F255F5C954760B12F6689F84E78811040C7A4), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5901;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5901 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5902;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5902 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5903;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5903 = { sizeof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD), -1, sizeof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5904;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5904 = { sizeof(U3CModuleU3E_t8B36B9B16FF72CF5A0EBA03D2FA162E77C86534C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5905;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5905 = { sizeof(SharedBetweenAnimatorsAttribute_t44FFD5D3B5AEBB394182D66E2198FA398087449C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5906;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5906 = { sizeof(StateMachineBehaviour_t59C5685227B06222F5AF7027E2DA530AB99AFDF7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5907;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5907 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5908;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5908 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5909;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5909 = { sizeof(Enumerator_t81434F7D5603121F3D7BD6DB916FE1C755307530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5910;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5910 = { sizeof(Animation_t6593B06C39E3B139808B19F2C719C860F3F61040), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5911;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5911 = { sizeof(AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5912;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5912 = { sizeof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5913;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5913 = { sizeof(AnimationClip_t00BD2F131D308A4AD2C6B0BF66644FC25FECE712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5914;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5914 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5915;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5915 = { sizeof(AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03)+ sizeof(RuntimeObject), sizeof(AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5916;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5916 = { sizeof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2)+ sizeof(RuntimeObject), sizeof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5917;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5917 = { sizeof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD)+ sizeof(RuntimeObject), sizeof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5918;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5918 = { sizeof(Animator_t8A52E42AE54F76681838FE9E632683EF3952E883), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5919;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5919 = { sizeof(OnOverrideControllerDirtyCallback_tDC67F7661A27502AD804BDE0B696955AFD4A44D5), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5920;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5920 = { sizeof(AnimatorOverrideController_tF78BD58B30BB0D767E7A96F8428EA66F2DFD5493), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5921;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5921 = { sizeof(Avatar_t7861E57EEE2CF8CC61BD63C09737BA22F7ABCA0F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5922;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5922 = { sizeof(SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126)+ sizeof(RuntimeObject), sizeof(SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5923;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5923 = { sizeof(HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E)+ sizeof(RuntimeObject), sizeof(HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5924;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5924 = { sizeof(HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8)+ sizeof(RuntimeObject), sizeof(HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5925;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5925 = { sizeof(HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44)+ sizeof(RuntimeObject), sizeof(HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5926;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5926 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5927;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5927 = { sizeof(AvatarMask_tC1D777FFB77C952502ECF6D80FAFAD16B27B02AF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5928;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5928 = { sizeof(Motion_tBCD49FBF5608AD21FC03B63C8182FABCEF2707AC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5929;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5929 = { sizeof(RuntimeAnimatorController_t6F7C753402B42EC23C163099CF935C5E0D7A7254), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5930;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5930 = { sizeof(AnimationPlayableBinding_t44AB912C26E38CBC1F5027F5C96A321C3B1C1612), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5931;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5931 = { sizeof(DiscreteEvaluationAttribute_tF23FCB5AB01B394BF5BD84623364A965C90F8BB9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5932;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5932 = { sizeof(NotKeyableAttribute_tDDB6B25B26F649E3CED893EE1E63B6DE66844483), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5933;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5933 = { sizeof(AnimationClipPlayable_t54CEA0DD315B1674C2BD49E681005C4271D73969)+ sizeof(RuntimeObject), sizeof(AnimationClipPlayable_t54CEA0DD315B1674C2BD49E681005C4271D73969), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5934;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5934 = { sizeof(AnimationHumanStream_t31E8EAD3F7C2C29CAE7B4EFB87AA84ECC6DCC6EC)+ sizeof(RuntimeObject), sizeof(AnimationHumanStream_t31E8EAD3F7C2C29CAE7B4EFB87AA84ECC6DCC6EC), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5935;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5935 = { sizeof(AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D)+ sizeof(RuntimeObject), sizeof(AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D), sizeof(AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5936;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5936 = { sizeof(AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0)+ sizeof(RuntimeObject), sizeof(AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0), sizeof(AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5937;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5937 = { sizeof(AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18)+ sizeof(RuntimeObject), sizeof(AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18), sizeof(AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5938;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5938 = { sizeof(AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4)+ sizeof(RuntimeObject), sizeof(AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4), sizeof(AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5939;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5939 = { sizeof(AnimationPlayableExtensions_t3F93F70729DACA99F423992CFC1B380C1942AD4D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5940;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5940 = { sizeof(AnimationPlayableGraphExtensions_tF833C072961F30409DB4D2A1B8B1B5BAE53221B3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5941;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5941 = { sizeof(AnimationPlayableOutput_t753AC95DC826789BC537D18449E93114777DDF4E)+ sizeof(RuntimeObject), sizeof(AnimationPlayableOutput_t753AC95DC826789BC537D18449E93114777DDF4E), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5942;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5942 = { sizeof(AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C)+ sizeof(RuntimeObject), sizeof(AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C), sizeof(AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5943;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5943 = { sizeof(AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD)+ sizeof(RuntimeObject), sizeof(AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD), sizeof(AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5944;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5944 = { sizeof(AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127)+ sizeof(RuntimeObject), sizeof(AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127), sizeof(AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5945;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5945 = { sizeof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A)+ sizeof(RuntimeObject), sizeof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5946;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5946 = { sizeof(AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A)+ sizeof(RuntimeObject), sizeof(AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A), sizeof(AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5947;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5947 = { sizeof(U3CModuleU3E_t3B74AF9E7E84B3C57D4687184E31363228069DF2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5948;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5948 = { sizeof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756)+ sizeof(RuntimeObject), sizeof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5949;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5949 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5950;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5950 = { sizeof(GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D)+ sizeof(RuntimeObject), sizeof(GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D), sizeof(GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5951;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5951 = { sizeof(GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A)+ sizeof(RuntimeObject), sizeof(GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5952;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5952 = { sizeof(Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F), sizeof(Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5953;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5953 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5954;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5954 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5955;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5955 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5956;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5956 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5957;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5957 = { sizeof(FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172)+ sizeof(RuntimeObject), sizeof(FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5958;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5958 = { sizeof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A), -1, sizeof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5959;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5959 = { sizeof(FontEngineUtilities_t08D8707F6F929B42407961E303FD339A793E5BBB)+ sizeof(RuntimeObject), sizeof(FontEngineUtilities_t08D8707F6F929B42407961E303FD339A793E5BBB), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5960;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5960 = { sizeof(GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C)+ sizeof(RuntimeObject), sizeof(GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5961;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5961 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5962;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5962 = { sizeof(GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E)+ sizeof(RuntimeObject), sizeof(GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5963;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5963 = { sizeof(GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7)+ sizeof(RuntimeObject), sizeof(GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5964;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5964 = { sizeof(GlyphPairAdjustmentRecord_t6E4295094D349DBF22BC59116FBC8F22EA55420E)+ sizeof(RuntimeObject), sizeof(GlyphPairAdjustmentRecord_t6E4295094D349DBF22BC59116FBC8F22EA55420E), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5965;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5965 = { sizeof(U3CModuleU3E_t48670A4A557D138A8C338D0B799AEE0AB575434C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5966;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5966 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5967;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5967 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5968;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5968 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5969;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5969 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5970;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5970 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5971;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5971 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5972;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5972 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5973;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5973 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5974;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5974 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5975;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5975 = { sizeof(Range_tDDBAD7CBDC5DD273DA4330F4E9CDF24C62F16ED6)+ sizeof(RuntimeObject), sizeof(Range_tDDBAD7CBDC5DD273DA4330F4E9CDF24C62F16ED6), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5976;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5976 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5977;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5977 = { sizeof(LocalUser_t55C68E98993F86B6FBB7A25F28EB989CD7E6A3AD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5978;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5978 = { sizeof(UserProfile_t3EF35349E23201EF9F3C5956C44384FA45C1EF29), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5979;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5979 = { sizeof(Achievement_t723EE724DCEBFAE9555CDC909FDA84F71EB5719B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5980;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5980 = { sizeof(AchievementDescription_t0D2306DF6EE55C872DB06E6855D7B1AE0E6DDEF9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5981;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5981 = { sizeof(Score_t9ED78BAAA0A342F85A3473CCF95CE31E6BF03D53), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5982;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5982 = { sizeof(Leaderboard_tBDB34CC6F79318BE6D7761015C70C8A5CC64EC71), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5983;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5983 = { sizeof(GcUserProfileData_tCDEF4010D44CB370077CE47055C89CD9E808A535)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5984;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5984 = { sizeof(GcAchievementDescriptionData_t9C5BBAB764F0088FE40698EB33FE79D5173B2086)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5985;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5985 = { sizeof(GcAchievementData_tBA953A0889E78C0050ED548758B7AF25F12799D4)+ sizeof(RuntimeObject), sizeof(GcAchievementData_tBA953A0889E78C0050ED548758B7AF25F12799D4_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5986;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5986 = { sizeof(GcScoreData_t90ADB5BBE4EF7B4B1E0503E9E0934EA2ED254F0F)+ sizeof(RuntimeObject), sizeof(GcScoreData_t90ADB5BBE4EF7B4B1E0503E9E0934EA2ED254F0F_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5987;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5987 = { sizeof(U3CU3Ec__DisplayClass21_0_t9AE48FD34643B3E83D69BD45882795E81C13E0A1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5988;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5988 = { sizeof(GameCenterPlatform_tF95E5DA7B46424313E1812B6B80DE102ABB1F96F), -1, sizeof(GameCenterPlatform_tF95E5DA7B46424313E1812B6B80DE102ABB1F96F_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5989;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5989 = { sizeof(GcLeaderboard_t4C8666E37C725723182101E6F36DFAB20D581E9D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5990;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5990 = { sizeof(U3CModuleU3E_t98AB86DBD4B6990BFAA7E2230BDA19430302AC99), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5991;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5991 = { sizeof(EmbeddedAttribute_tE12941E90BE7D41A61A8FF0507EC86D12F1C2ACC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5992;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5992 = { sizeof(IsReadOnlyAttribute_t2C6085B49D0BEB030B26D149806E40189CACCA6F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5993;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5993 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5994;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5994 = { sizeof(InputTracking_tA4F34D4D5EC8E560B56ED295177C040D9C9815F1), -1, sizeof(InputTracking_tA4F34D4D5EC8E560B56ED295177C040D9C9815F1_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5995;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5995 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5996;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5996 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5997;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5997 = { sizeof(XRNodeState_t683158812A1D80A6BC73DB97405BB0B795A9111A)+ sizeof(RuntimeObject), sizeof(XRNodeState_t683158812A1D80A6BC73DB97405BB0B795A9111A), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5998;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5998 = { sizeof(uint32_t)+ sizeof(RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5999;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5999 = { sizeof(uint32_t)+ sizeof(RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6000;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6000 = { sizeof(uint32_t)+ sizeof(RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6001;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6001 = { sizeof(uint32_t)+ sizeof(RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6002;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6002 = { sizeof(InputFeatureUsage_t1E251DC4F8CD697080F0F5D98388955AF8B87599)+ sizeof(RuntimeObject), sizeof(InputFeatureUsage_t1E251DC4F8CD697080F0F5D98388955AF8B87599_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6003;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6003 = { sizeof(InputDevice_t882EE3EE8A71D8F5F38BA3F9356A49F24510E8BD)+ sizeof(RuntimeObject), sizeof(InputDevice_t882EE3EE8A71D8F5F38BA3F9356A49F24510E8BD_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6004;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6004 = { sizeof(Hand_t67B90BC0D36CBC92DF7E38BD15463B925CB5912C)+ sizeof(RuntimeObject), sizeof(Hand_t67B90BC0D36CBC92DF7E38BD15463B925CB5912C), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6005;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6005 = { sizeof(Eyes_t9FD3821575977E294F11E0EB92D2A4CA509ED8C7)+ sizeof(RuntimeObject), sizeof(Eyes_t9FD3821575977E294F11E0EB92D2A4CA509ED8C7), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6006;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6006 = { sizeof(Bone_t2558B1DD1E4F405EA4D76A3B8D5149CA16011975)+ sizeof(RuntimeObject), sizeof(Bone_t2558B1DD1E4F405EA4D76A3B8D5149CA16011975), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6007;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6007 = { sizeof(InputDevices_t02B79FC19CEA9AC29A9945F5CDA6D790730FBF34), sizeof(InputDevices_t02B79FC19CEA9AC29A9945F5CDA6D790730FBF34_marshaled_pinvoke), sizeof(InputDevices_t02B79FC19CEA9AC29A9945F5CDA6D790730FBF34_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6008;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6008 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6009;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6009 = { sizeof(XRRenderParameter_t0C786C9DBEFFCAD6204039BD181B412B69F95260)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6010;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6010 = { sizeof(XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8)+ sizeof(RuntimeObject), sizeof(XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6011;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6011 = { sizeof(XRBlitParams_tB6F9130166CF43540F2547163E8542CFC7266CD9)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6012;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6012 = { sizeof(XRMirrorViewBlitDesc_tC913B2856BA2160BC3AD99F0C67293850E2162E0)+ sizeof(RuntimeObject), sizeof(XRMirrorViewBlitDesc_tC913B2856BA2160BC3AD99F0C67293850E2162E0_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6013;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6013 = { sizeof(XRDisplaySubsystem_t4B00B0BF1894A039ACFA8DDC2C2EB9301118C1F1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6014;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6014 = { sizeof(XRDisplaySubsystemDescriptor_t72DD88EE9094488AE723A495F48884BA4EA8311A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6015;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6015 = { sizeof(XRInputSubsystem_tFECE6683FCAEBF05BAD05E5D612690095D8BAD34), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6016;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6016 = { sizeof(XRInputSubsystemDescriptor_t42088DD6542C0BDD27C2951B911E4F69DD1F917D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6017;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6017 = { sizeof(MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC)+ sizeof(RuntimeObject), sizeof(MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC), sizeof(MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6018;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6018 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6019;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6019 = { sizeof(HashCodeHelper_tC1D27B890F89E7B1158911DDBE91E869D2087387), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6020;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6020 = { sizeof(MeshGenerationResult_tD5A6D639B2CF1A3F855AFB41861DEC48DC0D3A9C)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6021;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6021 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6022;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6022 = { sizeof(MeshTransformList_t761D725D4B30CFD7DDF57B3725004994FB3B561F)+ sizeof(RuntimeObject), sizeof(MeshTransformList_t761D725D4B30CFD7DDF57B3725004994FB3B561F), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6023;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6023 = { sizeof(XRMeshSubsystem_tDDC31EC10D4F0517542F9EB296428A0F7EC2C3B2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6024;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6024 = { sizeof(XRMeshSubsystemDescriptor_tD9814661B8661C69D5A0DBB76C9AF61778B9CEC1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6025;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6025 = { sizeof(U3CModuleU3E_t42F165DEA2597BD5AB2C914FCF80349ECF878162), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6026;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6026 = { sizeof(WebRequestUtils_t23F1FB533DBFDA3BE5624D901D535B4C6EFAD443), -1, sizeof(WebRequestUtils_t23F1FB533DBFDA3BE5624D901D535B4C6EFAD443_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6027;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6027 = { sizeof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045), -1, sizeof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6028;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6028 = { sizeof(WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B), -1, sizeof(WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6029;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6029 = { sizeof(UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6030;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6030 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6031;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6031 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6032;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6032 = { sizeof(UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6033;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6033 = { sizeof(CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804), sizeof(CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6034;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6034 = { sizeof(DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB), sizeof(DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6035;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6035 = { sizeof(DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974), sizeof(DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6036;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6036 = { sizeof(UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6), sizeof(UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6037;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6037 = { sizeof(U3CModuleU3E_tCFCF033B61CFCC76C69180CF9A7B07EED67725EA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6038;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6038 = { sizeof(ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6039;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6039 = { sizeof(Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6040;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6040 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6041;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6041 = { sizeof(Physics_t1244C2983AEAFA149425AFFC3DF53BC91C18ED56), -1, sizeof(Physics_t1244C2983AEAFA149425AFFC3DF53BC91C18ED56_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6042;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6042 = { sizeof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960)+ sizeof(RuntimeObject), sizeof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6043;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6043 = { sizeof(RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5)+ sizeof(RuntimeObject), sizeof(RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6044;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6044 = { sizeof(Rigidbody_t268697F5A994213ED97393309870968BC1C7393C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6045;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6045 = { sizeof(Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6046;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6046 = { sizeof(CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6047;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6047 = { sizeof(MeshCollider_tB525E4DDE383252364ED0BDD32CF2B53914EE455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6048;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6048 = { sizeof(CapsuleCollider_t3A1671C74F0836ABEF5D01A7470B5B2BE290A808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6049;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6049 = { sizeof(BoxCollider_tFA5D239388334D6DE0B8FFDAD6825C5B03786E23), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6050;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6050 = { sizeof(SphereCollider_tBA111C542CE97F6873DE742757213D6265C7D275), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6051;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6051 = { sizeof(ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9)+ sizeof(RuntimeObject), sizeof(ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6052;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6052 = { sizeof(PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE)+ sizeof(RuntimeObject), sizeof(PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6053;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6053 = { sizeof(U3CModuleU3E_t9A99FA938A0D410D25805C4E4D63978454163723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6054;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6054 = { sizeof(Il2CppEagerStaticClassConstructionAttribute_t01F60DE85CC3427802A892466A27D5245AEA1BAF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6055;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6055 = { sizeof(IntFloatUnion_t549256A9DD754252DD18383D9CE7EA55EBBD6D96)+ sizeof(RuntimeObject), sizeof(IntFloatUnion_t549256A9DD754252DD18383D9CE7EA55EBBD6D96), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6056;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6056 = { sizeof(math_t084A7021C0897F87DB209393BE7FBDFD6A7FBFA6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6057;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6057 = { sizeof(DebuggerProxy_t2F3C70F890B53139614C348B6F97B31367817B27), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6058;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6058 = { sizeof(float2_t24AA5C0F612B0672315EDAFEC9D9E7F1C4A5B0BA)+ sizeof(RuntimeObject), sizeof(float2_t24AA5C0F612B0672315EDAFEC9D9E7F1C4A5B0BA), sizeof(float2_t24AA5C0F612B0672315EDAFEC9D9E7F1C4A5B0BA_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6059;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6059 = { sizeof(DebuggerProxy_t7ADF52C52717A46ECA0C565057AE4DE54B2C69B8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6060;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6060 = { sizeof(float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E)+ sizeof(RuntimeObject), sizeof(float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E), sizeof(float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6061;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6061 = { sizeof(float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79)+ sizeof(RuntimeObject), sizeof(float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79), sizeof(float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6062;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6062 = { sizeof(DebuggerProxy_tD5F63FE661BF3F780BEFBE721E36E7A87B310121), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6063;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6063 = { sizeof(float4_t89D9A294E7A79BD81BFBDD18654508532958555E)+ sizeof(RuntimeObject), sizeof(float4_t89D9A294E7A79BD81BFBDD18654508532958555E), sizeof(float4_t89D9A294E7A79BD81BFBDD18654508532958555E_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6064;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6064 = { sizeof(float4x4_t7EDD16F7F57DC7F61A6302535F7C19FB97915DF2)+ sizeof(RuntimeObject), sizeof(float4x4_t7EDD16F7F57DC7F61A6302535F7C19FB97915DF2), sizeof(float4x4_t7EDD16F7F57DC7F61A6302535F7C19FB97915DF2_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6065;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6065 = { sizeof(half_tE8A6792149696F478D662DD4B736964C0FF018AF)+ sizeof(RuntimeObject), sizeof(half_tE8A6792149696F478D662DD4B736964C0FF018AF), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6066;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6066 = { sizeof(DebuggerProxy_tE4ABD062E1F1465B4A2601D1A2B183D9A691F3E7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6067;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6067 = { sizeof(int2_tF4AC25F87943DC0B2BB3456B0B919B3B42A9432A)+ sizeof(RuntimeObject), sizeof(int2_tF4AC25F87943DC0B2BB3456B0B919B3B42A9432A), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6068;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6068 = { sizeof(quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4)+ sizeof(RuntimeObject), sizeof(quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4), sizeof(quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6069;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6069 = { sizeof(DebuggerProxy_t834C4C5984734C3563C7BCAA0995DB56453E04DB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6070;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6070 = { sizeof(uint2_t157753816C23B82EB918C3D3AFCFDDE06A04C05F)+ sizeof(RuntimeObject), sizeof(uint2_t157753816C23B82EB918C3D3AFCFDDE06A04C05F), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6071;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6071 = { sizeof(DebuggerProxy_tFD3C8C80DBBCC5A0DBFB6C74F3A6C2BDFD1F362A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6072;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6072 = { sizeof(uint3_tC1C1C817DB46ED2E6A6C7390716FDDD565917F7B)+ sizeof(RuntimeObject), sizeof(uint3_tC1C1C817DB46ED2E6A6C7390716FDDD565917F7B), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6073;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6073 = { sizeof(DebuggerProxy_t3A8A5A2398C653D7A869D7B07919E08EB23696E8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6074;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6074 = { sizeof(uint4_t6C69CBFAE9BF0F727D52B68779D4A3F0DBA8D5C9)+ sizeof(RuntimeObject), sizeof(uint4_t6C69CBFAE9BF0F727D52B68779D4A3F0DBA8D5C9), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6075;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6075 = { sizeof(U3CModuleU3E_t0643977EA9107777E6F2E30DC5F5326A467F5F6B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6076;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6076 = { sizeof(PhysicsScene2D_t550D023B9E77BE6844564BB4F9FA291EEA10FDC9)+ sizeof(RuntimeObject), sizeof(PhysicsScene2D_t550D023B9E77BE6844564BB4F9FA291EEA10FDC9), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6077;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6077 = { sizeof(Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D), -1, sizeof(Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6078;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6078 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6079;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6079 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6080;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6080 = { sizeof(ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14)+ sizeof(RuntimeObject), sizeof(ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6081;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6081 = { sizeof(Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B), sizeof(Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6082;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6082 = { sizeof(ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801)+ sizeof(RuntimeObject), sizeof(ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6083;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6083 = { sizeof(RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA)+ sizeof(RuntimeObject), sizeof(RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6084;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6084 = { sizeof(Rigidbody2D_tBEBE9523CF4448544085AF46BF7E10AA499F320F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6085;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6085 = { sizeof(Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6086;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6086 = { sizeof(CircleCollider2D_t5D665D58EACA966EA4033BCF0EE91E198552E786), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6087;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6087 = { sizeof(BoxCollider2D_tF860C7737FFB062CEC06577E0CD8364EEC1D4EDA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6088;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6088 = { sizeof(PolygonCollider2D_t7CEFFFEE6522175436B408712B052D236889C89E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6089;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6089 = { sizeof(CompositeCollider2D_t58511A535241FD7BDA84C6F3DF2C38220D4079D8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6090;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6090 = { sizeof(PhysicsMaterial2D_t20AD48FB40C1BED4689A8135E81015703323C065), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6091;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6091 = { sizeof(U3CModuleU3E_t7A78175E99B61C7B4022EA3D1E12E92F7F669089), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6092;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6092 = { sizeof(BurstCompileAttribute_t35957F7418CF3B99A40C9E1C66CD3C56094A2C9D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6093;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6093 = { sizeof(FakeDelegate_t659588AB379C77AF08088ED8B2E2A5ECFF38CE16), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6094;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6094 = { sizeof(BurstCompiler_t2715484E1FF256726FC4D4D8E17C35A4C8DFA2B8), -1, sizeof(BurstCompiler_t2715484E1FF256726FC4D4D8E17C35A4C8DFA2B8_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6095;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6095 = { sizeof(BurstCompilerOptions_t5F93118F305E1B0C950C6F9AF8BCA74033DA01C9), -1, sizeof(BurstCompilerOptions_t5F93118F305E1B0C950C6F9AF8BCA74033DA01C9_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6096;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6096 = { sizeof(PreserveAttribute_tA1799B67558808CC16DE11D04CC1D42AAA569133), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6097;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6097 = { sizeof(BurstRuntime_tA87CEB6EE77F6DA708C87C3DAEC7862E3A1B0EA1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6098;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6098 = { sizeof(PreserveAttribute_t54BBA699FC0C1DD99BED77D21CADC33A352E1999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6099;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6099 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6100;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6100 = { sizeof(NumberBuffer_tF09E8463D840202ECA50F50BE6D57729C18213B4)+ sizeof(RuntimeObject), sizeof(NumberBuffer_tF09E8463D840202ECA50F50BE6D57729C18213B4_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6101;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6101 = { sizeof(uint8_t)+ sizeof(RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6102;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6102 = { sizeof(FormatOptions_tBD49C0C9CC14282D1249620565FC537D4D4AFB84)+ sizeof(RuntimeObject), sizeof(FormatOptions_tBD49C0C9CC14282D1249620565FC537D4D4AFB84_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6103;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6103 = { sizeof(U3Cm_blocksU3Ee__FixedBuffer_tBBE20C4EF7009465021F0375E2128D5DCFF59F7C)+ sizeof(RuntimeObject), sizeof(U3Cm_blocksU3Ee__FixedBuffer_tBBE20C4EF7009465021F0375E2128D5DCFF59F7C), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6104;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6104 = { sizeof(tBigInt_t6A436AD3913A2950571338A5018B48B299987358)+ sizeof(RuntimeObject), sizeof(tBigInt_t6A436AD3913A2950571338A5018B48B299987358), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6105;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6105 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6106;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6106 = { sizeof(tFloatUnion32_t1140001CA96F869F598FBC16C082BC2BA85AB2CA)+ sizeof(RuntimeObject), sizeof(tFloatUnion32_t1140001CA96F869F598FBC16C082BC2BA85AB2CA), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6107;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6107 = { sizeof(tFloatUnion64_t737111FBE1FD2D4509E72C45FE6389106B60B2FC)+ sizeof(RuntimeObject), sizeof(tFloatUnion64_t737111FBE1FD2D4509E72C45FE6389106B60B2FC), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6108;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6108 = { sizeof(BurstString_tD6AF700FD5AF48728FC90C6CA2AA2E48C6472AF1), -1, sizeof(BurstString_tD6AF700FD5AF48728FC90C6CA2AA2E48C6472AF1_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6109;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6109 = { sizeof(PreserveAttribute_tDEA15EF9DCAB8AC4428ED72A2A1377384FE7C27B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6110;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6110 = { sizeof(SharedStatic_t83F4045688B6DB97142DC2BCAE88140D165FFE35), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6111;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6111 = { sizeof(__StaticArrayInitTypeSizeU3D3_t2857C07F0A23FB025DA0D81FCD2BE07B4ADCC026)+ sizeof(RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D3_t2857C07F0A23FB025DA0D81FCD2BE07B4ADCC026), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6112;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6112 = { sizeof(__StaticArrayInitTypeSizeU3D32_tF5E240ACF4B30B5A5F8C77E9E49CC2F8559D76D9)+ sizeof(RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D32_tF5E240ACF4B30B5A5F8C77E9E49CC2F8559D76D9), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6113;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6113 = { sizeof(__StaticArrayInitTypeSizeU3D256_tFFE4CE163BD2DCEAA09662C2BCC33B3C37AB0D22)+ sizeof(RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D256_tFFE4CE163BD2DCEAA09662C2BCC33B3C37AB0D22), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6114;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6114 = { sizeof(U3CPrivateImplementationDetailsU3E_t2CADAF0D55AC9D0785A6F7B80D4772CF1220C48F), -1, sizeof(U3CPrivateImplementationDetailsU3E_t2CADAF0D55AC9D0785A6F7B80D4772CF1220C48F_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6115;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6115 = { sizeof(U3CModuleU3E_t7A1E3DF1BFD27FA828B031F2A96909F13C3F170B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6116;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6116 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6117;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6117 = { sizeof(CanvasGroup_t048C1461B14628CFAEBE6E7353093ADB04EBC094), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6118;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6118 = { sizeof(CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6119;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6119 = { sizeof(RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244), -1, sizeof(RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6120;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6120 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6121;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6121 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6122;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6122 = { sizeof(WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6123;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6123 = { sizeof(Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26), -1, sizeof(Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6124;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6124 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6125;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6125 = { sizeof(UISystemProfilerApi_t891AC4E16D3C12EAFD2748AE04F7A070F632396A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6126;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6126 = { sizeof(U3CModuleU3E_t806C4A82D63BA5BEE007D75772441609D967BADA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6127;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6127 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6128;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6128 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6129;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6129 = { sizeof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6130;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6130 = { sizeof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6131;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6131 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6132;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6132 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6133;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6133 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6134;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6134 = { sizeof(TextMesh_t7E1981C7B03E50D5CA5A3AD5B0D9BB0AB6EE91F8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6135;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6135 = { sizeof(UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD)+ sizeof(RuntimeObject), sizeof(UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6136;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6136 = { sizeof(UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC)+ sizeof(RuntimeObject), sizeof(UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6137;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6137 = { sizeof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207)+ sizeof(RuntimeObject), sizeof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207), sizeof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6138;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6138 = { sizeof(FontTextureRebuildCallback_t76D5E172DF8AA57E67763D453AAC40F0961D09B1), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6139;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6139 = { sizeof(Font_tC95270EA3198038970422D78B74A7F2E218A96B6), -1, sizeof(Font_tC95270EA3198038970422D78B74A7F2E218A96B6_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6140;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6140 = { sizeof(U3CModuleU3E_tD4D8152B1CC10B76FF3BD3BF122F926B6BF0D3EE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6141;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6141 = { sizeof(MainModule_tC7ECD8330C14B0808478A748048988A6085CE2A9)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6142;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6142 = { sizeof(TextureSheetAnimationModule_tB53F451F252E24ACC3EF80D770DB4FBE1A13D1F6)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6143;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6143 = { sizeof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D)+ sizeof(RuntimeObject), sizeof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6144;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6144 = { sizeof(MinMaxCurve_t812D571297EED6498776CC58949A42B172E60E23)+ sizeof(RuntimeObject), sizeof(MinMaxCurve_t812D571297EED6498776CC58949A42B172E60E23), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6145;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6145 = { sizeof(MinMaxGradient_tFF31B8EC2855D0074AB86E8B37BEA6609070AC69)+ sizeof(RuntimeObject), sizeof(MinMaxGradient_tFF31B8EC2855D0074AB86E8B37BEA6609070AC69), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6146;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6146 = { sizeof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0)+ sizeof(RuntimeObject), sizeof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6147;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6147 = { sizeof(CustomDataModule_t44BDC4DC2B7E66B5298D766D86667C063E8595C4)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6148;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6148 = { sizeof(ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6149;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6149 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6150;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6150 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6151;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6151 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6152;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6152 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6153;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6153 = { sizeof(ParticleSystemRenderer_t576C271A363A738A6C576D4C6AEFB3B5B23E46C4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6154;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6154 = { sizeof(U3CModuleU3E_t61602430437A8A096CA205928842E5E8AA51729D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6155;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6155 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6156;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6156 = { sizeof(CameraShake_tF662E3447A81E0F4B337C630E8DDF9758C02DFD0), -1, sizeof(CameraShake_tF662E3447A81E0F4B337C630E8DDF9758C02DFD0_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6157;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6157 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6158;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6158 = { sizeof(AnimatedLight_t452FBE1EDE1B867DB67C7383966C9992DC6A9C59), -1, sizeof(AnimatedLight_t452FBE1EDE1B867DB67C7383966C9992DC6A9C59_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6159;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6159 = { sizeof(CFXR_Effect_t3017EDCEABB0C70663A82AAA961EE597C0AF743A), -1, sizeof(CFXR_Effect_t3017EDCEABB0C70663A82AAA961EE597C0AF743A_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6160;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6160 = { sizeof(CFXR_EmissionBySurface_t0512AB30B6428D57F6153E5928283CD14E1DFDD7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6161;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6161 = { sizeof(CFXR_ParticleText_t4E5797B5E79E3513E6901E6C7CD8ACB5891EA1D3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6162;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6162 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6163;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6163 = { sizeof(Kerning_t5359477AFC231ACCA7CB4DAE0513897AABCA7500), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6164;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6164 = { sizeof(CFXR_ParticleTextFontAsset_t19D1F73819A2F60E0FA1B17ECA99FB3FAAF756ED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6165;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6165 = { sizeof(U3CModuleU3E_t941B0EB06FD57B79F043CCA70C8AA4C0B3FB68E7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6166;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6166 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6167;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6167 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6168;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6168 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6169;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6169 = { sizeof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417)+ sizeof(RuntimeObject), sizeof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6170;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6170 = { sizeof(CameraRaycastHelper_tEF8B5EE50B6F5141652EAAF44A77E8B3621FE455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6171;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6171 = { sizeof(Input_t47D83E2A50E6AF7F8A47AA06FBEF9EBE6BBC22BB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6172;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6172 = { sizeof(HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314)+ sizeof(RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6173;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6173 = { sizeof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39), -1, sizeof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6174;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6174 = { sizeof(U3CModuleU3E_t416C1B54F702B9F0B5C7C848BFDFA85A9E90F443), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6175;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6175 = { sizeof(AssetFileNameExtensionAttribute_tEA86B663DC42BB5C4F9A2A081CD7D28845D9D056), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6176;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6176 = { sizeof(ThreadAndSerializationSafeAttribute_t819C12E8106F42E7493B11DDA93C36F6FB864357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6177;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6177 = { sizeof(WritableAttribute_t7D85DADDFD6751C94E2E9594E562AD281A3B6E7B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6178;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6178 = { sizeof(UnityEngineModuleAssembly_tB6587DA5BA2569921894019758C4D69095012710), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6179;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6179 = { sizeof(NativeClassAttribute_t774C48B9F745C9B0FD2FA82F9B42D4A18E162FA7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6180;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6180 = { sizeof(UnityString_tEB81DAFE75C642A9472D9FEDA7C2EC19A7B672B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6181;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6181 = { sizeof(VisibleToOtherModulesAttribute_tE7803AC6A0462A18B7EEF17C4A1036DEE993B489), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6182;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6182 = { sizeof(NativeConditionalAttribute_tB722B3ED350E82853F8CEFF672A6CDC4B6B362CA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6183;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6183 = { sizeof(NativeHeaderAttribute_t35DDAA41C31EEE4C94D2586F33D3EB26C0EA6F51), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6184;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6184 = { sizeof(NativeNameAttribute_t222751782B5418807DFE2A88CA0B24CA691B8621), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6185;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6185 = { sizeof(NativeWritableSelfAttribute_t2ABC353836DDC2F15B1FBED9C0CF2E5ED0D1686C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6186;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6186 = { sizeof(NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6187;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6187 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6188;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6188 = { sizeof(NativePropertyAttribute_tAF7FB03BF7FFE9E8AB0E75B0F842FC0AA22AE607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6189;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6189 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6190;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6190 = { sizeof(NativeAsStructAttribute_t48549F0E2D38CC0251B7BF2780E434EA141DF2D8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6191;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6191 = { sizeof(NativeTypeAttribute_tB60F1675F1F20B6CB1B871FDDD067D672F75B8D1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6192;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6192 = { sizeof(NotNullAttribute_t2E29B7802E8ED55CEA04EC4A6C254C6B60272DF7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6193;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6193 = { sizeof(UnmarshalledAttribute_t3D645C3393EF99EED2893026413D4F5B489CD13B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6194;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6194 = { sizeof(FreeFunctionAttribute_t1200571BEDF64167E58F976FB7374AEA5D9BCBB6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6195;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6195 = { sizeof(ThreadSafeAttribute_t2535A209D57BDA2FF398C4CA766059277FC349FE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6196;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6196 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6197;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6197 = { sizeof(StaticAccessorAttribute_tDE194716AED7A414D473DC570B2E0035A5CE130A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6198;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6198 = { sizeof(NativeThrowsAttribute_t211CE8D047A8D45676C9ED399D5AA3B4A2C3E625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6199;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6199 = { sizeof(IgnoreAttribute_tAB3F6C4808BA16CD585D60A6353B3E0599DFCE4D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6200;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6200 = { sizeof(PreventReadOnlyInstanceModificationAttribute_t7FBCFCBA855C80F9E87486C8A6B4DDBA47B78415), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6201;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6201 = { sizeof(UsedByNativeCodeAttribute_t3FE9A7CDCC6A3A4122D8BF44F1D0A37BB38894C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6202;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6202 = { sizeof(RequiredByNativeCodeAttribute_t86B11F2BA12BB463CE3258E64E16B43484014FCA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6203;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6203 = { sizeof(U3CModuleU3E_t462BCCFB9B78348533823E0754F65F52A5348F89), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6204;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6204 = { sizeof(AudioConfigurationChangeHandler_tE071B0CBA3B3A77D3E41F5FCB65B4017885B3177), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6205;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6205 = { sizeof(Mobile_t304A73480DF447472BDB16BA19A9E4FE2C8CB2DD), -1, sizeof(Mobile_t304A73480DF447472BDB16BA19A9E4FE2C8CB2DD_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6206;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6206 = { sizeof(AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD), -1, sizeof(AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6207;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6207 = { sizeof(PCMReaderCallback_t3396D9613664F0AFF65FB91018FD0F901CC16F1E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6208;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6208 = { sizeof(PCMSetPositionCallback_t8D7135A2FB40647CAEC93F5254AD59E18DEB6072), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6209;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6209 = { sizeof(AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6210;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6210 = { sizeof(AudioBehaviour_t2DC0BEF7B020C952F3D2DA5AAAC88501C7EEB941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6211;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6211 = { sizeof(AudioListener_t1D629CE9BC079C8ECDE8F822616E8A8E319EAE35), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6212;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6212 = { sizeof(AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6213;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6213 = { sizeof(AudioClipPlayable_tD4B758E68CAE03CB0CD31F90C8A3E603B97143A0)+ sizeof(RuntimeObject), sizeof(AudioClipPlayable_tD4B758E68CAE03CB0CD31F90C8A3E603B97143A0), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6214;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6214 = { sizeof(AudioMixer_tE2E8D79241711CDF9AB428C7FB96A35D80E40B04), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6215;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6215 = { sizeof(AudioMixerPlayable_t6AADDF0C53DF1B4C17969EC24B3B4E4975F3A56C)+ sizeof(RuntimeObject), sizeof(AudioMixerPlayable_t6AADDF0C53DF1B4C17969EC24B3B4E4975F3A56C), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6216;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6216 = { sizeof(AudioPlayableOutput_tC3DFF8095F429D90129A367EAB98A24F6D6ADF20)+ sizeof(RuntimeObject), sizeof(AudioPlayableOutput_tC3DFF8095F429D90129A367EAB98A24F6D6ADF20), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6217;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6217 = { sizeof(SampleFramesHandler_tFE84FF9BBCEFB880D46227188F375BEF680AAA30), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6218;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6218 = { sizeof(AudioSampleProvider_t602353124A2F6F2AEC38E56C3C21932344F712E2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6219;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6219 = { sizeof(U3CModuleU3E_tCDD16073F88F09BB7B50158A053DD15949D8ADB6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6220;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6220 = { sizeof(IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6221;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6221 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6222;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6222 = { sizeof(IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D), sizeof(IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6223;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6223 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6224;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6224 = { sizeof(SubsystemDescriptorBindings_t12C0380442BAE5AD9760662561CAD0AE7B41FFFE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6225;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6225 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6226;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6226 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6227;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6227 = { sizeof(Subsystem_t5E67EE95D848FB950AD5D76325BF8959A6F7C7D7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6228;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6228 = { sizeof(SubsystemDescriptor_tF417D2751C69A8B0DD86162EBCE55F84D3493A71), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6229;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6229 = { sizeof(Internal_SubsystemDescriptors_t087D53EE6F9D7AAEA9E38D42AF436C952DF7936F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6230;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6230 = { sizeof(SubsystemManager_t9A7261E4D0B53B996F04B8707D8E1C33AB65E824), -1, sizeof(SubsystemManager_t9A7261E4D0B53B996F04B8707D8E1C33AB65E824_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6231;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6231 = { sizeof(SubsystemDescriptorStore_tEF3761B84B8C25EA4B93F94A487551820B268250), -1, sizeof(SubsystemDescriptorStore_tEF3761B84B8C25EA4B93F94A487551820B268250_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6232;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6232 = { sizeof(SubsystemDescriptorWithProvider_t2A61A2C951A4A179E898CF207726BF6B5AF474D5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6233;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6233 = { sizeof(SubsystemWithProvider_tC72E35EE2D413A4B0635B058154BABF265F31242), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6234;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6234 = { sizeof(U3CModuleU3E_t6B4A7D64487421A1C7A9ACB5578F8A35510E2A0C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6235;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6235 = { sizeof(UpdatedEventHandler_tB0D5A5BA322FE093894992C29DCF51E7E12579C4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6236;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6236 = { sizeof(RemoteSettings_t9DFFC747AB3E7A39DF4527F245B529A407427250), -1, sizeof(RemoteSettings_t9DFFC747AB3E7A39DF4527F245B529A407427250_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6237;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6237 = { sizeof(RemoteConfigSettings_tC979947EE51355162B3241B9F80D95A8FD25FE52), sizeof(RemoteConfigSettings_tC979947EE51355162B3241B9F80D95A8FD25FE52_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6238;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6238 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6239;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6239 = { sizeof(RemoteConfigSettingsHelper_t29B2673892F8181388B45FFEEE354B3773629588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6240;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6240 = { sizeof(ContinuousEvent_t71122F6F65BF7EA8490EA664A55D5C03790CB6CF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6241;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6241 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6242;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6242 = { sizeof(SessionStateChanged_t1180FB66E702B635CAD9316DC661D931277B2A0C), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6243;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6243 = { sizeof(IdentityTokenChanged_tE8CB0DAB5F6E640A847803F582E6CB6237742395), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6244;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6244 = { sizeof(AnalyticsSessionInfo_tDE8F7A9E13EF9723E2D975F76E916753DA61AD76), -1, sizeof(AnalyticsSessionInfo_tDE8F7A9E13EF9723E2D975F76E916753DA61AD76_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6245;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6245 = { sizeof(U3CModuleU3E_tE7BE147157D59625477C35248C6A6C59EA2900FB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6246;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6246 = { sizeof(NativeUpdateCallback_tC5CA5A9117B79251968A4DA3758552EFE1D37495), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6247;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6247 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6248;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6248 = { sizeof(NativeInputEventBuffer_t4EE5873AD7998E0E83C9F8585C338AB14C9101FD)+ sizeof(RuntimeObject), sizeof(NativeInputEventBuffer_t4EE5873AD7998E0E83C9F8585C338AB14C9101FD), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6249;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6249 = { sizeof(NativeInputEvent_tDE7DE9A48ACA442A8D37E2920836D00C26408CB8)+ sizeof(RuntimeObject), sizeof(NativeInputEvent_tDE7DE9A48ACA442A8D37E2920836D00C26408CB8), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6250;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6250 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6251;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6251 = { sizeof(NativeInputSystem_tCFE5554EBC0D3EE1DAD80FC55CE0DE38A3DDC5EE), -1, sizeof(NativeInputSystem_tCFE5554EBC0D3EE1DAD80FC55CE0DE38A3DDC5EE_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6252;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6252 = { sizeof(U3CModuleU3E_t0F6AB019D77D717D42BE5AD848FFBD032B14CFFC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6253;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6253 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6254;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6254 = { sizeof(XRSettings_t783533FF87B79D6D0C6A47FA8EC9B17EC0820D97), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6255;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6255 = { sizeof(XRDevice_tD076A68EFE413B3EEEEA362BE0364A488B58F194), -1, sizeof(XRDevice_tD076A68EFE413B3EEEEA362BE0364A488B58F194_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6256;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6256 = { sizeof(U3CModuleU3E_t30BE97772842985A3AF723FA1759387A3E79C563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6257;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6257 = { sizeof(PlayableDirector_t895D7BC3CFBFFD823278F438EAC4AA91DBFEC475), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6258;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6258 = { sizeof(U3CModuleU3E_t2F9091E403B25A5364AE8A6B2C249E31D405E3F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6259;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6259 = { sizeof(JsonUtility_t731013D97E03B7EDAE6186D6D6826A53B85F7197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6260;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6260 = { sizeof(U3CModuleU3E_t5E8190EE43F4DF5D80E8A6651A0469A8FD445F94), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6261;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6261 = { sizeof(Tilemap_t18C4166D0AC702D5BFC0C411FA73C4B61D9D1751), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6262;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6262 = { sizeof(TilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6263;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6263 = { sizeof(U3CModuleU3E_t4BC86AB59C63F2CC6EB51BE560C15CFCAE821BC7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6264;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6264 = { sizeof(SpriteShapeRenderer_tE998BB73CF661079736CCC23617E597AB230A4AC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6265;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6265 = { sizeof(U3CModuleU3E_tB4B8BFF6E6672C9C5CA86F52CA248539A4A5120B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6266;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6266 = { sizeof(WebViewObject_t38ED83F3B0AE8A7AF9FBAFC94A725C7385DD51E2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6267;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6267 = { sizeof(U3CModuleU3E_t2C7BF608494A5C8FB8C8C4D318FB27BCF6CE322A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6268;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6268 = { sizeof(GridLayout_tAD661B1E1E57C16BE21C8C13432EA04FE1F0418B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6269;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6269 = { sizeof(U3CModuleU3E_t90149EF90407715CC46EB5A9704669888393F1DE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6270;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6270 = { sizeof(ASN1_t33549D58797C9C33AA83F13AD184EAA00C584A6F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6271;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6271 = { sizeof(ASN1Convert_tDA6D2B7710D7868F3D559D5BE7F2C7816BB50AB6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6272;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6272 = { sizeof(BitConverterLE_tB6EF365ED05024FCC12DA3939B10FDEBDB29E1BD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6273;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6273 = { sizeof(CryptoConvert_t676AC22DA6332E9936696ECC97197AB7B1BC7252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6274;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6274 = { sizeof(U3CModuleU3E_tACA7C44D47EE520DBE4B854B783CBE378B317C8B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6275;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6275 = { sizeof(CFXR_Demo_tC798392D488F2E202DFEB3464B0A5EFEC019BE6C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6276;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6276 = { sizeof(CFXR_Demo_Rotate_tC9BF71B92F879B0DC058EC49386E2B596A3021F2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6277;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6277 = { sizeof(CFXR_Demo_Translate_t95CB6CDBAE93F397B9CA608E9B233BAB8C7FB891), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6278;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6278 = { sizeof(U3CModuleU3E_t5C873E33121FECD351F7CB74331B5D342A4C978F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6279;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6279 = { sizeof(Bloom_tA14A4ADA32420926093CB5242D06D17DA1A9BB1E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6280;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6280 = { sizeof(U3CModuleU3E_t4791F64F4B6411D4D033A002CAD365D597AA2451), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6281;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6281 = { sizeof(XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD), -1, sizeof(XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6282;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6282 = { sizeof(XmlNode_t3180B9B3D5C36CD58F5327D9F13458E3B3F030AF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6283;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6283 = { sizeof(U3CModuleU3E_tBF95EDE7AC72079FEC40B1A3B57D72418FF75C99), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6284;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6284 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6285;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6285 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6286;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6286 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6287;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6287 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6288;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6288 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6289;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6289 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6290;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6290 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6291;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6291 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6292;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6292 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6293;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6293 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6294;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6294 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6295;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6295 = { sizeof(LightData_tAC4023737E9903DE3F96B993AA323E062ABCB9ED)+ sizeof(RuntimeObject), sizeof(LightData_tAC4023737E9903DE3F96B993AA323E062ABCB9ED), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6296;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6296 = { sizeof(ShaderInput_t048F7464F457883E25851701A5BBF15ADDDABAA3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6297;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6297 = { sizeof(U3CModuleU3E_tDA285F13E9413BF3B79A99D6E310BE9AF3444EEB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6298;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6298 = { sizeof(ConfigurationElement_tAE3EE71C256825472831FFBB7F491275DFAF089E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6299;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6299 = { sizeof(ConfigurationSection_t0BC609F0151B160A4FAB8226679B62AF22539C3E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6300;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6300 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6301;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6301 = { sizeof(ConfigurationPropertyCollection_t1DEB95D3283BB11A46B862E9D13710ED698B6C93), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6302;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6302 = { sizeof(ConfigurationElementCollection_t56E8398661A85A59616301BADF13026FB1492606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6303;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6303 = { sizeof(ConfigurationCollectionAttribute_t1D7DBAAB4908B6B8F26EA1C66106A67BDE949558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6304;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6304 = { sizeof(ConfigurationSectionGroup_tE7948C2D31B193F4BA8828947ED3094B952C7863), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6305;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6305 = { sizeof(IgnoreSection_t43A7C33C0083D18639AA3CC3D75DD93FCF1C5D97), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6306;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6306 = { sizeof(ThrowStub_t9161280E38728A40D9B1A975AEE62E89C379E400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6307;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6307 = { sizeof(U3CModuleU3E_t030FC11D37663E6AAF04929A5E099A4974BAB334), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6308;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6308 = { sizeof(DoNotObfuscateClassAttribute_t28D5BEC72DA2A88F118DB9764424391DB2F9312B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6309;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6309 = { sizeof(DoNotUseClassForFakeCodeAttribute_t198754EECF12C9E58F495144BD1336F77995FEB9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6310;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6310 = { sizeof(DoNotRenameAttribute_tDF0E0CDAF447C0119529B7A7C87DFDAABCFA0653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6311;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6311 = { sizeof(DoNotObfuscateMethodBodyAttribute_t0DCC53A255BEBEF9BBAE0F007FC7A9D4BA547868), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6312;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6312 = { sizeof(NotObfuscatedCauseAttribute_tFB6D7D82C7F2ABE6CE218ACB6CE3E849DC9C2865), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6313;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6313 = { sizeof(ObfuscateAnywayAttribute_tFF38D6CED6FAC08B8615C58AECEDEA379854B579), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6314;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6314 = { 0, sizeof(Il2CppIActivationFactory*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6315;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6315 = { sizeof(Il2CppComObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6316;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize6316 = { sizeof(__Il2CppComDelegate_tD0DD2BBA6AC8F151D32B6DFD02F6BDA339F8DC4D), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
