﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOFade(UnityEngine.AudioSource,System.Single,System.Single)
extern void DOTweenModuleAudio_DOFade_m1D693C3CE476AABDF1973BE1CDB974276D71307A (void);
// 0x00000002 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOPitch(UnityEngine.AudioSource,System.Single,System.Single)
extern void DOTweenModuleAudio_DOPitch_m21C03AF57AD7EEC7C1CF82CA7F660A58BD17D7CF (void);
// 0x00000003 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOSetFloat(UnityEngine.Audio.AudioMixer,System.String,System.Single,System.Single)
extern void DOTweenModuleAudio_DOSetFloat_m95C5B73E2208425BB0D225DBBE705A62C34ADB8B (void);
// 0x00000004 System.Int32 DG.Tweening.DOTweenModuleAudio::DOComplete(UnityEngine.Audio.AudioMixer,System.Boolean)
extern void DOTweenModuleAudio_DOComplete_mB34A0C4B5156B4D5D7F88E1F1B3549F0ED35048C (void);
// 0x00000005 System.Int32 DG.Tweening.DOTweenModuleAudio::DOKill(UnityEngine.Audio.AudioMixer,System.Boolean)
extern void DOTweenModuleAudio_DOKill_mAF9948DBD5491192943527B5FD65561EB5E6DA23 (void);
// 0x00000006 System.Int32 DG.Tweening.DOTweenModuleAudio::DOFlip(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOFlip_mA5DAA9E654CBF4C302F7376463DC80EF9FE480B0 (void);
// 0x00000007 System.Int32 DG.Tweening.DOTweenModuleAudio::DOGoto(UnityEngine.Audio.AudioMixer,System.Single,System.Boolean)
extern void DOTweenModuleAudio_DOGoto_m0E560369CDA932ED0068A0CED7D733FE203E43E1 (void);
// 0x00000008 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPause(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPause_mDC395F8C1B08A113E79E42FAB5EE1EF453B1136D (void);
// 0x00000009 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlay(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlay_m86F982951B3B2786390449AFE30BAEFBDC7E92D7 (void);
// 0x0000000A System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlayBackwards(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlayBackwards_mD371D3D6C3D85A6CE99B597BB0305D9E6EDD87F4 (void);
// 0x0000000B System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlayForward(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlayForward_m561924708D16379FF482E79B5710AB2FB5B1E9FB (void);
// 0x0000000C System.Int32 DG.Tweening.DOTweenModuleAudio::DORestart(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DORestart_m0B4E5FF932DACDE8A3B1C85BE6B5A8FCB12AC0F0 (void);
// 0x0000000D System.Int32 DG.Tweening.DOTweenModuleAudio::DORewind(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DORewind_m20E78D2E878D7E35D859B4789DBDC2DF968B06E3 (void);
// 0x0000000E System.Int32 DG.Tweening.DOTweenModuleAudio::DOSmoothRewind(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOSmoothRewind_m775A16C85A17C3471062578BCC5A45683A67AC5A (void);
// 0x0000000F System.Int32 DG.Tweening.DOTweenModuleAudio::DOTogglePause(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOTogglePause_m23CC6F5387C5060B72E4000C5F4A61B944A5F971 (void);
// 0x00000010 System.Void DG.Tweening.DOTweenModuleAudio/cy::.ctor()
extern void cy__ctor_mAD0200923EC5D8CAC85E9DFAC91E671886E097EF (void);
// 0x00000011 System.Single DG.Tweening.DOTweenModuleAudio/cy::bau()
extern void cy_bau_mF4F3F3411DEA9CA80FF495942F48374DB6DE7975 (void);
// 0x00000012 System.Void DG.Tweening.DOTweenModuleAudio/cy::bav(System.Single)
extern void cy_bav_m8C2EE9610DA7D80B90DB97136E3D49B0A98FAB06 (void);
// 0x00000013 System.Void DG.Tweening.DOTweenModuleAudio/cz::.ctor()
extern void cz__ctor_m9328977F90A2F7547EA0720A2C8A297C9591A5B6 (void);
// 0x00000014 System.Single DG.Tweening.DOTweenModuleAudio/cz::baw()
extern void cz_baw_m28CBB54AE2F1308DD5F98238438AB0903477FBB1 (void);
// 0x00000015 System.Void DG.Tweening.DOTweenModuleAudio/cz::bax(System.Single)
extern void cz_bax_mF797BA4CDEF8CDA4CC84E0567CE9885756501741 (void);
// 0x00000016 System.Void DG.Tweening.DOTweenModuleAudio/da::.ctor()
extern void da__ctor_m2EB018AFBE7166123DBDD52AE2240314AB8D7222 (void);
// 0x00000017 System.Single DG.Tweening.DOTweenModuleAudio/da::bay()
extern void da_bay_m42C2FAD4273E6BB52565BC063CDE6E23342B4D22 (void);
// 0x00000018 System.Void DG.Tweening.DOTweenModuleAudio/da::baz(System.Single)
extern void da_baz_m38E2F677AA346B2BE06EF7B38CECA75AAD0E23EB (void);
// 0x00000019 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMove(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMove_m5B66A39F2C06E19EDFA4D9D79B8CEDD1B2C1AE9C (void);
// 0x0000001A DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveX(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveX_mAE96C9DA1E0C5EC23A773965A91B86C987BAEAAB (void);
// 0x0000001B DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveY(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveY_mE4DCBAC03F31FBF07EDFB15A55E2822256EAE03A (void);
// 0x0000001C DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveZ(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveZ_m5915FC138CB7BBFFCEF34456590EF4670B1893CE (void);
// 0x0000001D DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenModulePhysics::DORotate(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern void DOTweenModulePhysics_DORotate_m65EA16E432A1B477A5A696EF95257FE812C948D6 (void);
// 0x0000001E DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenModulePhysics::DOLookAt(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.AxisConstraint,System.Nullable`1<UnityEngine.Vector3>)
extern void DOTweenModulePhysics_DOLookAt_m86BB568C280583D5140AF00ACDB6102191ECD98D (void);
// 0x0000001F DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics::DOJump(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOJump_m66B1A29F77189B9981097A35A69E00974F0ACA65 (void);
// 0x00000020 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics_DOPath_m1443BEE665EB5D1A4EF2C356BE311406251657D4 (void);
// 0x00000021 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics_DOLocalPath_m16C97084468DB6E53825EEED8132F80E127EF0FA (void);
// 0x00000022 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::bbr(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_bbr_m18FC0C755ECE981DD2D8F3000BE727C1ED71C78B (void);
// 0x00000023 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::bbs(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_bbs_m1415FDD052274D1E8989D011D34B9CF0EA388CF1 (void);
// 0x00000024 System.Void DG.Tweening.DOTweenModulePhysics/db::.ctor()
extern void db__ctor_mC47DAB57935178AA388F296B145282A06B907646 (void);
// 0x00000025 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/db::bba()
extern void db_bba_mE1D8F4D74FE9A142CC3A94D1EF584F5034668FE8 (void);
// 0x00000026 System.Void DG.Tweening.DOTweenModulePhysics/dc::.ctor()
extern void dc__ctor_m41D031119870A391E7B37DC66A96C2B23514684C (void);
// 0x00000027 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/dc::bbb()
extern void dc_bbb_mCF736ECBEE88A914ECC6370502845380F79CFE5B (void);
// 0x00000028 System.Void DG.Tweening.DOTweenModulePhysics/dd::.ctor()
extern void dd__ctor_m34B82646EC7DC3F7512BF3B715979133E2470BED (void);
// 0x00000029 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/dd::bbc()
extern void dd_bbc_m20F0962AE47C761B9DB13CDBABE5019540BD5A64 (void);
// 0x0000002A System.Void DG.Tweening.DOTweenModulePhysics/de::.ctor()
extern void de__ctor_m9F8780F631E023B10D133198A1B1EADA815EE8FE (void);
// 0x0000002B UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/de::bbd()
extern void de_bbd_m29215FC0F3FAB3BA668818D4D7EE0A250EF62448 (void);
// 0x0000002C System.Void DG.Tweening.DOTweenModulePhysics/df::.ctor()
extern void df__ctor_m29ACA03FEBD0AA1061A7B077D05AB540C386A972 (void);
// 0x0000002D UnityEngine.Quaternion DG.Tweening.DOTweenModulePhysics/df::bbe()
extern void df_bbe_m2DF1448224DC8E7D0C0E707C5AF171B107C2CA8E (void);
// 0x0000002E System.Void DG.Tweening.DOTweenModulePhysics/dg::.ctor()
extern void dg__ctor_m9244DAD4B53BBE947CA6065A2198CA744C1AE8B0 (void);
// 0x0000002F UnityEngine.Quaternion DG.Tweening.DOTweenModulePhysics/dg::bbf()
extern void dg_bbf_mC8BD356602E0BBE4C58A810F18D46650A57164E6 (void);
// 0x00000030 System.Void DG.Tweening.DOTweenModulePhysics/dh::.ctor()
extern void dh__ctor_m5AA9F8D1F6869A43D51C6791227359D26408DE1D (void);
// 0x00000031 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/dh::bbg()
extern void dh_bbg_mC549D527C1DB9B8450B32B91C494788D1BB45F93 (void);
// 0x00000032 System.Void DG.Tweening.DOTweenModulePhysics/dh::bbh()
extern void dh_bbh_m836CD1A928589FD86D388BC42DFB88800B3B83E5 (void);
// 0x00000033 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/dh::bbi()
extern void dh_bbi_mA2D0C76C9812DC9E919BA37C17F31AC806229E4C (void);
// 0x00000034 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/dh::bbj()
extern void dh_bbj_m8DF4B60E3D5B8F1C10AD90C5EF19F126EFFF8337 (void);
// 0x00000035 System.Void DG.Tweening.DOTweenModulePhysics/dh::bbk()
extern void dh_bbk_m601E4F8A594C58390CF8573D5DB90ED034AF9E59 (void);
// 0x00000036 System.Void DG.Tweening.DOTweenModulePhysics/di::.ctor()
extern void di__ctor_mD60FD2690C1DA38AF59744212EF474505A8BBA38 (void);
// 0x00000037 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/di::bbl()
extern void di_bbl_mFC8ABA84C90FE58B2D54A9FF9C07D0C241E71D5D (void);
// 0x00000038 System.Void DG.Tweening.DOTweenModulePhysics/dj::.ctor()
extern void dj__ctor_m4F84B875F118FBC68AB63E8FFF5654716A90F4AF (void);
// 0x00000039 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/dj::bbm()
extern void dj_bbm_m10AAE3DF1A75A38CD27BD61D8EF8ECA83CB5D208 (void);
// 0x0000003A System.Void DG.Tweening.DOTweenModulePhysics/dj::bbn(UnityEngine.Vector3)
extern void dj_bbn_m0EB086C7F47665F8606114DE8C809893C6CA5B15 (void);
// 0x0000003B System.Void DG.Tweening.DOTweenModulePhysics/dk::.ctor()
extern void dk__ctor_m95CF67F9EE4039565B1E36046C9E5813FC3F44A9 (void);
// 0x0000003C UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/dk::bbo()
extern void dk_bbo_mC7CFFB919C3DA41EE4D98C4D462B8EC1185B6C88 (void);
// 0x0000003D System.Void DG.Tweening.DOTweenModulePhysics/dl::.ctor()
extern void dl__ctor_m595DA2456184016A717CB6DB10C4B394F6692F56 (void);
// 0x0000003E UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/dl::bbp()
extern void dl_bbp_m7B1DA01BFD5C08F32B798CF6B169330C78159972 (void);
// 0x0000003F System.Void DG.Tweening.DOTweenModulePhysics/dl::bbq(UnityEngine.Vector3)
extern void dl_bbq_mCF87654BD9AD27D3A838EE15CAF4AE461E425ADC (void);
// 0x00000040 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMove(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMove_m6A74D57E65B6F8E119164B18F17D3ED7BB8B6B44 (void);
// 0x00000041 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMoveX(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMoveX_m7F9730EFF5DE54338332D74B2364243C211EB7F4 (void);
// 0x00000042 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMoveY(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMoveY_mA3B4A7F4B174180E22FA9F9CC8852B001460E990 (void);
// 0x00000043 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModulePhysics2D::DORotate(UnityEngine.Rigidbody2D,System.Single,System.Single)
extern void DOTweenModulePhysics2D_DORotate_m914F5F01CD6972EE0FCBA60FBC4AB340E647DC5E (void);
// 0x00000044 DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics2D::DOJump(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOJump_m994418231434C0151D7ADEE79A02174D6F4E5B2B (void);
// 0x00000045 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics2D::DOPath(UnityEngine.Rigidbody2D,UnityEngine.Vector2[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics2D_DOPath_m0CF606B5F7970A8D1FBC5816ED775CA1736C9D1C (void);
// 0x00000046 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics2D::DOLocalPath(UnityEngine.Rigidbody2D,UnityEngine.Vector2[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics2D_DOLocalPath_mEC1BAA9AEDBCE7F667D52E04746C6E29C7AFAA5B (void);
// 0x00000047 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics2D::bcl(UnityEngine.Rigidbody2D,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics2D_bcl_m561176A1CB70654F36596EB4FAA2A0BA18A5F6FC (void);
// 0x00000048 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics2D::bcm(UnityEngine.Rigidbody2D,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics2D_bcm_mC96ED4BD9FAFDA740603AA596DC5017F81406BC4 (void);
// 0x00000049 System.Void DG.Tweening.DOTweenModulePhysics2D/dm::.ctor()
extern void dm__ctor_m7704485ADF59862CD4F51B1F88BB6F3FE7122DA9 (void);
// 0x0000004A UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/dm::bbt()
extern void dm_bbt_m63BE38B319E5C44F6B75F20D6A6DE0742A9D7436 (void);
// 0x0000004B System.Void DG.Tweening.DOTweenModulePhysics2D/dn::.ctor()
extern void dn__ctor_m9B78216D418FF434BA188A90008C77CDCC43780D (void);
// 0x0000004C UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/dn::bbu()
extern void dn_bbu_m536CDF6B71EACBF7627D6BFBDE6D32354517BC3D (void);
// 0x0000004D System.Void DG.Tweening.DOTweenModulePhysics2D/do::.ctor()
extern void do__ctor_m1AF9DE759AC07A4B7C6C5693F7F93BD09657C745 (void);
// 0x0000004E UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/do::bbv()
extern void do_bbv_m28B4F8F0B88C3F3AD0014ABBCFC9ACD1BECF1DB0 (void);
// 0x0000004F System.Void DG.Tweening.DOTweenModulePhysics2D/dp::.ctor()
extern void dp__ctor_mF275231D9FF57EBE6D7956A648D278B4FFED103C (void);
// 0x00000050 System.Single DG.Tweening.DOTweenModulePhysics2D/dp::bbw()
extern void dp_bbw_mD15E3B2A1949021F8CDE3B791A0916203EBDECD9 (void);
// 0x00000051 System.Void DG.Tweening.DOTweenModulePhysics2D/dq::.ctor()
extern void dq__ctor_m7872F25A453CA1662D34B93B9239D2EFBB477256 (void);
// 0x00000052 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/dq::bbx()
extern void dq_bbx_m166339402617263D1D1E7FB95AF7E5F498EC4ECF (void);
// 0x00000053 System.Void DG.Tweening.DOTweenModulePhysics2D/dq::bby(UnityEngine.Vector2)
extern void dq_bby_mFEF453C822EC983BC6B6DA9F0FFA0511CD8CD16D (void);
// 0x00000054 System.Void DG.Tweening.DOTweenModulePhysics2D/dq::bbz()
extern void dq_bbz_mDA2C80A389E3FA0C7959CC63D2C7BD7392F01D46 (void);
// 0x00000055 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/dq::bca()
extern void dq_bca_mAB13B721407A1A51058B14A00BED5415E14B575C (void);
// 0x00000056 System.Void DG.Tweening.DOTweenModulePhysics2D/dq::bcb(UnityEngine.Vector2)
extern void dq_bcb_mD525CEFAA05EEB4ADE83D78994CCBC1B00490AC3 (void);
// 0x00000057 System.Void DG.Tweening.DOTweenModulePhysics2D/dq::bcc()
extern void dq_bcc_m48B287F837A4F474CBCE312C8E1D83CCC52F7D15 (void);
// 0x00000058 System.Void DG.Tweening.DOTweenModulePhysics2D/dr::.ctor()
extern void dr__ctor_m8BB219D1AE8144542E7A54D20395CB1D265D3F4D (void);
// 0x00000059 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics2D/dr::bcd()
extern void dr_bcd_m9273CC4E54FB22BA52A2E3DBC76B551759C5242C (void);
// 0x0000005A System.Void DG.Tweening.DOTweenModulePhysics2D/dr::bce(UnityEngine.Vector3)
extern void dr_bce_m4794A5742E07BA82A45B7F39C6030C4BE0EE81E9 (void);
// 0x0000005B System.Void DG.Tweening.DOTweenModulePhysics2D/ds::.ctor()
extern void ds__ctor_mDD17DBE05845C1034590C87093EE36F0FD429297 (void);
// 0x0000005C UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics2D/ds::bcf()
extern void ds_bcf_m94419222072FBAE14707EC32E10FE94411AE5F70 (void);
// 0x0000005D System.Void DG.Tweening.DOTweenModulePhysics2D/ds::bcg(UnityEngine.Vector3)
extern void ds_bcg_m739F771AD92B64A88E946ABF7336E2C7174CAB2B (void);
// 0x0000005E System.Void DG.Tweening.DOTweenModulePhysics2D/dt::.ctor()
extern void dt__ctor_m82CB54D5C1F2C6A71D70D225070AFB5C0DF58077 (void);
// 0x0000005F UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics2D/dt::bch()
extern void dt_bch_m3F4FD3D7FE1036B98A2C0C283373356E644FDD07 (void);
// 0x00000060 System.Void DG.Tweening.DOTweenModulePhysics2D/dt::bci(UnityEngine.Vector3)
extern void dt_bci_m7082B4ED1EB1D8D7D3BD3D7124272834AAFF659D (void);
// 0x00000061 System.Void DG.Tweening.DOTweenModulePhysics2D/du::.ctor()
extern void du__ctor_m471760F3BED421984E05052F9C4341E0854A277F (void);
// 0x00000062 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics2D/du::bcj()
extern void du_bcj_mED4E16041AE0EEAB27167FB29AFDE6CBC114C583 (void);
// 0x00000063 System.Void DG.Tweening.DOTweenModulePhysics2D/du::bck(UnityEngine.Vector3)
extern void du_bck_mD56A9C86BC9B092D4CE25BF94226BF0CA837D589 (void);
// 0x00000064 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOColor_m6E59DCD8638072DF9AA02645F45A4367CE903FF3 (void);
// 0x00000065 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOFade(UnityEngine.SpriteRenderer,System.Single,System.Single)
extern void DOTweenModuleSprite_DOFade_m6AE51AD4E4655B3A03EE59353450C611725E6D1A (void);
// 0x00000066 DG.Tweening.Sequence DG.Tweening.DOTweenModuleSprite::DOGradientColor(UnityEngine.SpriteRenderer,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleSprite_DOGradientColor_mBDB37BF5C3939448FCEFEB812414F303D7C99C72 (void);
// 0x00000067 DG.Tweening.Tweener DG.Tweening.DOTweenModuleSprite::DOBlendableColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOBlendableColor_m5570320359C3164713C74486272EC313724DD6F1 (void);
// 0x00000068 System.Void DG.Tweening.DOTweenModuleSprite/dv::.ctor()
extern void dv__ctor_m7A30511F992B495CC4940E66F7832D72E92C10EB (void);
// 0x00000069 UnityEngine.Color DG.Tweening.DOTweenModuleSprite/dv::bcn()
extern void dv_bcn_m61A22B9EEE07D224FAF4F2CB7A3BD75EAE5FB151 (void);
// 0x0000006A System.Void DG.Tweening.DOTweenModuleSprite/dv::bco(UnityEngine.Color)
extern void dv_bco_mB5DE6DCCE8C7BB9B6631B12B1919AA9EB0190DB8 (void);
// 0x0000006B System.Void DG.Tweening.DOTweenModuleSprite/dw::.ctor()
extern void dw__ctor_m233AEFBA70883B29767391E25CF7373611DA4E52 (void);
// 0x0000006C UnityEngine.Color DG.Tweening.DOTweenModuleSprite/dw::bcp()
extern void dw_bcp_m728AA54F1E058C0E7309411D806D9FA3D3DD7667 (void);
// 0x0000006D System.Void DG.Tweening.DOTweenModuleSprite/dw::bcq(UnityEngine.Color)
extern void dw_bcq_m5045D392AE52911044D43B45A4F88E37629B9E42 (void);
// 0x0000006E System.Void DG.Tweening.DOTweenModuleSprite/dx::.ctor()
extern void dx__ctor_mEDA8C895704FBD6C9CA68FD5810543E214EE5C90 (void);
// 0x0000006F UnityEngine.Color DG.Tweening.DOTweenModuleSprite/dx::bcr()
extern void dx_bcr_m1577F31A125D5687309AA49BD7E7C0B1BE9AB8E9 (void);
// 0x00000070 System.Void DG.Tweening.DOTweenModuleSprite/dx::bcs(UnityEngine.Color)
extern void dx_bcs_mBF5AE746C1B69786AE341D14A04B13A8D04C9C48 (void);
// 0x00000071 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mA073F33862AA7BEF33842B8200D0E79E046D114E (void);
// 0x00000072 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m961875592E62912276F701C48C3C3C3DDB4E7341 (void);
// 0x00000073 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Graphic,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mE2E3C2482CD1D3B81C2F4B0EA4E95CF2CC10CC21 (void);
// 0x00000074 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m385F3DCBA53A421BC288521879635D67E8155783 (void);
// 0x00000075 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mC1C042C14FF418F2BE7B8A6B3B73A2F8D3719EED (void);
// 0x00000076 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFillAmount(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFillAmount_mF99B81AA5E4A001FD8DEB2C7F6056271802D153D (void);
// 0x00000077 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOGradientColor(UnityEngine.UI.Image,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUI_DOGradientColor_mB08E7CC98E98E7A0A8A04DC84CDE434B038CF03E (void);
// 0x00000078 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOFlexibleSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOFlexibleSize_m2476B21F9C3D8213723B01A0FA93553D8E45F3B7 (void);
// 0x00000079 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOMinSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOMinSize_m2D92E524BAEA78CDA2815597D779B2835EF8CC03 (void);
// 0x0000007A DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPreferredSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPreferredSize_m59E62BDF15A51E4529FC3D920EBC77178DFEDFD8 (void);
// 0x0000007B DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Outline,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mAF1146331160A3A8C7FFA916BFA8B71A8531FCF2 (void);
// 0x0000007C DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Outline,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mFDEF44F0F048DD606FEF46F539D4F47AF40EF817 (void);
// 0x0000007D DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOScale(UnityEngine.UI.Outline,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOScale_mEBEA04E6C90874A4903841D8D2720867B2CA292F (void);
// 0x0000007E DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos_m5B60876E10960019C209F1C215F921E6F994831A (void);
// 0x0000007F DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosX_m483100309684443A215A8279D4E54B2769DAF3E8 (void);
// 0x00000080 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosY_mBCFDB7E014D45E6B56588C598C23D4EB111D1395 (void);
// 0x00000081 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3D(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3D_mBFA28183F6CBCCD444AF47AA85A0272E8D1CAA48 (void);
// 0x00000082 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DX_m6AE58A54659B9562734FB13539AE84FA69C34980 (void);
// 0x00000083 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DY_mC051E07149C6D443662E3682338A26D0ADC6591C (void);
// 0x00000084 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DZ(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DZ_m334F6667A6103C39B84749DC1EBE6557CCBE91D3 (void);
// 0x00000085 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMax(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMax_mA7BFD0284B3A23C3BF4DE76F542F60EAE1CC5368 (void);
// 0x00000086 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMin(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMin_m7EC8E45746F25D2679DE1267FBD5DEB4CC619057 (void);
// 0x00000087 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivot(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOPivot_m3F8012446C42571C9F1549771422917AEC9EEDF8 (void);
// 0x00000088 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotX(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotX_mFD1858E2B5933F093EB84E3D87A69783B04CA7F4 (void);
// 0x00000089 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotY(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotY_m7CA2B8D7162770DC0700106C9ADB97F9A4BD877B (void);
// 0x0000008A DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOSizeDelta(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOSizeDelta_mC2236310385A8326B78FE900A33586006C8B4474 (void);
// 0x0000008B DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOPunchAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPunchAnchorPos_mCE5DF2A8B462E813FBB84C4DBC5697848D1C2B45 (void);
// 0x0000008C DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,System.Single,System.Int32,System.Single,System.Boolean,System.Boolean,DG.Tweening.ShakeRandomnessMode)
extern void DOTweenModuleUI_DOShakeAnchorPos_m12477B21C8CD40EF1A6CA4F73A6AF1E35883B992 (void);
// 0x0000008D DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,UnityEngine.Vector2,System.Int32,System.Single,System.Boolean,System.Boolean,DG.Tweening.ShakeRandomnessMode)
extern void DOTweenModuleUI_DOShakeAnchorPos_m919C4BA7526A3CEA6FF8F4CF669FB7681E2893BB (void);
// 0x0000008E DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOJumpAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOJumpAnchorPos_m5629EA273819E9C2EE98068F8793202EC96765F3 (void);
// 0x0000008F DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DONormalizedPos(UnityEngine.UI.ScrollRect,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DONormalizedPos_mABC0EB2280EEC8C8A990E76E16D7BB5DA48A1706 (void);
// 0x00000090 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOHorizontalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOHorizontalNormalizedPos_m41E8A271B7FE392B43523F6B123DCBF26CB0BA74 (void);
// 0x00000091 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOVerticalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOVerticalNormalizedPos_mC141061A07ACD836F365221BA33198FF316BFBA3 (void);
// 0x00000092 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOValue(UnityEngine.UI.Slider,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOValue_m38DB0CB09A73E2DDC33B3EEE49D74944FBA82A38 (void);
// 0x00000093 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mAF96B607A774C0B0D912E56C3C11A87E4F093440 (void);
// 0x00000094 DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions> DG.Tweening.DOTweenModuleUI::DOCounter(UnityEngine.UI.Text,System.Int32,System.Int32,System.Single,System.Boolean,System.Globalization.CultureInfo)
extern void DOTweenModuleUI_DOCounter_m85EC79863973B9D9DF9D67E7B5DD835C86BF1438 (void);
// 0x00000095 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m5AD3262DBFA4B713B61D3F2203B2930562A3B54D (void);
// 0x00000096 DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions> DG.Tweening.DOTweenModuleUI::DOText(UnityEngine.UI.Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern void DOTweenModuleUI_DOText_mDE24022401FC6820F90F1BE517FF46F73AE9BC15 (void);
// 0x00000097 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m69717A08080A14BE5A01F1E8886EBF829CEF7227 (void);
// 0x00000098 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m9D9F47100CD74C031BBA7ABF9A16E45AC29D2E8F (void);
// 0x00000099 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m7569CFBAB6EFA6049EECFDBC96898880565C368A (void);
// 0x0000009A DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.CircleOptions> DG.Tweening.DOTweenModuleUI::DOShapeCircle(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShapeCircle_m0A5FCD4C41E4B1FF07B94647146806013735FAE4 (void);
// 0x0000009B UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/Utils::SwitchToRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
extern void Utils_SwitchToRectTransform_m6A264948787CBEE108E0D97B235AE921BFBB6152 (void);
// 0x0000009C System.Void DG.Tweening.DOTweenModuleUI/dy::.ctor()
extern void dy__ctor_mE4F46130E864DE7BC38EBA79DED520AC1B5002AD (void);
// 0x0000009D System.Single DG.Tweening.DOTweenModuleUI/dy::bct()
extern void dy_bct_m44A248CA7CECB087422215F4EFDB4D36A9E1AE3C (void);
// 0x0000009E System.Void DG.Tweening.DOTweenModuleUI/dy::bcu(System.Single)
extern void dy_bcu_m50B11C4F7E416C1FA8A72EB34F72643CE1848357 (void);
// 0x0000009F System.Void DG.Tweening.DOTweenModuleUI/dz::.ctor()
extern void dz__ctor_mEEDE9D4F5BEA81753CA7812087368522F11B0FB4 (void);
// 0x000000A0 UnityEngine.Color DG.Tweening.DOTweenModuleUI/dz::bcv()
extern void dz_bcv_m0FF01E2AAE3ED426AE92CD992518F6735F8DA3B9 (void);
// 0x000000A1 System.Void DG.Tweening.DOTweenModuleUI/dz::bcw(UnityEngine.Color)
extern void dz_bcw_m2AC34505EB5442ED013BDC00FF331AECF2BA00F1 (void);
// 0x000000A2 System.Void DG.Tweening.DOTweenModuleUI/ea::.ctor()
extern void ea__ctor_m99AF733F0412C4FADD0856D387B5BED745E3F1E9 (void);
// 0x000000A3 UnityEngine.Color DG.Tweening.DOTweenModuleUI/ea::bcx()
extern void ea_bcx_m2F1F9F014D8EE8448A573DFA2A2AAC8B7F845CCF (void);
// 0x000000A4 System.Void DG.Tweening.DOTweenModuleUI/ea::bcy(UnityEngine.Color)
extern void ea_bcy_m1046D91B0A016B38E09DF5E54359D93E95F8CED2 (void);
// 0x000000A5 System.Void DG.Tweening.DOTweenModuleUI/eb::.ctor()
extern void eb__ctor_m7E91F8BBB97290C0855A57BAD48A19887A5B566C (void);
// 0x000000A6 UnityEngine.Color DG.Tweening.DOTweenModuleUI/eb::bcz()
extern void eb_bcz_mB0718B20D75EBE327FE327646F1623F3162D9639 (void);
// 0x000000A7 System.Void DG.Tweening.DOTweenModuleUI/eb::bda(UnityEngine.Color)
extern void eb_bda_mABAEDE6234F9E77847364FCB60AA5709E8247139 (void);
// 0x000000A8 System.Void DG.Tweening.DOTweenModuleUI/ec::.ctor()
extern void ec__ctor_mD9B18AD35FA48C900ECB3FE7BA3BED7CF94B407D (void);
// 0x000000A9 UnityEngine.Color DG.Tweening.DOTweenModuleUI/ec::bdb()
extern void ec_bdb_m28DE708F569571D467238116FA93566B2DA5A09D (void);
// 0x000000AA System.Void DG.Tweening.DOTweenModuleUI/ec::bdc(UnityEngine.Color)
extern void ec_bdc_mC0F08EA457A80AD94E331B115297F8FA14175A79 (void);
// 0x000000AB System.Void DG.Tweening.DOTweenModuleUI/ed::.ctor()
extern void ed__ctor_mC6DD6B11CC2FB749F171253273837E8A9C83C674 (void);
// 0x000000AC System.Single DG.Tweening.DOTweenModuleUI/ed::bdd()
extern void ed_bdd_m8E354C436AD73EB7685F1C2A8C49D846F1DCD66E (void);
// 0x000000AD System.Void DG.Tweening.DOTweenModuleUI/ed::bde(System.Single)
extern void ed_bde_mD6C795DB29524B521D10ADA85C75781F13A6F6CF (void);
// 0x000000AE System.Void DG.Tweening.DOTweenModuleUI/ee::.ctor()
extern void ee__ctor_m10245C84EB6DC748FF2AC823EDC7CEDB37C49E59 (void);
// 0x000000AF UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/ee::bdf()
extern void ee_bdf_mF2A03CF4DFD3C3679EE1FE053E50C41FB2BE1178 (void);
// 0x000000B0 System.Void DG.Tweening.DOTweenModuleUI/ee::bdg(UnityEngine.Vector2)
extern void ee_bdg_mEC650A9E1A9FE038A1D8BBBBA4373A54C9DDF4E4 (void);
// 0x000000B1 System.Void DG.Tweening.DOTweenModuleUI/ef::.ctor()
extern void ef__ctor_m1751B12ACA3B3378671E6D8431277AE94D17E3FB (void);
// 0x000000B2 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/ef::bdh()
extern void ef_bdh_m23A64A8657B77253F08CB90935AA0660FAE68612 (void);
// 0x000000B3 System.Void DG.Tweening.DOTweenModuleUI/ef::bdi(UnityEngine.Vector2)
extern void ef_bdi_m6C5F1544174F005A9D8A951EE0DDE2F20B55F7F5 (void);
// 0x000000B4 System.Void DG.Tweening.DOTweenModuleUI/eg::.ctor()
extern void eg__ctor_m6772EA75EEB508BBCF6A095319D16A8EE382824B (void);
// 0x000000B5 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/eg::bdj()
extern void eg_bdj_mC8BA268280A300079CBE0482480842F7967A248E (void);
// 0x000000B6 System.Void DG.Tweening.DOTweenModuleUI/eg::bdk(UnityEngine.Vector2)
extern void eg_bdk_mAC6CEDEDC6F01B683FA132B3097C9B32216B7CCC (void);
// 0x000000B7 System.Void DG.Tweening.DOTweenModuleUI/eh::.ctor()
extern void eh__ctor_m3AF7C63099C38DF1EA025780A2E3ED9BC15F3D43 (void);
// 0x000000B8 UnityEngine.Color DG.Tweening.DOTweenModuleUI/eh::bdl()
extern void eh_bdl_m26C7A86D1A5119D89B8CBA3813DA2A9749143E76 (void);
// 0x000000B9 System.Void DG.Tweening.DOTweenModuleUI/eh::bdm(UnityEngine.Color)
extern void eh_bdm_mF670A0EEB67993A4D4D5E59AD9FFD51508479CAD (void);
// 0x000000BA System.Void DG.Tweening.DOTweenModuleUI/ei::.ctor()
extern void ei__ctor_m475BB1AA82C38F30EBDECDF6A95B06506DE85C04 (void);
// 0x000000BB UnityEngine.Color DG.Tweening.DOTweenModuleUI/ei::bdn()
extern void ei_bdn_mEB27D585D0019E968EE982FED4F75B254B73FCF8 (void);
// 0x000000BC System.Void DG.Tweening.DOTweenModuleUI/ei::bdo(UnityEngine.Color)
extern void ei_bdo_m89491E040AB47EFCB6E999DE2CA110213FAD66EC (void);
// 0x000000BD System.Void DG.Tweening.DOTweenModuleUI/ej::.ctor()
extern void ej__ctor_m251F85E9830FC286811A851C3AB216D9B945A3E6 (void);
// 0x000000BE UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/ej::bdp()
extern void ej_bdp_mAFE2A3418A47996D807372ABBEF47579FEDAE202 (void);
// 0x000000BF System.Void DG.Tweening.DOTweenModuleUI/ej::bdq(UnityEngine.Vector2)
extern void ej_bdq_mD6CB3B0C796BBF15FC87896BD7274BA5EA861B66 (void);
// 0x000000C0 System.Void DG.Tweening.DOTweenModuleUI/ek::.ctor()
extern void ek__ctor_m78B61B9C086B221D572E84AE7A36EAA74AFB83FC (void);
// 0x000000C1 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/ek::bdr()
extern void ek_bdr_m2CE48EB125771E1CA9966E28EF1508357251DFB4 (void);
// 0x000000C2 System.Void DG.Tweening.DOTweenModuleUI/ek::bds(UnityEngine.Vector2)
extern void ek_bds_m5CA64C63BBCC3246EFB417630042D77E2F05CB0E (void);
// 0x000000C3 System.Void DG.Tweening.DOTweenModuleUI/el::.ctor()
extern void el__ctor_m267028B1EE49C316617B4D214A0324091ECB8A3A (void);
// 0x000000C4 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/el::bdt()
extern void el_bdt_m5FFBBFF7884261900BEA74F05B1EE3CCCA9F3D60 (void);
// 0x000000C5 System.Void DG.Tweening.DOTweenModuleUI/el::bdu(UnityEngine.Vector2)
extern void el_bdu_m7A5E28242E64100A5EF8D99A56DAFC4453E6B68F (void);
// 0x000000C6 System.Void DG.Tweening.DOTweenModuleUI/em::.ctor()
extern void em__ctor_m05F9EAD54DB493E97FEEEF622E08255164AFDA69 (void);
// 0x000000C7 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/em::bdv()
extern void em_bdv_m7137E0BC00B3AD445B48A06BE457CA80683274C9 (void);
// 0x000000C8 System.Void DG.Tweening.DOTweenModuleUI/em::bdw(UnityEngine.Vector2)
extern void em_bdw_mD2CBA4B8F2ED1EDB32A181FA8962EC8E8FDF1991 (void);
// 0x000000C9 System.Void DG.Tweening.DOTweenModuleUI/en::.ctor()
extern void en__ctor_mC7B55A491831CA1A213602633983FDC620B7AFB6 (void);
// 0x000000CA UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/en::bdx()
extern void en_bdx_m72F21B71ADF5694686B3BC2933ED1FDA29ED386A (void);
// 0x000000CB System.Void DG.Tweening.DOTweenModuleUI/en::bdy(UnityEngine.Vector3)
extern void en_bdy_m4FF38316AE1ADA57223B60B9B2C2E50185C1F4C9 (void);
// 0x000000CC System.Void DG.Tweening.DOTweenModuleUI/eo::.ctor()
extern void eo__ctor_m4C5EA2B567A867F69C23648744D957FF5F068A47 (void);
// 0x000000CD UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/eo::bdz()
extern void eo_bdz_mD241988C7B248736C0225318E8356A0DC7E2C0A3 (void);
// 0x000000CE System.Void DG.Tweening.DOTweenModuleUI/eo::bea(UnityEngine.Vector3)
extern void eo_bea_m0334A75C6857A73B55DA2EDA443496FE47CD29B5 (void);
// 0x000000CF System.Void DG.Tweening.DOTweenModuleUI/ep::.ctor()
extern void ep__ctor_m837A8EAD5A76BDA4E61CBAB9B5456B5F825BBA05 (void);
// 0x000000D0 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/ep::beb()
extern void ep_beb_m46A3E74A81BA66DF67D7A3552CB2CA0BF594168C (void);
// 0x000000D1 System.Void DG.Tweening.DOTweenModuleUI/ep::bec(UnityEngine.Vector3)
extern void ep_bec_m8D6666679BE61C8AD9B3A8A38B115ED41916798C (void);
// 0x000000D2 System.Void DG.Tweening.DOTweenModuleUI/eq::.ctor()
extern void eq__ctor_m6AFDAD8D316975409BF1F89AE83FA232C92CF72E (void);
// 0x000000D3 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/eq::bed()
extern void eq_bed_m78D6A9A4C169D676C7532340A8134B1BE21198E2 (void);
// 0x000000D4 System.Void DG.Tweening.DOTweenModuleUI/eq::bee(UnityEngine.Vector3)
extern void eq_bee_m763609B224CEEAB50A59A3F3617C7403CC7FFAE5 (void);
// 0x000000D5 System.Void DG.Tweening.DOTweenModuleUI/er::.ctor()
extern void er__ctor_m3C75B2A6E21BFEC890202FDBC14858E49FCD6F7A (void);
// 0x000000D6 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/er::bef()
extern void er_bef_m6100B8336BC3E825833749268E183919D860E5FA (void);
// 0x000000D7 System.Void DG.Tweening.DOTweenModuleUI/er::beg(UnityEngine.Vector2)
extern void er_beg_m469710C96CD70B0B83ECCD307D278BA2F579DF23 (void);
// 0x000000D8 System.Void DG.Tweening.DOTweenModuleUI/es::.ctor()
extern void es__ctor_mBDF6EC64C9D9F4F76542B5628EE5DEB18B1A33FC (void);
// 0x000000D9 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/es::beh()
extern void es_beh_mEC80ACD606128810CD5591F3B3AB95C0F6EB28F6 (void);
// 0x000000DA System.Void DG.Tweening.DOTweenModuleUI/es::bei(UnityEngine.Vector2)
extern void es_bei_mF2EE49868BCF32B313EE3EC3C4A6469CCC18C520 (void);
// 0x000000DB System.Void DG.Tweening.DOTweenModuleUI/et::.ctor()
extern void et__ctor_m8035CAF052CD8844BCAF65C57F3648B8CFFBCA2B (void);
// 0x000000DC UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/et::bej()
extern void et_bej_mC10D8CBB1F18E54356CE5E3F57960BF2C1866D69 (void);
// 0x000000DD System.Void DG.Tweening.DOTweenModuleUI/et::bek(UnityEngine.Vector2)
extern void et_bek_m36D263BAC5397036850EFC6D5AFF5966B6009625 (void);
// 0x000000DE System.Void DG.Tweening.DOTweenModuleUI/eu::.ctor()
extern void eu__ctor_m9D4A6A900173936B9D3675123CA8AB97D8C42B4B (void);
// 0x000000DF UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/eu::bel()
extern void eu_bel_mEC31344992863C08C32A39AFF11DEA5CAAF5EBC7 (void);
// 0x000000E0 System.Void DG.Tweening.DOTweenModuleUI/eu::bem(UnityEngine.Vector2)
extern void eu_bem_mF4783A99CE56AF1D1347FCE4CC19AEBE78134331 (void);
// 0x000000E1 System.Void DG.Tweening.DOTweenModuleUI/ev::.ctor()
extern void ev__ctor_mF45B45769C0B9AA2B5A26CB0943CEA036177A3D7 (void);
// 0x000000E2 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/ev::ben()
extern void ev_ben_mFBECDCD464210BE9D4786874F2AAEBF96B3EB70F (void);
// 0x000000E3 System.Void DG.Tweening.DOTweenModuleUI/ev::beo(UnityEngine.Vector2)
extern void ev_beo_m35C0D0094ECC520CD233D27D69E3F9C48B3D9E54 (void);
// 0x000000E4 System.Void DG.Tweening.DOTweenModuleUI/ew::.ctor()
extern void ew__ctor_mD842B4ADDEEEE28A2D1027C99D7E46DF268BF2CD (void);
// 0x000000E5 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/ew::bep()
extern void ew_bep_m2E27FFE535D5A4A185FBE041B49111F35AFDEDD0 (void);
// 0x000000E6 System.Void DG.Tweening.DOTweenModuleUI/ew::beq(UnityEngine.Vector2)
extern void ew_beq_m6DB9FE0D6ABEC3527E52593CB0D560D9C8E6A699 (void);
// 0x000000E7 System.Void DG.Tweening.DOTweenModuleUI/ex::.ctor()
extern void ex__ctor_mA1E3F7DBBA4CC33EB4E9982690E985EE4424115E (void);
// 0x000000E8 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/ex::ber()
extern void ex_ber_m9C6AE962D6A2E8655B8F019DF5FCBFE4049BEDB8 (void);
// 0x000000E9 System.Void DG.Tweening.DOTweenModuleUI/ex::bes(UnityEngine.Vector3)
extern void ex_bes_m095FE019172F54C44D377B803E578DBD03491F16 (void);
// 0x000000EA System.Void DG.Tweening.DOTweenModuleUI/ey::.ctor()
extern void ey__ctor_mCD2E752C7402602967B22CB4C216E8118D6ADDD6 (void);
// 0x000000EB UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/ey::bet()
extern void ey_bet_mA7C41897A449820871E9B7AD070F955F73B7C850 (void);
// 0x000000EC System.Void DG.Tweening.DOTweenModuleUI/ey::beu(UnityEngine.Vector3)
extern void ey_beu_m0EA4CDA03F4CC9F01464F27DC36FA9F21304DAA2 (void);
// 0x000000ED System.Void DG.Tweening.DOTweenModuleUI/ez::.ctor()
extern void ez__ctor_m260C5C7200A0C225604C116CAA2EA501B21920E1 (void);
// 0x000000EE UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/ez::bev()
extern void ez_bev_mA7CCEBD55A64A8855A286F4AA00A40F6163A7842 (void);
// 0x000000EF System.Void DG.Tweening.DOTweenModuleUI/ez::bew(UnityEngine.Vector3)
extern void ez_bew_mE7F2C9AFBEC9E58AA9E336B9103B4254579DA562 (void);
// 0x000000F0 System.Void DG.Tweening.DOTweenModuleUI/fa::.ctor()
extern void fa__ctor_m7849F665248329349A45195C2D1B77684C412D5A (void);
// 0x000000F1 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/fa::bex()
extern void fa_bex_m75AC3DC9774E97501C08399DCEB405A175EA5854 (void);
// 0x000000F2 System.Void DG.Tweening.DOTweenModuleUI/fa::bey(UnityEngine.Vector2)
extern void fa_bey_mCF475F66DA0BB233C116A66BF995FA901C12873C (void);
// 0x000000F3 System.Void DG.Tweening.DOTweenModuleUI/fa::bez()
extern void fa_bez_m64F164E24EBD9291A59183F5F548077A5F7957BD (void);
// 0x000000F4 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/fa::bfa()
extern void fa_bfa_mDDB58E90FB67CD572B946FE48F29CC845FFE05C3 (void);
// 0x000000F5 System.Void DG.Tweening.DOTweenModuleUI/fa::bfb(UnityEngine.Vector2)
extern void fa_bfb_m239D4F8F0189F4311182532B6BC8F70613AC2E21 (void);
// 0x000000F6 System.Void DG.Tweening.DOTweenModuleUI/fa::bfc()
extern void fa_bfc_mEE061203C61E901D933E0FFA069DE0013732569F (void);
// 0x000000F7 System.Void DG.Tweening.DOTweenModuleUI/fb::.ctor()
extern void fb__ctor_m788489B5A214A6B0AD358C169FC1A18655305658 (void);
// 0x000000F8 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/fb::bfd()
extern void fb_bfd_mD6DED0C77AF9662B996E155CFD27F1074489AB70 (void);
// 0x000000F9 System.Void DG.Tweening.DOTweenModuleUI/fb::bfe(UnityEngine.Vector2)
extern void fb_bfe_m0F3D5DC8B38C21F23435310E7FD9ECA7D02D16DE (void);
// 0x000000FA System.Void DG.Tweening.DOTweenModuleUI/fc::.ctor()
extern void fc__ctor_m84E9DE8BF07161EADA7BFD8062240CFB118B0A4F (void);
// 0x000000FB System.Single DG.Tweening.DOTweenModuleUI/fc::bff()
extern void fc_bff_m315F514242C6FC5E7E6BBD36409A4F1F1C42303A (void);
// 0x000000FC System.Void DG.Tweening.DOTweenModuleUI/fc::bfg(System.Single)
extern void fc_bfg_m8FDCEDC13307DDFF4B26B6902E56D6BFECD7AE76 (void);
// 0x000000FD System.Void DG.Tweening.DOTweenModuleUI/fd::.ctor()
extern void fd__ctor_mFE7257161A84AAE6E6E720A974558F686ECCC2AC (void);
// 0x000000FE System.Single DG.Tweening.DOTweenModuleUI/fd::bfh()
extern void fd_bfh_m774CCD224563101163E10403AE1CA478CDC55000 (void);
// 0x000000FF System.Void DG.Tweening.DOTweenModuleUI/fd::bfi(System.Single)
extern void fd_bfi_m319BF1239F9197089FDBAA674B45DB54A0536FD8 (void);
// 0x00000100 System.Void DG.Tweening.DOTweenModuleUI/fe::.ctor()
extern void fe__ctor_m6669F78B55570F292B56490E9095D33F945F6631 (void);
// 0x00000101 System.Single DG.Tweening.DOTweenModuleUI/fe::bfj()
extern void fe_bfj_m97F1AF8A0C75292E42EBF3C2DBDC38B25DA5FEE0 (void);
// 0x00000102 System.Void DG.Tweening.DOTweenModuleUI/fe::bfk(System.Single)
extern void fe_bfk_m32412797A5C14F94AE8C761F5E3F2B68225453EB (void);
// 0x00000103 System.Void DG.Tweening.DOTweenModuleUI/ff::.ctor()
extern void ff__ctor_m2C10047CEB633435F9609021C6E26CB130EA6B5F (void);
// 0x00000104 UnityEngine.Color DG.Tweening.DOTweenModuleUI/ff::bfl()
extern void ff_bfl_m4FC8EAF7880E388D37BC02AF7A1F33EAC6016C92 (void);
// 0x00000105 System.Void DG.Tweening.DOTweenModuleUI/ff::bfm(UnityEngine.Color)
extern void ff_bfm_m2B635BCE9B642C42F093DC06A5AE70D027CAE0D1 (void);
// 0x00000106 System.Void DG.Tweening.DOTweenModuleUI/fg::.ctor()
extern void fg__ctor_mF1AF56A8BB88F68B97B0246CD209A3480040A1E4 (void);
// 0x00000107 System.Int32 DG.Tweening.DOTweenModuleUI/fg::bfn()
extern void fg_bfn_m7749725079A05CB7D52C512338FD31BAA2D287BA (void);
// 0x00000108 System.Void DG.Tweening.DOTweenModuleUI/fg::bfo(System.Int32)
extern void fg_bfo_m99C89120EF8FBA323B0BF99638991272B2199136 (void);
// 0x00000109 System.Void DG.Tweening.DOTweenModuleUI/fh::.ctor()
extern void fh__ctor_m892D1C680E6EFCFE8659FFCF25D5EB8C16628299 (void);
// 0x0000010A UnityEngine.Color DG.Tweening.DOTweenModuleUI/fh::bfp()
extern void fh_bfp_m3B3C851EF608859977526E78BCFF7660D9565999 (void);
// 0x0000010B System.Void DG.Tweening.DOTweenModuleUI/fh::bfq(UnityEngine.Color)
extern void fh_bfq_mCC9FEF22604A520456750FE6C81211A6A49D4C44 (void);
// 0x0000010C System.Void DG.Tweening.DOTweenModuleUI/fi::.ctor()
extern void fi__ctor_m6DB4423B9BB10F62E89053F84F5ED51540EFAF1D (void);
// 0x0000010D System.String DG.Tweening.DOTweenModuleUI/fi::bfr()
extern void fi_bfr_m460399ABB62EBA5EE4846DA9BFACCBB608FF3190 (void);
// 0x0000010E System.Void DG.Tweening.DOTweenModuleUI/fi::bfs(System.String)
extern void fi_bfs_mBE30D372AD18DB0CC86A839E2B77AD68F227F03F (void);
// 0x0000010F System.Void DG.Tweening.DOTweenModuleUI/fj::.ctor()
extern void fj__ctor_mC8F102840D1917115490F28067412D08629D6AA9 (void);
// 0x00000110 UnityEngine.Color DG.Tweening.DOTweenModuleUI/fj::bft()
extern void fj_bft_mBD387F944F9F604BD54237D2F96EFCA2107C1EE8 (void);
// 0x00000111 System.Void DG.Tweening.DOTweenModuleUI/fj::bfu(UnityEngine.Color)
extern void fj_bfu_m1E8BA1F8831723096AB3427877FD83DA4BD78EFD (void);
// 0x00000112 System.Void DG.Tweening.DOTweenModuleUI/fk::.ctor()
extern void fk__ctor_m73DCC65C29FBEEB61D1985CD39AB9267A677FA1C (void);
// 0x00000113 UnityEngine.Color DG.Tweening.DOTweenModuleUI/fk::bfv()
extern void fk_bfv_m2656A13A55A629F0ECC87792224DCE530BD9BF6F (void);
// 0x00000114 System.Void DG.Tweening.DOTweenModuleUI/fk::bfw(UnityEngine.Color)
extern void fk_bfw_mFB9E679DF932CEE8FE22ACF7076EE3E9FDE0C3AB (void);
// 0x00000115 System.Void DG.Tweening.DOTweenModuleUI/fl::.ctor()
extern void fl__ctor_mF82268188F9ABE8234C747EB1A27D091F3D6BAA9 (void);
// 0x00000116 UnityEngine.Color DG.Tweening.DOTweenModuleUI/fl::bfx()
extern void fl_bfx_mD8B942989081C6FE9A6AD57CFEE0AC9C357124DC (void);
// 0x00000117 System.Void DG.Tweening.DOTweenModuleUI/fl::bfy(UnityEngine.Color)
extern void fl_bfy_m1E9AA41FC34D872DBE018CEF3A6DC208ACE8FD47 (void);
// 0x00000118 System.Void DG.Tweening.DOTweenModuleUI/fm::.ctor()
extern void fm__ctor_m29944AD3DD9DE3D6A441AD219C809CABBD42CCAE (void);
// 0x00000119 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/fm::bfz()
extern void fm_bfz_mBA6336EADB0F13C78039892A66AB272A73385F2E (void);
// 0x0000011A System.Void DG.Tweening.DOTweenModuleUI/fm::bga(UnityEngine.Vector2)
extern void fm_bga_mDE9C4E0DC66EE5BC7A952312FFF24D052844BB0F (void);
// 0x0000011B DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_m5989D5E5CCAA8B96FEC40CE899DDD9BC255F4126 (void);
// 0x0000011C DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.String,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_m16A5065B7DA6EA92AC15D1E186EE14617F64896B (void);
// 0x0000011D UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForCompletion(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForCompletion_m080081BC7D031CAE8CB22FC166C6E19FAA91295C (void);
// 0x0000011E UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForRewind(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForRewind_m7EB6AB643D5F309EE41F15305ADB1324E16853A7 (void);
// 0x0000011F UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForKill(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForKill_m9592B2D81F0C76C9F8003645BDB0C4C3BA4E6A3B (void);
// 0x00000120 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForElapsedLoops(DG.Tweening.Tween,System.Int32,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForElapsedLoops_m08D7DB6E86076A4D3224DE7F651529C03B0C9B93 (void);
// 0x00000121 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForPosition(DG.Tweening.Tween,System.Single,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForPosition_m918A05EB65877997F48B218DE2B9B95A4E198A0E (void);
// 0x00000122 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForStart(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForStart_m3E6DF08DBF844D468C3DD2A14AB96AAF3717BA53 (void);
// 0x00000123 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOOffset(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOOffset_mB985D0A38167056313BD49AA97A9A2F12B6C1CA2 (void);
// 0x00000124 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOTiling(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOTiling_m354938745AA34ED1539D3CBF54C94A5E5753B903 (void);
// 0x00000125 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForCompletion(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForCompletion_m47D1F5AACF019F8C3233238D5B73EBD325685681 (void);
// 0x00000126 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForRewind(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForRewind_m4B4AD3964B5CBB9F56BB1050E8EF7C0AF414060E (void);
// 0x00000127 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForKill(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForKill_m4CE6BA6CDF93365F4D14CE2909C7F7FF6B332928 (void);
// 0x00000128 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForElapsedLoops(DG.Tweening.Tween,System.Int32)
extern void DOTweenModuleUnityVersion_AsyncWaitForElapsedLoops_m08434CDEC811346692A3795B1736C8499E391277 (void);
// 0x00000129 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForPosition(DG.Tweening.Tween,System.Single)
extern void DOTweenModuleUnityVersion_AsyncWaitForPosition_mA1767089E7840940CEB85BEAF15FBC02CE56D487 (void);
// 0x0000012A System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForStart(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForStart_m3BAC94C50C109B2795BAA10D582617B3F2F95126 (void);
// 0x0000012B System.Void DG.Tweening.DOTweenModuleUnityVersion/fn::.ctor()
extern void fn__ctor_mB30C0F2C702B322FD5695D1AA80054E21D0597EF (void);
// 0x0000012C UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion/fn::bgb()
extern void fn_bgb_mD2875C8F96A2A4966041A1F72C0D231457E3CB9E (void);
// 0x0000012D System.Void DG.Tweening.DOTweenModuleUnityVersion/fn::bgc(UnityEngine.Vector2)
extern void fn_bgc_m877D40B3191027F3259023A08E291607645873D8 (void);
// 0x0000012E System.Void DG.Tweening.DOTweenModuleUnityVersion/fo::.ctor()
extern void fo__ctor_m91305CABABD2927201AD41057147AA9AE812BDFB (void);
// 0x0000012F UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion/fo::bgd()
extern void fo_bgd_m93A426B05C34E3ABEBC0C7FE4584D5222AE6B102 (void);
// 0x00000130 System.Void DG.Tweening.DOTweenModuleUnityVersion/fo::bge(UnityEngine.Vector2)
extern void fo_bge_mDF2E1112BA2A37EBEA24DD8370172A92D0A7970F (void);
// 0x00000131 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::MoveNext()
extern void U3CAsyncWaitForCompletionU3Ed__10_MoveNext_m7AE96D202005CC27A2EBEB47057FADA081D6DCDA (void);
// 0x00000132 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_m6240E5A12BCFEF8EA8588C4C18B3BB48BBA71FBB (void);
// 0x00000133 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::MoveNext()
extern void U3CAsyncWaitForRewindU3Ed__11_MoveNext_m5E01823C6DBEFF1763AD6606972918D90935D340 (void);
// 0x00000134 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_m3F25AF370573B0A32A25D5D01483AF62C16AD5C3 (void);
// 0x00000135 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::MoveNext()
extern void U3CAsyncWaitForKillU3Ed__12_MoveNext_mC10BB60238B855C88268AEE7DA0D7E8CF59AC885 (void);
// 0x00000136 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m7BC3B905F4C08F129E4E6DFD12DDA73E5756DD0B (void);
// 0x00000137 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::MoveNext()
extern void U3CAsyncWaitForElapsedLoopsU3Ed__13_MoveNext_mF44348C1C974B48E1B0F6FE03AD1F713A4A6F8A2 (void);
// 0x00000138 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m4D5053D74FA671E83FA48E11A3EE5C6534F995AB (void);
// 0x00000139 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::MoveNext()
extern void U3CAsyncWaitForPositionU3Ed__14_MoveNext_mB726C581B4A8055414678A86C800C4A0525E5121 (void);
// 0x0000013A System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_m060A3B894F6700B46D57FD2443F396C7C9CC7111 (void);
// 0x0000013B System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::MoveNext()
extern void U3CAsyncWaitForStartU3Ed__15_MoveNext_mDBD2BBC1B727D4C915B0E2AB910330411086BC12 (void);
// 0x0000013C System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForStartU3Ed__15_SetStateMachine_m3B0B96BE14F355F5A0AF74DD42C7774158140A41 (void);
// 0x0000013D System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForCompletion::get_keepWaiting()
extern void WaitForCompletion_get_keepWaiting_mD2C729ECA6B6F6BE57B718492953D588237F743D (void);
// 0x0000013E System.Void DG.Tweening.DOTweenCYInstruction/WaitForCompletion::.ctor(DG.Tweening.Tween)
extern void WaitForCompletion__ctor_mE85B7CC8B7FE6A2AE84DB8210249A61018118C4D (void);
// 0x0000013F System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForRewind::get_keepWaiting()
extern void WaitForRewind_get_keepWaiting_mF46CA00D1A5FDF140C0C027C4109C7373245A16C (void);
// 0x00000140 System.Void DG.Tweening.DOTweenCYInstruction/WaitForRewind::.ctor(DG.Tweening.Tween)
extern void WaitForRewind__ctor_mD79A34DBDAC1B30FBEBD36A8D749EC81935520B2 (void);
// 0x00000141 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForKill::get_keepWaiting()
extern void WaitForKill_get_keepWaiting_m4C085E03853426F4D4F5C312334101AE9BA75E3F (void);
// 0x00000142 System.Void DG.Tweening.DOTweenCYInstruction/WaitForKill::.ctor(DG.Tweening.Tween)
extern void WaitForKill__ctor_m75C2D3D54DBBBB35DE297B947C08C982CABF1BAF (void);
// 0x00000143 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::get_keepWaiting()
extern void WaitForElapsedLoops_get_keepWaiting_mC2A906DBE9A06B7697041B76EC7C6AF58F928D6C (void);
// 0x00000144 System.Void DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::.ctor(DG.Tweening.Tween,System.Int32)
extern void WaitForElapsedLoops__ctor_mA0E7D5A115AB56AD618E24B320476B81B9CAEC7A (void);
// 0x00000145 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForPosition::get_keepWaiting()
extern void WaitForPosition_get_keepWaiting_mA71AB7B7C269AB062BB6AF9CDF42E0F54EA6549F (void);
// 0x00000146 System.Void DG.Tweening.DOTweenCYInstruction/WaitForPosition::.ctor(DG.Tweening.Tween,System.Single)
extern void WaitForPosition__ctor_mB6CCFCE59F142931CF19AC3BA4FE5CAFF1CBA4AD (void);
// 0x00000147 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForStart::get_keepWaiting()
extern void WaitForStart_get_keepWaiting_mA0B7858981A2CEDE516D5F2C5133629C6D537E0B (void);
// 0x00000148 System.Void DG.Tweening.DOTweenCYInstruction/WaitForStart::.ctor(DG.Tweening.Tween)
extern void WaitForStart__ctor_m3AEC2F6E37815ED0D94C2FA5F3B784456C6E49D1 (void);
// 0x00000149 System.Void DG.Tweening.DOTweenModuleUtils::Init()
extern void DOTweenModuleUtils_Init_m8FD2F0DFE3D768ECE332A27B64ED208ED53568D5 (void);
// 0x0000014A System.Void DG.Tweening.DOTweenModuleUtils::bgf()
extern void DOTweenModuleUtils_bgf_m1486C03354670D09C17BCC5CC626740305B36530 (void);
// 0x0000014B System.Void DG.Tweening.DOTweenModuleUtils/Physics::SetOrientationOnPath(DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform)
extern void Physics_SetOrientationOnPath_mC2C4E6FA124458E00765F647C3E824CC31A4696D (void);
// 0x0000014C System.Boolean DG.Tweening.DOTweenModuleUtils/Physics::HasRigidbody2D(UnityEngine.Component)
extern void Physics_HasRigidbody2D_m80204646E4EEA6187F16FE74B1608D54DC0EFA00 (void);
// 0x0000014D System.Boolean DG.Tweening.DOTweenModuleUtils/Physics::HasRigidbody(UnityEngine.Component)
extern void Physics_HasRigidbody_m81595D61A93C148222BD16F87F82F465286457B8 (void);
// 0x0000014E DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModuleUtils/Physics::CreateDOTweenPathTween(UnityEngine.MonoBehaviour,System.Boolean,System.Boolean,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void Physics_CreateDOTweenPathTween_m9AA6DE226F6876ACF693B7254E3DB5773E7651E0 (void);
static Il2CppMethodPointer s_methodPointers[334] = 
{
	DOTweenModuleAudio_DOFade_m1D693C3CE476AABDF1973BE1CDB974276D71307A,
	DOTweenModuleAudio_DOPitch_m21C03AF57AD7EEC7C1CF82CA7F660A58BD17D7CF,
	DOTweenModuleAudio_DOSetFloat_m95C5B73E2208425BB0D225DBBE705A62C34ADB8B,
	DOTweenModuleAudio_DOComplete_mB34A0C4B5156B4D5D7F88E1F1B3549F0ED35048C,
	DOTweenModuleAudio_DOKill_mAF9948DBD5491192943527B5FD65561EB5E6DA23,
	DOTweenModuleAudio_DOFlip_mA5DAA9E654CBF4C302F7376463DC80EF9FE480B0,
	DOTweenModuleAudio_DOGoto_m0E560369CDA932ED0068A0CED7D733FE203E43E1,
	DOTweenModuleAudio_DOPause_mDC395F8C1B08A113E79E42FAB5EE1EF453B1136D,
	DOTweenModuleAudio_DOPlay_m86F982951B3B2786390449AFE30BAEFBDC7E92D7,
	DOTweenModuleAudio_DOPlayBackwards_mD371D3D6C3D85A6CE99B597BB0305D9E6EDD87F4,
	DOTweenModuleAudio_DOPlayForward_m561924708D16379FF482E79B5710AB2FB5B1E9FB,
	DOTweenModuleAudio_DORestart_m0B4E5FF932DACDE8A3B1C85BE6B5A8FCB12AC0F0,
	DOTweenModuleAudio_DORewind_m20E78D2E878D7E35D859B4789DBDC2DF968B06E3,
	DOTweenModuleAudio_DOSmoothRewind_m775A16C85A17C3471062578BCC5A45683A67AC5A,
	DOTweenModuleAudio_DOTogglePause_m23CC6F5387C5060B72E4000C5F4A61B944A5F971,
	cy__ctor_mAD0200923EC5D8CAC85E9DFAC91E671886E097EF,
	cy_bau_mF4F3F3411DEA9CA80FF495942F48374DB6DE7975,
	cy_bav_m8C2EE9610DA7D80B90DB97136E3D49B0A98FAB06,
	cz__ctor_m9328977F90A2F7547EA0720A2C8A297C9591A5B6,
	cz_baw_m28CBB54AE2F1308DD5F98238438AB0903477FBB1,
	cz_bax_mF797BA4CDEF8CDA4CC84E0567CE9885756501741,
	da__ctor_m2EB018AFBE7166123DBDD52AE2240314AB8D7222,
	da_bay_m42C2FAD4273E6BB52565BC063CDE6E23342B4D22,
	da_baz_m38E2F677AA346B2BE06EF7B38CECA75AAD0E23EB,
	DOTweenModulePhysics_DOMove_m5B66A39F2C06E19EDFA4D9D79B8CEDD1B2C1AE9C,
	DOTweenModulePhysics_DOMoveX_mAE96C9DA1E0C5EC23A773965A91B86C987BAEAAB,
	DOTweenModulePhysics_DOMoveY_mE4DCBAC03F31FBF07EDFB15A55E2822256EAE03A,
	DOTweenModulePhysics_DOMoveZ_m5915FC138CB7BBFFCEF34456590EF4670B1893CE,
	DOTweenModulePhysics_DORotate_m65EA16E432A1B477A5A696EF95257FE812C948D6,
	DOTweenModulePhysics_DOLookAt_m86BB568C280583D5140AF00ACDB6102191ECD98D,
	DOTweenModulePhysics_DOJump_m66B1A29F77189B9981097A35A69E00974F0ACA65,
	DOTweenModulePhysics_DOPath_m1443BEE665EB5D1A4EF2C356BE311406251657D4,
	DOTweenModulePhysics_DOLocalPath_m16C97084468DB6E53825EEED8132F80E127EF0FA,
	DOTweenModulePhysics_bbr_m18FC0C755ECE981DD2D8F3000BE727C1ED71C78B,
	DOTweenModulePhysics_bbs_m1415FDD052274D1E8989D011D34B9CF0EA388CF1,
	db__ctor_mC47DAB57935178AA388F296B145282A06B907646,
	db_bba_mE1D8F4D74FE9A142CC3A94D1EF584F5034668FE8,
	dc__ctor_m41D031119870A391E7B37DC66A96C2B23514684C,
	dc_bbb_mCF736ECBEE88A914ECC6370502845380F79CFE5B,
	dd__ctor_m34B82646EC7DC3F7512BF3B715979133E2470BED,
	dd_bbc_m20F0962AE47C761B9DB13CDBABE5019540BD5A64,
	de__ctor_m9F8780F631E023B10D133198A1B1EADA815EE8FE,
	de_bbd_m29215FC0F3FAB3BA668818D4D7EE0A250EF62448,
	df__ctor_m29ACA03FEBD0AA1061A7B077D05AB540C386A972,
	df_bbe_m2DF1448224DC8E7D0C0E707C5AF171B107C2CA8E,
	dg__ctor_m9244DAD4B53BBE947CA6065A2198CA744C1AE8B0,
	dg_bbf_mC8BD356602E0BBE4C58A810F18D46650A57164E6,
	dh__ctor_m5AA9F8D1F6869A43D51C6791227359D26408DE1D,
	dh_bbg_mC549D527C1DB9B8450B32B91C494788D1BB45F93,
	dh_bbh_m836CD1A928589FD86D388BC42DFB88800B3B83E5,
	dh_bbi_mA2D0C76C9812DC9E919BA37C17F31AC806229E4C,
	dh_bbj_m8DF4B60E3D5B8F1C10AD90C5EF19F126EFFF8337,
	dh_bbk_m601E4F8A594C58390CF8573D5DB90ED034AF9E59,
	di__ctor_mD60FD2690C1DA38AF59744212EF474505A8BBA38,
	di_bbl_mFC8ABA84C90FE58B2D54A9FF9C07D0C241E71D5D,
	dj__ctor_m4F84B875F118FBC68AB63E8FFF5654716A90F4AF,
	dj_bbm_m10AAE3DF1A75A38CD27BD61D8EF8ECA83CB5D208,
	dj_bbn_m0EB086C7F47665F8606114DE8C809893C6CA5B15,
	dk__ctor_m95CF67F9EE4039565B1E36046C9E5813FC3F44A9,
	dk_bbo_mC7CFFB919C3DA41EE4D98C4D462B8EC1185B6C88,
	dl__ctor_m595DA2456184016A717CB6DB10C4B394F6692F56,
	dl_bbp_m7B1DA01BFD5C08F32B798CF6B169330C78159972,
	dl_bbq_mCF87654BD9AD27D3A838EE15CAF4AE461E425ADC,
	DOTweenModulePhysics2D_DOMove_m6A74D57E65B6F8E119164B18F17D3ED7BB8B6B44,
	DOTweenModulePhysics2D_DOMoveX_m7F9730EFF5DE54338332D74B2364243C211EB7F4,
	DOTweenModulePhysics2D_DOMoveY_mA3B4A7F4B174180E22FA9F9CC8852B001460E990,
	DOTweenModulePhysics2D_DORotate_m914F5F01CD6972EE0FCBA60FBC4AB340E647DC5E,
	DOTweenModulePhysics2D_DOJump_m994418231434C0151D7ADEE79A02174D6F4E5B2B,
	DOTweenModulePhysics2D_DOPath_m0CF606B5F7970A8D1FBC5816ED775CA1736C9D1C,
	DOTweenModulePhysics2D_DOLocalPath_mEC1BAA9AEDBCE7F667D52E04746C6E29C7AFAA5B,
	DOTweenModulePhysics2D_bcl_m561176A1CB70654F36596EB4FAA2A0BA18A5F6FC,
	DOTweenModulePhysics2D_bcm_mC96ED4BD9FAFDA740603AA596DC5017F81406BC4,
	dm__ctor_m7704485ADF59862CD4F51B1F88BB6F3FE7122DA9,
	dm_bbt_m63BE38B319E5C44F6B75F20D6A6DE0742A9D7436,
	dn__ctor_m9B78216D418FF434BA188A90008C77CDCC43780D,
	dn_bbu_m536CDF6B71EACBF7627D6BFBDE6D32354517BC3D,
	do__ctor_m1AF9DE759AC07A4B7C6C5693F7F93BD09657C745,
	do_bbv_m28B4F8F0B88C3F3AD0014ABBCFC9ACD1BECF1DB0,
	dp__ctor_mF275231D9FF57EBE6D7956A648D278B4FFED103C,
	dp_bbw_mD15E3B2A1949021F8CDE3B791A0916203EBDECD9,
	dq__ctor_m7872F25A453CA1662D34B93B9239D2EFBB477256,
	dq_bbx_m166339402617263D1D1E7FB95AF7E5F498EC4ECF,
	dq_bby_mFEF453C822EC983BC6B6DA9F0FFA0511CD8CD16D,
	dq_bbz_mDA2C80A389E3FA0C7959CC63D2C7BD7392F01D46,
	dq_bca_mAB13B721407A1A51058B14A00BED5415E14B575C,
	dq_bcb_mD525CEFAA05EEB4ADE83D78994CCBC1B00490AC3,
	dq_bcc_m48B287F837A4F474CBCE312C8E1D83CCC52F7D15,
	dr__ctor_m8BB219D1AE8144542E7A54D20395CB1D265D3F4D,
	dr_bcd_m9273CC4E54FB22BA52A2E3DBC76B551759C5242C,
	dr_bce_m4794A5742E07BA82A45B7F39C6030C4BE0EE81E9,
	ds__ctor_mDD17DBE05845C1034590C87093EE36F0FD429297,
	ds_bcf_m94419222072FBAE14707EC32E10FE94411AE5F70,
	ds_bcg_m739F771AD92B64A88E946ABF7336E2C7174CAB2B,
	dt__ctor_m82CB54D5C1F2C6A71D70D225070AFB5C0DF58077,
	dt_bch_m3F4FD3D7FE1036B98A2C0C283373356E644FDD07,
	dt_bci_m7082B4ED1EB1D8D7D3BD3D7124272834AAFF659D,
	du__ctor_m471760F3BED421984E05052F9C4341E0854A277F,
	du_bcj_mED4E16041AE0EEAB27167FB29AFDE6CBC114C583,
	du_bck_mD56A9C86BC9B092D4CE25BF94226BF0CA837D589,
	DOTweenModuleSprite_DOColor_m6E59DCD8638072DF9AA02645F45A4367CE903FF3,
	DOTweenModuleSprite_DOFade_m6AE51AD4E4655B3A03EE59353450C611725E6D1A,
	DOTweenModuleSprite_DOGradientColor_mBDB37BF5C3939448FCEFEB812414F303D7C99C72,
	DOTweenModuleSprite_DOBlendableColor_m5570320359C3164713C74486272EC313724DD6F1,
	dv__ctor_m7A30511F992B495CC4940E66F7832D72E92C10EB,
	dv_bcn_m61A22B9EEE07D224FAF4F2CB7A3BD75EAE5FB151,
	dv_bco_mB5DE6DCCE8C7BB9B6631B12B1919AA9EB0190DB8,
	dw__ctor_m233AEFBA70883B29767391E25CF7373611DA4E52,
	dw_bcp_m728AA54F1E058C0E7309411D806D9FA3D3DD7667,
	dw_bcq_m5045D392AE52911044D43B45A4F88E37629B9E42,
	dx__ctor_mEDA8C895704FBD6C9CA68FD5810543E214EE5C90,
	dx_bcr_m1577F31A125D5687309AA49BD7E7C0B1BE9AB8E9,
	dx_bcs_mBF5AE746C1B69786AE341D14A04B13A8D04C9C48,
	DOTweenModuleUI_DOFade_mA073F33862AA7BEF33842B8200D0E79E046D114E,
	DOTweenModuleUI_DOColor_m961875592E62912276F701C48C3C3C3DDB4E7341,
	DOTweenModuleUI_DOFade_mE2E3C2482CD1D3B81C2F4B0EA4E95CF2CC10CC21,
	DOTweenModuleUI_DOColor_m385F3DCBA53A421BC288521879635D67E8155783,
	DOTweenModuleUI_DOFade_mC1C042C14FF418F2BE7B8A6B3B73A2F8D3719EED,
	DOTweenModuleUI_DOFillAmount_mF99B81AA5E4A001FD8DEB2C7F6056271802D153D,
	DOTweenModuleUI_DOGradientColor_mB08E7CC98E98E7A0A8A04DC84CDE434B038CF03E,
	DOTweenModuleUI_DOFlexibleSize_m2476B21F9C3D8213723B01A0FA93553D8E45F3B7,
	DOTweenModuleUI_DOMinSize_m2D92E524BAEA78CDA2815597D779B2835EF8CC03,
	DOTweenModuleUI_DOPreferredSize_m59E62BDF15A51E4529FC3D920EBC77178DFEDFD8,
	DOTweenModuleUI_DOColor_mAF1146331160A3A8C7FFA916BFA8B71A8531FCF2,
	DOTweenModuleUI_DOFade_mFDEF44F0F048DD606FEF46F539D4F47AF40EF817,
	DOTweenModuleUI_DOScale_mEBEA04E6C90874A4903841D8D2720867B2CA292F,
	DOTweenModuleUI_DOAnchorPos_m5B60876E10960019C209F1C215F921E6F994831A,
	DOTweenModuleUI_DOAnchorPosX_m483100309684443A215A8279D4E54B2769DAF3E8,
	DOTweenModuleUI_DOAnchorPosY_mBCFDB7E014D45E6B56588C598C23D4EB111D1395,
	DOTweenModuleUI_DOAnchorPos3D_mBFA28183F6CBCCD444AF47AA85A0272E8D1CAA48,
	DOTweenModuleUI_DOAnchorPos3DX_m6AE58A54659B9562734FB13539AE84FA69C34980,
	DOTweenModuleUI_DOAnchorPos3DY_mC051E07149C6D443662E3682338A26D0ADC6591C,
	DOTweenModuleUI_DOAnchorPos3DZ_m334F6667A6103C39B84749DC1EBE6557CCBE91D3,
	DOTweenModuleUI_DOAnchorMax_mA7BFD0284B3A23C3BF4DE76F542F60EAE1CC5368,
	DOTweenModuleUI_DOAnchorMin_m7EC8E45746F25D2679DE1267FBD5DEB4CC619057,
	DOTweenModuleUI_DOPivot_m3F8012446C42571C9F1549771422917AEC9EEDF8,
	DOTweenModuleUI_DOPivotX_mFD1858E2B5933F093EB84E3D87A69783B04CA7F4,
	DOTweenModuleUI_DOPivotY_m7CA2B8D7162770DC0700106C9ADB97F9A4BD877B,
	DOTweenModuleUI_DOSizeDelta_mC2236310385A8326B78FE900A33586006C8B4474,
	DOTweenModuleUI_DOPunchAnchorPos_mCE5DF2A8B462E813FBB84C4DBC5697848D1C2B45,
	DOTweenModuleUI_DOShakeAnchorPos_m12477B21C8CD40EF1A6CA4F73A6AF1E35883B992,
	DOTweenModuleUI_DOShakeAnchorPos_m919C4BA7526A3CEA6FF8F4CF669FB7681E2893BB,
	DOTweenModuleUI_DOJumpAnchorPos_m5629EA273819E9C2EE98068F8793202EC96765F3,
	DOTweenModuleUI_DONormalizedPos_mABC0EB2280EEC8C8A990E76E16D7BB5DA48A1706,
	DOTweenModuleUI_DOHorizontalNormalizedPos_m41E8A271B7FE392B43523F6B123DCBF26CB0BA74,
	DOTweenModuleUI_DOVerticalNormalizedPos_mC141061A07ACD836F365221BA33198FF316BFBA3,
	DOTweenModuleUI_DOValue_m38DB0CB09A73E2DDC33B3EEE49D74944FBA82A38,
	DOTweenModuleUI_DOColor_mAF96B607A774C0B0D912E56C3C11A87E4F093440,
	DOTweenModuleUI_DOCounter_m85EC79863973B9D9DF9D67E7B5DD835C86BF1438,
	DOTweenModuleUI_DOFade_m5AD3262DBFA4B713B61D3F2203B2930562A3B54D,
	DOTweenModuleUI_DOText_mDE24022401FC6820F90F1BE517FF46F73AE9BC15,
	DOTweenModuleUI_DOBlendableColor_m69717A08080A14BE5A01F1E8886EBF829CEF7227,
	DOTweenModuleUI_DOBlendableColor_m9D9F47100CD74C031BBA7ABF9A16E45AC29D2E8F,
	DOTweenModuleUI_DOBlendableColor_m7569CFBAB6EFA6049EECFDBC96898880565C368A,
	DOTweenModuleUI_DOShapeCircle_m0A5FCD4C41E4B1FF07B94647146806013735FAE4,
	Utils_SwitchToRectTransform_m6A264948787CBEE108E0D97B235AE921BFBB6152,
	dy__ctor_mE4F46130E864DE7BC38EBA79DED520AC1B5002AD,
	dy_bct_m44A248CA7CECB087422215F4EFDB4D36A9E1AE3C,
	dy_bcu_m50B11C4F7E416C1FA8A72EB34F72643CE1848357,
	dz__ctor_mEEDE9D4F5BEA81753CA7812087368522F11B0FB4,
	dz_bcv_m0FF01E2AAE3ED426AE92CD992518F6735F8DA3B9,
	dz_bcw_m2AC34505EB5442ED013BDC00FF331AECF2BA00F1,
	ea__ctor_m99AF733F0412C4FADD0856D387B5BED745E3F1E9,
	ea_bcx_m2F1F9F014D8EE8448A573DFA2A2AAC8B7F845CCF,
	ea_bcy_m1046D91B0A016B38E09DF5E54359D93E95F8CED2,
	eb__ctor_m7E91F8BBB97290C0855A57BAD48A19887A5B566C,
	eb_bcz_mB0718B20D75EBE327FE327646F1623F3162D9639,
	eb_bda_mABAEDE6234F9E77847364FCB60AA5709E8247139,
	ec__ctor_mD9B18AD35FA48C900ECB3FE7BA3BED7CF94B407D,
	ec_bdb_m28DE708F569571D467238116FA93566B2DA5A09D,
	ec_bdc_mC0F08EA457A80AD94E331B115297F8FA14175A79,
	ed__ctor_mC6DD6B11CC2FB749F171253273837E8A9C83C674,
	ed_bdd_m8E354C436AD73EB7685F1C2A8C49D846F1DCD66E,
	ed_bde_mD6C795DB29524B521D10ADA85C75781F13A6F6CF,
	ee__ctor_m10245C84EB6DC748FF2AC823EDC7CEDB37C49E59,
	ee_bdf_mF2A03CF4DFD3C3679EE1FE053E50C41FB2BE1178,
	ee_bdg_mEC650A9E1A9FE038A1D8BBBBA4373A54C9DDF4E4,
	ef__ctor_m1751B12ACA3B3378671E6D8431277AE94D17E3FB,
	ef_bdh_m23A64A8657B77253F08CB90935AA0660FAE68612,
	ef_bdi_m6C5F1544174F005A9D8A951EE0DDE2F20B55F7F5,
	eg__ctor_m6772EA75EEB508BBCF6A095319D16A8EE382824B,
	eg_bdj_mC8BA268280A300079CBE0482480842F7967A248E,
	eg_bdk_mAC6CEDEDC6F01B683FA132B3097C9B32216B7CCC,
	eh__ctor_m3AF7C63099C38DF1EA025780A2E3ED9BC15F3D43,
	eh_bdl_m26C7A86D1A5119D89B8CBA3813DA2A9749143E76,
	eh_bdm_mF670A0EEB67993A4D4D5E59AD9FFD51508479CAD,
	ei__ctor_m475BB1AA82C38F30EBDECDF6A95B06506DE85C04,
	ei_bdn_mEB27D585D0019E968EE982FED4F75B254B73FCF8,
	ei_bdo_m89491E040AB47EFCB6E999DE2CA110213FAD66EC,
	ej__ctor_m251F85E9830FC286811A851C3AB216D9B945A3E6,
	ej_bdp_mAFE2A3418A47996D807372ABBEF47579FEDAE202,
	ej_bdq_mD6CB3B0C796BBF15FC87896BD7274BA5EA861B66,
	ek__ctor_m78B61B9C086B221D572E84AE7A36EAA74AFB83FC,
	ek_bdr_m2CE48EB125771E1CA9966E28EF1508357251DFB4,
	ek_bds_m5CA64C63BBCC3246EFB417630042D77E2F05CB0E,
	el__ctor_m267028B1EE49C316617B4D214A0324091ECB8A3A,
	el_bdt_m5FFBBFF7884261900BEA74F05B1EE3CCCA9F3D60,
	el_bdu_m7A5E28242E64100A5EF8D99A56DAFC4453E6B68F,
	em__ctor_m05F9EAD54DB493E97FEEEF622E08255164AFDA69,
	em_bdv_m7137E0BC00B3AD445B48A06BE457CA80683274C9,
	em_bdw_mD2CBA4B8F2ED1EDB32A181FA8962EC8E8FDF1991,
	en__ctor_mC7B55A491831CA1A213602633983FDC620B7AFB6,
	en_bdx_m72F21B71ADF5694686B3BC2933ED1FDA29ED386A,
	en_bdy_m4FF38316AE1ADA57223B60B9B2C2E50185C1F4C9,
	eo__ctor_m4C5EA2B567A867F69C23648744D957FF5F068A47,
	eo_bdz_mD241988C7B248736C0225318E8356A0DC7E2C0A3,
	eo_bea_m0334A75C6857A73B55DA2EDA443496FE47CD29B5,
	ep__ctor_m837A8EAD5A76BDA4E61CBAB9B5456B5F825BBA05,
	ep_beb_m46A3E74A81BA66DF67D7A3552CB2CA0BF594168C,
	ep_bec_m8D6666679BE61C8AD9B3A8A38B115ED41916798C,
	eq__ctor_m6AFDAD8D316975409BF1F89AE83FA232C92CF72E,
	eq_bed_m78D6A9A4C169D676C7532340A8134B1BE21198E2,
	eq_bee_m763609B224CEEAB50A59A3F3617C7403CC7FFAE5,
	er__ctor_m3C75B2A6E21BFEC890202FDBC14858E49FCD6F7A,
	er_bef_m6100B8336BC3E825833749268E183919D860E5FA,
	er_beg_m469710C96CD70B0B83ECCD307D278BA2F579DF23,
	es__ctor_mBDF6EC64C9D9F4F76542B5628EE5DEB18B1A33FC,
	es_beh_mEC80ACD606128810CD5591F3B3AB95C0F6EB28F6,
	es_bei_mF2EE49868BCF32B313EE3EC3C4A6469CCC18C520,
	et__ctor_m8035CAF052CD8844BCAF65C57F3648B8CFFBCA2B,
	et_bej_mC10D8CBB1F18E54356CE5E3F57960BF2C1866D69,
	et_bek_m36D263BAC5397036850EFC6D5AFF5966B6009625,
	eu__ctor_m9D4A6A900173936B9D3675123CA8AB97D8C42B4B,
	eu_bel_mEC31344992863C08C32A39AFF11DEA5CAAF5EBC7,
	eu_bem_mF4783A99CE56AF1D1347FCE4CC19AEBE78134331,
	ev__ctor_mF45B45769C0B9AA2B5A26CB0943CEA036177A3D7,
	ev_ben_mFBECDCD464210BE9D4786874F2AAEBF96B3EB70F,
	ev_beo_m35C0D0094ECC520CD233D27D69E3F9C48B3D9E54,
	ew__ctor_mD842B4ADDEEEE28A2D1027C99D7E46DF268BF2CD,
	ew_bep_m2E27FFE535D5A4A185FBE041B49111F35AFDEDD0,
	ew_beq_m6DB9FE0D6ABEC3527E52593CB0D560D9C8E6A699,
	ex__ctor_mA1E3F7DBBA4CC33EB4E9982690E985EE4424115E,
	ex_ber_m9C6AE962D6A2E8655B8F019DF5FCBFE4049BEDB8,
	ex_bes_m095FE019172F54C44D377B803E578DBD03491F16,
	ey__ctor_mCD2E752C7402602967B22CB4C216E8118D6ADDD6,
	ey_bet_mA7C41897A449820871E9B7AD070F955F73B7C850,
	ey_beu_m0EA4CDA03F4CC9F01464F27DC36FA9F21304DAA2,
	ez__ctor_m260C5C7200A0C225604C116CAA2EA501B21920E1,
	ez_bev_mA7CCEBD55A64A8855A286F4AA00A40F6163A7842,
	ez_bew_mE7F2C9AFBEC9E58AA9E336B9103B4254579DA562,
	fa__ctor_m7849F665248329349A45195C2D1B77684C412D5A,
	fa_bex_m75AC3DC9774E97501C08399DCEB405A175EA5854,
	fa_bey_mCF475F66DA0BB233C116A66BF995FA901C12873C,
	fa_bez_m64F164E24EBD9291A59183F5F548077A5F7957BD,
	fa_bfa_mDDB58E90FB67CD572B946FE48F29CC845FFE05C3,
	fa_bfb_m239D4F8F0189F4311182532B6BC8F70613AC2E21,
	fa_bfc_mEE061203C61E901D933E0FFA069DE0013732569F,
	fb__ctor_m788489B5A214A6B0AD358C169FC1A18655305658,
	fb_bfd_mD6DED0C77AF9662B996E155CFD27F1074489AB70,
	fb_bfe_m0F3D5DC8B38C21F23435310E7FD9ECA7D02D16DE,
	fc__ctor_m84E9DE8BF07161EADA7BFD8062240CFB118B0A4F,
	fc_bff_m315F514242C6FC5E7E6BBD36409A4F1F1C42303A,
	fc_bfg_m8FDCEDC13307DDFF4B26B6902E56D6BFECD7AE76,
	fd__ctor_mFE7257161A84AAE6E6E720A974558F686ECCC2AC,
	fd_bfh_m774CCD224563101163E10403AE1CA478CDC55000,
	fd_bfi_m319BF1239F9197089FDBAA674B45DB54A0536FD8,
	fe__ctor_m6669F78B55570F292B56490E9095D33F945F6631,
	fe_bfj_m97F1AF8A0C75292E42EBF3C2DBDC38B25DA5FEE0,
	fe_bfk_m32412797A5C14F94AE8C761F5E3F2B68225453EB,
	ff__ctor_m2C10047CEB633435F9609021C6E26CB130EA6B5F,
	ff_bfl_m4FC8EAF7880E388D37BC02AF7A1F33EAC6016C92,
	ff_bfm_m2B635BCE9B642C42F093DC06A5AE70D027CAE0D1,
	fg__ctor_mF1AF56A8BB88F68B97B0246CD209A3480040A1E4,
	fg_bfn_m7749725079A05CB7D52C512338FD31BAA2D287BA,
	fg_bfo_m99C89120EF8FBA323B0BF99638991272B2199136,
	fh__ctor_m892D1C680E6EFCFE8659FFCF25D5EB8C16628299,
	fh_bfp_m3B3C851EF608859977526E78BCFF7660D9565999,
	fh_bfq_mCC9FEF22604A520456750FE6C81211A6A49D4C44,
	fi__ctor_m6DB4423B9BB10F62E89053F84F5ED51540EFAF1D,
	fi_bfr_m460399ABB62EBA5EE4846DA9BFACCBB608FF3190,
	fi_bfs_mBE30D372AD18DB0CC86A839E2B77AD68F227F03F,
	fj__ctor_mC8F102840D1917115490F28067412D08629D6AA9,
	fj_bft_mBD387F944F9F604BD54237D2F96EFCA2107C1EE8,
	fj_bfu_m1E8BA1F8831723096AB3427877FD83DA4BD78EFD,
	fk__ctor_m73DCC65C29FBEEB61D1985CD39AB9267A677FA1C,
	fk_bfv_m2656A13A55A629F0ECC87792224DCE530BD9BF6F,
	fk_bfw_mFB9E679DF932CEE8FE22ACF7076EE3E9FDE0C3AB,
	fl__ctor_mF82268188F9ABE8234C747EB1A27D091F3D6BAA9,
	fl_bfx_mD8B942989081C6FE9A6AD57CFEE0AC9C357124DC,
	fl_bfy_m1E9AA41FC34D872DBE018CEF3A6DC208ACE8FD47,
	fm__ctor_m29944AD3DD9DE3D6A441AD219C809CABBD42CCAE,
	fm_bfz_mBA6336EADB0F13C78039892A66AB272A73385F2E,
	fm_bga_mDE9C4E0DC66EE5BC7A952312FFF24D052844BB0F,
	DOTweenModuleUnityVersion_DOGradientColor_m5989D5E5CCAA8B96FEC40CE899DDD9BC255F4126,
	DOTweenModuleUnityVersion_DOGradientColor_m16A5065B7DA6EA92AC15D1E186EE14617F64896B,
	DOTweenModuleUnityVersion_WaitForCompletion_m080081BC7D031CAE8CB22FC166C6E19FAA91295C,
	DOTweenModuleUnityVersion_WaitForRewind_m7EB6AB643D5F309EE41F15305ADB1324E16853A7,
	DOTweenModuleUnityVersion_WaitForKill_m9592B2D81F0C76C9F8003645BDB0C4C3BA4E6A3B,
	DOTweenModuleUnityVersion_WaitForElapsedLoops_m08D7DB6E86076A4D3224DE7F651529C03B0C9B93,
	DOTweenModuleUnityVersion_WaitForPosition_m918A05EB65877997F48B218DE2B9B95A4E198A0E,
	DOTweenModuleUnityVersion_WaitForStart_m3E6DF08DBF844D468C3DD2A14AB96AAF3717BA53,
	DOTweenModuleUnityVersion_DOOffset_mB985D0A38167056313BD49AA97A9A2F12B6C1CA2,
	DOTweenModuleUnityVersion_DOTiling_m354938745AA34ED1539D3CBF54C94A5E5753B903,
	DOTweenModuleUnityVersion_AsyncWaitForCompletion_m47D1F5AACF019F8C3233238D5B73EBD325685681,
	DOTweenModuleUnityVersion_AsyncWaitForRewind_m4B4AD3964B5CBB9F56BB1050E8EF7C0AF414060E,
	DOTweenModuleUnityVersion_AsyncWaitForKill_m4CE6BA6CDF93365F4D14CE2909C7F7FF6B332928,
	DOTweenModuleUnityVersion_AsyncWaitForElapsedLoops_m08434CDEC811346692A3795B1736C8499E391277,
	DOTweenModuleUnityVersion_AsyncWaitForPosition_mA1767089E7840940CEB85BEAF15FBC02CE56D487,
	DOTweenModuleUnityVersion_AsyncWaitForStart_m3BAC94C50C109B2795BAA10D582617B3F2F95126,
	fn__ctor_mB30C0F2C702B322FD5695D1AA80054E21D0597EF,
	fn_bgb_mD2875C8F96A2A4966041A1F72C0D231457E3CB9E,
	fn_bgc_m877D40B3191027F3259023A08E291607645873D8,
	fo__ctor_m91305CABABD2927201AD41057147AA9AE812BDFB,
	fo_bgd_m93A426B05C34E3ABEBC0C7FE4584D5222AE6B102,
	fo_bge_mDF2E1112BA2A37EBEA24DD8370172A92D0A7970F,
	U3CAsyncWaitForCompletionU3Ed__10_MoveNext_m7AE96D202005CC27A2EBEB47057FADA081D6DCDA,
	U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_m6240E5A12BCFEF8EA8588C4C18B3BB48BBA71FBB,
	U3CAsyncWaitForRewindU3Ed__11_MoveNext_m5E01823C6DBEFF1763AD6606972918D90935D340,
	U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_m3F25AF370573B0A32A25D5D01483AF62C16AD5C3,
	U3CAsyncWaitForKillU3Ed__12_MoveNext_mC10BB60238B855C88268AEE7DA0D7E8CF59AC885,
	U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m7BC3B905F4C08F129E4E6DFD12DDA73E5756DD0B,
	U3CAsyncWaitForElapsedLoopsU3Ed__13_MoveNext_mF44348C1C974B48E1B0F6FE03AD1F713A4A6F8A2,
	U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m4D5053D74FA671E83FA48E11A3EE5C6534F995AB,
	U3CAsyncWaitForPositionU3Ed__14_MoveNext_mB726C581B4A8055414678A86C800C4A0525E5121,
	U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_m060A3B894F6700B46D57FD2443F396C7C9CC7111,
	U3CAsyncWaitForStartU3Ed__15_MoveNext_mDBD2BBC1B727D4C915B0E2AB910330411086BC12,
	U3CAsyncWaitForStartU3Ed__15_SetStateMachine_m3B0B96BE14F355F5A0AF74DD42C7774158140A41,
	WaitForCompletion_get_keepWaiting_mD2C729ECA6B6F6BE57B718492953D588237F743D,
	WaitForCompletion__ctor_mE85B7CC8B7FE6A2AE84DB8210249A61018118C4D,
	WaitForRewind_get_keepWaiting_mF46CA00D1A5FDF140C0C027C4109C7373245A16C,
	WaitForRewind__ctor_mD79A34DBDAC1B30FBEBD36A8D749EC81935520B2,
	WaitForKill_get_keepWaiting_m4C085E03853426F4D4F5C312334101AE9BA75E3F,
	WaitForKill__ctor_m75C2D3D54DBBBB35DE297B947C08C982CABF1BAF,
	WaitForElapsedLoops_get_keepWaiting_mC2A906DBE9A06B7697041B76EC7C6AF58F928D6C,
	WaitForElapsedLoops__ctor_mA0E7D5A115AB56AD618E24B320476B81B9CAEC7A,
	WaitForPosition_get_keepWaiting_mA71AB7B7C269AB062BB6AF9CDF42E0F54EA6549F,
	WaitForPosition__ctor_mB6CCFCE59F142931CF19AC3BA4FE5CAFF1CBA4AD,
	WaitForStart_get_keepWaiting_mA0B7858981A2CEDE516D5F2C5133629C6D537E0B,
	WaitForStart__ctor_m3AEC2F6E37815ED0D94C2FA5F3B784456C6E49D1,
	DOTweenModuleUtils_Init_m8FD2F0DFE3D768ECE332A27B64ED208ED53568D5,
	DOTweenModuleUtils_bgf_m1486C03354670D09C17BCC5CC626740305B36530,
	Physics_SetOrientationOnPath_mC2C4E6FA124458E00765F647C3E824CC31A4696D,
	Physics_HasRigidbody2D_m80204646E4EEA6187F16FE74B1608D54DC0EFA00,
	Physics_HasRigidbody_m81595D61A93C148222BD16F87F82F465286457B8,
	Physics_CreateDOTweenPathTween_m9AA6DE226F6876ACF693B7254E3DB5773E7651E0,
};
extern void U3CAsyncWaitForCompletionU3Ed__10_MoveNext_m7AE96D202005CC27A2EBEB47057FADA081D6DCDA_AdjustorThunk (void);
extern void U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_m6240E5A12BCFEF8EA8588C4C18B3BB48BBA71FBB_AdjustorThunk (void);
extern void U3CAsyncWaitForRewindU3Ed__11_MoveNext_m5E01823C6DBEFF1763AD6606972918D90935D340_AdjustorThunk (void);
extern void U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_m3F25AF370573B0A32A25D5D01483AF62C16AD5C3_AdjustorThunk (void);
extern void U3CAsyncWaitForKillU3Ed__12_MoveNext_mC10BB60238B855C88268AEE7DA0D7E8CF59AC885_AdjustorThunk (void);
extern void U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m7BC3B905F4C08F129E4E6DFD12DDA73E5756DD0B_AdjustorThunk (void);
extern void U3CAsyncWaitForElapsedLoopsU3Ed__13_MoveNext_mF44348C1C974B48E1B0F6FE03AD1F713A4A6F8A2_AdjustorThunk (void);
extern void U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m4D5053D74FA671E83FA48E11A3EE5C6534F995AB_AdjustorThunk (void);
extern void U3CAsyncWaitForPositionU3Ed__14_MoveNext_mB726C581B4A8055414678A86C800C4A0525E5121_AdjustorThunk (void);
extern void U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_m060A3B894F6700B46D57FD2443F396C7C9CC7111_AdjustorThunk (void);
extern void U3CAsyncWaitForStartU3Ed__15_MoveNext_mDBD2BBC1B727D4C915B0E2AB910330411086BC12_AdjustorThunk (void);
extern void U3CAsyncWaitForStartU3Ed__15_SetStateMachine_m3B0B96BE14F355F5A0AF74DD42C7774158140A41_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[12] = 
{
	{ 0x06000131, U3CAsyncWaitForCompletionU3Ed__10_MoveNext_m7AE96D202005CC27A2EBEB47057FADA081D6DCDA_AdjustorThunk },
	{ 0x06000132, U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_m6240E5A12BCFEF8EA8588C4C18B3BB48BBA71FBB_AdjustorThunk },
	{ 0x06000133, U3CAsyncWaitForRewindU3Ed__11_MoveNext_m5E01823C6DBEFF1763AD6606972918D90935D340_AdjustorThunk },
	{ 0x06000134, U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_m3F25AF370573B0A32A25D5D01483AF62C16AD5C3_AdjustorThunk },
	{ 0x06000135, U3CAsyncWaitForKillU3Ed__12_MoveNext_mC10BB60238B855C88268AEE7DA0D7E8CF59AC885_AdjustorThunk },
	{ 0x06000136, U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m7BC3B905F4C08F129E4E6DFD12DDA73E5756DD0B_AdjustorThunk },
	{ 0x06000137, U3CAsyncWaitForElapsedLoopsU3Ed__13_MoveNext_mF44348C1C974B48E1B0F6FE03AD1F713A4A6F8A2_AdjustorThunk },
	{ 0x06000138, U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m4D5053D74FA671E83FA48E11A3EE5C6534F995AB_AdjustorThunk },
	{ 0x06000139, U3CAsyncWaitForPositionU3Ed__14_MoveNext_mB726C581B4A8055414678A86C800C4A0525E5121_AdjustorThunk },
	{ 0x0600013A, U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_m060A3B894F6700B46D57FD2443F396C7C9CC7111_AdjustorThunk },
	{ 0x0600013B, U3CAsyncWaitForStartU3Ed__15_MoveNext_mDBD2BBC1B727D4C915B0E2AB910330411086BC12_AdjustorThunk },
	{ 0x0600013C, U3CAsyncWaitForStartU3Ed__15_SetStateMachine_m3B0B96BE14F355F5A0AF74DD42C7774158140A41_AdjustorThunk },
};
static const int32_t s_InvokerIndices[334] = 
{
	8707,
	8707,
	8196,
	9379,
	9379,
	10164,
	8626,
	10164,
	10164,
	10164,
	10164,
	10164,
	10164,
	10164,
	10164,
	6893,
	6821,
	5548,
	6893,
	6821,
	5548,
	6893,
	6821,
	5548,
	8226,
	8212,
	8212,
	8212,
	8227,
	7630,
	7325,
	7217,
	7217,
	8195,
	8195,
	6893,
	6883,
	6893,
	6883,
	6893,
	6883,
	6893,
	6883,
	6893,
	6780,
	6893,
	6780,
	6893,
	6883,
	6893,
	6883,
	6883,
	6893,
	6893,
	6883,
	6893,
	6883,
	5605,
	6893,
	6883,
	6893,
	6883,
	5605,
	8223,
	8212,
	8212,
	8707,
	7322,
	7217,
	7217,
	8195,
	8195,
	6893,
	6881,
	6893,
	6881,
	6893,
	6881,
	6893,
	6821,
	6893,
	6881,
	5603,
	6893,
	6881,
	5603,
	6893,
	6893,
	6883,
	5605,
	6893,
	6883,
	5605,
	6893,
	6883,
	5605,
	6893,
	6883,
	5605,
	8685,
	8707,
	8701,
	8685,
	6893,
	6657,
	5389,
	6893,
	6657,
	5389,
	6893,
	6657,
	5389,
	8707,
	8685,
	8707,
	8685,
	8707,
	8707,
	8701,
	8223,
	8223,
	8223,
	8685,
	8707,
	8714,
	8223,
	8212,
	8212,
	8226,
	8212,
	8212,
	8212,
	8223,
	8223,
	8714,
	8707,
	8707,
	8223,
	7322,
	7169,
	7170,
	7322,
	8223,
	8212,
	8212,
	8212,
	8685,
	7312,
	8707,
	7318,
	8685,
	8685,
	8685,
	7323,
	9587,
	6893,
	6821,
	5548,
	6893,
	6657,
	5389,
	6893,
	6657,
	5389,
	6893,
	6657,
	5389,
	6893,
	6657,
	5389,
	6893,
	6821,
	5548,
	6893,
	6881,
	5603,
	6893,
	6881,
	5603,
	6893,
	6881,
	5603,
	6893,
	6657,
	5389,
	6893,
	6657,
	5389,
	6893,
	6881,
	5603,
	6893,
	6881,
	5603,
	6893,
	6881,
	5603,
	6893,
	6881,
	5603,
	6893,
	6883,
	5605,
	6893,
	6883,
	5605,
	6893,
	6883,
	5605,
	6893,
	6883,
	5605,
	6893,
	6881,
	5603,
	6893,
	6881,
	5603,
	6893,
	6881,
	5603,
	6893,
	6881,
	5603,
	6893,
	6881,
	5603,
	6893,
	6881,
	5603,
	6893,
	6883,
	5605,
	6893,
	6883,
	5605,
	6893,
	6883,
	5605,
	6893,
	6881,
	5603,
	6893,
	6881,
	5603,
	6893,
	6893,
	6881,
	5603,
	6893,
	6821,
	5548,
	6893,
	6821,
	5548,
	6893,
	6821,
	5548,
	6893,
	6657,
	5389,
	6893,
	6732,
	5465,
	6893,
	6657,
	5389,
	6893,
	6762,
	5494,
	6893,
	6657,
	5389,
	6893,
	6657,
	5389,
	6893,
	6657,
	5389,
	6893,
	6881,
	5603,
	8701,
	8192,
	9452,
	9452,
	9452,
	8690,
	8705,
	9452,
	8221,
	8221,
	10262,
	10262,
	10262,
	9457,
	9465,
	10262,
	6893,
	6881,
	5603,
	6893,
	6881,
	5603,
	6893,
	5494,
	6893,
	5494,
	6893,
	5494,
	6893,
	5494,
	6893,
	5494,
	6893,
	5494,
	6654,
	5494,
	6654,
	5494,
	6654,
	5494,
	6654,
	3136,
	6654,
	3152,
	6654,
	5494,
	10695,
	10695,
	8390,
	10055,
	10055,
	7311,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	334,
	s_methodPointers,
	12,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
