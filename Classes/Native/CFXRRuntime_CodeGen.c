﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CartoonFX.CFXR_Effect::ResetState()
extern void CFXR_Effect_ResetState_m610E43E011438C9B8496202AC20150A8A296C1FF (void);
// 0x00000002 System.Void CartoonFX.CFXR_Effect::Awake()
extern void CFXR_Effect_Awake_mB25E65B9616B1020B47D93C27D43E4EC230DA465 (void);
// 0x00000003 System.Void CartoonFX.CFXR_Effect::OnEnable()
extern void CFXR_Effect_OnEnable_mADA162F093C9E4F496E703369D9A80C1FC5394F7 (void);
// 0x00000004 System.Void CartoonFX.CFXR_Effect::OnDisable()
extern void CFXR_Effect_OnDisable_m2CF4DC60ED0CCC367C03B7BC2FB083C49A8D3317 (void);
// 0x00000005 System.Void CartoonFX.CFXR_Effect::Update()
extern void CFXR_Effect_Update_m035CC14FF344F0061BDB30C7D3A51E8BFA926CE3 (void);
// 0x00000006 System.Void CartoonFX.CFXR_Effect::Animate(System.Single)
extern void CFXR_Effect_Animate_mB58DB9057AF39423B6C97F6107FE8C687927818B (void);
// 0x00000007 System.Void CartoonFX.CFXR_Effect::FadeOut(System.Single)
extern void CFXR_Effect_FadeOut_mE504B347809886CCD0A95EC517B054C6AB087995 (void);
// 0x00000008 System.Void CartoonFX.CFXR_Effect::.ctor()
extern void CFXR_Effect__ctor_m31E304B0066DFD0B76A44742CC7CF0C8F717EB04 (void);
// 0x00000009 System.Void CartoonFX.CFXR_Effect/CameraShake::bgi(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera)
extern void CameraShake_bgi_m4915221D3B04F2B7B93C1DDE9D3476779B8FCD90 (void);
// 0x0000000A System.Void CartoonFX.CFXR_Effect/CameraShake::bgj(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera)
extern void CameraShake_bgj_m6F1EC0B306A38E8A6F593D6DFCB18657E6A7026D (void);
// 0x0000000B System.Void CartoonFX.CFXR_Effect/CameraShake::bgk(UnityEngine.Camera)
extern void CameraShake_bgk_mEF2309A7DB714B0EBC31353E66B7F8CC7863DA51 (void);
// 0x0000000C System.Void CartoonFX.CFXR_Effect/CameraShake::bgl(UnityEngine.Camera)
extern void CameraShake_bgl_m77FE0359876CB91069420FE1EE8A6308F1336CEF (void);
// 0x0000000D System.Void CartoonFX.CFXR_Effect/CameraShake::bgm(CartoonFX.CFXR_Effect/CameraShake)
extern void CameraShake_bgm_mD34BC0FEC4D4A9834F8E504B2A0DEEECB1BBF04C (void);
// 0x0000000E System.Void CartoonFX.CFXR_Effect/CameraShake::bgn(CartoonFX.CFXR_Effect/CameraShake)
extern void CameraShake_bgn_m0A451E085C2C9B852DDA9AB8B880D0B6CB613F03 (void);
// 0x0000000F System.Void CartoonFX.CFXR_Effect/CameraShake::bgo(UnityEngine.Camera)
extern void CameraShake_bgo_mC7E8A52E0C052ABAD61EB91FFB7B52A70EA877FD (void);
// 0x00000010 System.Void CartoonFX.CFXR_Effect/CameraShake::bgp(UnityEngine.Camera)
extern void CameraShake_bgp_m46D4B4C33A36B8863D78A570BE888AB9FABFA2FE (void);
// 0x00000011 System.Void CartoonFX.CFXR_Effect/CameraShake::fetchCameras()
extern void CameraShake_fetchCameras_mBCB8270C08E9C224B438E93F9FEBD94A08C6D85B (void);
// 0x00000012 System.Void CartoonFX.CFXR_Effect/CameraShake::StartShake()
extern void CameraShake_StartShake_mB917662A46460659A807D99F141FC9B9C0A02CD4 (void);
// 0x00000013 System.Void CartoonFX.CFXR_Effect/CameraShake::StopShake()
extern void CameraShake_StopShake_mC67CD54979ACABCC6CB318B8B3AADD539C4E3EDE (void);
// 0x00000014 System.Void CartoonFX.CFXR_Effect/CameraShake::animate(System.Single)
extern void CameraShake_animate_m434261E45F0AC0F48E12BA641E135292B5045FC1 (void);
// 0x00000015 System.Void CartoonFX.CFXR_Effect/CameraShake::.ctor()
extern void CameraShake__ctor_mD923E68C7BA3FB7317E4B7CC4131217FA23EC056 (void);
// 0x00000016 System.Void CartoonFX.CFXR_Effect/CameraShake::.cctor()
extern void CameraShake__cctor_m98166064D4A798C278C3A75C031244A566C138DE (void);
// 0x00000017 System.Void CartoonFX.CFXR_Effect/AnimatedLight::animate(System.Single)
extern void AnimatedLight_animate_mD33AEB88B3937D4F4D75CFA9723BA1DDC2955426 (void);
// 0x00000018 System.Void CartoonFX.CFXR_Effect/AnimatedLight::animateFadeOut(System.Single)
extern void AnimatedLight_animateFadeOut_mDC5D169D4A8B5DE4928D27CEBE6DE07EB55406E2 (void);
// 0x00000019 System.Void CartoonFX.CFXR_Effect/AnimatedLight::reset()
extern void AnimatedLight_reset_m43CAFEA18F765285CC5BD53D9D836FDD46BCAB71 (void);
// 0x0000001A System.Void CartoonFX.CFXR_Effect/AnimatedLight::.ctor()
extern void AnimatedLight__ctor_mD72E4057AC1B447DE5EF7069CAA2C0443E75115A (void);
// 0x0000001B System.Void CartoonFX.CFXR_Effect/AnimatedLight::.cctor()
extern void AnimatedLight__cctor_mD7264F3E5BA090B43096D89FCC42DBF52CC533F7 (void);
// 0x0000001C System.Void CartoonFX.CFXR_EmissionBySurface::.ctor()
extern void CFXR_EmissionBySurface__ctor_m11DD798A78F524B8CCB32522BFBC803CEAD5CA32 (void);
// 0x0000001D System.Void CartoonFX.CFXR_ParticleText::Awake()
extern void CFXR_ParticleText_Awake_m352451D17C999E2DF55171F975F09B34FA865D8D (void);
// 0x0000001E System.Void CartoonFX.CFXR_ParticleText::bgq()
extern void CFXR_ParticleText_bgq_mC60346AD8A8348D64D42BDB674BE5D1257517E8F (void);
// 0x0000001F System.Void CartoonFX.CFXR_ParticleText::UpdateText(System.String,System.Nullable`1<System.Single>,System.Nullable`1<UnityEngine.Color>,System.Nullable`1<UnityEngine.Color>,System.Nullable`1<UnityEngine.Color>,System.Nullable`1<System.Single>)
extern void CFXR_ParticleText_UpdateText_mD2C7936B4CC8542E63286C45474607CA6DC581D3 (void);
// 0x00000020 System.Void CartoonFX.CFXR_ParticleText::.ctor()
extern void CFXR_ParticleText__ctor_m4C8DF5AE03CA264427D8F117EAECF1814583F741 (void);
// 0x00000021 System.Void CartoonFX.CFXR_ParticleTextFontAsset::bgr()
extern void CFXR_ParticleTextFontAsset_bgr_mC1BEFF4AD304E0B5DB6C54C2D78224E4C54DF85B (void);
// 0x00000022 System.Boolean CartoonFX.CFXR_ParticleTextFontAsset::IsValid()
extern void CFXR_ParticleTextFontAsset_IsValid_m1B2A4563CCB6AAB0F0FAC4E93F6B1738AF726DCF (void);
// 0x00000023 System.Void CartoonFX.CFXR_ParticleTextFontAsset::.ctor()
extern void CFXR_ParticleTextFontAsset__ctor_m6921F09A711893F0A6A8E2BB1AB7798F8B361B48 (void);
// 0x00000024 System.Void CartoonFX.CFXR_ParticleTextFontAsset/Kerning::.ctor()
extern void Kerning__ctor_mE4EF1B5BAFA9083398C45EF5535154487362644E (void);
static Il2CppMethodPointer s_methodPointers[36] = 
{
	CFXR_Effect_ResetState_m610E43E011438C9B8496202AC20150A8A296C1FF,
	CFXR_Effect_Awake_mB25E65B9616B1020B47D93C27D43E4EC230DA465,
	CFXR_Effect_OnEnable_mADA162F093C9E4F496E703369D9A80C1FC5394F7,
	CFXR_Effect_OnDisable_m2CF4DC60ED0CCC367C03B7BC2FB083C49A8D3317,
	CFXR_Effect_Update_m035CC14FF344F0061BDB30C7D3A51E8BFA926CE3,
	CFXR_Effect_Animate_mB58DB9057AF39423B6C97F6107FE8C687927818B,
	CFXR_Effect_FadeOut_mE504B347809886CCD0A95EC517B054C6AB087995,
	CFXR_Effect__ctor_m31E304B0066DFD0B76A44742CC7CF0C8F717EB04,
	CameraShake_bgi_m4915221D3B04F2B7B93C1DDE9D3476779B8FCD90,
	CameraShake_bgj_m6F1EC0B306A38E8A6F593D6DFCB18657E6A7026D,
	CameraShake_bgk_mEF2309A7DB714B0EBC31353E66B7F8CC7863DA51,
	CameraShake_bgl_m77FE0359876CB91069420FE1EE8A6308F1336CEF,
	CameraShake_bgm_mD34BC0FEC4D4A9834F8E504B2A0DEEECB1BBF04C,
	CameraShake_bgn_m0A451E085C2C9B852DDA9AB8B880D0B6CB613F03,
	CameraShake_bgo_mC7E8A52E0C052ABAD61EB91FFB7B52A70EA877FD,
	CameraShake_bgp_m46D4B4C33A36B8863D78A570BE888AB9FABFA2FE,
	CameraShake_fetchCameras_mBCB8270C08E9C224B438E93F9FEBD94A08C6D85B,
	CameraShake_StartShake_mB917662A46460659A807D99F141FC9B9C0A02CD4,
	CameraShake_StopShake_mC67CD54979ACABCC6CB318B8B3AADD539C4E3EDE,
	CameraShake_animate_m434261E45F0AC0F48E12BA641E135292B5045FC1,
	CameraShake__ctor_mD923E68C7BA3FB7317E4B7CC4131217FA23EC056,
	CameraShake__cctor_m98166064D4A798C278C3A75C031244A566C138DE,
	AnimatedLight_animate_mD33AEB88B3937D4F4D75CFA9723BA1DDC2955426,
	AnimatedLight_animateFadeOut_mDC5D169D4A8B5DE4928D27CEBE6DE07EB55406E2,
	AnimatedLight_reset_m43CAFEA18F765285CC5BD53D9D836FDD46BCAB71,
	AnimatedLight__ctor_mD72E4057AC1B447DE5EF7069CAA2C0443E75115A,
	AnimatedLight__cctor_mD7264F3E5BA090B43096D89FCC42DBF52CC533F7,
	CFXR_EmissionBySurface__ctor_m11DD798A78F524B8CCB32522BFBC803CEAD5CA32,
	CFXR_ParticleText_Awake_m352451D17C999E2DF55171F975F09B34FA865D8D,
	CFXR_ParticleText_bgq_mC60346AD8A8348D64D42BDB674BE5D1257517E8F,
	CFXR_ParticleText_UpdateText_mD2C7936B4CC8542E63286C45474607CA6DC581D3,
	CFXR_ParticleText__ctor_m4C8DF5AE03CA264427D8F117EAECF1814583F741,
	CFXR_ParticleTextFontAsset_bgr_mC1BEFF4AD304E0B5DB6C54C2D78224E4C54DF85B,
	CFXR_ParticleTextFontAsset_IsValid_m1B2A4563CCB6AAB0F0FAC4E93F6B1738AF726DCF,
	CFXR_ParticleTextFontAsset__ctor_m6921F09A711893F0A6A8E2BB1AB7798F8B361B48,
	Kerning__ctor_mE4EF1B5BAFA9083398C45EF5535154487362644E,
};
static const int32_t s_InvokerIndices[36] = 
{
	6893,
	6893,
	6893,
	6893,
	6893,
	5548,
	5548,
	6893,
	9835,
	9835,
	10481,
	10481,
	10481,
	10481,
	5494,
	5494,
	6893,
	6893,
	6893,
	5548,
	6893,
	10695,
	5548,
	5548,
	6893,
	6893,
	10695,
	6893,
	6893,
	6893,
	249,
	6893,
	6893,
	6654,
	6893,
	6893,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_CFXRRuntime_CodeGenModule;
const Il2CppCodeGenModule g_CFXRRuntime_CodeGenModule = 
{
	"CFXRRuntime.dll",
	36,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
