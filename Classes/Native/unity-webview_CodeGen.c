﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void WebViewObject::Awake()
extern void WebViewObject_Awake_mB9D8F439EF77BF9402FC23AAB03F878574228048 (void);
// 0x00000002 System.Boolean WebViewObject::get_IsKeyboardVisible()
extern void WebViewObject_get_IsKeyboardVisible_mBC3BC667F8FDD8B89C55B466E7D0D1567693A173 (void);
// 0x00000003 System.IntPtr WebViewObject::_CWebViewPlugin_Init(System.String,System.Boolean,System.Boolean,System.String,System.Boolean,System.Int32,System.Boolean,System.Boolean,System.Int32)
extern void WebViewObject__CWebViewPlugin_Init_m486E39DB9D85CB93AE99844C240EA3B53A676AB3 (void);
// 0x00000004 System.Int32 WebViewObject::_CWebViewPlugin_Destroy(System.IntPtr)
extern void WebViewObject__CWebViewPlugin_Destroy_m7B8B2C6F90746C56A96C9BB53568927617176498 (void);
// 0x00000005 System.Void WebViewObject::_CWebViewPlugin_SetMargins(System.IntPtr,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern void WebViewObject__CWebViewPlugin_SetMargins_m072CE271F87DDFFF0E134DB1D3CA837D82B9A7BF (void);
// 0x00000006 System.Void WebViewObject::_CWebViewPlugin_SetVisibility(System.IntPtr,System.Boolean)
extern void WebViewObject__CWebViewPlugin_SetVisibility_m400B0F30A5FD5EA774B3BCCE3AF5A05F71A784A6 (void);
// 0x00000007 System.Void WebViewObject::_CWebViewPlugin_SetScrollbarsVisibility(System.IntPtr,System.Boolean)
extern void WebViewObject__CWebViewPlugin_SetScrollbarsVisibility_m2476178AC1BE9368F8AFF26112B4FEEE003B89C7 (void);
// 0x00000008 System.Void WebViewObject::_CWebViewPlugin_SetAlertDialogEnabled(System.IntPtr,System.Boolean)
extern void WebViewObject__CWebViewPlugin_SetAlertDialogEnabled_mEFE2F2DD64EE0893B37054CC08DFB106A38BD971 (void);
// 0x00000009 System.Void WebViewObject::_CWebViewPlugin_SetScrollBounceEnabled(System.IntPtr,System.Boolean)
extern void WebViewObject__CWebViewPlugin_SetScrollBounceEnabled_mEF514F8022A350897D8DBBBE746D40D29654CFF3 (void);
// 0x0000000A System.Void WebViewObject::_CWebViewPlugin_SetInteractionEnabled(System.IntPtr,System.Boolean)
extern void WebViewObject__CWebViewPlugin_SetInteractionEnabled_mB4505783D1F543FAFAD12086D794E9456F1BDC80 (void);
// 0x0000000B System.Boolean WebViewObject::_CWebViewPlugin_SetURLPattern(System.IntPtr,System.String,System.String,System.String)
extern void WebViewObject__CWebViewPlugin_SetURLPattern_m7FAF1AFDB4657A57BBA0C5750092A69342F9DF41 (void);
// 0x0000000C System.Void WebViewObject::_CWebViewPlugin_LoadURL(System.IntPtr,System.String)
extern void WebViewObject__CWebViewPlugin_LoadURL_mF3BC67D423B4A3132E20E77F68DD8F2DCC40210D (void);
// 0x0000000D System.Void WebViewObject::_CWebViewPlugin_LoadHTML(System.IntPtr,System.String,System.String)
extern void WebViewObject__CWebViewPlugin_LoadHTML_m2C486C41677C6444F57DF0D79E37FF706851CD83 (void);
// 0x0000000E System.Void WebViewObject::_CWebViewPlugin_EvaluateJS(System.IntPtr,System.String)
extern void WebViewObject__CWebViewPlugin_EvaluateJS_mE1DFDCCF574554A64D67C4712574837F276736FA (void);
// 0x0000000F System.Int32 WebViewObject::_CWebViewPlugin_Progress(System.IntPtr)
extern void WebViewObject__CWebViewPlugin_Progress_mAFC67F67913E8D8D3784BF54C1255889DA073D4B (void);
// 0x00000010 System.Boolean WebViewObject::_CWebViewPlugin_CanGoBack(System.IntPtr)
extern void WebViewObject__CWebViewPlugin_CanGoBack_m26BBE9F33159F0D4134E5672B895446502BD73D4 (void);
// 0x00000011 System.Boolean WebViewObject::_CWebViewPlugin_CanGoForward(System.IntPtr)
extern void WebViewObject__CWebViewPlugin_CanGoForward_mEE9462A38A2A46F34CB10751A411B44465FAD710 (void);
// 0x00000012 System.Void WebViewObject::_CWebViewPlugin_GoBack(System.IntPtr)
extern void WebViewObject__CWebViewPlugin_GoBack_mC15488444CB417A3F9EF294F29CFE4ED65DA8857 (void);
// 0x00000013 System.Void WebViewObject::_CWebViewPlugin_GoForward(System.IntPtr)
extern void WebViewObject__CWebViewPlugin_GoForward_mD8F46910E09B394F727F086FA653C806B16B5791 (void);
// 0x00000014 System.Void WebViewObject::_CWebViewPlugin_Reload(System.IntPtr)
extern void WebViewObject__CWebViewPlugin_Reload_m7ABD2BC31394ADFAF5AE5EE2F6FFD5ACA8B7381B (void);
// 0x00000015 System.Void WebViewObject::_CWebViewPlugin_AddCustomHeader(System.IntPtr,System.String,System.String)
extern void WebViewObject__CWebViewPlugin_AddCustomHeader_mAAE78B1CCE04D4A00172E844AE95ED283B14A8F2 (void);
// 0x00000016 System.String WebViewObject::_CWebViewPlugin_GetCustomHeaderValue(System.IntPtr,System.String)
extern void WebViewObject__CWebViewPlugin_GetCustomHeaderValue_mA6E51E5AB307D643E11A297ED4D05995355A4E88 (void);
// 0x00000017 System.Void WebViewObject::_CWebViewPlugin_RemoveCustomHeader(System.IntPtr,System.String)
extern void WebViewObject__CWebViewPlugin_RemoveCustomHeader_mCCE66F43A174B453751BC0EBF1986A3F1A232460 (void);
// 0x00000018 System.Void WebViewObject::_CWebViewPlugin_ClearCustomHeader(System.IntPtr)
extern void WebViewObject__CWebViewPlugin_ClearCustomHeader_m1855C7017F63969B5EED4506FED160CB771DFC2A (void);
// 0x00000019 System.Void WebViewObject::_CWebViewPlugin_ClearCookies()
extern void WebViewObject__CWebViewPlugin_ClearCookies_m7FCC8210626A0F022BC8D6F11B2A423A7C300FD4 (void);
// 0x0000001A System.Void WebViewObject::_CWebViewPlugin_SaveCookies()
extern void WebViewObject__CWebViewPlugin_SaveCookies_m6E9EE5F72F0B403A199A52502AF346782636A6F4 (void);
// 0x0000001B System.Void WebViewObject::_CWebViewPlugin_GetCookies(System.IntPtr,System.String)
extern void WebViewObject__CWebViewPlugin_GetCookies_mEBE776075CD1290CE067D39F7C6C75A4F8676399 (void);
// 0x0000001C System.Void WebViewObject::_CWebViewPlugin_SetBasicAuthInfo(System.IntPtr,System.String,System.String)
extern void WebViewObject__CWebViewPlugin_SetBasicAuthInfo_m51AE24FCA7CEBC77FA27DD901157649EFAB1BF9A (void);
// 0x0000001D System.Void WebViewObject::_CWebViewPlugin_ClearCache(System.IntPtr,System.Boolean)
extern void WebViewObject__CWebViewPlugin_ClearCache_m3E0823A7B9908EDB08951A8858A5782D882988D7 (void);
// 0x0000001E System.Void WebViewObject::_CWebViewPlugin_SetSuspended(System.IntPtr,System.Boolean)
extern void WebViewObject__CWebViewPlugin_SetSuspended_mF28265A58D9F0A667409706F04F303CAEB86A7C6 (void);
// 0x0000001F System.Boolean WebViewObject::IsWebViewAvailable()
extern void WebViewObject_IsWebViewAvailable_mFF06F81E630FDAF0475D5ADA9747C46EC03DAE16 (void);
// 0x00000020 System.Void WebViewObject::Init(System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Boolean,System.Boolean,System.String,System.Int32,System.Int32,System.Boolean,System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void WebViewObject_Init_mEAF31AD85AEE437FFAED0E788A7A654EC3469FA8 (void);
// 0x00000021 System.Void WebViewObject::OnDestroy()
extern void WebViewObject_OnDestroy_m7EFD8FA090DB20FDC619991440AE4A11E88A28B6 (void);
// 0x00000022 System.Void WebViewObject::Pause()
extern void WebViewObject_Pause_m3F69CAA00AAEBDCAAF3EE4AAA5905B0457434DFE (void);
// 0x00000023 System.Void WebViewObject::Resume()
extern void WebViewObject_Resume_m31417432D1821B761DC42AAA4D904D3414978A62 (void);
// 0x00000024 System.Void WebViewObject::SetCenterPositionWithScale(UnityEngine.Vector2,UnityEngine.Vector2)
extern void WebViewObject_SetCenterPositionWithScale_mEAC59D9AB7EE9B5ACC83F8FFE1AEAFE908B6BE4C (void);
// 0x00000025 System.Void WebViewObject::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void WebViewObject_SetMargins_m1D6F1FD6D8317D2FCD550768B9D794B544564366 (void);
// 0x00000026 System.Void WebViewObject::SetVisibility(System.Boolean)
extern void WebViewObject_SetVisibility_m39DDC96A831F01C141B133FA6DECBBE7C6F3EC73 (void);
// 0x00000027 System.Boolean WebViewObject::GetVisibility()
extern void WebViewObject_GetVisibility_m0890B19FC4DE937652199F48F367F9289B05C5BC (void);
// 0x00000028 System.Void WebViewObject::SetScrollbarsVisibility(System.Boolean)
extern void WebViewObject_SetScrollbarsVisibility_mA536E94DB18C1C1008B08D4D8871A5F70B612CC9 (void);
// 0x00000029 System.Void WebViewObject::SetInteractionEnabled(System.Boolean)
extern void WebViewObject_SetInteractionEnabled_m0A170EC9DEB85617D88A81A304578A920A40A020 (void);
// 0x0000002A System.Void WebViewObject::SetAlertDialogEnabled(System.Boolean)
extern void WebViewObject_SetAlertDialogEnabled_mBE0FF528AAF5AA7388FFC15B186F7D5C9E0A6997 (void);
// 0x0000002B System.Boolean WebViewObject::GetAlertDialogEnabled()
extern void WebViewObject_GetAlertDialogEnabled_m0BD28C73C7A622198221B7098124F1A9A8E28639 (void);
// 0x0000002C System.Void WebViewObject::SetScrollBounceEnabled(System.Boolean)
extern void WebViewObject_SetScrollBounceEnabled_mA82705EA3501742EDBC32C070EF672DFCA911A13 (void);
// 0x0000002D System.Boolean WebViewObject::GetScrollBounceEnabled()
extern void WebViewObject_GetScrollBounceEnabled_m9863E3FF0FB0CE451E300A90B54A602144D404BE (void);
// 0x0000002E System.Void WebViewObject::SetCameraAccess(System.Boolean)
extern void WebViewObject_SetCameraAccess_mC350596E9C91F387A372225C2B9DCF15AE920741 (void);
// 0x0000002F System.Void WebViewObject::SetMicrophoneAccess(System.Boolean)
extern void WebViewObject_SetMicrophoneAccess_m1C76FBF405A2F295888F5EFF1DAF699B2A6B5033 (void);
// 0x00000030 System.Boolean WebViewObject::SetURLPattern(System.String,System.String,System.String)
extern void WebViewObject_SetURLPattern_m10C1538DFCA8321950D49EA2AFDE5C91F1656489 (void);
// 0x00000031 System.Void WebViewObject::LoadURL(System.String)
extern void WebViewObject_LoadURL_m53B0B97166FD99F560551E5ED196885812A44FF5 (void);
// 0x00000032 System.Void WebViewObject::LoadHTML(System.String,System.String)
extern void WebViewObject_LoadHTML_m908C0F70B3099CE8E5299D707A64C8A72238D9A7 (void);
// 0x00000033 System.Void WebViewObject::EvaluateJS(System.String)
extern void WebViewObject_EvaluateJS_m2117470986EFF11E43A47DEF6E6E45FD2C28C27D (void);
// 0x00000034 System.Int32 WebViewObject::Progress()
extern void WebViewObject_Progress_m7B3A5EFDAC58AA59F2B77885DBCF54E1C2650018 (void);
// 0x00000035 System.Boolean WebViewObject::CanGoBack()
extern void WebViewObject_CanGoBack_m8E73FBF10169F93B89060519845A9835D93F1471 (void);
// 0x00000036 System.Boolean WebViewObject::CanGoForward()
extern void WebViewObject_CanGoForward_mDC6F06D4F38626DC95BF3E56A845346EE4E121E0 (void);
// 0x00000037 System.Void WebViewObject::GoBack()
extern void WebViewObject_GoBack_m3A6A9D15AAF225D5913AD8057C6499622640C5A5 (void);
// 0x00000038 System.Void WebViewObject::GoForward()
extern void WebViewObject_GoForward_m417FB43369F7AD99F9526B6D93834354136663B0 (void);
// 0x00000039 System.Void WebViewObject::Reload()
extern void WebViewObject_Reload_mBA7B99FBF68CC720D0F98CF5662BDE276DEBB158 (void);
// 0x0000003A System.Void WebViewObject::CallOnError(System.String)
extern void WebViewObject_CallOnError_m787719CEAD0F007F35AF8D4EFA0BCF6A6E63B42A (void);
// 0x0000003B System.Void WebViewObject::CallOnHttpError(System.String)
extern void WebViewObject_CallOnHttpError_mE10BC848A4035739FDDB3C0BAB14C734B2D8ABBF (void);
// 0x0000003C System.Void WebViewObject::CallOnStarted(System.String)
extern void WebViewObject_CallOnStarted_mBC28195C9674F3C2CC12456DC8F082F86EAEF854 (void);
// 0x0000003D System.Void WebViewObject::CallOnLoaded(System.String)
extern void WebViewObject_CallOnLoaded_m561F518F395A2FDB0BCF19E6D6FA94CC9CF765C2 (void);
// 0x0000003E System.Void WebViewObject::CallFromJS(System.String)
extern void WebViewObject_CallFromJS_mFBB59CD78CA5295A5585D5452F0EEA20E01539BC (void);
// 0x0000003F System.Void WebViewObject::CallOnHooked(System.String)
extern void WebViewObject_CallOnHooked_m920D16D6528B44DD6DEA45368CED310D51FC2C2F (void);
// 0x00000040 System.Void WebViewObject::CallOnCookies(System.String)
extern void WebViewObject_CallOnCookies_mCA0D4DC3E5556089A49E048C5E447E5B8BDFD81E (void);
// 0x00000041 System.Void WebViewObject::AddCustomHeader(System.String,System.String)
extern void WebViewObject_AddCustomHeader_mD6588AE2ACF6AAD483217EBFB5DAC2A84939A4D0 (void);
// 0x00000042 System.String WebViewObject::GetCustomHeaderValue(System.String)
extern void WebViewObject_GetCustomHeaderValue_m6C0C211A2FFC0EAB3FE724372F572F5BB56FBD80 (void);
// 0x00000043 System.Void WebViewObject::RemoveCustomHeader(System.String)
extern void WebViewObject_RemoveCustomHeader_mE5D8106E902C59069674F874CBD870043D661B7B (void);
// 0x00000044 System.Void WebViewObject::ClearCustomHeader()
extern void WebViewObject_ClearCustomHeader_mD546FA1AB3B09FC9E062EC89ED6B9417C1176526 (void);
// 0x00000045 System.Void WebViewObject::ClearCookies()
extern void WebViewObject_ClearCookies_m8304C6FA463D3DDE1183566D1E9805B87615B075 (void);
// 0x00000046 System.Void WebViewObject::SaveCookies()
extern void WebViewObject_SaveCookies_m6EF4334EC21D455C0C6A70B4B353F9942A92F5E1 (void);
// 0x00000047 System.Void WebViewObject::GetCookies(System.String)
extern void WebViewObject_GetCookies_mFFC33687D38A31E6D9F55F41D5805595AC1EF2D2 (void);
// 0x00000048 System.Void WebViewObject::SetBasicAuthInfo(System.String,System.String)
extern void WebViewObject_SetBasicAuthInfo_mBF5E57E10F5D6DF10BF91E880B00FDE162951E11 (void);
// 0x00000049 System.Void WebViewObject::ClearCache(System.Boolean)
extern void WebViewObject_ClearCache_m1365D809C7A51F3A5EFAC16581E17E38FAA54D33 (void);
// 0x0000004A System.Void WebViewObject::SetTextZoom(System.Int32)
extern void WebViewObject_SetTextZoom_mE2F4060CAECDBFB2B90F73FA9FE11D38F1311BD1 (void);
// 0x0000004B System.Void WebViewObject::.ctor()
extern void WebViewObject__ctor_mCF74D7A0FF5875B0B98D3200379AD046B0B95F81 (void);
static Il2CppMethodPointer s_methodPointers[75] = 
{
	WebViewObject_Awake_mB9D8F439EF77BF9402FC23AAB03F878574228048,
	WebViewObject_get_IsKeyboardVisible_mBC3BC667F8FDD8B89C55B466E7D0D1567693A173,
	WebViewObject__CWebViewPlugin_Init_m486E39DB9D85CB93AE99844C240EA3B53A676AB3,
	WebViewObject__CWebViewPlugin_Destroy_m7B8B2C6F90746C56A96C9BB53568927617176498,
	WebViewObject__CWebViewPlugin_SetMargins_m072CE271F87DDFFF0E134DB1D3CA837D82B9A7BF,
	WebViewObject__CWebViewPlugin_SetVisibility_m400B0F30A5FD5EA774B3BCCE3AF5A05F71A784A6,
	WebViewObject__CWebViewPlugin_SetScrollbarsVisibility_m2476178AC1BE9368F8AFF26112B4FEEE003B89C7,
	WebViewObject__CWebViewPlugin_SetAlertDialogEnabled_mEFE2F2DD64EE0893B37054CC08DFB106A38BD971,
	WebViewObject__CWebViewPlugin_SetScrollBounceEnabled_mEF514F8022A350897D8DBBBE746D40D29654CFF3,
	WebViewObject__CWebViewPlugin_SetInteractionEnabled_mB4505783D1F543FAFAD12086D794E9456F1BDC80,
	WebViewObject__CWebViewPlugin_SetURLPattern_m7FAF1AFDB4657A57BBA0C5750092A69342F9DF41,
	WebViewObject__CWebViewPlugin_LoadURL_mF3BC67D423B4A3132E20E77F68DD8F2DCC40210D,
	WebViewObject__CWebViewPlugin_LoadHTML_m2C486C41677C6444F57DF0D79E37FF706851CD83,
	WebViewObject__CWebViewPlugin_EvaluateJS_mE1DFDCCF574554A64D67C4712574837F276736FA,
	WebViewObject__CWebViewPlugin_Progress_mAFC67F67913E8D8D3784BF54C1255889DA073D4B,
	WebViewObject__CWebViewPlugin_CanGoBack_m26BBE9F33159F0D4134E5672B895446502BD73D4,
	WebViewObject__CWebViewPlugin_CanGoForward_mEE9462A38A2A46F34CB10751A411B44465FAD710,
	WebViewObject__CWebViewPlugin_GoBack_mC15488444CB417A3F9EF294F29CFE4ED65DA8857,
	WebViewObject__CWebViewPlugin_GoForward_mD8F46910E09B394F727F086FA653C806B16B5791,
	WebViewObject__CWebViewPlugin_Reload_m7ABD2BC31394ADFAF5AE5EE2F6FFD5ACA8B7381B,
	WebViewObject__CWebViewPlugin_AddCustomHeader_mAAE78B1CCE04D4A00172E844AE95ED283B14A8F2,
	WebViewObject__CWebViewPlugin_GetCustomHeaderValue_mA6E51E5AB307D643E11A297ED4D05995355A4E88,
	WebViewObject__CWebViewPlugin_RemoveCustomHeader_mCCE66F43A174B453751BC0EBF1986A3F1A232460,
	WebViewObject__CWebViewPlugin_ClearCustomHeader_m1855C7017F63969B5EED4506FED160CB771DFC2A,
	WebViewObject__CWebViewPlugin_ClearCookies_m7FCC8210626A0F022BC8D6F11B2A423A7C300FD4,
	WebViewObject__CWebViewPlugin_SaveCookies_m6E9EE5F72F0B403A199A52502AF346782636A6F4,
	WebViewObject__CWebViewPlugin_GetCookies_mEBE776075CD1290CE067D39F7C6C75A4F8676399,
	WebViewObject__CWebViewPlugin_SetBasicAuthInfo_m51AE24FCA7CEBC77FA27DD901157649EFAB1BF9A,
	WebViewObject__CWebViewPlugin_ClearCache_m3E0823A7B9908EDB08951A8858A5782D882988D7,
	WebViewObject__CWebViewPlugin_SetSuspended_mF28265A58D9F0A667409706F04F303CAEB86A7C6,
	WebViewObject_IsWebViewAvailable_mFF06F81E630FDAF0475D5ADA9747C46EC03DAE16,
	WebViewObject_Init_mEAF31AD85AEE437FFAED0E788A7A654EC3469FA8,
	WebViewObject_OnDestroy_m7EFD8FA090DB20FDC619991440AE4A11E88A28B6,
	WebViewObject_Pause_m3F69CAA00AAEBDCAAF3EE4AAA5905B0457434DFE,
	WebViewObject_Resume_m31417432D1821B761DC42AAA4D904D3414978A62,
	WebViewObject_SetCenterPositionWithScale_mEAC59D9AB7EE9B5ACC83F8FFE1AEAFE908B6BE4C,
	WebViewObject_SetMargins_m1D6F1FD6D8317D2FCD550768B9D794B544564366,
	WebViewObject_SetVisibility_m39DDC96A831F01C141B133FA6DECBBE7C6F3EC73,
	WebViewObject_GetVisibility_m0890B19FC4DE937652199F48F367F9289B05C5BC,
	WebViewObject_SetScrollbarsVisibility_mA536E94DB18C1C1008B08D4D8871A5F70B612CC9,
	WebViewObject_SetInteractionEnabled_m0A170EC9DEB85617D88A81A304578A920A40A020,
	WebViewObject_SetAlertDialogEnabled_mBE0FF528AAF5AA7388FFC15B186F7D5C9E0A6997,
	WebViewObject_GetAlertDialogEnabled_m0BD28C73C7A622198221B7098124F1A9A8E28639,
	WebViewObject_SetScrollBounceEnabled_mA82705EA3501742EDBC32C070EF672DFCA911A13,
	WebViewObject_GetScrollBounceEnabled_m9863E3FF0FB0CE451E300A90B54A602144D404BE,
	WebViewObject_SetCameraAccess_mC350596E9C91F387A372225C2B9DCF15AE920741,
	WebViewObject_SetMicrophoneAccess_m1C76FBF405A2F295888F5EFF1DAF699B2A6B5033,
	WebViewObject_SetURLPattern_m10C1538DFCA8321950D49EA2AFDE5C91F1656489,
	WebViewObject_LoadURL_m53B0B97166FD99F560551E5ED196885812A44FF5,
	WebViewObject_LoadHTML_m908C0F70B3099CE8E5299D707A64C8A72238D9A7,
	WebViewObject_EvaluateJS_m2117470986EFF11E43A47DEF6E6E45FD2C28C27D,
	WebViewObject_Progress_m7B3A5EFDAC58AA59F2B77885DBCF54E1C2650018,
	WebViewObject_CanGoBack_m8E73FBF10169F93B89060519845A9835D93F1471,
	WebViewObject_CanGoForward_mDC6F06D4F38626DC95BF3E56A845346EE4E121E0,
	WebViewObject_GoBack_m3A6A9D15AAF225D5913AD8057C6499622640C5A5,
	WebViewObject_GoForward_m417FB43369F7AD99F9526B6D93834354136663B0,
	WebViewObject_Reload_mBA7B99FBF68CC720D0F98CF5662BDE276DEBB158,
	WebViewObject_CallOnError_m787719CEAD0F007F35AF8D4EFA0BCF6A6E63B42A,
	WebViewObject_CallOnHttpError_mE10BC848A4035739FDDB3C0BAB14C734B2D8ABBF,
	WebViewObject_CallOnStarted_mBC28195C9674F3C2CC12456DC8F082F86EAEF854,
	WebViewObject_CallOnLoaded_m561F518F395A2FDB0BCF19E6D6FA94CC9CF765C2,
	WebViewObject_CallFromJS_mFBB59CD78CA5295A5585D5452F0EEA20E01539BC,
	WebViewObject_CallOnHooked_m920D16D6528B44DD6DEA45368CED310D51FC2C2F,
	WebViewObject_CallOnCookies_mCA0D4DC3E5556089A49E048C5E447E5B8BDFD81E,
	WebViewObject_AddCustomHeader_mD6588AE2ACF6AAD483217EBFB5DAC2A84939A4D0,
	WebViewObject_GetCustomHeaderValue_m6C0C211A2FFC0EAB3FE724372F572F5BB56FBD80,
	WebViewObject_RemoveCustomHeader_mE5D8106E902C59069674F874CBD870043D661B7B,
	WebViewObject_ClearCustomHeader_mD546FA1AB3B09FC9E062EC89ED6B9417C1176526,
	WebViewObject_ClearCookies_m8304C6FA463D3DDE1183566D1E9805B87615B075,
	WebViewObject_SaveCookies_m6EF4334EC21D455C0C6A70B4B353F9942A92F5E1,
	WebViewObject_GetCookies_mFFC33687D38A31E6D9F55F41D5805595AC1EF2D2,
	WebViewObject_SetBasicAuthInfo_mBF5E57E10F5D6DF10BF91E880B00FDE162951E11,
	WebViewObject_ClearCache_m1365D809C7A51F3A5EFAC16581E17E38FAA54D33,
	WebViewObject_SetTextZoom_mE2F4060CAECDBFB2B90F73FA9FE11D38F1311BD1,
	WebViewObject__ctor_mCF74D7A0FF5875B0B98D3200379AD046B0B95F81,
};
static const int32_t s_InvokerIndices[75] = 
{
	6893,
	6654,
	7127,
	10162,
	7364,
	9748,
	9748,
	9748,
	9748,
	9748,
	7902,
	9751,
	8988,
	9751,
	10162,
	10054,
	10054,
	10479,
	10479,
	10479,
	8988,
	9449,
	9751,
	10479,
	10695,
	10695,
	9751,
	8988,
	9748,
	9748,
	10611,
	7,
	6893,
	6893,
	6893,
	3231,
	501,
	5385,
	6654,
	5385,
	5385,
	5385,
	6654,
	5385,
	6654,
	5385,
	5385,
	1247,
	5494,
	3144,
	5494,
	6732,
	6654,
	6654,
	6893,
	6893,
	6893,
	5494,
	5494,
	5494,
	5494,
	5494,
	5494,
	5494,
	3144,
	4880,
	5494,
	6893,
	6893,
	6893,
	5494,
	3144,
	5385,
	5465,
	6893,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_unityU2Dwebview_CodeGenModule;
const Il2CppCodeGenModule g_unityU2Dwebview_CodeGenModule = 
{
	"unity-webview.dll",
	75,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
