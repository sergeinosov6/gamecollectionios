﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void ExampleGUI::OnGUI()
extern void ExampleGUI_OnGUI_m7F98D722B7B9B570BBAA43D6E62AA1C9ADAB9514 (void);
// 0x00000002 System.Void ExampleGUI::fq(System.Int32)
extern void ExampleGUI_fq_m1A6F8162E695A87DBB61379A4B7B6004030DC92B (void);
// 0x00000003 System.Void ExampleGUI::HandleGooglePlayId(System.String)
extern void ExampleGUI_HandleGooglePlayId_m3E5E31CFA1DACF07F0E799F5C9D79A0AAE15ABE0 (void);
// 0x00000004 System.Void ExampleGUI::AttributionChangedCallback(com.adjust.sdk.AdjustAttribution)
extern void ExampleGUI_AttributionChangedCallback_mF94C25FEB4A93C591437B1B7B980C4FEA040BA73 (void);
// 0x00000005 System.Void ExampleGUI::EventSuccessCallback(com.adjust.sdk.AdjustEventSuccess)
extern void ExampleGUI_EventSuccessCallback_m31F74382525AD79CE9078C9E3DBA80527C24124D (void);
// 0x00000006 System.Void ExampleGUI::EventFailureCallback(com.adjust.sdk.AdjustEventFailure)
extern void ExampleGUI_EventFailureCallback_m3E6FA431F4BD7E4B85E03C02B841950D78AF35EE (void);
// 0x00000007 System.Void ExampleGUI::SessionSuccessCallback(com.adjust.sdk.AdjustSessionSuccess)
extern void ExampleGUI_SessionSuccessCallback_m0900AAA8351E1B38AA866EC6BC8D138165E2146C (void);
// 0x00000008 System.Void ExampleGUI::SessionFailureCallback(com.adjust.sdk.AdjustSessionFailure)
extern void ExampleGUI_SessionFailureCallback_mE56B4A25ADA21A2A3CE51C1CC1B9DED1CDC93059 (void);
// 0x00000009 System.Void ExampleGUI::fr(System.String)
extern void ExampleGUI_fr_mEBBF08D3DCB0698F0C4F68598BC7DF677D3A9F0D (void);
// 0x0000000A System.Void ExampleGUI::.ctor()
extern void ExampleGUI__ctor_m781F1113ADEDC46C79DCD9CDEF57E6A92867A83D (void);
// 0x0000000B System.Void ExampleGUI/<>c::.cctor()
extern void U3CU3Ec__cctor_m8004B56DF658186326E479AFFA0822C993DD72ED (void);
// 0x0000000C System.Void ExampleGUI/<>c::.ctor()
extern void U3CU3Ec__ctor_m4795792C3C133DF92B1D9761F0AF23E6BE3D3FB6 (void);
// 0x0000000D System.Void ExampleGUI/<>c::fp(System.String)
extern void U3CU3Ec_fp_m863AC16EABD9413AC8E20C1A9818EEB3D7ACB1D9 (void);
// 0x0000000E System.Void CameraMenuMoving::Awake()
extern void CameraMenuMoving_Awake_m3D2FDA555E2F67618DC3163293D90DA89E8AC335 (void);
// 0x0000000F System.Void CameraMenuMoving::Update()
extern void CameraMenuMoving_Update_mC0BF00E6A8FF54D62DDE29E51D6BBF3FEFD4F0AD (void);
// 0x00000010 System.Void CameraMenuMoving::.ctor()
extern void CameraMenuMoving__ctor_m173C7C079483127207AA4DFF8A3A15FDA4D9C935 (void);
// 0x00000011 System.Void CircleSlider::Start()
extern void CircleSlider_Start_m6AE9A64452308957253285B8C3F01CB999F756DC (void);
// 0x00000012 System.Void CircleSlider::Update()
extern void CircleSlider_Update_mEAE72D122EA1F00E41FF9062651F48B48E83D1FA (void);
// 0x00000013 System.Void CircleSlider::.ctor()
extern void CircleSlider__ctor_m62FA48BB6ADA7D3F0CF5A3D515E64F662E85C0F0 (void);
// 0x00000014 System.Void JustRotate::Update()
extern void JustRotate_Update_mCF011C9447F4A8F52F83B8FEAA7463FDDFAF5452 (void);
// 0x00000015 System.Void JustRotate::.ctor()
extern void JustRotate__ctor_mCB13490E96F212DED7A9E79F59828664A3077904 (void);
// 0x00000016 System.Void SliderRunTo1::Start()
extern void SliderRunTo1_Start_m4AAB1FDA12B530B7C49D304B95198889B57DDC14 (void);
// 0x00000017 System.Void SliderRunTo1::Update()
extern void SliderRunTo1_Update_m4DA8480B33A0D072A98B99ABBAA544C539DD0330 (void);
// 0x00000018 System.Void SliderRunTo1::.ctor()
extern void SliderRunTo1__ctor_m56F8C938A45BC24AF24076F32B8918CDEE61FBC6 (void);
// 0x00000019 System.Void SliderRunTo1Action::Start()
extern void SliderRunTo1Action_Start_mEAEB5C8912CBD6EE48C7B830CD2742BF4640D184 (void);
// 0x0000001A System.Void SliderRunTo1Action::Update()
extern void SliderRunTo1Action_Update_m2C6AE848FDC10C4E366C38515E9C943DD0186772 (void);
// 0x0000001B System.Void SliderRunTo1Action::.ctor()
extern void SliderRunTo1Action__ctor_mCDC91B6FE1F141D72BBDE2F4D17AB249D3E67250 (void);
// 0x0000001C System.Void SliderValuePass::Start()
extern void SliderValuePass_Start_m0FEC6932F01B69FD1BB800F684C7C97F5FFE6CFC (void);
// 0x0000001D System.Void SliderValuePass::UpdateProgress(System.Single)
extern void SliderValuePass_UpdateProgress_mD6E59B90095418375D083F59CC8DB2AD902B576B (void);
// 0x0000001E System.Void SliderValuePass::.ctor()
extern void SliderValuePass__ctor_m9BA53DA42B8045C5FD6D811719B5C782930CA890 (void);
// 0x0000001F System.Void ToggleChangeTextColor::Start()
extern void ToggleChangeTextColor_Start_mBB5B72F592E9F6E83B743D5CA794F18EBCA4D35C (void);
// 0x00000020 System.Void ToggleChangeTextColor::ChangeColor(System.Boolean)
extern void ToggleChangeTextColor_ChangeColor_m12EA071DBF52F2F3605FF6B0AA6197D808976200 (void);
// 0x00000021 System.Void ToggleChangeTextColor::.ctor()
extern void ToggleChangeTextColor__ctor_m454A6780EB585D08CC24BF5EE105A6A986230568 (void);
// 0x00000022 System.Void CFX_AutoStopLoopedEffect::OnEnable()
extern void CFX_AutoStopLoopedEffect_OnEnable_mFAE624B7728A2F5F7865E685F4594BD0C9629FA5 (void);
// 0x00000023 System.Void CFX_AutoStopLoopedEffect::Update()
extern void CFX_AutoStopLoopedEffect_Update_m8416521A39549298F203B44EF089B09EBD8793FD (void);
// 0x00000024 System.Void CFX_AutoStopLoopedEffect::.ctor()
extern void CFX_AutoStopLoopedEffect__ctor_m66E2F5DD06231BC3D8D01C11A34D7D55BE5F7AAE (void);
// 0x00000025 System.Void CFX_Demo_New::Awake()
extern void CFX_Demo_New_Awake_m2BEACA32BC84D821DF07AF25B7B0B99BFE8F7371 (void);
// 0x00000026 System.Void CFX_Demo_New::Update()
extern void CFX_Demo_New_Update_m1172B662225E3FC12899D5FEEC66702AEA702EFB (void);
// 0x00000027 System.Void CFX_Demo_New::OnToggleGround()
extern void CFX_Demo_New_OnToggleGround_mC1E9AC90083482747297AA60D9CE9ECABF307929 (void);
// 0x00000028 System.Void CFX_Demo_New::OnToggleCamera()
extern void CFX_Demo_New_OnToggleCamera_m756EE04AC3DC67A47CFA88C4CC86B1FB9FE0BD02 (void);
// 0x00000029 System.Void CFX_Demo_New::OnToggleSlowMo()
extern void CFX_Demo_New_OnToggleSlowMo_m20658A6343C784057EFACEBED012978C4F78D48B (void);
// 0x0000002A System.Void CFX_Demo_New::OnPreviousEffect()
extern void CFX_Demo_New_OnPreviousEffect_mA2A209F797976C97EB976A21083FF4C6B9652812 (void);
// 0x0000002B System.Void CFX_Demo_New::OnNextEffect()
extern void CFX_Demo_New_OnNextEffect_mBE56FE317B38E35579D4EC0868755EB2F4E4E640 (void);
// 0x0000002C System.Void CFX_Demo_New::fx()
extern void CFX_Demo_New_fx_mF958F91593CC4F4BF75BAD97FCF37CF509A5F8E5 (void);
// 0x0000002D UnityEngine.GameObject CFX_Demo_New::fy()
extern void CFX_Demo_New_fy_m519AD3ABAC8E5D868A06D4AAF9BD8293167C0F5D (void);
// 0x0000002E System.Collections.IEnumerator CFX_Demo_New::CheckForDeletedParticles()
extern void CFX_Demo_New_CheckForDeletedParticles_m9693C5D1E5FB5189A81426EB45A4E1EE6416EE41 (void);
// 0x0000002F System.Void CFX_Demo_New::fz()
extern void CFX_Demo_New_fz_m5FDEC24B46D99A5A5007CDD7F8A96791959ED896 (void);
// 0x00000030 System.Void CFX_Demo_New::ga()
extern void CFX_Demo_New_ga_m450FCA6AEE31DE87931FD616D1672E1157C597D7 (void);
// 0x00000031 System.Void CFX_Demo_New::gb()
extern void CFX_Demo_New_gb_m654A141FFF9CFB8E324CB83E6C1F66BB0E00E020 (void);
// 0x00000032 System.Void CFX_Demo_New::.ctor()
extern void CFX_Demo_New__ctor_m3F3E772F218F5D0DF6E561E20AE15B042CED6752 (void);
// 0x00000033 System.Void CFX_Demo_New/<>c::.cctor()
extern void U3CU3Ec__cctor_mAB1F46E7745FB53F6527920549715240437DAA6A (void);
// 0x00000034 System.Void CFX_Demo_New/<>c::.ctor()
extern void U3CU3Ec__ctor_m97F374614077DD1A825F933DED9B82FDCBAF91D4 (void);
// 0x00000035 System.Int32 CFX_Demo_New/<>c::fs(UnityEngine.GameObject,UnityEngine.GameObject)
extern void U3CU3Ec_fs_m31CBCC6A389D642FF0A0033E70D06E5E841EDA10 (void);
// 0x00000036 System.Void CFX_Demo_New/a::.ctor(System.Int32)
extern void a__ctor_m9C4DF0DAD9DFD630B96FEA232B72A10ACCE427B2 (void);
// 0x00000037 System.Void CFX_Demo_New/a::ft()
extern void a_ft_mABA22AAB97AE7C25DDF7D73554D23FBB9DF869AF (void);
// 0x00000038 System.Boolean CFX_Demo_New/a::MoveNext()
extern void a_MoveNext_m582CAFC0843108712A0C183E9896EEDCEBAA18B1 (void);
// 0x00000039 System.Object CFX_Demo_New/a::fu()
extern void a_fu_mB924091A3C3F6FD590168E577CE43C81581DA2C7 (void);
// 0x0000003A System.Void CFX_Demo_New/a::fv()
extern void a_fv_mEB08502AF115D1B45C99D8AD95D197DAF5283482 (void);
// 0x0000003B System.Object CFX_Demo_New/a::fw()
extern void a_fw_m6787263201C7E95FE5652A4BEE243E863DE52CC0 (void);
// 0x0000003C System.Void CFX_Demo_RandomDir::Start()
extern void CFX_Demo_RandomDir_Start_mBEA6B11F6CDBC5D92B9039AA8B18A7EBEF5448C8 (void);
// 0x0000003D System.Void CFX_Demo_RandomDir::.ctor()
extern void CFX_Demo_RandomDir__ctor_m0D93F82C71A0EAE6FF074A8F3EDBBB81ED3D0B48 (void);
// 0x0000003E System.Void CFX_Demo_RandomDirectionTranslate::Start()
extern void CFX_Demo_RandomDirectionTranslate_Start_m96E0BCED718092574499B7127C3A8673849208DB (void);
// 0x0000003F System.Void CFX_Demo_RandomDirectionTranslate::Update()
extern void CFX_Demo_RandomDirectionTranslate_Update_m4024B24E39F74439D91BB2A5C0077350853814AE (void);
// 0x00000040 System.Void CFX_Demo_RandomDirectionTranslate::.ctor()
extern void CFX_Demo_RandomDirectionTranslate__ctor_m0069918B310E5DB0ABB7A4081DAE7EAFD6A1E0CF (void);
// 0x00000041 System.Void CFX_Demo_RotateCamera::Update()
extern void CFX_Demo_RotateCamera_Update_mF984EC78638612B1C89597528E3FD5ACBD2539A4 (void);
// 0x00000042 System.Void CFX_Demo_RotateCamera::.ctor()
extern void CFX_Demo_RotateCamera__ctor_m0ADA46D335888F3F43AC518ED19FFB1F1AABC7C8 (void);
// 0x00000043 System.Void CFX_Demo_RotateCamera::.cctor()
extern void CFX_Demo_RotateCamera__cctor_m4A71CF2705030EA82F2818BD43F82DC91C01486C (void);
// 0x00000044 System.Void CFX_Demo_Translate::Start()
extern void CFX_Demo_Translate_Start_m2408E76C85181E0D900CEFEA8E3CE9214763AC76 (void);
// 0x00000045 System.Void CFX_Demo_Translate::Update()
extern void CFX_Demo_Translate_Update_mD71C66B91F9EC43DD9ABBA2D8A45CBD57DE8EBF7 (void);
// 0x00000046 System.Void CFX_Demo_Translate::.ctor()
extern void CFX_Demo_Translate__ctor_m44AD33D5825BE599180B3ED3E3668893F46E29DB (void);
// 0x00000047 System.Void CFX_AutoDestructShuriken::OnEnable()
extern void CFX_AutoDestructShuriken_OnEnable_m95AB27DC91BCCF60D378AE0665B095D44331D78A (void);
// 0x00000048 System.Collections.IEnumerator CFX_AutoDestructShuriken::CheckIfAlive()
extern void CFX_AutoDestructShuriken_CheckIfAlive_mCFAEBF84CD35FBF9C742C9A170183136AAB53535 (void);
// 0x00000049 System.Void CFX_AutoDestructShuriken::.ctor()
extern void CFX_AutoDestructShuriken__ctor_m22A7ABD6E38836099374390249F88639BB36CC33 (void);
// 0x0000004A System.Void CFX_AutoDestructShuriken/c::.ctor(System.Int32)
extern void c__ctor_mEAE65DED48436AAD1D041C00B2C2B7F65C00D9E0 (void);
// 0x0000004B System.Void CFX_AutoDestructShuriken/c::gc()
extern void c_gc_m09C7EB51128C9438A454158A2416A6D98AF6F84A (void);
// 0x0000004C System.Boolean CFX_AutoDestructShuriken/c::MoveNext()
extern void c_MoveNext_m0894A2BCD71F74704E245DDF5AD039636A5E7FD2 (void);
// 0x0000004D System.Object CFX_AutoDestructShuriken/c::gd()
extern void c_gd_m7D6B449267B6BF8464BF79AD94B4CA56B5B0ECF5 (void);
// 0x0000004E System.Void CFX_AutoDestructShuriken/c::ge()
extern void c_ge_mE459DC11359AEB26E2438A3566FA30FD23E75F30 (void);
// 0x0000004F System.Object CFX_AutoDestructShuriken/c::gf()
extern void c_gf_mE022ABC2223DE423753B07EC95F05DD9E0E2CC3B (void);
// 0x00000050 System.Void CFX_AutoRotate::Update()
extern void CFX_AutoRotate_Update_m6B9C72C2FC80E6C5FABD9DB647C8FDAE2BD56328 (void);
// 0x00000051 System.Void CFX_AutoRotate::.ctor()
extern void CFX_AutoRotate__ctor_m66822106F9299AE6FB3E7BEE628175E86FAEF866 (void);
// 0x00000052 System.Void CFX_LightFlicker::Awake()
extern void CFX_LightFlicker_Awake_mDD217C09D0CD73F750E12C784CF9B9671729817A (void);
// 0x00000053 System.Void CFX_LightFlicker::OnEnable()
extern void CFX_LightFlicker_OnEnable_mBAD2393E6F61D5D7981870E576F1238783ED8E3C (void);
// 0x00000054 System.Void CFX_LightFlicker::Update()
extern void CFX_LightFlicker_Update_m7EDE8F7D320D87C751799FE5EAEECAF24F33315E (void);
// 0x00000055 System.Void CFX_LightFlicker::.ctor()
extern void CFX_LightFlicker__ctor_mAB98FAF050C7B8A46472C42EBE3D802CBDFBE96C (void);
// 0x00000056 System.Void CFX_LightIntensityFade::Start()
extern void CFX_LightIntensityFade_Start_mA64A524D6D15ADAE266FF2F3F8F81F38ACBD3EB6 (void);
// 0x00000057 System.Void CFX_LightIntensityFade::OnEnable()
extern void CFX_LightIntensityFade_OnEnable_m56FA183840193584063C3B0AF49B0FA8DB8D7014 (void);
// 0x00000058 System.Void CFX_LightIntensityFade::Update()
extern void CFX_LightIntensityFade_Update_mCE17E56AE5A9CE5993315D8F00B6AD2C9BEC08B4 (void);
// 0x00000059 System.Void CFX_LightIntensityFade::.ctor()
extern void CFX_LightIntensityFade__ctor_m0CFBDFFAFF4B1B51471D1B4AE3A3DE92BB8EE722 (void);
// 0x0000005A System.Void SampleCustomHeader::Start()
extern void SampleCustomHeader_Start_mC6F0E5DE5F1E84CE5E80400678F0A0E7E7B04DDA (void);
// 0x0000005B System.Void SampleCustomHeader::Update()
extern void SampleCustomHeader_Update_mC8AEAE90831BC4426B0F939BF6CFF1B49AAE4A80 (void);
// 0x0000005C System.Void SampleCustomHeader::OnGUI()
extern void SampleCustomHeader_OnGUI_m2DD476D04D317B6C974ED5540A15153AC8C9D4A4 (void);
// 0x0000005D System.Void SampleCustomHeader::.ctor()
extern void SampleCustomHeader__ctor_m798BDF5D8247757A7C4804ADC588255B96D44BB3 (void);
// 0x0000005E System.Void SampleWebView::Awake()
extern void SampleWebView_Awake_mDBF7BF2B80A5864CCD88FFB647C77A1313A012F7 (void);
// 0x0000005F System.Collections.IEnumerator SampleWebView::gq()
extern void SampleWebView_gq_m2809F7A9285BB94E683285944D75EBBEAEC1E53E (void);
// 0x00000060 System.Void SampleWebView::.ctor()
extern void SampleWebView__ctor_mA1A876DE33B67AC2652D7D6EF97596B4789FB3E0 (void);
// 0x00000061 System.Void SampleWebView::gr(System.String)
extern void SampleWebView_gr_mE251629CC61E864D57B7E0319D2BC8DFB4B17A00 (void);
// 0x00000062 System.Void SampleWebView/<>c::.cctor()
extern void U3CU3Ec__cctor_mBA34CA72A72A17C43E319EFE05DBB2666725861F (void);
// 0x00000063 System.Void SampleWebView/<>c::.ctor()
extern void U3CU3Ec__ctor_m19620618A100D0F898A3AE8728E17A28B4EAE24D (void);
// 0x00000064 System.Void SampleWebView/<>c::gg(System.String)
extern void U3CU3Ec_gg_mA4C542A8B42ADC7B1F0342D4EA78077E85D0A8FB (void);
// 0x00000065 System.Void SampleWebView/<>c::gh(System.String)
extern void U3CU3Ec_gh_m2330B3CD4EE885003E91F41EE3D77D903CF6A256 (void);
// 0x00000066 System.Void SampleWebView/<>c::gi(System.String)
extern void U3CU3Ec_gi_mBD0DF78433D5CFF077235BEFD030B83E73E84C8B (void);
// 0x00000067 System.Void SampleWebView/<>c::gj(System.String)
extern void U3CU3Ec_gj_mAB3D2EA81AB384C7B8A8D0702A85EFFADACC9A23 (void);
// 0x00000068 System.Void SampleWebView/<>c::gk(System.String)
extern void U3CU3Ec_gk_m1E189BDEFC08730B568036EB304220A49AF6FDAC (void);
// 0x00000069 System.Void SampleWebView/<>c::gl(System.String)
extern void U3CU3Ec_gl_m24C9ED7082F971312880E3EE169D382BCA746143 (void);
// 0x0000006A System.Void SampleWebView/e::.ctor(System.Int32)
extern void e__ctor_m11AB00EF52CE90C90F8320DDF4B6544D3680329B (void);
// 0x0000006B System.Void SampleWebView/e::gm()
extern void e_gm_m43FD929C326648BA57102D2EEBF15C9FDA05551B (void);
// 0x0000006C System.Boolean SampleWebView/e::MoveNext()
extern void e_MoveNext_m3DC10366F1BEC32CBB865C06D52EAF80FC73BFBF (void);
// 0x0000006D System.Object SampleWebView/e::gn()
extern void e_gn_m0C80DDE6447CA6C5D38ED677CFBD9F3B16783624 (void);
// 0x0000006E System.Void SampleWebView/e::go()
extern void e_go_m2C55ADB30D91AA9C5E6C6B374BB736609FAE6EE4 (void);
// 0x0000006F System.Object SampleWebView/e::gp()
extern void e_gp_mCA7188E65CD594CFEAB6DAA329C4C8466F0F082E (void);
// 0x00000070 System.Void AdjustHandler::Awake()
extern void AdjustHandler_Awake_m35F7FCEEFFEBCC3692E9F12AF6A91DC909EDE7E6 (void);
// 0x00000071 System.Collections.IEnumerator AdjustHandler::Start()
extern void AdjustHandler_Start_m80DC13EF8E913B5B3335929C4B658CAD4F10AFC8 (void);
// 0x00000072 System.Void AdjustHandler::hb(System.Int32)
extern void AdjustHandler_hb_mED723B3CBE5C7327CCCFE54B55125015577FFDAC (void);
// 0x00000073 System.Collections.IEnumerator AdjustHandler::hc()
extern void AdjustHandler_hc_mAAED7D296F3E90984B66B87DBE990D2418D77A66 (void);
// 0x00000074 System.Void AdjustHandler::hd(com.adjust.sdk.AdjustAttribution)
extern void AdjustHandler_hd_mB326B5CF460D27AEC1B1094303307FB7D3113611 (void);
// 0x00000075 System.String AdjustHandler::he(com.adjust.sdk.AdjustAttribution)
extern void AdjustHandler_he_mAE07A4D190E8041B8EF2916BD9589F1FEE6CA10E (void);
// 0x00000076 System.Void AdjustHandler::.ctor()
extern void AdjustHandler__ctor_m570962B2FCA935A388F07235F8B8AB828F52075D (void);
// 0x00000077 System.Void AdjustHandler/f::.ctor(System.Int32)
extern void f__ctor_m9DA94C17CF1944A8258A3437D11AABE109E48C0D (void);
// 0x00000078 System.Void AdjustHandler/f::gs()
extern void f_gs_m8B1EE60493878D8788F5396568E721AD2540AB14 (void);
// 0x00000079 System.Boolean AdjustHandler/f::MoveNext()
extern void f_MoveNext_m5AEC2A0D5D8E398E7EFDDBD2F8F8285CF37C2468 (void);
// 0x0000007A System.Object AdjustHandler/f::gt()
extern void f_gt_mA07FC051C696623E5B77B2F7B78755D8B7E45BDF (void);
// 0x0000007B System.Void AdjustHandler/f::gu()
extern void f_gu_mEE0780C63017A8C3F4F13F6E8DFF54672B2D9AAB (void);
// 0x0000007C System.Object AdjustHandler/f::gv()
extern void f_gv_mC02AEEC4E79F4F2021A2CB31B819B3FFE86E3FE4 (void);
// 0x0000007D System.Void AdjustHandler/g::.ctor(System.Int32)
extern void g__ctor_m445BEB3DBA61FCEE90F3141CE36D2B2A9496DFB1 (void);
// 0x0000007E System.Void AdjustHandler/g::gw()
extern void g_gw_m5CA4FBF5A8C579DDACCB2FBA39A0EA3DD656622B (void);
// 0x0000007F System.Boolean AdjustHandler/g::MoveNext()
extern void g_MoveNext_m0AAF5480619752A53111A2C00C005F3FD7AFDE42 (void);
// 0x00000080 System.Void AdjustHandler/g::gx()
extern void g_gx_mB044E97103A0CDD0E5D835DA8779594BDD29432C (void);
// 0x00000081 System.Object AdjustHandler/g::gy()
extern void g_gy_mD9D156127056A6575C94B06B18AA59AD504AD1E2 (void);
// 0x00000082 System.Void AdjustHandler/g::gz()
extern void g_gz_mC1FBA134B4E740FF53AA7127AFC9925C5BE86336 (void);
// 0x00000083 System.Object AdjustHandler/g::ha()
extern void g_ha_m7454B2A24793DA64EBA94F8B342BFC67944A25EB (void);
// 0x00000084 System.Void ArcadeGameMachine::Play()
extern void ArcadeGameMachine_Play_m17039B4E6AC0D4E96382FC061785AA92AEC63BB9 (void);
// 0x00000085 System.Void ArcadeGameMachine::.ctor()
extern void ArcadeGameMachine__ctor_m760676BE1E78E6F5A682B0E1C44B8B088050FFD1 (void);
// 0x00000086 System.Void ButtonToMenu::OnEnable()
extern void ButtonToMenu_OnEnable_m52F918BF565F35765C83353B508D2B1191D04EA8 (void);
// 0x00000087 System.Void ButtonToMenu::hf()
extern void ButtonToMenu_hf_m816A092A7AB277E7C369B44C14497B6971B642E4 (void);
// 0x00000088 System.Void ButtonToMenu::.ctor()
extern void ButtonToMenu__ctor_mA276999DC46CBAC98358E7CDE03315354609B62D (void);
// 0x00000089 System.Void GameSelectorMenu::Awake()
extern void GameSelectorMenu_Awake_mFA9877852A501B1A35C38628FEDA689E87E55384 (void);
// 0x0000008A System.Void GameSelectorMenu::Update()
extern void GameSelectorMenu_Update_m6A189D526D74A5BBD74F2A958DD6722C91C60A32 (void);
// 0x0000008B UnityEngine.Collider GameSelectorMenu::hg(UnityEngine.Vector2)
extern void GameSelectorMenu_hg_mEED507F3769EA50D974C782C891456F9AE0D93F2 (void);
// 0x0000008C System.Void GameSelectorMenu::.ctor()
extern void GameSelectorMenu__ctor_m53267B2F8FEFDDE784631A84AEA63891BA1A5492 (void);
// 0x0000008D System.Void PlayBtn::Awake()
extern void PlayBtn_Awake_m35E8369CE1D28E36BB8FD03E6311599992917B2B (void);
// 0x0000008E System.Void PlayBtn::Update()
extern void PlayBtn_Update_mFA7AB764AABC986AC9C9A7FA3543296DF6F51E8A (void);
// 0x0000008F System.Void PlayBtn::.ctor()
extern void PlayBtn__ctor_m5E2FF67C86EEF88574AB9FB2403EAC0405DC3E04 (void);
// 0x00000090 System.Boolean RotatePhoneBannerUI::hh()
extern void RotatePhoneBannerUI_hh_m1B758106DA56135088BBA33C96AED07CADA1EE2D (void);
// 0x00000091 System.Void RotatePhoneBannerUI::hi(System.Boolean)
extern void RotatePhoneBannerUI_hi_mA34D0DD658614C701EFD38FEC34305E89A9EE86F (void);
// 0x00000092 System.Void RotatePhoneBannerUI::Update()
extern void RotatePhoneBannerUI_Update_mDB4AD4A38E23E94DC842D778FBA2A48DE2E025EA (void);
// 0x00000093 System.Void RotatePhoneBannerUI::.ctor()
extern void RotatePhoneBannerUI__ctor_m1349C2E7A28EE722FDB427C21AA2472DAC5A8F39 (void);
// 0x00000094 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_m025CE203564D82A1CDCE5E5719DB07E29811D0B7 (void);
// 0x00000095 System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_mD49D03719CAEBB3F59F24A7FA8F4FD30C8B54E46 (void);
// 0x00000096 System.Void ChatController::hj(System.String)
extern void ChatController_hj_mAEF1C2CFA99A469171B00812FE9E5AE4F55A0942 (void);
// 0x00000097 System.Void ChatController::.ctor()
extern void ChatController__ctor_m39C05E9EB8C8C40664D5655BCAB9EEBCB31F9719 (void);
// 0x00000098 System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_mF83641F913F3455A3AE6ADCEA5DEB2A323FCB58F (void);
// 0x00000099 System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m0F0C6DD803E99B2C15F3369ABD94EC273FADC75B (void);
// 0x0000009A System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_m1D86ECDDD4A7A6DF98748B11BAC74D2D3B2F9435 (void);
// 0x0000009B System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mB8A6567BB58BDFD0FC70980AFA952748DF1E80E9 (void);
// 0x0000009C System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_m465E8527E49D1AA672A9A8A3B96FE78C24D11138 (void);
// 0x0000009D System.Void EnvMapAnimator/h::.ctor(System.Int32)
extern void h__ctor_m6B340AB88E61A0146E4C2E5783A3B0650D52746E (void);
// 0x0000009E System.Void EnvMapAnimator/h::hk()
extern void h_hk_m0375CF0F39E8B262FCCC2699A5A6B1626238B366 (void);
// 0x0000009F System.Boolean EnvMapAnimator/h::MoveNext()
extern void h_MoveNext_m7D8F0181D594C1657EEEB84576EA263912F68FEA (void);
// 0x000000A0 System.Object EnvMapAnimator/h::hl()
extern void h_hl_m51F09F3A2591DC6D998E7468E3413B4F8A6707EB (void);
// 0x000000A1 System.Void EnvMapAnimator/h::hm()
extern void h_hm_m6D29E3C2B69860DD7DBAF394D13AFA3FA2674F2F (void);
// 0x000000A2 System.Object EnvMapAnimator/h::hn()
extern void h_hn_mCC7F7CBE7101E3EB3E50CC05060152E384BF3A75 (void);
// 0x000000A3 System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m786CF8A4D85EB9E1BE8785A58007F8796991BDB9 (void);
// 0x000000A4 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m9DC5F1168E5F4963C063C88384ADEBA8980BBFE0 (void);
// 0x000000A5 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mE50FE1DE042CE58055C824840D77FCDA6A2AF4D3 (void);
// 0x000000A6 System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_m70833F265A016119F88136746B4C59F45B5E067D (void);
// 0x000000A7 TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_mA62049738125E3C48405E6DFF09E2D42300BE8C3 (void);
// 0x000000A8 System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m6B85C54F4E751BF080324D94FB8DA6286CD5A43C (void);
// 0x000000A9 TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m95CDEB7394FFF38F310717EEEFDCD481D96A5E82 (void);
// 0x000000AA System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_mFFBD9D70A791A3F2065C1063F258465EDA8AC2C5 (void);
// 0x000000AB TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mF22771B4213EEB3AEFCDA390A4FF28FED5D9184C (void);
// 0x000000AC System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_mA7EB31AF14EAADD968857DDAC994F7728B7B02E3 (void);
// 0x000000AD TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mDDF07E7000993FCD6EAF2FBD2D2226EB66273908 (void);
// 0x000000AE System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m098580AA8098939290113692072E18F9A293B427 (void);
// 0x000000AF TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_m87FB9EABE7F917B2F910A18A3B5F1AE3020D976D (void);
// 0x000000B0 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m6741C71F7E218C744CD7AA18B7456382E4B703FF (void);
// 0x000000B1 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_mE2D7EB8218B248F11BE54C507396B9B6B12E0052 (void);
// 0x000000B2 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mBF0056A3C00834477F7D221BEE17C26784559DE1 (void);
// 0x000000B3 System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_mF5B4CCF0C9F2EFE24B6D4C7B31C620C91ABBC07A (void);
// 0x000000B4 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mC0561024D04FED2D026BEB3EC183550092823AE6 (void);
// 0x000000B5 System.Void TMPro.TMP_TextEventHandler::ho(System.Char,System.Int32)
extern void TMP_TextEventHandler_ho_m10176A9C3F6463CAC4C490194928C60156F0FDC9 (void);
// 0x000000B6 System.Void TMPro.TMP_TextEventHandler::hp(System.Char,System.Int32)
extern void TMP_TextEventHandler_hp_mABB9F448E9F5CF01F736D70CB50055D0D50E0073 (void);
// 0x000000B7 System.Void TMPro.TMP_TextEventHandler::hq(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_hq_m01B60E51F51C098C6C6462E434CF4545F2986763 (void);
// 0x000000B8 System.Void TMPro.TMP_TextEventHandler::hr(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_hr_m67CF249BFB77A6FDBCCD1EC5DDD18B137580D8F1 (void);
// 0x000000B9 System.Void TMPro.TMP_TextEventHandler::hs(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_hs_mD2D86832465F691E4A0BE2F48F45895253CE221B (void);
// 0x000000BA System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_mADE4C28CAE14991CF0B1CC1A9D0EBAF0CF1107AB (void);
// 0x000000BB System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m054FE9253D3C4478F57DE900A15AC9A61EC3C11E (void);
// 0x000000BC System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m89C1D1F720F140491B28D9B32B0C7202EE8C4963 (void);
// 0x000000BD System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m3F52F327A9627042EDB065C1080CEB764F1154F2 (void);
// 0x000000BE System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m419828B3E32BC3F6F5AAC88D7B90CF50A74C80B2 (void);
// 0x000000BF System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m4083D6FF46F61AAF956F77FFE849B5166E2579BC (void);
// 0x000000C0 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_m6CF91B0D99B3AC9317731D0C08B2EDA6AA56B9E9 (void);
// 0x000000C1 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_m9E12F5F809E8FF4A6EEFCDB016C1F884716347C4 (void);
// 0x000000C2 System.Void TMPro.Examples.Benchmark01/i::.ctor(System.Int32)
extern void i__ctor_mC530C3F49395DE615CBB7ECB5A8DCF9F9EE7ECAF (void);
// 0x000000C3 System.Void TMPro.Examples.Benchmark01/i::ht()
extern void i_ht_m621AE8BEBE5E335E6548154E8316CF5EB786B103 (void);
// 0x000000C4 System.Boolean TMPro.Examples.Benchmark01/i::MoveNext()
extern void i_MoveNext_mCB114E94E32FE4C5C050B482AC9A4554FF2614EA (void);
// 0x000000C5 System.Object TMPro.Examples.Benchmark01/i::hu()
extern void i_hu_m6120099F71F945E63398282615622CB6AC75AA8D (void);
// 0x000000C6 System.Void TMPro.Examples.Benchmark01/i::hv()
extern void i_hv_m1BEDED11DDCF3F61B8938E65B0E888E698B334B9 (void);
// 0x000000C7 System.Object TMPro.Examples.Benchmark01/i::hw()
extern void i_hw_mB8619157279E23098F2E297BC9C2EEDA5EDA4356 (void);
// 0x000000C8 System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m565A619941AAFFC17BB16A4A73DF63F7E54E3AFA (void);
// 0x000000C9 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_m9DCE74210552C6961BF7460C1F812E484771F8EB (void);
// 0x000000CA System.Void TMPro.Examples.Benchmark01_UGUI/j::.ctor(System.Int32)
extern void j__ctor_m58D42A7FBF05708666272FADEE61F161606D1884 (void);
// 0x000000CB System.Void TMPro.Examples.Benchmark01_UGUI/j::hx()
extern void j_hx_m6827F98282F00769DB1373FF805DF0D9EABD213D (void);
// 0x000000CC System.Boolean TMPro.Examples.Benchmark01_UGUI/j::MoveNext()
extern void j_MoveNext_m3ADC8AD16F89087CC2707835166AC092164D89B1 (void);
// 0x000000CD System.Object TMPro.Examples.Benchmark01_UGUI/j::hy()
extern void j_hy_mF3F711E8FC914F62678160608244E6E83BE1E047 (void);
// 0x000000CE System.Void TMPro.Examples.Benchmark01_UGUI/j::hz()
extern void j_hz_m04CC16B6185A5E81EDDB6A7A2FA20B3B06FB1BBD (void);
// 0x000000CF System.Object TMPro.Examples.Benchmark01_UGUI/j::ia()
extern void j_ia_mEEDF9B01D48D4FB51833BD6025FB4E455856B2BD (void);
// 0x000000D0 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_mB56F21A9861A3DAF9F4E7F1DD4A023E05B379E29 (void);
// 0x000000D1 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_mE5DCB1CF4C1FDBA742B51B11427B9DE209630BF1 (void);
// 0x000000D2 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mDEE8E96AE811C5A84CB2C04440CD4662E2F918D3 (void);
// 0x000000D3 System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_mCCFD9402E218265F6D34A1EA7ACCD3AD3D80380D (void);
// 0x000000D4 System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m8A29BB2CC6375B2D3D57B5A90D18F2435352E5F6 (void);
// 0x000000D5 System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mD2F5056019DD08B3DB897F6D194E86AB66E92F90 (void);
// 0x000000D6 System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m282E4E495D8D1921A87481729549B68BEDAD2D27 (void);
// 0x000000D7 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m2D75756734457ADE0F15F191B63521A47C426788 (void);
// 0x000000D8 System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m749E20374F32FF190EC51D70C717A8117934F2A5 (void);
// 0x000000D9 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m07E7F5C7D91713F8BB489480304D130570D7858F (void);
// 0x000000DA System.Void TMPro.Examples.CameraController::ib()
extern void CameraController_ib_m83FE5CDB1E9F66368318A419277299F62BB8A50F (void);
// 0x000000DB System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_mE37608FBFBF61F76A1E0EEACF79B040321476878 (void);
// 0x000000DC System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC05FEB5A72FED289171C58787FE09DBD9356FC72 (void);
// 0x000000DD System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_m7FB0886C3E6D76C0020E4D38DC1C44AB70BF3695 (void);
// 0x000000DE System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_mA786C14AE887FF4012A35FAB3DF59ECF6A77835A (void);
// 0x000000DF System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_m3D158D58F1840CBDA3B887326275893121E31371 (void);
// 0x000000E0 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mEF0B5D3EE00206199ABB80CE893AA85DF3FE5C88 (void);
// 0x000000E1 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::ig()
extern void ShaderPropAnimator_ig_mD86932227DD6A345FFBCB9AC858B148B9DA35DEA (void);
// 0x000000E2 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_m51C29C66EFD7FCA3AE68CDEFD38A4A89BF48220B (void);
// 0x000000E3 System.Void TMPro.Examples.ShaderPropAnimator/k::.ctor(System.Int32)
extern void k__ctor_m0E13644123979DE6E6A5FDEBD98901953F591528 (void);
// 0x000000E4 System.Void TMPro.Examples.ShaderPropAnimator/k::ic()
extern void k_ic_m74B95B414992423EDC77E40C1161CD97498BD668 (void);
// 0x000000E5 System.Boolean TMPro.Examples.ShaderPropAnimator/k::MoveNext()
extern void k_MoveNext_m9BD5A66A6C4059F26C23CA1712AFFCEB011EE0C6 (void);
// 0x000000E6 System.Object TMPro.Examples.ShaderPropAnimator/k::id()
extern void k_id_mF7536BD4AC3795CCA0FAD701FD59CD5A022228BD (void);
// 0x000000E7 System.Void TMPro.Examples.ShaderPropAnimator/k::ie()
extern void k_ie_m3AD2F19911AE13D26B50BB63D0934B3AB82A54CE (void);
// 0x000000E8 System.Object TMPro.Examples.ShaderPropAnimator/k::if()
extern void k_if_mEBAF7969AC5263A8106F3A525D56589696A7E6E2 (void);
// 0x000000E9 System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_mC4159EF79F863FBD86AEA2B81D86FDF04834A6F8 (void);
// 0x000000EA System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mBD8A31D53D01FEBB9B432077599239AC6A5DEAFE (void);
// 0x000000EB System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mC91E912195EEE18292A8FCA7650739E3DDB81807 (void);
// 0x000000EC System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m2D48E0903620C2D870D5176FCFD12A8989801C93 (void);
// 0x000000ED System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m7577B96B07C4EB0666BF6F028074176258009690 (void);
// 0x000000EE UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::il(UnityEngine.AnimationCurve)
extern void SkewTextExample_il_mE7ED9BEC964184449A257BEDC5A343B8A2E70933 (void);
// 0x000000EF System.Collections.IEnumerator TMPro.Examples.SkewTextExample::im()
extern void SkewTextExample_im_m35D80FB7B9888808EDDFD8DBEB1829F43D55463E (void);
// 0x000000F0 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m711325FB390A6DFA994B6ADF746C9EBF846A0A22 (void);
// 0x000000F1 System.Void TMPro.Examples.SkewTextExample/l::.ctor(System.Int32)
extern void l__ctor_mAF3BE015E0DDE8EEC10E29A93250E767CAFC1AC0 (void);
// 0x000000F2 System.Void TMPro.Examples.SkewTextExample/l::ih()
extern void l_ih_m03302FBAB431F2C105F1D4FACF179258499FAC57 (void);
// 0x000000F3 System.Boolean TMPro.Examples.SkewTextExample/l::MoveNext()
extern void l_MoveNext_m8AEA21F97F8EB0D21E0026569F76A764CA382A86 (void);
// 0x000000F4 System.Object TMPro.Examples.SkewTextExample/l::ii()
extern void l_ii_m2982E065EEFBB340F883048771376D8A4EAD3AF5 (void);
// 0x000000F5 System.Void TMPro.Examples.SkewTextExample/l::ij()
extern void l_ij_m54D56190B1A7C601D06E1E5C518236F68985E62E (void);
// 0x000000F6 System.Object TMPro.Examples.SkewTextExample/l::ik()
extern void l_ik_mEF266F1D2BFB348363A20864B6203F0322565923 (void);
// 0x000000F7 System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m8D56A3C1E06AD96B35B88C3AA8C61FB2A03E627D (void);
// 0x000000F8 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m3BFE1E2B1BB5ED247DED9DBEF293FCCBD63760C6 (void);
// 0x000000F9 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m824BBE09CC217EB037FFB36756726A9C946526D0 (void);
// 0x000000FA System.Void TMPro.Examples.TeleType/m::.ctor(System.Int32)
extern void m__ctor_mCB70A1ABAC23935609971C4E5EB96C6FCA781B80 (void);
// 0x000000FB System.Void TMPro.Examples.TeleType/m::in()
extern void m_in_mCDE4E87D88A0FAC9F826069CE36922F8512E01DC (void);
// 0x000000FC System.Boolean TMPro.Examples.TeleType/m::MoveNext()
extern void m_MoveNext_m3CA9C2D89FD2EF31C9C0BBBB58ACACA97A2536A0 (void);
// 0x000000FD System.Object TMPro.Examples.TeleType/m::io()
extern void m_io_m0C35CAA36DF9EFBDC111F0BC378D68B6D09626DC (void);
// 0x000000FE System.Void TMPro.Examples.TeleType/m::ip()
extern void m_ip_m9C7DA6916EE3D7157BEF78A60D893E127C7A272F (void);
// 0x000000FF System.Object TMPro.Examples.TeleType/m::iq()
extern void m_iq_mCF04D07167DDEBAF4CC0930C6B8184E06EF4B694 (void);
// 0x00000100 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_m55D28DC1F590D98621B0284B53C8A22D07CD3F7C (void);
// 0x00000101 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m5667F64AE1F48EBA2FF1B3D2D53E2AFCAB738B39 (void);
// 0x00000102 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_mDF58D349E4D62866410AAA376BE5BBAE4153FF95 (void);
// 0x00000103 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m4B3A741D6C5279590453148419B422E8D7314689 (void);
// 0x00000104 System.Void TMPro.Examples.TextConsoleSimulator::iz(UnityEngine.Object)
extern void TextConsoleSimulator_iz_m06C0361CEBBE88F56C5A3FDE11B8F3F219552ECD (void);
// 0x00000105 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::ja(TMPro.TMP_Text)
extern void TextConsoleSimulator_ja_mA0E3929F8A34D0439D8313A6F84713FD5B900CAF (void);
// 0x00000106 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::jb(TMPro.TMP_Text)
extern void TextConsoleSimulator_jb_m055C31AA087A705E9571436739090544432F62D4 (void);
// 0x00000107 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_mBDDE8A2DCED8B140D78D5FE560897665753AB025 (void);
// 0x00000108 System.Void TMPro.Examples.TextConsoleSimulator/n::.ctor(System.Int32)
extern void n__ctor_mA3995540D306C82F96A54411435FBD7EEAB53191 (void);
// 0x00000109 System.Void TMPro.Examples.TextConsoleSimulator/n::ir()
extern void n_ir_m315FDA71861EF32023C968840C0A7129B6AA5265 (void);
// 0x0000010A System.Boolean TMPro.Examples.TextConsoleSimulator/n::MoveNext()
extern void n_MoveNext_mE8B628C5342720FFE6275001CD07C345E43A134B (void);
// 0x0000010B System.Object TMPro.Examples.TextConsoleSimulator/n::is()
extern void n_is_m8257E331D55FFDB31DD8B42C3928F9868035C1EE (void);
// 0x0000010C System.Void TMPro.Examples.TextConsoleSimulator/n::it()
extern void n_it_m4FDE17A6C417D01AD2A6DDE4ACDE2D56AD781C83 (void);
// 0x0000010D System.Object TMPro.Examples.TextConsoleSimulator/n::iu()
extern void n_iu_mA9778BFC632F9DC80EDCF210006146DD5F85C075 (void);
// 0x0000010E System.Void TMPro.Examples.TextConsoleSimulator/o::.ctor(System.Int32)
extern void o__ctor_mBC755591F3EC20D1AC8A3A28D113836B1267B142 (void);
// 0x0000010F System.Void TMPro.Examples.TextConsoleSimulator/o::iv()
extern void o_iv_m4D22337709BE1949935BA9A8287A0FAF3A740AD4 (void);
// 0x00000110 System.Boolean TMPro.Examples.TextConsoleSimulator/o::MoveNext()
extern void o_MoveNext_m79D9ADB208208E5B9BF2FDCDF5600F956EE7937D (void);
// 0x00000111 System.Object TMPro.Examples.TextConsoleSimulator/o::iw()
extern void o_iw_m98B2B1A2B21FA48996E10643E9797DB1F6BB8066 (void);
// 0x00000112 System.Void TMPro.Examples.TextConsoleSimulator/o::ix()
extern void o_ix_m15A06D70A3046AAE6D49E796CDDEC522651469FD (void);
// 0x00000113 System.Object TMPro.Examples.TextConsoleSimulator/o::iy()
extern void o_iy_m77BAAF5CDF74D675B99F53F3BDE547ABEA76B59C (void);
// 0x00000114 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m600F1825C26BB683047156FD815AE4376D2672F2 (void);
// 0x00000115 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m8121246A4310A0014ECA36144B9DCE093FE8AE49 (void);
// 0x00000116 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_mA1E370089458CD380E9BA7740C2BC2032F084148 (void);
// 0x00000117 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_mA02B20CF33E43FE99FD5F1B90F7F350262F0BEBE (void);
// 0x00000118 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_mD08AF0FB6944A51BC6EA15D6BE4E33AA4A916E3E (void);
// 0x00000119 System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m272097816057A64A9FFE16F69C6844DCF88E9557 (void);
// 0x0000011A System.Void TMPro.Examples.TextMeshProFloatingText/p::.ctor(System.Int32)
extern void p__ctor_mB34FCBF53494493E2865B3FD2D92C4DE8AFBABB1 (void);
// 0x0000011B System.Void TMPro.Examples.TextMeshProFloatingText/p::jc()
extern void p_jc_m0B9AAE9BA9FE0D780D6059C64D3CD7A8833B5957 (void);
// 0x0000011C System.Boolean TMPro.Examples.TextMeshProFloatingText/p::MoveNext()
extern void p_MoveNext_m11CCFB7C78636D21CC2D7637F5A61463242F350A (void);
// 0x0000011D System.Object TMPro.Examples.TextMeshProFloatingText/p::jd()
extern void p_jd_mE37A7F27E4840BD35EF04CE536A5F909A3F9B1FE (void);
// 0x0000011E System.Void TMPro.Examples.TextMeshProFloatingText/p::je()
extern void p_je_mD488CCE44FD425C693F957737161BF467DC1E889 (void);
// 0x0000011F System.Object TMPro.Examples.TextMeshProFloatingText/p::jf()
extern void p_jf_m6DD1469B64878BECF0F5076848C995B231753A0A (void);
// 0x00000120 System.Void TMPro.Examples.TextMeshProFloatingText/q::.ctor(System.Int32)
extern void q__ctor_m5F6271E3EAB84B4538FF3C66B8A1AE62B09BEA4F (void);
// 0x00000121 System.Void TMPro.Examples.TextMeshProFloatingText/q::jg()
extern void q_jg_m06591336EE6FA978DB73106B01801713E708B9C4 (void);
// 0x00000122 System.Boolean TMPro.Examples.TextMeshProFloatingText/q::MoveNext()
extern void q_MoveNext_mC95A783178593C950AE1577E0AE3D18FA87E344A (void);
// 0x00000123 System.Object TMPro.Examples.TextMeshProFloatingText/q::jh()
extern void q_jh_m443C4C9AABF6EDEDAC843DFBFBF052BEF38D0750 (void);
// 0x00000124 System.Void TMPro.Examples.TextMeshProFloatingText/q::ji()
extern void q_ji_mE06B38568964AABB1FFE57EB9AA006FA93732AC9 (void);
// 0x00000125 System.Object TMPro.Examples.TextMeshProFloatingText/q::jj()
extern void q_jj_m21C9A03FE653DF3EDE43ACE2A031EC3C1A7A8086 (void);
// 0x00000126 System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_m9A84A570D2582918A6B1287139527E9AB2CF088D (void);
// 0x00000127 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m3EE98071CA27A18904B859A0A6B215BDFEB50A66 (void);
// 0x00000128 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m8409A62C31C4A6B6CEC2F48F1DC9777460C28233 (void);
// 0x00000129 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m0F92D44F62A9AC086DE3DF1E4C7BFAF645EE7084 (void);
// 0x0000012A System.Void TMPro.Examples.TMPro_InstructionOverlay::jk(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_jk_mFDA23063ADF56A8C7FD3DA5F860744B0C65E8215 (void);
// 0x0000012B System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m247258528E488171765F77A9A3C6B7E079E64839 (void);
// 0x0000012C System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m6E620605AE9CCC3789A2D5CFD841E5DAB8592063 (void);
// 0x0000012D System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m3D4A9AB04728F0ABD4C7C8A462E2C811308D97A1 (void);
// 0x0000012E System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m43F9206FDB1606CD28F1A441188E777546CFEA2A (void);
// 0x0000012F System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m99156EF53E5848DE83107BFAC803C33DC964265C (void);
// 0x00000130 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_m9B5D0A86D174DA019F3EB5C6E9BD54634B2F909A (void);
// 0x00000131 System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5251EE9AC9DCB99D0871EE83624C8A9012E6A079 (void);
// 0x00000132 System.Void TMPro.Examples.TMP_FrameRateCounter::jl(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_jl_mA573CB9DDB0233EEAA73D98321369EE744A44F83 (void);
// 0x00000133 System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mD8804AE37CED37A01DF943624D3C2C48FBC9AE43 (void);
// 0x00000134 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mABF0C00DDBB37230534C49AD9CA342D96757AA3E (void);
// 0x00000135 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m4AE76C19CBF131CB80B73A7C71378CA063CFC4C6 (void);
// 0x00000136 System.Void TMPro.Examples.TMP_TextEventCheck::jm(System.Char,System.Int32)
extern void TMP_TextEventCheck_jm_mCEC0687FC3C3F477362F0817739562D68E56EA28 (void);
// 0x00000137 System.Void TMPro.Examples.TMP_TextEventCheck::jn(System.Char,System.Int32)
extern void TMP_TextEventCheck_jn_mD29C63E452E309CBA4752C18CE58CEB1FACC8AB4 (void);
// 0x00000138 System.Void TMPro.Examples.TMP_TextEventCheck::jo(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_jo_m75996E48CC8DF8E721A3117405642FCFAB2A3EA6 (void);
// 0x00000139 System.Void TMPro.Examples.TMP_TextEventCheck::jp(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_jp_m49B65FC01AC32E3D95AA5E8F4DF514CE6D297F8B (void);
// 0x0000013A System.Void TMPro.Examples.TMP_TextEventCheck::jq(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_jq_m258909931D38942078DA6BDFCFC07D887605B5F5 (void);
// 0x0000013B System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_m8F6CDB8774BDF6C6B909919393AC0290BA2BB0AF (void);
// 0x0000013C System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_m54C6EE99B1DC2B4DE1F8E870974B3B41B970C37E (void);
// 0x0000013D System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_m662ED2E3CDB7AE16174109344A01A50AF3C44797 (void);
// 0x0000013E System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_m1A711EC87962C6C5A7157414CD059D984D3BD55B (void);
// 0x0000013F System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_m747F05CBEF90BF713BF726E47CA37DC86D9B439A (void);
// 0x00000140 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_m5D7D8A07591506FB7291E84A951AB5C43DAA5503 (void);
// 0x00000141 System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_m4C56A438A3140D5CF9C7AFB8466E11142F4FA3BE (void);
// 0x00000142 System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m773D4C87E67823272DBF597B9CADE82DD3BFFD87 (void);
// 0x00000143 System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m8DA695DB0913F7123C4ADAFD5BEAB4424FA5861B (void);
// 0x00000144 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_mF2EF7AE0E015218AB77936BD5FD6863F7788F11D (void);
// 0x00000145 System.Void TMPro.Examples.TMP_TextSelector_B::jr(UnityEngine.Object)
extern void TMP_TextSelector_B_jr_m74B679824B6593D33B0D63336617E1DAC9C9F2F5 (void);
// 0x00000146 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_mE1B3969D788695E37240927FC6B1827CC6DD5EFF (void);
// 0x00000147 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_mBAF5711E20E579D21258BD4040454A64E1134D98 (void);
// 0x00000148 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m40ED8F7E47FF6FD8B38BE96B2216267F61509D65 (void);
// 0x00000149 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m773B56D918B1D0F73C5ABC0EB22FD34D39AFBB97 (void);
// 0x0000014A System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_mF409D728900872CC323B18DDA7F91265058BE772 (void);
// 0x0000014B System.Void TMPro.Examples.TMP_TextSelector_B::js(System.Int32)
extern void TMP_TextSelector_B_js_m1642A106C244D5BE2A2110858B3AF14BB1B7D3F2 (void);
// 0x0000014C System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mB45DD6360094ADBEF5E8020E8C62404B7E45E301 (void);
// 0x0000014D System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m3E0ECAD08FA25B61DD75F4D36EC3F1DE5A22A491 (void);
// 0x0000014E System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m11EF02C330E5D834C41F009CF088A3150352567F (void);
// 0x0000014F System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m568E467033B0FF7C67251895A0772CFA197789A3 (void);
// 0x00000150 System.Void TMPro.Examples.TMP_UiFrameRateCounter::jt(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_jt_m43C3DC714E0A0A880E4FA70433C67C30DA8175E7 (void);
// 0x00000151 System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_mBF5305427799EBC515580C2747FE604A6DFEC848 (void);
// 0x00000152 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m8895A9C06DB3EC4379334601DC726F1AFAF543C1 (void);
// 0x00000153 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_m36846DA72BFC7FDFA944A368C9DB62D17A15917B (void);
// 0x00000154 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::jy()
extern void VertexColorCycler_jy_m0B46ACC0725ED373B4C3B54F3354987C750B734F (void);
// 0x00000155 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m673CA077DC5E935BABCEA79E5E70116E9934F4C1 (void);
// 0x00000156 System.Void TMPro.Examples.VertexColorCycler/r::.ctor(System.Int32)
extern void r__ctor_m5628C84890815547CA50B062495F0DC7B918F9E4 (void);
// 0x00000157 System.Void TMPro.Examples.VertexColorCycler/r::ju()
extern void r_ju_m5951E3BD52BD7561EE4519538B5883A700806A4F (void);
// 0x00000158 System.Boolean TMPro.Examples.VertexColorCycler/r::MoveNext()
extern void r_MoveNext_mACC42A6A064C31245ACC4947EC3AC18D6FA30203 (void);
// 0x00000159 System.Object TMPro.Examples.VertexColorCycler/r::jv()
extern void r_jv_mBCD38631F9A548A6428C436DC7BE82E18047B4CF (void);
// 0x0000015A System.Void TMPro.Examples.VertexColorCycler/r::jw()
extern void r_jw_mBB1AE2A2217CA7DB280D9F0B73CFC98DCEFFB2FC (void);
// 0x0000015B System.Object TMPro.Examples.VertexColorCycler/r::jx()
extern void r_jx_mF60EF9840279CC559540C268B494CD720AFF8565 (void);
// 0x0000015C System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m0DF2AC9C728A15EEB427F1FE2426E3C31FBA544C (void);
// 0x0000015D System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_mCD5C1FDDBA809B04AC6F6CB00562D0AA45BC4354 (void);
// 0x0000015E System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mB670406B3982BFC44CB6BB05A73F1BE877FDFAF2 (void);
// 0x0000015F System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_mDE6155803CF2B1E6CE0EBAE8DF7DB93601E1DD76 (void);
// 0x00000160 System.Void TMPro.Examples.VertexJitter::kd(UnityEngine.Object)
extern void VertexJitter_kd_mBEB57841E43FF2BF6F9831AC747D7CA7E59EA718 (void);
// 0x00000161 System.Collections.IEnumerator TMPro.Examples.VertexJitter::ke()
extern void VertexJitter_ke_mE63433B45D26689CD57294E3EB5C7C229C690F57 (void);
// 0x00000162 System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m41E4682405B3C0B19779BA8CB77156D65D64716D (void);
// 0x00000163 System.Void TMPro.Examples.VertexJitter/u::.ctor(System.Int32)
extern void u__ctor_m9BE8090269B8C0F19ECDE71683EA23CE009E4D4D (void);
// 0x00000164 System.Void TMPro.Examples.VertexJitter/u::jz()
extern void u_jz_m3316E1CC21286F58CF3AAD7AA1E8F0A36F049F4A (void);
// 0x00000165 System.Boolean TMPro.Examples.VertexJitter/u::MoveNext()
extern void u_MoveNext_m905638306B83A76E5617D10058F5C0DB729308FF (void);
// 0x00000166 System.Object TMPro.Examples.VertexJitter/u::ka()
extern void u_ka_m2F20B214BB9365CAA957E5146E734A41DA390C15 (void);
// 0x00000167 System.Void TMPro.Examples.VertexJitter/u::kb()
extern void u_kb_m9ED3B7DF1A711267A2DB115A209B4387FA9BBC0E (void);
// 0x00000168 System.Object TMPro.Examples.VertexJitter/u::kc()
extern void u_kc_m9181707C46A64B0C34FF699A76168D5F849A44A6 (void);
// 0x00000169 System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m092957B0A67A153E7CD56A75A438087DE4806867 (void);
// 0x0000016A System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m52E2A036C9EB2C1D633BA7F43E31C36983972304 (void);
// 0x0000016B System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m52F58AF9438377D222543AA67CFF7B30FCCB0F23 (void);
// 0x0000016C System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_mDD8B5538BDFBC2BA242B997B879E7ED64ACAFC5E (void);
// 0x0000016D System.Void TMPro.Examples.VertexShakeA::kj(UnityEngine.Object)
extern void VertexShakeA_kj_m5E8022945BD70C4B82489FD6B4D56B6EF0CE59B3 (void);
// 0x0000016E System.Collections.IEnumerator TMPro.Examples.VertexShakeA::kk()
extern void VertexShakeA_kk_m4537D9F71FAE5C85AEA9292FB96DF245E174D87A (void);
// 0x0000016F System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m63ED483A292CA310B90144E0779C0472AAC22CBB (void);
// 0x00000170 System.Void TMPro.Examples.VertexShakeA/w::.ctor(System.Int32)
extern void w__ctor_m9062DB7CD727E5039B61D6BDF8F36C019633E8B0 (void);
// 0x00000171 System.Void TMPro.Examples.VertexShakeA/w::kf()
extern void w_kf_mB02200729610F0F6FE271DB5855B30A4241396C8 (void);
// 0x00000172 System.Boolean TMPro.Examples.VertexShakeA/w::MoveNext()
extern void w_MoveNext_mE10C0D457AB1E0558442E89928DB5A6D69D0DEFD (void);
// 0x00000173 System.Object TMPro.Examples.VertexShakeA/w::kg()
extern void w_kg_m66D88FE5A7AF6C3A18F0575A43AC1F376374099D (void);
// 0x00000174 System.Void TMPro.Examples.VertexShakeA/w::kh()
extern void w_kh_mBBAD076D1EE2A083186EE224EFED6494F0D89CB4 (void);
// 0x00000175 System.Object TMPro.Examples.VertexShakeA/w::ki()
extern void w_ki_m07388D6C835BB60EA8FF2D04F14FC91A52190232 (void);
// 0x00000176 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_mFA9A180BD1769CC79E6325314B5652D605ABE58E (void);
// 0x00000177 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m4999DF4598174EDA2A47F4F667B5CE061DF97C21 (void);
// 0x00000178 System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m2FB32CBD277A271400BF8AF2A35294C09FE9B8E5 (void);
// 0x00000179 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m58786A0944340EF16E024ADB596C9AB5686C2AF1 (void);
// 0x0000017A System.Void TMPro.Examples.VertexShakeB::kp(UnityEngine.Object)
extern void VertexShakeB_kp_m3E393480AB2B628BEC34C959E331D1D69479D528 (void);
// 0x0000017B System.Collections.IEnumerator TMPro.Examples.VertexShakeB::kq()
extern void VertexShakeB_kq_mCFB1D0B2BCFAE00997AB9CA5801BCEA6B2E782CC (void);
// 0x0000017C System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m9D068774503CF8642CC0BAC0E909ECE91E4E2198 (void);
// 0x0000017D System.Void TMPro.Examples.VertexShakeB/x::.ctor(System.Int32)
extern void x__ctor_mDFF851EC8FE819AE218E1C2423A1C5FA1823DAA5 (void);
// 0x0000017E System.Void TMPro.Examples.VertexShakeB/x::kl()
extern void x_kl_mAF95ABCFAD6F688242A22C1D657A6A277E65C835 (void);
// 0x0000017F System.Boolean TMPro.Examples.VertexShakeB/x::MoveNext()
extern void x_MoveNext_m7681A14E42D44387309A2A58AEC8195DB0EE633D (void);
// 0x00000180 System.Object TMPro.Examples.VertexShakeB/x::km()
extern void x_km_m2793D456E15D8EDDCECC13B254DFCC204DD70980 (void);
// 0x00000181 System.Void TMPro.Examples.VertexShakeB/x::kn()
extern void x_kn_m822A6A2EA852A37BA727E3F4A76B502CC13C451A (void);
// 0x00000182 System.Object TMPro.Examples.VertexShakeB/x::ko()
extern void x_ko_m6E8BC32AD702CF0BB32EFCA7947FF8B1854B83C8 (void);
// 0x00000183 System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m29C1DE789B968D726EDD69F605321A223D47C1A0 (void);
// 0x00000184 System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_mE3719F01B6A8590066988F425F8A63103B5A7B47 (void);
// 0x00000185 System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_mBB91C9EFA049395743D27358A427BB2B05850B47 (void);
// 0x00000186 System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_mB03D03148C98EBC9117D69510D24F21978546FCB (void);
// 0x00000187 System.Void TMPro.Examples.VertexZoom::kw(UnityEngine.Object)
extern void VertexZoom_kw_m3D401A5F9A8620F38C66059CE239ACDFC48D62E4 (void);
// 0x00000188 System.Collections.IEnumerator TMPro.Examples.VertexZoom::kx()
extern void VertexZoom_kx_mB22DAE2D2D0D48A93A6DC7C66D673B3D12C5982E (void);
// 0x00000189 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_m454AF80ACB5C555BCB4B5E658A22B5A4FCC39422 (void);
// 0x0000018A System.Void TMPro.Examples.VertexZoom/y::.ctor()
extern void y__ctor_m45DC59D4511BC8174A2BE8796070DF0FCE0030B6 (void);
// 0x0000018B System.Int32 TMPro.Examples.VertexZoom/y::kr(System.Int32,System.Int32)
extern void y_kr_mC1EB6F2441C4B860E5C1E4366758FD91F11DDD9A (void);
// 0x0000018C System.Void TMPro.Examples.VertexZoom/z::.ctor(System.Int32)
extern void z__ctor_m7411FA8C4ABD8809ADDBD6240F0776E7A2015206 (void);
// 0x0000018D System.Void TMPro.Examples.VertexZoom/z::ks()
extern void z_ks_m4F0050A286C74696628047135735807A8503A6B6 (void);
// 0x0000018E System.Boolean TMPro.Examples.VertexZoom/z::MoveNext()
extern void z_MoveNext_m64D17099BD43C955390E8862E4B4FF215ED0AD3C (void);
// 0x0000018F System.Object TMPro.Examples.VertexZoom/z::kt()
extern void z_kt_mBAAF29A87B7ECE0F3A823EA4C1082BDE6BFED817 (void);
// 0x00000190 System.Void TMPro.Examples.VertexZoom/z::ku()
extern void z_ku_m559AFB9D3ABAC30E52DC9EE075927BC6B536AA51 (void);
// 0x00000191 System.Object TMPro.Examples.VertexZoom/z::kv()
extern void z_kv_m9D59C82A870A92C2B250F3329062B656DAE4A761 (void);
// 0x00000192 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_m92842E51B4DBB2E4341ACB179468049FAB23949F (void);
// 0x00000193 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m3339EDC03B6FC498916520CBCCDB5F9FA090F809 (void);
// 0x00000194 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::lc(UnityEngine.AnimationCurve)
extern void WarpTextExample_lc_m9980C264584E561C7F0E2BB79878C602E612CB44 (void);
// 0x00000195 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::ld()
extern void WarpTextExample_ld_mD80F799ECA1C966F78C0D3A6E2414E79CD03A444 (void);
// 0x00000196 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_mBD48A5403123F25C45B5E60C19E1EA397FBA1795 (void);
// 0x00000197 System.Void TMPro.Examples.WarpTextExample/ba::.ctor(System.Int32)
extern void ba__ctor_m43B3192FEB8D2C931FCDE5311C60B535D9FC8B82 (void);
// 0x00000198 System.Void TMPro.Examples.WarpTextExample/ba::ky()
extern void ba_ky_mC7419CBA4AFE0C68BF9E1041932F99B4086A21A9 (void);
// 0x00000199 System.Boolean TMPro.Examples.WarpTextExample/ba::MoveNext()
extern void ba_MoveNext_m6D9CF9564E9982E0D85C00861E618F0EBA623819 (void);
// 0x0000019A System.Object TMPro.Examples.WarpTextExample/ba::kz()
extern void ba_kz_mDF45FD77E9382185D424FD5022864379A39B7A16 (void);
// 0x0000019B System.Void TMPro.Examples.WarpTextExample/ba::la()
extern void ba_la_mD88495D50D34285CCD22472F856EB013FA235C02 (void);
// 0x0000019C System.Object TMPro.Examples.WarpTextExample/ba::lb()
extern void ba_lb_mD96A4DA5435ABAE966673632DA7529D5BDF320BD (void);
// 0x0000019D System.Void _2D_BUNDLE.Wave.Scripts.ColorManager::Awake()
extern void ColorManager_Awake_m490C578D56D7609C20CCD2943A60D8AC6F156EE9 (void);
// 0x0000019E System.Void _2D_BUNDLE.Wave.Scripts.ColorManager::Start()
extern void ColorManager_Start_m82FF9163BEA1C7E31FA273970B288F6A43A585D6 (void);
// 0x0000019F System.Void _2D_BUNDLE.Wave.Scripts.ColorManager::ChangeBackgroundColor()
extern void ColorManager_ChangeBackgroundColor_mD861FCB929F44CE05B95EDAF37D68E2522EF7881 (void);
// 0x000001A0 System.Void _2D_BUNDLE.Wave.Scripts.ColorManager::.ctor()
extern void ColorManager__ctor_m6ADB1782811C33A94CEAB5E159F70799D627C5B9 (void);
// 0x000001A1 System.Void _2D_BUNDLE.Wave.Scripts.DisplayManager::Awake()
extern void DisplayManager_Awake_m01335DCC14EBA25AA446004EB5B4C7B2153A98F1 (void);
// 0x000001A2 System.Void _2D_BUNDLE.Wave.Scripts.DisplayManager::Update()
extern void DisplayManager_Update_m38F8000E01AE1A561B40A5D978D715F25CA5241D (void);
// 0x000001A3 System.Void _2D_BUNDLE.Wave.Scripts.DisplayManager::le()
extern void DisplayManager_le_mD4F3B34D4132B5D2437329955BBB699140FBD113 (void);
// 0x000001A4 System.Single _2D_BUNDLE.Wave.Scripts.DisplayManager::GetWidth()
extern void DisplayManager_GetWidth_mFDFB36DB2D50C3F84F758291C28444533BA014DC (void);
// 0x000001A5 System.Void _2D_BUNDLE.Wave.Scripts.DisplayManager::.ctor()
extern void DisplayManager__ctor_m8205FE124021FDCD885822245CD10692DC944060 (void);
// 0x000001A6 System.Void _2D_BUNDLE.Wave.Scripts.FollowPlayer::Update()
extern void FollowPlayer_Update_mC13522DC9FFB4DE999F76EAB359ED651CC54BBA9 (void);
// 0x000001A7 System.Void _2D_BUNDLE.Wave.Scripts.FollowPlayer::.ctor()
extern void FollowPlayer__ctor_m7B55BE377D37E89EBC21D786A2F4D9FDABFF3B6F (void);
// 0x000001A8 System.Void _2D_BUNDLE.Wave.Scripts.GameManager::Awake()
extern void GameManager_Awake_m32449FAD0ABB6DA999982CBA8D7A04C8DA0041DD (void);
// 0x000001A9 System.Void _2D_BUNDLE.Wave.Scripts.GameManager::Update()
extern void GameManager_Update_m8D69A640252954387F011678EB4083C919AE87E8 (void);
// 0x000001AA System.Collections.IEnumerator _2D_BUNDLE.Wave.Scripts.GameManager::ln()
extern void GameManager_ln_m3C993654A4D7307C5489B96454A5CC4E587B639E (void);
// 0x000001AB System.Void _2D_BUNDLE.Wave.Scripts.GameManager::GameOver()
extern void GameManager_GameOver_m320826E52553065DA089F31CD7DB2EF5F6A3A675 (void);
// 0x000001AC System.Collections.IEnumerator _2D_BUNDLE.Wave.Scripts.GameManager::lo()
extern void GameManager_lo_m66D0168DBD356A43B1470DC203E74C9FA0228B51 (void);
// 0x000001AD System.Void _2D_BUNDLE.Wave.Scripts.GameManager::Restart()
extern void GameManager_Restart_m43130AE3498C335714E4BA04CBB27902BDC5288C (void);
// 0x000001AE System.Void _2D_BUNDLE.Wave.Scripts.GameManager::.ctor()
extern void GameManager__ctor_m472A74B544FE99B623745252A13EE966727FC0EE (void);
// 0x000001AF System.Void _2D_BUNDLE.Wave.Scripts.GameManager/bb::.ctor(System.Int32)
extern void bb__ctor_mB1E8635EBBC07D3515A24237CB05FC954A1408A9 (void);
// 0x000001B0 System.Void _2D_BUNDLE.Wave.Scripts.GameManager/bb::lf()
extern void bb_lf_m9BE286D9CD385F40CCABE91309F493C304B70170 (void);
// 0x000001B1 System.Boolean _2D_BUNDLE.Wave.Scripts.GameManager/bb::MoveNext()
extern void bb_MoveNext_mECD01F3AE07646E747EDA229057EEB3588B8558E (void);
// 0x000001B2 System.Object _2D_BUNDLE.Wave.Scripts.GameManager/bb::lg()
extern void bb_lg_m1C2CE4973FD01AC92D7F7C7A2CCE6CADCBBF1EF7 (void);
// 0x000001B3 System.Void _2D_BUNDLE.Wave.Scripts.GameManager/bb::lh()
extern void bb_lh_m8FF39FD7986A86C983DF3DAA894019B9382BBACD (void);
// 0x000001B4 System.Object _2D_BUNDLE.Wave.Scripts.GameManager/bb::li()
extern void bb_li_m29E49850DA67A0456280E39FA2C44DFE6679BB67 (void);
// 0x000001B5 System.Void _2D_BUNDLE.Wave.Scripts.GameManager/bc::.ctor(System.Int32)
extern void bc__ctor_mA60993DFF132226BC7EBFE9AA08B35D8580CE0CB (void);
// 0x000001B6 System.Void _2D_BUNDLE.Wave.Scripts.GameManager/bc::lj()
extern void bc_lj_m25756378F126F29BA63A9084946AD5E1F4EE407E (void);
// 0x000001B7 System.Boolean _2D_BUNDLE.Wave.Scripts.GameManager/bc::MoveNext()
extern void bc_MoveNext_mF22BFB7660636EADA806B44D145935AC9D15B322 (void);
// 0x000001B8 System.Object _2D_BUNDLE.Wave.Scripts.GameManager/bc::lk()
extern void bc_lk_m1C5BEF5042562F558B17A40A4CBE8A3661551CC4 (void);
// 0x000001B9 System.Void _2D_BUNDLE.Wave.Scripts.GameManager/bc::ll()
extern void bc_ll_m33D7F5BC188A8873DB8AEDB2820A1982ADD926FF (void);
// 0x000001BA System.Object _2D_BUNDLE.Wave.Scripts.GameManager/bc::lm()
extern void bc_lm_m2081FEDCEE7B0B7E7CFCC74FCBB210F0CD031443 (void);
// 0x000001BB System.Void _2D_BUNDLE.Wave.Scripts.ObstacleManager::Start()
extern void ObstacleManager_Start_m28B6D8E2ECC292A0AC602BE50B54121AE4E1A08B (void);
// 0x000001BC System.Void _2D_BUNDLE.Wave.Scripts.ObstacleManager::Update()
extern void ObstacleManager_Update_mFC3133AD340EB570915C8528BA891A7DFD2D2D69 (void);
// 0x000001BD System.Void _2D_BUNDLE.Wave.Scripts.ObstacleManager::lp()
extern void ObstacleManager_lp_m89C27B328008FABA19845A761262210133416ED8 (void);
// 0x000001BE System.Void _2D_BUNDLE.Wave.Scripts.ObstacleManager::.ctor()
extern void ObstacleManager__ctor_mB202BFCFB76004F490942697AEC807CA69870298 (void);
// 0x000001BF System.Void _2D_BUNDLE.Wave.Scripts.ObstacleMovement::Start()
extern void ObstacleMovement_Start_m9611E6A71A98FC2D3BBCF756309F9C120485712D (void);
// 0x000001C0 System.Void _2D_BUNDLE.Wave.Scripts.ObstacleMovement::Update()
extern void ObstacleMovement_Update_mF3BAB0AA886EA89BEDA9048B5B60E3970AC56163 (void);
// 0x000001C1 System.Void _2D_BUNDLE.Wave.Scripts.ObstacleMovement::lq()
extern void ObstacleMovement_lq_mD984FA005E0178B9EA46A3E952C0B990149D3F6B (void);
// 0x000001C2 System.Void _2D_BUNDLE.Wave.Scripts.ObstacleMovement::lr()
extern void ObstacleMovement_lr_m0E8E3A43C9EF51415CE7B5BDC2E714BC459089F0 (void);
// 0x000001C3 System.Void _2D_BUNDLE.Wave.Scripts.ObstacleMovement::ls()
extern void ObstacleMovement_ls_mA62E178DED2CC4764E1B0433D9BE3838814E34F8 (void);
// 0x000001C4 System.Void _2D_BUNDLE.Wave.Scripts.ObstacleMovement::lt()
extern void ObstacleMovement_lt_m84F2BD55B26D87109D0DBF3BE23EA44B5E7732D2 (void);
// 0x000001C5 System.Void _2D_BUNDLE.Wave.Scripts.ObstacleMovement::.ctor()
extern void ObstacleMovement__ctor_m92BCB75AF394EA3037156AAEA8B04EFEC3635CD3 (void);
// 0x000001C6 System.Void _2D_BUNDLE.Wave.Scripts.ObstacleParent::Start()
extern void ObstacleParent_Start_m5C27FE43E8BF0E76E2861049C0AB8D53E4E9FE6F (void);
// 0x000001C7 System.Collections.IEnumerator _2D_BUNDLE.Wave.Scripts.ObstacleParent::ly()
extern void ObstacleParent_ly_m390BEED88D279FC129D8B04AE05C19C5F257C0FA (void);
// 0x000001C8 System.Void _2D_BUNDLE.Wave.Scripts.ObstacleParent::.ctor()
extern void ObstacleParent__ctor_mA16A0298FD0D37693AE408F881E228A9C308D5DB (void);
// 0x000001C9 System.Void _2D_BUNDLE.Wave.Scripts.ObstacleParent/bd::.ctor(System.Int32)
extern void bd__ctor_m71AA4D06CE68DD19AC58539D73D1DE0BE345D520 (void);
// 0x000001CA System.Void _2D_BUNDLE.Wave.Scripts.ObstacleParent/bd::lu()
extern void bd_lu_mABF7DB4E39420E1896D3F1696D050E3084B2FC9C (void);
// 0x000001CB System.Boolean _2D_BUNDLE.Wave.Scripts.ObstacleParent/bd::MoveNext()
extern void bd_MoveNext_m990D5421E67C4A5BE766C5A423ABFD51E18D2D49 (void);
// 0x000001CC System.Object _2D_BUNDLE.Wave.Scripts.ObstacleParent/bd::lv()
extern void bd_lv_mB200571983ABFE34E11F06EAEF861D35CB7AAE38 (void);
// 0x000001CD System.Void _2D_BUNDLE.Wave.Scripts.ObstacleParent/bd::lw()
extern void bd_lw_m146928D89A3131569FCBD659A09BA6DEDA34D92D (void);
// 0x000001CE System.Object _2D_BUNDLE.Wave.Scripts.ObstacleParent/bd::lx()
extern void bd_lx_m128046F4705C53D1384C70697EAB08D2299ECD4B (void);
// 0x000001CF System.Void _2D_BUNDLE.Wave.Scripts.Player::Start()
extern void Player_Start_m3A9BD073012F67CB983646DB5D3E6B443F404F66 (void);
// 0x000001D0 System.Void _2D_BUNDLE.Wave.Scripts.Player::Update()
extern void Player_Update_mFBF7171ED857D469C6B9FCD22E16DF5026281E6E (void);
// 0x000001D1 System.Void _2D_BUNDLE.Wave.Scripts.Player::lz()
extern void Player_lz_m98B366378D735BF4546DF6AACF93478BE1775208 (void);
// 0x000001D2 System.Void _2D_BUNDLE.Wave.Scripts.Player::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Player_OnTriggerEnter2D_m7BDB33E23764DBF62B0A14BABA739CF92EC7D649 (void);
// 0x000001D3 System.Void _2D_BUNDLE.Wave.Scripts.Player::.ctor()
extern void Player__ctor_m30F8A7452908FE4CA77E28B4547937BA4EF65488 (void);
// 0x000001D4 System.Void _2D_BUNDLE.Wave.Scripts.ScoreManager::Awake()
extern void ScoreManager_Awake_mF687A4553FDEBA33B4D91BE2CB825CE7BF565B81 (void);
// 0x000001D5 System.Void _2D_BUNDLE.Wave.Scripts.ScoreManager::AddScore()
extern void ScoreManager_AddScore_m6EEE7869ADA876D48969DC3ADA7B0088EF2989C9 (void);
// 0x000001D6 System.Int32 _2D_BUNDLE.Wave.Scripts.ScoreManager::GetScore()
extern void ScoreManager_GetScore_m5E3AC405C69F2E82D0B74DD5D4CA42B1D16FE097 (void);
// 0x000001D7 System.Void _2D_BUNDLE.Wave.Scripts.ScoreManager::.ctor()
extern void ScoreManager__ctor_m7B86337BDE99A3BBA7173DD7F8F9CE974C803CED (void);
// 0x000001D8 _2D_BUNDLE.WallToWall2022.Scripts.Ball _2D_BUNDLE.WallToWall2022.Scripts.Ball::get_Instance()
extern void Ball_get_Instance_mD92105F1E056FC3B7F20B65762D929C0A57C22F1 (void);
// 0x000001D9 System.Void _2D_BUNDLE.WallToWall2022.Scripts.Ball::set_Instance(_2D_BUNDLE.WallToWall2022.Scripts.Ball)
extern void Ball_set_Instance_mAC56F6291CBF20E017E1F3EA1A389B79DFA9A75B (void);
// 0x000001DA System.Void _2D_BUNDLE.WallToWall2022.Scripts.Ball::Awake()
extern void Ball_Awake_mFC726B55B8E0139DA58C3B11CD8A2575D4CC9DB3 (void);
// 0x000001DB System.Void _2D_BUNDLE.WallToWall2022.Scripts.Ball::Start()
extern void Ball_Start_m2A1E75B0BA54336DC4A6B455412496E1133C5403 (void);
// 0x000001DC System.Void _2D_BUNDLE.WallToWall2022.Scripts.Ball::GameStart()
extern void Ball_GameStart_mE270DF9C9F0697305491443BB1326D1AAF04BBB3 (void);
// 0x000001DD System.Void _2D_BUNDLE.WallToWall2022.Scripts.Ball::Update()
extern void Ball_Update_m9656FD3279C999854982B011466D170DC11BB46B (void);
// 0x000001DE System.Void _2D_BUNDLE.WallToWall2022.Scripts.Ball::ma()
extern void Ball_ma_m76C9BD3918EB6F851E35024FC247D910C1FB81A4 (void);
// 0x000001DF System.Void _2D_BUNDLE.WallToWall2022.Scripts.Ball::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Ball_OnCollisionEnter2D_m2510CFB11D6C4F6CB357CC6DB47C6880975A3B7A (void);
// 0x000001E0 _2D_BUNDLE.WallToWall2022.Scripts.Wall _2D_BUNDLE.WallToWall2022.Scripts.Ball::GetTouchedWall()
extern void Ball_GetTouchedWall_m34DAA030427DF3D4B4BA7B58D0F37F278A68A797 (void);
// 0x000001E1 UnityEngine.Vector3 _2D_BUNDLE.WallToWall2022.Scripts.Ball::GetTouchedPosition()
extern void Ball_GetTouchedPosition_mFCD322CA42C7161AAC2B758DD321F35D05D8AB97 (void);
// 0x000001E2 System.Void _2D_BUNDLE.WallToWall2022.Scripts.Ball::Explode()
extern void Ball_Explode_mA9650D5D9894A6F5DB369C72FC9D472846071405 (void);
// 0x000001E3 System.Void _2D_BUNDLE.WallToWall2022.Scripts.Ball::GameOvered()
extern void Ball_GameOvered_mB1AF35367DC37047865D2661CC67786C41066948 (void);
// 0x000001E4 System.Void _2D_BUNDLE.WallToWall2022.Scripts.Ball::mb(_2D_BUNDLE.WallToWall2022.Scripts.Ball/be)
extern void Ball_mb_m09151F5DE4F583C2B550F7EEA06494FDFB2C7905 (void);
// 0x000001E5 System.Void _2D_BUNDLE.WallToWall2022.Scripts.Ball::.ctor()
extern void Ball__ctor_mEB1C3A6DD57B97CC7BB163FB5C43FFBAEA0A1151 (void);
// 0x000001E6 _2D_BUNDLE.WallToWall2022.Scripts.BallActionManager _2D_BUNDLE.WallToWall2022.Scripts.BallActionManager::get_Instance()
extern void BallActionManager_get_Instance_mDB11C8D2A82542EF8A340609F472460869352E7E (void);
// 0x000001E7 System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallActionManager::set_Instance(_2D_BUNDLE.WallToWall2022.Scripts.BallActionManager)
extern void BallActionManager_set_Instance_m7506DCFE2F4D406BCEA126F68BA094FF4D8E951C (void);
// 0x000001E8 System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallActionManager::Awake()
extern void BallActionManager_Awake_m7F3431C1F9247AED579237A42828D4892CB8D81C (void);
// 0x000001E9 System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallActionManager::InvokeOnBallGenerated()
extern void BallActionManager_InvokeOnBallGenerated_mFD635788FFCEE3136703353B7C281B8A41AAAD58 (void);
// 0x000001EA System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallActionManager::InvokeOnGameStarted()
extern void BallActionManager_InvokeOnGameStarted_m9BCF3C192B1AAB2AA0F73D09CA74EDD25FC4191E (void);
// 0x000001EB System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallActionManager::InvokeOnBallJumped()
extern void BallActionManager_InvokeOnBallJumped_mACA8406A1200867865DFC6C07ADA56336DAB41C3 (void);
// 0x000001EC System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallActionManager::InvokeOnWallTouched()
extern void BallActionManager_InvokeOnWallTouched_mE673D9FCDDAABD935E1F23A46E01F90A1EAE0453 (void);
// 0x000001ED System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallActionManager::InvokeOnObstacleTouched()
extern void BallActionManager_InvokeOnObstacleTouched_mE9A6B28FE1817765E66117E324727D33651CEBB8 (void);
// 0x000001EE System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallActionManager::InvokeOnBallExploded()
extern void BallActionManager_InvokeOnBallExploded_m43A75AD7693C255351C6510060A7B38F59496202 (void);
// 0x000001EF System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallActionManager::.ctor()
extern void BallActionManager__ctor_m82F84496F0036FA4C8D611DBD668F1A717D015DB (void);
// 0x000001F0 System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallEffectHandler::Awake()
extern void BallEffectHandler_Awake_m9D6ADB7AFD76EE8F02C23DC390925BA816EF1C16 (void);
// 0x000001F1 System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallEffectHandler::Start()
extern void BallEffectHandler_Start_m666BEA086375F51680806A889D646B343418D649 (void);
// 0x000001F2 System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallEffectHandler::mf()
extern void BallEffectHandler_mf_m6A594780A2A6FE8284AA24C2E4FBBE9E2296C764 (void);
// 0x000001F3 System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallEffectHandler::mg()
extern void BallEffectHandler_mg_m3983B59E63E4C487F9A78BEACA1B3FBE6CAF86AF (void);
// 0x000001F4 System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallEffectHandler::mh()
extern void BallEffectHandler_mh_m8F300747E53A4087A0F2DD625BC9C016505AEF23 (void);
// 0x000001F5 System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallEffectHandler::mi()
extern void BallEffectHandler_mi_m528D1314DDC168C4BA4CBA66F42F325792A152BE (void);
// 0x000001F6 System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallEffectHandler::mj()
extern void BallEffectHandler_mj_m46D76D6ACEC62FD3719EEC119BCF4FA47133E84F (void);
// 0x000001F7 System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallEffectHandler::.ctor()
extern void BallEffectHandler__ctor_mC3C9EA0247FEA0FC6AC8F705D7141B6503926B67 (void);
// 0x000001F8 System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallEffectHandler::mk()
extern void BallEffectHandler_mk_m0736365A0528B53B5AE2B31D909CB23FD517CC08 (void);
// 0x000001F9 System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallEffectHandler/<>c::.cctor()
extern void U3CU3Ec__cctor_mFB02249ED3D5BCFDDF61266DE592E26BD70B7D3B (void);
// 0x000001FA System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallEffectHandler/<>c::.ctor()
extern void U3CU3Ec__ctor_mB2E95812D5F6FAB8235996AD939736B806C8F609 (void);
// 0x000001FB System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallEffectHandler/<>c::mc()
extern void U3CU3Ec_mc_mE63F9EEFEFCD1A04386D4818671B11ED606804D1 (void);
// 0x000001FC System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallEffectHandler/<>c::md()
extern void U3CU3Ec_md_mE26B6F6851CC8A1EA551092E7FC9521BB6AD1F01 (void);
// 0x000001FD System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallEffectHandler/<>c::me()
extern void U3CU3Ec_me_m3BA94B40664F12E722E6DAA5B144B1932553D690 (void);
// 0x000001FE System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallGenerator::Start()
extern void BallGenerator_Start_m4D0E8D6629C1459FE76EA89F417ED27A61DAFC64 (void);
// 0x000001FF System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallGenerator::ml()
extern void BallGenerator_ml_m4DF900D0A8270D97CDD3EA4419C4A54B4CDB6C69 (void);
// 0x00000200 System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallGenerator::mm()
extern void BallGenerator_mm_m521F4558E05CC599F30D7A39D82E81DFBA815C62 (void);
// 0x00000201 System.Void _2D_BUNDLE.WallToWall2022.Scripts.BallGenerator::.ctor()
extern void BallGenerator__ctor_mE3FDB6F5B4010A1D5E6D58D16BC5F74F85CEABAB (void);
// 0x00000202 System.Void _2D_BUNDLE.WallToWall2022.Scripts.CameraManager::Start()
extern void CameraManager_Start_mE3C7B981C2ECAEC32EBC46817F7375F153435549 (void);
// 0x00000203 System.Void _2D_BUNDLE.WallToWall2022.Scripts.CameraManager::mn()
extern void CameraManager_mn_m38EA7ED144940E8291C90FC1973D987C743147E3 (void);
// 0x00000204 System.Void _2D_BUNDLE.WallToWall2022.Scripts.CameraManager::mo()
extern void CameraManager_mo_m5D27F1163CF9CDD0C0138438A8F71CB87F24B98D (void);
// 0x00000205 System.Void _2D_BUNDLE.WallToWall2022.Scripts.CameraManager::mp()
extern void CameraManager_mp_mB52E32D13B7AEE28C9B4DDF61105A8CDC8466E79 (void);
// 0x00000206 System.Void _2D_BUNDLE.WallToWall2022.Scripts.CameraManager::mq()
extern void CameraManager_mq_m0EA5C2B203DE930932772D87EB6FEC7EE71A90EB (void);
// 0x00000207 System.Void _2D_BUNDLE.WallToWall2022.Scripts.CameraManager::mr(System.Single,System.Single,System.Int32)
extern void CameraManager_mr_mB15D52D48EDF2E1674565FD652578F830D2CF4B7 (void);
// 0x00000208 System.Void _2D_BUNDLE.WallToWall2022.Scripts.CameraManager::.ctor()
extern void CameraManager__ctor_mF986325A67AC4558185B7E42AE5486BC61C17BFC (void);
// 0x00000209 System.Void _2D_BUNDLE.WallToWall2022.Scripts.DotTrail::Start()
extern void DotTrail_Start_m55D572C4717725B2CAE8C296718EA9E153CE5A8E (void);
// 0x0000020A System.Collections.IEnumerator _2D_BUNDLE.WallToWall2022.Scripts.DotTrail::DrawDotCoroutine()
extern void DotTrail_DrawDotCoroutine_m544B2810E3226BB8935B11F1FCBA78CD39D27D3B (void);
// 0x0000020B System.Void _2D_BUNDLE.WallToWall2022.Scripts.DotTrail::.ctor()
extern void DotTrail__ctor_m1FB42F428423DF42C7EBF53B35FCD9E84B2D5922 (void);
// 0x0000020C System.Void _2D_BUNDLE.WallToWall2022.Scripts.DotTrail/bf::.ctor(System.Int32)
extern void bf__ctor_m349CE7E3614F5FA57513DD4A3561B0A6CE2C5E63 (void);
// 0x0000020D System.Void _2D_BUNDLE.WallToWall2022.Scripts.DotTrail/bf::ms()
extern void bf_ms_mB54709CE446F9624074C1164DCB77B6F1BD06F30 (void);
// 0x0000020E System.Boolean _2D_BUNDLE.WallToWall2022.Scripts.DotTrail/bf::MoveNext()
extern void bf_MoveNext_m884B4BE97B521830DD92A994B8C896557347214E (void);
// 0x0000020F System.Object _2D_BUNDLE.WallToWall2022.Scripts.DotTrail/bf::mt()
extern void bf_mt_m8CEB9CCA687183C3D5896593342A6ECB7C652A10 (void);
// 0x00000210 System.Void _2D_BUNDLE.WallToWall2022.Scripts.DotTrail/bf::mu()
extern void bf_mu_mB416CD1EBB37F423B33DF5140C0E058A3FD7B0E7 (void);
// 0x00000211 System.Object _2D_BUNDLE.WallToWall2022.Scripts.DotTrail/bf::mv()
extern void bf_mv_m8104D0CF4D48FA5A8AAFE77192E0EF7CC186288E (void);
// 0x00000212 System.Void _2D_BUNDLE.WallToWall2022.Scripts.DotweenManager::Awake()
extern void DotweenManager_Awake_m44DCDF768CB7B8A3BAFF2605F2801F2C18949F6C (void);
// 0x00000213 System.Void _2D_BUNDLE.WallToWall2022.Scripts.DotweenManager::.ctor()
extern void DotweenManager__ctor_m9CAC0DD928993F9B87C0E6C2ABF0A4EEE05CDC17 (void);
// 0x00000214 System.Void _2D_BUNDLE.WallToWall2022.Scripts.GameOverUI::Awake()
extern void GameOverUI_Awake_m9644D0CEA1F1B611B47B31B4A08912CAAA829D9C (void);
// 0x00000215 System.Void _2D_BUNDLE.WallToWall2022.Scripts.GameOverUI::Start()
extern void GameOverUI_Start_mE7A914C4B9F5A50781799E81F28806B7A0B4176C (void);
// 0x00000216 System.Void _2D_BUNDLE.WallToWall2022.Scripts.GameOverUI::mx()
extern void GameOverUI_mx_m8A43E8B423DC3388576B26E1FBA3E76F612EE0E8 (void);
// 0x00000217 System.Void _2D_BUNDLE.WallToWall2022.Scripts.GameOverUI::my()
extern void GameOverUI_my_m6BD2F242B88BE8A351EFC2C82EE9A7A9D94D404C (void);
// 0x00000218 System.Void _2D_BUNDLE.WallToWall2022.Scripts.GameOverUI::mz()
extern void GameOverUI_mz_mBD7B2437CA14A10DDF3B2FD4D067BB4AB07D0958 (void);
// 0x00000219 System.Void _2D_BUNDLE.WallToWall2022.Scripts.GameOverUI::na()
extern void GameOverUI_na_m737E01799FA40DD9125E5114DCBC4F93D78F23B6 (void);
// 0x0000021A System.Void _2D_BUNDLE.WallToWall2022.Scripts.GameOverUI::nb()
extern void GameOverUI_nb_m84A40F7F30F79BB06926B58CDC54303A00CCB4C7 (void);
// 0x0000021B System.Void _2D_BUNDLE.WallToWall2022.Scripts.GameOverUI::.ctor()
extern void GameOverUI__ctor_mBA2B6F5970DFB528F06736AB53AD6FACBA780B0A (void);
// 0x0000021C System.Void _2D_BUNDLE.WallToWall2022.Scripts.GameOverUI::nc()
extern void GameOverUI_nc_m025916B01AB09B8B5FFE231832699E69027663B5 (void);
// 0x0000021D System.Void _2D_BUNDLE.WallToWall2022.Scripts.GameOverUI/<>c::.cctor()
extern void U3CU3Ec__cctor_m957872919AEDB94B7137E643842F5D85495E2AC2 (void);
// 0x0000021E System.Void _2D_BUNDLE.WallToWall2022.Scripts.GameOverUI/<>c::.ctor()
extern void U3CU3Ec__ctor_m0EB4E928E0EC569A04C92BB3365720565E268E45 (void);
// 0x0000021F System.Void _2D_BUNDLE.WallToWall2022.Scripts.GameOverUI/<>c::mw()
extern void U3CU3Ec_mw_mCB107D3D055850E96765C27CC85980F683C3FE87 (void);
// 0x00000220 _2D_BUNDLE.WallToWall2022.Scripts.GamePlayManager _2D_BUNDLE.WallToWall2022.Scripts.GamePlayManager::get_Instance()
extern void GamePlayManager_get_Instance_m1D2C2FBAA8D163D399560A7E4C02BEC2D7B3DDCA (void);
// 0x00000221 System.Void _2D_BUNDLE.WallToWall2022.Scripts.GamePlayManager::set_Instance(_2D_BUNDLE.WallToWall2022.Scripts.GamePlayManager)
extern void GamePlayManager_set_Instance_mCB6EF46D3FCEEDE2ACE0F6F7C63A0C9A19E91EC1 (void);
// 0x00000222 System.Void _2D_BUNDLE.WallToWall2022.Scripts.GamePlayManager::Awake()
extern void GamePlayManager_Awake_mD3F9A4345E4D32F6E2564E46CA667C78F57E1E68 (void);
// 0x00000223 System.Void _2D_BUNDLE.WallToWall2022.Scripts.GamePlayManager::GameOvered()
extern void GamePlayManager_GameOvered_m78B86ADFF3CBE4423E12CE78E5E801F931280FEC (void);
// 0x00000224 System.Void _2D_BUNDLE.WallToWall2022.Scripts.GamePlayManager::ReloadScene()
extern void GamePlayManager_ReloadScene_mF0D1D74D474F9912FDA753B38A36D0EE3971626C (void);
// 0x00000225 System.Void _2D_BUNDLE.WallToWall2022.Scripts.GamePlayManager::.ctor()
extern void GamePlayManager__ctor_m5ED88D4891E7CC9D09141C12AAF96B8ED632E654 (void);
// 0x00000226 System.Void _2D_BUNDLE.WallToWall2022.Scripts.Obstacle::Awake()
extern void Obstacle_Awake_mBC3DC059E6052B4F7027F828AB2E754BA17853E1 (void);
// 0x00000227 System.Void _2D_BUNDLE.WallToWall2022.Scripts.Obstacle::Start()
extern void Obstacle_Start_mE8FDA3F73109552FB8E8595E37A775E3A37174D7 (void);
// 0x00000228 System.Void _2D_BUNDLE.WallToWall2022.Scripts.Obstacle::nd()
extern void Obstacle_nd_mDDEDA7807649807425A4437B7E8216421F297D5B (void);
// 0x00000229 System.Void _2D_BUNDLE.WallToWall2022.Scripts.Obstacle::DestroyEffect()
extern void Obstacle_DestroyEffect_m26C8810D7094A2D183939896E9E497F6D5EF0CF7 (void);
// 0x0000022A System.Void _2D_BUNDLE.WallToWall2022.Scripts.Obstacle::.ctor()
extern void Obstacle__ctor_mDE5156FB11CFEF62E978EBD7EEEF7D903D40ACAE (void);
// 0x0000022B System.Void _2D_BUNDLE.WallToWall2022.Scripts.Obstacle::ne()
extern void Obstacle_ne_m7FB70B9BC4F12B5F86C10FB1F0368A58CE70C625 (void);
// 0x0000022C System.Void _2D_BUNDLE.WallToWall2022.Scripts.Obstacle::nf()
extern void Obstacle_nf_mCC6059D64313DDA3F6262147F8F2CDD074FFC8D8 (void);
// 0x0000022D System.Void _2D_BUNDLE.WallToWall2022.Scripts.Obstacle::ng()
extern void Obstacle_ng_m18933E23A4200119D32514F06FC9982DB0C11021 (void);
// 0x0000022E System.Void _2D_BUNDLE.WallToWall2022.Scripts.ObstacleManager::Awake()
extern void ObstacleManager_Awake_mA421F3B2E1A8F7EA32AD72D5FE43B6BFA84201B5 (void);
// 0x0000022F System.Void _2D_BUNDLE.WallToWall2022.Scripts.ObstacleManager::Start()
extern void ObstacleManager_Start_m531DF614ADEE8A3482AE65A428BDD74200B070F0 (void);
// 0x00000230 System.Collections.IEnumerator _2D_BUNDLE.WallToWall2022.Scripts.ObstacleManager::nl()
extern void ObstacleManager_nl_m74E35D251F47A3363DE2A15FD1C64A0A51FDAE3E (void);
// 0x00000231 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ObstacleManager::HandleObstacle()
extern void ObstacleManager_HandleObstacle_m0AB1DC50944BACDBABE6E452A706D2205CE4915D (void);
// 0x00000232 System.Boolean _2D_BUNDLE.WallToWall2022.Scripts.ObstacleManager::nm()
extern void ObstacleManager_nm_mE42B5BDD379BD78EA9CBEAC1A8310430CAFAADD3 (void);
// 0x00000233 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ObstacleManager::nn()
extern void ObstacleManager_nn_m5278904F2220976CEDBBFDA096DA06ED68312A90 (void);
// 0x00000234 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ObstacleManager::.ctor()
extern void ObstacleManager__ctor_m46A423647AD9739027087D308E50933D86C5706D (void);
// 0x00000235 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ObstacleManager/bg::.ctor(System.Int32)
extern void bg__ctor_mDBB0D96119111A43E6685B4C8EB9C94A9F6010AB (void);
// 0x00000236 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ObstacleManager/bg::nh()
extern void bg_nh_m5BC99CFE0E0B8109DA7FEC46FD0EA107B644558F (void);
// 0x00000237 System.Boolean _2D_BUNDLE.WallToWall2022.Scripts.ObstacleManager/bg::MoveNext()
extern void bg_MoveNext_mDDEBC2220E0474B68B2BEBFF7E7479E464D9EC9F (void);
// 0x00000238 System.Object _2D_BUNDLE.WallToWall2022.Scripts.ObstacleManager/bg::ni()
extern void bg_ni_m8ED85DECDEE96D50215CF3881F585CEA1689FF68 (void);
// 0x00000239 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ObstacleManager/bg::nj()
extern void bg_nj_m4CCA6210902BFC13056E12DC260BB56709E23180 (void);
// 0x0000023A System.Object _2D_BUNDLE.WallToWall2022.Scripts.ObstacleManager/bg::nk()
extern void bg_nk_m6CD04DCDFC8E4E686D32F14A43D731F92869E258 (void);
// 0x0000023B System.Void _2D_BUNDLE.WallToWall2022.Scripts.PfChild::OnDestroy()
extern void PfChild_OnDestroy_m99C8F37D50048E540FD6580D9068DA745EA7E69A (void);
// 0x0000023C System.Void _2D_BUNDLE.WallToWall2022.Scripts.PfChild::.ctor()
extern void PfChild__ctor_mF6872A54317D9C1D93552A6DB0F341DA179A19D7 (void);
// 0x0000023D System.Void _2D_BUNDLE.WallToWall2022.Scripts.PfParent::Awake()
extern void PfParent_Awake_mE124BF81CB632C32835522F5E32478917FFF6AF4 (void);
// 0x0000023E System.Void _2D_BUNDLE.WallToWall2022.Scripts.PfParent::no()
extern void PfParent_no_m2DDF90A4A2CC0D7AC7F7B9114DC3E3EF3A806091 (void);
// 0x0000023F System.Void _2D_BUNDLE.WallToWall2022.Scripts.PfParent::.ctor()
extern void PfParent__ctor_m3925D790924CBFD0669709D0B5E3BC7E6BECFDDD (void);
// 0x00000240 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScaleAndColorAnimation::Start()
extern void ScaleAndColorAnimation_Start_mE8E6963B7CCEBFCAF410EB594A25D2DC53DB7043 (void);
// 0x00000241 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScaleAndColorAnimation::np()
extern void ScaleAndColorAnimation_np_m9810C8612C51B35B145A32193902D6B2EDDDB8E7 (void);
// 0x00000242 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScaleAndColorAnimation::nq()
extern void ScaleAndColorAnimation_nq_mC620E13DFD5DFCA714A83E69EB8F702CBA11E4AB (void);
// 0x00000243 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScaleAndColorAnimation::nr()
extern void ScaleAndColorAnimation_nr_mC449E24A8EE2F4C53288AA03FF2FFA911CB36106 (void);
// 0x00000244 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScaleAndColorAnimation::ns()
extern void ScaleAndColorAnimation_ns_mA08A65E2711D0DE67609A16BA155E1F4E3B6764B (void);
// 0x00000245 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScaleAndColorAnimation::.ctor()
extern void ScaleAndColorAnimation__ctor_mE3C7F0F51CAC02E0708511C6EA3F343F56A02BD0 (void);
// 0x00000246 _2D_BUNDLE.WallToWall2022.Scripts.ScoreManager _2D_BUNDLE.WallToWall2022.Scripts.ScoreManager::get_Instance()
extern void ScoreManager_get_Instance_m71D27F07DC8C1B968B5E6693C613DDA2C6404E91 (void);
// 0x00000247 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScoreManager::set_Instance(_2D_BUNDLE.WallToWall2022.Scripts.ScoreManager)
extern void ScoreManager_set_Instance_mA213E6025FD45474513E29E85DCC50DA1BBBACC7 (void);
// 0x00000248 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScoreManager::Awake()
extern void ScoreManager_Awake_m4CCB8A46340B283F5F89B856F1C48D0EE74A5C93 (void);
// 0x00000249 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScoreManager::Start()
extern void ScoreManager_Start_m4DDFF58DEE0C9B88A74A40BA9DF3678ADBAA08E8 (void);
// 0x0000024A System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScoreManager::nt()
extern void ScoreManager_nt_mA6F166ABA84074B04C59C57B656F4E6F64CCAC27 (void);
// 0x0000024B System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScoreManager::nu(System.Int32)
extern void ScoreManager_nu_m6557D8B597DEAF758C0665844FD8FB73C0683C8F (void);
// 0x0000024C System.Int32 _2D_BUNDLE.WallToWall2022.Scripts.ScoreManager::GetCurrentScore()
extern void ScoreManager_GetCurrentScore_mC0072C9B88C4AD3A009917A64049018D664F3F23 (void);
// 0x0000024D System.Int32 _2D_BUNDLE.WallToWall2022.Scripts.ScoreManager::GetBestScore()
extern void ScoreManager_GetBestScore_m00DB771C7947EE52D9B49AE9449DC98D26D0032D (void);
// 0x0000024E System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScoreManager::.ctor()
extern void ScoreManager__ctor_m630A24D46A8768B03FEBEFD0660CB8EDF2FE8F24 (void);
// 0x0000024F System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScoreUI::Awake()
extern void ScoreUI_Awake_m6F635FE2CB2C6D3DECADB7C0F454D64CEA240773 (void);
// 0x00000250 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScoreUI::Start()
extern void ScoreUI_Start_mD1A15F0E34EA17C78D473C4B3E7BBDC7BC0B99C0 (void);
// 0x00000251 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScoreUI::nv()
extern void ScoreUI_nv_mF1653E6027706D3C88CAFD79C1B6556EDCF5EE35 (void);
// 0x00000252 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScoreUI::nw()
extern void ScoreUI_nw_m6F07A36F465F8F5D805F76F968EC7C33CD1BCD61 (void);
// 0x00000253 System.Void _2D_BUNDLE.WallToWall2022.Scripts.ScoreUI::.ctor()
extern void ScoreUI__ctor_m638664F853E1789D0FE675010D92B9B07EC13162 (void);
// 0x00000254 System.Void _2D_BUNDLE.WallToWall2022.Scripts.SoundManager::Awake()
extern void SoundManager_Awake_m386D90E2CF2E6F9491AE780AF40587EC52081069 (void);
// 0x00000255 System.Void _2D_BUNDLE.WallToWall2022.Scripts.SoundManager::Start()
extern void SoundManager_Start_mA7FC8807A2DE6A6E5882E679598D55AB49734FB5 (void);
// 0x00000256 System.Void _2D_BUNDLE.WallToWall2022.Scripts.SoundManager::nx()
extern void SoundManager_nx_mB9DA61616B3B32AB6D166F53227A59AFD174C8F9 (void);
// 0x00000257 System.Void _2D_BUNDLE.WallToWall2022.Scripts.SoundManager::ny()
extern void SoundManager_ny_m240020CB508F3FFD3C61326F86EAF86A25040D28 (void);
// 0x00000258 System.Void _2D_BUNDLE.WallToWall2022.Scripts.SoundManager::nz()
extern void SoundManager_nz_mAD4FFB6047E654D7D1FE25EB3D08E14BD64DDF58 (void);
// 0x00000259 System.Void _2D_BUNDLE.WallToWall2022.Scripts.SoundManager::oa()
extern void SoundManager_oa_m2AB0F05FF33608592A6A0DBD453368935142FAE6 (void);
// 0x0000025A System.Void _2D_BUNDLE.WallToWall2022.Scripts.SoundManager::ob()
extern void SoundManager_ob_m3D2AC42D450673AB09CF3D236A6383C723AC0660 (void);
// 0x0000025B System.Void _2D_BUNDLE.WallToWall2022.Scripts.SoundManager::oc()
extern void SoundManager_oc_mB84CBCF195F8952B01B49B811C2FC696B181FD9F (void);
// 0x0000025C System.Void _2D_BUNDLE.WallToWall2022.Scripts.SoundManager::.ctor()
extern void SoundManager__ctor_mB0D0985174F015B1122FB408CE812FAAC077AFEC (void);
// 0x0000025D System.Void _2D_BUNDLE.WallToWall2022.Scripts.Wall::Start()
extern void Wall_Start_mECF5392AED0A71D948EC01BBCDA06676C909B6CC (void);
// 0x0000025E System.Void _2D_BUNDLE.WallToWall2022.Scripts.Wall::od()
extern void Wall_od_mFFC4213EA96BC1B0734722A59E4C6EE748EE7CD0 (void);
// 0x0000025F System.Void _2D_BUNDLE.WallToWall2022.Scripts.Wall::.ctor()
extern void Wall__ctor_mB23FA331F047DABDB49861A28ED0D4C5AD6B14B5 (void);
// 0x00000260 System.Void _2D_BUNDLE.WallToWall2022.Scripts.WallTypeHolder::.ctor()
extern void WallTypeHolder__ctor_mA4EB967F9FA4F3FEDD2F2E25005B46ED7DE3104D (void);
// 0x00000261 System.Void _2D_BUNDLE.LeftOrRight.Scripts.ActionManager::.ctor()
extern void ActionManager__ctor_mAA3CDA382E507836841588422E0CB2942D2EED08 (void);
// 0x00000262 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Afterimage::Start()
extern void Afterimage_Start_m477A718F52E90CF365F8EDF1223FDECDF5B1D1BD (void);
// 0x00000263 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Afterimage::OnDestroy()
extern void Afterimage_OnDestroy_mCC01B33F85F857E293951D2C63C3246765A3CC67 (void);
// 0x00000264 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Afterimage::Update()
extern void Afterimage_Update_m2C00C518F769F227C1D6FAE8EC9D032E7409DEE0 (void);
// 0x00000265 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Afterimage::Move()
extern void Afterimage_Move_mBCDC9C3866E9CB1C7BFA32FCC9E9B501AFB1C938 (void);
// 0x00000266 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Afterimage::oe()
extern void Afterimage_oe_m540909EA78A3CD75803D76ACCCEA8CFBD43F7100 (void);
// 0x00000267 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Afterimage::of()
extern void Afterimage_of_m53430D821C87FED4C74D95A319EDAF3991833BFB (void);
// 0x00000268 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Afterimage::og()
extern void Afterimage_og_m7E08243082EA0B78108D39173A4432EE9DB91ABD (void);
// 0x00000269 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Afterimage::.ctor()
extern void Afterimage__ctor_m1F77CA1732919E245A5C071F0CBE7D444A00A9BC (void);
// 0x0000026A System.Void _2D_BUNDLE.LeftOrRight.Scripts.AudioManager::Start()
extern void AudioManager_Start_mDCCAB3FA2C9B2E7A57DABAFEAD5E9CC45A97F3AA (void);
// 0x0000026B System.Void _2D_BUNDLE.LeftOrRight.Scripts.AudioManager::OnDestroy()
extern void AudioManager_OnDestroy_mFB6FC90B2A0EE069E851D92B5A9FB9A5C99348ED (void);
// 0x0000026C System.Void _2D_BUNDLE.LeftOrRight.Scripts.AudioManager::oh()
extern void AudioManager_oh_m6CDF1821553EAB1657DE9453F297217917D4E693 (void);
// 0x0000026D System.Void _2D_BUNDLE.LeftOrRight.Scripts.AudioManager::oi(System.Int32)
extern void AudioManager_oi_mD64804F5BAF49FE5900E167688ACF7F9A5AF8651 (void);
// 0x0000026E System.Void _2D_BUNDLE.LeftOrRight.Scripts.AudioManager::oj()
extern void AudioManager_oj_m33CDB773938FE7086F38AF5983C6130B4EB333C9 (void);
// 0x0000026F System.Void _2D_BUNDLE.LeftOrRight.Scripts.AudioManager::ok()
extern void AudioManager_ok_m1691041983F8EA0C7FEC26AAAF026CFF26CBD491 (void);
// 0x00000270 System.Void _2D_BUNDLE.LeftOrRight.Scripts.AudioManager::.ctor()
extern void AudioManager__ctor_mADBE84ED80DA8399E415ADD39EF226B77230B2CB (void);
// 0x00000271 System.Void _2D_BUNDLE.LeftOrRight.Scripts.CameraManager::Start()
extern void CameraManager_Start_m92B55335EEAB451EE6F3A39CC9FBD559380D11C5 (void);
// 0x00000272 System.Void _2D_BUNDLE.LeftOrRight.Scripts.CameraManager::OnDestroy()
extern void CameraManager_OnDestroy_mFDFC3F8CF8FD7E949474D53EDD6839F97346FD17 (void);
// 0x00000273 System.Void _2D_BUNDLE.LeftOrRight.Scripts.CameraManager::Update()
extern void CameraManager_Update_m2C9527CB46AC4B266C9CC9BF618577FE9E7912CA (void);
// 0x00000274 System.Collections.IEnumerator _2D_BUNDLE.LeftOrRight.Scripts.CameraManager::ot()
extern void CameraManager_ot_m40596E993F43A35386C42AAA1362BAD55123905C (void);
// 0x00000275 System.Void _2D_BUNDLE.LeftOrRight.Scripts.CameraManager::ou()
extern void CameraManager_ou_mC39888BCFFD759432FB3620F98F052E2E03A2E95 (void);
// 0x00000276 System.Void _2D_BUNDLE.LeftOrRight.Scripts.CameraManager::ov()
extern void CameraManager_ov_m24C7884C5093BB61A68D3D825121B6D413B7DFE3 (void);
// 0x00000277 System.Collections.IEnumerator _2D_BUNDLE.LeftOrRight.Scripts.CameraManager::ow()
extern void CameraManager_ow_mB394BAF576FE81B7F3AF4EBDE02B4AC1117AEFCF (void);
// 0x00000278 System.Void _2D_BUNDLE.LeftOrRight.Scripts.CameraManager::.ctor()
extern void CameraManager__ctor_m4D352DFED4530FFC5FE1067A1B1731A4C042DF09 (void);
// 0x00000279 System.Void _2D_BUNDLE.LeftOrRight.Scripts.CameraManager/bh::.ctor(System.Int32)
extern void bh__ctor_mF5A530549DA4DBFE109CB023242738538AF997CF (void);
// 0x0000027A System.Void _2D_BUNDLE.LeftOrRight.Scripts.CameraManager/bh::ol()
extern void bh_ol_m03609B9F398488D2CF5D34FAB10459D1DB9038B4 (void);
// 0x0000027B System.Boolean _2D_BUNDLE.LeftOrRight.Scripts.CameraManager/bh::MoveNext()
extern void bh_MoveNext_m98C5B1562DBF34E69B483C6775FD0126372549A9 (void);
// 0x0000027C System.Object _2D_BUNDLE.LeftOrRight.Scripts.CameraManager/bh::om()
extern void bh_om_m5EEC1EFC9EDF030476525ABD3B16530DA29B5B51 (void);
// 0x0000027D System.Void _2D_BUNDLE.LeftOrRight.Scripts.CameraManager/bh::on()
extern void bh_on_m8C968DCD664D43ECA2613490DCE59E7C8B46E8D6 (void);
// 0x0000027E System.Object _2D_BUNDLE.LeftOrRight.Scripts.CameraManager/bh::oo()
extern void bh_oo_mC3A540EA53FB26AD9AE80F8FB1A69D656866B7BE (void);
// 0x0000027F System.Void _2D_BUNDLE.LeftOrRight.Scripts.CameraManager/bi::.ctor(System.Int32)
extern void bi__ctor_mB1C65277A84566A682F07829B202305EB3DDACD6 (void);
// 0x00000280 System.Void _2D_BUNDLE.LeftOrRight.Scripts.CameraManager/bi::op()
extern void bi_op_m18D32C3B194528D928191242DE29A514C066B4CF (void);
// 0x00000281 System.Boolean _2D_BUNDLE.LeftOrRight.Scripts.CameraManager/bi::MoveNext()
extern void bi_MoveNext_m66AC1F59E57B68785EC60F490FAEAC7EF1B9C6AA (void);
// 0x00000282 System.Object _2D_BUNDLE.LeftOrRight.Scripts.CameraManager/bi::oq()
extern void bi_oq_m8FFA01AC6DCE92F6FE34C849A1493DB8FE7D6A2E (void);
// 0x00000283 System.Void _2D_BUNDLE.LeftOrRight.Scripts.CameraManager/bi::or()
extern void bi_or_m42A1D41CB80E5DB77119B66632099F5E36B1A1D8 (void);
// 0x00000284 System.Object _2D_BUNDLE.LeftOrRight.Scripts.CameraManager/bi::os()
extern void bi_os_m3F7A853E3148AA1B5738643ACF5F90D8D9D24B66 (void);
// 0x00000285 System.Void _2D_BUNDLE.LeftOrRight.Scripts.ColorManager::Start()
extern void ColorManager_Start_m1616D8A28EDF782D2636E305E67413F7CBB016E7 (void);
// 0x00000286 System.Void _2D_BUNDLE.LeftOrRight.Scripts.ColorManager::OnDestroy()
extern void ColorManager_OnDestroy_m375D79347704FA091D6ABE01E7E412024B433040 (void);
// 0x00000287 System.Void _2D_BUNDLE.LeftOrRight.Scripts.ColorManager::ox()
extern void ColorManager_ox_m8DD08C77B8446A4C7E9F11E8F035B7635035DE29 (void);
// 0x00000288 System.Void _2D_BUNDLE.LeftOrRight.Scripts.ColorManager::oy()
extern void ColorManager_oy_m61D6A7E9D65ADF07F28BAB97DF0E56080B4E8AE6 (void);
// 0x00000289 System.Void _2D_BUNDLE.LeftOrRight.Scripts.ColorManager::.ctor()
extern void ColorManager__ctor_mE3078F3B8DA2C08E63DA49591B6F2AB7343F459B (void);
// 0x0000028A System.Void _2D_BUNDLE.LeftOrRight.Scripts.CountdownManager::Start()
extern void CountdownManager_Start_m55393FF46FB7849059CA39D1ECC9A48C85D8D4C4 (void);
// 0x0000028B System.Void _2D_BUNDLE.LeftOrRight.Scripts.CountdownManager::OnDestroy()
extern void CountdownManager_OnDestroy_m888B2F72DC8E42A58979244AC9D5F816B5BEF552 (void);
// 0x0000028C System.Void _2D_BUNDLE.LeftOrRight.Scripts.CountdownManager::Update()
extern void CountdownManager_Update_m8DDD6D347634BCC7801E3E14AFFF04FFB4EAF354 (void);
// 0x0000028D System.Void _2D_BUNDLE.LeftOrRight.Scripts.CountdownManager::oz()
extern void CountdownManager_oz_mAC279F9F74FF7BD576CE1E3287689E85267D93EA (void);
// 0x0000028E System.Single _2D_BUNDLE.LeftOrRight.Scripts.CountdownManager::GetTime()
extern void CountdownManager_GetTime_m51FEB44BFAE8084D786D3CC238F6AF6D79CD0850 (void);
// 0x0000028F System.Void _2D_BUNDLE.LeftOrRight.Scripts.CountdownManager::pa(System.Int32)
extern void CountdownManager_pa_mD5A47CBD44BCF7A14FF842F3E4E319897F14B31A (void);
// 0x00000290 System.Void _2D_BUNDLE.LeftOrRight.Scripts.CountdownManager::pb()
extern void CountdownManager_pb_mFD42A6C68B8A287EE8A26849EA3449200223CBAA (void);
// 0x00000291 System.Void _2D_BUNDLE.LeftOrRight.Scripts.CountdownManager::.ctor()
extern void CountdownManager__ctor_mE823D6C1C1BDA4914FF22CEDC98FB2C232D713BA (void);
// 0x00000292 System.Collections.IEnumerator _2D_BUNDLE.LeftOrRight.Scripts.GameManager::Start()
extern void GameManager_Start_m355B83ED1631702B30CC64B8768982A2D258F68C (void);
// 0x00000293 System.Void _2D_BUNDLE.LeftOrRight.Scripts.GameManager::Restart()
extern void GameManager_Restart_m541C04252BEF29F324EC18D0F114F0C1D6347314 (void);
// 0x00000294 System.Void _2D_BUNDLE.LeftOrRight.Scripts.GameManager::.ctor()
extern void GameManager__ctor_m58F6BC63E1952578CEB1531BB04BBD1BACCC3F1C (void);
// 0x00000295 System.Void _2D_BUNDLE.LeftOrRight.Scripts.GameManager/bj::.ctor(System.Int32)
extern void bj__ctor_mCD393AE7FB3AC826644FED79DE7CF47B8DC85EA6 (void);
// 0x00000296 System.Void _2D_BUNDLE.LeftOrRight.Scripts.GameManager/bj::pc()
extern void bj_pc_m7AA06DB318893155ABFA87CA19DD1E66FA18FA48 (void);
// 0x00000297 System.Boolean _2D_BUNDLE.LeftOrRight.Scripts.GameManager/bj::MoveNext()
extern void bj_MoveNext_mC7D22B2EEEB294392A2C50ED873DA5D8DBA1EDE8 (void);
// 0x00000298 System.Object _2D_BUNDLE.LeftOrRight.Scripts.GameManager/bj::pd()
extern void bj_pd_mF50DD3C1BCF9F232E878BFA84D345A711EA74676 (void);
// 0x00000299 System.Void _2D_BUNDLE.LeftOrRight.Scripts.GameManager/bj::pe()
extern void bj_pe_m64751E96AC1C6DF6999B9E60B9ACCD5C438BAD14 (void);
// 0x0000029A System.Object _2D_BUNDLE.LeftOrRight.Scripts.GameManager/bj::pf()
extern void bj_pf_m4C1F52819397F9FB6C4A047A5E6710382B7C75AF (void);
// 0x0000029B System.Void _2D_BUNDLE.LeftOrRight.Scripts.Item::Start()
extern void Item_Start_mDC43C7E15C9E7E0F6C7C5FA3A6BD50E34433575C (void);
// 0x0000029C System.Void _2D_BUNDLE.LeftOrRight.Scripts.Item::OnDestroy()
extern void Item_OnDestroy_mD2EE5BDACDC6D5C77B9D9F62BD6BB8D0B2CB7732 (void);
// 0x0000029D System.Void _2D_BUNDLE.LeftOrRight.Scripts.Item::pg()
extern void Item_pg_m251F919DF7FF7BEC2DB908DBEBD89F8FB40B50D8 (void);
// 0x0000029E System.Void _2D_BUNDLE.LeftOrRight.Scripts.Item::ph(System.Int32)
extern void Item_ph_m067EC5417042A329C547FD637EC227677F06171B (void);
// 0x0000029F System.Void _2D_BUNDLE.LeftOrRight.Scripts.Item::pi()
extern void Item_pi_mDD110CEE36E510751FEEB751C96909CA711E5421 (void);
// 0x000002A0 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Item::pj()
extern void Item_pj_mA36B1415A6AAE697C5380457BC61368362242474 (void);
// 0x000002A1 System.Int32 _2D_BUNDLE.LeftOrRight.Scripts.Item::GetTime()
extern void Item_GetTime_m0A55D1280012D3CA043651A6F49632F07D9DE292 (void);
// 0x000002A2 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Item::.ctor()
extern void Item__ctor_mC997281247671AD4C70FBFA5D6DE1CD650AB6648 (void);
// 0x000002A3 System.Void _2D_BUNDLE.LeftOrRight.Scripts.ItemManager::Start()
extern void ItemManager_Start_mA4045F3FE079BC2F42BFEF85FEBEE839C8C9E74D (void);
// 0x000002A4 System.Void _2D_BUNDLE.LeftOrRight.Scripts.ItemManager::OnDestroy()
extern void ItemManager_OnDestroy_mA194ADDB8E94FD0254B4EEC85263392F58C5933C (void);
// 0x000002A5 System.Void _2D_BUNDLE.LeftOrRight.Scripts.ItemManager::pk(System.Int32)
extern void ItemManager_pk_m7D35B0172AD4225BCEFFB14BD95C4E2132362889 (void);
// 0x000002A6 System.Void _2D_BUNDLE.LeftOrRight.Scripts.ItemManager::MakeNewItem()
extern void ItemManager_MakeNewItem_mA71C3F3047C0F2BA55B50AB872D00EAE08540D3C (void);
// 0x000002A7 System.Void _2D_BUNDLE.LeftOrRight.Scripts.ItemManager::.ctor()
extern void ItemManager__ctor_m50B2F30D7A1F0E056A106253A8B2777E8BD2941B (void);
// 0x000002A8 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player::Awake()
extern void Player_Awake_m2462269B88A45E0B0EBAF1C6231413E49D20F7D2 (void);
// 0x000002A9 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player::Start()
extern void Player_Start_mB33B2176D4CBE390182A05A3CCE276BF323C4628 (void);
// 0x000002AA System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player::OnDestroy()
extern void Player_OnDestroy_m5C90E005263826EE41186B571EBD521E63A8796F (void);
// 0x000002AB System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player::Update()
extern void Player_Update_m4DAD0B204CC333CF25A9A90B460CBF27E0DC9C01 (void);
// 0x000002AC System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player::pu()
extern void Player_pu_m396FAAA4B3481C190B0259655D5A83AC5766A50D (void);
// 0x000002AD System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player::Move()
extern void Player_Move_m916785F01B6567CE631AFC936BC4B9FE122E7716 (void);
// 0x000002AE System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Player_OnTriggerEnter2D_m5A5D9351820FF36CBA383221C2663AB02ABDB7EC (void);
// 0x000002AF System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player::pv()
extern void Player_pv_mC3ADD2E4E5202858DE3A9DDF282D52D9116037D5 (void);
// 0x000002B0 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player::pw()
extern void Player_pw_mD378E906232DAF26AD3A6A80BE72087F2B61B6FF (void);
// 0x000002B1 System.Collections.IEnumerator _2D_BUNDLE.LeftOrRight.Scripts.Player::px()
extern void Player_px_m2E928397BA07D62575B8C700D1E7BE984DC07F9A (void);
// 0x000002B2 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player::SetToReady()
extern void Player_SetToReady_m45694D7E2B364FC0ACB3EEC95A5EAE964F1E3A24 (void);
// 0x000002B3 System.Collections.IEnumerator _2D_BUNDLE.LeftOrRight.Scripts.Player::py()
extern void Player_py_m097C7215AFCD8681E251AE97272BD1D5DED66174 (void);
// 0x000002B4 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player::SetDirection(System.Int32)
extern void Player_SetDirection_m980AEC03B72EDA925B3FE6622432102A3393051F (void);
// 0x000002B5 System.Boolean _2D_BUNDLE.LeftOrRight.Scripts.Player::IsStarted()
extern void Player_IsStarted_mFA5EBBA9688DFA3E47C21318C08C91759C7B3307 (void);
// 0x000002B6 System.Boolean _2D_BUNDLE.LeftOrRight.Scripts.Player::IsDead()
extern void Player_IsDead_m71FD3FB80F0D5397BB42A503239AAA12E31F179D (void);
// 0x000002B7 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player::.ctor()
extern void Player__ctor_mDCB31459BE35C4F17DDA704BD31B6E6267D27A21 (void);
// 0x000002B8 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player/bk::.ctor(System.Int32)
extern void bk__ctor_mE90D457218E41BF8F1F13B3896BF1BD29D103E53 (void);
// 0x000002B9 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player/bk::pl()
extern void bk_pl_mA159A45CDA9DBC3CCEA41034242D2AB46373CBC7 (void);
// 0x000002BA System.Boolean _2D_BUNDLE.LeftOrRight.Scripts.Player/bk::MoveNext()
extern void bk_MoveNext_m375CEF5C71AC0F2885B81C3FB0E50D4A6F98671F (void);
// 0x000002BB System.Object _2D_BUNDLE.LeftOrRight.Scripts.Player/bk::pm()
extern void bk_pm_m898FC22EFDC5790D418A9CDF5745ED8A11D7D52E (void);
// 0x000002BC System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player/bk::pn()
extern void bk_pn_mC347FF8FB0A7CCEE6D9748E708C4220AB6248DD9 (void);
// 0x000002BD System.Object _2D_BUNDLE.LeftOrRight.Scripts.Player/bk::po()
extern void bk_po_m537494BA3B5308C6E9915C3273E0F7EB26560A98 (void);
// 0x000002BE System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player/bl::.ctor(System.Int32)
extern void bl__ctor_m328647131621984E3905046A7CDF2810E9C3CA46 (void);
// 0x000002BF System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player/bl::pp()
extern void bl_pp_mFFA764E1D2939102AA13B77E2080C2994DCD2473 (void);
// 0x000002C0 System.Boolean _2D_BUNDLE.LeftOrRight.Scripts.Player/bl::MoveNext()
extern void bl_MoveNext_m6A76CAD56094D249E75A2F0D9043B3E5230783FE (void);
// 0x000002C1 System.Object _2D_BUNDLE.LeftOrRight.Scripts.Player/bl::pq()
extern void bl_pq_m6D5ED13FDC4B40F47B925C3E4947B1E0E0BE316D (void);
// 0x000002C2 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Player/bl::pr()
extern void bl_pr_mCB132AB9EE75B7895A2F311086B266012366E315 (void);
// 0x000002C3 System.Object _2D_BUNDLE.LeftOrRight.Scripts.Player/bl::pt()
extern void bl_pt_m9BD7A646EF97582D8F055225560BC816BE783BA7 (void);
// 0x000002C4 System.Void _2D_BUNDLE.LeftOrRight.Scripts.RedMask::Start()
extern void RedMask_Start_m22127DD848AEDF3D2B1BF7B82F87A7E0AB6B13F4 (void);
// 0x000002C5 System.Void _2D_BUNDLE.LeftOrRight.Scripts.RedMask::OnDestroy()
extern void RedMask_OnDestroy_mD34DA89928AADA3C317F74BEF977701E8D053526 (void);
// 0x000002C6 System.Void _2D_BUNDLE.LeftOrRight.Scripts.RedMask::Update()
extern void RedMask_Update_mCAA2B0556342F2E5FC21446D66E26189617F11E7 (void);
// 0x000002C7 System.Void _2D_BUNDLE.LeftOrRight.Scripts.RedMask::pz()
extern void RedMask_pz_m7DC0959F635CD80A14EA4CD10B77AB0F9F0F5727 (void);
// 0x000002C8 System.Void _2D_BUNDLE.LeftOrRight.Scripts.RedMask::qa()
extern void RedMask_qa_m2F7D9EE01945ED95A3F3FCE5978A7842477E248B (void);
// 0x000002C9 System.Void _2D_BUNDLE.LeftOrRight.Scripts.RedMask::.ctor()
extern void RedMask__ctor_m557AA296A5FFEC7CEDB7B18081C265E3D183C148 (void);
// 0x000002CA System.Void _2D_BUNDLE.LeftOrRight.Scripts.ScoreManager::Awake()
extern void ScoreManager_Awake_m687FB67A3755ED50680F7FDD6A3FF7F60BD0013C (void);
// 0x000002CB System.Void _2D_BUNDLE.LeftOrRight.Scripts.ScoreManager::Start()
extern void ScoreManager_Start_m63250F37FE772E9E0BB14D1228F89246B37D766B (void);
// 0x000002CC System.Void _2D_BUNDLE.LeftOrRight.Scripts.ScoreManager::OnDestroy()
extern void ScoreManager_OnDestroy_m25588987FA526B0B0F60B3BAD4BFE696A0538162 (void);
// 0x000002CD System.Void _2D_BUNDLE.LeftOrRight.Scripts.ScoreManager::qb()
extern void ScoreManager_qb_m69E35A687C31521FE2816AD8ECAE6107F3B2DE93 (void);
// 0x000002CE System.Void _2D_BUNDLE.LeftOrRight.Scripts.ScoreManager::qc(System.Int32)
extern void ScoreManager_qc_mACA81F8EC827C1D5771A275C47F8428C40ECD117 (void);
// 0x000002CF System.Void _2D_BUNDLE.LeftOrRight.Scripts.ScoreManager::qd()
extern void ScoreManager_qd_m63A3980B590D77DFE02058E8DD077848181CA38D (void);
// 0x000002D0 System.Void _2D_BUNDLE.LeftOrRight.Scripts.ScoreManager::qe()
extern void ScoreManager_qe_mD1E9339107F20A2D3F09E723991CE927AF5DDE33 (void);
// 0x000002D1 System.Int32 _2D_BUNDLE.LeftOrRight.Scripts.ScoreManager::GetCurrentScore()
extern void ScoreManager_GetCurrentScore_m12F15C9EE6400156E638E95309C305C921190CF0 (void);
// 0x000002D2 System.Void _2D_BUNDLE.LeftOrRight.Scripts.ScoreManager::.ctor()
extern void ScoreManager__ctor_m9C73858C6176A83E8BC5244196B6E35181CF96B6 (void);
// 0x000002D3 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Stairs::StairsEffectCoroutine()
extern void Stairs_StairsEffectCoroutine_m227271C14F880498AE51B99A9B48FED1D7CF5FFC (void);
// 0x000002D4 System.Collections.IEnumerator _2D_BUNDLE.LeftOrRight.Scripts.Stairs::qj()
extern void Stairs_qj_mCB57127D045EFE64DDC0A819C3A4F4310ECBC317 (void);
// 0x000002D5 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Stairs::.ctor()
extern void Stairs__ctor_mA2392CCFE155A3EA346558D7764316A5E9429BC0 (void);
// 0x000002D6 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Stairs/bm::.ctor(System.Int32)
extern void bm__ctor_mF2D1A8506267A940D656F6B5F41DC102DB5A1BE1 (void);
// 0x000002D7 System.Void _2D_BUNDLE.LeftOrRight.Scripts.Stairs/bm::qf()
extern void bm_qf_m15BC7A176A85EE1BE0CDAE097AD663D13A251561 (void);
// 0x000002D8 System.Boolean _2D_BUNDLE.LeftOrRight.Scripts.Stairs/bm::MoveNext()
extern void bm_MoveNext_m2F17F6624509619D6A3F21BE3A9A37B7C3895970 (void);
// 0x000002D9 System.Object _2D_BUNDLE.LeftOrRight.Scripts.Stairs/bm::qg()
extern void bm_qg_m644AC8EE90689ED9ABB928834F81A6846E81866E (void);
// 0x000002DA System.Void _2D_BUNDLE.LeftOrRight.Scripts.Stairs/bm::qh()
extern void bm_qh_m57E0FE22BC6A792036CDE77F825DA6C72CE3A8CE (void);
// 0x000002DB System.Object _2D_BUNDLE.LeftOrRight.Scripts.Stairs/bm::qi()
extern void bm_qi_m0077613FA57E2537F1810A7D5FD49FF8ECEE7FAA (void);
// 0x000002DC System.Void _2D_BUNDLE.LeftOrRight.Scripts.StairsManager::Start()
extern void StairsManager_Start_mB49D980A10F1BD586FB8EA8041AB24E426F2AE70 (void);
// 0x000002DD System.Void _2D_BUNDLE.LeftOrRight.Scripts.StairsManager::OnDestroy()
extern void StairsManager_OnDestroy_m2DF06F6A83D2204F4EC3450A8EC9F6213E140A49 (void);
// 0x000002DE System.Void _2D_BUNDLE.LeftOrRight.Scripts.StairsManager::qo()
extern void StairsManager_qo_m61328A15A0026B7A9B0ACD1CB3A7B6FEC5802B79 (void);
// 0x000002DF System.Void _2D_BUNDLE.LeftOrRight.Scripts.StairsManager::qp()
extern void StairsManager_qp_m42992830797DA3217093D7E552AFE3AD41550030 (void);
// 0x000002E0 System.Collections.IEnumerator _2D_BUNDLE.LeftOrRight.Scripts.StairsManager::qq()
extern void StairsManager_qq_mFEFD579161834F8DF0E3D7515E11E617F9E6BEEA (void);
// 0x000002E1 System.Void _2D_BUNDLE.LeftOrRight.Scripts.StairsManager::qr()
extern void StairsManager_qr_m49F4B6D6F795D0DEAFD4A42D139E2B6D8EC6C478 (void);
// 0x000002E2 System.Void _2D_BUNDLE.LeftOrRight.Scripts.StairsManager::qs()
extern void StairsManager_qs_mF8BB6777BC85B78946ADE370E307C74D4F2A3493 (void);
// 0x000002E3 UnityEngine.Vector3 _2D_BUNDLE.LeftOrRight.Scripts.StairsManager::GetNextStairsForItem()
extern void StairsManager_GetNextStairsForItem_mC855925B7FDD37CF9CC9DC5A6E8F7A170DD90076 (void);
// 0x000002E4 System.Single _2D_BUNDLE.LeftOrRight.Scripts.StairsManager::GetStairHeight()
extern void StairsManager_GetStairHeight_m7B78603249F88D8807AF9C37367E76412DC5D455 (void);
// 0x000002E5 System.ValueTuple`2<System.Single,System.Single> _2D_BUNDLE.LeftOrRight.Scripts.StairsManager::GetDistance()
extern void StairsManager_GetDistance_m4C908B9829108C012959806B460604E545B9B7C0 (void);
// 0x000002E6 System.Void _2D_BUNDLE.LeftOrRight.Scripts.StairsManager::.ctor()
extern void StairsManager__ctor_m96120CB7589C1837DA2C3A0F5181CDBA03335F64 (void);
// 0x000002E7 System.Void _2D_BUNDLE.LeftOrRight.Scripts.StairsManager/bn::.ctor(System.Int32)
extern void bn__ctor_m60FA6EE620B5EEB422C0164E7980E3082B5A215C (void);
// 0x000002E8 System.Void _2D_BUNDLE.LeftOrRight.Scripts.StairsManager/bn::qk()
extern void bn_qk_m138BBEF88F9B2E512265379BCDD79A2C12F240B8 (void);
// 0x000002E9 System.Boolean _2D_BUNDLE.LeftOrRight.Scripts.StairsManager/bn::MoveNext()
extern void bn_MoveNext_mEF805D59DBCD832219FE16B9568F9DCDDB8BA149 (void);
// 0x000002EA System.Object _2D_BUNDLE.LeftOrRight.Scripts.StairsManager/bn::ql()
extern void bn_ql_m109DB4A0880DC979DD648A7B2EF1C58AD163E320 (void);
// 0x000002EB System.Void _2D_BUNDLE.LeftOrRight.Scripts.StairsManager/bn::qm()
extern void bn_qm_mE6ED2E4ACC16B108E7058E1F08D926545B186943 (void);
// 0x000002EC System.Object _2D_BUNDLE.LeftOrRight.Scripts.StairsManager/bn::qn()
extern void bn_qn_mB492FA84896EBD1AC38F1C14B502727A944A270B (void);
// 0x000002ED System.Void _2D_BUNDLE.LeftOrRight.Scripts.UiManager::Start()
extern void UiManager_Start_m9A6F84C4E6AB2F1613F4AA7105A758824320ED40 (void);
// 0x000002EE System.Void _2D_BUNDLE.LeftOrRight.Scripts.UiManager::OnDestroy()
extern void UiManager_OnDestroy_m443C9E3AC23B1E70199611FE4AB6375CBA8B173B (void);
// 0x000002EF System.Void _2D_BUNDLE.LeftOrRight.Scripts.UiManager::qx()
extern void UiManager_qx_m4799829285D1CBF84C0EB15CC81107DA249BF179 (void);
// 0x000002F0 System.Void _2D_BUNDLE.LeftOrRight.Scripts.UiManager::HowToButton()
extern void UiManager_HowToButton_m8992057B7335A5C65337E57522CCF37E7B4D45F3 (void);
// 0x000002F1 System.Void _2D_BUNDLE.LeftOrRight.Scripts.UiManager::CloseButton()
extern void UiManager_CloseButton_m2F4563EE7D1FB0BEBB0FEB7761C77BA5197929F5 (void);
// 0x000002F2 System.Void _2D_BUNDLE.LeftOrRight.Scripts.UiManager::qy()
extern void UiManager_qy_m330E48D5005970800DC3F33163390237CBF0BD83 (void);
// 0x000002F3 System.Collections.IEnumerator _2D_BUNDLE.LeftOrRight.Scripts.UiManager::qz()
extern void UiManager_qz_m07D89D21F8702B8F0268C8E597FEF0772CEEF69F (void);
// 0x000002F4 System.Void _2D_BUNDLE.LeftOrRight.Scripts.UiManager::ra()
extern void UiManager_ra_m5FEB9154A3840B8F0CA078F1E46EFB9C1CC9D4D4 (void);
// 0x000002F5 System.Void _2D_BUNDLE.LeftOrRight.Scripts.UiManager::.ctor()
extern void UiManager__ctor_mC8510C4460142DFF04064F1FC7CB9E7FB4A096F0 (void);
// 0x000002F6 System.Void _2D_BUNDLE.LeftOrRight.Scripts.UiManager/bo::.ctor(System.Int32)
extern void bo__ctor_m0684CCE4C6D8407226F18C8FCABE051B083478D4 (void);
// 0x000002F7 System.Void _2D_BUNDLE.LeftOrRight.Scripts.UiManager/bo::qt()
extern void bo_qt_mAD667D35DD34E366616C695EF96D5C4D977AD0E6 (void);
// 0x000002F8 System.Boolean _2D_BUNDLE.LeftOrRight.Scripts.UiManager/bo::MoveNext()
extern void bo_MoveNext_m4D16E6606826E485728274234C12DFEC9C60D176 (void);
// 0x000002F9 System.Object _2D_BUNDLE.LeftOrRight.Scripts.UiManager/bo::qu()
extern void bo_qu_m594FA061A4FE9EF4AB1B38CA4644CCB1DCD3FE76 (void);
// 0x000002FA System.Void _2D_BUNDLE.LeftOrRight.Scripts.UiManager/bo::qv()
extern void bo_qv_mBF7E0FB66C0734E86501417F48D18E16A3915B8F (void);
// 0x000002FB System.Object _2D_BUNDLE.LeftOrRight.Scripts.UiManager/bo::qw()
extern void bo_qw_m5D1A8705062F774E4905BD4797C7E63244A8FCAB (void);
// 0x000002FC System.Void _2D_BUNDLE.JumpAndShoot.Scripts.CameraFollow::LateUpdate()
extern void CameraFollow_LateUpdate_m125090DC50DE475F408C513F91BBF6049ACD2AEA (void);
// 0x000002FD System.Void _2D_BUNDLE.JumpAndShoot.Scripts.CameraFollow::rc()
extern void CameraFollow_rc_m4338BF7E92242FD6B37D444291731A4503D5E1C8 (void);
// 0x000002FE System.Void _2D_BUNDLE.JumpAndShoot.Scripts.CameraFollow::.ctor()
extern void CameraFollow__ctor_mDF52FB7BA37E9AD94F53A364F8BB76F866535C3C (void);
// 0x000002FF System.Void _2D_BUNDLE.JumpAndShoot.Scripts.ColorManager::Awake()
extern void ColorManager_Awake_mE6F9306AB138F12FF6E6A8F631C22A4A1F518594 (void);
// 0x00000300 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.ColorManager::Start()
extern void ColorManager_Start_m99ECC72F5B32CD93A8824C724F131C06212CB046 (void);
// 0x00000301 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.ColorManager::rd()
extern void ColorManager_rd_m4BA2E413479541BA8211721217C23CBBF2837545 (void);
// 0x00000302 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.ColorManager::ChangeBackgroundColor()
extern void ColorManager_ChangeBackgroundColor_m6A305CC596FE38BA5D88AE2C089EEE15F78957D7 (void);
// 0x00000303 System.Single _2D_BUNDLE.JumpAndShoot.Scripts.ColorManager::GetCurrentHue()
extern void ColorManager_GetCurrentHue_mA02EA7BBBD923EB7011B63354CCD01538076910D (void);
// 0x00000304 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.ColorManager::.ctor()
extern void ColorManager__ctor_mE9F395089A79AD2B77501D1C069CEE436E3626B0 (void);
// 0x00000305 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.GameManager::Awake()
extern void GameManager_Awake_mD3586CDF4BB13CBE5FD3168E37B1FDD7CF5A475A (void);
// 0x00000306 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.GameManager::Start()
extern void GameManager_Start_m8ECCF0E5CF9218BC02F261988E543FC605A60FCD (void);
// 0x00000307 System.Collections.IEnumerator _2D_BUNDLE.JumpAndShoot.Scripts.GameManager::ri()
extern void GameManager_ri_m3A1EFD50DFED0F422CE9D7677E0294D375582B84 (void);
// 0x00000308 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.GameManager::Restart()
extern void GameManager_Restart_mC6F3479CE10A521CC5CE860DE68D52D5F75FA328 (void);
// 0x00000309 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.GameManager::.ctor()
extern void GameManager__ctor_m9BB33D4B97961DA9206D29193B632068A2F6AE3A (void);
// 0x0000030A System.Void _2D_BUNDLE.JumpAndShoot.Scripts.GameManager/bp::.ctor(System.Int32)
extern void bp__ctor_m58E0D58650CCD871E28EE4A7E0B7D944B6BB112F (void);
// 0x0000030B System.Void _2D_BUNDLE.JumpAndShoot.Scripts.GameManager/bp::re()
extern void bp_re_m1A40D0461113A9E887C34B6F2C03BDBDA2163C89 (void);
// 0x0000030C System.Boolean _2D_BUNDLE.JumpAndShoot.Scripts.GameManager/bp::MoveNext()
extern void bp_MoveNext_m2473DAE8994E9F98693BAC02A4A79FC92CF25F6F (void);
// 0x0000030D System.Object _2D_BUNDLE.JumpAndShoot.Scripts.GameManager/bp::rf()
extern void bp_rf_mC8B948BDB8CE9CEACEABD43BED9476672CA1E0E0 (void);
// 0x0000030E System.Void _2D_BUNDLE.JumpAndShoot.Scripts.GameManager/bp::rg()
extern void bp_rg_mE53EF6D7D8816CEBDA0A70966C99833BA60DE3CF (void);
// 0x0000030F System.Object _2D_BUNDLE.JumpAndShoot.Scripts.GameManager/bp::rh()
extern void bp_rh_m59D1DD8C7D20A019D29C490FCA2393FAF3FA841F (void);
// 0x00000310 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.GameOverEffectPanel::Awake()
extern void GameOverEffectPanel_Awake_m52008E2521C3D1497745B0A55C1F5C9CCE0F8893 (void);
// 0x00000311 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.GameOverEffectPanel::.ctor()
extern void GameOverEffectPanel__ctor_mC4FA60E4FD8E2005A1F9DCD7B713A49B9ED6816D (void);
// 0x00000312 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.GetDisplayBound::Awake()
extern void GetDisplayBound_Awake_mBE123233777B6F65608BD581A00DB8216A115E22 (void);
// 0x00000313 System.Single _2D_BUNDLE.JumpAndShoot.Scripts.GetDisplayBound::GetLeftWallPosition()
extern void GetDisplayBound_GetLeftWallPosition_m9E4EEFEFBF800FF8C98CB2D8DE387B9544698437 (void);
// 0x00000314 System.Single _2D_BUNDLE.JumpAndShoot.Scripts.GetDisplayBound::GetRightWallPosition()
extern void GetDisplayBound_GetRightWallPosition_m12EF537E4C2D53B830057A65EA1294083C83F8F2 (void);
// 0x00000315 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.GetDisplayBound::.ctor()
extern void GetDisplayBound__ctor_m4FB46BD6F2B086913FDA087A653DA9853792A8A2 (void);
// 0x00000316 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Player::Awake()
extern void Player_Awake_m879C97D3E6BAAB8B7896DBD486140987626AAF0E (void);
// 0x00000317 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Player::Start()
extern void Player_Start_mD6D66C529373A5CDA06754E9A2B1D135DBCDEB5C (void);
// 0x00000318 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Player::Update()
extern void Player_Update_m7FDC6A6CA5E7B016FAAE2FE129A2665DD2B4D8A8 (void);
// 0x00000319 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Player::rn()
extern void Player_rn_mEF4096AA18A0E2BA0AF7B350C7A29879E9F22239 (void);
// 0x0000031A System.Collections.IEnumerator _2D_BUNDLE.JumpAndShoot.Scripts.Player::ro()
extern void Player_ro_m68BC4BEC681240EBE6D491AE3C88CCCC15B9BC71 (void);
// 0x0000031B System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Player::rp()
extern void Player_rp_m69D7FBEFD36C3B81CD493E5CFE91BD92B4433A27 (void);
// 0x0000031C System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Player::rq()
extern void Player_rq_m6E8BF252B0B08F6D178A3DEDB1C727A94B09098F (void);
// 0x0000031D System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Player::rr()
extern void Player_rr_mABD73DDC22AB704A11AD58D1D62ECEF4E1372994 (void);
// 0x0000031E System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Player::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Player_OnCollisionEnter2D_mF8D3D65B6D85BCD59CAEF13B5BBA69A075F81384 (void);
// 0x0000031F System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Player::OnCollisionExit2D(UnityEngine.Collision2D)
extern void Player_OnCollisionExit2D_m981D0134E0BF303621A0ED9C08BF5A5051296422 (void);
// 0x00000320 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Player::rs(UnityEngine.Collision2D)
extern void Player_rs_m206D4D26F38CFF643308EBE6F1980505B6655FC8 (void);
// 0x00000321 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Player::.ctor()
extern void Player__ctor_m79D03802B4473AF5801C91A72EF6375BFD07F314 (void);
// 0x00000322 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Player/br::.ctor(System.Int32)
extern void br__ctor_m722F9230FF4F6FCF047BB258C42A77A395193E3B (void);
// 0x00000323 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Player/br::rj()
extern void br_rj_mFBFEB5EE1882F543906ACB9D8794CF073C1A15D7 (void);
// 0x00000324 System.Boolean _2D_BUNDLE.JumpAndShoot.Scripts.Player/br::MoveNext()
extern void br_MoveNext_m03E4A0917F437275BC620EE14B7683E9421E78A9 (void);
// 0x00000325 System.Object _2D_BUNDLE.JumpAndShoot.Scripts.Player/br::rk()
extern void br_rk_mF9684FEB13CE7368D0FE26710318E2A7A89F224D (void);
// 0x00000326 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Player/br::rl()
extern void br_rl_m9797D2BB05A75A57F5895BCE5251C8433433CC8E (void);
// 0x00000327 System.Object _2D_BUNDLE.JumpAndShoot.Scripts.Player/br::rm()
extern void br_rm_m6D407F6E3762D143D6451AA0BE25F1FAC06820BF (void);
// 0x00000328 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.ScaleAndColorAnimation::Start()
extern void ScaleAndColorAnimation_Start_m560E50F6B004AD91D964BE6835D578AE21144356 (void);
// 0x00000329 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.ScaleAndColorAnimation::rt()
extern void ScaleAndColorAnimation_rt_m4A331EB768E244C1B18DFA4C15F8EA94D3BC1C9F (void);
// 0x0000032A System.Void _2D_BUNDLE.JumpAndShoot.Scripts.ScaleAndColorAnimation::ru()
extern void ScaleAndColorAnimation_ru_m924EDF472804B55C72F9D2EEFB30F6EBE7BD3300 (void);
// 0x0000032B System.Void _2D_BUNDLE.JumpAndShoot.Scripts.ScaleAndColorAnimation::rv()
extern void ScaleAndColorAnimation_rv_m150CA9FD88FC6EE77AD6F31B16A784B11FFFBE61 (void);
// 0x0000032C System.Void _2D_BUNDLE.JumpAndShoot.Scripts.ScaleAndColorAnimation::rw()
extern void ScaleAndColorAnimation_rw_m91E83DBA12C8379F6FDE039A4FF68AA8FC7C19B5 (void);
// 0x0000032D System.Boolean _2D_BUNDLE.JumpAndShoot.Scripts.ScaleAndColorAnimation::rx()
extern void ScaleAndColorAnimation_rx_m9277C6B3494E83FD998A39271B62F5AD3068C1C8 (void);
// 0x0000032E System.Void _2D_BUNDLE.JumpAndShoot.Scripts.ScaleAndColorAnimation::.ctor()
extern void ScaleAndColorAnimation__ctor_m0DC0B88B0F2D1683D41D5B8839FF16B058671F34 (void);
// 0x0000032F System.Void _2D_BUNDLE.JumpAndShoot.Scripts.ScoreManager::Awake()
extern void ScoreManager_Awake_m7A1357B88AB36DEAB1E8C6D5BCE67D5A1EF5B7BA (void);
// 0x00000330 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.ScoreManager::AddScore(System.Int32)
extern void ScoreManager_AddScore_m8623FE126777861C1B1C97C204E01409687EBAB9 (void);
// 0x00000331 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.ScoreManager::.ctor()
extern void ScoreManager__ctor_mCDE56329D0AFE13A27802CA97AC0602E06A68F64 (void);
// 0x00000332 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.StartEffectPanel::Awake()
extern void StartEffectPanel_Awake_m0288F8E960EB9612091DBA272008D5F3583E6C09 (void);
// 0x00000333 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.StartEffectPanel::.ctor()
extern void StartEffectPanel__ctor_m6177C3786FA0E115AC9059FC6FE53B709A9B66B7 (void);
// 0x00000334 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Step::Update()
extern void Step_Update_m477D30BD79B9F59B47D8E9AF23FFBEA0BA7E488A (void);
// 0x00000335 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Step::sc()
extern void Step_sc_mA178C895BE6CBE7ADAB03DAC848F90D71C3144F6 (void);
// 0x00000336 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Step::LandingEffect()
extern void Step_LandingEffect_m3DFAAE578F79186B5B3AE33598F9AD5DEC47CED4 (void);
// 0x00000337 System.Collections.IEnumerator _2D_BUNDLE.JumpAndShoot.Scripts.Step::LandingEffectCoroutine()
extern void Step_LandingEffectCoroutine_m70B35A685FE8BBD1104432ED9EFF98E2295E2DD6 (void);
// 0x00000338 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Step::SetDistance(System.Single)
extern void Step_SetDistance_m4C29D365F22BA312FDFE093EA8143B0E8750E817 (void);
// 0x00000339 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Step::SetVelocity(System.Single)
extern void Step_SetVelocity_mF52FE45E549C08EF2A44CE2BB66C1439F7119ECD (void);
// 0x0000033A System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Step::.ctor()
extern void Step__ctor_mE570630E48D7873403A07B1E1B9A85D162987F3C (void);
// 0x0000033B System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Step/bs::.ctor(System.Int32)
extern void bs__ctor_mE811588FB0DEFAC15927FE2097153C20789F8D4D (void);
// 0x0000033C System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Step/bs::ry()
extern void bs_ry_mE57ADE478E205D10ED90862B26EEA763727A0A9B (void);
// 0x0000033D System.Boolean _2D_BUNDLE.JumpAndShoot.Scripts.Step/bs::MoveNext()
extern void bs_MoveNext_m00CE97013E4177EF1EFF5CA27D4A7D773ADA8BEC (void);
// 0x0000033E System.Object _2D_BUNDLE.JumpAndShoot.Scripts.Step/bs::rz()
extern void bs_rz_m867B688A46ECD23F82EEEB1DBFD31A5AFC76DFEE (void);
// 0x0000033F System.Void _2D_BUNDLE.JumpAndShoot.Scripts.Step/bs::sa()
extern void bs_sa_m7F12D97B69E786605225758E14798840BD5E341F (void);
// 0x00000340 System.Object _2D_BUNDLE.JumpAndShoot.Scripts.Step/bs::sb()
extern void bs_sb_m0C6B2004FC51DCBDE8037CECC56E93440326F0B8 (void);
// 0x00000341 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.StepManager::Awake()
extern void StepManager_Awake_m55B1469C6F64E25E96F41194B117AC2B9745DDE6 (void);
// 0x00000342 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.StepManager::Start()
extern void StepManager_Start_m8D10619F9E441FE16E3F36E60F4B07D518130AFC (void);
// 0x00000343 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.StepManager::sd()
extern void StepManager_sd_m8609E49D8C6F8E21B17A09E5D3F74D80F2A1EAAF (void);
// 0x00000344 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.StepManager::CreateNewStep()
extern void StepManager_CreateNewStep_m4241375FEF99368701AB0EB49E5F85475EE80EEB (void);
// 0x00000345 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.StepManager::.ctor()
extern void StepManager__ctor_m3CC58BF48624FDFB7E2C6A303699B1E47EA55301 (void);
// 0x00000346 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.UIManager::Awake()
extern void UIManager_Awake_mB9B63EB11315DD138C7A360764E062D38AB2DF8D (void);
// 0x00000347 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.UIManager::GameOver()
extern void UIManager_GameOver_m03D2592F9838AEFFE33C75612F8D98FA0AAB4832 (void);
// 0x00000348 System.Collections.IEnumerator _2D_BUNDLE.JumpAndShoot.Scripts.UIManager::si()
extern void UIManager_si_m102009651ACED538BF6DDC4E0B55905D6DF35740 (void);
// 0x00000349 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.UIManager::OpenHowToPanel()
extern void UIManager_OpenHowToPanel_m910AF731EDBE273AD5011A110DBD029AB4433FDB (void);
// 0x0000034A System.Void _2D_BUNDLE.JumpAndShoot.Scripts.UIManager::CloseHowToPanel()
extern void UIManager_CloseHowToPanel_mFAA338EB570B00D96230F51AF83A9526148C4F0E (void);
// 0x0000034B System.Void _2D_BUNDLE.JumpAndShoot.Scripts.UIManager::.ctor()
extern void UIManager__ctor_m921859BF8E3190A20AF69B1893E31BD2302CA4DF (void);
// 0x0000034C System.Void _2D_BUNDLE.JumpAndShoot.Scripts.UIManager/bt::.ctor(System.Int32)
extern void bt__ctor_m5C8158A7676982C7EF0A53E8C1DD139A87C242AF (void);
// 0x0000034D System.Void _2D_BUNDLE.JumpAndShoot.Scripts.UIManager/bt::se()
extern void bt_se_m8CAF97A934FBA0E6A52A131C2ABA79633AC6C627 (void);
// 0x0000034E System.Boolean _2D_BUNDLE.JumpAndShoot.Scripts.UIManager/bt::MoveNext()
extern void bt_MoveNext_mF1DB1F11551AA22687EB6EF8C978D8234B86A791 (void);
// 0x0000034F System.Object _2D_BUNDLE.JumpAndShoot.Scripts.UIManager/bt::sf()
extern void bt_sf_m8B94E94652F734EC39FAEF250F3FC7FBBB2A2B26 (void);
// 0x00000350 System.Void _2D_BUNDLE.JumpAndShoot.Scripts.UIManager/bt::sg()
extern void bt_sg_m571133A45007639CC65D3788EA539B23EC17250E (void);
// 0x00000351 System.Object _2D_BUNDLE.JumpAndShoot.Scripts.UIManager/bt::sh()
extern void bt_sh_mDBC1AF4BBC281E3B7646D8E8FA353B164BF83386 (void);
// 0x00000352 System.Void _2D_BUNDLE.ColorJump.Scripts.DisplayManager::Awake()
extern void DisplayManager_Awake_m8F62E67CA557943AB6F8A21D76151C3C0215B3CB (void);
// 0x00000353 System.Void _2D_BUNDLE.ColorJump.Scripts.DisplayManager::sj()
extern void DisplayManager_sj_mF8B8BC51C64096433D0AF8C5DA59273BACEFA8B4 (void);
// 0x00000354 System.Single _2D_BUNDLE.ColorJump.Scripts.DisplayManager::GetHeight()
extern void DisplayManager_GetHeight_m308B3CDDEE1ED36945E254BD8E8BF6FD85941743 (void);
// 0x00000355 System.Single _2D_BUNDLE.ColorJump.Scripts.DisplayManager::GetLeft()
extern void DisplayManager_GetLeft_m003BFF1C3243236661F49D193BE35D74BEFA2967 (void);
// 0x00000356 System.Single _2D_BUNDLE.ColorJump.Scripts.DisplayManager::GetRight()
extern void DisplayManager_GetRight_m6A06DB0EF854A6627D7FB7DCEB236C7B3939B530 (void);
// 0x00000357 System.Single _2D_BUNDLE.ColorJump.Scripts.DisplayManager::GetBottom()
extern void DisplayManager_GetBottom_m3076599AF4C05FF3617309255CC9B9A1D8840EE1 (void);
// 0x00000358 System.Void _2D_BUNDLE.ColorJump.Scripts.DisplayManager::.ctor()
extern void DisplayManager__ctor_m7BFFB9C355D3AD94EAF667461DB9E78954ECA356 (void);
// 0x00000359 System.Void _2D_BUNDLE.ColorJump.Scripts.EffectManager::Awake()
extern void EffectManager_Awake_m77483CE51BCC5C5A4DA98996256A7249450E359C (void);
// 0x0000035A System.Void _2D_BUNDLE.ColorJump.Scripts.EffectManager::Start()
extern void EffectManager_Start_m0326A72E70526C8866CB1390006B605BFDAF71E5 (void);
// 0x0000035B System.Collections.IEnumerator _2D_BUNDLE.ColorJump.Scripts.EffectManager::ss()
extern void EffectManager_ss_mF52CE15FB6D18EAB48CC8358237ADADE866A0A58 (void);
// 0x0000035C System.Void _2D_BUNDLE.ColorJump.Scripts.EffectManager::StartGame()
extern void EffectManager_StartGame_mB45D36A776DDEEB82D14AB1525358FCBF15B7098 (void);
// 0x0000035D System.Collections.IEnumerator _2D_BUNDLE.ColorJump.Scripts.EffectManager::GameOverEffectCoroutine()
extern void EffectManager_GameOverEffectCoroutine_m0AD98F78BB8313509A90183A71A6532DF56F9FBE (void);
// 0x0000035E System.Void _2D_BUNDLE.ColorJump.Scripts.EffectManager::.ctor()
extern void EffectManager__ctor_m69EB2F67C7F0FEF460FAC809D0104DC6581C5A44 (void);
// 0x0000035F System.Void _2D_BUNDLE.ColorJump.Scripts.EffectManager/bu::.ctor(System.Int32)
extern void bu__ctor_mA741D6EE6818BE34BD94898A915BE2EEFD0F8EB4 (void);
// 0x00000360 System.Void _2D_BUNDLE.ColorJump.Scripts.EffectManager/bu::sk()
extern void bu_sk_m149B8FE0FF284CEA447B07E6ACDCFF49F28239BE (void);
// 0x00000361 System.Boolean _2D_BUNDLE.ColorJump.Scripts.EffectManager/bu::MoveNext()
extern void bu_MoveNext_m089286570A80060024D4911FCB5D77BF94E3D168 (void);
// 0x00000362 System.Object _2D_BUNDLE.ColorJump.Scripts.EffectManager/bu::sl()
extern void bu_sl_mA87D1753E7C694FF88933A010287BBEE1806306C (void);
// 0x00000363 System.Void _2D_BUNDLE.ColorJump.Scripts.EffectManager/bu::sm()
extern void bu_sm_mA64A4E92DB9AB16E7F98CE1A51DA4CC9D7E43503 (void);
// 0x00000364 System.Object _2D_BUNDLE.ColorJump.Scripts.EffectManager/bu::sn()
extern void bu_sn_mC4FE8203ED85FE18920A9024785A2BB0DD2AB112 (void);
// 0x00000365 System.Void _2D_BUNDLE.ColorJump.Scripts.EffectManager/bv::.ctor(System.Int32)
extern void bv__ctor_m7CB8BAE85AE2FEBAD7A0DB62F845E293A2F6BBBC (void);
// 0x00000366 System.Void _2D_BUNDLE.ColorJump.Scripts.EffectManager/bv::so()
extern void bv_so_m9C839250617AC1325A4DFFC88EBBE2AE769DBA27 (void);
// 0x00000367 System.Boolean _2D_BUNDLE.ColorJump.Scripts.EffectManager/bv::MoveNext()
extern void bv_MoveNext_m39F83C7E26DC51876B163807227B6EA6E2F64C5D (void);
// 0x00000368 System.Object _2D_BUNDLE.ColorJump.Scripts.EffectManager/bv::sp()
extern void bv_sp_m64300D0D31E68663E3DB4E36B566139A11475DC1 (void);
// 0x00000369 System.Void _2D_BUNDLE.ColorJump.Scripts.EffectManager/bv::sq()
extern void bv_sq_m44F878475A2886A17DB290F8467FA19709FD1C65 (void);
// 0x0000036A System.Object _2D_BUNDLE.ColorJump.Scripts.EffectManager/bv::sr()
extern void bv_sr_mF56FFCE5DC64AC31B086BBD656A43070D3ACDF9D (void);
// 0x0000036B System.Void _2D_BUNDLE.ColorJump.Scripts.FollowPlayer::LateUpdate()
extern void FollowPlayer_LateUpdate_m2524F596591BBB0C3CA2685E4C316D962A9EB153 (void);
// 0x0000036C System.Void _2D_BUNDLE.ColorJump.Scripts.FollowPlayer::.ctor()
extern void FollowPlayer__ctor_m47FC9E6A363317D93CA1D76BB5AAC2797E36EA6D (void);
// 0x0000036D System.Void _2D_BUNDLE.ColorJump.Scripts.GamePlayManager::Awake()
extern void GamePlayManager_Awake_mD0D181D4C6323051493FB268E8A5B83F9275D025 (void);
// 0x0000036E System.Void _2D_BUNDLE.ColorJump.Scripts.GamePlayManager::GameOver()
extern void GamePlayManager_GameOver_mCD459E7B8B01AC658F6B5530B10D2AEEE0BA1DFA (void);
// 0x0000036F System.Void _2D_BUNDLE.ColorJump.Scripts.GamePlayManager::Restart()
extern void GamePlayManager_Restart_m0BD1DB26456D15416AAF729A0E0A2C086082F167 (void);
// 0x00000370 System.Void _2D_BUNDLE.ColorJump.Scripts.GamePlayManager::Update()
extern void GamePlayManager_Update_m32B0A45E78BD98F857B2A88E2294C9F68A702B0E (void);
// 0x00000371 System.Void _2D_BUNDLE.ColorJump.Scripts.GamePlayManager::.ctor()
extern void GamePlayManager__ctor_m404F484E4CBF8222C74026BAE6A8350F522520A0 (void);
// 0x00000372 System.Void _2D_BUNDLE.ColorJump.Scripts.Player::Awake()
extern void Player_Awake_mF11684C07780DA36501C23E2EEE88039886D26AB (void);
// 0x00000373 System.Void _2D_BUNDLE.ColorJump.Scripts.Player::Update()
extern void Player_Update_mDE6038D6317FDBF40E7615B729C4AA3BAC1A9B3F (void);
// 0x00000374 System.Void _2D_BUNDLE.ColorJump.Scripts.Player::st()
extern void Player_st_mBCA9F98A9E66C689CA76B9C4625CA91F022D610E (void);
// 0x00000375 System.Void _2D_BUNDLE.ColorJump.Scripts.Player::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Player_OnTriggerEnter2D_mEEFB6D36328A00799783DFB21752ED75BC6AE1ED (void);
// 0x00000376 System.Void _2D_BUNDLE.ColorJump.Scripts.Player::su()
extern void Player_su_mBA405262631EBCABF09F118D21444B3AC4A09E30 (void);
// 0x00000377 System.Void _2D_BUNDLE.ColorJump.Scripts.Player::sv(UnityEngine.Collider2D)
extern void Player_sv_m9363643DD8A4B444F8F41E839231CB242E5ABA9D (void);
// 0x00000378 System.Void _2D_BUNDLE.ColorJump.Scripts.Player::sw(UnityEngine.Collider2D)
extern void Player_sw_mD96938EE65DC4560DAD9EAEAFFD2CC9AD0D2BA0B (void);
// 0x00000379 System.Void _2D_BUNDLE.ColorJump.Scripts.Player::sx(UnityEngine.Collider2D)
extern void Player_sx_m72814A85321189DF7FCA5F2612F34EB830A9BFAE (void);
// 0x0000037A System.Void _2D_BUNDLE.ColorJump.Scripts.Player::sy()
extern void Player_sy_m71C014C5A76A686009648DFA0C47646712FFCA9A (void);
// 0x0000037B System.Void _2D_BUNDLE.ColorJump.Scripts.Player::.ctor()
extern void Player__ctor_m9622A59F819995B09A5F9FF94F92083D311C8EA0 (void);
// 0x0000037C System.Void _2D_BUNDLE.ColorJump.Scripts.ScoreManager::Awake()
extern void ScoreManager_Awake_mD4C4EF88A4E207881815B36C012E315A0DE4C6D1 (void);
// 0x0000037D System.Void _2D_BUNDLE.ColorJump.Scripts.ScoreManager::AddScore(System.Int32)
extern void ScoreManager_AddScore_mE0AEC824A218966110ED131F108B219F60994A09 (void);
// 0x0000037E System.Void _2D_BUNDLE.ColorJump.Scripts.ScoreManager::.ctor()
extern void ScoreManager__ctor_mADF0093B4268C7DF3AC33940B431D611E7F11576 (void);
// 0x0000037F System.Void _2D_BUNDLE.ColorJump.Scripts.Step::Start()
extern void Step_Start_m7232B02248375DA1CAA12F71579E83B61A75052E (void);
// 0x00000380 System.Void _2D_BUNDLE.ColorJump.Scripts.Step::Update()
extern void Step_Update_m73AFA1DF3BFA53A8F3BA4A5EBC945D98D3F23316 (void);
// 0x00000381 System.Void _2D_BUNDLE.ColorJump.Scripts.Step::sz()
extern void Step_sz_m1256430A6DBBC9E339E7CA6E283AFCC206E21B20 (void);
// 0x00000382 System.Void _2D_BUNDLE.ColorJump.Scripts.Step::.ctor()
extern void Step__ctor_m41211B84D1A20C3E771DBD8502C7BE465FD76163 (void);
// 0x00000383 System.Void _2D_BUNDLE.ColorJump.Scripts.StepManager::Awake()
extern void StepManager_Awake_mC6891B1C0065C2A0D2FDD74F2068415708491D1B (void);
// 0x00000384 System.Void _2D_BUNDLE.ColorJump.Scripts.StepManager::Start()
extern void StepManager_Start_m95E00D52CAAFD9615B9D72D2711A1D9EE5EA05FF (void);
// 0x00000385 System.Void _2D_BUNDLE.ColorJump.Scripts.StepManager::ta()
extern void StepManager_ta_mA47F8EC84DCE407C111A11D3290EE28230118887 (void);
// 0x00000386 System.Void _2D_BUNDLE.ColorJump.Scripts.StepManager::MakeNewStep()
extern void StepManager_MakeNewStep_mFB8158B5306DB1C2825D2B022E3BE063EDC77A09 (void);
// 0x00000387 System.Void _2D_BUNDLE.ColorJump.Scripts.StepManager::tb(System.Single)
extern void StepManager_tb_m0F755C3307D3DE3C8FA8B603D4D9939E29C97C37 (void);
// 0x00000388 System.Void _2D_BUNDLE.ColorJump.Scripts.StepManager::tc(System.Single)
extern void StepManager_tc_mD0938B21DACDB27783FF30EC86FD27F976AA9775 (void);
// 0x00000389 System.Void _2D_BUNDLE.ColorJump.Scripts.StepManager::.ctor()
extern void StepManager__ctor_mEE66D60797301BE8442D5E3EED28FCDD078782A2 (void);
// 0x0000038A TheChase.SceneLoadingManager TheChase.SceneLoadingManager::get_Instance()
extern void SceneLoadingManager_get_Instance_mC629F875CD419A5CFD47E0D43181CF3D64B64BBC (void);
// 0x0000038B System.Void TheChase.SceneLoadingManager::set_Instance(TheChase.SceneLoadingManager)
extern void SceneLoadingManager_set_Instance_mA8CB476EF6410520B1037C6DA267508F7435373F (void);
// 0x0000038C System.Void TheChase.SceneLoadingManager::Awake()
extern void SceneLoadingManager_Awake_mC332A1B912C8CCD4E7A416AE952BED6E7AAC7AD2 (void);
// 0x0000038D System.Void TheChase.SceneLoadingManager::Start()
extern void SceneLoadingManager_Start_m931133546C6785E7EBB718729147DFAF3E7B39F8 (void);
// 0x0000038E System.Collections.IEnumerator TheChase.SceneLoadingManager::tl(UnityEngine.Vector3,System.Single)
extern void SceneLoadingManager_tl_m2A879C63FC69DE454472B19FE6EDEF6A5E2735CD (void);
// 0x0000038F System.Collections.IEnumerator TheChase.SceneLoadingManager::tm(System.String)
extern void SceneLoadingManager_tm_m2A84F8E381AA125F5788B084F8D28BE3660F3A9C (void);
// 0x00000390 System.Void TheChase.SceneLoadingManager::LoadGameScene()
extern void SceneLoadingManager_LoadGameScene_mCD738F5BBFEFCDC2827D9FFB4B4ABDD6C1BFA06C (void);
// 0x00000391 System.Void TheChase.SceneLoadingManager::LoadInitScene()
extern void SceneLoadingManager_LoadInitScene_m3663C940F4464F6A4C7BE22D3D5A84021124F707 (void);
// 0x00000392 System.Void TheChase.SceneLoadingManager::.ctor()
extern void SceneLoadingManager__ctor_m4845613B2470610E7B8126E7AE41B07A105FE6BA (void);
// 0x00000393 System.Void TheChase.SceneLoadingManager/bw::.ctor(System.Int32)
extern void bw__ctor_m688E71FD9FA8B1F9A4EDF83FBEA3440751586F95 (void);
// 0x00000394 System.Void TheChase.SceneLoadingManager/bw::td()
extern void bw_td_m6BDD846ED383CA1ECB9B9982551F4AC70B3885CB (void);
// 0x00000395 System.Boolean TheChase.SceneLoadingManager/bw::MoveNext()
extern void bw_MoveNext_m45C877637E256C6007DEBA096237D4488303640F (void);
// 0x00000396 System.Object TheChase.SceneLoadingManager/bw::te()
extern void bw_te_mA733E58FB131E100FFB27B41CD2C9116AD35D2CF (void);
// 0x00000397 System.Void TheChase.SceneLoadingManager/bw::tf()
extern void bw_tf_m6D86C1EA1D3533CB0AE8BF05FFC4470FE66137E2 (void);
// 0x00000398 System.Object TheChase.SceneLoadingManager/bw::tg()
extern void bw_tg_m8CDDF88FAD26A5D952EB4EDAE907241E83BCDB9D (void);
// 0x00000399 System.Void TheChase.SceneLoadingManager/bx::.ctor(System.Int32)
extern void bx__ctor_m99F001AEA6EE6F5EF85B6612B8F73139F06A5754 (void);
// 0x0000039A System.Void TheChase.SceneLoadingManager/bx::th()
extern void bx_th_m65577B103532FB1E6880E44575EA8ACB2BB256B3 (void);
// 0x0000039B System.Boolean TheChase.SceneLoadingManager/bx::MoveNext()
extern void bx_MoveNext_mEC5B7C92A984499EDC0B37B9C27B28BEF4A9E631 (void);
// 0x0000039C System.Object TheChase.SceneLoadingManager/bx::ti()
extern void bx_ti_m4EA35E51E73AD56A112AFFEFFB93EA7223E07E32 (void);
// 0x0000039D System.Void TheChase.SceneLoadingManager/bx::tj()
extern void bx_tj_mF21D5AB25AF46073BCB09F79C402DEAAB47972EC (void);
// 0x0000039E System.Object TheChase.SceneLoadingManager/bx::tk()
extern void bx_tk_mEB4FD4B32A292C8DE78B592FB30BCE79D0332A80 (void);
// 0x0000039F TheChase.MainMenuUi TheChase.MainMenuUi::get_Instance()
extern void MainMenuUi_get_Instance_mEA374B5EB3D68D62A42E6D8A5620CB27DAF169FA (void);
// 0x000003A0 System.Void TheChase.MainMenuUi::set_Instance(TheChase.MainMenuUi)
extern void MainMenuUi_set_Instance_mD61263C4AED21ABB92A62AA1E933A079F604432D (void);
// 0x000003A1 System.Void TheChase.MainMenuUi::Awake()
extern void MainMenuUi_Awake_m39097B9AE40FB9FA63ACA1B1E3ED5691657D0576 (void);
// 0x000003A2 System.Void TheChase.MainMenuUi::OnDestroy()
extern void MainMenuUi_OnDestroy_m5BE3AB5EE3AFE6F20BC708630CA7CD1C7FF9F7F1 (void);
// 0x000003A3 System.Void TheChase.MainMenuUi::Start()
extern void MainMenuUi_Start_mED6520042AE0226330F9A68F164DCF2E5FD30EDF (void);
// 0x000003A4 System.Void TheChase.MainMenuUi::tn()
extern void MainMenuUi_tn_m5B31D7BB64891992D8E46D2D98E04EA83B0CAABE (void);
// 0x000003A5 System.Void TheChase.MainMenuUi::tp()
extern void MainMenuUi_tp_m9A2570A90F2B5D97BCFB2A16D4F397B9511614D5 (void);
// 0x000003A6 System.Void TheChase.MainMenuUi::SetSelectedCarObject(UnityEngine.GameObject)
extern void MainMenuUi_SetSelectedCarObject_m93E60E533BAD50107390B36163987BF24EA3EDF7 (void);
// 0x000003A7 System.Void TheChase.MainMenuUi::SetSelectedMapObject(System.Int32,System.Int32)
extern void MainMenuUi_SetSelectedMapObject_m6FA566440FE04467927C458D5D9A3BD77924CE95 (void);
// 0x000003A8 System.Void TheChase.MainMenuUi::TestLoad()
extern void MainMenuUi_TestLoad_m396C06805349F1D5B3560F109ECD1BFB65F44413 (void);
// 0x000003A9 System.Void TheChase.MainMenuUi::LoadGameScene()
extern void MainMenuUi_LoadGameScene_m97421B6389E5A1446938CDAD39B5D46E91E60C6D (void);
// 0x000003AA System.Void TheChase.MainMenuUi::ShowSettingsPanel()
extern void MainMenuUi_ShowSettingsPanel_mEA7270F28B793F47E78FAE7DAFDD0D29ED7A5BEF (void);
// 0x000003AB System.Void TheChase.MainMenuUi::.ctor()
extern void MainMenuUi__ctor_m6CA05116A2D7FDF37893FFCE4BF35EAC1BCC3D21 (void);
// 0x000003AC System.Void TheChase.ResetDialog::Awake()
extern void ResetDialog_Awake_m8E57997616E2BAC85244041D1A5DD55268E56D6C (void);
// 0x000003AD System.Void TheChase.ResetDialog::Start()
extern void ResetDialog_Start_mDBB973F8CE6ED279F02A05C855498184A0E29859 (void);
// 0x000003AE System.Void TheChase.ResetDialog::tq()
extern void ResetDialog_tq_m4981BF336835534C3E93ED033E66B303D4630D3A (void);
// 0x000003AF System.Void TheChase.ResetDialog::tr()
extern void ResetDialog_tr_m8A3BA9CB5EC9194D895A7A92FB78086633CA1A48 (void);
// 0x000003B0 System.Void TheChase.ResetDialog::.ctor()
extern void ResetDialog__ctor_mE8F2835200BDBD2717A5817E8E10211ED79BA8E1 (void);
// 0x000003B1 System.Void TheChase.SelectionPanel::OnEnable()
extern void SelectionPanel_OnEnable_m1DADB51292717411554EB9147128FC7AF45C4386 (void);
// 0x000003B2 System.Void TheChase.SelectionPanel::OnDisable()
extern void SelectionPanel_OnDisable_mBA04B4869FDA4362807F2930F90EE498E81B021D (void);
// 0x000003B3 System.Void TheChase.SelectionPanel::Start()
extern void SelectionPanel_Start_m5FA94B0C9F7904E415164503837C9A81E1291180 (void);
// 0x000003B4 System.Void TheChase.SelectionPanel::ts()
extern void SelectionPanel_ts_m5D48AE0AD2B1B0C3D716FFE556DFD7C862BB8169 (void);
// 0x000003B5 System.Void TheChase.SelectionPanel::tt()
extern void SelectionPanel_tt_m73CBE6A4F492C1CCC06BE0467759C497D6DC4273 (void);
// 0x000003B6 System.Void TheChase.SelectionPanel::tu(hardartcore.TheChase.Scripts.ScriptableObjects.CarScriptableObject)
extern void SelectionPanel_tu_m3D04ACA7E860AFB77B85557849B49C105B174D90 (void);
// 0x000003B7 System.Void TheChase.SelectionPanel::tv(hardartcore.TheChase.Scripts.ScriptableObjects.CarScriptableObject)
extern void SelectionPanel_tv_m167F521A1E75BDDCD6A232F00B900472EAE1E638 (void);
// 0x000003B8 System.Void TheChase.SelectionPanel::tw(System.Int32)
extern void SelectionPanel_tw_mB665DF4CACBEC764CEEC675130D675377414616A (void);
// 0x000003B9 System.Int32 TheChase.SelectionPanel::tx()
extern void SelectionPanel_tx_m4F2330EA28388147544A87955E250018E09585C7 (void);
// 0x000003BA System.Void TheChase.SelectionPanel::ty(System.Int32)
extern void SelectionPanel_ty_m533739C84370B95BBC126695C01B8B2ECCE444B3 (void);
// 0x000003BB System.Void TheChase.SelectionPanel::tz()
extern void SelectionPanel_tz_mFA1419A28459BC10829A4970128F473582134812 (void);
// 0x000003BC System.Void TheChase.SelectionPanel::ua()
extern void SelectionPanel_ua_mD4259589A894DEB29D0F7C295153126ADF911AC1 (void);
// 0x000003BD System.Void TheChase.SelectionPanel::ShowCarSelectionPanel()
extern void SelectionPanel_ShowCarSelectionPanel_mBA37E10E2D7E3D16C60526B5F95E13BE4EE15775 (void);
// 0x000003BE System.Void TheChase.SelectionPanel::ShowMapSelectionPanel()
extern void SelectionPanel_ShowMapSelectionPanel_mED5DC672DD6175C24DD49AD9CDAF5E9951E530BD (void);
// 0x000003BF System.Void TheChase.SelectionPanel::.ctor()
extern void SelectionPanel__ctor_mDBAA94C3B8A273C5AEBEC51949CBA28E682DA340 (void);
// 0x000003C0 System.Void TheChase.SelectionPanel::ub()
extern void SelectionPanel_ub_m309919094F5CA07F1D6989A95E2BB5E0209DBE0B (void);
// 0x000003C1 System.Void TheChase.SelectionPanel::uc()
extern void SelectionPanel_uc_m71A535C5B4B469D3D0FDEA886839B3216BA1DAF0 (void);
// 0x000003C2 System.Void TheChase.SettingsDialog::Awake()
extern void SettingsDialog_Awake_m6D770B068CC33AC48C9AC31D57ACFC5459A45F00 (void);
// 0x000003C3 System.Void TheChase.SettingsDialog::Start()
extern void SettingsDialog_Start_m82D7AA1FFB7E37F23F224F01A569032C02F051DD (void);
// 0x000003C4 System.Void TheChase.SettingsDialog::ud()
extern void SettingsDialog_ud_mAAB7AA7CF6E3F24C7F0685083BE6FB02297E4766 (void);
// 0x000003C5 System.Void TheChase.SettingsDialog::ue()
extern void SettingsDialog_ue_m8D6BFB5D5F0A5D9CEB3F5D2FB7A94C474097ABA5 (void);
// 0x000003C6 System.Void TheChase.SettingsDialog::uf()
extern void SettingsDialog_uf_m00E534A6E858036DA05F1902E3EF2DD6FE9178F3 (void);
// 0x000003C7 System.Void TheChase.SettingsDialog::.ctor()
extern void SettingsDialog__ctor_mFD1502511486C4ABD0710ACD212BB866B8DC28CB (void);
// 0x000003C8 System.Collections.IEnumerator hardartcore.TheChase.Scripts.Utils.CoroutineUtils::ChangePositionOverTime(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void CoroutineUtils_ChangePositionOverTime_mE6C80C7FE1D540C9903782617333D762A349D985 (void);
// 0x000003C9 System.Collections.IEnumerator hardartcore.TheChase.Scripts.Utils.CoroutineUtils::ScaleObjectOverTime(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void CoroutineUtils_ScaleObjectOverTime_mA76626931E9F66594971753FBDD5F4DA44976D48 (void);
// 0x000003CA System.Collections.IEnumerator hardartcore.TheChase.Scripts.Utils.CoroutineUtils::FadeCanvasColorOverTime(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void CoroutineUtils_FadeCanvasColorOverTime_m54C2CC0BB193727D3A32D7304BED0E171081C21C (void);
// 0x000003CB System.Void hardartcore.TheChase.Scripts.Utils.CoroutineUtils/bz::.ctor(System.Int32)
extern void bz__ctor_mA3CDF975209598BC497F0ABF4195790FA8F221A8 (void);
// 0x000003CC System.Void hardartcore.TheChase.Scripts.Utils.CoroutineUtils/bz::ug()
extern void bz_ug_mA25EC5FC10D0465E8CF45FBE3BAAAC28D7F3F053 (void);
// 0x000003CD System.Boolean hardartcore.TheChase.Scripts.Utils.CoroutineUtils/bz::MoveNext()
extern void bz_MoveNext_m0F6BA152CC254365602EC698EA5FA54479AE6663 (void);
// 0x000003CE System.Object hardartcore.TheChase.Scripts.Utils.CoroutineUtils/bz::uh()
extern void bz_uh_m88301D9E2CE573F55566BED251BA4E4EA7EFBCFC (void);
// 0x000003CF System.Void hardartcore.TheChase.Scripts.Utils.CoroutineUtils/bz::ui()
extern void bz_ui_m06E6C61965420B79610A25FF221E7CB4B7598447 (void);
// 0x000003D0 System.Object hardartcore.TheChase.Scripts.Utils.CoroutineUtils/bz::uj()
extern void bz_uj_mB79BB94B078D3B65D86A003D69D97EAEE14BE5E1 (void);
// 0x000003D1 System.Void hardartcore.TheChase.Scripts.Utils.CoroutineUtils/ca::.ctor(System.Int32)
extern void ca__ctor_m4CFC9C8B890604267A0F420A0943D8A009E9A83B (void);
// 0x000003D2 System.Void hardartcore.TheChase.Scripts.Utils.CoroutineUtils/ca::uk()
extern void ca_uk_m9664649736BF613E0CD9C8442BDD2306B3A1ECF6 (void);
// 0x000003D3 System.Boolean hardartcore.TheChase.Scripts.Utils.CoroutineUtils/ca::MoveNext()
extern void ca_MoveNext_m4776DA9D5C488FE04B5CF51E8CF21E2DA0C90D5C (void);
// 0x000003D4 System.Object hardartcore.TheChase.Scripts.Utils.CoroutineUtils/ca::ul()
extern void ca_ul_m08DE9F724D53860E7699F9B35050DBC2B2E3A069 (void);
// 0x000003D5 System.Void hardartcore.TheChase.Scripts.Utils.CoroutineUtils/ca::um()
extern void ca_um_m8D447ECBF1936521D5A8BDDAF02D378ABDC7C36C (void);
// 0x000003D6 System.Object hardartcore.TheChase.Scripts.Utils.CoroutineUtils/ca::un()
extern void ca_un_m212E184A3190811BAA3F2A2625DF2BBDFE01A2EC (void);
// 0x000003D7 System.Void hardartcore.TheChase.Scripts.Utils.CoroutineUtils/cb::.ctor(System.Int32)
extern void cb__ctor_m29CAA25131E216109D2F3766D55B62D35E6683BC (void);
// 0x000003D8 System.Void hardartcore.TheChase.Scripts.Utils.CoroutineUtils/cb::uo()
extern void cb_uo_mF9DD4F609B6E6482B25B2EF7AB86D5FED0A33B4C (void);
// 0x000003D9 System.Boolean hardartcore.TheChase.Scripts.Utils.CoroutineUtils/cb::MoveNext()
extern void cb_MoveNext_m2D9ED935744A82575280D9368CFC20F1E85B7485 (void);
// 0x000003DA System.Object hardartcore.TheChase.Scripts.Utils.CoroutineUtils/cb::up()
extern void cb_up_m2B75F060CC72AAC2C2D80051580AE3546847209A (void);
// 0x000003DB System.Void hardartcore.TheChase.Scripts.Utils.CoroutineUtils/cb::uq()
extern void cb_uq_mCFD88E553E715E7B9E833D2139455BA0508008F8 (void);
// 0x000003DC System.Object hardartcore.TheChase.Scripts.Utils.CoroutineUtils/cb::ur()
extern void cb_ur_m58CF9D59F0B8542C5DBB66F87C1ADB90C3A3E307 (void);
// 0x000003DD System.Void hardartcore.TheChase.Scripts.Utils.DestroyAfter::OnEnable()
extern void DestroyAfter_OnEnable_mC5D9A259CC5555D80F61C72B5A62E6AD1E4C3CFE (void);
// 0x000003DE System.Void hardartcore.TheChase.Scripts.Utils.DestroyAfter::us()
extern void DestroyAfter_us_m0BB83E1F5D4C9CBC63D12BBE48371439754CBB60 (void);
// 0x000003DF System.Void hardartcore.TheChase.Scripts.Utils.DestroyAfter::.ctor()
extern void DestroyAfter__ctor_mBD07788D70039FA205C6A3BD56470F986C5DCC02 (void);
// 0x000003E0 System.Void hardartcore.TheChase.Scripts.Utils.DisableAfter::OnEnable()
extern void DisableAfter_OnEnable_mA392679601029CE731A90C2AEC85E783DA3ED5D4 (void);
// 0x000003E1 System.Void hardartcore.TheChase.Scripts.Utils.DisableAfter::ux()
extern void DisableAfter_ux_m4BFBF38D95219356AF9D57007832C67EE606A1DE (void);
// 0x000003E2 System.Collections.IEnumerator hardartcore.TheChase.Scripts.Utils.DisableAfter::uy()
extern void DisableAfter_uy_mAAADDA972F76EE5B81F9FA3FF0EA3D0C1CC2542C (void);
// 0x000003E3 System.Void hardartcore.TheChase.Scripts.Utils.DisableAfter::.ctor()
extern void DisableAfter__ctor_m89718CD70AE045BB8FDCD3E1D1BF162A1D28BA5C (void);
// 0x000003E4 System.Void hardartcore.TheChase.Scripts.Utils.DisableAfter/cc::.ctor(System.Int32)
extern void cc__ctor_m024616AADD716A21F8EAE9660E60B81BFB3BBAFB (void);
// 0x000003E5 System.Void hardartcore.TheChase.Scripts.Utils.DisableAfter/cc::ut()
extern void cc_ut_m668D4C17694FC7A19C12508ED3A8291D675AB6D3 (void);
// 0x000003E6 System.Boolean hardartcore.TheChase.Scripts.Utils.DisableAfter/cc::MoveNext()
extern void cc_MoveNext_m4A530A52B20C65C7885FD9C65C92F705B1385CEE (void);
// 0x000003E7 System.Object hardartcore.TheChase.Scripts.Utils.DisableAfter/cc::uu()
extern void cc_uu_m936E995C1BEE233C0365ED136686D850B88BA4AD (void);
// 0x000003E8 System.Void hardartcore.TheChase.Scripts.Utils.DisableAfter/cc::uv()
extern void cc_uv_m76F4C2F46995610E54EE41556B029E3C7009C860 (void);
// 0x000003E9 System.Object hardartcore.TheChase.Scripts.Utils.DisableAfter/cc::uw()
extern void cc_uw_mEE77A89D863E968165C65EECD512E4F8C62AE7C2 (void);
// 0x000003EA UnityEngine.Transform hardartcore.TheChase.Scripts.Utils.Extensions::Clear(UnityEngine.Transform)
extern void Extensions_Clear_mC2C03B0DC8FEBF2549CB798CCFACDF14C2CCCFA4 (void);
// 0x000003EB System.Void hardartcore.TheChase.Scripts.Utils.Extensions::SetActive(UnityEngine.UI.Button,System.Boolean)
extern void Extensions_SetActive_m9601B7134F09262A00D364BFD3F0BEA2313FDFC3 (void);
// 0x000003EC System.Boolean hardartcore.TheChase.Scripts.Utils.Extensions::IsCollidingWithPlayer(UnityEngine.Collider)
extern void Extensions_IsCollidingWithPlayer_m4E66148220B510DBFA7B4C465DA366B26CE44CAA (void);
// 0x000003ED System.Boolean hardartcore.TheChase.Scripts.Utils.Extensions::CompareTag(UnityEngine.Collider,System.String)
extern void Extensions_CompareTag_m8066D30BAF46C213161115499FFE735E8F908591 (void);
// 0x000003EE System.Boolean hardartcore.TheChase.Scripts.Utils.Extensions::CompareTag(UnityEngine.Collision,System.String)
extern void Extensions_CompareTag_m8BD0E526513A1A248A6D3BB5B969F1E945B4887A (void);
// 0x000003EF System.Void hardartcore.TheChase.Scripts.Utils.Extensions::Reset(UnityEngine.Rigidbody)
extern void Extensions_Reset_m78C0D911D9C1CBFB300E4E0E6C1C5A8E8AF0522D (void);
// 0x000003F0 UnityEngine.Vector3 hardartcore.TheChase.Scripts.Utils.Extensions::With(UnityEngine.Vector3,System.Nullable`1<System.Single>,System.Nullable`1<System.Single>,System.Nullable`1<System.Single>)
extern void Extensions_With_mD86BB64814E7D7193E74965DE97A4977FA367366 (void);
// 0x000003F1 UnityEngine.Quaternion hardartcore.TheChase.Scripts.Utils.Extensions::With(UnityEngine.Quaternion,System.Nullable`1<System.Single>,System.Nullable`1<System.Single>,System.Nullable`1<System.Single>)
extern void Extensions_With_mE957B67B975ACF3C2A51432B17CAABA2B4C3693D (void);
// 0x000003F2 System.Boolean hardartcore.TheChase.Scripts.Utils.Extensions::IsVisibleFrom(UnityEngine.Renderer,UnityEngine.Camera)
extern void Extensions_IsVisibleFrom_m8ACD272CE7215E96F85ADA744C7525F86F4120B0 (void);
// 0x000003F3 System.Void hardartcore.TheChase.Scripts.UI.AnimatedButton::Awake()
extern void AnimatedButton_Awake_m4E2F723B4237823C0A60D357A5DF4942C66B73A9 (void);
// 0x000003F4 System.Void hardartcore.TheChase.Scripts.UI.AnimatedButton::Start()
extern void AnimatedButton_Start_mCE1CB9B4FD668038F2C72AE76B713B50F3296363 (void);
// 0x000003F5 System.Void hardartcore.TheChase.Scripts.UI.AnimatedButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void AnimatedButton_OnPointerDown_m3D456C20D75506D88BBBBD06F0916F84E4C1EF56 (void);
// 0x000003F6 System.Void hardartcore.TheChase.Scripts.UI.AnimatedButton::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void AnimatedButton_OnPointerEnter_m6266A26411B03170EAFCB95FA1BD270D9752C1F8 (void);
// 0x000003F7 System.Void hardartcore.TheChase.Scripts.UI.AnimatedButton::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void AnimatedButton_OnPointerExit_m83E61A5D507EF08468B356257ECACC2F44146BA8 (void);
// 0x000003F8 System.Void hardartcore.TheChase.Scripts.UI.AnimatedButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void AnimatedButton_OnPointerUp_m46BBB03A94FCC162D538FB9EAF9BB04E0929BB7E (void);
// 0x000003F9 System.Void hardartcore.TheChase.Scripts.UI.AnimatedButton::uz(UnityEngine.EventSystems.BaseEventData)
extern void AnimatedButton_uz_m45FBA4A93BC49110C02435D09000232B54A7829F (void);
// 0x000003FA System.Void hardartcore.TheChase.Scripts.UI.AnimatedButton::va(UnityEngine.EventSystems.BaseEventData)
extern void AnimatedButton_va_m58FFC1BDAFF4278F7FFD19BBA8FA6858E7B952B1 (void);
// 0x000003FB System.Void hardartcore.TheChase.Scripts.UI.AnimatedButton::vb()
extern void AnimatedButton_vb_mCBD0EB7E406664D3061E066AE79B13F37CC6613F (void);
// 0x000003FC System.Void hardartcore.TheChase.Scripts.UI.AnimatedButton::vc(System.Boolean)
extern void AnimatedButton_vc_m2447352D9FE22BBF77C7D51F8601EDBDCEE4652F (void);
// 0x000003FD System.Void hardartcore.TheChase.Scripts.UI.AnimatedButton::SetInteractable(System.Boolean)
extern void AnimatedButton_SetInteractable_mE3A7E9459260C9CCFEF1F1D1EAEBC4B5024A28BF (void);
// 0x000003FE System.Void hardartcore.TheChase.Scripts.UI.AnimatedButton::.ctor()
extern void AnimatedButton__ctor_m4412A85585ABF3C66EA6001A3F36702F797709A6 (void);
// 0x000003FF System.Void hardartcore.TheChase.Scripts.UI.AnimatedButton::vd(UnityEngine.EventSystems.BaseEventData)
extern void AnimatedButton_vd_m747611B7F6FE10F6F794BE50E97FB79D2658E648 (void);
// 0x00000400 System.Void hardartcore.TheChase.Scripts.UI.AnimatedButton::ve(UnityEngine.EventSystems.BaseEventData)
extern void AnimatedButton_ve_m07FC85300A619AAB6ED6D3CD309606DC8A6EE998 (void);
// 0x00000401 System.Void hardartcore.TheChase.Scripts.UI.AnimatedButton::vf(UnityEngine.EventSystems.BaseEventData)
extern void AnimatedButton_vf_m0C6B53CE37011B570C340D488F2CF111A3298D71 (void);
// 0x00000402 System.Void hardartcore.TheChase.Scripts.UI.AnimatedButton::vg(UnityEngine.EventSystems.BaseEventData)
extern void AnimatedButton_vg_mA57EC9E6A844BF92838F8294A920E0168EBD727F (void);
// 0x00000403 System.Void hardartcore.TheChase.Scripts.UI.BombsCount::Awake()
extern void BombsCount_Awake_m27B63C209DF4F27B5BE424C7E9D738F27BE81BD9 (void);
// 0x00000404 System.Void hardartcore.TheChase.Scripts.UI.BombsCount::OnEnable()
extern void BombsCount_OnEnable_m53991A03D4381D8723B059BBB9E7C63DA9EE8FBD (void);
// 0x00000405 System.Void hardartcore.TheChase.Scripts.UI.BombsCount::OnDisable()
extern void BombsCount_OnDisable_m884B640889C4D6E20846A7948013826574FBE466 (void);
// 0x00000406 System.Void hardartcore.TheChase.Scripts.UI.BombsCount::vh(System.Int32)
extern void BombsCount_vh_mF7B0ECA67F56AF5075E077165D3E95AC95088B4E (void);
// 0x00000407 System.Void hardartcore.TheChase.Scripts.UI.BombsCount::.ctor()
extern void BombsCount__ctor_m682C153D14F6F0F782B21E704C92C00A293BAEC0 (void);
// 0x00000408 System.Void hardartcore.TheChase.Scripts.UI.CoinsText::Awake()
extern void CoinsText_Awake_m59B0A0B5C51DAFB73D0F4F3444D01A0CCFDD47A5 (void);
// 0x00000409 System.Void hardartcore.TheChase.Scripts.UI.CoinsText::Start()
extern void CoinsText_Start_mB2FFC5FAE8E2E91BCAAF5E93ADF185F3BE6E7109 (void);
// 0x0000040A System.Void hardartcore.TheChase.Scripts.UI.CoinsText::OnEnable()
extern void CoinsText_OnEnable_mBA0DE21006D49627FCE117BCB47714F5F717DB3B (void);
// 0x0000040B System.Void hardartcore.TheChase.Scripts.UI.CoinsText::OnDisable()
extern void CoinsText_OnDisable_mA9E51994C62CC29B1B36E6213A90590CDC482ADD (void);
// 0x0000040C System.Void hardartcore.TheChase.Scripts.UI.CoinsText::vi(System.Int32,System.Int32)
extern void CoinsText_vi_m4ED3282D49C738236435650A630435461FE23FD0 (void);
// 0x0000040D System.Void hardartcore.TheChase.Scripts.UI.CoinsText::.ctor()
extern void CoinsText__ctor_m43F84CF6FE84D974A8EA9AD8C357DC000C5BE120 (void);
// 0x0000040E System.Void hardartcore.TheChase.Scripts.UI.Counter::OnEnable()
extern void Counter_OnEnable_mFFE5EFB40149AE9FFEACE8F1A287C690C72F230E (void);
// 0x0000040F System.Void hardartcore.TheChase.Scripts.UI.Counter::OnDisable()
extern void Counter_OnDisable_m095FEE4B2C9B38020A25D0EED84DA97001A3E463 (void);
// 0x00000410 System.Void hardartcore.TheChase.Scripts.UI.Counter::Start()
extern void Counter_Start_mC6BD83AE11EC13FC97896993522C7667B38F0371 (void);
// 0x00000411 System.Void hardartcore.TheChase.Scripts.UI.Counter::vj()
extern void Counter_vj_m0DE4631646399BAC24A0EFF8E85F269A9F04B24E (void);
// 0x00000412 System.Void hardartcore.TheChase.Scripts.UI.Counter::vk()
extern void Counter_vk_m23F3B17E109CF2B154BB0FD1F2720C4979CE9D65 (void);
// 0x00000413 System.Void hardartcore.TheChase.Scripts.UI.Counter::StartCounter()
extern void Counter_StartCounter_m598F53A6D0A01363AB6AA33D5442A4F131CEE05F (void);
// 0x00000414 System.String hardartcore.TheChase.Scripts.UI.Counter::vl()
extern void Counter_vl_mD51B8EB80F299B87E4F41DEF4C08F36C4A51212C (void);
// 0x00000415 System.Void hardartcore.TheChase.Scripts.UI.Counter::vm()
extern void Counter_vm_mA3B394DB684261AB1CE694B0DF5C329F3F8A14FD (void);
// 0x00000416 System.Void hardartcore.TheChase.Scripts.UI.Counter::PauseCounter()
extern void Counter_PauseCounter_mC194FCED35718D484A8A3D03CE8ADFF6775F7195 (void);
// 0x00000417 System.Void hardartcore.TheChase.Scripts.UI.Counter::ResumeCounter()
extern void Counter_ResumeCounter_m0D4D2F93072AF08D845C4FBC466D376AC08DB055 (void);
// 0x00000418 System.Void hardartcore.TheChase.Scripts.UI.Counter::.ctor()
extern void Counter__ctor_m637C9D5FA3D4A621C198FDD4521970B6C1A7D5D1 (void);
// 0x00000419 System.Boolean hardartcore.TheChase.Scripts.UI.Dialog::get_Shown()
extern void Dialog_get_Shown_mEDEF53FC6AAC28CB734D4D95F6A3E8DE6522761D (void);
// 0x0000041A System.Void hardartcore.TheChase.Scripts.UI.Dialog::set_Shown(System.Boolean)
extern void Dialog_set_Shown_m01177B5FE5943361B77CEC5EC188A7F94ECC20D9 (void);
// 0x0000041B System.Collections.IEnumerator hardartcore.TheChase.Scripts.UI.Dialog::vr()
extern void Dialog_vr_mCE03DAFB7F7A5DCD942BBB6C12E753D650DA10BB (void);
// 0x0000041C System.Void hardartcore.TheChase.Scripts.UI.Dialog::ShowDialog()
extern void Dialog_ShowDialog_m55C6B44EFDE4FCF65FF90DD877882122071357E5 (void);
// 0x0000041D System.Void hardartcore.TheChase.Scripts.UI.Dialog::HideDialog()
extern void Dialog_HideDialog_m0293351CCC7CC96F15D4D11843093FCE956210A3 (void);
// 0x0000041E System.Void hardartcore.TheChase.Scripts.UI.Dialog::.ctor()
extern void Dialog__ctor_m0398413233CDCC5536BDB4F0815926F40CAE5C7C (void);
// 0x0000041F System.Void hardartcore.TheChase.Scripts.UI.Dialog/cd::.ctor(System.Int32)
extern void cd__ctor_mD26C03CB5E963FCE78372140411435F1767C8731 (void);
// 0x00000420 System.Void hardartcore.TheChase.Scripts.UI.Dialog/cd::vn()
extern void cd_vn_mBA9FB1B3B1EBCDAB5FA7FA0291CC20DFD219305B (void);
// 0x00000421 System.Boolean hardartcore.TheChase.Scripts.UI.Dialog/cd::MoveNext()
extern void cd_MoveNext_mA57287C3BAB0201DB9A89492E05C9A0D53EE1BC4 (void);
// 0x00000422 System.Object hardartcore.TheChase.Scripts.UI.Dialog/cd::vo()
extern void cd_vo_m10B1BCA7BCDCFCAC1D3BAB92F7338D49EFC03259 (void);
// 0x00000423 System.Void hardartcore.TheChase.Scripts.UI.Dialog/cd::vp()
extern void cd_vp_m07E2D604C1FBEA1E4E71C629F768638E704E6585 (void);
// 0x00000424 System.Object hardartcore.TheChase.Scripts.UI.Dialog/cd::vq()
extern void cd_vq_m4EB25EC6AD7A7601504B521A2386BE5185990F1F (void);
// 0x00000425 System.Void hardartcore.TheChase.Scripts.UI.FuelProgressBar::Awake()
extern void FuelProgressBar_Awake_mCDCA69EC2A4E2B70E9C135DEE6D1C707C296095A (void);
// 0x00000426 System.Void hardartcore.TheChase.Scripts.UI.FuelProgressBar::OnEnable()
extern void FuelProgressBar_OnEnable_mEE68B8DCBEE02F0A74566826EFC4CA8F54E5B2C8 (void);
// 0x00000427 System.Void hardartcore.TheChase.Scripts.UI.FuelProgressBar::OnDisable()
extern void FuelProgressBar_OnDisable_m29D89427491DAF2D6080618BEC33DA2CC4654336 (void);
// 0x00000428 System.Void hardartcore.TheChase.Scripts.UI.FuelProgressBar::vs(System.Single)
extern void FuelProgressBar_vs_m7707394AB5D1166D3571A3C860DAB78A43485481 (void);
// 0x00000429 System.Void hardartcore.TheChase.Scripts.UI.FuelProgressBar::.ctor()
extern void FuelProgressBar__ctor_m57D47DB5006495320CF1EC42445DADD98C9BD0E0 (void);
// 0x0000042A System.Void hardartcore.TheChase.Scripts.UI.MagnetModeIndicator::Awake()
extern void MagnetModeIndicator_Awake_m15690B59489D71BB25EE85CF5E2CF843DC5571DF (void);
// 0x0000042B System.Void hardartcore.TheChase.Scripts.UI.MagnetModeIndicator::OnEnable()
extern void MagnetModeIndicator_OnEnable_m9AA8F54644B630CB145C02DCDA68DBC0DEB218BE (void);
// 0x0000042C System.Void hardartcore.TheChase.Scripts.UI.MagnetModeIndicator::OnDisable()
extern void MagnetModeIndicator_OnDisable_mA33022C3D4A15657742E83102E5AD27A9D54F832 (void);
// 0x0000042D System.Void hardartcore.TheChase.Scripts.UI.MagnetModeIndicator::OnDestroy()
extern void MagnetModeIndicator_OnDestroy_mACA4CA4D71BCE20B792EF9923CAA1197ADAEDA57 (void);
// 0x0000042E System.Void hardartcore.TheChase.Scripts.UI.MagnetModeIndicator::vx(System.Boolean)
extern void MagnetModeIndicator_vx_mF457A7EDA66E05A561E0237C23B29061FC0F2660 (void);
// 0x0000042F System.Collections.IEnumerator hardartcore.TheChase.Scripts.UI.MagnetModeIndicator::StartCounter()
extern void MagnetModeIndicator_StartCounter_mBC48ED292C92E08CB7E177DEEFE39C1FE702EE93 (void);
// 0x00000430 System.Void hardartcore.TheChase.Scripts.UI.MagnetModeIndicator::.ctor()
extern void MagnetModeIndicator__ctor_mF142EA273A5E5AFED9E4BAC4E9CE474FDC98FFAF (void);
// 0x00000431 System.Void hardartcore.TheChase.Scripts.UI.MagnetModeIndicator/ce::.ctor(System.Int32)
extern void ce__ctor_m9FC2934B19F17BE74A62E5E4AB15F01B57C74EC3 (void);
// 0x00000432 System.Void hardartcore.TheChase.Scripts.UI.MagnetModeIndicator/ce::vt()
extern void ce_vt_mBB08FB0C0E4B3645D291E81A262F4B14C969D247 (void);
// 0x00000433 System.Boolean hardartcore.TheChase.Scripts.UI.MagnetModeIndicator/ce::MoveNext()
extern void ce_MoveNext_mB664FD4971B0A7958AB3F82224AD59E9B974A37A (void);
// 0x00000434 System.Object hardartcore.TheChase.Scripts.UI.MagnetModeIndicator/ce::vu()
extern void ce_vu_m57C8C9D6BE0CC50785FC086EDAC7F263C3DBD4D8 (void);
// 0x00000435 System.Void hardartcore.TheChase.Scripts.UI.MagnetModeIndicator/ce::vv()
extern void ce_vv_m0807F912CF8BF83BC39AE4BABD3C307740D80F46 (void);
// 0x00000436 System.Object hardartcore.TheChase.Scripts.UI.MagnetModeIndicator/ce::vw()
extern void ce_vw_m03BF6CA94543FCB0C75C5D3E40ACBF09A24DED51 (void);
// 0x00000437 System.Void hardartcore.TheChase.Scripts.ScriptableObjects.CarScriptableObject::.ctor()
extern void CarScriptableObject__ctor_m2D7A4BFCE9458FB3262F090B7FBBAB735FF2268C (void);
// 0x00000438 System.Void hardartcore.TheChase.Scripts.ScriptableObjects.MapScriptableObject::.ctor()
extern void MapScriptableObject__ctor_m6FF5F943ABB16150F0C36D14DDF3309BF49F96E8 (void);
// 0x00000439 System.Void hardartcore.TheChase.Scripts.Objects.BaseBehaviour::Start()
extern void BaseBehaviour_Start_m58F33C1B7E33C71B952C33271C6AAEF3C116F043 (void);
// 0x0000043A System.Void hardartcore.TheChase.Scripts.Objects.BaseBehaviour::BaseUpdate()
// 0x0000043B System.Void hardartcore.TheChase.Scripts.Objects.BaseBehaviour::.ctor()
extern void BaseBehaviour__ctor_m2E07FE9AB983681359A621176E50EB9D19A68C01 (void);
// 0x0000043C System.Void hardartcore.TheChase.Scripts.Objects.BaseCollectable::OnEnable()
extern void BaseCollectable_OnEnable_m4DD2D2CA56F4426F143A692245A746F1BCE79212 (void);
// 0x0000043D System.Void hardartcore.TheChase.Scripts.Objects.BaseCollectable::OnTriggerEnter(UnityEngine.Collider)
extern void BaseCollectable_OnTriggerEnter_m67289A06F6ECCA8B27B6A592FDA0F47AB54763D4 (void);
// 0x0000043E System.Void hardartcore.TheChase.Scripts.Objects.BaseCollectable::BaseUpdate()
extern void BaseCollectable_BaseUpdate_m748D6AFF2B781313817DAB56CA1919D3EDA280D6 (void);
// 0x0000043F System.Void hardartcore.TheChase.Scripts.Objects.BaseCollectable::vy()
// 0x00000440 System.Void hardartcore.TheChase.Scripts.Objects.BaseCollectable::.ctor()
extern void BaseCollectable__ctor_m91EBD7179E8C8ED28567B8CEC93F3F1A36B4B77D (void);
// 0x00000441 System.Void hardartcore.TheChase.Scripts.Objects.Bomb::Awake()
extern void Bomb_Awake_m1B2DCD9BE33AF1CAA7463FC19408B3442475175D (void);
// 0x00000442 System.Void hardartcore.TheChase.Scripts.Objects.Bomb::OnEnable()
extern void Bomb_OnEnable_m4329A1FACE67CB62D18282994416757C641F8A8E (void);
// 0x00000443 System.Void hardartcore.TheChase.Scripts.Objects.Bomb::OnCollisionEnter(UnityEngine.Collision)
extern void Bomb_OnCollisionEnter_m0F2DF4E5B89B619DE1A58ED67AE3546D1CFB64A4 (void);
// 0x00000444 System.Void hardartcore.TheChase.Scripts.Objects.Bomb::EnableCollider()
extern void Bomb_EnableCollider_m283CB0D97FAFBDDECEE50D477DCD04DD809CA770 (void);
// 0x00000445 System.Void hardartcore.TheChase.Scripts.Objects.Bomb::.ctor()
extern void Bomb__ctor_m89AC6B18117F7DBB6BBC8C12817AF2BA44AD14CE (void);
// 0x00000446 System.Void hardartcore.TheChase.Scripts.Objects.Car::.ctor()
extern void Car__ctor_m16823A9C0BF4746ACC70FDB806FD40824C8D6C45 (void);
// 0x00000447 System.Void hardartcore.TheChase.Scripts.Objects.Coin::Awake()
extern void Coin_Awake_m77EBB5A9FB1D6C4D908A9FBA850000170D1FC7DD (void);
// 0x00000448 System.Void hardartcore.TheChase.Scripts.Objects.Coin::OnEnable()
extern void Coin_OnEnable_m661D39CD4820EA34BECFA8800D6446DC38F6BAAB (void);
// 0x00000449 System.Void hardartcore.TheChase.Scripts.Objects.Coin::OnDisable()
extern void Coin_OnDisable_m3EB3F561B9E78D6E75C9C280E4FDBE7067F5A6AF (void);
// 0x0000044A System.Void hardartcore.TheChase.Scripts.Objects.Coin::OnTriggerEnter(UnityEngine.Collider)
extern void Coin_OnTriggerEnter_m82E8B6501943CB61F8EB69033CA1C6C64CC68BC8 (void);
// 0x0000044B System.Void hardartcore.TheChase.Scripts.Objects.Coin::OnTriggerExit(UnityEngine.Collider)
extern void Coin_OnTriggerExit_m038ECF32C2EE563873A9C117E756803D0D418201 (void);
// 0x0000044C System.Void hardartcore.TheChase.Scripts.Objects.Coin::vy()
extern void Coin_vy_mA9021F00E37C9B61868343757E084AD934C29702 (void);
// 0x0000044D System.Void hardartcore.TheChase.Scripts.Objects.Coin::wd(System.Boolean)
extern void Coin_wd_mB291316468D1C1DACAEFD50D67DB076673065311 (void);
// 0x0000044E System.Void hardartcore.TheChase.Scripts.Objects.Coin::we()
extern void Coin_we_m5873A769E81F7603D2E8EA843D58EB9940B7E519 (void);
// 0x0000044F System.Collections.IEnumerator hardartcore.TheChase.Scripts.Objects.Coin::wf()
extern void Coin_wf_mE0F81F6B2A1E6E6FCF94C589855F372F481EED98 (void);
// 0x00000450 System.Void hardartcore.TheChase.Scripts.Objects.Coin::SetCurrentTile(hardartcore.TheChase.Scripts.Objects.Tile)
extern void Coin_SetCurrentTile_mCA4E990B24CF9B58EBFAC52EED12442258AB48F5 (void);
// 0x00000451 System.Void hardartcore.TheChase.Scripts.Objects.Coin::.ctor()
extern void Coin__ctor_m0FD6A5D75C4E45DBC44718A490659C0E8B731FA7 (void);
// 0x00000452 System.Void hardartcore.TheChase.Scripts.Objects.Coin/cf::.ctor(System.Int32)
extern void cf__ctor_mB08806534BD183A9AA3385DB27D3164B255DBF99 (void);
// 0x00000453 System.Void hardartcore.TheChase.Scripts.Objects.Coin/cf::vz()
extern void cf_vz_m00EF2A063E94129A190C68E4C62FAED075711C55 (void);
// 0x00000454 System.Boolean hardartcore.TheChase.Scripts.Objects.Coin/cf::MoveNext()
extern void cf_MoveNext_mDA7CC8754656522C830978A438E9891F0DF0098F (void);
// 0x00000455 System.Object hardartcore.TheChase.Scripts.Objects.Coin/cf::wa()
extern void cf_wa_m51C6517A6D8E854834752B6BC2C9F8A7819B320D (void);
// 0x00000456 System.Void hardartcore.TheChase.Scripts.Objects.Coin/cf::wb()
extern void cf_wb_m4266281F32149BB8A7276D8FE9C88EF3679B4468 (void);
// 0x00000457 System.Object hardartcore.TheChase.Scripts.Objects.Coin/cf::wc()
extern void cf_wc_mD4276501D5E84BB93FC0013465C3187657D1B9F7 (void);
// 0x00000458 System.Void hardartcore.TheChase.Scripts.Objects.Explosion::Explode(UnityEngine.Vector3)
extern void Explosion_Explode_m76F1016C6CF835BC970FBC9AB7A76878D95E9748 (void);
// 0x00000459 System.Void hardartcore.TheChase.Scripts.Objects.Explosion::.ctor()
extern void Explosion__ctor_m91CDAE004330874986CA97E2F335972DE2070A68 (void);
// 0x0000045A System.Void hardartcore.TheChase.Scripts.Objects.FuelTank::vy()
extern void FuelTank_vy_mF675A4DBAFB968AF1E95EC5A57C8DCBD4720EEFD (void);
// 0x0000045B System.Void hardartcore.TheChase.Scripts.Objects.FuelTank::.ctor()
extern void FuelTank__ctor_mA7A7542F6BA5F7E50751B8E1431AD9D18FA9C42D (void);
// 0x0000045C System.Void hardartcore.TheChase.Scripts.Objects.Lights::Start()
extern void Lights_Start_m3B135931A334CED0E3993AD2536ABF76A4DEC46F (void);
// 0x0000045D System.Void hardartcore.TheChase.Scripts.Objects.Lights::OnEnable()
extern void Lights_OnEnable_m6553C96D0C7399381743887240D1F242AD97F11F (void);
// 0x0000045E System.Void hardartcore.TheChase.Scripts.Objects.Lights::OnDisable()
extern void Lights_OnDisable_m1A8035DD59B37314BBC0ADEEA680A63F709FB2C9 (void);
// 0x0000045F System.Collections.IEnumerator hardartcore.TheChase.Scripts.Objects.Lights::wk()
extern void Lights_wk_m0690FE42F68D7720D0F73C2CFF90BE6240447E91 (void);
// 0x00000460 System.Void hardartcore.TheChase.Scripts.Objects.Lights::.ctor()
extern void Lights__ctor_m31922245534BEE816BBBA03F6676D5009FA61FBB (void);
// 0x00000461 System.Void hardartcore.TheChase.Scripts.Objects.Lights/cg::.ctor(System.Int32)
extern void cg__ctor_m16BE6DF0B85C3B790DB35DFB99941B2D826C46B0 (void);
// 0x00000462 System.Void hardartcore.TheChase.Scripts.Objects.Lights/cg::wg()
extern void cg_wg_mF12FEC7B48419FA1B6ABDD8403C04EB90990CEA6 (void);
// 0x00000463 System.Boolean hardartcore.TheChase.Scripts.Objects.Lights/cg::MoveNext()
extern void cg_MoveNext_m2BC44A24BD503AC830D4733F04A9CF2FA6AA9B2D (void);
// 0x00000464 System.Object hardartcore.TheChase.Scripts.Objects.Lights/cg::wh()
extern void cg_wh_mE2C4648489DA768D9659F3E418AE6060119B0EB7 (void);
// 0x00000465 System.Void hardartcore.TheChase.Scripts.Objects.Lights/cg::wi()
extern void cg_wi_m2F643D523A1497E680A718EBB384658DBBCDACE7 (void);
// 0x00000466 System.Object hardartcore.TheChase.Scripts.Objects.Lights/cg::wj()
extern void cg_wj_m7FBB7F9DB300999EF76781FB9701E0F3358E926E (void);
// 0x00000467 System.Void hardartcore.TheChase.Scripts.Objects.Magnet::vy()
extern void Magnet_vy_mE4474474D35018582F22741632633A6C793B52DC (void);
// 0x00000468 System.Void hardartcore.TheChase.Scripts.Objects.Magnet::.ctor()
extern void Magnet__ctor_m8E85619251C8E68332E0731B5DFEE26CB1B64C89 (void);
// 0x00000469 System.Void hardartcore.TheChase.Scripts.Objects.PresentBox::OnEnable()
extern void PresentBox_OnEnable_m0DA65A907B0C96561EE8FB02652731FC8D37ED3E (void);
// 0x0000046A System.Void hardartcore.TheChase.Scripts.Objects.PresentBox::vy()
extern void PresentBox_vy_mD0115656FE76A328F5A17011D7E7F385F335855F (void);
// 0x0000046B System.Collections.IEnumerator hardartcore.TheChase.Scripts.Objects.PresentBox::wp()
extern void PresentBox_wp_mF80509C8774852EB36B45C0C5642E75CAE1834CF (void);
// 0x0000046C System.Void hardartcore.TheChase.Scripts.Objects.PresentBox::.ctor()
extern void PresentBox__ctor_m6EFD779175168FA53B2793B52294081694DC5D05 (void);
// 0x0000046D System.Void hardartcore.TheChase.Scripts.Objects.PresentBox/ch::.ctor(System.Int32)
extern void ch__ctor_m8D3B93F090EF2D330DF3BA98C3AC18A309900BF2 (void);
// 0x0000046E System.Void hardartcore.TheChase.Scripts.Objects.PresentBox/ch::wl()
extern void ch_wl_m0FBE1F8688683CDD0A732430C447C03E4348D561 (void);
// 0x0000046F System.Boolean hardartcore.TheChase.Scripts.Objects.PresentBox/ch::MoveNext()
extern void ch_MoveNext_m4FD39D27911EEF159EEB2E7282DA836D675F4D19 (void);
// 0x00000470 System.Object hardartcore.TheChase.Scripts.Objects.PresentBox/ch::wm()
extern void ch_wm_mFACB9BACA62091B04BF0B9782E19A47A982C142C (void);
// 0x00000471 System.Void hardartcore.TheChase.Scripts.Objects.PresentBox/ch::wn()
extern void ch_wn_mB831097C994A8C446579D4DED6DE0943D038EE08 (void);
// 0x00000472 System.Object hardartcore.TheChase.Scripts.Objects.PresentBox/ch::wo()
extern void ch_wo_m6E573F6BA773F50CA2B258B68469CB652248A80B (void);
// 0x00000473 System.Void hardartcore.TheChase.Scripts.Objects.Tile::Start()
extern void Tile_Start_mBBECA202289A4A9A189B092F6C16E8E9C7A2649C (void);
// 0x00000474 hardartcore.TheChase.Scripts.Managers.PoolManager/ObjectType hardartcore.TheChase.Scripts.Objects.Tile::wq()
extern void Tile_wq_mACE815BCC27FEFED3E102AC3CF910E8DF834AF76 (void);
// 0x00000475 System.Void hardartcore.TheChase.Scripts.Objects.Tile::InitObjects()
extern void Tile_InitObjects_m1A80F5EB409A0F7EF1CDD8A5F1F0CB675B797EAD (void);
// 0x00000476 System.Void hardartcore.TheChase.Scripts.Objects.Tile::SpawnAbilities()
extern void Tile_SpawnAbilities_mC4CA1D6831C74FB452010EDBD4EB0F10A3C663F3 (void);
// 0x00000477 System.Void hardartcore.TheChase.Scripts.Objects.Tile::BaseUpdate()
extern void Tile_BaseUpdate_mCB80C33066F9CFE4224FA4DAC93A13B3B3F0C5EB (void);
// 0x00000478 System.Void hardartcore.TheChase.Scripts.Objects.Tile::SetCoinCollected(UnityEngine.GameObject)
extern void Tile_SetCoinCollected_m2B8D5D2BC7853D189F4BA17BEC0F4516EB8EC420 (void);
// 0x00000479 System.Void hardartcore.TheChase.Scripts.Objects.Tile::.ctor()
extern void Tile__ctor_m92CD11B528C8BF0FFF4F687714906899F8ECAFE6 (void);
// 0x0000047A hardartcore.TheChase.Scripts.Managers.CarsManager hardartcore.TheChase.Scripts.Managers.CarsManager::get_Instance()
extern void CarsManager_get_Instance_m5D3D0C0A97D0BAFFDCD48ED8FAFB97E477E00412 (void);
// 0x0000047B System.Void hardartcore.TheChase.Scripts.Managers.CarsManager::set_Instance(hardartcore.TheChase.Scripts.Managers.CarsManager)
extern void CarsManager_set_Instance_m1C1E3A0F46D275B8BA5FAC3EF38B5AFEAE605AD6 (void);
// 0x0000047C System.Void hardartcore.TheChase.Scripts.Managers.CarsManager::add_OnCarSelected(UnityEngine.Events.UnityAction`1<hardartcore.TheChase.Scripts.ScriptableObjects.CarScriptableObject>)
extern void CarsManager_add_OnCarSelected_m3B3FD817E26C2981D8F8907B5552F2FEE9561CBA (void);
// 0x0000047D System.Void hardartcore.TheChase.Scripts.Managers.CarsManager::remove_OnCarSelected(UnityEngine.Events.UnityAction`1<hardartcore.TheChase.Scripts.ScriptableObjects.CarScriptableObject>)
extern void CarsManager_remove_OnCarSelected_mFEA21F5151524D8924EEB2730DCEBA339BF9C6BA (void);
// 0x0000047E System.Void hardartcore.TheChase.Scripts.Managers.CarsManager::Awake()
extern void CarsManager_Awake_m3CD7607CCF71CC67010CAAF38559B236A26C51CB (void);
// 0x0000047F System.Void hardartcore.TheChase.Scripts.Managers.CarsManager::OnEnable()
extern void CarsManager_OnEnable_m63E18DB5F4E97896A5E34A52AB3A5B5AA6EA95A6 (void);
// 0x00000480 System.Void hardartcore.TheChase.Scripts.Managers.CarsManager::OnDisable()
extern void CarsManager_OnDisable_m0F92B50D3C6F281EA5EC8DE8AC770BA18BD1FB74 (void);
// 0x00000481 System.Void hardartcore.TheChase.Scripts.Managers.CarsManager::wr()
extern void CarsManager_wr_m60C251A25A8799FACABC89CE7B81418262FE074E (void);
// 0x00000482 System.Void hardartcore.TheChase.Scripts.Managers.CarsManager::ws()
extern void CarsManager_ws_m831682076D93A87C2B2A99325C4644C0AE5E2C97 (void);
// 0x00000483 hardartcore.TheChase.Scripts.ScriptableObjects.CarScriptableObject hardartcore.TheChase.Scripts.Managers.CarsManager::GetSelectedCar()
extern void CarsManager_GetSelectedCar_mFE43EB202FF61C1D1A3779C7BC826579A8B19589 (void);
// 0x00000484 System.Void hardartcore.TheChase.Scripts.Managers.CarsManager::ChangeCar(System.Int32)
extern void CarsManager_ChangeCar_m7005A51D27C31B18AD8A950B2FEBF6EAB77B4CB5 (void);
// 0x00000485 System.Void hardartcore.TheChase.Scripts.Managers.CarsManager::ResetSelectedCarIndex()
extern void CarsManager_ResetSelectedCarIndex_m557C00D1B8E7660F21AF58A5B4407E2117890CAF (void);
// 0x00000486 System.Boolean hardartcore.TheChase.Scripts.Managers.CarsManager::UnlockACar()
extern void CarsManager_UnlockACar_m650799BD5A1A1691208F072D059FA53C0C3CD641 (void);
// 0x00000487 System.Void hardartcore.TheChase.Scripts.Managers.CarsManager::SelectCar()
extern void CarsManager_SelectCar_m4A8BC77841EBC1FE35439CC8B31911D10E0D53C3 (void);
// 0x00000488 System.Void hardartcore.TheChase.Scripts.Managers.CarsManager::.ctor()
extern void CarsManager__ctor_m9011AFBC8BD942DEB9A1D71D1F081A1653958B6A (void);
// 0x00000489 System.Void hardartcore.TheChase.Scripts.Managers.CurrencyManager::add_OnCoinsUpdated(UnityEngine.Events.UnityAction`2<System.Int32,System.Int32>)
extern void CurrencyManager_add_OnCoinsUpdated_mF2E6096BD39B6FE89C7F40156063136574911868 (void);
// 0x0000048A System.Void hardartcore.TheChase.Scripts.Managers.CurrencyManager::remove_OnCoinsUpdated(UnityEngine.Events.UnityAction`2<System.Int32,System.Int32>)
extern void CurrencyManager_remove_OnCoinsUpdated_mD700E7B4E8430C46875514E4BD8B97C474ECD35B (void);
// 0x0000048B hardartcore.TheChase.Scripts.Managers.CurrencyManager hardartcore.TheChase.Scripts.Managers.CurrencyManager::get_Instance()
extern void CurrencyManager_get_Instance_mC44C06A7308844AC95AF8286E6C1151C5DD905D6 (void);
// 0x0000048C System.Void hardartcore.TheChase.Scripts.Managers.CurrencyManager::set_Instance(hardartcore.TheChase.Scripts.Managers.CurrencyManager)
extern void CurrencyManager_set_Instance_m1D9BFC189D7ED5BFB3FB46BDA271F743D1050B13 (void);
// 0x0000048D System.Int32 hardartcore.TheChase.Scripts.Managers.CurrencyManager::get_Coins()
extern void CurrencyManager_get_Coins_m917ABEF1A0F458B90E51984AAB9692A0CE0E32F9 (void);
// 0x0000048E System.Void hardartcore.TheChase.Scripts.Managers.CurrencyManager::set_Coins(System.Int32)
extern void CurrencyManager_set_Coins_mE37697882527E42D2E13D064E9471985E512ACAA (void);
// 0x0000048F System.Void hardartcore.TheChase.Scripts.Managers.CurrencyManager::Awake()
extern void CurrencyManager_Awake_mA97F2186C8AEBEF96626820400F8D4E44F45B997 (void);
// 0x00000490 System.Void hardartcore.TheChase.Scripts.Managers.CurrencyManager::wt()
extern void CurrencyManager_wt_mDF83066492570826459F6D2BF03E03D8BACC8DE5 (void);
// 0x00000491 System.Void hardartcore.TheChase.Scripts.Managers.CurrencyManager::AddCoins(System.Int32)
extern void CurrencyManager_AddCoins_mE8C76802F7387E3557F2BB53C24C3F903FA9A2EA (void);
// 0x00000492 System.Void hardartcore.TheChase.Scripts.Managers.CurrencyManager::SubtractCoins(System.Int32)
extern void CurrencyManager_SubtractCoins_m73ADFF4BC8CC8390F5395E550467E42748B8825C (void);
// 0x00000493 System.Void hardartcore.TheChase.Scripts.Managers.CurrencyManager::.ctor()
extern void CurrencyManager__ctor_m0FDE645A55D2D9D1732971990077444358E8FD34 (void);
// 0x00000494 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::add_OnGameStarted(UnityEngine.Events.UnityAction)
extern void GameManager_add_OnGameStarted_mAD71F2355824C3FA12D31015F0613AB021D5C7ED (void);
// 0x00000495 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::remove_OnGameStarted(UnityEngine.Events.UnityAction)
extern void GameManager_remove_OnGameStarted_m5539562F31EA600409DFB86FB9E2AEA921292544 (void);
// 0x00000496 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::add_OnGameOver(UnityEngine.Events.UnityAction)
extern void GameManager_add_OnGameOver_mB9E6494EF2694816A08705711CA2E93B423E54AF (void);
// 0x00000497 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::remove_OnGameOver(UnityEngine.Events.UnityAction)
extern void GameManager_remove_OnGameOver_m5EB19B4AB4825E9F39F595B7F4A7E15BF6C0F34E (void);
// 0x00000498 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::add_OnMagnetModeChanged(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern void GameManager_add_OnMagnetModeChanged_m247895196DE2679DB45406F3ECF2D1E79DF50D6B (void);
// 0x00000499 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::remove_OnMagnetModeChanged(UnityEngine.Events.UnityAction`1<System.Boolean>)
extern void GameManager_remove_OnMagnetModeChanged_m9EF9E83C6CC84F0FD0EE6E743A8FD6931D2EB7F5 (void);
// 0x0000049A System.Int32 hardartcore.TheChase.Scripts.Managers.GameManager::get_CollectedCoins()
extern void GameManager_get_CollectedCoins_mD84143859726DA875A266E557840142E3036A559 (void);
// 0x0000049B System.Void hardartcore.TheChase.Scripts.Managers.GameManager::set_CollectedCoins(System.Int32)
extern void GameManager_set_CollectedCoins_m4AFC02635B9BE12DAC0D2346B62024F62D7BE418 (void);
// 0x0000049C System.Boolean hardartcore.TheChase.Scripts.Managers.GameManager::get_IsMagnetModeEnabled()
extern void GameManager_get_IsMagnetModeEnabled_m683B828F1D5B2C08FAD4405E5F0D6AA787FFFE1F (void);
// 0x0000049D System.Void hardartcore.TheChase.Scripts.Managers.GameManager::set_IsMagnetModeEnabled(System.Boolean)
extern void GameManager_set_IsMagnetModeEnabled_mD11E45FAB3A94567FE26E68670B37A5F675D015E (void);
// 0x0000049E System.Void hardartcore.TheChase.Scripts.Managers.GameManager::Awake()
extern void GameManager_Awake_mACFB2BABD5AC56855BC97D16BF18A0A8CB14F8AC (void);
// 0x0000049F System.Void hardartcore.TheChase.Scripts.Managers.GameManager::OnEnable()
extern void GameManager_OnEnable_m73A52AF6AE29245BC87E9FE58C30A6DB595FD731 (void);
// 0x000004A0 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::OnDisable()
extern void GameManager_OnDisable_m87357F569CDEE0E5C4FA0417922750CEA1C058AB (void);
// 0x000004A1 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::Start()
extern void GameManager_Start_m086F6FAF5ACC48DCDB69CDEE99F1C6D9B0C5E61C (void);
// 0x000004A2 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::xk()
extern void GameManager_xk_m7A50F9936C3021E9052F644833E9297B3C43FDE7 (void);
// 0x000004A3 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::xl(System.Int32,System.Int32)
extern void GameManager_xl_m3595789886350F8EF192EAB28585E511B3C3C03D (void);
// 0x000004A4 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::xm()
extern void GameManager_xm_m26959CE5B93B8A68A58E5AE829C0086F6F6E6A09 (void);
// 0x000004A5 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::xn()
extern void GameManager_xn_mBF2BDA29793DB1BE51A3C1ADB1DD6DAC6CEE7127 (void);
// 0x000004A6 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::xo(hardartcore.TheChase.Scripts.Managers.PoolManager/ObjectType)
extern void GameManager_xo_mD91183D559155083EC70AF67724011635CDBF062 (void);
// 0x000004A7 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::xp()
extern void GameManager_xp_m541EF81B56D312F710297CDAD38101DDAEE51F82 (void);
// 0x000004A8 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::DisableMagnetMode()
extern void GameManager_DisableMagnetMode_m4B92F04EFC87EE1E2A1B5A5F7F8B41B7B6B98FA7 (void);
// 0x000004A9 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::StartGame()
extern void GameManager_StartGame_m5DB7F4DAE4964889FF17B0D86013B0EA2A56B5F8 (void);
// 0x000004AA System.Void hardartcore.TheChase.Scripts.Managers.GameManager::RestartGame()
extern void GameManager_RestartGame_mBCE3D2414197ABD9DD209755A4899AC43E3F5379 (void);
// 0x000004AB System.Boolean hardartcore.TheChase.Scripts.Managers.GameManager::IsPaused()
extern void GameManager_IsPaused_m80F8255A7CDE24490152969AB30FE9E2E5C38977 (void);
// 0x000004AC System.Boolean hardartcore.TheChase.Scripts.Managers.GameManager::IsGameStarted()
extern void GameManager_IsGameStarted_m9C6F91DEDB157210F8455518875F7ED88C094074 (void);
// 0x000004AD System.Void hardartcore.TheChase.Scripts.Managers.GameManager::EnableMagnetMode()
extern void GameManager_EnableMagnetMode_mDA610F8D650D8C7EDA6AC509A768FDE29E7BE827 (void);
// 0x000004AE UnityEngine.Transform hardartcore.TheChase.Scripts.Managers.GameManager::GetPlayerTransform()
extern void GameManager_GetPlayerTransform_mA3E13ED99C54BB926A56E4B9A495F7DEFA882A0D (void);
// 0x000004AF System.Int32 hardartcore.TheChase.Scripts.Managers.GameManager::GetTravelledDistance()
extern void GameManager_GetTravelledDistance_m8A529D82754574206A7BC5DE32A56BAAB71335F0 (void);
// 0x000004B0 System.Boolean hardartcore.TheChase.Scripts.Managers.GameManager::IsGameOver()
extern void GameManager_IsGameOver_m46DF36724A7D7F180A391AA32DF0D82768492B48 (void);
// 0x000004B1 System.Void hardartcore.TheChase.Scripts.Managers.GameManager::.ctor()
extern void GameManager__ctor_m5E44982A4E710D519DD287F7BF9831B82B95676B (void);
// 0x000004B2 System.Collections.IEnumerator hardartcore.TheChase.Scripts.Managers.GameManager::xq()
extern void GameManager_xq_mC19DD29042B997B022B2F38FCFBCC6D9851CCAD3 (void);
// 0x000004B3 System.Collections.IEnumerator hardartcore.TheChase.Scripts.Managers.GameManager::xr()
extern void GameManager_xr_mD4423E62AED934395B82ACE6F605E6812C8319CA (void);
// 0x000004B4 System.Collections.IEnumerator hardartcore.TheChase.Scripts.Managers.GameManager::xs()
extern void GameManager_xs_m0521F184DE5F422DF19731498CB172E99229BDDE (void);
// 0x000004B5 System.Collections.IEnumerator hardartcore.TheChase.Scripts.Managers.GameManager::xt()
extern void GameManager_xt_m8B9EBA770E41F32C8076E6F1259A08AB45930F6A (void);
// 0x000004B6 System.Void hardartcore.TheChase.Scripts.Managers.GameManager/cj::.ctor(System.Int32)
extern void cj__ctor_m74458FC83C8E1F6F2691146B74588EA7E345357C (void);
// 0x000004B7 System.Void hardartcore.TheChase.Scripts.Managers.GameManager/cj::wu()
extern void cj_wu_m17A67CEBA429E7986DC50038AC955365A9B23186 (void);
// 0x000004B8 System.Boolean hardartcore.TheChase.Scripts.Managers.GameManager/cj::MoveNext()
extern void cj_MoveNext_m5817D7AEB365A13BB199BDB5CDE2217AFA4401FC (void);
// 0x000004B9 System.Object hardartcore.TheChase.Scripts.Managers.GameManager/cj::wv()
extern void cj_wv_mC444465954941F75D1297AB1B585525B63C5DF02 (void);
// 0x000004BA System.Void hardartcore.TheChase.Scripts.Managers.GameManager/cj::ww()
extern void cj_ww_mF17EF8A6B38706E2C8A4233F03FE0BD2354EDB93 (void);
// 0x000004BB System.Object hardartcore.TheChase.Scripts.Managers.GameManager/cj::wx()
extern void cj_wx_mB28E149BB97310B14F159D3163EA95D6F85F30BE (void);
// 0x000004BC System.Void hardartcore.TheChase.Scripts.Managers.GameManager/ck::.ctor(System.Int32)
extern void ck__ctor_m54EADC92BBD810EFC2FABB3270A8DE99200A8685 (void);
// 0x000004BD System.Void hardartcore.TheChase.Scripts.Managers.GameManager/ck::wy()
extern void ck_wy_mDF736A356D65E4CD85E255F87C7327661706B6F2 (void);
// 0x000004BE System.Boolean hardartcore.TheChase.Scripts.Managers.GameManager/ck::MoveNext()
extern void ck_MoveNext_mA9AD2F8CA35A3CB130972BFBD8733A0C3EA41BBB (void);
// 0x000004BF System.Object hardartcore.TheChase.Scripts.Managers.GameManager/ck::wz()
extern void ck_wz_mBC4068905E434B9B77B61F9F53B8963A0C9E0EB3 (void);
// 0x000004C0 System.Void hardartcore.TheChase.Scripts.Managers.GameManager/ck::xa()
extern void ck_xa_mB671D325ECC7977AA18CFAC2AD06CABD3A8C42FF (void);
// 0x000004C1 System.Object hardartcore.TheChase.Scripts.Managers.GameManager/ck::xb()
extern void ck_xb_mCDB6EECC76FECEE52E59DCD98D6FB0733016DD80 (void);
// 0x000004C2 System.Void hardartcore.TheChase.Scripts.Managers.GameManager/cl::.ctor(System.Int32)
extern void cl__ctor_m1FA6B1C62D86DEDE841062FC63C3496F776CA14C (void);
// 0x000004C3 System.Void hardartcore.TheChase.Scripts.Managers.GameManager/cl::xc()
extern void cl_xc_m97417C09FD8FA93A341DB9E9255897609762AB61 (void);
// 0x000004C4 System.Boolean hardartcore.TheChase.Scripts.Managers.GameManager/cl::MoveNext()
extern void cl_MoveNext_m9B7DFF94A2C980CBCA4C973908C85700A6C0B2EC (void);
// 0x000004C5 System.Object hardartcore.TheChase.Scripts.Managers.GameManager/cl::xd()
extern void cl_xd_mEDC945C874322F898762B063AF727A97A822CA43 (void);
// 0x000004C6 System.Void hardartcore.TheChase.Scripts.Managers.GameManager/cl::xe()
extern void cl_xe_m342503C37890ED2B58B9F40844F5FADAB52C0B89 (void);
// 0x000004C7 System.Object hardartcore.TheChase.Scripts.Managers.GameManager/cl::xf()
extern void cl_xf_mF303503F26FA0BDD0D5C2855C00C4FB6E3C9DD11 (void);
// 0x000004C8 System.Void hardartcore.TheChase.Scripts.Managers.GameManager/cm::.ctor(System.Int32)
extern void cm__ctor_m5CA0F6D6B7B05AEB7E08BE9AF27E18B53C0A904F (void);
// 0x000004C9 System.Void hardartcore.TheChase.Scripts.Managers.GameManager/cm::xg()
extern void cm_xg_m74600D7069B6D73455A1D26F17DA3F4F0B1E1D6B (void);
// 0x000004CA System.Boolean hardartcore.TheChase.Scripts.Managers.GameManager/cm::MoveNext()
extern void cm_MoveNext_m6D1EBFE6F42EC1E1087AF866762987DA8581184A (void);
// 0x000004CB System.Object hardartcore.TheChase.Scripts.Managers.GameManager/cm::xh()
extern void cm_xh_m35BC84247D0559105A4825DF40320B8250BA9863 (void);
// 0x000004CC System.Void hardartcore.TheChase.Scripts.Managers.GameManager/cm::xi()
extern void cm_xi_m235FB603086A5E2D6A4CD5C903CD89680069EE09 (void);
// 0x000004CD System.Object hardartcore.TheChase.Scripts.Managers.GameManager/cm::xj()
extern void cm_xj_m8AFCFBCC811EA19C574680D940B01A5977C2289B (void);
// 0x000004CE hardartcore.TheChase.Scripts.Managers.MapManager hardartcore.TheChase.Scripts.Managers.MapManager::get_Instance()
extern void MapManager_get_Instance_m746F960C371CFB4E2CE0DAC5AB0303A5A0400B5B (void);
// 0x000004CF System.Void hardartcore.TheChase.Scripts.Managers.MapManager::set_Instance(hardartcore.TheChase.Scripts.Managers.MapManager)
extern void MapManager_set_Instance_m5273563B2A10AB105AF65C8FB3056D4CEB7AE6C7 (void);
// 0x000004D0 System.Void hardartcore.TheChase.Scripts.Managers.MapManager::add_OnMapSelected(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void MapManager_add_OnMapSelected_m57ED02104C0455905B62E7E2343FAB4B4FC883E7 (void);
// 0x000004D1 System.Void hardartcore.TheChase.Scripts.Managers.MapManager::remove_OnMapSelected(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void MapManager_remove_OnMapSelected_mD6DB68589D9A25DB525434A67414EA3DC3065970 (void);
// 0x000004D2 System.Int32 hardartcore.TheChase.Scripts.Managers.MapManager::get_SelectedMapIndex()
extern void MapManager_get_SelectedMapIndex_m3879571B947B9EF38BEF1DFAD8D9925F8FA46B9C (void);
// 0x000004D3 System.Void hardartcore.TheChase.Scripts.Managers.MapManager::set_SelectedMapIndex(System.Int32)
extern void MapManager_set_SelectedMapIndex_m18648D51103F027E11A211AADB3E55171064B359 (void);
// 0x000004D4 System.Void hardartcore.TheChase.Scripts.Managers.MapManager::Awake()
extern void MapManager_Awake_m923541E958A8F328720E0E0CC961C073FF31C88D (void);
// 0x000004D5 hardartcore.TheChase.Scripts.ScriptableObjects.MapScriptableObject hardartcore.TheChase.Scripts.Managers.MapManager::GetSelectedMap()
extern void MapManager_GetSelectedMap_mF4FA0B834EE12AF815AAC46274FAB9CC5834956E (void);
// 0x000004D6 System.Void hardartcore.TheChase.Scripts.Managers.MapManager::ChangeMap(System.Int32)
extern void MapManager_ChangeMap_m15A7E626E67CE199FB5B05EEE9A1EAB4A0AB2690 (void);
// 0x000004D7 System.Void hardartcore.TheChase.Scripts.Managers.MapManager::SelectMap()
extern void MapManager_SelectMap_mA46F9494CB593E05608E57BEAC7A4571B9873D38 (void);
// 0x000004D8 System.Void hardartcore.TheChase.Scripts.Managers.MapManager::ResetSelectedMapIndex()
extern void MapManager_ResetSelectedMapIndex_m24CE2EF6C08EC4129687A12388CC7D0F3FE7A104 (void);
// 0x000004D9 System.Void hardartcore.TheChase.Scripts.Managers.MapManager::LoadSelectedMap()
extern void MapManager_LoadSelectedMap_m5FE25ABEC634C88A814B6E5B9D632B4B4720AF97 (void);
// 0x000004DA System.Void hardartcore.TheChase.Scripts.Managers.MapManager::.ctor()
extern void MapManager__ctor_mF2BFDFEB7BCA407659ABBB92DF6EC9EBD647A06B (void);
// 0x000004DB hardartcore.TheChase.Scripts.Managers.PoolManager hardartcore.TheChase.Scripts.Managers.PoolManager::get_Instance()
extern void PoolManager_get_Instance_m5F5096461D745CFC79FD9C08F4BD2B771700A5F1 (void);
// 0x000004DC System.Void hardartcore.TheChase.Scripts.Managers.PoolManager::set_Instance(hardartcore.TheChase.Scripts.Managers.PoolManager)
extern void PoolManager_set_Instance_m2011EF385CAF7610F1034F32F349F7398856D96E (void);
// 0x000004DD System.Void hardartcore.TheChase.Scripts.Managers.PoolManager::add_OnMapItemsInitialized(UnityEngine.Events.UnityAction)
extern void PoolManager_add_OnMapItemsInitialized_mB34BA1E4FBCDB49A696705270162BC5CE407A2CB (void);
// 0x000004DE System.Void hardartcore.TheChase.Scripts.Managers.PoolManager::remove_OnMapItemsInitialized(UnityEngine.Events.UnityAction)
extern void PoolManager_remove_OnMapItemsInitialized_m64F68D13B6567D370CCBF2C34BD0709741549C9E (void);
// 0x000004DF System.Void hardartcore.TheChase.Scripts.Managers.PoolManager::Awake()
extern void PoolManager_Awake_m24733113D4D7FF04EAAA10930AA70A286D1C111A (void);
// 0x000004E0 System.Void hardartcore.TheChase.Scripts.Managers.PoolManager::OnEnable()
extern void PoolManager_OnEnable_m9EDA54BEB5CE9898CC4A3F7FB0D71E43DC6D0928 (void);
// 0x000004E1 System.Void hardartcore.TheChase.Scripts.Managers.PoolManager::OnDisable()
extern void PoolManager_OnDisable_m7D309E0BEA7ADE9D5C4D07D721BCB8A685714E01 (void);
// 0x000004E2 System.Void hardartcore.TheChase.Scripts.Managers.PoolManager::xv()
extern void PoolManager_xv_m51E8990D0717EFF7163C1D84C59072A7B41EE9DA (void);
// 0x000004E3 System.Void hardartcore.TheChase.Scripts.Managers.PoolManager::xw()
extern void PoolManager_xw_m234D0D90CF5D34D7337F2F518408121F4D53CA23 (void);
// 0x000004E4 System.Void hardartcore.TheChase.Scripts.Managers.PoolManager::xx(hardartcore.TheChase.Scripts.Managers.PoolManager/PoolItem)
extern void PoolManager_xx_mD6C11B552491008BDB7CB91F359CC8B2B1E7AF2A (void);
// 0x000004E5 UnityEngine.GameObject hardartcore.TheChase.Scripts.Managers.PoolManager::xy(UnityEngine.GameObject)
extern void PoolManager_xy_mE090DA71C672E3197CBD8D77632B2545AB9CEC5B (void);
// 0x000004E6 System.Void hardartcore.TheChase.Scripts.Managers.PoolManager::PlayExplosion(UnityEngine.Vector3)
extern void PoolManager_PlayExplosion_m8BF2A355C96B8039DCC7A99847619FB8EBE2950D (void);
// 0x000004E7 UnityEngine.GameObject hardartcore.TheChase.Scripts.Managers.PoolManager::GetPooledObject(hardartcore.TheChase.Scripts.Managers.PoolManager/ObjectType)
extern void PoolManager_GetPooledObject_m49587865ADF1E54D9A4C4BC20F643D023B7BEBD9 (void);
// 0x000004E8 System.Void hardartcore.TheChase.Scripts.Managers.PoolManager::.ctor()
extern void PoolManager__ctor_m0A73471A1C90485A73CAAE7E9763E5624899634B (void);
// 0x000004E9 System.Void hardartcore.TheChase.Scripts.Managers.PoolManager/PoolItem::.ctor()
extern void PoolItem__ctor_mD886AE06D514FE1A4281966C5B6AB64D9559BC56 (void);
// 0x000004EA System.Void hardartcore.TheChase.Scripts.Managers.PoolManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m26A34F4E0266475F0BE42E309A27E22DE02E34D5 (void);
// 0x000004EB System.Void hardartcore.TheChase.Scripts.Managers.PoolManager/<>c::.ctor()
extern void U3CU3Ec__ctor_mD07E5B2FD9E609C964C17F72BD1F41FCB7FD00A1 (void);
// 0x000004EC System.Boolean hardartcore.TheChase.Scripts.Managers.PoolManager/<>c::xu(UnityEngine.GameObject)
extern void U3CU3Ec_xu_m4A65E588D3B7CA28DE464EB378307C05AC2E0229 (void);
// 0x000004ED hardartcore.TheChase.Scripts.Managers.SnackBarManager hardartcore.TheChase.Scripts.Managers.SnackBarManager::get_Instance()
extern void SnackBarManager_get_Instance_mAB4E6E0E62E6099A5C153FCF665FE3E161A17D34 (void);
// 0x000004EE System.Void hardartcore.TheChase.Scripts.Managers.SnackBarManager::set_Instance(hardartcore.TheChase.Scripts.Managers.SnackBarManager)
extern void SnackBarManager_set_Instance_m6C5D9358FF963AF7BB6A9DFFBED6E01D6CD764BD (void);
// 0x000004EF System.Void hardartcore.TheChase.Scripts.Managers.SnackBarManager::Awake()
extern void SnackBarManager_Awake_mF14DBAE67BCCDC1A20373A36A367F8A3515E110E (void);
// 0x000004F0 System.Void hardartcore.TheChase.Scripts.Managers.SnackBarManager::OnDestroy()
extern void SnackBarManager_OnDestroy_m25A8C2BA7B64D29EB6D6CC6625FE515B026A510E (void);
// 0x000004F1 System.Collections.IEnumerator hardartcore.TheChase.Scripts.Managers.SnackBarManager::yh(System.String,System.String,UnityEngine.Sprite)
extern void SnackBarManager_yh_m52B9368D16FBC39173A894C70F5AB78874D02C8F (void);
// 0x000004F2 System.Collections.IEnumerator hardartcore.TheChase.Scripts.Managers.SnackBarManager::yi()
extern void SnackBarManager_yi_mA699E87E7A397F1340CE4DAAB372FFFDEFF41391 (void);
// 0x000004F3 System.Void hardartcore.TheChase.Scripts.Managers.SnackBarManager::ShowSnackbar(System.String,System.String,UnityEngine.Sprite)
extern void SnackBarManager_ShowSnackbar_mD7BC1C9DCCF7491F936C25D3F639D76E400F6F41 (void);
// 0x000004F4 System.Void hardartcore.TheChase.Scripts.Managers.SnackBarManager::.ctor()
extern void SnackBarManager__ctor_m9DF65A910D27555DB9EEBE839401710FEED3083F (void);
// 0x000004F5 System.Void hardartcore.TheChase.Scripts.Managers.SnackBarManager/cn::.ctor(System.Int32)
extern void cn__ctor_m573C69A773201B4D83CC82E21D32C3EA5D854E96 (void);
// 0x000004F6 System.Void hardartcore.TheChase.Scripts.Managers.SnackBarManager/cn::xz()
extern void cn_xz_m9F56513DEA3DF30CF2B69A8C9C5AF312D8525B51 (void);
// 0x000004F7 System.Boolean hardartcore.TheChase.Scripts.Managers.SnackBarManager/cn::MoveNext()
extern void cn_MoveNext_mDBA7A3884CB5556A54C2E34C6286617A6B1CD910 (void);
// 0x000004F8 System.Object hardartcore.TheChase.Scripts.Managers.SnackBarManager/cn::ya()
extern void cn_ya_m174DD0F62CD65CFE5B7AE0B779B643D3B56E5783 (void);
// 0x000004F9 System.Void hardartcore.TheChase.Scripts.Managers.SnackBarManager/cn::yb()
extern void cn_yb_m67C4191C6E6AC05DAF44BEE63D6CF843DEE5FF78 (void);
// 0x000004FA System.Object hardartcore.TheChase.Scripts.Managers.SnackBarManager/cn::yc()
extern void cn_yc_mEEFB471EDF8EA5A2D93C77C7722BFC795EFF36C3 (void);
// 0x000004FB System.Void hardartcore.TheChase.Scripts.Managers.SnackBarManager/co::.ctor(System.Int32)
extern void co__ctor_m4DF2F1A4FCD8B8CAEB451A6BD4366784F8C3BB89 (void);
// 0x000004FC System.Void hardartcore.TheChase.Scripts.Managers.SnackBarManager/co::yd()
extern void co_yd_m1CC98CBE550F5169E5E6EB7BBA62E6A7C12DC477 (void);
// 0x000004FD System.Boolean hardartcore.TheChase.Scripts.Managers.SnackBarManager/co::MoveNext()
extern void co_MoveNext_m2D001FF637E63C57F805A8950D4D0D4AF152D317 (void);
// 0x000004FE System.Object hardartcore.TheChase.Scripts.Managers.SnackBarManager/co::ye()
extern void co_ye_mEC59A14524992D4E8E74DD92271CD68C758CCA5D (void);
// 0x000004FF System.Void hardartcore.TheChase.Scripts.Managers.SnackBarManager/co::yf()
extern void co_yf_m48CCF5A5FF5134D8CC18B4C6B0B3511759EAF787 (void);
// 0x00000500 System.Object hardartcore.TheChase.Scripts.Managers.SnackBarManager/co::yg()
extern void co_yg_mA86FDC74C8952C2BD60BAC534F533EC6E2C315C9 (void);
// 0x00000501 hardartcore.TheChase.Scripts.Managers.SoundManager hardartcore.TheChase.Scripts.Managers.SoundManager::get_Instance()
extern void SoundManager_get_Instance_mB51CBBAB8D905910FD56499F2752BE40C9BEB4AB (void);
// 0x00000502 System.Void hardartcore.TheChase.Scripts.Managers.SoundManager::set_Instance(hardartcore.TheChase.Scripts.Managers.SoundManager)
extern void SoundManager_set_Instance_mB2FA7C9A8A3DE91318D6D8DFD576C2893B2CEB49 (void);
// 0x00000503 System.Void hardartcore.TheChase.Scripts.Managers.SoundManager::Awake()
extern void SoundManager_Awake_mD2AAE32AA89EA38E274EC60BD73F9979CC1489E9 (void);
// 0x00000504 System.Void hardartcore.TheChase.Scripts.Managers.SoundManager::yj(UnityEngine.AudioClip,System.Single)
extern void SoundManager_yj_m5EE775BD1C64495853821A821AD23EF49FDDFDD7 (void);
// 0x00000505 System.Int32 hardartcore.TheChase.Scripts.Managers.SoundManager::GetMusicState()
extern void SoundManager_GetMusicState_m8F0A4C9BC39C006B6C81BF60ADBE01CC61D83F65 (void);
// 0x00000506 System.Int32 hardartcore.TheChase.Scripts.Managers.SoundManager::UpdateMusicState()
extern void SoundManager_UpdateMusicState_m754476DF44CE9FA749028F32B4C454EECF45DBFD (void);
// 0x00000507 System.Void hardartcore.TheChase.Scripts.Managers.SoundManager::PlayBackgroundChaseSound()
extern void SoundManager_PlayBackgroundChaseSound_m5AA533C50EF468F147D107172701B4592BE5D95B (void);
// 0x00000508 System.Void hardartcore.TheChase.Scripts.Managers.SoundManager::StopBackgroundChaseSound()
extern void SoundManager_StopBackgroundChaseSound_mC559E98CFF876FDEE69504928A330A7F67BAFC2D (void);
// 0x00000509 System.Void hardartcore.TheChase.Scripts.Managers.SoundManager::PlayCollectCoin()
extern void SoundManager_PlayCollectCoin_m0A6DB36AD3DF28DFD6942CEA4AE92690EC462467 (void);
// 0x0000050A System.Void hardartcore.TheChase.Scripts.Managers.SoundManager::PlayExplosionSound(System.Single)
extern void SoundManager_PlayExplosionSound_mBC2667FA6A5DE3238C275B0471AC7769222AC00D (void);
// 0x0000050B System.Void hardartcore.TheChase.Scripts.Managers.SoundManager::PlayButtonClickSound()
extern void SoundManager_PlayButtonClickSound_m953A36DEB0A178A1C82C1E039E6BB826A8A9C8AD (void);
// 0x0000050C System.Void hardartcore.TheChase.Scripts.Managers.SoundManager::.ctor()
extern void SoundManager__ctor_m5706466DF19DE3266DB8F6125E37E8BB013DF2C4 (void);
// 0x0000050D hardartcore.TheChase.Scripts.Managers.TileSpawnManager hardartcore.TheChase.Scripts.Managers.TileSpawnManager::get_Instance()
extern void TileSpawnManager_get_Instance_m35E23F0FFE8CC068A3DD95CB064B94914E5C2BDC (void);
// 0x0000050E System.Void hardartcore.TheChase.Scripts.Managers.TileSpawnManager::set_Instance(hardartcore.TheChase.Scripts.Managers.TileSpawnManager)
extern void TileSpawnManager_set_Instance_m20F8FE15C46E1D47DC203B0D442B04DBB9BCD71B (void);
// 0x0000050F System.Void hardartcore.TheChase.Scripts.Managers.TileSpawnManager::add_OnTilesCreated(UnityEngine.Events.UnityAction)
extern void TileSpawnManager_add_OnTilesCreated_m2E4535A2A79D5BEDE94788D609AAA6885D78EC21 (void);
// 0x00000510 System.Void hardartcore.TheChase.Scripts.Managers.TileSpawnManager::remove_OnTilesCreated(UnityEngine.Events.UnityAction)
extern void TileSpawnManager_remove_OnTilesCreated_m5FA9B722952EC73C8510D0C5397CFC521B6C922C (void);
// 0x00000511 hardartcore.TheChase.Scripts.Objects.Tile[] hardartcore.TheChase.Scripts.Managers.TileSpawnManager::yl()
extern void TileSpawnManager_yl_mDEC843DF206C548C44ED0630F34F1F052140C5DB (void);
// 0x00000512 System.Void hardartcore.TheChase.Scripts.Managers.TileSpawnManager::ym(hardartcore.TheChase.Scripts.Objects.Tile[])
extern void TileSpawnManager_ym_mB2D16FEAE7450D6A2A0DDC0C5E61EB18195B7553 (void);
// 0x00000513 System.Void hardartcore.TheChase.Scripts.Managers.TileSpawnManager::Awake()
extern void TileSpawnManager_Awake_mC018813FBD2A705E9942D960FD572B5BE5CDFFA4 (void);
// 0x00000514 System.Void hardartcore.TheChase.Scripts.Managers.TileSpawnManager::yn(UnityEngine.Vector3)
extern void TileSpawnManager_yn_m74A5E8EBEF3B91E33D9A4026A60C4A8CAF8FCC52 (void);
// 0x00000515 System.Void hardartcore.TheChase.Scripts.Managers.TileSpawnManager::yo(hardartcore.TheChase.Scripts.ScriptableObjects.MapScriptableObject)
extern void TileSpawnManager_yo_mE5B00E7C3105A5269C0BDD2EBC3F026432D93FF5 (void);
// 0x00000516 System.Void hardartcore.TheChase.Scripts.Managers.TileSpawnManager::InitMap(hardartcore.TheChase.Scripts.ScriptableObjects.MapScriptableObject)
extern void TileSpawnManager_InitMap_mB2BF4395B7E61EB630FC34FCC6FD3CB850749FED (void);
// 0x00000517 System.Void hardartcore.TheChase.Scripts.Managers.TileSpawnManager::InitTileObjects()
extern void TileSpawnManager_InitTileObjects_mDF27F30AAE26F686564BD5F0577FE46747DCF19B (void);
// 0x00000518 System.Void hardartcore.TheChase.Scripts.Managers.TileSpawnManager::CheckTilesAroundCurrentOne(UnityEngine.GameObject)
extern void TileSpawnManager_CheckTilesAroundCurrentOne_m59B1C6E49E67EFAEE4DCAA1935E287F7EF28D2E1 (void);
// 0x00000519 hardartcore.TheChase.Scripts.Objects.Tile hardartcore.TheChase.Scripts.Managers.TileSpawnManager::GetActiveTile()
extern void TileSpawnManager_GetActiveTile_m40738D353E55437F6E499F4BA67BCA0CA92ADA37 (void);
// 0x0000051A System.Void hardartcore.TheChase.Scripts.Managers.TileSpawnManager::HideTileAtIndex(UnityEngine.Vector3)
extern void TileSpawnManager_HideTileAtIndex_m215F27B3C85F561CFBC0B50DC0B551331FC002FB (void);
// 0x0000051B System.Void hardartcore.TheChase.Scripts.Managers.TileSpawnManager::.ctor()
extern void TileSpawnManager__ctor_mD71304BDB7B350C9520EE0CB748D82ECD13D454B (void);
// 0x0000051C System.Void hardartcore.TheChase.Scripts.Managers.TileSpawnManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m1321E63F62B0C654E3F645545DBB11FA525DFC85 (void);
// 0x0000051D System.Void hardartcore.TheChase.Scripts.Managers.TileSpawnManager/<>c::.ctor()
extern void U3CU3Ec__ctor_mF9F8B982D25F96C90D6ABB4791CEF0D0BA975632 (void);
// 0x0000051E System.Boolean hardartcore.TheChase.Scripts.Managers.TileSpawnManager/<>c::yk(hardartcore.TheChase.Scripts.Objects.Tile)
extern void U3CU3Ec_yk_m327AF7E8970442DF53E5E1CB6BD8EE5EFD737786 (void);
// 0x0000051F System.Void hardartcore.TheChase.Scripts.Managers.UiManager::OnEnable()
extern void UiManager_OnEnable_m4307CD617D23176E221D75D760C33C589D8DE042 (void);
// 0x00000520 System.Void hardartcore.TheChase.Scripts.Managers.UiManager::OnDisable()
extern void UiManager_OnDisable_m3A5302126124A5E983ABEAD14707BF37BF55D901 (void);
// 0x00000521 System.Void hardartcore.TheChase.Scripts.Managers.UiManager::Start()
extern void UiManager_Start_m0CC6C31B7E8D642CB0EC4A8E376254E3BF63F38C (void);
// 0x00000522 System.Void hardartcore.TheChase.Scripts.Managers.UiManager::yp()
extern void UiManager_yp_mCF2F3D780DA6C14D1839A00925346BE0987A80AA (void);
// 0x00000523 System.Void hardartcore.TheChase.Scripts.Managers.UiManager::yq()
extern void UiManager_yq_mC2A9EF119E6735F61A497DC29C67727BE32C37EE (void);
// 0x00000524 System.Void hardartcore.TheChase.Scripts.Managers.UiManager::yr()
extern void UiManager_yr_m46845878EA9A33EF78989F446A73C2EF479958D8 (void);
// 0x00000525 System.Void hardartcore.TheChase.Scripts.Managers.UiManager::ys()
extern void UiManager_ys_mA1F06E9FEE6CA510DDE13DAFA8943B4492D612D2 (void);
// 0x00000526 System.Void hardartcore.TheChase.Scripts.Managers.UiManager::StartGame()
extern void UiManager_StartGame_m4908AC31ED6C6CBE90D337100BD549FDF93F85D5 (void);
// 0x00000527 System.Void hardartcore.TheChase.Scripts.Managers.UiManager::RestartGame()
extern void UiManager_RestartGame_m84DA67551F5761E6EF6E61638F475A32EDAC3CFE (void);
// 0x00000528 System.Void hardartcore.TheChase.Scripts.Managers.UiManager::GoToInitScene()
extern void UiManager_GoToInitScene_m5FECC7F18BC5E309E13862EBAAC191A5DCF75660 (void);
// 0x00000529 System.Void hardartcore.TheChase.Scripts.Managers.UiManager::.ctor()
extern void UiManager__ctor_mD1F1C8C9A22F491D2D4FCF8A35281A88842E1E13 (void);
// 0x0000052A hardartcore.TheChase.Scripts.Managers.UpdateManager hardartcore.TheChase.Scripts.Managers.UpdateManager::get_Instance()
extern void UpdateManager_get_Instance_mF677A7E574917A2C1D77D8CAD868ABB7D2BECBE2 (void);
// 0x0000052B System.Void hardartcore.TheChase.Scripts.Managers.UpdateManager::set_Instance(hardartcore.TheChase.Scripts.Managers.UpdateManager)
extern void UpdateManager_set_Instance_mA3654A58EF47906724581626AD99467790F5F790 (void);
// 0x0000052C System.Void hardartcore.TheChase.Scripts.Managers.UpdateManager::Awake()
extern void UpdateManager_Awake_m8F94D3D6D28EBDE1F348A8AEDFB6EEF069EFD079 (void);
// 0x0000052D System.Void hardartcore.TheChase.Scripts.Managers.UpdateManager::Update()
extern void UpdateManager_Update_mE31245ACCFED139E61F54AEBC625926011A8F291 (void);
// 0x0000052E System.Void hardartcore.TheChase.Scripts.Managers.UpdateManager::OnDestroy()
extern void UpdateManager_OnDestroy_m8C0357837A0207EB75F25B3179984D6B94160521 (void);
// 0x0000052F System.Void hardartcore.TheChase.Scripts.Managers.UpdateManager::Add(hardartcore.TheChase.Scripts.Objects.BaseBehaviour)
extern void UpdateManager_Add_m690C087371717CF4A057F3BE56F35DED4859B580 (void);
// 0x00000530 System.Void hardartcore.TheChase.Scripts.Managers.UpdateManager::.ctor()
extern void UpdateManager__ctor_m8CFEC2C0D5BBECA92B913059CBB4277AF1EA6F7F (void);
// 0x00000531 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::add_OnFuelConsumed(UnityEngine.Events.UnityAction`1<System.Single>)
extern void CarController_add_OnFuelConsumed_m885A96DB6904E114C04CFDA1F51C484A48289A3B (void);
// 0x00000532 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::remove_OnFuelConsumed(UnityEngine.Events.UnityAction`1<System.Single>)
extern void CarController_remove_OnFuelConsumed_m5A08D91EB43745CE6A30E2EBE4B7BBC517700A0D (void);
// 0x00000533 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::add_OnBombsCountUpdated(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void CarController_add_OnBombsCountUpdated_mCB5FA081586D46D93AB29697FD09533A14DDA69C (void);
// 0x00000534 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::remove_OnBombsCountUpdated(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void CarController_remove_OnBombsCountUpdated_m9ED67AEA521FD6FFA8B99CA26579608D789BD487 (void);
// 0x00000535 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::add_OnPlayerDied(UnityEngine.Events.UnityAction)
extern void CarController_add_OnPlayerDied_m6393154C6F116D54E5BA4BD0C055343E7FE86671 (void);
// 0x00000536 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::remove_OnPlayerDied(UnityEngine.Events.UnityAction)
extern void CarController_remove_OnPlayerDied_m1F39B259AECDB17D904142491D9A8C77E119486D (void);
// 0x00000537 System.Single hardartcore.TheChase.Scripts.Controllers.CarController::get_Speed()
extern void CarController_get_Speed_mE4BAC885A1D3C6CA88EA22615A7ABEC8A099BE29 (void);
// 0x00000538 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::Awake()
extern void CarController_Awake_m1393A9B328D714F4F4F5AE8367E9E422D1E1541A (void);
// 0x00000539 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::OnEnable()
extern void CarController_OnEnable_m804623733A55494E0A98C8D4A1E91085331C71DB (void);
// 0x0000053A System.Void hardartcore.TheChase.Scripts.Controllers.CarController::OnDisable()
extern void CarController_OnDisable_m52B34C66FE824FC864813FC5BF8B35FA0921C95B (void);
// 0x0000053B System.Void hardartcore.TheChase.Scripts.Controllers.CarController::Start()
extern void CarController_Start_m3F4F08725FBD2BFC8381D492C7BC6BD485E7286B (void);
// 0x0000053C System.Void hardartcore.TheChase.Scripts.Controllers.CarController::BaseUpdate()
extern void CarController_BaseUpdate_m12B828AC3D1088F16B6912FF2136C66CF5131D54 (void);
// 0x0000053D System.Void hardartcore.TheChase.Scripts.Controllers.CarController::FixedUpdate()
extern void CarController_FixedUpdate_mE352E6855869538D867DD56B21EAE3C202F2BA3F (void);
// 0x0000053E System.Void hardartcore.TheChase.Scripts.Controllers.CarController::OnCollisionEnter(UnityEngine.Collision)
extern void CarController_OnCollisionEnter_mC907E6591F99D8E2910EC89E2EC9E831286B016F (void);
// 0x0000053F System.Void hardartcore.TheChase.Scripts.Controllers.CarController::yx()
extern void CarController_yx_m8F4D4BB878C1D12D9BBCB7888A5E1B8BC7A1E023 (void);
// 0x00000540 System.Collections.IEnumerator hardartcore.TheChase.Scripts.Controllers.CarController::yy()
extern void CarController_yy_mC578AB1B23351291FA8BFCB2749F8E72D41D08FC (void);
// 0x00000541 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::yz()
extern void CarController_yz_mF21A6441539EAB4DA03831DB745D16F495C2CBC7 (void);
// 0x00000542 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::za(hardartcore.TheChase.Scripts.ScriptableObjects.CarScriptableObject)
extern void CarController_za_mA19F86918065BA1B5F761976C4D8CE47600B526F (void);
// 0x00000543 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::zb(System.Boolean)
extern void CarController_zb_mA37947BCB02CD0A19CACF3504FE9A9D0125FA0F8 (void);
// 0x00000544 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::zc()
extern void CarController_zc_m38FE16AA6FF5E0FE9584B9CCDA10AC98008E93D0 (void);
// 0x00000545 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::Move(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void CarController_Move_m980B2CDD1D387898F893F93194849045E8D44143 (void);
// 0x00000546 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::Action(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void CarController_Action_m0164C9FBC668AEFECEAE87AAB8B872885FC01580 (void);
// 0x00000547 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::Boost()
extern void CarController_Boost_m42226244B953791EE52564CE7BA92F6FB67A8BFE (void);
// 0x00000548 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::AddBombs(System.Int32)
extern void CarController_AddBombs_m6B1F9993A7B54746C4712F302D53CED35013F247 (void);
// 0x00000549 System.Void hardartcore.TheChase.Scripts.Controllers.CarController::AddFuel(System.Int32)
extern void CarController_AddFuel_m68BA1B4A272D232014D150A37EE278F835CC26A9 (void);
// 0x0000054A System.Int32 hardartcore.TheChase.Scripts.Controllers.CarController::GetTravelledDistance()
extern void CarController_GetTravelledDistance_mBBF1B4CD1A28516D405137DE417E1E9F50079D16 (void);
// 0x0000054B System.Void hardartcore.TheChase.Scripts.Controllers.CarController::.ctor()
extern void CarController__ctor_m228A076830FFF5A8C1C8E2885FBD9345799EE7F5 (void);
// 0x0000054C System.Void hardartcore.TheChase.Scripts.Controllers.CarController/cp::.ctor(System.Int32)
extern void cp__ctor_m946EC04D2196063E33D82923EAF9148E9C228820 (void);
// 0x0000054D System.Void hardartcore.TheChase.Scripts.Controllers.CarController/cp::yt()
extern void cp_yt_mB1566E7002A0F9ED287F49E3E4ACF6B5D0132F69 (void);
// 0x0000054E System.Boolean hardartcore.TheChase.Scripts.Controllers.CarController/cp::MoveNext()
extern void cp_MoveNext_m54FD1CA200DAE943D6A14F23CEB3136A110C2A07 (void);
// 0x0000054F System.Object hardartcore.TheChase.Scripts.Controllers.CarController/cp::yu()
extern void cp_yu_mE0E4AFD03467A6C99A996FE5C48E2766FC47FE96 (void);
// 0x00000550 System.Void hardartcore.TheChase.Scripts.Controllers.CarController/cp::yv()
extern void cp_yv_m749C3C3E1E198302D9262134331A56903B0CC3DB (void);
// 0x00000551 System.Object hardartcore.TheChase.Scripts.Controllers.CarController/cp::yw()
extern void cp_yw_m7880EA5F510675A1D18E94C4BC5888301C116E5B (void);
// 0x00000552 System.Void hardartcore.TheChase.Scripts.Controllers.CopAIController::add_OnCopDied(UnityEngine.Events.UnityAction)
extern void CopAIController_add_OnCopDied_m1E76C9D2861AE18CDC19FE0DF72CBD299CDFF23A (void);
// 0x00000553 System.Void hardartcore.TheChase.Scripts.Controllers.CopAIController::remove_OnCopDied(UnityEngine.Events.UnityAction)
extern void CopAIController_remove_OnCopDied_m75F1A23CEAE16C40B402B05B1CE85B63562023E9 (void);
// 0x00000554 System.Void hardartcore.TheChase.Scripts.Controllers.CopAIController::OnEnable()
extern void CopAIController_OnEnable_m438317627FB0A480ED1298AEA26D4ED04F098A6F (void);
// 0x00000555 System.Void hardartcore.TheChase.Scripts.Controllers.CopAIController::BaseUpdate()
extern void CopAIController_BaseUpdate_mC8002FD51FDD923DC0077D9D9AF23EFF60585290 (void);
// 0x00000556 System.Void hardartcore.TheChase.Scripts.Controllers.CopAIController::FixedUpdate()
extern void CopAIController_FixedUpdate_m24DC85EC3955CBCF2D25FC0BF4C963355593E12E (void);
// 0x00000557 System.Void hardartcore.TheChase.Scripts.Controllers.CopAIController::OnCollisionEnter(UnityEngine.Collision)
extern void CopAIController_OnCollisionEnter_mA36162F243D5561374124CDE7A8E05D442F49532 (void);
// 0x00000558 System.Void hardartcore.TheChase.Scripts.Controllers.CopAIController::DisableCop()
extern void CopAIController_DisableCop_m8E407F7D4C6E8ED35915940FE302E6A6ED43056F (void);
// 0x00000559 System.Void hardartcore.TheChase.Scripts.Controllers.CopAIController::zd()
extern void CopAIController_zd_m29DA8284301F320D42A0892A8E543E6209BAFC8B (void);
// 0x0000055A System.Void hardartcore.TheChase.Scripts.Controllers.CopAIController::.ctor()
extern void CopAIController__ctor_mB393BE36AE297E2111C58777B19157950E08E2EC (void);
// 0x0000055B System.Void com.adjust.sdk.JSONNode::Add(System.String,com.adjust.sdk.JSONNode)
extern void JSONNode_Add_mA73D290ED9AFFE3F42861A87D20DD066CBF02BA3 (void);
// 0x0000055C com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::get_Item(System.Int32)
extern void JSONNode_get_Item_mD7BBCD16DB5D0500676559AFB2A2594B0103D8F3 (void);
// 0x0000055D System.Void com.adjust.sdk.JSONNode::set_Item(System.Int32,com.adjust.sdk.JSONNode)
extern void JSONNode_set_Item_mD4F5BEDADBDB66141EC0EB6F3E8E0C04A481CD6B (void);
// 0x0000055E com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::get_Item(System.String)
extern void JSONNode_get_Item_m85A1C5F8B26938A5BDD5CBA14C84E4A37B3D4ACF (void);
// 0x0000055F System.Void com.adjust.sdk.JSONNode::set_Item(System.String,com.adjust.sdk.JSONNode)
extern void JSONNode_set_Item_mD9F8F18C2B5F9054A722B4CFD8129E2C48A4BE5D (void);
// 0x00000560 System.String com.adjust.sdk.JSONNode::get_Value()
extern void JSONNode_get_Value_m422474020628AD90572986DC9999B5E33D51044F (void);
// 0x00000561 System.Void com.adjust.sdk.JSONNode::set_Value(System.String)
extern void JSONNode_set_Value_mF7DD7A968B9CE3DE2DEC99CFBB8F6A0035DA0BBC (void);
// 0x00000562 System.Int32 com.adjust.sdk.JSONNode::get_Count()
extern void JSONNode_get_Count_m6A898D357D50A5C2E8042AFCB3D0E795B3F0FB25 (void);
// 0x00000563 System.Void com.adjust.sdk.JSONNode::Add(com.adjust.sdk.JSONNode)
extern void JSONNode_Add_m8EE67AF49568BB6CC3DD0968C5E7E206EF3F4A99 (void);
// 0x00000564 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::Remove(System.String)
extern void JSONNode_Remove_mCC44CF5159F5A8661D1906F9444477EF5C990B71 (void);
// 0x00000565 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::Remove(System.Int32)
extern void JSONNode_Remove_m404F68BE1FB04E8F4B50EADF3490CAC177BB57FA (void);
// 0x00000566 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::Remove(com.adjust.sdk.JSONNode)
extern void JSONNode_Remove_m68BB8C3076B8DE3EB84D43AC8FEFA4057F2973A1 (void);
// 0x00000567 System.Collections.Generic.IEnumerable`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONNode::get_Childs()
extern void JSONNode_get_Childs_m041DDDFC000548A689CE29D3288D53AA643C3482 (void);
// 0x00000568 System.Collections.Generic.IEnumerable`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONNode::get_DeepChilds()
extern void JSONNode_get_DeepChilds_m44F560506E902A0B0DABB6B82562D127259FEFC5 (void);
// 0x00000569 System.String com.adjust.sdk.JSONNode::ToString()
extern void JSONNode_ToString_mCE094AA189C11D45C5915FA3D229C9FD1076A6E1 (void);
// 0x0000056A System.String com.adjust.sdk.JSONNode::ToString(System.String)
extern void JSONNode_ToString_m729A82FD7DD7412637589CDE0158F98B5E7C14C2 (void);
// 0x0000056B System.Int32 com.adjust.sdk.JSONNode::get_AsInt()
extern void JSONNode_get_AsInt_m06D351315628BE80806748B43605699EBD2724C7 (void);
// 0x0000056C System.Void com.adjust.sdk.JSONNode::set_AsInt(System.Int32)
extern void JSONNode_set_AsInt_m351063CC07FA09FFBF85FB543D40ADC1F90B751A (void);
// 0x0000056D System.Single com.adjust.sdk.JSONNode::get_AsFloat()
extern void JSONNode_get_AsFloat_m18888F5DF88D1ABD784270641319B7ABDDDBE9E6 (void);
// 0x0000056E System.Void com.adjust.sdk.JSONNode::set_AsFloat(System.Single)
extern void JSONNode_set_AsFloat_mD796198844BF326B7288864EB6C4D5AD4E510E9C (void);
// 0x0000056F System.Double com.adjust.sdk.JSONNode::get_AsDouble()
extern void JSONNode_get_AsDouble_m3CBB3A48ADA115310C2187857B1C71ABC7806752 (void);
// 0x00000570 System.Void com.adjust.sdk.JSONNode::set_AsDouble(System.Double)
extern void JSONNode_set_AsDouble_mA5DA00CF265864C946AA28CCBAC8A7FF4D22BA76 (void);
// 0x00000571 System.Boolean com.adjust.sdk.JSONNode::get_AsBool()
extern void JSONNode_get_AsBool_m8D64DD4B3FC875095E283DBE5AC867003123635F (void);
// 0x00000572 System.Void com.adjust.sdk.JSONNode::set_AsBool(System.Boolean)
extern void JSONNode_set_AsBool_mB969B70E7AF09CCF88B79BCF1FBFC03D9F8DB6AD (void);
// 0x00000573 com.adjust.sdk.JSONArray com.adjust.sdk.JSONNode::get_AsArray()
extern void JSONNode_get_AsArray_m7B937E045E18295E048B40D1B7C3A58E01F710F3 (void);
// 0x00000574 com.adjust.sdk.JSONClass com.adjust.sdk.JSONNode::get_AsObject()
extern void JSONNode_get_AsObject_m09F640638EA27AE9A75A66D413209E1B0A4C1700 (void);
// 0x00000575 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::op_Implicit(System.String)
extern void JSONNode_op_Implicit_m9105117784C6916F6B646D36A1370FB58A9C6762 (void);
// 0x00000576 System.String com.adjust.sdk.JSONNode::op_Implicit(com.adjust.sdk.JSONNode)
extern void JSONNode_op_Implicit_m8DDFBC3FCEA2F2087B7A132A483F724B1529B183 (void);
// 0x00000577 System.Boolean com.adjust.sdk.JSONNode::op_Equality(com.adjust.sdk.JSONNode,System.Object)
extern void JSONNode_op_Equality_mF53AB65ABCF70E4C7D035DF059648FED12577634 (void);
// 0x00000578 System.Boolean com.adjust.sdk.JSONNode::op_Inequality(com.adjust.sdk.JSONNode,System.Object)
extern void JSONNode_op_Inequality_m31F67DC83671EE7A334E8A1A0445AC08CFDD9BD5 (void);
// 0x00000579 System.Boolean com.adjust.sdk.JSONNode::Equals(System.Object)
extern void JSONNode_Equals_m024E691480E99CC5FCAC978BEA06E3C89AC936BE (void);
// 0x0000057A System.Int32 com.adjust.sdk.JSONNode::GetHashCode()
extern void JSONNode_GetHashCode_m205B8B106B815AF3CFC5FFAC4A4E253452BC08E5 (void);
// 0x0000057B System.String com.adjust.sdk.JSONNode::zs(System.String)
extern void JSONNode_zs_m12F77455895F24BBA9071F9946E381E85E01BB10 (void);
// 0x0000057C com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::Parse(System.String)
extern void JSONNode_Parse_mD6D2A7EBFFEDF36552C27F27CB53F9DBA6C9D12E (void);
// 0x0000057D System.Void com.adjust.sdk.JSONNode::Serialize(System.IO.BinaryWriter)
extern void JSONNode_Serialize_m7B9E652622D50367BB8DB62252A0ADDBD2A02A4F (void);
// 0x0000057E System.Void com.adjust.sdk.JSONNode::SaveToStream(System.IO.Stream)
extern void JSONNode_SaveToStream_mAAF83974E2DB60FD527965E28263B1202E195C27 (void);
// 0x0000057F System.Void com.adjust.sdk.JSONNode::SaveToCompressedStream(System.IO.Stream)
extern void JSONNode_SaveToCompressedStream_mDFE8495C0252DA781F53FB62A4E580D9267594B1 (void);
// 0x00000580 System.Void com.adjust.sdk.JSONNode::SaveToCompressedFile(System.String)
extern void JSONNode_SaveToCompressedFile_m5EE5A35989E44A29862AC7BA21F5C78179BCBD71 (void);
// 0x00000581 System.String com.adjust.sdk.JSONNode::SaveToCompressedBase64()
extern void JSONNode_SaveToCompressedBase64_mC5D7414CD46FEBB67BB8567FDA778D453B87FA7B (void);
// 0x00000582 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::Deserialize(System.IO.BinaryReader)
extern void JSONNode_Deserialize_m5C0519240162A7247E99053C69876E590587B38C (void);
// 0x00000583 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::LoadFromCompressedFile(System.String)
extern void JSONNode_LoadFromCompressedFile_mEB55B68104BA51A066485FBBC1CB6A1D16DAA504 (void);
// 0x00000584 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::LoadFromCompressedStream(System.IO.Stream)
extern void JSONNode_LoadFromCompressedStream_m7368A19FFACEDA51ED6E39DE7919FDF11836F030 (void);
// 0x00000585 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::LoadFromCompressedBase64(System.String)
extern void JSONNode_LoadFromCompressedBase64_mE4C615EAEC9DD685E6659BC69C77CDF7A6FE1CFB (void);
// 0x00000586 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::LoadFromStream(System.IO.Stream)
extern void JSONNode_LoadFromStream_mD78A68A7F71EE78FB1E12311DBFFFAA3E6AF8B69 (void);
// 0x00000587 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode::LoadFromBase64(System.String)
extern void JSONNode_LoadFromBase64_m33C55652F013772F68C8CA8B54CEE7EBE297B08C (void);
// 0x00000588 System.Void com.adjust.sdk.JSONNode::.ctor()
extern void JSONNode__ctor_mCC776C44E5B26CB440CD077D35CF116A45EAE5EA (void);
// 0x00000589 System.Void com.adjust.sdk.JSONNode/cq::.ctor(System.Int32)
extern void cq__ctor_m4044EAC23C652482EBBEB617F327E0FD2101017D (void);
// 0x0000058A System.Void com.adjust.sdk.JSONNode/cq::ze()
extern void cq_ze_mA27ED6EBE1AE5B216F8EB3816FC2CB90F5DDBA7C (void);
// 0x0000058B System.Boolean com.adjust.sdk.JSONNode/cq::MoveNext()
extern void cq_MoveNext_m1882B101044E36CA0447BA869438F2FDA5CA68A3 (void);
// 0x0000058C com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode/cq::zf()
extern void cq_zf_mBA3957DE8CDD716F30ABF9A0BCE59C34131389CC (void);
// 0x0000058D System.Void com.adjust.sdk.JSONNode/cq::zg()
extern void cq_zg_m51D111E08778B75ADC111542B9B7B5A73F5BE8F3 (void);
// 0x0000058E System.Object com.adjust.sdk.JSONNode/cq::zh()
extern void cq_zh_m9D39FF5B15496A972DFD8A0A828C8F5C3504E033 (void);
// 0x0000058F System.Collections.Generic.IEnumerator`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONNode/cq::zi()
extern void cq_zi_m4293FDE103E4214A8D0A5443523395E38F626C92 (void);
// 0x00000590 System.Collections.IEnumerator com.adjust.sdk.JSONNode/cq::zj()
extern void cq_zj_mAABB424EC28B01BFAD1680F387540F6573EB048C (void);
// 0x00000591 System.Void com.adjust.sdk.JSONNode/cr::.ctor(System.Int32)
extern void cr__ctor_mA576954E592AB5CF94A5FBC324EAC3AFF8D751FB (void);
// 0x00000592 System.Void com.adjust.sdk.JSONNode/cr::zk()
extern void cr_zk_mD3B92350D22B14145A9893F3713A61B1510175F8 (void);
// 0x00000593 System.Boolean com.adjust.sdk.JSONNode/cr::MoveNext()
extern void cr_MoveNext_m22F21D368C71A1D524F14596988DDC2F01A5EF05 (void);
// 0x00000594 System.Void com.adjust.sdk.JSONNode/cr::zl()
extern void cr_zl_mF82C4ACD5EA6A4F07DA23269277293BC2CD3990F (void);
// 0x00000595 System.Void com.adjust.sdk.JSONNode/cr::zm()
extern void cr_zm_m1C40AB63B5297DA0E216BBA5C3B3C710A04D280C (void);
// 0x00000596 com.adjust.sdk.JSONNode com.adjust.sdk.JSONNode/cr::zn()
extern void cr_zn_m688C0017487141EBF471594E7BD672DB25428255 (void);
// 0x00000597 System.Void com.adjust.sdk.JSONNode/cr::zo()
extern void cr_zo_m8898F6737EC87FA678B162993D2D026224E068B9 (void);
// 0x00000598 System.Object com.adjust.sdk.JSONNode/cr::zp()
extern void cr_zp_mDA099CB536AC248624C10901DCF03E8047C6F6F0 (void);
// 0x00000599 System.Collections.Generic.IEnumerator`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONNode/cr::zq()
extern void cr_zq_m3556EA3768B3A9414314130BAB820A6B31E6DB10 (void);
// 0x0000059A System.Collections.IEnumerator com.adjust.sdk.JSONNode/cr::zr()
extern void cr_zr_m8F21627A170FD142F78A9826B71AB91583641D24 (void);
// 0x0000059B com.adjust.sdk.JSONNode com.adjust.sdk.JSONArray::get_Item(System.Int32)
extern void JSONArray_get_Item_m755E3E8E6725A4FBF408E5BD1A8432C419410503 (void);
// 0x0000059C System.Void com.adjust.sdk.JSONArray::set_Item(System.Int32,com.adjust.sdk.JSONNode)
extern void JSONArray_set_Item_mC4EF1F71394FFDFAE7153C0AC8E698EE4AA5B9F5 (void);
// 0x0000059D com.adjust.sdk.JSONNode com.adjust.sdk.JSONArray::get_Item(System.String)
extern void JSONArray_get_Item_m70C52946E9D9B5DE99DBE66423561B152ECCB454 (void);
// 0x0000059E System.Void com.adjust.sdk.JSONArray::set_Item(System.String,com.adjust.sdk.JSONNode)
extern void JSONArray_set_Item_m2AB7F7C0919BABC722D5262F62960F58A0BCCFDE (void);
// 0x0000059F System.Int32 com.adjust.sdk.JSONArray::get_Count()
extern void JSONArray_get_Count_m810A578C961B778B760E51A3D0111CFB8152864B (void);
// 0x000005A0 System.Void com.adjust.sdk.JSONArray::Add(System.String,com.adjust.sdk.JSONNode)
extern void JSONArray_Add_m11ED297B0CE157395B41622C3A70577A4CCDEE1B (void);
// 0x000005A1 com.adjust.sdk.JSONNode com.adjust.sdk.JSONArray::Remove(System.Int32)
extern void JSONArray_Remove_m294856B07209D367010855B3B71CEACD50B8F6AF (void);
// 0x000005A2 com.adjust.sdk.JSONNode com.adjust.sdk.JSONArray::Remove(com.adjust.sdk.JSONNode)
extern void JSONArray_Remove_m6C5A0F04019B3D0DC675038B5B04AE38079B786A (void);
// 0x000005A3 System.Collections.Generic.IEnumerable`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONArray::get_Childs()
extern void JSONArray_get_Childs_mC749B8F046AD011544966486F60CA9C0C217A874 (void);
// 0x000005A4 System.Collections.IEnumerator com.adjust.sdk.JSONArray::GetEnumerator()
extern void JSONArray_GetEnumerator_m4E4EA5BBF194BA5E068CA738E573C73DD1ED5635 (void);
// 0x000005A5 System.String com.adjust.sdk.JSONArray::ToString()
extern void JSONArray_ToString_m58F4AFD74189F06A5380F48EB94330B1F62787AB (void);
// 0x000005A6 System.String com.adjust.sdk.JSONArray::ToString(System.String)
extern void JSONArray_ToString_m97E2C58394C45CEB1C54965B96C6E9CB19B8005B (void);
// 0x000005A7 System.Void com.adjust.sdk.JSONArray::Serialize(System.IO.BinaryWriter)
extern void JSONArray_Serialize_m85EA6F504087F2B91019ADACE0AF84BE56C977DF (void);
// 0x000005A8 System.Void com.adjust.sdk.JSONArray::.ctor()
extern void JSONArray__ctor_m6ECA2300A22DEFC3387A72AF03FEC3355B150C4E (void);
// 0x000005A9 System.Void com.adjust.sdk.JSONArray/cs::.ctor(System.Int32)
extern void cs__ctor_mD7515745585E31CAB8AA98CBA539BF0D587F667D (void);
// 0x000005AA System.Void com.adjust.sdk.JSONArray/cs::zt()
extern void cs_zt_m44770F3B3255682687E6507451F166B54301A8B7 (void);
// 0x000005AB System.Boolean com.adjust.sdk.JSONArray/cs::MoveNext()
extern void cs_MoveNext_mEC847BFD683E4B6EC7E0B4C454BB031BDCA4AA0A (void);
// 0x000005AC System.Void com.adjust.sdk.JSONArray/cs::zu()
extern void cs_zu_mE834DE4E0EB7D2992A9E005C0FE7E9D75E067C1F (void);
// 0x000005AD com.adjust.sdk.JSONNode com.adjust.sdk.JSONArray/cs::zv()
extern void cs_zv_m546C6B8CC1A6FA9422BCF0412BB7E48355952A3F (void);
// 0x000005AE System.Void com.adjust.sdk.JSONArray/cs::zw()
extern void cs_zw_m677C8BEEDDE68F61C7A8E6959295E5FEA4966750 (void);
// 0x000005AF System.Object com.adjust.sdk.JSONArray/cs::zx()
extern void cs_zx_m7284A5CB9B08A487AEE5E38C979715DCEB39C18C (void);
// 0x000005B0 System.Collections.Generic.IEnumerator`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONArray/cs::zy()
extern void cs_zy_mD2C3CEC74ED5ED243639021664D3C922FB5A842F (void);
// 0x000005B1 System.Collections.IEnumerator com.adjust.sdk.JSONArray/cs::zz()
extern void cs_zz_m265F6467EB8B11393055C3342B8A78494C7F7950 (void);
// 0x000005B2 System.Void com.adjust.sdk.JSONArray/ct::.ctor(System.Int32)
extern void ct__ctor_mDA1E8DFE4E336588B13B3D387378CB746FD70A53 (void);
// 0x000005B3 System.Void com.adjust.sdk.JSONArray/ct::baa()
extern void ct_baa_m797FCC0F026085CECC48930A6CCE4D3FB354F07E (void);
// 0x000005B4 System.Boolean com.adjust.sdk.JSONArray/ct::MoveNext()
extern void ct_MoveNext_m701B24977BAB7680C2C9CB5AC01C87BD3785FE1B (void);
// 0x000005B5 System.Void com.adjust.sdk.JSONArray/ct::bab()
extern void ct_bab_mD02935AAE79B0F307D99ADC8251A084FD7A5E88B (void);
// 0x000005B6 System.Object com.adjust.sdk.JSONArray/ct::bac()
extern void ct_bac_m1397738AFAD2164B0DDB0C77E0EA31137CCCA6F8 (void);
// 0x000005B7 System.Void com.adjust.sdk.JSONArray/ct::bad()
extern void ct_bad_mF193D25CF936A8DC3F709F444762A3D7899CE990 (void);
// 0x000005B8 System.Object com.adjust.sdk.JSONArray/ct::bae()
extern void ct_bae_m573870A631776F76FC63B777CAEF5726DDBBAA11 (void);
// 0x000005B9 com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass::get_Item(System.String)
extern void JSONClass_get_Item_m49CF33B11E4FCA72EBC0D1E89BFA1F64D1C82D93 (void);
// 0x000005BA System.Void com.adjust.sdk.JSONClass::set_Item(System.String,com.adjust.sdk.JSONNode)
extern void JSONClass_set_Item_m887D13A167F244972827C10AD304E445D273B72E (void);
// 0x000005BB com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass::get_Item(System.Int32)
extern void JSONClass_get_Item_m332F023B545862E9766531549B9A5D8276645551 (void);
// 0x000005BC System.Void com.adjust.sdk.JSONClass::set_Item(System.Int32,com.adjust.sdk.JSONNode)
extern void JSONClass_set_Item_m8E3A9CA1DEDAFDA4165B2D5CEE425BC01B829B71 (void);
// 0x000005BD System.Int32 com.adjust.sdk.JSONClass::get_Count()
extern void JSONClass_get_Count_mD26CAC564152CABFB3D996FE189D7184F6C9FB3C (void);
// 0x000005BE System.Void com.adjust.sdk.JSONClass::Add(System.String,com.adjust.sdk.JSONNode)
extern void JSONClass_Add_m8016B5271985A574BAC8E7745DE5F950299FFF38 (void);
// 0x000005BF com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass::Remove(System.String)
extern void JSONClass_Remove_mC59E957355660E1DE1D528916D23F86B3EB5F680 (void);
// 0x000005C0 com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass::Remove(System.Int32)
extern void JSONClass_Remove_m3A246109A77359EAAF01B6324AD2F436608DA680 (void);
// 0x000005C1 com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass::Remove(com.adjust.sdk.JSONNode)
extern void JSONClass_Remove_mDE17444DFA5339F564FBB7071E7913D9C1B0C877 (void);
// 0x000005C2 System.Collections.Generic.IEnumerable`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONClass::get_Childs()
extern void JSONClass_get_Childs_m7ED5CDF26A34F041E5CBF93B9EC616E940AEC925 (void);
// 0x000005C3 System.Collections.IEnumerator com.adjust.sdk.JSONClass::GetEnumerator()
extern void JSONClass_GetEnumerator_mC63BF81FA10F977320F60BFF46CDA748A7F09DAD (void);
// 0x000005C4 System.String com.adjust.sdk.JSONClass::ToString()
extern void JSONClass_ToString_m2851DE91AA6840330E1A2A64F5803D702AD3BF37 (void);
// 0x000005C5 System.String com.adjust.sdk.JSONClass::ToString(System.String)
extern void JSONClass_ToString_m82935C8A4E9EB9D8E5693077AB9603452F71B027 (void);
// 0x000005C6 System.Void com.adjust.sdk.JSONClass::Serialize(System.IO.BinaryWriter)
extern void JSONClass_Serialize_mBA8EAE5D8ADB2413DE2A726447B0037B5686BFF8 (void);
// 0x000005C7 System.Void com.adjust.sdk.JSONClass::.ctor()
extern void JSONClass__ctor_m02C51CEB21719D911A19C729D02745553A1EB446 (void);
// 0x000005C8 System.Void com.adjust.sdk.JSONClass/cu::.ctor()
extern void cu__ctor_m7D6B9EDF0DB9592830BA9C5CBFC521BCE83DB8AC (void);
// 0x000005C9 System.Boolean com.adjust.sdk.JSONClass/cu::baf(System.Collections.Generic.KeyValuePair`2<System.String,com.adjust.sdk.JSONNode>)
extern void cu_baf_m7B5B02F8EA750BAE1421D707C6620974BC74ED99 (void);
// 0x000005CA System.Void com.adjust.sdk.JSONClass/cv::.ctor(System.Int32)
extern void cv__ctor_mC3C1BB385D6A5DE18E76450B1666916C9130B5BE (void);
// 0x000005CB System.Void com.adjust.sdk.JSONClass/cv::bag()
extern void cv_bag_m87720C53BD286866D9328CBD7BCC6E76E30A39D3 (void);
// 0x000005CC System.Boolean com.adjust.sdk.JSONClass/cv::MoveNext()
extern void cv_MoveNext_m76AE02FFC3A2ACFC0CA3D9DA5829529D0656F9CF (void);
// 0x000005CD System.Void com.adjust.sdk.JSONClass/cv::bah()
extern void cv_bah_mEC8D55BA31540AE8CFD0114A4F88EE79B8AA8AC1 (void);
// 0x000005CE com.adjust.sdk.JSONNode com.adjust.sdk.JSONClass/cv::bai()
extern void cv_bai_mAFCB312A5EFA4F98D15EFA99F8094BBDCD1F0144 (void);
// 0x000005CF System.Void com.adjust.sdk.JSONClass/cv::baj()
extern void cv_baj_mFB41E64CDEF68685D327AF202B5AA5C1E5755ECB (void);
// 0x000005D0 System.Object com.adjust.sdk.JSONClass/cv::bak()
extern void cv_bak_m110DD3FA7A664D16ADD0A82B8C7146C4D414FC5E (void);
// 0x000005D1 System.Collections.Generic.IEnumerator`1<com.adjust.sdk.JSONNode> com.adjust.sdk.JSONClass/cv::bal()
extern void cv_bal_m35E55EC8830427D373E5D370B03E82AAC3D25CDE (void);
// 0x000005D2 System.Collections.IEnumerator com.adjust.sdk.JSONClass/cv::bam()
extern void cv_bam_mFD157059A06FC444A8959B843B4D1928A5B2A5CC (void);
// 0x000005D3 System.Void com.adjust.sdk.JSONClass/cw::.ctor(System.Int32)
extern void cw__ctor_m83E88076AE7AF0D4F42CE5BAAAC48659B5D0A6F4 (void);
// 0x000005D4 System.Void com.adjust.sdk.JSONClass/cw::ban()
extern void cw_ban_m4715EF672276CB85DD88E918478261824A78DA17 (void);
// 0x000005D5 System.Boolean com.adjust.sdk.JSONClass/cw::MoveNext()
extern void cw_MoveNext_m39848F7CE7CB5DC9CF577899DA649892CBFA1F00 (void);
// 0x000005D6 System.Void com.adjust.sdk.JSONClass/cw::bao()
extern void cw_bao_m906A91B10CEB60EDB3C05433A0DAEB58521FEE23 (void);
// 0x000005D7 System.Object com.adjust.sdk.JSONClass/cw::bap()
extern void cw_bap_mBAF1DBD60DF2E2ACF518E0124F6500F2F64D9C9F (void);
// 0x000005D8 System.Void com.adjust.sdk.JSONClass/cw::baq()
extern void cw_baq_mB94D7F9D5AB74C284A7424E5EC09279DE5F1AF6D (void);
// 0x000005D9 System.Object com.adjust.sdk.JSONClass/cw::bar()
extern void cw_bar_m9839CCDAA7A75B82C884BDAF6FD2EA95D0E494C1 (void);
// 0x000005DA System.String com.adjust.sdk.JSONData::get_Value()
extern void JSONData_get_Value_m3ADA5A705F607FB7C35D4605EA832E89599BE425 (void);
// 0x000005DB System.Void com.adjust.sdk.JSONData::set_Value(System.String)
extern void JSONData_set_Value_m217DDEC42EA58FCFA6CBC6EFE473BD448A16DBE2 (void);
// 0x000005DC System.Void com.adjust.sdk.JSONData::.ctor(System.String)
extern void JSONData__ctor_mF07078A36644CD1C44FD4394482FFF67BCCEEAC5 (void);
// 0x000005DD System.Void com.adjust.sdk.JSONData::.ctor(System.Single)
extern void JSONData__ctor_m0BA2DCE6697B2DF087570A042E0BD1D48B1B7AD2 (void);
// 0x000005DE System.Void com.adjust.sdk.JSONData::.ctor(System.Double)
extern void JSONData__ctor_m2EDE89D6B666DDFB6A4D2399A8462D82EC448AEE (void);
// 0x000005DF System.Void com.adjust.sdk.JSONData::.ctor(System.Boolean)
extern void JSONData__ctor_m0AB47342A4FDA49AF5B0775BE663DCE5A697B350 (void);
// 0x000005E0 System.Void com.adjust.sdk.JSONData::.ctor(System.Int32)
extern void JSONData__ctor_m312352D90C0634BDBABD84416086C56157235C3F (void);
// 0x000005E1 System.String com.adjust.sdk.JSONData::ToString()
extern void JSONData_ToString_m4C8593075AFF9E56B99908CEEA6988EB2AE61580 (void);
// 0x000005E2 System.String com.adjust.sdk.JSONData::ToString(System.String)
extern void JSONData_ToString_m67FDD9B98DF0DD19D397B6E210603DF3194840FD (void);
// 0x000005E3 System.Void com.adjust.sdk.JSONData::Serialize(System.IO.BinaryWriter)
extern void JSONData_Serialize_m509455814FED79C97E0C05F354839F51488D9D91 (void);
// 0x000005E4 System.Void com.adjust.sdk.cx::.ctor(com.adjust.sdk.JSONNode)
extern void cx__ctor_m9BD0C97D3C0C696535EF8966A0DCA823F1704B8D (void);
// 0x000005E5 System.Void com.adjust.sdk.cx::.ctor(com.adjust.sdk.JSONNode,System.String)
extern void cx__ctor_mE068864EA917C5A327317818E676C0E514416414 (void);
// 0x000005E6 System.Void com.adjust.sdk.cx::bas(com.adjust.sdk.JSONNode)
extern void cx_bas_m5F8A64329A9C11CD352D007E26BF3B7F01AF011C (void);
// 0x000005E7 com.adjust.sdk.JSONNode com.adjust.sdk.cx::get_Item(System.Int32)
extern void cx_get_Item_m203CFDFCE7924FDD3CF95AD900A8B080229E0E18 (void);
// 0x000005E8 System.Void com.adjust.sdk.cx::set_Item(System.Int32,com.adjust.sdk.JSONNode)
extern void cx_set_Item_m3BB206EF1D8A84818737B3BCADF49F5B7AC02AA2 (void);
// 0x000005E9 com.adjust.sdk.JSONNode com.adjust.sdk.cx::get_Item(System.String)
extern void cx_get_Item_m7CB753895D45B23D846E0C7E9DDFAA8BB0B81619 (void);
// 0x000005EA System.Void com.adjust.sdk.cx::set_Item(System.String,com.adjust.sdk.JSONNode)
extern void cx_set_Item_mE7C09FDAFB47C8B55F805C7691A392F05988BD68 (void);
// 0x000005EB System.Void com.adjust.sdk.cx::Add(com.adjust.sdk.JSONNode)
extern void cx_Add_m09AF39B907ED532773F1EFC5AE4B56D3AE0A4011 (void);
// 0x000005EC System.Void com.adjust.sdk.cx::Add(System.String,com.adjust.sdk.JSONNode)
extern void cx_Add_m632A7EA17AD7E47A2B20D13977C94F4E47BE59BE (void);
// 0x000005ED System.Boolean com.adjust.sdk.cx::op_Equality(com.adjust.sdk.cx,System.Object)
extern void cx_op_Equality_mDA5942C6916726F6F51A88EEB3BCFA05691241DD (void);
// 0x000005EE System.Boolean com.adjust.sdk.cx::op_Inequality(com.adjust.sdk.cx,System.Object)
extern void cx_op_Inequality_mB8DC05A734397941501FD283C409D8094BCCEC6B (void);
// 0x000005EF System.Boolean com.adjust.sdk.cx::Equals(System.Object)
extern void cx_Equals_m53C3E5BA4844BCCB0324EECD527C599D37FD0240 (void);
// 0x000005F0 System.Int32 com.adjust.sdk.cx::GetHashCode()
extern void cx_GetHashCode_m1FEBF53AA382CA8E42BFE9D8D22210227596A02E (void);
// 0x000005F1 System.String com.adjust.sdk.cx::ToString()
extern void cx_ToString_m230DE8B515EC7FCCB37E290B6B9E62C00E685601 (void);
// 0x000005F2 System.String com.adjust.sdk.cx::ToString(System.String)
extern void cx_ToString_m153F285B63E900796FC7D574369F1E37D5C211C2 (void);
// 0x000005F3 System.Int32 com.adjust.sdk.cx::get_AsInt()
extern void cx_get_AsInt_m9B88090A1763D0EA7F3FE801809E82C9795E24DC (void);
// 0x000005F4 System.Void com.adjust.sdk.cx::set_AsInt(System.Int32)
extern void cx_set_AsInt_m5425D36540774C637B7B729BCEC0C15C9810C028 (void);
// 0x000005F5 System.Single com.adjust.sdk.cx::get_AsFloat()
extern void cx_get_AsFloat_m8D79B0B6C23D6DDCF58D7C9AD2F02D14312ACCFF (void);
// 0x000005F6 System.Void com.adjust.sdk.cx::set_AsFloat(System.Single)
extern void cx_set_AsFloat_m8D0B74B6FB390D6056183DBF30D7ADC39971876E (void);
// 0x000005F7 System.Double com.adjust.sdk.cx::get_AsDouble()
extern void cx_get_AsDouble_m888A32BDCC84EEDAAE47DFF2EC2684D4EDF1B110 (void);
// 0x000005F8 System.Void com.adjust.sdk.cx::set_AsDouble(System.Double)
extern void cx_set_AsDouble_m24CECF617343501DB1103E42C9D6EA1A479C7C04 (void);
// 0x000005F9 System.Boolean com.adjust.sdk.cx::get_AsBool()
extern void cx_get_AsBool_mBF5BEB63AC409E24323DCCAEBCF4DADA0F247AE0 (void);
// 0x000005FA System.Void com.adjust.sdk.cx::set_AsBool(System.Boolean)
extern void cx_set_AsBool_m505B8BC2AE764F8D45F4439485C96C55CB2DCFE6 (void);
// 0x000005FB com.adjust.sdk.JSONArray com.adjust.sdk.cx::get_AsArray()
extern void cx_get_AsArray_mEE34DDDC408E0AD02D66F0A0C534D6003DBAB48A (void);
// 0x000005FC com.adjust.sdk.JSONClass com.adjust.sdk.cx::get_AsObject()
extern void cx_get_AsObject_m7C55ED22AC5ED4133ECB475D338EE60037E7A51B (void);
// 0x000005FD com.adjust.sdk.JSONNode com.adjust.sdk.JSON::Parse(System.String)
extern void JSON_Parse_m64D44E2E2DCDC73C4FC5A08B5D13F92209F2482E (void);
// 0x000005FE System.Void com.adjust.sdk.AdjustiOS::_AdjustLaunchApp(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int64,System.Int64,System.Int64,System.Int64,System.Int64,System.Double,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void AdjustiOS__AdjustLaunchApp_m48B7F15D15001A96F611AF64CAE22CE898367F0C (void);
// 0x000005FF System.Void com.adjust.sdk.AdjustiOS::_AdjustTrackEvent(System.String,System.Double,System.String,System.String,System.String,System.String,System.String,System.Int32,System.String,System.String)
extern void AdjustiOS__AdjustTrackEvent_mA076136EF8FB10FC204C569BEDCDD0030BC75EFF (void);
// 0x00000600 System.Void com.adjust.sdk.AdjustiOS::_AdjustSetEnabled(System.Int32)
extern void AdjustiOS__AdjustSetEnabled_m706DA3D25C337389171010CA5C9C1780466B8E28 (void);
// 0x00000601 System.Int32 com.adjust.sdk.AdjustiOS::_AdjustIsEnabled()
extern void AdjustiOS__AdjustIsEnabled_m6546882679AE7B220BE17EA489E51D70C12FF7C8 (void);
// 0x00000602 System.Void com.adjust.sdk.AdjustiOS::_AdjustSetOfflineMode(System.Int32)
extern void AdjustiOS__AdjustSetOfflineMode_m3BB67F740D3A2266C0B76C310ED0B441E9B6888A (void);
// 0x00000603 System.Void com.adjust.sdk.AdjustiOS::_AdjustSetDeviceToken(System.String)
extern void AdjustiOS__AdjustSetDeviceToken_m1AE42A3D7B1774D44B9624D84E130BA8114C6038 (void);
// 0x00000604 System.Void com.adjust.sdk.AdjustiOS::_AdjustAppWillOpenUrl(System.String)
extern void AdjustiOS__AdjustAppWillOpenUrl_mF7D7BF178886A07122CF342AA889EAB70F77F47E (void);
// 0x00000605 System.String com.adjust.sdk.AdjustiOS::_AdjustGetIdfa()
extern void AdjustiOS__AdjustGetIdfa_mD12F0F0C29EA4CCAFF80DE5BA48608C31702A63C (void);
// 0x00000606 System.String com.adjust.sdk.AdjustiOS::_AdjustGetAdid()
extern void AdjustiOS__AdjustGetAdid_m8647E5B2818A9E1CA464609A84C2BBA86DE721F0 (void);
// 0x00000607 System.String com.adjust.sdk.AdjustiOS::_AdjustGetSdkVersion()
extern void AdjustiOS__AdjustGetSdkVersion_m40F9E7EF215CDF9D06BEF5B06D646BCE42A5442F (void);
// 0x00000608 System.Void com.adjust.sdk.AdjustiOS::_AdjustGdprForgetMe()
extern void AdjustiOS__AdjustGdprForgetMe_m137841B602947A115C668A382491D340401CC504 (void);
// 0x00000609 System.Void com.adjust.sdk.AdjustiOS::_AdjustDisableThirdPartySharing()
extern void AdjustiOS__AdjustDisableThirdPartySharing_m2C4EE65750742ED9E13E6106F33D67A86864CC13 (void);
// 0x0000060A System.String com.adjust.sdk.AdjustiOS::_AdjustGetAttribution()
extern void AdjustiOS__AdjustGetAttribution_m60CAD18B259E45D37F5507601E1E62769D392E6D (void);
// 0x0000060B System.Void com.adjust.sdk.AdjustiOS::_AdjustSendFirstPackages()
extern void AdjustiOS__AdjustSendFirstPackages_m7A8D2B989AA0E74CE4412F1F56977B9F546FBA16 (void);
// 0x0000060C System.Void com.adjust.sdk.AdjustiOS::_AdjustAddSessionPartnerParameter(System.String,System.String)
extern void AdjustiOS__AdjustAddSessionPartnerParameter_m49AF272411EFFF3E2EDB7479D89BE858D1C6BA5F (void);
// 0x0000060D System.Void com.adjust.sdk.AdjustiOS::_AdjustAddSessionCallbackParameter(System.String,System.String)
extern void AdjustiOS__AdjustAddSessionCallbackParameter_m25B46D6EEAD09B7F24D50CE736C486159B6EB7DC (void);
// 0x0000060E System.Void com.adjust.sdk.AdjustiOS::_AdjustRemoveSessionPartnerParameter(System.String)
extern void AdjustiOS__AdjustRemoveSessionPartnerParameter_m8F69CA9E7048C0F72C516DE519F39978C496F5C6 (void);
// 0x0000060F System.Void com.adjust.sdk.AdjustiOS::_AdjustRemoveSessionCallbackParameter(System.String)
extern void AdjustiOS__AdjustRemoveSessionCallbackParameter_mFDDC161139248B53E9BE4E1E9C0D65834B1B99A6 (void);
// 0x00000610 System.Void com.adjust.sdk.AdjustiOS::_AdjustResetSessionPartnerParameters()
extern void AdjustiOS__AdjustResetSessionPartnerParameters_m1C40932340A054A4DDD734653591F810DB34BF26 (void);
// 0x00000611 System.Void com.adjust.sdk.AdjustiOS::_AdjustResetSessionCallbackParameters()
extern void AdjustiOS__AdjustResetSessionCallbackParameters_m1872CBAFD960F0325E569456106EBC5DCB10C23E (void);
// 0x00000612 System.Void com.adjust.sdk.AdjustiOS::_AdjustTrackAdRevenue(System.String,System.String)
extern void AdjustiOS__AdjustTrackAdRevenue_m9876079F57C48B10782219549DC6196C631B2B0C (void);
// 0x00000613 System.Void com.adjust.sdk.AdjustiOS::_AdjustTrackAdRevenueNew(System.String,System.Double,System.String,System.Int32,System.String,System.String,System.String,System.String,System.String)
extern void AdjustiOS__AdjustTrackAdRevenueNew_m474166EB4172174A11A87399BC9B354FC3E80DB1 (void);
// 0x00000614 System.Void com.adjust.sdk.AdjustiOS::_AdjustTrackAppStoreSubscription(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern void AdjustiOS__AdjustTrackAppStoreSubscription_m8E52F62DE40B61892DE25621515EAADE24BEB45B (void);
// 0x00000615 System.Void com.adjust.sdk.AdjustiOS::_AdjustTrackThirdPartySharing(System.Int32,System.String,System.String)
extern void AdjustiOS__AdjustTrackThirdPartySharing_m7AE5A4E423F25FA2940B02926E56DFEFCA3AD544 (void);
// 0x00000616 System.Void com.adjust.sdk.AdjustiOS::_AdjustTrackMeasurementConsent(System.Int32)
extern void AdjustiOS__AdjustTrackMeasurementConsent_mD7716533FDD4FC8A6DA98F8E12CA1F87D4A9AC37 (void);
// 0x00000617 System.Void com.adjust.sdk.AdjustiOS::_AdjustSetTestOptions(System.String,System.String,System.String,System.String,System.String,System.Int64,System.Int64,System.Int64,System.Int64,System.Int32,System.Int32,System.Int32,System.Int32)
extern void AdjustiOS__AdjustSetTestOptions_m46134BDA07D02C0C98C58B9D8AE9E8F153C8E10F (void);
// 0x00000618 System.Void com.adjust.sdk.AdjustiOS::_AdjustRequestTrackingAuthorizationWithCompletionHandler(System.String)
extern void AdjustiOS__AdjustRequestTrackingAuthorizationWithCompletionHandler_m337778AEC74ACC7EF9675B1E4CCC68B53B125AA5 (void);
// 0x00000619 System.Void com.adjust.sdk.AdjustiOS::_AdjustUpdateConversionValue(System.Int32)
extern void AdjustiOS__AdjustUpdateConversionValue_m92D7E58F26D4F431025B2987FD3AF2E7B7C8AAA3 (void);
// 0x0000061A System.Void com.adjust.sdk.AdjustiOS::_AdjustUpdateConversionValueWithCallback(System.Int32,System.String)
extern void AdjustiOS__AdjustUpdateConversionValueWithCallback_mDC7B4FAC0E48D800C52D5C43866D1FEAACCF4A0D (void);
// 0x0000061B System.Void com.adjust.sdk.AdjustiOS::_AdjustUpdateConversionValueWithCallbackSkad4(System.Int32,System.String,System.Int32,System.String)
extern void AdjustiOS__AdjustUpdateConversionValueWithCallbackSkad4_m05CAD07C12C399EA18F45403421466C82C953422 (void);
// 0x0000061C System.Void com.adjust.sdk.AdjustiOS::_AdjustCheckForNewAttStatus()
extern void AdjustiOS__AdjustCheckForNewAttStatus_m0D3815DD047C72CB1970334B83A1C1B859A9810D (void);
// 0x0000061D System.Int32 com.adjust.sdk.AdjustiOS::_AdjustGetAppTrackingAuthorizationStatus()
extern void AdjustiOS__AdjustGetAppTrackingAuthorizationStatus_mC6724413894E2E28F21D584E29175A89B2199673 (void);
// 0x0000061E System.Void com.adjust.sdk.AdjustiOS::_AdjustTrackSubsessionStart()
extern void AdjustiOS__AdjustTrackSubsessionStart_m24D23D6D287CB825CB4547CD73E27439DCE1FBA6 (void);
// 0x0000061F System.Void com.adjust.sdk.AdjustiOS::_AdjustTrackSubsessionEnd()
extern void AdjustiOS__AdjustTrackSubsessionEnd_mF1B9FCF570312D80CD5E6F78F51FB602B35E8561 (void);
// 0x00000620 System.String com.adjust.sdk.AdjustiOS::_AdjustGetLastDeeplink()
extern void AdjustiOS__AdjustGetLastDeeplink_mE482EFF8C64642E746FEFCA539C306EF2A3B2DBE (void);
// 0x00000621 System.Void com.adjust.sdk.AdjustiOS::_AdjustVerifyAppStorePurchase(System.String,System.String,System.String,System.String)
extern void AdjustiOS__AdjustVerifyAppStorePurchase_m67CE68ED39D744F0445E25628635C901B9DFFD09 (void);
// 0x00000622 System.Void com.adjust.sdk.AdjustiOS::.ctor()
extern void AdjustiOS__ctor_m2004768A8ED7B143D79B19053ADFDB7F779F846B (void);
// 0x00000623 System.Void com.adjust.sdk.AdjustiOS::Start(com.adjust.sdk.AdjustConfig)
extern void AdjustiOS_Start_mDC27C5DAAE652685F628EDC7668F67134484793F (void);
// 0x00000624 System.Void com.adjust.sdk.AdjustiOS::TrackEvent(com.adjust.sdk.AdjustEvent)
extern void AdjustiOS_TrackEvent_m9A44E273F8844ADD9186D9266655B257EA0B608C (void);
// 0x00000625 System.Void com.adjust.sdk.AdjustiOS::SetEnabled(System.Boolean)
extern void AdjustiOS_SetEnabled_m52758F3082AF7EBB207DECCA5111E3058443A27A (void);
// 0x00000626 System.Boolean com.adjust.sdk.AdjustiOS::IsEnabled()
extern void AdjustiOS_IsEnabled_m2F34A7E95874105C1785D08A1271E06DB3BD800C (void);
// 0x00000627 System.Void com.adjust.sdk.AdjustiOS::SetOfflineMode(System.Boolean)
extern void AdjustiOS_SetOfflineMode_m682E9FF10715A461A311D0E1738C59B6B523D6C9 (void);
// 0x00000628 System.Void com.adjust.sdk.AdjustiOS::SendFirstPackages()
extern void AdjustiOS_SendFirstPackages_m013286B86A0037ED04FE641A8E707A2E2C17C219 (void);
// 0x00000629 System.Void com.adjust.sdk.AdjustiOS::AppWillOpenUrl(System.String)
extern void AdjustiOS_AppWillOpenUrl_mC9E8DFA392B4C6062E9B8E3D0CA35567D86243D5 (void);
// 0x0000062A System.Void com.adjust.sdk.AdjustiOS::AddSessionPartnerParameter(System.String,System.String)
extern void AdjustiOS_AddSessionPartnerParameter_m095E9BC036146C18D12F91C72ABF1E97A4C655AA (void);
// 0x0000062B System.Void com.adjust.sdk.AdjustiOS::AddSessionCallbackParameter(System.String,System.String)
extern void AdjustiOS_AddSessionCallbackParameter_mCF436C86975D262B880040D958EF92BA94D0BF16 (void);
// 0x0000062C System.Void com.adjust.sdk.AdjustiOS::RemoveSessionPartnerParameter(System.String)
extern void AdjustiOS_RemoveSessionPartnerParameter_m5772B4D6BCCB899D88243A08B51D84BC0D695AC3 (void);
// 0x0000062D System.Void com.adjust.sdk.AdjustiOS::RemoveSessionCallbackParameter(System.String)
extern void AdjustiOS_RemoveSessionCallbackParameter_m521A79D726DB870ECEE835A3057497D14E82E042 (void);
// 0x0000062E System.Void com.adjust.sdk.AdjustiOS::ResetSessionPartnerParameters()
extern void AdjustiOS_ResetSessionPartnerParameters_mE9DC21EF018307277A35AE8FAF94D07C933C4170 (void);
// 0x0000062F System.Void com.adjust.sdk.AdjustiOS::ResetSessionCallbackParameters()
extern void AdjustiOS_ResetSessionCallbackParameters_m5F8CABA15F440EDAEDB9F4172BE8DF8EE52EF16C (void);
// 0x00000630 System.Void com.adjust.sdk.AdjustiOS::TrackAdRevenue(System.String,System.String)
extern void AdjustiOS_TrackAdRevenue_m81E71CFC6CD90555EA9DA436A4059405ADACD07C (void);
// 0x00000631 System.Void com.adjust.sdk.AdjustiOS::TrackAdRevenue(com.adjust.sdk.AdjustAdRevenue)
extern void AdjustiOS_TrackAdRevenue_mAE68D4D7959CBC41EB171AA4C1F8D5607978CF35 (void);
// 0x00000632 System.Void com.adjust.sdk.AdjustiOS::TrackAppStoreSubscription(com.adjust.sdk.AdjustAppStoreSubscription)
extern void AdjustiOS_TrackAppStoreSubscription_m2D3A411D472D7690014DE47B0CCABD3AAF0898DA (void);
// 0x00000633 System.Void com.adjust.sdk.AdjustiOS::TrackThirdPartySharing(com.adjust.sdk.AdjustThirdPartySharing)
extern void AdjustiOS_TrackThirdPartySharing_m3DCDD5683B6320AD01C259F79491EB2BF87832E3 (void);
// 0x00000634 System.Void com.adjust.sdk.AdjustiOS::TrackMeasurementConsent(System.Boolean)
extern void AdjustiOS_TrackMeasurementConsent_m5C7390162C7F37B704B7230B80779F3EF53D5A20 (void);
// 0x00000635 System.Void com.adjust.sdk.AdjustiOS::RequestTrackingAuthorizationWithCompletionHandler(System.String)
extern void AdjustiOS_RequestTrackingAuthorizationWithCompletionHandler_m95594475A57F52D6E55A438D965B3EEC6E878279 (void);
// 0x00000636 System.Void com.adjust.sdk.AdjustiOS::UpdateConversionValue(System.Int32)
extern void AdjustiOS_UpdateConversionValue_m9B549F0ED21C26F73419A529082CD35EB054BC4B (void);
// 0x00000637 System.Void com.adjust.sdk.AdjustiOS::UpdateConversionValue(System.Int32,System.String)
extern void AdjustiOS_UpdateConversionValue_m4DA9BC581EA2D9E6F2F9AB5C576A33916B28F543 (void);
// 0x00000638 System.Void com.adjust.sdk.AdjustiOS::UpdateConversionValue(System.Int32,System.String,System.Boolean,System.String)
extern void AdjustiOS_UpdateConversionValue_mA9166832F19A58739C6F43980D3C1D1048AA03E2 (void);
// 0x00000639 System.Void com.adjust.sdk.AdjustiOS::CheckForNewAttStatus()
extern void AdjustiOS_CheckForNewAttStatus_m6ED68A3566C06CC5BAE83D9DE65909F9ABD52F3A (void);
// 0x0000063A System.Int32 com.adjust.sdk.AdjustiOS::GetAppTrackingAuthorizationStatus()
extern void AdjustiOS_GetAppTrackingAuthorizationStatus_m4360763287FF0595EABEF07C0F81F1E50A189434 (void);
// 0x0000063B System.Void com.adjust.sdk.AdjustiOS::SetDeviceToken(System.String)
extern void AdjustiOS_SetDeviceToken_mED18B01FEBCA5CC835B823C676ABB3A3FD3D150C (void);
// 0x0000063C System.String com.adjust.sdk.AdjustiOS::GetIdfa()
extern void AdjustiOS_GetIdfa_m90B5A77AF3A65336435F770A2D5E1260322653E1 (void);
// 0x0000063D System.String com.adjust.sdk.AdjustiOS::GetAdid()
extern void AdjustiOS_GetAdid_mF436DCAA79328E5C6B8E7B024E7E7392FBB3B451 (void);
// 0x0000063E System.String com.adjust.sdk.AdjustiOS::GetSdkVersion()
extern void AdjustiOS_GetSdkVersion_mAE7A8E2C96A8B5EB3F37442E81965A62D50D721F (void);
// 0x0000063F System.Void com.adjust.sdk.AdjustiOS::GdprForgetMe()
extern void AdjustiOS_GdprForgetMe_m72283B010575C38702DC11B6E7C7AB49266A7D83 (void);
// 0x00000640 System.Void com.adjust.sdk.AdjustiOS::DisableThirdPartySharing()
extern void AdjustiOS_DisableThirdPartySharing_m87D57BD5B6ABBEEF045CD2ADFB06A1CC974FD27F (void);
// 0x00000641 com.adjust.sdk.AdjustAttribution com.adjust.sdk.AdjustiOS::GetAttribution()
extern void AdjustiOS_GetAttribution_m567CBC2E722BFDEA812159738FEF97CA1822CE6E (void);
// 0x00000642 System.String com.adjust.sdk.AdjustiOS::GetLastDeeplink()
extern void AdjustiOS_GetLastDeeplink_m59E6BDE8479809D82440F075167F7FD7415561BE (void);
// 0x00000643 System.Void com.adjust.sdk.AdjustiOS::VerifyAppStorePurchase(com.adjust.sdk.AdjustAppStorePurchase,System.String)
extern void AdjustiOS_VerifyAppStorePurchase_m4CF366F97B6516A802682FC807B00EA3C6A6332F (void);
// 0x00000644 System.Void com.adjust.sdk.AdjustiOS::SetTestOptions(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustiOS_SetTestOptions_m8E6BE3E6E960B581D19C47D13CA8C49BBC3557BC (void);
// 0x00000645 System.Void com.adjust.sdk.AdjustiOS::TrackSubsessionStart(System.String)
extern void AdjustiOS_TrackSubsessionStart_m8C073ADE4C06B7037A1557ACD9257D77582AADCF (void);
// 0x00000646 System.Void com.adjust.sdk.AdjustiOS::TrackSubsessionEnd(System.String)
extern void AdjustiOS_TrackSubsessionEnd_mBEEC538934EE716E4380FDBD582CBBF4D5C4321A (void);
// 0x00000647 System.Void com.adjust.sdk.Adjust::Awake()
extern void Adjust_Awake_m1B0E9298029BFF09C771F77DF8181CADFCB63BA8 (void);
// 0x00000648 System.Void com.adjust.sdk.Adjust::OnApplicationPause(System.Boolean)
extern void Adjust_OnApplicationPause_m3ADC342D8050B80840CB85B003EBCF689C8E4012 (void);
// 0x00000649 System.Void com.adjust.sdk.Adjust::start(com.adjust.sdk.AdjustConfig)
extern void Adjust_start_mF24352A04B12F9A3D5314851E1F476DD4BCBF0E3 (void);
// 0x0000064A System.Void com.adjust.sdk.Adjust::trackEvent(com.adjust.sdk.AdjustEvent)
extern void Adjust_trackEvent_m788CBA9B9C606FE179B1582368C6A8A171425E36 (void);
// 0x0000064B System.Void com.adjust.sdk.Adjust::setEnabled(System.Boolean)
extern void Adjust_setEnabled_mEF3633C6A6BBC4F91FCDB4F934A281DDE96712F1 (void);
// 0x0000064C System.Boolean com.adjust.sdk.Adjust::isEnabled()
extern void Adjust_isEnabled_m254951D8A14448BE51FD7527AC80CC3950E6EE4B (void);
// 0x0000064D System.Void com.adjust.sdk.Adjust::setOfflineMode(System.Boolean)
extern void Adjust_setOfflineMode_mDA3666A20F780FFDD8BCC07F9DB4D3215823C360 (void);
// 0x0000064E System.Void com.adjust.sdk.Adjust::setDeviceToken(System.String)
extern void Adjust_setDeviceToken_mC1407160399AC998FF11E556CBFA3B38950B67A8 (void);
// 0x0000064F System.Void com.adjust.sdk.Adjust::gdprForgetMe()
extern void Adjust_gdprForgetMe_m7A0CFD6A9B4137418F35E5AA201B6410737359C0 (void);
// 0x00000650 System.Void com.adjust.sdk.Adjust::disableThirdPartySharing()
extern void Adjust_disableThirdPartySharing_m302224B75CA744974396F66A08AA1FE587873BE5 (void);
// 0x00000651 System.Void com.adjust.sdk.Adjust::appWillOpenUrl(System.String)
extern void Adjust_appWillOpenUrl_m1E13932CE37598AA3C42C4509D2323491569A6F7 (void);
// 0x00000652 System.Void com.adjust.sdk.Adjust::sendFirstPackages()
extern void Adjust_sendFirstPackages_mCFE8665FB15B08EC04CB5BCF14C2923E5189883F (void);
// 0x00000653 System.Void com.adjust.sdk.Adjust::addSessionPartnerParameter(System.String,System.String)
extern void Adjust_addSessionPartnerParameter_m4AC3D2786FFF94A176E93DEBF049FCC6BB71B3E6 (void);
// 0x00000654 System.Void com.adjust.sdk.Adjust::addSessionCallbackParameter(System.String,System.String)
extern void Adjust_addSessionCallbackParameter_mCCA2594D1EAADD151C56F4537823EBBFE3EA645F (void);
// 0x00000655 System.Void com.adjust.sdk.Adjust::removeSessionPartnerParameter(System.String)
extern void Adjust_removeSessionPartnerParameter_mA50E1FF8D276CA300DFBC2D8C52E1C60194BD98C (void);
// 0x00000656 System.Void com.adjust.sdk.Adjust::removeSessionCallbackParameter(System.String)
extern void Adjust_removeSessionCallbackParameter_m9D480CA9958869ACA6205BCA9CE84D235B4E45B1 (void);
// 0x00000657 System.Void com.adjust.sdk.Adjust::resetSessionPartnerParameters()
extern void Adjust_resetSessionPartnerParameters_m788D347452CE9472C4B4BFCB581272C33A903459 (void);
// 0x00000658 System.Void com.adjust.sdk.Adjust::resetSessionCallbackParameters()
extern void Adjust_resetSessionCallbackParameters_m092D40CDF59B8BE4FB165BB105F914E67E61D7B4 (void);
// 0x00000659 System.Void com.adjust.sdk.Adjust::trackAdRevenue(System.String,System.String)
extern void Adjust_trackAdRevenue_mE7017F85963C48E260AE4A390B6E03C1365CEC8F (void);
// 0x0000065A System.Void com.adjust.sdk.Adjust::trackAdRevenue(com.adjust.sdk.AdjustAdRevenue)
extern void Adjust_trackAdRevenue_m7F1739F079028F6AFA4F42629B803A152F0BB9AE (void);
// 0x0000065B System.Void com.adjust.sdk.Adjust::trackAppStoreSubscription(com.adjust.sdk.AdjustAppStoreSubscription)
extern void Adjust_trackAppStoreSubscription_m3BE586B120A00A43CD00D754363EE0548488EB5B (void);
// 0x0000065C System.Void com.adjust.sdk.Adjust::trackPlayStoreSubscription(com.adjust.sdk.AdjustPlayStoreSubscription)
extern void Adjust_trackPlayStoreSubscription_mB28584B6B95978DD627EF386DE383E258A55C394 (void);
// 0x0000065D System.Void com.adjust.sdk.Adjust::trackThirdPartySharing(com.adjust.sdk.AdjustThirdPartySharing)
extern void Adjust_trackThirdPartySharing_mDBB4F949AA7D3532188DB67890C38CF006E5C3AE (void);
// 0x0000065E System.Void com.adjust.sdk.Adjust::trackMeasurementConsent(System.Boolean)
extern void Adjust_trackMeasurementConsent_m28091C4476B42F91B98E2864860109C8B2FF7F3C (void);
// 0x0000065F System.Void com.adjust.sdk.Adjust::requestTrackingAuthorizationWithCompletionHandler(System.Action`1<System.Int32>,System.String)
extern void Adjust_requestTrackingAuthorizationWithCompletionHandler_m0F4258A1D04183560AE8A0DB926BD1DD063330D2 (void);
// 0x00000660 System.Void com.adjust.sdk.Adjust::updateConversionValue(System.Int32)
extern void Adjust_updateConversionValue_mB83B123737964C3CDD68E5B94588575C973EEADF (void);
// 0x00000661 System.Void com.adjust.sdk.Adjust::updateConversionValue(System.Int32,System.Action`1<System.String>,System.String)
extern void Adjust_updateConversionValue_m22B9DD112274E04D0CE5267196B8FC850FF32BF0 (void);
// 0x00000662 System.Void com.adjust.sdk.Adjust::updateConversionValue(System.Int32,System.String,System.Boolean,System.Action`1<System.String>,System.String)
extern void Adjust_updateConversionValue_m7619FC95F34018A0C8B115B114417776FB016C2A (void);
// 0x00000663 System.Void com.adjust.sdk.Adjust::checkForNewAttStatus()
extern void Adjust_checkForNewAttStatus_mE8CA001423E1EFFB7D610102747D76F5B2DE02AF (void);
// 0x00000664 System.Int32 com.adjust.sdk.Adjust::getAppTrackingAuthorizationStatus()
extern void Adjust_getAppTrackingAuthorizationStatus_m2CD6E53C3C56055BEF58BCA99C30417FE4231006 (void);
// 0x00000665 System.String com.adjust.sdk.Adjust::getAdid()
extern void Adjust_getAdid_m2285DFA62339E5ED400D271E8E661FD0174600A6 (void);
// 0x00000666 com.adjust.sdk.AdjustAttribution com.adjust.sdk.Adjust::getAttribution()
extern void Adjust_getAttribution_m3B7BBB4900BDCC0B92D6A103FE178D9E808F2188 (void);
// 0x00000667 System.String com.adjust.sdk.Adjust::getWinAdid()
extern void Adjust_getWinAdid_m528B95DD3250ED9D5BCAB2B58C3B8CD7F6B940F6 (void);
// 0x00000668 System.String com.adjust.sdk.Adjust::getIdfa()
extern void Adjust_getIdfa_mB068686DAF448330241C2367A2767891EADA60CB (void);
// 0x00000669 System.String com.adjust.sdk.Adjust::getSdkVersion()
extern void Adjust_getSdkVersion_m8F7D96C6A76363F65E34D362C4912D3D5DA34E7D (void);
// 0x0000066A System.Void com.adjust.sdk.Adjust::setReferrer(System.String)
extern void Adjust_setReferrer_m223FB53F552416333FC5B55D1E6F3D5DD5F9270B (void);
// 0x0000066B System.Void com.adjust.sdk.Adjust::getGoogleAdId(System.Action`1<System.String>)
extern void Adjust_getGoogleAdId_m9F83C2BEA8B17987BC6BD492C1593444D427CB06 (void);
// 0x0000066C System.String com.adjust.sdk.Adjust::getAmazonAdId()
extern void Adjust_getAmazonAdId_mCF6657242C0F74B3D50BD6C412C91311CFE96688 (void);
// 0x0000066D System.String com.adjust.sdk.Adjust::getLastDeeplink()
extern void Adjust_getLastDeeplink_mF3BC9E789AEB1C8FB8C54BEDF98F3D04049D2D66 (void);
// 0x0000066E System.Void com.adjust.sdk.Adjust::verifyAppStorePurchase(com.adjust.sdk.AdjustAppStorePurchase,System.Action`1<com.adjust.sdk.AdjustPurchaseVerificationInfo>,System.String)
extern void Adjust_verifyAppStorePurchase_mE3B63AAEDB2569792D5167200AEFE589E69AE2FC (void);
// 0x0000066F System.Void com.adjust.sdk.Adjust::verifyPlayStorePurchase(com.adjust.sdk.AdjustPlayStorePurchase,System.Action`1<com.adjust.sdk.AdjustPurchaseVerificationInfo>)
extern void Adjust_verifyPlayStorePurchase_m769B3D59149BABD106127A3D5E9C84F67CFA3865 (void);
// 0x00000670 System.Void com.adjust.sdk.Adjust::GetNativeAttribution(System.String)
extern void Adjust_GetNativeAttribution_mC07ACAADB8E276755784A0A592779820032C4C18 (void);
// 0x00000671 System.Void com.adjust.sdk.Adjust::GetNativeEventSuccess(System.String)
extern void Adjust_GetNativeEventSuccess_m0E93EE7CEAA6FFE60BDE52C2ABE236C93278CD69 (void);
// 0x00000672 System.Void com.adjust.sdk.Adjust::GetNativeEventFailure(System.String)
extern void Adjust_GetNativeEventFailure_m73DD7A70DBE04F90F8B5B8E83B35ABC8EF02B42F (void);
// 0x00000673 System.Void com.adjust.sdk.Adjust::GetNativeSessionSuccess(System.String)
extern void Adjust_GetNativeSessionSuccess_mF82661EBAB1FCB7EAD4E14096B48F6F74941F09F (void);
// 0x00000674 System.Void com.adjust.sdk.Adjust::GetNativeSessionFailure(System.String)
extern void Adjust_GetNativeSessionFailure_mDEB4E95D140D4168954E922418B64E1C84F60D90 (void);
// 0x00000675 System.Void com.adjust.sdk.Adjust::GetNativeDeferredDeeplink(System.String)
extern void Adjust_GetNativeDeferredDeeplink_m3881E728E772FE695964BB594E54E7FE2BF9F6F0 (void);
// 0x00000676 System.Void com.adjust.sdk.Adjust::GetNativeConversionValueUpdated(System.String)
extern void Adjust_GetNativeConversionValueUpdated_m137A58ED56529A7A6EE2D9F4D9DB8BC2E902B581 (void);
// 0x00000677 System.Void com.adjust.sdk.Adjust::GetNativeSkad4ConversionValueUpdated(System.String)
extern void Adjust_GetNativeSkad4ConversionValueUpdated_m071B1B5ADFCA375028CF566698A2F4F6156607C5 (void);
// 0x00000678 System.Void com.adjust.sdk.Adjust::GetNativeSkadCompletionDelegate(System.String)
extern void Adjust_GetNativeSkadCompletionDelegate_mF7D29D0B2A1AA7D38B1C4923DFC34072920B3FDC (void);
// 0x00000679 System.Void com.adjust.sdk.Adjust::GetNativeSkad4CompletionDelegate(System.String)
extern void Adjust_GetNativeSkad4CompletionDelegate_mABA4E95DF2115E03AEBF4754E8C67EA61116C57E (void);
// 0x0000067A System.Void com.adjust.sdk.Adjust::GetAuthorizationStatus(System.String)
extern void Adjust_GetAuthorizationStatus_mEB2B4F74FBAAD83B387FACE5EC1F6B0DEB344883 (void);
// 0x0000067B System.Void com.adjust.sdk.Adjust::GetNativeVerificationInfo(System.String)
extern void Adjust_GetNativeVerificationInfo_m7D6A32E6A3F4A393312B5BDDE97814D96D2C976D (void);
// 0x0000067C System.Boolean com.adjust.sdk.Adjust::bat()
extern void Adjust_bat_m7BCB001B58A932B97FDB4BC1BB8F83309E0714AC (void);
// 0x0000067D System.Void com.adjust.sdk.Adjust::SetTestOptions(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void Adjust_SetTestOptions_m5313B20E29B0029F24CF33ECE44DC008CDD6F360 (void);
// 0x0000067E System.Void com.adjust.sdk.Adjust::.ctor()
extern void Adjust__ctor_m3303F268C76843435B868BB9D9E307FBD20A8F0B (void);
// 0x0000067F System.Void com.adjust.sdk.AdjustAdRevenue::.ctor(System.String)
extern void AdjustAdRevenue__ctor_m4C94C4313766148F6D9DC7451483B6EC847EEFB8 (void);
// 0x00000680 System.Void com.adjust.sdk.AdjustAdRevenue::setRevenue(System.Double,System.String)
extern void AdjustAdRevenue_setRevenue_mB37B06AC7FE6C0D6FF8BF3DEDD7C5E2A58E3E3A7 (void);
// 0x00000681 System.Void com.adjust.sdk.AdjustAdRevenue::setAdImpressionsCount(System.Int32)
extern void AdjustAdRevenue_setAdImpressionsCount_m3181A0D66506FA4D9971B18CC8E3DDB921EB1115 (void);
// 0x00000682 System.Void com.adjust.sdk.AdjustAdRevenue::setAdRevenueNetwork(System.String)
extern void AdjustAdRevenue_setAdRevenueNetwork_m500036ED01D100B35A12B3DD99AA9E754EA72B25 (void);
// 0x00000683 System.Void com.adjust.sdk.AdjustAdRevenue::setAdRevenueUnit(System.String)
extern void AdjustAdRevenue_setAdRevenueUnit_mF46B42441260BED2E68D98661A8D90D4F202C856 (void);
// 0x00000684 System.Void com.adjust.sdk.AdjustAdRevenue::setAdRevenuePlacement(System.String)
extern void AdjustAdRevenue_setAdRevenuePlacement_m1C1843A4ED920DDBAD223DFCD78131655804CC0B (void);
// 0x00000685 System.Void com.adjust.sdk.AdjustAdRevenue::addCallbackParameter(System.String,System.String)
extern void AdjustAdRevenue_addCallbackParameter_m2B3A25714F44C6FBA06C09FB6ABD9F703EC9335C (void);
// 0x00000686 System.Void com.adjust.sdk.AdjustAdRevenue::addPartnerParameter(System.String,System.String)
extern void AdjustAdRevenue_addPartnerParameter_mAF18DE2CE37C15D2179C77ADF244D1AB260D32D3 (void);
// 0x00000687 System.Void com.adjust.sdk.AdjustAppStorePurchase::.ctor(System.String,System.String,System.String)
extern void AdjustAppStorePurchase__ctor_mC1D24DD9ABED88BF92A169ABE33573DAB8FFF404 (void);
// 0x00000688 System.Void com.adjust.sdk.AdjustAppStoreSubscription::.ctor(System.String,System.String,System.String,System.String)
extern void AdjustAppStoreSubscription__ctor_m0D3482433734BA539F0A09252DE3659D21FD1536 (void);
// 0x00000689 System.Void com.adjust.sdk.AdjustAppStoreSubscription::setTransactionDate(System.String)
extern void AdjustAppStoreSubscription_setTransactionDate_mD1BA71DA15248006C26B22602D1BF4A83B0ACC0C (void);
// 0x0000068A System.Void com.adjust.sdk.AdjustAppStoreSubscription::setSalesRegion(System.String)
extern void AdjustAppStoreSubscription_setSalesRegion_m0E1646795FA1466592F7E7A7D14B04EC02D6E39B (void);
// 0x0000068B System.Void com.adjust.sdk.AdjustAppStoreSubscription::addCallbackParameter(System.String,System.String)
extern void AdjustAppStoreSubscription_addCallbackParameter_mD67B08D11C9DCD410CB8966744F3962905E8AA70 (void);
// 0x0000068C System.Void com.adjust.sdk.AdjustAppStoreSubscription::addPartnerParameter(System.String,System.String)
extern void AdjustAppStoreSubscription_addPartnerParameter_m6B639A50999BE4CC82D58CCCE7D1F50536D62019 (void);
// 0x0000068D System.String com.adjust.sdk.AdjustAttribution::get_adid()
extern void AdjustAttribution_get_adid_m7FEE4DDFADFF7764690922FE17064A8475DCC159 (void);
// 0x0000068E System.Void com.adjust.sdk.AdjustAttribution::set_adid(System.String)
extern void AdjustAttribution_set_adid_m8FF9650D73A3B30569FA924D09F2A1B5841800F6 (void);
// 0x0000068F System.String com.adjust.sdk.AdjustAttribution::get_network()
extern void AdjustAttribution_get_network_m8430B735848CDEF80E9054A358E1147FBD19AEE3 (void);
// 0x00000690 System.Void com.adjust.sdk.AdjustAttribution::set_network(System.String)
extern void AdjustAttribution_set_network_m68ED3E4E1E6850226D667FDE9829B402AF120D20 (void);
// 0x00000691 System.String com.adjust.sdk.AdjustAttribution::get_adgroup()
extern void AdjustAttribution_get_adgroup_m15DAB5440B779D12C1BD8BCF9C47B20F14692416 (void);
// 0x00000692 System.Void com.adjust.sdk.AdjustAttribution::set_adgroup(System.String)
extern void AdjustAttribution_set_adgroup_m04EB13F0176574C01F8E233A15E6E7AB71CDEBFB (void);
// 0x00000693 System.String com.adjust.sdk.AdjustAttribution::get_campaign()
extern void AdjustAttribution_get_campaign_mB839E1C4DD4EC624B6C46E9444F1A9D868EA0750 (void);
// 0x00000694 System.Void com.adjust.sdk.AdjustAttribution::set_campaign(System.String)
extern void AdjustAttribution_set_campaign_m29AC5BBED526925450C7D081A5A656E9A71470E9 (void);
// 0x00000695 System.String com.adjust.sdk.AdjustAttribution::get_creative()
extern void AdjustAttribution_get_creative_mC15C380B618E220C2143920CCB88EBAF8A864B36 (void);
// 0x00000696 System.Void com.adjust.sdk.AdjustAttribution::set_creative(System.String)
extern void AdjustAttribution_set_creative_mF0F350C3D8521BBC5D841A28428210CD9CF41183 (void);
// 0x00000697 System.String com.adjust.sdk.AdjustAttribution::get_clickLabel()
extern void AdjustAttribution_get_clickLabel_m45D150F891EF508E44F219A4CBE768A05BCA866D (void);
// 0x00000698 System.Void com.adjust.sdk.AdjustAttribution::set_clickLabel(System.String)
extern void AdjustAttribution_set_clickLabel_mAAFCDD0362AFE2EF2F6AEC66E6973B65B75692DE (void);
// 0x00000699 System.String com.adjust.sdk.AdjustAttribution::get_trackerName()
extern void AdjustAttribution_get_trackerName_mEA8576F240393B289A3C0CC66F9D7F2E965EEB52 (void);
// 0x0000069A System.Void com.adjust.sdk.AdjustAttribution::set_trackerName(System.String)
extern void AdjustAttribution_set_trackerName_m731697B9763F60A9FC502CC6A1A27BDBD2574876 (void);
// 0x0000069B System.String com.adjust.sdk.AdjustAttribution::get_trackerToken()
extern void AdjustAttribution_get_trackerToken_mB2CB9686A8CC7243A6C4391F4728F1BA8197F64A (void);
// 0x0000069C System.Void com.adjust.sdk.AdjustAttribution::set_trackerToken(System.String)
extern void AdjustAttribution_set_trackerToken_m6093F9C8CC27B2425BB1373F51EDFA26B9E2103F (void);
// 0x0000069D System.String com.adjust.sdk.AdjustAttribution::get_costType()
extern void AdjustAttribution_get_costType_m94B271C6C975D4C945D5912D7879C411BB2F25C6 (void);
// 0x0000069E System.Void com.adjust.sdk.AdjustAttribution::set_costType(System.String)
extern void AdjustAttribution_set_costType_m2B994A60E50367E752D803F431BE9B010BE784B0 (void);
// 0x0000069F System.Nullable`1<System.Double> com.adjust.sdk.AdjustAttribution::get_costAmount()
extern void AdjustAttribution_get_costAmount_m570856A2EFDAE1646AB3EBE61E9D11FC7A872182 (void);
// 0x000006A0 System.Void com.adjust.sdk.AdjustAttribution::set_costAmount(System.Nullable`1<System.Double>)
extern void AdjustAttribution_set_costAmount_m8C20F2BD1C52F1109660D5A965B5159BA4DC5647 (void);
// 0x000006A1 System.String com.adjust.sdk.AdjustAttribution::get_costCurrency()
extern void AdjustAttribution_get_costCurrency_m746AD16AC39C41F680D4420B830529EAF595E999 (void);
// 0x000006A2 System.Void com.adjust.sdk.AdjustAttribution::set_costCurrency(System.String)
extern void AdjustAttribution_set_costCurrency_m4C83141F90E118ADEA5CCA620335B9FDD0C38D51 (void);
// 0x000006A3 System.String com.adjust.sdk.AdjustAttribution::get_fbInstallReferrer()
extern void AdjustAttribution_get_fbInstallReferrer_m730BCBF4BD7687B6ABA49F85E1E3592944782A68 (void);
// 0x000006A4 System.Void com.adjust.sdk.AdjustAttribution::set_fbInstallReferrer(System.String)
extern void AdjustAttribution_set_fbInstallReferrer_m03CE43EE59FB3D653CB09AB9BD1DE86EE11D292D (void);
// 0x000006A5 System.Void com.adjust.sdk.AdjustAttribution::.ctor()
extern void AdjustAttribution__ctor_m36B38620BB1475A4ACE1EDB1CCA466AB2F754307 (void);
// 0x000006A6 System.Void com.adjust.sdk.AdjustAttribution::.ctor(System.String)
extern void AdjustAttribution__ctor_m8274D1B29F0C4D4D99E2067269DBF55161E3B98A (void);
// 0x000006A7 System.Void com.adjust.sdk.AdjustAttribution::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustAttribution__ctor_mE3820E52AF63417CE1FF2ADAAE8B1BFA701344C9 (void);
// 0x000006A8 System.Void com.adjust.sdk.AdjustConfig::.ctor(System.String,com.adjust.sdk.AdjustEnvironment)
extern void AdjustConfig__ctor_m718373AA152F4C6F3AB5E805B4630AB008A32395 (void);
// 0x000006A9 System.Void com.adjust.sdk.AdjustConfig::.ctor(System.String,com.adjust.sdk.AdjustEnvironment,System.Boolean)
extern void AdjustConfig__ctor_m96C4907B142108F8818BEBC52EDC03D90B5C6EA7 (void);
// 0x000006AA System.Void com.adjust.sdk.AdjustConfig::setLogLevel(com.adjust.sdk.AdjustLogLevel)
extern void AdjustConfig_setLogLevel_mDA93163BE7A5E536C670CCDC0CCF7C93B9B3E54F (void);
// 0x000006AB System.Void com.adjust.sdk.AdjustConfig::setDefaultTracker(System.String)
extern void AdjustConfig_setDefaultTracker_mA67C3195A19A5E9AA2B5AF9E071336CA9E1AB724 (void);
// 0x000006AC System.Void com.adjust.sdk.AdjustConfig::setExternalDeviceId(System.String)
extern void AdjustConfig_setExternalDeviceId_m5AA54126D0A69091B9573F3A530BD2AF8B450FDF (void);
// 0x000006AD System.Void com.adjust.sdk.AdjustConfig::setLaunchDeferredDeeplink(System.Boolean)
extern void AdjustConfig_setLaunchDeferredDeeplink_m8D6806307929E8E3AE2F01CE3C08BF96DDCD526F (void);
// 0x000006AE System.Void com.adjust.sdk.AdjustConfig::setSendInBackground(System.Boolean)
extern void AdjustConfig_setSendInBackground_m039AABBAF2DB300CE62F8CBF78DA3A5E36604317 (void);
// 0x000006AF System.Void com.adjust.sdk.AdjustConfig::setEventBufferingEnabled(System.Boolean)
extern void AdjustConfig_setEventBufferingEnabled_mBB81E8C7A41ABCA6326F518EE53905C327B1F982 (void);
// 0x000006B0 System.Void com.adjust.sdk.AdjustConfig::setCoppaCompliantEnabled(System.Boolean)
extern void AdjustConfig_setCoppaCompliantEnabled_m43149C9F256F85E6149011100CEC777326B818DF (void);
// 0x000006B1 System.Void com.adjust.sdk.AdjustConfig::setNeedsCost(System.Boolean)
extern void AdjustConfig_setNeedsCost_m27ACE0EB3E57AECBD640B2A1B4510BCFBE8553DD (void);
// 0x000006B2 System.Void com.adjust.sdk.AdjustConfig::setDelayStart(System.Double)
extern void AdjustConfig_setDelayStart_m5E3583922F84F6E2B9988052D54ABECE6113B0B6 (void);
// 0x000006B3 System.Void com.adjust.sdk.AdjustConfig::setUserAgent(System.String)
extern void AdjustConfig_setUserAgent_mDD4FFFE5044037A2BC22003F631A9989361DFA1D (void);
// 0x000006B4 System.Void com.adjust.sdk.AdjustConfig::setIsDeviceKnown(System.Boolean)
extern void AdjustConfig_setIsDeviceKnown_mAD1C556F14A0DBAED60254F330EF9625F3AB6EDA (void);
// 0x000006B5 System.Void com.adjust.sdk.AdjustConfig::setUrlStrategy(System.String)
extern void AdjustConfig_setUrlStrategy_m43C184E9915977FC7955F22A086111B7836E2263 (void);
// 0x000006B6 System.Void com.adjust.sdk.AdjustConfig::setAppSecret(System.Int64,System.Int64,System.Int64,System.Int64,System.Int64)
extern void AdjustConfig_setAppSecret_mCF9AAAE31F6A695F806709B8599E319706BE15DE (void);
// 0x000006B7 System.Void com.adjust.sdk.AdjustConfig::setDeferredDeeplinkDelegate(System.Action`1<System.String>,System.String)
extern void AdjustConfig_setDeferredDeeplinkDelegate_m0434CB6325F267D824956505E38F55C1BC69F750 (void);
// 0x000006B8 System.Action`1<System.String> com.adjust.sdk.AdjustConfig::getDeferredDeeplinkDelegate()
extern void AdjustConfig_getDeferredDeeplinkDelegate_m5E71CF0E1CD8ED86E14052643073B2B34A19E574 (void);
// 0x000006B9 System.Void com.adjust.sdk.AdjustConfig::setAttributionChangedDelegate(System.Action`1<com.adjust.sdk.AdjustAttribution>,System.String)
extern void AdjustConfig_setAttributionChangedDelegate_m16311DC0B297069CC826AB0CEE81C747C47B7054 (void);
// 0x000006BA System.Action`1<com.adjust.sdk.AdjustAttribution> com.adjust.sdk.AdjustConfig::getAttributionChangedDelegate()
extern void AdjustConfig_getAttributionChangedDelegate_m0B91F876BC47C733C887A0C674C69C7A2AAE859E (void);
// 0x000006BB System.Void com.adjust.sdk.AdjustConfig::setEventSuccessDelegate(System.Action`1<com.adjust.sdk.AdjustEventSuccess>,System.String)
extern void AdjustConfig_setEventSuccessDelegate_mC93D376662090A2A7D5341FCB0EB6F5D47034C00 (void);
// 0x000006BC System.Action`1<com.adjust.sdk.AdjustEventSuccess> com.adjust.sdk.AdjustConfig::getEventSuccessDelegate()
extern void AdjustConfig_getEventSuccessDelegate_m803B0AF83809209BDCA4FD72ADCD37A3D6525AAE (void);
// 0x000006BD System.Void com.adjust.sdk.AdjustConfig::setEventFailureDelegate(System.Action`1<com.adjust.sdk.AdjustEventFailure>,System.String)
extern void AdjustConfig_setEventFailureDelegate_mDF106AB503D7AE6A0EF9FC23C86FDB561C53D919 (void);
// 0x000006BE System.Action`1<com.adjust.sdk.AdjustEventFailure> com.adjust.sdk.AdjustConfig::getEventFailureDelegate()
extern void AdjustConfig_getEventFailureDelegate_m55B097E3E827DAA9D0A03C3827815990DEEFAA73 (void);
// 0x000006BF System.Void com.adjust.sdk.AdjustConfig::setSessionSuccessDelegate(System.Action`1<com.adjust.sdk.AdjustSessionSuccess>,System.String)
extern void AdjustConfig_setSessionSuccessDelegate_m38873292BB0382A5A82272A971C2C8FB32EE97ED (void);
// 0x000006C0 System.Action`1<com.adjust.sdk.AdjustSessionSuccess> com.adjust.sdk.AdjustConfig::getSessionSuccessDelegate()
extern void AdjustConfig_getSessionSuccessDelegate_mDD3BD6C6F62AF59330E60B1570D2FC3D42DE20C1 (void);
// 0x000006C1 System.Void com.adjust.sdk.AdjustConfig::setSessionFailureDelegate(System.Action`1<com.adjust.sdk.AdjustSessionFailure>,System.String)
extern void AdjustConfig_setSessionFailureDelegate_m3BEF1CB7417F8E3E12E59E610DBE1FEA8584E2AC (void);
// 0x000006C2 System.Action`1<com.adjust.sdk.AdjustSessionFailure> com.adjust.sdk.AdjustConfig::getSessionFailureDelegate()
extern void AdjustConfig_getSessionFailureDelegate_mB847ACF06A571D19D85DD18BA596E78F646AED66 (void);
// 0x000006C3 System.Void com.adjust.sdk.AdjustConfig::setAllowiAdInfoReading(System.Boolean)
extern void AdjustConfig_setAllowiAdInfoReading_mBCAE2AC7ED0E99E915648114A3424E985EFE469C (void);
// 0x000006C4 System.Void com.adjust.sdk.AdjustConfig::setAllowAdServicesInfoReading(System.Boolean)
extern void AdjustConfig_setAllowAdServicesInfoReading_m232716609D173872EF41FD5837A9D0133419C4C1 (void);
// 0x000006C5 System.Void com.adjust.sdk.AdjustConfig::setAllowIdfaReading(System.Boolean)
extern void AdjustConfig_setAllowIdfaReading_m439C9CAB2FDE23F534F838B3BEAC30B917E483CA (void);
// 0x000006C6 System.Void com.adjust.sdk.AdjustConfig::deactivateSKAdNetworkHandling()
extern void AdjustConfig_deactivateSKAdNetworkHandling_m9E3A12F2125AE97AF898E7AC49DBCE9085D93B9E (void);
// 0x000006C7 System.Void com.adjust.sdk.AdjustConfig::setLinkMeEnabled(System.Boolean)
extern void AdjustConfig_setLinkMeEnabled_mC3B85AB4A602F3BB59B8B4B7FA973D9F2B8EB55E (void);
// 0x000006C8 System.Void com.adjust.sdk.AdjustConfig::setConversionValueUpdatedDelegate(System.Action`1<System.Int32>,System.String)
extern void AdjustConfig_setConversionValueUpdatedDelegate_m944853EAA8941CDC4ECEA27C4C9CAD01279639B4 (void);
// 0x000006C9 System.Action`1<System.Int32> com.adjust.sdk.AdjustConfig::getConversionValueUpdatedDelegate()
extern void AdjustConfig_getConversionValueUpdatedDelegate_mA8286519D143FC8FA6AA32373F2169099ABEEE23 (void);
// 0x000006CA System.Void com.adjust.sdk.AdjustConfig::setSkad4ConversionValueUpdatedDelegate(System.Action`3<System.Int32,System.String,System.Boolean>,System.String)
extern void AdjustConfig_setSkad4ConversionValueUpdatedDelegate_mBDC7D976A8BD22E4680DA70B9EC5EE7ECC4A45E4 (void);
// 0x000006CB System.Action`3<System.Int32,System.String,System.Boolean> com.adjust.sdk.AdjustConfig::getSkad4ConversionValueUpdatedDelegate()
extern void AdjustConfig_getSkad4ConversionValueUpdatedDelegate_m791D25B57223A51EDE3A28E916F1A6AB43EC2FFF (void);
// 0x000006CC System.Void com.adjust.sdk.AdjustConfig::setAttConsentWaitingInterval(System.Int32)
extern void AdjustConfig_setAttConsentWaitingInterval_m207E02DC9D926C771965BB2270A18A914B1B1DA3 (void);
// 0x000006CD System.Void com.adjust.sdk.AdjustConfig::setProcessName(System.String)
extern void AdjustConfig_setProcessName_mA05E8249BDBEECED54C503BAAE53011D4EF18E53 (void);
// 0x000006CE System.Void com.adjust.sdk.AdjustConfig::setReadMobileEquipmentIdentity(System.Boolean)
extern void AdjustConfig_setReadMobileEquipmentIdentity_m60C524B45682B362D3A43D8EA2AAB5E324F3D16C (void);
// 0x000006CF System.Void com.adjust.sdk.AdjustConfig::setPreinstallTrackingEnabled(System.Boolean)
extern void AdjustConfig_setPreinstallTrackingEnabled_m50FF6E90421C467AAB8D1668E426E2F2F5B15BDA (void);
// 0x000006D0 System.Void com.adjust.sdk.AdjustConfig::setPreinstallFilePath(System.String)
extern void AdjustConfig_setPreinstallFilePath_mF70F4E2F50F2E73E7EAF1DEAB6351F6AB6EB728A (void);
// 0x000006D1 System.Void com.adjust.sdk.AdjustConfig::setPlayStoreKidsAppEnabled(System.Boolean)
extern void AdjustConfig_setPlayStoreKidsAppEnabled_m6786F76DFEE24836BA51A2FA1B798FB2AEA86484 (void);
// 0x000006D2 System.Void com.adjust.sdk.AdjustConfig::setFinalAndroidAttributionEnabled(System.Boolean)
extern void AdjustConfig_setFinalAndroidAttributionEnabled_m6A5F408AE10EAB53570FA465B37920760A0E5F60 (void);
// 0x000006D3 System.Void com.adjust.sdk.AdjustConfig::setLogDelegate(System.Action`1<System.String>)
extern void AdjustConfig_setLogDelegate_mCD5A0B2CC87D71A6618CB76ED218FFDB346D487C (void);
// 0x000006D4 System.String com.adjust.sdk.AdjustEnvironmentExtension::ToLowercaseString(com.adjust.sdk.AdjustEnvironment)
extern void AdjustEnvironmentExtension_ToLowercaseString_mAEDC5B0CBA386D07FB258ED1BDFC83CB4394D49B (void);
// 0x000006D5 System.Void com.adjust.sdk.AdjustEvent::.ctor(System.String)
extern void AdjustEvent__ctor_mB6F2EEAE794AF0DBA97B384BD745A06235288C03 (void);
// 0x000006D6 System.Void com.adjust.sdk.AdjustEvent::setRevenue(System.Double,System.String)
extern void AdjustEvent_setRevenue_mA8B68C9A56C7A90FDD61D07D6E9B527EA4BAEB49 (void);
// 0x000006D7 System.Void com.adjust.sdk.AdjustEvent::addCallbackParameter(System.String,System.String)
extern void AdjustEvent_addCallbackParameter_m69836E8BCB0600E59592F2226886F7E3717267DC (void);
// 0x000006D8 System.Void com.adjust.sdk.AdjustEvent::addPartnerParameter(System.String,System.String)
extern void AdjustEvent_addPartnerParameter_m5C8A9B71C8E3668F18D2A7107128C2AA7F60115B (void);
// 0x000006D9 System.Void com.adjust.sdk.AdjustEvent::setCallbackId(System.String)
extern void AdjustEvent_setCallbackId_m77A43125761431059046C8BD038A4090A6F67A98 (void);
// 0x000006DA System.Void com.adjust.sdk.AdjustEvent::setTransactionId(System.String)
extern void AdjustEvent_setTransactionId_mD82CAE578CF9FBBB0F73937723AE9679D33AA254 (void);
// 0x000006DB System.Void com.adjust.sdk.AdjustEvent::setProductId(System.String)
extern void AdjustEvent_setProductId_mA5A287AD1813C5E813F12A097C4587AD23B2C8AA (void);
// 0x000006DC System.Void com.adjust.sdk.AdjustEvent::setReceipt(System.String,System.String)
extern void AdjustEvent_setReceipt_m0DA7BC506BF585B0EFDD3E0FEDC51EECE0406BFD (void);
// 0x000006DD System.Void com.adjust.sdk.AdjustEvent::setReceipt(System.String)
extern void AdjustEvent_setReceipt_m44238E44C88E2249BF57CB4775FF867EAEA76497 (void);
// 0x000006DE System.Void com.adjust.sdk.AdjustEvent::setPurchaseToken(System.String)
extern void AdjustEvent_setPurchaseToken_m921AABC8958704EE95BB091E047E4DF47E4E4F3F (void);
// 0x000006DF System.String com.adjust.sdk.AdjustEventFailure::get_Adid()
extern void AdjustEventFailure_get_Adid_m63A229A1E387D51BA76FD857843A30909472F4E9 (void);
// 0x000006E0 System.Void com.adjust.sdk.AdjustEventFailure::set_Adid(System.String)
extern void AdjustEventFailure_set_Adid_m1C9E862F9EE373D5F36B28D07F944581B4733FCC (void);
// 0x000006E1 System.String com.adjust.sdk.AdjustEventFailure::get_Message()
extern void AdjustEventFailure_get_Message_m39E32498366357A63414ACBF2D829D67E378435C (void);
// 0x000006E2 System.Void com.adjust.sdk.AdjustEventFailure::set_Message(System.String)
extern void AdjustEventFailure_set_Message_m67C166B4D02AD43A8835555633ED6A41B6470472 (void);
// 0x000006E3 System.String com.adjust.sdk.AdjustEventFailure::get_Timestamp()
extern void AdjustEventFailure_get_Timestamp_m8AD7E740ED2BAD647DF69D3E9E20DA10AEA7894C (void);
// 0x000006E4 System.Void com.adjust.sdk.AdjustEventFailure::set_Timestamp(System.String)
extern void AdjustEventFailure_set_Timestamp_m144FA4FAB62F3AE2D92C8A729A4D80C78129FC8F (void);
// 0x000006E5 System.String com.adjust.sdk.AdjustEventFailure::get_EventToken()
extern void AdjustEventFailure_get_EventToken_m790B0C32B96810DB063845DB41C7EA5392511E0F (void);
// 0x000006E6 System.Void com.adjust.sdk.AdjustEventFailure::set_EventToken(System.String)
extern void AdjustEventFailure_set_EventToken_m0107E2C7300ECD415209E1F64A6B8AD04F33798E (void);
// 0x000006E7 System.String com.adjust.sdk.AdjustEventFailure::get_CallbackId()
extern void AdjustEventFailure_get_CallbackId_m7C6B49AB5A6AE7A9287E309C85E4DDC8B6E01F6F (void);
// 0x000006E8 System.Void com.adjust.sdk.AdjustEventFailure::set_CallbackId(System.String)
extern void AdjustEventFailure_set_CallbackId_mE4D4EE9B87B3B947F952C7BC539A177AA609B0FD (void);
// 0x000006E9 System.Boolean com.adjust.sdk.AdjustEventFailure::get_WillRetry()
extern void AdjustEventFailure_get_WillRetry_m437C69AED2629C0A51F93160CF269ECB51C48138 (void);
// 0x000006EA System.Void com.adjust.sdk.AdjustEventFailure::set_WillRetry(System.Boolean)
extern void AdjustEventFailure_set_WillRetry_m4C79E145286998F97FFFC7106C792794C06669E9 (void);
// 0x000006EB System.Collections.Generic.Dictionary`2<System.String,System.Object> com.adjust.sdk.AdjustEventFailure::get_JsonResponse()
extern void AdjustEventFailure_get_JsonResponse_mB7A9E1270C3CA4F577552217E4FDB3CCFB32852A (void);
// 0x000006EC System.Void com.adjust.sdk.AdjustEventFailure::set_JsonResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustEventFailure_set_JsonResponse_mC129C66E6BD3773556DD9984F8A9B41987A480EE (void);
// 0x000006ED System.Void com.adjust.sdk.AdjustEventFailure::.ctor()
extern void AdjustEventFailure__ctor_m528922562AC18ADE49AC59EFECDF9DDDF06D9827 (void);
// 0x000006EE System.Void com.adjust.sdk.AdjustEventFailure::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustEventFailure__ctor_mD35BD0B33754A00AF01D005F17CE529500281A14 (void);
// 0x000006EF System.Void com.adjust.sdk.AdjustEventFailure::.ctor(System.String)
extern void AdjustEventFailure__ctor_mE44FDD70724F8F42E19DE705B7A0771C23BE0284 (void);
// 0x000006F0 System.Void com.adjust.sdk.AdjustEventFailure::BuildJsonResponseFromString(System.String)
extern void AdjustEventFailure_BuildJsonResponseFromString_mFC779A74C66E513EC19EF86F780AE363B25A828A (void);
// 0x000006F1 System.String com.adjust.sdk.AdjustEventFailure::GetJsonResponse()
extern void AdjustEventFailure_GetJsonResponse_m4A9D1FDB6FF13C9F955E00C64A4996F5826A31FD (void);
// 0x000006F2 System.String com.adjust.sdk.AdjustEventSuccess::get_Adid()
extern void AdjustEventSuccess_get_Adid_m9107BA449922578E0F9B8CB8B4541FE26A6C56C5 (void);
// 0x000006F3 System.Void com.adjust.sdk.AdjustEventSuccess::set_Adid(System.String)
extern void AdjustEventSuccess_set_Adid_mF832EF6F1DC6FE8156A132AD42AA1060E539A7AD (void);
// 0x000006F4 System.String com.adjust.sdk.AdjustEventSuccess::get_Message()
extern void AdjustEventSuccess_get_Message_m5B29D1C7B3CF3C7CED972991740A888131931DE2 (void);
// 0x000006F5 System.Void com.adjust.sdk.AdjustEventSuccess::set_Message(System.String)
extern void AdjustEventSuccess_set_Message_m38D9A47DB181615424C49B59C6E4A562B3E5F89F (void);
// 0x000006F6 System.String com.adjust.sdk.AdjustEventSuccess::get_Timestamp()
extern void AdjustEventSuccess_get_Timestamp_m193EB4EBA0B8DA8CF0863D1DF75FEF141B1D3B10 (void);
// 0x000006F7 System.Void com.adjust.sdk.AdjustEventSuccess::set_Timestamp(System.String)
extern void AdjustEventSuccess_set_Timestamp_m0CCE0BEF1E47ACA8E07187A73BBE9ACFEEC6586B (void);
// 0x000006F8 System.String com.adjust.sdk.AdjustEventSuccess::get_EventToken()
extern void AdjustEventSuccess_get_EventToken_m5784EFFBAE4463DA0ECFF6A537731DC98E286A3E (void);
// 0x000006F9 System.Void com.adjust.sdk.AdjustEventSuccess::set_EventToken(System.String)
extern void AdjustEventSuccess_set_EventToken_mAF539927077C6E4B98FC29622DE5D26C3A5F2C64 (void);
// 0x000006FA System.String com.adjust.sdk.AdjustEventSuccess::get_CallbackId()
extern void AdjustEventSuccess_get_CallbackId_m3D7D77C8EF5837C5EAAB45998FD4C7A02C04D983 (void);
// 0x000006FB System.Void com.adjust.sdk.AdjustEventSuccess::set_CallbackId(System.String)
extern void AdjustEventSuccess_set_CallbackId_mA49D8F4F34D8A1C9FB36A15EFB7572AC187A28C9 (void);
// 0x000006FC System.Collections.Generic.Dictionary`2<System.String,System.Object> com.adjust.sdk.AdjustEventSuccess::get_JsonResponse()
extern void AdjustEventSuccess_get_JsonResponse_mC1ED1F8BC320A1BE406D403D15DB0EA699A01A75 (void);
// 0x000006FD System.Void com.adjust.sdk.AdjustEventSuccess::set_JsonResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustEventSuccess_set_JsonResponse_mCA8F4E6DE391C1D4B8BCEEFB437BA5EE1E717D90 (void);
// 0x000006FE System.Void com.adjust.sdk.AdjustEventSuccess::.ctor()
extern void AdjustEventSuccess__ctor_m8E95350D1027E90E42E4A890D5D8F6C683C1388C (void);
// 0x000006FF System.Void com.adjust.sdk.AdjustEventSuccess::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustEventSuccess__ctor_m3AF21839E90ADA4ACF33D117311F354A788FFE1B (void);
// 0x00000700 System.Void com.adjust.sdk.AdjustEventSuccess::.ctor(System.String)
extern void AdjustEventSuccess__ctor_m572E2ED470E4819DFF8462F86CD0A35EE856DE75 (void);
// 0x00000701 System.Void com.adjust.sdk.AdjustEventSuccess::BuildJsonResponseFromString(System.String)
extern void AdjustEventSuccess_BuildJsonResponseFromString_mB45093E3AE421B1E1C210318F2081EB7016C065C (void);
// 0x00000702 System.String com.adjust.sdk.AdjustEventSuccess::GetJsonResponse()
extern void AdjustEventSuccess_GetJsonResponse_mC8F1B778DCD86E0CFCE0A7F34D2AE30E440E465B (void);
// 0x00000703 System.String com.adjust.sdk.AdjustLogLevelExtension::ToLowercaseString(com.adjust.sdk.AdjustLogLevel)
extern void AdjustLogLevelExtension_ToLowercaseString_mEF9C47460E6774026C495F7646A4369476C53588 (void);
// 0x00000704 System.String com.adjust.sdk.AdjustLogLevelExtension::ToUppercaseString(com.adjust.sdk.AdjustLogLevel)
extern void AdjustLogLevelExtension_ToUppercaseString_m457BEEAE7375DBA0C92F1180B69A432CE360A133 (void);
// 0x00000705 System.Void com.adjust.sdk.AdjustPlayStorePurchase::.ctor(System.String,System.String)
extern void AdjustPlayStorePurchase__ctor_m7B9ABD9ED2899FDF05BDF64D3E52047C489915F2 (void);
// 0x00000706 System.Void com.adjust.sdk.AdjustPlayStoreSubscription::.ctor(System.String,System.String,System.String,System.String,System.String,System.String)
extern void AdjustPlayStoreSubscription__ctor_m8FAA1BDF8B8C354B18FB090ACB1EF65E0B381EA1 (void);
// 0x00000707 System.Void com.adjust.sdk.AdjustPlayStoreSubscription::setPurchaseTime(System.String)
extern void AdjustPlayStoreSubscription_setPurchaseTime_mFB33C90CFC1A515912E08927A41E27DEB80094F4 (void);
// 0x00000708 System.Void com.adjust.sdk.AdjustPlayStoreSubscription::addCallbackParameter(System.String,System.String)
extern void AdjustPlayStoreSubscription_addCallbackParameter_m384EC847C6931509BE14FF2275D6AB32F493F9A4 (void);
// 0x00000709 System.Void com.adjust.sdk.AdjustPlayStoreSubscription::addPartnerParameter(System.String,System.String)
extern void AdjustPlayStoreSubscription_addPartnerParameter_m864989B749FAE715C806A3772FC7B968FFD4A5F4 (void);
// 0x0000070A System.Int32 com.adjust.sdk.AdjustPurchaseVerificationInfo::get_code()
extern void AdjustPurchaseVerificationInfo_get_code_m8FB046D3E105D4581BDA3A4C60BD5FE7FE0608BF (void);
// 0x0000070B System.Void com.adjust.sdk.AdjustPurchaseVerificationInfo::set_code(System.Int32)
extern void AdjustPurchaseVerificationInfo_set_code_mC3B02737B3A0F2A350DFF882C39A2829D2E7A8EC (void);
// 0x0000070C System.String com.adjust.sdk.AdjustPurchaseVerificationInfo::get_message()
extern void AdjustPurchaseVerificationInfo_get_message_mF44C745A7801EFE4681CA685163736AAD65BD093 (void);
// 0x0000070D System.Void com.adjust.sdk.AdjustPurchaseVerificationInfo::set_message(System.String)
extern void AdjustPurchaseVerificationInfo_set_message_m2CF4884928E67730C04532244236225CF728AEB6 (void);
// 0x0000070E System.String com.adjust.sdk.AdjustPurchaseVerificationInfo::get_verificationStatus()
extern void AdjustPurchaseVerificationInfo_get_verificationStatus_mBD654E0CF645A3B31439DFD66DEEF7A18469FF97 (void);
// 0x0000070F System.Void com.adjust.sdk.AdjustPurchaseVerificationInfo::set_verificationStatus(System.String)
extern void AdjustPurchaseVerificationInfo_set_verificationStatus_mAD3AC1434552BFC186F6B8A51F2116F04CE9D692 (void);
// 0x00000710 System.Void com.adjust.sdk.AdjustPurchaseVerificationInfo::.ctor()
extern void AdjustPurchaseVerificationInfo__ctor_mFD144FB4E4ACF2B998F0FA447CD249C2850972F1 (void);
// 0x00000711 System.Void com.adjust.sdk.AdjustPurchaseVerificationInfo::.ctor(System.String)
extern void AdjustPurchaseVerificationInfo__ctor_m8C6412DCE35238BF47E8E4202598F74CE085668E (void);
// 0x00000712 System.String com.adjust.sdk.AdjustSessionFailure::get_Adid()
extern void AdjustSessionFailure_get_Adid_m55CBA752E653E41BB100CA0666E984AC41A1C986 (void);
// 0x00000713 System.Void com.adjust.sdk.AdjustSessionFailure::set_Adid(System.String)
extern void AdjustSessionFailure_set_Adid_m9D52E417E29F03D868D2A5C1BA50578FAE232BC7 (void);
// 0x00000714 System.String com.adjust.sdk.AdjustSessionFailure::get_Message()
extern void AdjustSessionFailure_get_Message_m7FB5952110E6198593306F2D2206C87878241071 (void);
// 0x00000715 System.Void com.adjust.sdk.AdjustSessionFailure::set_Message(System.String)
extern void AdjustSessionFailure_set_Message_m84D2E372880BCEAB77F55A2D5E3228A2D0179835 (void);
// 0x00000716 System.String com.adjust.sdk.AdjustSessionFailure::get_Timestamp()
extern void AdjustSessionFailure_get_Timestamp_m16815BDDD78D3DC8836D6929D7ECA0287567E1C9 (void);
// 0x00000717 System.Void com.adjust.sdk.AdjustSessionFailure::set_Timestamp(System.String)
extern void AdjustSessionFailure_set_Timestamp_m4620F96554EF0DBF543BF574C3B9E2CBEA0BF46E (void);
// 0x00000718 System.Boolean com.adjust.sdk.AdjustSessionFailure::get_WillRetry()
extern void AdjustSessionFailure_get_WillRetry_mDC6EF21BB9ED54A38E87A437F25B3E1ABFB64CB7 (void);
// 0x00000719 System.Void com.adjust.sdk.AdjustSessionFailure::set_WillRetry(System.Boolean)
extern void AdjustSessionFailure_set_WillRetry_m891830EFFC0F200C979980F639EF51F2357E6BCF (void);
// 0x0000071A System.Collections.Generic.Dictionary`2<System.String,System.Object> com.adjust.sdk.AdjustSessionFailure::get_JsonResponse()
extern void AdjustSessionFailure_get_JsonResponse_m3CC10F98CEFA48F10203B4B21CA8B7F48313E337 (void);
// 0x0000071B System.Void com.adjust.sdk.AdjustSessionFailure::set_JsonResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustSessionFailure_set_JsonResponse_m9697C8316211570DED147C08CA044DB7A9626B6E (void);
// 0x0000071C System.Void com.adjust.sdk.AdjustSessionFailure::.ctor()
extern void AdjustSessionFailure__ctor_m55084005614B14B05358BFC8D8093D0E1BA5D577 (void);
// 0x0000071D System.Void com.adjust.sdk.AdjustSessionFailure::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustSessionFailure__ctor_mC8D3BF875D5D8A394B38A08DA6FD82FE78D65AB2 (void);
// 0x0000071E System.Void com.adjust.sdk.AdjustSessionFailure::.ctor(System.String)
extern void AdjustSessionFailure__ctor_mF96CCCD25D8F54F5FE37C1532E5A7D5B1FADEB3F (void);
// 0x0000071F System.Void com.adjust.sdk.AdjustSessionFailure::BuildJsonResponseFromString(System.String)
extern void AdjustSessionFailure_BuildJsonResponseFromString_m2D4F30200FC6361CACC4417A512F8E14FF9C38A6 (void);
// 0x00000720 System.String com.adjust.sdk.AdjustSessionFailure::GetJsonResponse()
extern void AdjustSessionFailure_GetJsonResponse_mE5D4C31B41ED1899C26AB32CD2648ADEFDE09351 (void);
// 0x00000721 System.String com.adjust.sdk.AdjustSessionSuccess::get_Adid()
extern void AdjustSessionSuccess_get_Adid_m647C0D4B4E911D6C8BE1634A171F548461180414 (void);
// 0x00000722 System.Void com.adjust.sdk.AdjustSessionSuccess::set_Adid(System.String)
extern void AdjustSessionSuccess_set_Adid_m4393AA9B18910CE351BB43D1C510132B4F971573 (void);
// 0x00000723 System.String com.adjust.sdk.AdjustSessionSuccess::get_Message()
extern void AdjustSessionSuccess_get_Message_m86BB21FF8BEC5DA95055C3A12413D7CEAF1731EA (void);
// 0x00000724 System.Void com.adjust.sdk.AdjustSessionSuccess::set_Message(System.String)
extern void AdjustSessionSuccess_set_Message_mD680D8861FD8EE269D0994D51498AC2210694E99 (void);
// 0x00000725 System.String com.adjust.sdk.AdjustSessionSuccess::get_Timestamp()
extern void AdjustSessionSuccess_get_Timestamp_mE2D213502F0F03A341B1E39DC4152AEF5C68F813 (void);
// 0x00000726 System.Void com.adjust.sdk.AdjustSessionSuccess::set_Timestamp(System.String)
extern void AdjustSessionSuccess_set_Timestamp_m2ED4611CC016044E197BF515B3A7C81C27B207EA (void);
// 0x00000727 System.Collections.Generic.Dictionary`2<System.String,System.Object> com.adjust.sdk.AdjustSessionSuccess::get_JsonResponse()
extern void AdjustSessionSuccess_get_JsonResponse_m13404EAE48C660945ED5BBC50A26E9AB2E4B8595 (void);
// 0x00000728 System.Void com.adjust.sdk.AdjustSessionSuccess::set_JsonResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustSessionSuccess_set_JsonResponse_mCFFE1E0F01BD95837EE0A4E9D89CE5913C3E0FBC (void);
// 0x00000729 System.Void com.adjust.sdk.AdjustSessionSuccess::.ctor()
extern void AdjustSessionSuccess__ctor_m5D4F0E9806EDCE8130DE98471E7ECA654B744F9A (void);
// 0x0000072A System.Void com.adjust.sdk.AdjustSessionSuccess::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdjustSessionSuccess__ctor_m468034512A1D2682AA0F15926CE8CA80F239C31D (void);
// 0x0000072B System.Void com.adjust.sdk.AdjustSessionSuccess::.ctor(System.String)
extern void AdjustSessionSuccess__ctor_mFD79CF038E807DE1559B54362B6E87EFAEFCD542 (void);
// 0x0000072C System.Void com.adjust.sdk.AdjustSessionSuccess::BuildJsonResponseFromString(System.String)
extern void AdjustSessionSuccess_BuildJsonResponseFromString_m2CA7E40EDAD331AE6DEDF385D364682D7AC8ACCE (void);
// 0x0000072D System.String com.adjust.sdk.AdjustSessionSuccess::GetJsonResponse()
extern void AdjustSessionSuccess_GetJsonResponse_m22B1531644212867F4EFF412E5B90CC8F7A15C5D (void);
// 0x0000072E System.Void com.adjust.sdk.AdjustThirdPartySharing::.ctor(System.Nullable`1<System.Boolean>)
extern void AdjustThirdPartySharing__ctor_mD050F802304C5E3A20E88D7C1F8AE85586641A82 (void);
// 0x0000072F System.Void com.adjust.sdk.AdjustThirdPartySharing::addGranularOption(System.String,System.String,System.String)
extern void AdjustThirdPartySharing_addGranularOption_m430DCE18F822237234F208C6FFD6C7837A2A1A77 (void);
// 0x00000730 System.Void com.adjust.sdk.AdjustThirdPartySharing::addPartnerSharingSetting(System.String,System.String,System.Boolean)
extern void AdjustThirdPartySharing_addPartnerSharingSetting_m46C4F5606AF8CE842EFA05FD126197ACCEC911E1 (void);
// 0x00000731 System.String com.adjust.sdk.AdjustUrlStrategyExtension::ToLowerCaseString(com.adjust.sdk.AdjustUrlStrategy)
extern void AdjustUrlStrategyExtension_ToLowerCaseString_mC501B171FABC8E81E217A019B01F9D079D4DC7A0 (void);
// 0x00000732 System.Int32 com.adjust.sdk.AdjustUtils::ConvertLogLevel(System.Nullable`1<com.adjust.sdk.AdjustLogLevel>)
extern void AdjustUtils_ConvertLogLevel_mF7D0CB4C0B08008E37686670B7361871B737A53F (void);
// 0x00000733 System.Int32 com.adjust.sdk.AdjustUtils::ConvertBool(System.Nullable`1<System.Boolean>)
extern void AdjustUtils_ConvertBool_mBFC3BC841A003201C7056448C67C35625379E786 (void);
// 0x00000734 System.Double com.adjust.sdk.AdjustUtils::ConvertDouble(System.Nullable`1<System.Double>)
extern void AdjustUtils_ConvertDouble_m328F7E087047FA52AEF1D681FCCD32D80791B749 (void);
// 0x00000735 System.Int32 com.adjust.sdk.AdjustUtils::ConvertInt(System.Nullable`1<System.Int32>)
extern void AdjustUtils_ConvertInt_mE9AACF8054BA25B7605B3F8727091ED4F41CF37C (void);
// 0x00000736 System.Int64 com.adjust.sdk.AdjustUtils::ConvertLong(System.Nullable`1<System.Int64>)
extern void AdjustUtils_ConvertLong_m7B66091ED09C4DA947FB5C61D5AC40762100FAF4 (void);
// 0x00000737 System.String com.adjust.sdk.AdjustUtils::ConvertListToJson(System.Collections.Generic.List`1<System.String>)
extern void AdjustUtils_ConvertListToJson_m0834067B90DD8AA9713B0A395933C806BDB84689 (void);
// 0x00000738 System.String com.adjust.sdk.AdjustUtils::GetJsonResponseCompact(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustUtils_GetJsonResponseCompact_mB1763C6F6A17665BAA0534CE919BCFB7D7D491F6 (void);
// 0x00000739 System.String com.adjust.sdk.AdjustUtils::GetJsonString(com.adjust.sdk.JSONNode,System.String)
extern void AdjustUtils_GetJsonString_m7E4ABC127B656F2CF1D6D5C2973CCDC9345477A1 (void);
// 0x0000073A System.Void com.adjust.sdk.AdjustUtils::WriteJsonResponseDictionary(com.adjust.sdk.JSONClass,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AdjustUtils_WriteJsonResponseDictionary_m45C6F803D1190D8144D7E3441A4CF870606463ED (void);
// 0x0000073B System.String com.adjust.sdk.AdjustUtils::TryGetValue(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.String)
extern void AdjustUtils_TryGetValue_m3BF1818C3435B2DD8794C6BF52073DE2D50A57E9 (void);
// 0x0000073C System.Int32 com.adjust.sdk.AdjustUtils::GetSkad4ConversionValue(System.String)
extern void AdjustUtils_GetSkad4ConversionValue_mF1B95F499F7AECC0987FA3A4DD57E10F9582741E (void);
// 0x0000073D System.String com.adjust.sdk.AdjustUtils::GetSkad4CoarseValue(System.String)
extern void AdjustUtils_GetSkad4CoarseValue_m6A96D9597EAAD2D606A7B8730683A1870E324FCA (void);
// 0x0000073E System.Boolean com.adjust.sdk.AdjustUtils::GetSkad4LockWindow(System.String)
extern void AdjustUtils_GetSkad4LockWindow_mE9E55E3A5B683CDF1BF463568133655A4BEEA39C (void);
// 0x0000073F System.Void com.adjust.sdk.AdjustUtils::.ctor()
extern void AdjustUtils__ctor_mEE74F3B9A26BAE12B3C426FF63604FD7396544A2 (void);
// 0x00000740 System.Void com.adjust.sdk.AdjustUtils::.cctor()
extern void AdjustUtils__cctor_m4489DD780E5669549E8C7EDAF985BDEC7AC456E1 (void);
static Il2CppMethodPointer s_methodPointers[1856] = 
{
	ExampleGUI_OnGUI_m7F98D722B7B9B570BBAA43D6E62AA1C9ADAB9514,
	ExampleGUI_fq_m1A6F8162E695A87DBB61379A4B7B6004030DC92B,
	ExampleGUI_HandleGooglePlayId_m3E5E31CFA1DACF07F0E799F5C9D79A0AAE15ABE0,
	ExampleGUI_AttributionChangedCallback_mF94C25FEB4A93C591437B1B7B980C4FEA040BA73,
	ExampleGUI_EventSuccessCallback_m31F74382525AD79CE9078C9E3DBA80527C24124D,
	ExampleGUI_EventFailureCallback_m3E6FA431F4BD7E4B85E03C02B841950D78AF35EE,
	ExampleGUI_SessionSuccessCallback_m0900AAA8351E1B38AA866EC6BC8D138165E2146C,
	ExampleGUI_SessionFailureCallback_mE56B4A25ADA21A2A3CE51C1CC1B9DED1CDC93059,
	ExampleGUI_fr_mEBBF08D3DCB0698F0C4F68598BC7DF677D3A9F0D,
	ExampleGUI__ctor_m781F1113ADEDC46C79DCD9CDEF57E6A92867A83D,
	U3CU3Ec__cctor_m8004B56DF658186326E479AFFA0822C993DD72ED,
	U3CU3Ec__ctor_m4795792C3C133DF92B1D9761F0AF23E6BE3D3FB6,
	U3CU3Ec_fp_m863AC16EABD9413AC8E20C1A9818EEB3D7ACB1D9,
	CameraMenuMoving_Awake_m3D2FDA555E2F67618DC3163293D90DA89E8AC335,
	CameraMenuMoving_Update_mC0BF00E6A8FF54D62DDE29E51D6BBF3FEFD4F0AD,
	CameraMenuMoving__ctor_m173C7C079483127207AA4DFF8A3A15FDA4D9C935,
	CircleSlider_Start_m6AE9A64452308957253285B8C3F01CB999F756DC,
	CircleSlider_Update_mEAE72D122EA1F00E41FF9062651F48B48E83D1FA,
	CircleSlider__ctor_m62FA48BB6ADA7D3F0CF5A3D515E64F662E85C0F0,
	JustRotate_Update_mCF011C9447F4A8F52F83B8FEAA7463FDDFAF5452,
	JustRotate__ctor_mCB13490E96F212DED7A9E79F59828664A3077904,
	SliderRunTo1_Start_m4AAB1FDA12B530B7C49D304B95198889B57DDC14,
	SliderRunTo1_Update_m4DA8480B33A0D072A98B99ABBAA544C539DD0330,
	SliderRunTo1__ctor_m56F8C938A45BC24AF24076F32B8918CDEE61FBC6,
	SliderRunTo1Action_Start_mEAEB5C8912CBD6EE48C7B830CD2742BF4640D184,
	SliderRunTo1Action_Update_m2C6AE848FDC10C4E366C38515E9C943DD0186772,
	SliderRunTo1Action__ctor_mCDC91B6FE1F141D72BBDE2F4D17AB249D3E67250,
	SliderValuePass_Start_m0FEC6932F01B69FD1BB800F684C7C97F5FFE6CFC,
	SliderValuePass_UpdateProgress_mD6E59B90095418375D083F59CC8DB2AD902B576B,
	SliderValuePass__ctor_m9BA53DA42B8045C5FD6D811719B5C782930CA890,
	ToggleChangeTextColor_Start_mBB5B72F592E9F6E83B743D5CA794F18EBCA4D35C,
	ToggleChangeTextColor_ChangeColor_m12EA071DBF52F2F3605FF6B0AA6197D808976200,
	ToggleChangeTextColor__ctor_m454A6780EB585D08CC24BF5EE105A6A986230568,
	CFX_AutoStopLoopedEffect_OnEnable_mFAE624B7728A2F5F7865E685F4594BD0C9629FA5,
	CFX_AutoStopLoopedEffect_Update_m8416521A39549298F203B44EF089B09EBD8793FD,
	CFX_AutoStopLoopedEffect__ctor_m66E2F5DD06231BC3D8D01C11A34D7D55BE5F7AAE,
	CFX_Demo_New_Awake_m2BEACA32BC84D821DF07AF25B7B0B99BFE8F7371,
	CFX_Demo_New_Update_m1172B662225E3FC12899D5FEEC66702AEA702EFB,
	CFX_Demo_New_OnToggleGround_mC1E9AC90083482747297AA60D9CE9ECABF307929,
	CFX_Demo_New_OnToggleCamera_m756EE04AC3DC67A47CFA88C4CC86B1FB9FE0BD02,
	CFX_Demo_New_OnToggleSlowMo_m20658A6343C784057EFACEBED012978C4F78D48B,
	CFX_Demo_New_OnPreviousEffect_mA2A209F797976C97EB976A21083FF4C6B9652812,
	CFX_Demo_New_OnNextEffect_mBE56FE317B38E35579D4EC0868755EB2F4E4E640,
	CFX_Demo_New_fx_mF958F91593CC4F4BF75BAD97FCF37CF509A5F8E5,
	CFX_Demo_New_fy_m519AD3ABAC8E5D868A06D4AAF9BD8293167C0F5D,
	CFX_Demo_New_CheckForDeletedParticles_m9693C5D1E5FB5189A81426EB45A4E1EE6416EE41,
	CFX_Demo_New_fz_m5FDEC24B46D99A5A5007CDD7F8A96791959ED896,
	CFX_Demo_New_ga_m450FCA6AEE31DE87931FD616D1672E1157C597D7,
	CFX_Demo_New_gb_m654A141FFF9CFB8E324CB83E6C1F66BB0E00E020,
	CFX_Demo_New__ctor_m3F3E772F218F5D0DF6E561E20AE15B042CED6752,
	U3CU3Ec__cctor_mAB1F46E7745FB53F6527920549715240437DAA6A,
	U3CU3Ec__ctor_m97F374614077DD1A825F933DED9B82FDCBAF91D4,
	U3CU3Ec_fs_m31CBCC6A389D642FF0A0033E70D06E5E841EDA10,
	a__ctor_m9C4DF0DAD9DFD630B96FEA232B72A10ACCE427B2,
	a_ft_mABA22AAB97AE7C25DDF7D73554D23FBB9DF869AF,
	a_MoveNext_m582CAFC0843108712A0C183E9896EEDCEBAA18B1,
	a_fu_mB924091A3C3F6FD590168E577CE43C81581DA2C7,
	a_fv_mEB08502AF115D1B45C99D8AD95D197DAF5283482,
	a_fw_m6787263201C7E95FE5652A4BEE243E863DE52CC0,
	CFX_Demo_RandomDir_Start_mBEA6B11F6CDBC5D92B9039AA8B18A7EBEF5448C8,
	CFX_Demo_RandomDir__ctor_m0D93F82C71A0EAE6FF074A8F3EDBBB81ED3D0B48,
	CFX_Demo_RandomDirectionTranslate_Start_m96E0BCED718092574499B7127C3A8673849208DB,
	CFX_Demo_RandomDirectionTranslate_Update_m4024B24E39F74439D91BB2A5C0077350853814AE,
	CFX_Demo_RandomDirectionTranslate__ctor_m0069918B310E5DB0ABB7A4081DAE7EAFD6A1E0CF,
	CFX_Demo_RotateCamera_Update_mF984EC78638612B1C89597528E3FD5ACBD2539A4,
	CFX_Demo_RotateCamera__ctor_m0ADA46D335888F3F43AC518ED19FFB1F1AABC7C8,
	CFX_Demo_RotateCamera__cctor_m4A71CF2705030EA82F2818BD43F82DC91C01486C,
	CFX_Demo_Translate_Start_m2408E76C85181E0D900CEFEA8E3CE9214763AC76,
	CFX_Demo_Translate_Update_mD71C66B91F9EC43DD9ABBA2D8A45CBD57DE8EBF7,
	CFX_Demo_Translate__ctor_m44AD33D5825BE599180B3ED3E3668893F46E29DB,
	CFX_AutoDestructShuriken_OnEnable_m95AB27DC91BCCF60D378AE0665B095D44331D78A,
	CFX_AutoDestructShuriken_CheckIfAlive_mCFAEBF84CD35FBF9C742C9A170183136AAB53535,
	CFX_AutoDestructShuriken__ctor_m22A7ABD6E38836099374390249F88639BB36CC33,
	c__ctor_mEAE65DED48436AAD1D041C00B2C2B7F65C00D9E0,
	c_gc_m09C7EB51128C9438A454158A2416A6D98AF6F84A,
	c_MoveNext_m0894A2BCD71F74704E245DDF5AD039636A5E7FD2,
	c_gd_m7D6B449267B6BF8464BF79AD94B4CA56B5B0ECF5,
	c_ge_mE459DC11359AEB26E2438A3566FA30FD23E75F30,
	c_gf_mE022ABC2223DE423753B07EC95F05DD9E0E2CC3B,
	CFX_AutoRotate_Update_m6B9C72C2FC80E6C5FABD9DB647C8FDAE2BD56328,
	CFX_AutoRotate__ctor_m66822106F9299AE6FB3E7BEE628175E86FAEF866,
	CFX_LightFlicker_Awake_mDD217C09D0CD73F750E12C784CF9B9671729817A,
	CFX_LightFlicker_OnEnable_mBAD2393E6F61D5D7981870E576F1238783ED8E3C,
	CFX_LightFlicker_Update_m7EDE8F7D320D87C751799FE5EAEECAF24F33315E,
	CFX_LightFlicker__ctor_mAB98FAF050C7B8A46472C42EBE3D802CBDFBE96C,
	CFX_LightIntensityFade_Start_mA64A524D6D15ADAE266FF2F3F8F81F38ACBD3EB6,
	CFX_LightIntensityFade_OnEnable_m56FA183840193584063C3B0AF49B0FA8DB8D7014,
	CFX_LightIntensityFade_Update_mCE17E56AE5A9CE5993315D8F00B6AD2C9BEC08B4,
	CFX_LightIntensityFade__ctor_m0CFBDFFAFF4B1B51471D1B4AE3A3DE92BB8EE722,
	SampleCustomHeader_Start_mC6F0E5DE5F1E84CE5E80400678F0A0E7E7B04DDA,
	SampleCustomHeader_Update_mC8AEAE90831BC4426B0F939BF6CFF1B49AAE4A80,
	SampleCustomHeader_OnGUI_m2DD476D04D317B6C974ED5540A15153AC8C9D4A4,
	SampleCustomHeader__ctor_m798BDF5D8247757A7C4804ADC588255B96D44BB3,
	SampleWebView_Awake_mDBF7BF2B80A5864CCD88FFB647C77A1313A012F7,
	SampleWebView_gq_m2809F7A9285BB94E683285944D75EBBEAEC1E53E,
	SampleWebView__ctor_mA1A876DE33B67AC2652D7D6EF97596B4789FB3E0,
	SampleWebView_gr_mE251629CC61E864D57B7E0319D2BC8DFB4B17A00,
	U3CU3Ec__cctor_mBA34CA72A72A17C43E319EFE05DBB2666725861F,
	U3CU3Ec__ctor_m19620618A100D0F898A3AE8728E17A28B4EAE24D,
	U3CU3Ec_gg_mA4C542A8B42ADC7B1F0342D4EA78077E85D0A8FB,
	U3CU3Ec_gh_m2330B3CD4EE885003E91F41EE3D77D903CF6A256,
	U3CU3Ec_gi_mBD0DF78433D5CFF077235BEFD030B83E73E84C8B,
	U3CU3Ec_gj_mAB3D2EA81AB384C7B8A8D0702A85EFFADACC9A23,
	U3CU3Ec_gk_m1E189BDEFC08730B568036EB304220A49AF6FDAC,
	U3CU3Ec_gl_m24C9ED7082F971312880E3EE169D382BCA746143,
	e__ctor_m11AB00EF52CE90C90F8320DDF4B6544D3680329B,
	e_gm_m43FD929C326648BA57102D2EEBF15C9FDA05551B,
	e_MoveNext_m3DC10366F1BEC32CBB865C06D52EAF80FC73BFBF,
	e_gn_m0C80DDE6447CA6C5D38ED677CFBD9F3B16783624,
	e_go_m2C55ADB30D91AA9C5E6C6B374BB736609FAE6EE4,
	e_gp_mCA7188E65CD594CFEAB6DAA329C4C8466F0F082E,
	AdjustHandler_Awake_m35F7FCEEFFEBCC3692E9F12AF6A91DC909EDE7E6,
	AdjustHandler_Start_m80DC13EF8E913B5B3335929C4B658CAD4F10AFC8,
	AdjustHandler_hb_mED723B3CBE5C7327CCCFE54B55125015577FFDAC,
	AdjustHandler_hc_mAAED7D296F3E90984B66B87DBE990D2418D77A66,
	AdjustHandler_hd_mB326B5CF460D27AEC1B1094303307FB7D3113611,
	AdjustHandler_he_mAE07A4D190E8041B8EF2916BD9589F1FEE6CA10E,
	AdjustHandler__ctor_m570962B2FCA935A388F07235F8B8AB828F52075D,
	f__ctor_m9DA94C17CF1944A8258A3437D11AABE109E48C0D,
	f_gs_m8B1EE60493878D8788F5396568E721AD2540AB14,
	f_MoveNext_m5AEC2A0D5D8E398E7EFDDBD2F8F8285CF37C2468,
	f_gt_mA07FC051C696623E5B77B2F7B78755D8B7E45BDF,
	f_gu_mEE0780C63017A8C3F4F13F6E8DFF54672B2D9AAB,
	f_gv_mC02AEEC4E79F4F2021A2CB31B819B3FFE86E3FE4,
	g__ctor_m445BEB3DBA61FCEE90F3141CE36D2B2A9496DFB1,
	g_gw_m5CA4FBF5A8C579DDACCB2FBA39A0EA3DD656622B,
	g_MoveNext_m0AAF5480619752A53111A2C00C005F3FD7AFDE42,
	g_gx_mB044E97103A0CDD0E5D835DA8779594BDD29432C,
	g_gy_mD9D156127056A6575C94B06B18AA59AD504AD1E2,
	g_gz_mC1FBA134B4E740FF53AA7127AFC9925C5BE86336,
	g_ha_m7454B2A24793DA64EBA94F8B342BFC67944A25EB,
	ArcadeGameMachine_Play_m17039B4E6AC0D4E96382FC061785AA92AEC63BB9,
	ArcadeGameMachine__ctor_m760676BE1E78E6F5A682B0E1C44B8B088050FFD1,
	ButtonToMenu_OnEnable_m52F918BF565F35765C83353B508D2B1191D04EA8,
	ButtonToMenu_hf_m816A092A7AB277E7C369B44C14497B6971B642E4,
	ButtonToMenu__ctor_mA276999DC46CBAC98358E7CDE03315354609B62D,
	GameSelectorMenu_Awake_mFA9877852A501B1A35C38628FEDA689E87E55384,
	GameSelectorMenu_Update_m6A189D526D74A5BBD74F2A958DD6722C91C60A32,
	GameSelectorMenu_hg_mEED507F3769EA50D974C782C891456F9AE0D93F2,
	GameSelectorMenu__ctor_m53267B2F8FEFDDE784631A84AEA63891BA1A5492,
	PlayBtn_Awake_m35E8369CE1D28E36BB8FD03E6311599992917B2B,
	PlayBtn_Update_mFA7AB764AABC986AC9C9A7FA3543296DF6F51E8A,
	PlayBtn__ctor_m5E2FF67C86EEF88574AB9FB2403EAC0405DC3E04,
	RotatePhoneBannerUI_hh_m1B758106DA56135088BBA33C96AED07CADA1EE2D,
	RotatePhoneBannerUI_hi_mA34D0DD658614C701EFD38FEC34305E89A9EE86F,
	RotatePhoneBannerUI_Update_mDB4AD4A38E23E94DC842D778FBA2A48DE2E025EA,
	RotatePhoneBannerUI__ctor_m1349C2E7A28EE722FDB427C21AA2472DAC5A8F39,
	ChatController_OnEnable_m025CE203564D82A1CDCE5E5719DB07E29811D0B7,
	ChatController_OnDisable_mD49D03719CAEBB3F59F24A7FA8F4FD30C8B54E46,
	ChatController_hj_mAEF1C2CFA99A469171B00812FE9E5AE4F55A0942,
	ChatController__ctor_m39C05E9EB8C8C40664D5655BCAB9EEBCB31F9719,
	DropdownSample_OnButtonClick_mF83641F913F3455A3AE6ADCEA5DEB2A323FCB58F,
	DropdownSample__ctor_m0F0C6DD803E99B2C15F3369ABD94EC273FADC75B,
	EnvMapAnimator_Awake_m1D86ECDDD4A7A6DF98748B11BAC74D2D3B2F9435,
	EnvMapAnimator_Start_mB8A6567BB58BDFD0FC70980AFA952748DF1E80E9,
	EnvMapAnimator__ctor_m465E8527E49D1AA672A9A8A3B96FE78C24D11138,
	h__ctor_m6B340AB88E61A0146E4C2E5783A3B0650D52746E,
	h_hk_m0375CF0F39E8B262FCCC2699A5A6B1626238B366,
	h_MoveNext_m7D8F0181D594C1657EEEB84576EA263912F68FEA,
	h_hl_m51F09F3A2591DC6D998E7468E3413B4F8A6707EB,
	h_hm_m6D29E3C2B69860DD7DBAF394D13AFA3FA2674F2F,
	h_hn_mCC7F7CBE7101E3EB3E50CC05060152E384BF3A75,
	TMP_DigitValidator_Validate_m786CF8A4D85EB9E1BE8785A58007F8796991BDB9,
	TMP_DigitValidator__ctor_m9DC5F1168E5F4963C063C88384ADEBA8980BBFE0,
	TMP_PhoneNumberValidator_Validate_mE50FE1DE042CE58055C824840D77FCDA6A2AF4D3,
	TMP_PhoneNumberValidator__ctor_m70833F265A016119F88136746B4C59F45B5E067D,
	TMP_TextEventHandler_get_onCharacterSelection_mA62049738125E3C48405E6DFF09E2D42300BE8C3,
	TMP_TextEventHandler_set_onCharacterSelection_m6B85C54F4E751BF080324D94FB8DA6286CD5A43C,
	TMP_TextEventHandler_get_onSpriteSelection_m95CDEB7394FFF38F310717EEEFDCD481D96A5E82,
	TMP_TextEventHandler_set_onSpriteSelection_mFFBD9D70A791A3F2065C1063F258465EDA8AC2C5,
	TMP_TextEventHandler_get_onWordSelection_mF22771B4213EEB3AEFCDA390A4FF28FED5D9184C,
	TMP_TextEventHandler_set_onWordSelection_mA7EB31AF14EAADD968857DDAC994F7728B7B02E3,
	TMP_TextEventHandler_get_onLineSelection_mDDF07E7000993FCD6EAF2FBD2D2226EB66273908,
	TMP_TextEventHandler_set_onLineSelection_m098580AA8098939290113692072E18F9A293B427,
	TMP_TextEventHandler_get_onLinkSelection_m87FB9EABE7F917B2F910A18A3B5F1AE3020D976D,
	TMP_TextEventHandler_set_onLinkSelection_m6741C71F7E218C744CD7AA18B7456382E4B703FF,
	TMP_TextEventHandler_Awake_mE2D7EB8218B248F11BE54C507396B9B6B12E0052,
	TMP_TextEventHandler_LateUpdate_mBF0056A3C00834477F7D221BEE17C26784559DE1,
	TMP_TextEventHandler_OnPointerEnter_mF5B4CCF0C9F2EFE24B6D4C7B31C620C91ABBC07A,
	TMP_TextEventHandler_OnPointerExit_mC0561024D04FED2D026BEB3EC183550092823AE6,
	TMP_TextEventHandler_ho_m10176A9C3F6463CAC4C490194928C60156F0FDC9,
	TMP_TextEventHandler_hp_mABB9F448E9F5CF01F736D70CB50055D0D50E0073,
	TMP_TextEventHandler_hq_m01B60E51F51C098C6C6462E434CF4545F2986763,
	TMP_TextEventHandler_hr_m67CF249BFB77A6FDBCCD1EC5DDD18B137580D8F1,
	TMP_TextEventHandler_hs_mD2D86832465F691E4A0BE2F48F45895253CE221B,
	TMP_TextEventHandler__ctor_mADE4C28CAE14991CF0B1CC1A9D0EBAF0CF1107AB,
	CharacterSelectionEvent__ctor_m054FE9253D3C4478F57DE900A15AC9A61EC3C11E,
	SpriteSelectionEvent__ctor_m89C1D1F720F140491B28D9B32B0C7202EE8C4963,
	WordSelectionEvent__ctor_m3F52F327A9627042EDB065C1080CEB764F1154F2,
	LineSelectionEvent__ctor_m419828B3E32BC3F6F5AAC88D7B90CF50A74C80B2,
	LinkSelectionEvent__ctor_m4083D6FF46F61AAF956F77FFE849B5166E2579BC,
	Benchmark01_Start_m6CF91B0D99B3AC9317731D0C08B2EDA6AA56B9E9,
	Benchmark01__ctor_m9E12F5F809E8FF4A6EEFCDB016C1F884716347C4,
	i__ctor_mC530C3F49395DE615CBB7ECB5A8DCF9F9EE7ECAF,
	i_ht_m621AE8BEBE5E335E6548154E8316CF5EB786B103,
	i_MoveNext_mCB114E94E32FE4C5C050B482AC9A4554FF2614EA,
	i_hu_m6120099F71F945E63398282615622CB6AC75AA8D,
	i_hv_m1BEDED11DDCF3F61B8938E65B0E888E698B334B9,
	i_hw_mB8619157279E23098F2E297BC9C2EEDA5EDA4356,
	Benchmark01_UGUI_Start_m565A619941AAFFC17BB16A4A73DF63F7E54E3AFA,
	Benchmark01_UGUI__ctor_m9DCE74210552C6961BF7460C1F812E484771F8EB,
	j__ctor_m58D42A7FBF05708666272FADEE61F161606D1884,
	j_hx_m6827F98282F00769DB1373FF805DF0D9EABD213D,
	j_MoveNext_m3ADC8AD16F89087CC2707835166AC092164D89B1,
	j_hy_mF3F711E8FC914F62678160608244E6E83BE1E047,
	j_hz_m04CC16B6185A5E81EDDB6A7A2FA20B3B06FB1BBD,
	j_ia_mEEDF9B01D48D4FB51833BD6025FB4E455856B2BD,
	Benchmark02_Start_mB56F21A9861A3DAF9F4E7F1DD4A023E05B379E29,
	Benchmark02__ctor_mE5DCB1CF4C1FDBA742B51B11427B9DE209630BF1,
	Benchmark03_Awake_mDEE8E96AE811C5A84CB2C04440CD4662E2F918D3,
	Benchmark03_Start_mCCFD9402E218265F6D34A1EA7ACCD3AD3D80380D,
	Benchmark03__ctor_m8A29BB2CC6375B2D3D57B5A90D18F2435352E5F6,
	Benchmark04_Start_mD2F5056019DD08B3DB897F6D194E86AB66E92F90,
	Benchmark04__ctor_m282E4E495D8D1921A87481729549B68BEDAD2D27,
	CameraController_Awake_m2D75756734457ADE0F15F191B63521A47C426788,
	CameraController_Start_m749E20374F32FF190EC51D70C717A8117934F2A5,
	CameraController_LateUpdate_m07E7F5C7D91713F8BB489480304D130570D7858F,
	CameraController_ib_m83FE5CDB1E9F66368318A419277299F62BB8A50F,
	CameraController__ctor_mE37608FBFBF61F76A1E0EEACF79B040321476878,
	ObjectSpin_Awake_mC05FEB5A72FED289171C58787FE09DBD9356FC72,
	ObjectSpin_Update_m7FB0886C3E6D76C0020E4D38DC1C44AB70BF3695,
	ObjectSpin__ctor_mA786C14AE887FF4012A35FAB3DF59ECF6A77835A,
	ShaderPropAnimator_Awake_m3D158D58F1840CBDA3B887326275893121E31371,
	ShaderPropAnimator_Start_mEF0B5D3EE00206199ABB80CE893AA85DF3FE5C88,
	ShaderPropAnimator_ig_mD86932227DD6A345FFBCB9AC858B148B9DA35DEA,
	ShaderPropAnimator__ctor_m51C29C66EFD7FCA3AE68CDEFD38A4A89BF48220B,
	k__ctor_m0E13644123979DE6E6A5FDEBD98901953F591528,
	k_ic_m74B95B414992423EDC77E40C1161CD97498BD668,
	k_MoveNext_m9BD5A66A6C4059F26C23CA1712AFFCEB011EE0C6,
	k_id_mF7536BD4AC3795CCA0FAD701FD59CD5A022228BD,
	k_ie_m3AD2F19911AE13D26B50BB63D0934B3AB82A54CE,
	k_if_mEBAF7969AC5263A8106F3A525D56589696A7E6E2,
	SimpleScript_Start_mC4159EF79F863FBD86AEA2B81D86FDF04834A6F8,
	SimpleScript_Update_mBD8A31D53D01FEBB9B432077599239AC6A5DEAFE,
	SimpleScript__ctor_mC91E912195EEE18292A8FCA7650739E3DDB81807,
	SkewTextExample_Awake_m2D48E0903620C2D870D5176FCFD12A8989801C93,
	SkewTextExample_Start_m7577B96B07C4EB0666BF6F028074176258009690,
	SkewTextExample_il_mE7ED9BEC964184449A257BEDC5A343B8A2E70933,
	SkewTextExample_im_m35D80FB7B9888808EDDFD8DBEB1829F43D55463E,
	SkewTextExample__ctor_m711325FB390A6DFA994B6ADF746C9EBF846A0A22,
	l__ctor_mAF3BE015E0DDE8EEC10E29A93250E767CAFC1AC0,
	l_ih_m03302FBAB431F2C105F1D4FACF179258499FAC57,
	l_MoveNext_m8AEA21F97F8EB0D21E0026569F76A764CA382A86,
	l_ii_m2982E065EEFBB340F883048771376D8A4EAD3AF5,
	l_ij_m54D56190B1A7C601D06E1E5C518236F68985E62E,
	l_ik_mEF266F1D2BFB348363A20864B6203F0322565923,
	TeleType_Awake_m8D56A3C1E06AD96B35B88C3AA8C61FB2A03E627D,
	TeleType_Start_m3BFE1E2B1BB5ED247DED9DBEF293FCCBD63760C6,
	TeleType__ctor_m824BBE09CC217EB037FFB36756726A9C946526D0,
	m__ctor_mCB70A1ABAC23935609971C4E5EB96C6FCA781B80,
	m_in_mCDE4E87D88A0FAC9F826069CE36922F8512E01DC,
	m_MoveNext_m3CA9C2D89FD2EF31C9C0BBBB58ACACA97A2536A0,
	m_io_m0C35CAA36DF9EFBDC111F0BC378D68B6D09626DC,
	m_ip_m9C7DA6916EE3D7157BEF78A60D893E127C7A272F,
	m_iq_mCF04D07167DDEBAF4CC0930C6B8184E06EF4B694,
	TextConsoleSimulator_Awake_m55D28DC1F590D98621B0284B53C8A22D07CD3F7C,
	TextConsoleSimulator_Start_m5667F64AE1F48EBA2FF1B3D2D53E2AFCAB738B39,
	TextConsoleSimulator_OnEnable_mDF58D349E4D62866410AAA376BE5BBAE4153FF95,
	TextConsoleSimulator_OnDisable_m4B3A741D6C5279590453148419B422E8D7314689,
	TextConsoleSimulator_iz_m06C0361CEBBE88F56C5A3FDE11B8F3F219552ECD,
	TextConsoleSimulator_ja_mA0E3929F8A34D0439D8313A6F84713FD5B900CAF,
	TextConsoleSimulator_jb_m055C31AA087A705E9571436739090544432F62D4,
	TextConsoleSimulator__ctor_mBDDE8A2DCED8B140D78D5FE560897665753AB025,
	n__ctor_mA3995540D306C82F96A54411435FBD7EEAB53191,
	n_ir_m315FDA71861EF32023C968840C0A7129B6AA5265,
	n_MoveNext_mE8B628C5342720FFE6275001CD07C345E43A134B,
	n_is_m8257E331D55FFDB31DD8B42C3928F9868035C1EE,
	n_it_m4FDE17A6C417D01AD2A6DDE4ACDE2D56AD781C83,
	n_iu_mA9778BFC632F9DC80EDCF210006146DD5F85C075,
	o__ctor_mBC755591F3EC20D1AC8A3A28D113836B1267B142,
	o_iv_m4D22337709BE1949935BA9A8287A0FAF3A740AD4,
	o_MoveNext_m79D9ADB208208E5B9BF2FDCDF5600F956EE7937D,
	o_iw_m98B2B1A2B21FA48996E10643E9797DB1F6BB8066,
	o_ix_m15A06D70A3046AAE6D49E796CDDEC522651469FD,
	o_iy_m77BAAF5CDF74D675B99F53F3BDE547ABEA76B59C,
	TextMeshProFloatingText_Awake_m600F1825C26BB683047156FD815AE4376D2672F2,
	TextMeshProFloatingText_Start_m8121246A4310A0014ECA36144B9DCE093FE8AE49,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_mA1E370089458CD380E9BA7740C2BC2032F084148,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_mA02B20CF33E43FE99FD5F1B90F7F350262F0BEBE,
	TextMeshProFloatingText__ctor_mD08AF0FB6944A51BC6EA15D6BE4E33AA4A916E3E,
	TextMeshProFloatingText__cctor_m272097816057A64A9FFE16F69C6844DCF88E9557,
	p__ctor_mB34FCBF53494493E2865B3FD2D92C4DE8AFBABB1,
	p_jc_m0B9AAE9BA9FE0D780D6059C64D3CD7A8833B5957,
	p_MoveNext_m11CCFB7C78636D21CC2D7637F5A61463242F350A,
	p_jd_mE37A7F27E4840BD35EF04CE536A5F909A3F9B1FE,
	p_je_mD488CCE44FD425C693F957737161BF467DC1E889,
	p_jf_m6DD1469B64878BECF0F5076848C995B231753A0A,
	q__ctor_m5F6271E3EAB84B4538FF3C66B8A1AE62B09BEA4F,
	q_jg_m06591336EE6FA978DB73106B01801713E708B9C4,
	q_MoveNext_mC95A783178593C950AE1577E0AE3D18FA87E344A,
	q_jh_m443C4C9AABF6EDEDAC843DFBFBF052BEF38D0750,
	q_ji_mE06B38568964AABB1FFE57EB9AA006FA93732AC9,
	q_jj_m21C9A03FE653DF3EDE43ACE2A031EC3C1A7A8086,
	TextMeshSpawner_Awake_m9A84A570D2582918A6B1287139527E9AB2CF088D,
	TextMeshSpawner_Start_m3EE98071CA27A18904B859A0A6B215BDFEB50A66,
	TextMeshSpawner__ctor_m8409A62C31C4A6B6CEC2F48F1DC9777460C28233,
	TMPro_InstructionOverlay_Awake_m0F92D44F62A9AC086DE3DF1E4C7BFAF645EE7084,
	TMPro_InstructionOverlay_jk_mFDA23063ADF56A8C7FD3DA5F860744B0C65E8215,
	TMPro_InstructionOverlay__ctor_m247258528E488171765F77A9A3C6B7E079E64839,
	TMP_ExampleScript_01_Awake_m6E620605AE9CCC3789A2D5CFD841E5DAB8592063,
	TMP_ExampleScript_01_Update_m3D4A9AB04728F0ABD4C7C8A462E2C811308D97A1,
	TMP_ExampleScript_01__ctor_m43F9206FDB1606CD28F1A441188E777546CFEA2A,
	TMP_FrameRateCounter_Awake_m99156EF53E5848DE83107BFAC803C33DC964265C,
	TMP_FrameRateCounter_Start_m9B5D0A86D174DA019F3EB5C6E9BD54634B2F909A,
	TMP_FrameRateCounter_Update_m5251EE9AC9DCB99D0871EE83624C8A9012E6A079,
	TMP_FrameRateCounter_jl_mA573CB9DDB0233EEAA73D98321369EE744A44F83,
	TMP_FrameRateCounter__ctor_mD8804AE37CED37A01DF943624D3C2C48FBC9AE43,
	TMP_TextEventCheck_OnEnable_mABF0C00DDBB37230534C49AD9CA342D96757AA3E,
	TMP_TextEventCheck_OnDisable_m4AE76C19CBF131CB80B73A7C71378CA063CFC4C6,
	TMP_TextEventCheck_jm_mCEC0687FC3C3F477362F0817739562D68E56EA28,
	TMP_TextEventCheck_jn_mD29C63E452E309CBA4752C18CE58CEB1FACC8AB4,
	TMP_TextEventCheck_jo_m75996E48CC8DF8E721A3117405642FCFAB2A3EA6,
	TMP_TextEventCheck_jp_m49B65FC01AC32E3D95AA5E8F4DF514CE6D297F8B,
	TMP_TextEventCheck_jq_m258909931D38942078DA6BDFCFC07D887605B5F5,
	TMP_TextEventCheck__ctor_m8F6CDB8774BDF6C6B909919393AC0290BA2BB0AF,
	TMP_TextInfoDebugTool__ctor_m54C6EE99B1DC2B4DE1F8E870974B3B41B970C37E,
	TMP_TextSelector_A_Awake_m662ED2E3CDB7AE16174109344A01A50AF3C44797,
	TMP_TextSelector_A_LateUpdate_m1A711EC87962C6C5A7157414CD059D984D3BD55B,
	TMP_TextSelector_A_OnPointerEnter_m747F05CBEF90BF713BF726E47CA37DC86D9B439A,
	TMP_TextSelector_A_OnPointerExit_m5D7D8A07591506FB7291E84A951AB5C43DAA5503,
	TMP_TextSelector_A__ctor_m4C56A438A3140D5CF9C7AFB8466E11142F4FA3BE,
	TMP_TextSelector_B_Awake_m773D4C87E67823272DBF597B9CADE82DD3BFFD87,
	TMP_TextSelector_B_OnEnable_m8DA695DB0913F7123C4ADAFD5BEAB4424FA5861B,
	TMP_TextSelector_B_OnDisable_mF2EF7AE0E015218AB77936BD5FD6863F7788F11D,
	TMP_TextSelector_B_jr_m74B679824B6593D33B0D63336617E1DAC9C9F2F5,
	TMP_TextSelector_B_LateUpdate_mE1B3969D788695E37240927FC6B1827CC6DD5EFF,
	TMP_TextSelector_B_OnPointerEnter_mBAF5711E20E579D21258BD4040454A64E1134D98,
	TMP_TextSelector_B_OnPointerExit_m40ED8F7E47FF6FD8B38BE96B2216267F61509D65,
	TMP_TextSelector_B_OnPointerClick_m773B56D918B1D0F73C5ABC0EB22FD34D39AFBB97,
	TMP_TextSelector_B_OnPointerUp_mF409D728900872CC323B18DDA7F91265058BE772,
	TMP_TextSelector_B_js_m1642A106C244D5BE2A2110858B3AF14BB1B7D3F2,
	TMP_TextSelector_B__ctor_mB45DD6360094ADBEF5E8020E8C62404B7E45E301,
	TMP_UiFrameRateCounter_Awake_m3E0ECAD08FA25B61DD75F4D36EC3F1DE5A22A491,
	TMP_UiFrameRateCounter_Start_m11EF02C330E5D834C41F009CF088A3150352567F,
	TMP_UiFrameRateCounter_Update_m568E467033B0FF7C67251895A0772CFA197789A3,
	TMP_UiFrameRateCounter_jt_m43C3DC714E0A0A880E4FA70433C67C30DA8175E7,
	TMP_UiFrameRateCounter__ctor_mBF5305427799EBC515580C2747FE604A6DFEC848,
	VertexColorCycler_Awake_m8895A9C06DB3EC4379334601DC726F1AFAF543C1,
	VertexColorCycler_Start_m36846DA72BFC7FDFA944A368C9DB62D17A15917B,
	VertexColorCycler_jy_m0B46ACC0725ED373B4C3B54F3354987C750B734F,
	VertexColorCycler__ctor_m673CA077DC5E935BABCEA79E5E70116E9934F4C1,
	r__ctor_m5628C84890815547CA50B062495F0DC7B918F9E4,
	r_ju_m5951E3BD52BD7561EE4519538B5883A700806A4F,
	r_MoveNext_mACC42A6A064C31245ACC4947EC3AC18D6FA30203,
	r_jv_mBCD38631F9A548A6428C436DC7BE82E18047B4CF,
	r_jw_mBB1AE2A2217CA7DB280D9F0B73CFC98DCEFFB2FC,
	r_jx_mF60EF9840279CC559540C268B494CD720AFF8565,
	VertexJitter_Awake_m0DF2AC9C728A15EEB427F1FE2426E3C31FBA544C,
	VertexJitter_OnEnable_mCD5C1FDDBA809B04AC6F6CB00562D0AA45BC4354,
	VertexJitter_OnDisable_mB670406B3982BFC44CB6BB05A73F1BE877FDFAF2,
	VertexJitter_Start_mDE6155803CF2B1E6CE0EBAE8DF7DB93601E1DD76,
	VertexJitter_kd_mBEB57841E43FF2BF6F9831AC747D7CA7E59EA718,
	VertexJitter_ke_mE63433B45D26689CD57294E3EB5C7C229C690F57,
	VertexJitter__ctor_m41E4682405B3C0B19779BA8CB77156D65D64716D,
	u__ctor_m9BE8090269B8C0F19ECDE71683EA23CE009E4D4D,
	u_jz_m3316E1CC21286F58CF3AAD7AA1E8F0A36F049F4A,
	u_MoveNext_m905638306B83A76E5617D10058F5C0DB729308FF,
	u_ka_m2F20B214BB9365CAA957E5146E734A41DA390C15,
	u_kb_m9ED3B7DF1A711267A2DB115A209B4387FA9BBC0E,
	u_kc_m9181707C46A64B0C34FF699A76168D5F849A44A6,
	VertexShakeA_Awake_m092957B0A67A153E7CD56A75A438087DE4806867,
	VertexShakeA_OnEnable_m52E2A036C9EB2C1D633BA7F43E31C36983972304,
	VertexShakeA_OnDisable_m52F58AF9438377D222543AA67CFF7B30FCCB0F23,
	VertexShakeA_Start_mDD8B5538BDFBC2BA242B997B879E7ED64ACAFC5E,
	VertexShakeA_kj_m5E8022945BD70C4B82489FD6B4D56B6EF0CE59B3,
	VertexShakeA_kk_m4537D9F71FAE5C85AEA9292FB96DF245E174D87A,
	VertexShakeA__ctor_m63ED483A292CA310B90144E0779C0472AAC22CBB,
	w__ctor_m9062DB7CD727E5039B61D6BDF8F36C019633E8B0,
	w_kf_mB02200729610F0F6FE271DB5855B30A4241396C8,
	w_MoveNext_mE10C0D457AB1E0558442E89928DB5A6D69D0DEFD,
	w_kg_m66D88FE5A7AF6C3A18F0575A43AC1F376374099D,
	w_kh_mBBAD076D1EE2A083186EE224EFED6494F0D89CB4,
	w_ki_m07388D6C835BB60EA8FF2D04F14FC91A52190232,
	VertexShakeB_Awake_mFA9A180BD1769CC79E6325314B5652D605ABE58E,
	VertexShakeB_OnEnable_m4999DF4598174EDA2A47F4F667B5CE061DF97C21,
	VertexShakeB_OnDisable_m2FB32CBD277A271400BF8AF2A35294C09FE9B8E5,
	VertexShakeB_Start_m58786A0944340EF16E024ADB596C9AB5686C2AF1,
	VertexShakeB_kp_m3E393480AB2B628BEC34C959E331D1D69479D528,
	VertexShakeB_kq_mCFB1D0B2BCFAE00997AB9CA5801BCEA6B2E782CC,
	VertexShakeB__ctor_m9D068774503CF8642CC0BAC0E909ECE91E4E2198,
	x__ctor_mDFF851EC8FE819AE218E1C2423A1C5FA1823DAA5,
	x_kl_mAF95ABCFAD6F688242A22C1D657A6A277E65C835,
	x_MoveNext_m7681A14E42D44387309A2A58AEC8195DB0EE633D,
	x_km_m2793D456E15D8EDDCECC13B254DFCC204DD70980,
	x_kn_m822A6A2EA852A37BA727E3F4A76B502CC13C451A,
	x_ko_m6E8BC32AD702CF0BB32EFCA7947FF8B1854B83C8,
	VertexZoom_Awake_m29C1DE789B968D726EDD69F605321A223D47C1A0,
	VertexZoom_OnEnable_mE3719F01B6A8590066988F425F8A63103B5A7B47,
	VertexZoom_OnDisable_mBB91C9EFA049395743D27358A427BB2B05850B47,
	VertexZoom_Start_mB03D03148C98EBC9117D69510D24F21978546FCB,
	VertexZoom_kw_m3D401A5F9A8620F38C66059CE239ACDFC48D62E4,
	VertexZoom_kx_mB22DAE2D2D0D48A93A6DC7C66D673B3D12C5982E,
	VertexZoom__ctor_m454AF80ACB5C555BCB4B5E658A22B5A4FCC39422,
	y__ctor_m45DC59D4511BC8174A2BE8796070DF0FCE0030B6,
	y_kr_mC1EB6F2441C4B860E5C1E4366758FD91F11DDD9A,
	z__ctor_m7411FA8C4ABD8809ADDBD6240F0776E7A2015206,
	z_ks_m4F0050A286C74696628047135735807A8503A6B6,
	z_MoveNext_m64D17099BD43C955390E8862E4B4FF215ED0AD3C,
	z_kt_mBAAF29A87B7ECE0F3A823EA4C1082BDE6BFED817,
	z_ku_m559AFB9D3ABAC30E52DC9EE075927BC6B536AA51,
	z_kv_m9D59C82A870A92C2B250F3329062B656DAE4A761,
	WarpTextExample_Awake_m92842E51B4DBB2E4341ACB179468049FAB23949F,
	WarpTextExample_Start_m3339EDC03B6FC498916520CBCCDB5F9FA090F809,
	WarpTextExample_lc_m9980C264584E561C7F0E2BB79878C602E612CB44,
	WarpTextExample_ld_mD80F799ECA1C966F78C0D3A6E2414E79CD03A444,
	WarpTextExample__ctor_mBD48A5403123F25C45B5E60C19E1EA397FBA1795,
	ba__ctor_m43B3192FEB8D2C931FCDE5311C60B535D9FC8B82,
	ba_ky_mC7419CBA4AFE0C68BF9E1041932F99B4086A21A9,
	ba_MoveNext_m6D9CF9564E9982E0D85C00861E618F0EBA623819,
	ba_kz_mDF45FD77E9382185D424FD5022864379A39B7A16,
	ba_la_mD88495D50D34285CCD22472F856EB013FA235C02,
	ba_lb_mD96A4DA5435ABAE966673632DA7529D5BDF320BD,
	ColorManager_Awake_m490C578D56D7609C20CCD2943A60D8AC6F156EE9,
	ColorManager_Start_m82FF9163BEA1C7E31FA273970B288F6A43A585D6,
	ColorManager_ChangeBackgroundColor_mD861FCB929F44CE05B95EDAF37D68E2522EF7881,
	ColorManager__ctor_m6ADB1782811C33A94CEAB5E159F70799D627C5B9,
	DisplayManager_Awake_m01335DCC14EBA25AA446004EB5B4C7B2153A98F1,
	DisplayManager_Update_m38F8000E01AE1A561B40A5D978D715F25CA5241D,
	DisplayManager_le_mD4F3B34D4132B5D2437329955BBB699140FBD113,
	DisplayManager_GetWidth_mFDFB36DB2D50C3F84F758291C28444533BA014DC,
	DisplayManager__ctor_m8205FE124021FDCD885822245CD10692DC944060,
	FollowPlayer_Update_mC13522DC9FFB4DE999F76EAB359ED651CC54BBA9,
	FollowPlayer__ctor_m7B55BE377D37E89EBC21D786A2F4D9FDABFF3B6F,
	GameManager_Awake_m32449FAD0ABB6DA999982CBA8D7A04C8DA0041DD,
	GameManager_Update_m8D69A640252954387F011678EB4083C919AE87E8,
	GameManager_ln_m3C993654A4D7307C5489B96454A5CC4E587B639E,
	GameManager_GameOver_m320826E52553065DA089F31CD7DB2EF5F6A3A675,
	GameManager_lo_m66D0168DBD356A43B1470DC203E74C9FA0228B51,
	GameManager_Restart_m43130AE3498C335714E4BA04CBB27902BDC5288C,
	GameManager__ctor_m472A74B544FE99B623745252A13EE966727FC0EE,
	bb__ctor_mB1E8635EBBC07D3515A24237CB05FC954A1408A9,
	bb_lf_m9BE286D9CD385F40CCABE91309F493C304B70170,
	bb_MoveNext_mECD01F3AE07646E747EDA229057EEB3588B8558E,
	bb_lg_m1C2CE4973FD01AC92D7F7C7A2CCE6CADCBBF1EF7,
	bb_lh_m8FF39FD7986A86C983DF3DAA894019B9382BBACD,
	bb_li_m29E49850DA67A0456280E39FA2C44DFE6679BB67,
	bc__ctor_mA60993DFF132226BC7EBFE9AA08B35D8580CE0CB,
	bc_lj_m25756378F126F29BA63A9084946AD5E1F4EE407E,
	bc_MoveNext_mF22BFB7660636EADA806B44D145935AC9D15B322,
	bc_lk_m1C5BEF5042562F558B17A40A4CBE8A3661551CC4,
	bc_ll_m33D7F5BC188A8873DB8AEDB2820A1982ADD926FF,
	bc_lm_m2081FEDCEE7B0B7E7CFCC74FCBB210F0CD031443,
	ObstacleManager_Start_m28B6D8E2ECC292A0AC602BE50B54121AE4E1A08B,
	ObstacleManager_Update_mFC3133AD340EB570915C8528BA891A7DFD2D2D69,
	ObstacleManager_lp_m89C27B328008FABA19845A761262210133416ED8,
	ObstacleManager__ctor_mB202BFCFB76004F490942697AEC807CA69870298,
	ObstacleMovement_Start_m9611E6A71A98FC2D3BBCF756309F9C120485712D,
	ObstacleMovement_Update_mF3BAB0AA886EA89BEDA9048B5B60E3970AC56163,
	ObstacleMovement_lq_mD984FA005E0178B9EA46A3E952C0B990149D3F6B,
	ObstacleMovement_lr_m0E8E3A43C9EF51415CE7B5BDC2E714BC459089F0,
	ObstacleMovement_ls_mA62E178DED2CC4764E1B0433D9BE3838814E34F8,
	ObstacleMovement_lt_m84F2BD55B26D87109D0DBF3BE23EA44B5E7732D2,
	ObstacleMovement__ctor_m92BCB75AF394EA3037156AAEA8B04EFEC3635CD3,
	ObstacleParent_Start_m5C27FE43E8BF0E76E2861049C0AB8D53E4E9FE6F,
	ObstacleParent_ly_m390BEED88D279FC129D8B04AE05C19C5F257C0FA,
	ObstacleParent__ctor_mA16A0298FD0D37693AE408F881E228A9C308D5DB,
	bd__ctor_m71AA4D06CE68DD19AC58539D73D1DE0BE345D520,
	bd_lu_mABF7DB4E39420E1896D3F1696D050E3084B2FC9C,
	bd_MoveNext_m990D5421E67C4A5BE766C5A423ABFD51E18D2D49,
	bd_lv_mB200571983ABFE34E11F06EAEF861D35CB7AAE38,
	bd_lw_m146928D89A3131569FCBD659A09BA6DEDA34D92D,
	bd_lx_m128046F4705C53D1384C70697EAB08D2299ECD4B,
	Player_Start_m3A9BD073012F67CB983646DB5D3E6B443F404F66,
	Player_Update_mFBF7171ED857D469C6B9FCD22E16DF5026281E6E,
	Player_lz_m98B366378D735BF4546DF6AACF93478BE1775208,
	Player_OnTriggerEnter2D_m7BDB33E23764DBF62B0A14BABA739CF92EC7D649,
	Player__ctor_m30F8A7452908FE4CA77E28B4547937BA4EF65488,
	ScoreManager_Awake_mF687A4553FDEBA33B4D91BE2CB825CE7BF565B81,
	ScoreManager_AddScore_m6EEE7869ADA876D48969DC3ADA7B0088EF2989C9,
	ScoreManager_GetScore_m5E3AC405C69F2E82D0B74DD5D4CA42B1D16FE097,
	ScoreManager__ctor_m7B86337BDE99A3BBA7173DD7F8F9CE974C803CED,
	Ball_get_Instance_mD92105F1E056FC3B7F20B65762D929C0A57C22F1,
	Ball_set_Instance_mAC56F6291CBF20E017E1F3EA1A389B79DFA9A75B,
	Ball_Awake_mFC726B55B8E0139DA58C3B11CD8A2575D4CC9DB3,
	Ball_Start_m2A1E75B0BA54336DC4A6B455412496E1133C5403,
	Ball_GameStart_mE270DF9C9F0697305491443BB1326D1AAF04BBB3,
	Ball_Update_m9656FD3279C999854982B011466D170DC11BB46B,
	Ball_ma_m76C9BD3918EB6F851E35024FC247D910C1FB81A4,
	Ball_OnCollisionEnter2D_m2510CFB11D6C4F6CB357CC6DB47C6880975A3B7A,
	Ball_GetTouchedWall_m34DAA030427DF3D4B4BA7B58D0F37F278A68A797,
	Ball_GetTouchedPosition_mFCD322CA42C7161AAC2B758DD321F35D05D8AB97,
	Ball_Explode_mA9650D5D9894A6F5DB369C72FC9D472846071405,
	Ball_GameOvered_mB1AF35367DC37047865D2661CC67786C41066948,
	Ball_mb_m09151F5DE4F583C2B550F7EEA06494FDFB2C7905,
	Ball__ctor_mEB1C3A6DD57B97CC7BB163FB5C43FFBAEA0A1151,
	BallActionManager_get_Instance_mDB11C8D2A82542EF8A340609F472460869352E7E,
	BallActionManager_set_Instance_m7506DCFE2F4D406BCEA126F68BA094FF4D8E951C,
	BallActionManager_Awake_m7F3431C1F9247AED579237A42828D4892CB8D81C,
	BallActionManager_InvokeOnBallGenerated_mFD635788FFCEE3136703353B7C281B8A41AAAD58,
	BallActionManager_InvokeOnGameStarted_m9BCF3C192B1AAB2AA0F73D09CA74EDD25FC4191E,
	BallActionManager_InvokeOnBallJumped_mACA8406A1200867865DFC6C07ADA56336DAB41C3,
	BallActionManager_InvokeOnWallTouched_mE673D9FCDDAABD935E1F23A46E01F90A1EAE0453,
	BallActionManager_InvokeOnObstacleTouched_mE9A6B28FE1817765E66117E324727D33651CEBB8,
	BallActionManager_InvokeOnBallExploded_m43A75AD7693C255351C6510060A7B38F59496202,
	BallActionManager__ctor_m82F84496F0036FA4C8D611DBD668F1A717D015DB,
	BallEffectHandler_Awake_m9D6ADB7AFD76EE8F02C23DC390925BA816EF1C16,
	BallEffectHandler_Start_m666BEA086375F51680806A889D646B343418D649,
	BallEffectHandler_mf_m6A594780A2A6FE8284AA24C2E4FBBE9E2296C764,
	BallEffectHandler_mg_m3983B59E63E4C487F9A78BEACA1B3FBE6CAF86AF,
	BallEffectHandler_mh_m8F300747E53A4087A0F2DD625BC9C016505AEF23,
	BallEffectHandler_mi_m528D1314DDC168C4BA4CBA66F42F325792A152BE,
	BallEffectHandler_mj_m46D76D6ACEC62FD3719EEC119BCF4FA47133E84F,
	BallEffectHandler__ctor_mC3C9EA0247FEA0FC6AC8F705D7141B6503926B67,
	BallEffectHandler_mk_m0736365A0528B53B5AE2B31D909CB23FD517CC08,
	U3CU3Ec__cctor_mFB02249ED3D5BCFDDF61266DE592E26BD70B7D3B,
	U3CU3Ec__ctor_mB2E95812D5F6FAB8235996AD939736B806C8F609,
	U3CU3Ec_mc_mE63F9EEFEFCD1A04386D4818671B11ED606804D1,
	U3CU3Ec_md_mE26B6F6851CC8A1EA551092E7FC9521BB6AD1F01,
	U3CU3Ec_me_m3BA94B40664F12E722E6DAA5B144B1932553D690,
	BallGenerator_Start_m4D0E8D6629C1459FE76EA89F417ED27A61DAFC64,
	BallGenerator_ml_m4DF900D0A8270D97CDD3EA4419C4A54B4CDB6C69,
	BallGenerator_mm_m521F4558E05CC599F30D7A39D82E81DFBA815C62,
	BallGenerator__ctor_mE3FDB6F5B4010A1D5E6D58D16BC5F74F85CEABAB,
	CameraManager_Start_mE3C7B981C2ECAEC32EBC46817F7375F153435549,
	CameraManager_mn_m38EA7ED144940E8291C90FC1973D987C743147E3,
	CameraManager_mo_m5D27F1163CF9CDD0C0138438A8F71CB87F24B98D,
	CameraManager_mp_mB52E32D13B7AEE28C9B4DDF61105A8CDC8466E79,
	CameraManager_mq_m0EA5C2B203DE930932772D87EB6FEC7EE71A90EB,
	CameraManager_mr_mB15D52D48EDF2E1674565FD652578F830D2CF4B7,
	CameraManager__ctor_mF986325A67AC4558185B7E42AE5486BC61C17BFC,
	DotTrail_Start_m55D572C4717725B2CAE8C296718EA9E153CE5A8E,
	DotTrail_DrawDotCoroutine_m544B2810E3226BB8935B11F1FCBA78CD39D27D3B,
	DotTrail__ctor_m1FB42F428423DF42C7EBF53B35FCD9E84B2D5922,
	bf__ctor_m349CE7E3614F5FA57513DD4A3561B0A6CE2C5E63,
	bf_ms_mB54709CE446F9624074C1164DCB77B6F1BD06F30,
	bf_MoveNext_m884B4BE97B521830DD92A994B8C896557347214E,
	bf_mt_m8CEB9CCA687183C3D5896593342A6ECB7C652A10,
	bf_mu_mB416CD1EBB37F423B33DF5140C0E058A3FD7B0E7,
	bf_mv_m8104D0CF4D48FA5A8AAFE77192E0EF7CC186288E,
	DotweenManager_Awake_m44DCDF768CB7B8A3BAFF2605F2801F2C18949F6C,
	DotweenManager__ctor_m9CAC0DD928993F9B87C0E6C2ABF0A4EEE05CDC17,
	GameOverUI_Awake_m9644D0CEA1F1B611B47B31B4A08912CAAA829D9C,
	GameOverUI_Start_mE7A914C4B9F5A50781799E81F28806B7A0B4176C,
	GameOverUI_mx_m8A43E8B423DC3388576B26E1FBA3E76F612EE0E8,
	GameOverUI_my_m6BD2F242B88BE8A351EFC2C82EE9A7A9D94D404C,
	GameOverUI_mz_mBD7B2437CA14A10DDF3B2FD4D067BB4AB07D0958,
	GameOverUI_na_m737E01799FA40DD9125E5114DCBC4F93D78F23B6,
	GameOverUI_nb_m84A40F7F30F79BB06926B58CDC54303A00CCB4C7,
	GameOverUI__ctor_mBA2B6F5970DFB528F06736AB53AD6FACBA780B0A,
	GameOverUI_nc_m025916B01AB09B8B5FFE231832699E69027663B5,
	U3CU3Ec__cctor_m957872919AEDB94B7137E643842F5D85495E2AC2,
	U3CU3Ec__ctor_m0EB4E928E0EC569A04C92BB3365720565E268E45,
	U3CU3Ec_mw_mCB107D3D055850E96765C27CC85980F683C3FE87,
	GamePlayManager_get_Instance_m1D2C2FBAA8D163D399560A7E4C02BEC2D7B3DDCA,
	GamePlayManager_set_Instance_mCB6EF46D3FCEEDE2ACE0F6F7C63A0C9A19E91EC1,
	GamePlayManager_Awake_mD3F9A4345E4D32F6E2564E46CA667C78F57E1E68,
	GamePlayManager_GameOvered_m78B86ADFF3CBE4423E12CE78E5E801F931280FEC,
	GamePlayManager_ReloadScene_mF0D1D74D474F9912FDA753B38A36D0EE3971626C,
	GamePlayManager__ctor_m5ED88D4891E7CC9D09141C12AAF96B8ED632E654,
	Obstacle_Awake_mBC3DC059E6052B4F7027F828AB2E754BA17853E1,
	Obstacle_Start_mE8FDA3F73109552FB8E8595E37A775E3A37174D7,
	Obstacle_nd_mDDEDA7807649807425A4437B7E8216421F297D5B,
	Obstacle_DestroyEffect_m26C8810D7094A2D183939896E9E497F6D5EF0CF7,
	Obstacle__ctor_mDE5156FB11CFEF62E978EBD7EEEF7D903D40ACAE,
	Obstacle_ne_m7FB70B9BC4F12B5F86C10FB1F0368A58CE70C625,
	Obstacle_nf_mCC6059D64313DDA3F6262147F8F2CDD074FFC8D8,
	Obstacle_ng_m18933E23A4200119D32514F06FC9982DB0C11021,
	ObstacleManager_Awake_mA421F3B2E1A8F7EA32AD72D5FE43B6BFA84201B5,
	ObstacleManager_Start_m531DF614ADEE8A3482AE65A428BDD74200B070F0,
	ObstacleManager_nl_m74E35D251F47A3363DE2A15FD1C64A0A51FDAE3E,
	ObstacleManager_HandleObstacle_m0AB1DC50944BACDBABE6E452A706D2205CE4915D,
	ObstacleManager_nm_mE42B5BDD379BD78EA9CBEAC1A8310430CAFAADD3,
	ObstacleManager_nn_m5278904F2220976CEDBBFDA096DA06ED68312A90,
	ObstacleManager__ctor_m46A423647AD9739027087D308E50933D86C5706D,
	bg__ctor_mDBB0D96119111A43E6685B4C8EB9C94A9F6010AB,
	bg_nh_m5BC99CFE0E0B8109DA7FEC46FD0EA107B644558F,
	bg_MoveNext_mDDEBC2220E0474B68B2BEBFF7E7479E464D9EC9F,
	bg_ni_m8ED85DECDEE96D50215CF3881F585CEA1689FF68,
	bg_nj_m4CCA6210902BFC13056E12DC260BB56709E23180,
	bg_nk_m6CD04DCDFC8E4E686D32F14A43D731F92869E258,
	PfChild_OnDestroy_m99C8F37D50048E540FD6580D9068DA745EA7E69A,
	PfChild__ctor_mF6872A54317D9C1D93552A6DB0F341DA179A19D7,
	PfParent_Awake_mE124BF81CB632C32835522F5E32478917FFF6AF4,
	PfParent_no_m2DDF90A4A2CC0D7AC7F7B9114DC3E3EF3A806091,
	PfParent__ctor_m3925D790924CBFD0669709D0B5E3BC7E6BECFDDD,
	ScaleAndColorAnimation_Start_mE8E6963B7CCEBFCAF410EB594A25D2DC53DB7043,
	ScaleAndColorAnimation_np_m9810C8612C51B35B145A32193902D6B2EDDDB8E7,
	ScaleAndColorAnimation_nq_mC620E13DFD5DFCA714A83E69EB8F702CBA11E4AB,
	ScaleAndColorAnimation_nr_mC449E24A8EE2F4C53288AA03FF2FFA911CB36106,
	ScaleAndColorAnimation_ns_mA08A65E2711D0DE67609A16BA155E1F4E3B6764B,
	ScaleAndColorAnimation__ctor_mE3C7F0F51CAC02E0708511C6EA3F343F56A02BD0,
	ScoreManager_get_Instance_m71D27F07DC8C1B968B5E6693C613DDA2C6404E91,
	ScoreManager_set_Instance_mA213E6025FD45474513E29E85DCC50DA1BBBACC7,
	ScoreManager_Awake_m4CCB8A46340B283F5F89B856F1C48D0EE74A5C93,
	ScoreManager_Start_m4DDFF58DEE0C9B88A74A40BA9DF3678ADBAA08E8,
	ScoreManager_nt_mA6F166ABA84074B04C59C57B656F4E6F64CCAC27,
	ScoreManager_nu_m6557D8B597DEAF758C0665844FD8FB73C0683C8F,
	ScoreManager_GetCurrentScore_mC0072C9B88C4AD3A009917A64049018D664F3F23,
	ScoreManager_GetBestScore_m00DB771C7947EE52D9B49AE9449DC98D26D0032D,
	ScoreManager__ctor_m630A24D46A8768B03FEBEFD0660CB8EDF2FE8F24,
	ScoreUI_Awake_m6F635FE2CB2C6D3DECADB7C0F454D64CEA240773,
	ScoreUI_Start_mD1A15F0E34EA17C78D473C4B3E7BBDC7BC0B99C0,
	ScoreUI_nv_mF1653E6027706D3C88CAFD79C1B6556EDCF5EE35,
	ScoreUI_nw_m6F07A36F465F8F5D805F76F968EC7C33CD1BCD61,
	ScoreUI__ctor_m638664F853E1789D0FE675010D92B9B07EC13162,
	SoundManager_Awake_m386D90E2CF2E6F9491AE780AF40587EC52081069,
	SoundManager_Start_mA7FC8807A2DE6A6E5882E679598D55AB49734FB5,
	SoundManager_nx_mB9DA61616B3B32AB6D166F53227A59AFD174C8F9,
	SoundManager_ny_m240020CB508F3FFD3C61326F86EAF86A25040D28,
	SoundManager_nz_mAD4FFB6047E654D7D1FE25EB3D08E14BD64DDF58,
	SoundManager_oa_m2AB0F05FF33608592A6A0DBD453368935142FAE6,
	SoundManager_ob_m3D2AC42D450673AB09CF3D236A6383C723AC0660,
	SoundManager_oc_mB84CBCF195F8952B01B49B811C2FC696B181FD9F,
	SoundManager__ctor_mB0D0985174F015B1122FB408CE812FAAC077AFEC,
	Wall_Start_mECF5392AED0A71D948EC01BBCDA06676C909B6CC,
	Wall_od_mFFC4213EA96BC1B0734722A59E4C6EE748EE7CD0,
	Wall__ctor_mB23FA331F047DABDB49861A28ED0D4C5AD6B14B5,
	WallTypeHolder__ctor_mA4EB967F9FA4F3FEDD2F2E25005B46ED7DE3104D,
	ActionManager__ctor_mAA3CDA382E507836841588422E0CB2942D2EED08,
	Afterimage_Start_m477A718F52E90CF365F8EDF1223FDECDF5B1D1BD,
	Afterimage_OnDestroy_mCC01B33F85F857E293951D2C63C3246765A3CC67,
	Afterimage_Update_m2C00C518F769F227C1D6FAE8EC9D032E7409DEE0,
	Afterimage_Move_mBCDC9C3866E9CB1C7BFA32FCC9E9B501AFB1C938,
	Afterimage_oe_m540909EA78A3CD75803D76ACCCEA8CFBD43F7100,
	Afterimage_of_m53430D821C87FED4C74D95A319EDAF3991833BFB,
	Afterimage_og_m7E08243082EA0B78108D39173A4432EE9DB91ABD,
	Afterimage__ctor_m1F77CA1732919E245A5C071F0CBE7D444A00A9BC,
	AudioManager_Start_mDCCAB3FA2C9B2E7A57DABAFEAD5E9CC45A97F3AA,
	AudioManager_OnDestroy_mFB6FC90B2A0EE069E851D92B5A9FB9A5C99348ED,
	AudioManager_oh_m6CDF1821553EAB1657DE9453F297217917D4E693,
	AudioManager_oi_mD64804F5BAF49FE5900E167688ACF7F9A5AF8651,
	AudioManager_oj_m33CDB773938FE7086F38AF5983C6130B4EB333C9,
	AudioManager_ok_m1691041983F8EA0C7FEC26AAAF026CFF26CBD491,
	AudioManager__ctor_mADBE84ED80DA8399E415ADD39EF226B77230B2CB,
	CameraManager_Start_m92B55335EEAB451EE6F3A39CC9FBD559380D11C5,
	CameraManager_OnDestroy_mFDFC3F8CF8FD7E949474D53EDD6839F97346FD17,
	CameraManager_Update_m2C9527CB46AC4B266C9CC9BF618577FE9E7912CA,
	CameraManager_ot_m40596E993F43A35386C42AAA1362BAD55123905C,
	CameraManager_ou_mC39888BCFFD759432FB3620F98F052E2E03A2E95,
	CameraManager_ov_m24C7884C5093BB61A68D3D825121B6D413B7DFE3,
	CameraManager_ow_mB394BAF576FE81B7F3AF4EBDE02B4AC1117AEFCF,
	CameraManager__ctor_m4D352DFED4530FFC5FE1067A1B1731A4C042DF09,
	bh__ctor_mF5A530549DA4DBFE109CB023242738538AF997CF,
	bh_ol_m03609B9F398488D2CF5D34FAB10459D1DB9038B4,
	bh_MoveNext_m98C5B1562DBF34E69B483C6775FD0126372549A9,
	bh_om_m5EEC1EFC9EDF030476525ABD3B16530DA29B5B51,
	bh_on_m8C968DCD664D43ECA2613490DCE59E7C8B46E8D6,
	bh_oo_mC3A540EA53FB26AD9AE80F8FB1A69D656866B7BE,
	bi__ctor_mB1C65277A84566A682F07829B202305EB3DDACD6,
	bi_op_m18D32C3B194528D928191242DE29A514C066B4CF,
	bi_MoveNext_m66AC1F59E57B68785EC60F490FAEAC7EF1B9C6AA,
	bi_oq_m8FFA01AC6DCE92F6FE34C849A1493DB8FE7D6A2E,
	bi_or_m42A1D41CB80E5DB77119B66632099F5E36B1A1D8,
	bi_os_m3F7A853E3148AA1B5738643ACF5F90D8D9D24B66,
	ColorManager_Start_m1616D8A28EDF782D2636E305E67413F7CBB016E7,
	ColorManager_OnDestroy_m375D79347704FA091D6ABE01E7E412024B433040,
	ColorManager_ox_m8DD08C77B8446A4C7E9F11E8F035B7635035DE29,
	ColorManager_oy_m61D6A7E9D65ADF07F28BAB97DF0E56080B4E8AE6,
	ColorManager__ctor_mE3078F3B8DA2C08E63DA49591B6F2AB7343F459B,
	CountdownManager_Start_m55393FF46FB7849059CA39D1ECC9A48C85D8D4C4,
	CountdownManager_OnDestroy_m888B2F72DC8E42A58979244AC9D5F816B5BEF552,
	CountdownManager_Update_m8DDD6D347634BCC7801E3E14AFFF04FFB4EAF354,
	CountdownManager_oz_mAC279F9F74FF7BD576CE1E3287689E85267D93EA,
	CountdownManager_GetTime_m51FEB44BFAE8084D786D3CC238F6AF6D79CD0850,
	CountdownManager_pa_mD5A47CBD44BCF7A14FF842F3E4E319897F14B31A,
	CountdownManager_pb_mFD42A6C68B8A287EE8A26849EA3449200223CBAA,
	CountdownManager__ctor_mE823D6C1C1BDA4914FF22CEDC98FB2C232D713BA,
	GameManager_Start_m355B83ED1631702B30CC64B8768982A2D258F68C,
	GameManager_Restart_m541C04252BEF29F324EC18D0F114F0C1D6347314,
	GameManager__ctor_m58F6BC63E1952578CEB1531BB04BBD1BACCC3F1C,
	bj__ctor_mCD393AE7FB3AC826644FED79DE7CF47B8DC85EA6,
	bj_pc_m7AA06DB318893155ABFA87CA19DD1E66FA18FA48,
	bj_MoveNext_mC7D22B2EEEB294392A2C50ED873DA5D8DBA1EDE8,
	bj_pd_mF50DD3C1BCF9F232E878BFA84D345A711EA74676,
	bj_pe_m64751E96AC1C6DF6999B9E60B9ACCD5C438BAD14,
	bj_pf_m4C1F52819397F9FB6C4A047A5E6710382B7C75AF,
	Item_Start_mDC43C7E15C9E7E0F6C7C5FA3A6BD50E34433575C,
	Item_OnDestroy_mD2EE5BDACDC6D5C77B9D9F62BD6BB8D0B2CB7732,
	Item_pg_m251F919DF7FF7BEC2DB908DBEBD89F8FB40B50D8,
	Item_ph_m067EC5417042A329C547FD637EC227677F06171B,
	Item_pi_mDD110CEE36E510751FEEB751C96909CA711E5421,
	Item_pj_mA36B1415A6AAE697C5380457BC61368362242474,
	Item_GetTime_m0A55D1280012D3CA043651A6F49632F07D9DE292,
	Item__ctor_mC997281247671AD4C70FBFA5D6DE1CD650AB6648,
	ItemManager_Start_mA4045F3FE079BC2F42BFEF85FEBEE839C8C9E74D,
	ItemManager_OnDestroy_mA194ADDB8E94FD0254B4EEC85263392F58C5933C,
	ItemManager_pk_m7D35B0172AD4225BCEFFB14BD95C4E2132362889,
	ItemManager_MakeNewItem_mA71C3F3047C0F2BA55B50AB872D00EAE08540D3C,
	ItemManager__ctor_m50B2F30D7A1F0E056A106253A8B2777E8BD2941B,
	Player_Awake_m2462269B88A45E0B0EBAF1C6231413E49D20F7D2,
	Player_Start_mB33B2176D4CBE390182A05A3CCE276BF323C4628,
	Player_OnDestroy_m5C90E005263826EE41186B571EBD521E63A8796F,
	Player_Update_m4DAD0B204CC333CF25A9A90B460CBF27E0DC9C01,
	Player_pu_m396FAAA4B3481C190B0259655D5A83AC5766A50D,
	Player_Move_m916785F01B6567CE631AFC936BC4B9FE122E7716,
	Player_OnTriggerEnter2D_m5A5D9351820FF36CBA383221C2663AB02ABDB7EC,
	Player_pv_mC3ADD2E4E5202858DE3A9DDF282D52D9116037D5,
	Player_pw_mD378E906232DAF26AD3A6A80BE72087F2B61B6FF,
	Player_px_m2E928397BA07D62575B8C700D1E7BE984DC07F9A,
	Player_SetToReady_m45694D7E2B364FC0ACB3EEC95A5EAE964F1E3A24,
	Player_py_m097C7215AFCD8681E251AE97272BD1D5DED66174,
	Player_SetDirection_m980AEC03B72EDA925B3FE6622432102A3393051F,
	Player_IsStarted_mFA5EBBA9688DFA3E47C21318C08C91759C7B3307,
	Player_IsDead_m71FD3FB80F0D5397BB42A503239AAA12E31F179D,
	Player__ctor_mDCB31459BE35C4F17DDA704BD31B6E6267D27A21,
	bk__ctor_mE90D457218E41BF8F1F13B3896BF1BD29D103E53,
	bk_pl_mA159A45CDA9DBC3CCEA41034242D2AB46373CBC7,
	bk_MoveNext_m375CEF5C71AC0F2885B81C3FB0E50D4A6F98671F,
	bk_pm_m898FC22EFDC5790D418A9CDF5745ED8A11D7D52E,
	bk_pn_mC347FF8FB0A7CCEE6D9748E708C4220AB6248DD9,
	bk_po_m537494BA3B5308C6E9915C3273E0F7EB26560A98,
	bl__ctor_m328647131621984E3905046A7CDF2810E9C3CA46,
	bl_pp_mFFA764E1D2939102AA13B77E2080C2994DCD2473,
	bl_MoveNext_m6A76CAD56094D249E75A2F0D9043B3E5230783FE,
	bl_pq_m6D5ED13FDC4B40F47B925C3E4947B1E0E0BE316D,
	bl_pr_mCB132AB9EE75B7895A2F311086B266012366E315,
	bl_pt_m9BD7A646EF97582D8F055225560BC816BE783BA7,
	RedMask_Start_m22127DD848AEDF3D2B1BF7B82F87A7E0AB6B13F4,
	RedMask_OnDestroy_mD34DA89928AADA3C317F74BEF977701E8D053526,
	RedMask_Update_mCAA2B0556342F2E5FC21446D66E26189617F11E7,
	RedMask_pz_m7DC0959F635CD80A14EA4CD10B77AB0F9F0F5727,
	RedMask_qa_m2F7D9EE01945ED95A3F3FCE5978A7842477E248B,
	RedMask__ctor_m557AA296A5FFEC7CEDB7B18081C265E3D183C148,
	ScoreManager_Awake_m687FB67A3755ED50680F7FDD6A3FF7F60BD0013C,
	ScoreManager_Start_m63250F37FE772E9E0BB14D1228F89246B37D766B,
	ScoreManager_OnDestroy_m25588987FA526B0B0F60B3BAD4BFE696A0538162,
	ScoreManager_qb_m69E35A687C31521FE2816AD8ECAE6107F3B2DE93,
	ScoreManager_qc_mACA81F8EC827C1D5771A275C47F8428C40ECD117,
	ScoreManager_qd_m63A3980B590D77DFE02058E8DD077848181CA38D,
	ScoreManager_qe_mD1E9339107F20A2D3F09E723991CE927AF5DDE33,
	ScoreManager_GetCurrentScore_m12F15C9EE6400156E638E95309C305C921190CF0,
	ScoreManager__ctor_m9C73858C6176A83E8BC5244196B6E35181CF96B6,
	Stairs_StairsEffectCoroutine_m227271C14F880498AE51B99A9B48FED1D7CF5FFC,
	Stairs_qj_mCB57127D045EFE64DDC0A819C3A4F4310ECBC317,
	Stairs__ctor_mA2392CCFE155A3EA346558D7764316A5E9429BC0,
	bm__ctor_mF2D1A8506267A940D656F6B5F41DC102DB5A1BE1,
	bm_qf_m15BC7A176A85EE1BE0CDAE097AD663D13A251561,
	bm_MoveNext_m2F17F6624509619D6A3F21BE3A9A37B7C3895970,
	bm_qg_m644AC8EE90689ED9ABB928834F81A6846E81866E,
	bm_qh_m57E0FE22BC6A792036CDE77F825DA6C72CE3A8CE,
	bm_qi_m0077613FA57E2537F1810A7D5FD49FF8ECEE7FAA,
	StairsManager_Start_mB49D980A10F1BD586FB8EA8041AB24E426F2AE70,
	StairsManager_OnDestroy_m2DF06F6A83D2204F4EC3450A8EC9F6213E140A49,
	StairsManager_qo_m61328A15A0026B7A9B0ACD1CB3A7B6FEC5802B79,
	StairsManager_qp_m42992830797DA3217093D7E552AFE3AD41550030,
	StairsManager_qq_mFEFD579161834F8DF0E3D7515E11E617F9E6BEEA,
	StairsManager_qr_m49F4B6D6F795D0DEAFD4A42D139E2B6D8EC6C478,
	StairsManager_qs_mF8BB6777BC85B78946ADE370E307C74D4F2A3493,
	StairsManager_GetNextStairsForItem_mC855925B7FDD37CF9CC9DC5A6E8F7A170DD90076,
	StairsManager_GetStairHeight_m7B78603249F88D8807AF9C37367E76412DC5D455,
	StairsManager_GetDistance_m4C908B9829108C012959806B460604E545B9B7C0,
	StairsManager__ctor_m96120CB7589C1837DA2C3A0F5181CDBA03335F64,
	bn__ctor_m60FA6EE620B5EEB422C0164E7980E3082B5A215C,
	bn_qk_m138BBEF88F9B2E512265379BCDD79A2C12F240B8,
	bn_MoveNext_mEF805D59DBCD832219FE16B9568F9DCDDB8BA149,
	bn_ql_m109DB4A0880DC979DD648A7B2EF1C58AD163E320,
	bn_qm_mE6ED2E4ACC16B108E7058E1F08D926545B186943,
	bn_qn_mB492FA84896EBD1AC38F1C14B502727A944A270B,
	UiManager_Start_m9A6F84C4E6AB2F1613F4AA7105A758824320ED40,
	UiManager_OnDestroy_m443C9E3AC23B1E70199611FE4AB6375CBA8B173B,
	UiManager_qx_m4799829285D1CBF84C0EB15CC81107DA249BF179,
	UiManager_HowToButton_m8992057B7335A5C65337E57522CCF37E7B4D45F3,
	UiManager_CloseButton_m2F4563EE7D1FB0BEBB0FEB7761C77BA5197929F5,
	UiManager_qy_m330E48D5005970800DC3F33163390237CBF0BD83,
	UiManager_qz_m07D89D21F8702B8F0268C8E597FEF0772CEEF69F,
	UiManager_ra_m5FEB9154A3840B8F0CA078F1E46EFB9C1CC9D4D4,
	UiManager__ctor_mC8510C4460142DFF04064F1FC7CB9E7FB4A096F0,
	bo__ctor_m0684CCE4C6D8407226F18C8FCABE051B083478D4,
	bo_qt_mAD667D35DD34E366616C695EF96D5C4D977AD0E6,
	bo_MoveNext_m4D16E6606826E485728274234C12DFEC9C60D176,
	bo_qu_m594FA061A4FE9EF4AB1B38CA4644CCB1DCD3FE76,
	bo_qv_mBF7E0FB66C0734E86501417F48D18E16A3915B8F,
	bo_qw_m5D1A8705062F774E4905BD4797C7E63244A8FCAB,
	CameraFollow_LateUpdate_m125090DC50DE475F408C513F91BBF6049ACD2AEA,
	CameraFollow_rc_m4338BF7E92242FD6B37D444291731A4503D5E1C8,
	CameraFollow__ctor_mDF52FB7BA37E9AD94F53A364F8BB76F866535C3C,
	ColorManager_Awake_mE6F9306AB138F12FF6E6A8F631C22A4A1F518594,
	ColorManager_Start_m99ECC72F5B32CD93A8824C724F131C06212CB046,
	ColorManager_rd_m4BA2E413479541BA8211721217C23CBBF2837545,
	ColorManager_ChangeBackgroundColor_m6A305CC596FE38BA5D88AE2C089EEE15F78957D7,
	ColorManager_GetCurrentHue_mA02EA7BBBD923EB7011B63354CCD01538076910D,
	ColorManager__ctor_mE9F395089A79AD2B77501D1C069CEE436E3626B0,
	GameManager_Awake_mD3586CDF4BB13CBE5FD3168E37B1FDD7CF5A475A,
	GameManager_Start_m8ECCF0E5CF9218BC02F261988E543FC605A60FCD,
	GameManager_ri_m3A1EFD50DFED0F422CE9D7677E0294D375582B84,
	GameManager_Restart_mC6F3479CE10A521CC5CE860DE68D52D5F75FA328,
	GameManager__ctor_m9BB33D4B97961DA9206D29193B632068A2F6AE3A,
	bp__ctor_m58E0D58650CCD871E28EE4A7E0B7D944B6BB112F,
	bp_re_m1A40D0461113A9E887C34B6F2C03BDBDA2163C89,
	bp_MoveNext_m2473DAE8994E9F98693BAC02A4A79FC92CF25F6F,
	bp_rf_mC8B948BDB8CE9CEACEABD43BED9476672CA1E0E0,
	bp_rg_mE53EF6D7D8816CEBDA0A70966C99833BA60DE3CF,
	bp_rh_m59D1DD8C7D20A019D29C490FCA2393FAF3FA841F,
	GameOverEffectPanel_Awake_m52008E2521C3D1497745B0A55C1F5C9CCE0F8893,
	GameOverEffectPanel__ctor_mC4FA60E4FD8E2005A1F9DCD7B713A49B9ED6816D,
	GetDisplayBound_Awake_mBE123233777B6F65608BD581A00DB8216A115E22,
	GetDisplayBound_GetLeftWallPosition_m9E4EEFEFBF800FF8C98CB2D8DE387B9544698437,
	GetDisplayBound_GetRightWallPosition_m12EF537E4C2D53B830057A65EA1294083C83F8F2,
	GetDisplayBound__ctor_m4FB46BD6F2B086913FDA087A653DA9853792A8A2,
	Player_Awake_m879C97D3E6BAAB8B7896DBD486140987626AAF0E,
	Player_Start_mD6D66C529373A5CDA06754E9A2B1D135DBCDEB5C,
	Player_Update_m7FDC6A6CA5E7B016FAAE2FE129A2665DD2B4D8A8,
	Player_rn_mEF4096AA18A0E2BA0AF7B350C7A29879E9F22239,
	Player_ro_m68BC4BEC681240EBE6D491AE3C88CCCC15B9BC71,
	Player_rp_m69D7FBEFD36C3B81CD493E5CFE91BD92B4433A27,
	Player_rq_m6E8BF252B0B08F6D178A3DEDB1C727A94B09098F,
	Player_rr_mABD73DDC22AB704A11AD58D1D62ECEF4E1372994,
	Player_OnCollisionEnter2D_mF8D3D65B6D85BCD59CAEF13B5BBA69A075F81384,
	Player_OnCollisionExit2D_m981D0134E0BF303621A0ED9C08BF5A5051296422,
	Player_rs_m206D4D26F38CFF643308EBE6F1980505B6655FC8,
	Player__ctor_m79D03802B4473AF5801C91A72EF6375BFD07F314,
	br__ctor_m722F9230FF4F6FCF047BB258C42A77A395193E3B,
	br_rj_mFBFEB5EE1882F543906ACB9D8794CF073C1A15D7,
	br_MoveNext_m03E4A0917F437275BC620EE14B7683E9421E78A9,
	br_rk_mF9684FEB13CE7368D0FE26710318E2A7A89F224D,
	br_rl_m9797D2BB05A75A57F5895BCE5251C8433433CC8E,
	br_rm_m6D407F6E3762D143D6451AA0BE25F1FAC06820BF,
	ScaleAndColorAnimation_Start_m560E50F6B004AD91D964BE6835D578AE21144356,
	ScaleAndColorAnimation_rt_m4A331EB768E244C1B18DFA4C15F8EA94D3BC1C9F,
	ScaleAndColorAnimation_ru_m924EDF472804B55C72F9D2EEFB30F6EBE7BD3300,
	ScaleAndColorAnimation_rv_m150CA9FD88FC6EE77AD6F31B16A784B11FFFBE61,
	ScaleAndColorAnimation_rw_m91E83DBA12C8379F6FDE039A4FF68AA8FC7C19B5,
	ScaleAndColorAnimation_rx_m9277C6B3494E83FD998A39271B62F5AD3068C1C8,
	ScaleAndColorAnimation__ctor_m0DC0B88B0F2D1683D41D5B8839FF16B058671F34,
	ScoreManager_Awake_m7A1357B88AB36DEAB1E8C6D5BCE67D5A1EF5B7BA,
	ScoreManager_AddScore_m8623FE126777861C1B1C97C204E01409687EBAB9,
	ScoreManager__ctor_mCDE56329D0AFE13A27802CA97AC0602E06A68F64,
	StartEffectPanel_Awake_m0288F8E960EB9612091DBA272008D5F3583E6C09,
	StartEffectPanel__ctor_m6177C3786FA0E115AC9059FC6FE53B709A9B66B7,
	Step_Update_m477D30BD79B9F59B47D8E9AF23FFBEA0BA7E488A,
	Step_sc_mA178C895BE6CBE7ADAB03DAC848F90D71C3144F6,
	Step_LandingEffect_m3DFAAE578F79186B5B3AE33598F9AD5DEC47CED4,
	Step_LandingEffectCoroutine_m70B35A685FE8BBD1104432ED9EFF98E2295E2DD6,
	Step_SetDistance_m4C29D365F22BA312FDFE093EA8143B0E8750E817,
	Step_SetVelocity_mF52FE45E549C08EF2A44CE2BB66C1439F7119ECD,
	Step__ctor_mE570630E48D7873403A07B1E1B9A85D162987F3C,
	bs__ctor_mE811588FB0DEFAC15927FE2097153C20789F8D4D,
	bs_ry_mE57ADE478E205D10ED90862B26EEA763727A0A9B,
	bs_MoveNext_m00CE97013E4177EF1EFF5CA27D4A7D773ADA8BEC,
	bs_rz_m867B688A46ECD23F82EEEB1DBFD31A5AFC76DFEE,
	bs_sa_m7F12D97B69E786605225758E14798840BD5E341F,
	bs_sb_m0C6B2004FC51DCBDE8037CECC56E93440326F0B8,
	StepManager_Awake_m55B1469C6F64E25E96F41194B117AC2B9745DDE6,
	StepManager_Start_m8D10619F9E441FE16E3F36E60F4B07D518130AFC,
	StepManager_sd_m8609E49D8C6F8E21B17A09E5D3F74D80F2A1EAAF,
	StepManager_CreateNewStep_m4241375FEF99368701AB0EB49E5F85475EE80EEB,
	StepManager__ctor_m3CC58BF48624FDFB7E2C6A303699B1E47EA55301,
	UIManager_Awake_mB9B63EB11315DD138C7A360764E062D38AB2DF8D,
	UIManager_GameOver_m03D2592F9838AEFFE33C75612F8D98FA0AAB4832,
	UIManager_si_m102009651ACED538BF6DDC4E0B55905D6DF35740,
	UIManager_OpenHowToPanel_m910AF731EDBE273AD5011A110DBD029AB4433FDB,
	UIManager_CloseHowToPanel_mFAA338EB570B00D96230F51AF83A9526148C4F0E,
	UIManager__ctor_m921859BF8E3190A20AF69B1893E31BD2302CA4DF,
	bt__ctor_m5C8158A7676982C7EF0A53E8C1DD139A87C242AF,
	bt_se_m8CAF97A934FBA0E6A52A131C2ABA79633AC6C627,
	bt_MoveNext_mF1DB1F11551AA22687EB6EF8C978D8234B86A791,
	bt_sf_m8B94E94652F734EC39FAEF250F3FC7FBBB2A2B26,
	bt_sg_m571133A45007639CC65D3788EA539B23EC17250E,
	bt_sh_mDBC1AF4BBC281E3B7646D8E8FA353B164BF83386,
	DisplayManager_Awake_m8F62E67CA557943AB6F8A21D76151C3C0215B3CB,
	DisplayManager_sj_mF8B8BC51C64096433D0AF8C5DA59273BACEFA8B4,
	DisplayManager_GetHeight_m308B3CDDEE1ED36945E254BD8E8BF6FD85941743,
	DisplayManager_GetLeft_m003BFF1C3243236661F49D193BE35D74BEFA2967,
	DisplayManager_GetRight_m6A06DB0EF854A6627D7FB7DCEB236C7B3939B530,
	DisplayManager_GetBottom_m3076599AF4C05FF3617309255CC9B9A1D8840EE1,
	DisplayManager__ctor_m7BFFB9C355D3AD94EAF667461DB9E78954ECA356,
	EffectManager_Awake_m77483CE51BCC5C5A4DA98996256A7249450E359C,
	EffectManager_Start_m0326A72E70526C8866CB1390006B605BFDAF71E5,
	EffectManager_ss_mF52CE15FB6D18EAB48CC8358237ADADE866A0A58,
	EffectManager_StartGame_mB45D36A776DDEEB82D14AB1525358FCBF15B7098,
	EffectManager_GameOverEffectCoroutine_m0AD98F78BB8313509A90183A71A6532DF56F9FBE,
	EffectManager__ctor_m69EB2F67C7F0FEF460FAC809D0104DC6581C5A44,
	bu__ctor_mA741D6EE6818BE34BD94898A915BE2EEFD0F8EB4,
	bu_sk_m149B8FE0FF284CEA447B07E6ACDCFF49F28239BE,
	bu_MoveNext_m089286570A80060024D4911FCB5D77BF94E3D168,
	bu_sl_mA87D1753E7C694FF88933A010287BBEE1806306C,
	bu_sm_mA64A4E92DB9AB16E7F98CE1A51DA4CC9D7E43503,
	bu_sn_mC4FE8203ED85FE18920A9024785A2BB0DD2AB112,
	bv__ctor_m7CB8BAE85AE2FEBAD7A0DB62F845E293A2F6BBBC,
	bv_so_m9C839250617AC1325A4DFFC88EBBE2AE769DBA27,
	bv_MoveNext_m39F83C7E26DC51876B163807227B6EA6E2F64C5D,
	bv_sp_m64300D0D31E68663E3DB4E36B566139A11475DC1,
	bv_sq_m44F878475A2886A17DB290F8467FA19709FD1C65,
	bv_sr_mF56FFCE5DC64AC31B086BBD656A43070D3ACDF9D,
	FollowPlayer_LateUpdate_m2524F596591BBB0C3CA2685E4C316D962A9EB153,
	FollowPlayer__ctor_m47FC9E6A363317D93CA1D76BB5AAC2797E36EA6D,
	GamePlayManager_Awake_mD0D181D4C6323051493FB268E8A5B83F9275D025,
	GamePlayManager_GameOver_mCD459E7B8B01AC658F6B5530B10D2AEEE0BA1DFA,
	GamePlayManager_Restart_m0BD1DB26456D15416AAF729A0E0A2C086082F167,
	GamePlayManager_Update_m32B0A45E78BD98F857B2A88E2294C9F68A702B0E,
	GamePlayManager__ctor_m404F484E4CBF8222C74026BAE6A8350F522520A0,
	Player_Awake_mF11684C07780DA36501C23E2EEE88039886D26AB,
	Player_Update_mDE6038D6317FDBF40E7615B729C4AA3BAC1A9B3F,
	Player_st_mBCA9F98A9E66C689CA76B9C4625CA91F022D610E,
	Player_OnTriggerEnter2D_mEEFB6D36328A00799783DFB21752ED75BC6AE1ED,
	Player_su_mBA405262631EBCABF09F118D21444B3AC4A09E30,
	Player_sv_m9363643DD8A4B444F8F41E839231CB242E5ABA9D,
	Player_sw_mD96938EE65DC4560DAD9EAEAFFD2CC9AD0D2BA0B,
	Player_sx_m72814A85321189DF7FCA5F2612F34EB830A9BFAE,
	Player_sy_m71C014C5A76A686009648DFA0C47646712FFCA9A,
	Player__ctor_m9622A59F819995B09A5F9FF94F92083D311C8EA0,
	ScoreManager_Awake_mD4C4EF88A4E207881815B36C012E315A0DE4C6D1,
	ScoreManager_AddScore_mE0AEC824A218966110ED131F108B219F60994A09,
	ScoreManager__ctor_mADF0093B4268C7DF3AC33940B431D611E7F11576,
	Step_Start_m7232B02248375DA1CAA12F71579E83B61A75052E,
	Step_Update_m73AFA1DF3BFA53A8F3BA4A5EBC945D98D3F23316,
	Step_sz_m1256430A6DBBC9E339E7CA6E283AFCC206E21B20,
	Step__ctor_m41211B84D1A20C3E771DBD8502C7BE465FD76163,
	StepManager_Awake_mC6891B1C0065C2A0D2FDD74F2068415708491D1B,
	StepManager_Start_m95E00D52CAAFD9615B9D72D2711A1D9EE5EA05FF,
	StepManager_ta_mA47F8EC84DCE407C111A11D3290EE28230118887,
	StepManager_MakeNewStep_mFB8158B5306DB1C2825D2B022E3BE063EDC77A09,
	StepManager_tb_m0F755C3307D3DE3C8FA8B603D4D9939E29C97C37,
	StepManager_tc_mD0938B21DACDB27783FF30EC86FD27F976AA9775,
	StepManager__ctor_mEE66D60797301BE8442D5E3EED28FCDD078782A2,
	SceneLoadingManager_get_Instance_mC629F875CD419A5CFD47E0D43181CF3D64B64BBC,
	SceneLoadingManager_set_Instance_mA8CB476EF6410520B1037C6DA267508F7435373F,
	SceneLoadingManager_Awake_mC332A1B912C8CCD4E7A416AE952BED6E7AAC7AD2,
	SceneLoadingManager_Start_m931133546C6785E7EBB718729147DFAF3E7B39F8,
	SceneLoadingManager_tl_m2A879C63FC69DE454472B19FE6EDEF6A5E2735CD,
	SceneLoadingManager_tm_m2A84F8E381AA125F5788B084F8D28BE3660F3A9C,
	SceneLoadingManager_LoadGameScene_mCD738F5BBFEFCDC2827D9FFB4B4ABDD6C1BFA06C,
	SceneLoadingManager_LoadInitScene_m3663C940F4464F6A4C7BE22D3D5A84021124F707,
	SceneLoadingManager__ctor_m4845613B2470610E7B8126E7AE41B07A105FE6BA,
	bw__ctor_m688E71FD9FA8B1F9A4EDF83FBEA3440751586F95,
	bw_td_m6BDD846ED383CA1ECB9B9982551F4AC70B3885CB,
	bw_MoveNext_m45C877637E256C6007DEBA096237D4488303640F,
	bw_te_mA733E58FB131E100FFB27B41CD2C9116AD35D2CF,
	bw_tf_m6D86C1EA1D3533CB0AE8BF05FFC4470FE66137E2,
	bw_tg_m8CDDF88FAD26A5D952EB4EDAE907241E83BCDB9D,
	bx__ctor_m99F001AEA6EE6F5EF85B6612B8F73139F06A5754,
	bx_th_m65577B103532FB1E6880E44575EA8ACB2BB256B3,
	bx_MoveNext_mEC5B7C92A984499EDC0B37B9C27B28BEF4A9E631,
	bx_ti_m4EA35E51E73AD56A112AFFEFFB93EA7223E07E32,
	bx_tj_mF21D5AB25AF46073BCB09F79C402DEAAB47972EC,
	bx_tk_mEB4FD4B32A292C8DE78B592FB30BCE79D0332A80,
	MainMenuUi_get_Instance_mEA374B5EB3D68D62A42E6D8A5620CB27DAF169FA,
	MainMenuUi_set_Instance_mD61263C4AED21ABB92A62AA1E933A079F604432D,
	MainMenuUi_Awake_m39097B9AE40FB9FA63ACA1B1E3ED5691657D0576,
	MainMenuUi_OnDestroy_m5BE3AB5EE3AFE6F20BC708630CA7CD1C7FF9F7F1,
	MainMenuUi_Start_mED6520042AE0226330F9A68F164DCF2E5FD30EDF,
	MainMenuUi_tn_m5B31D7BB64891992D8E46D2D98E04EA83B0CAABE,
	MainMenuUi_tp_m9A2570A90F2B5D97BCFB2A16D4F397B9511614D5,
	MainMenuUi_SetSelectedCarObject_m93E60E533BAD50107390B36163987BF24EA3EDF7,
	MainMenuUi_SetSelectedMapObject_m6FA566440FE04467927C458D5D9A3BD77924CE95,
	MainMenuUi_TestLoad_m396C06805349F1D5B3560F109ECD1BFB65F44413,
	MainMenuUi_LoadGameScene_m97421B6389E5A1446938CDAD39B5D46E91E60C6D,
	MainMenuUi_ShowSettingsPanel_mEA7270F28B793F47E78FAE7DAFDD0D29ED7A5BEF,
	MainMenuUi__ctor_m6CA05116A2D7FDF37893FFCE4BF35EAC1BCC3D21,
	ResetDialog_Awake_m8E57997616E2BAC85244041D1A5DD55268E56D6C,
	ResetDialog_Start_mDBB973F8CE6ED279F02A05C855498184A0E29859,
	ResetDialog_tq_m4981BF336835534C3E93ED033E66B303D4630D3A,
	ResetDialog_tr_m8A3BA9CB5EC9194D895A7A92FB78086633CA1A48,
	ResetDialog__ctor_mE8F2835200BDBD2717A5817E8E10211ED79BA8E1,
	SelectionPanel_OnEnable_m1DADB51292717411554EB9147128FC7AF45C4386,
	SelectionPanel_OnDisable_mBA04B4869FDA4362807F2930F90EE498E81B021D,
	SelectionPanel_Start_m5FA94B0C9F7904E415164503837C9A81E1291180,
	SelectionPanel_ts_m5D48AE0AD2B1B0C3D716FFE556DFD7C862BB8169,
	SelectionPanel_tt_m73CBE6A4F492C1CCC06BE0467759C497D6DC4273,
	SelectionPanel_tu_m3D04ACA7E860AFB77B85557849B49C105B174D90,
	SelectionPanel_tv_m167F521A1E75BDDCD6A232F00B900472EAE1E638,
	SelectionPanel_tw_mB665DF4CACBEC764CEEC675130D675377414616A,
	SelectionPanel_tx_m4F2330EA28388147544A87955E250018E09585C7,
	SelectionPanel_ty_m533739C84370B95BBC126695C01B8B2ECCE444B3,
	SelectionPanel_tz_mFA1419A28459BC10829A4970128F473582134812,
	SelectionPanel_ua_mD4259589A894DEB29D0F7C295153126ADF911AC1,
	SelectionPanel_ShowCarSelectionPanel_mBA37E10E2D7E3D16C60526B5F95E13BE4EE15775,
	SelectionPanel_ShowMapSelectionPanel_mED5DC672DD6175C24DD49AD9CDAF5E9951E530BD,
	SelectionPanel__ctor_mDBAA94C3B8A273C5AEBEC51949CBA28E682DA340,
	SelectionPanel_ub_m309919094F5CA07F1D6989A95E2BB5E0209DBE0B,
	SelectionPanel_uc_m71A535C5B4B469D3D0FDEA886839B3216BA1DAF0,
	SettingsDialog_Awake_m6D770B068CC33AC48C9AC31D57ACFC5459A45F00,
	SettingsDialog_Start_m82D7AA1FFB7E37F23F224F01A569032C02F051DD,
	SettingsDialog_ud_mAAB7AA7CF6E3F24C7F0685083BE6FB02297E4766,
	SettingsDialog_ue_m8D6BFB5D5F0A5D9CEB3F5D2FB7A94C474097ABA5,
	SettingsDialog_uf_m00E534A6E858036DA05F1902E3EF2DD6FE9178F3,
	SettingsDialog__ctor_mFD1502511486C4ABD0710ACD212BB866B8DC28CB,
	CoroutineUtils_ChangePositionOverTime_mE6C80C7FE1D540C9903782617333D762A349D985,
	CoroutineUtils_ScaleObjectOverTime_mA76626931E9F66594971753FBDD5F4DA44976D48,
	CoroutineUtils_FadeCanvasColorOverTime_m54C2CC0BB193727D3A32D7304BED0E171081C21C,
	bz__ctor_mA3CDF975209598BC497F0ABF4195790FA8F221A8,
	bz_ug_mA25EC5FC10D0465E8CF45FBE3BAAAC28D7F3F053,
	bz_MoveNext_m0F6BA152CC254365602EC698EA5FA54479AE6663,
	bz_uh_m88301D9E2CE573F55566BED251BA4E4EA7EFBCFC,
	bz_ui_m06E6C61965420B79610A25FF221E7CB4B7598447,
	bz_uj_mB79BB94B078D3B65D86A003D69D97EAEE14BE5E1,
	ca__ctor_m4CFC9C8B890604267A0F420A0943D8A009E9A83B,
	ca_uk_m9664649736BF613E0CD9C8442BDD2306B3A1ECF6,
	ca_MoveNext_m4776DA9D5C488FE04B5CF51E8CF21E2DA0C90D5C,
	ca_ul_m08DE9F724D53860E7699F9B35050DBC2B2E3A069,
	ca_um_m8D447ECBF1936521D5A8BDDAF02D378ABDC7C36C,
	ca_un_m212E184A3190811BAA3F2A2625DF2BBDFE01A2EC,
	cb__ctor_m29CAA25131E216109D2F3766D55B62D35E6683BC,
	cb_uo_mF9DD4F609B6E6482B25B2EF7AB86D5FED0A33B4C,
	cb_MoveNext_m2D9ED935744A82575280D9368CFC20F1E85B7485,
	cb_up_m2B75F060CC72AAC2C2D80051580AE3546847209A,
	cb_uq_mCFD88E553E715E7B9E833D2139455BA0508008F8,
	cb_ur_m58CF9D59F0B8542C5DBB66F87C1ADB90C3A3E307,
	DestroyAfter_OnEnable_mC5D9A259CC5555D80F61C72B5A62E6AD1E4C3CFE,
	DestroyAfter_us_m0BB83E1F5D4C9CBC63D12BBE48371439754CBB60,
	DestroyAfter__ctor_mBD07788D70039FA205C6A3BD56470F986C5DCC02,
	DisableAfter_OnEnable_mA392679601029CE731A90C2AEC85E783DA3ED5D4,
	DisableAfter_ux_m4BFBF38D95219356AF9D57007832C67EE606A1DE,
	DisableAfter_uy_mAAADDA972F76EE5B81F9FA3FF0EA3D0C1CC2542C,
	DisableAfter__ctor_m89718CD70AE045BB8FDCD3E1D1BF162A1D28BA5C,
	cc__ctor_m024616AADD716A21F8EAE9660E60B81BFB3BBAFB,
	cc_ut_m668D4C17694FC7A19C12508ED3A8291D675AB6D3,
	cc_MoveNext_m4A530A52B20C65C7885FD9C65C92F705B1385CEE,
	cc_uu_m936E995C1BEE233C0365ED136686D850B88BA4AD,
	cc_uv_m76F4C2F46995610E54EE41556B029E3C7009C860,
	cc_uw_mEE77A89D863E968165C65EECD512E4F8C62AE7C2,
	Extensions_Clear_mC2C03B0DC8FEBF2549CB798CCFACDF14C2CCCFA4,
	Extensions_SetActive_m9601B7134F09262A00D364BFD3F0BEA2313FDFC3,
	Extensions_IsCollidingWithPlayer_m4E66148220B510DBFA7B4C465DA366B26CE44CAA,
	Extensions_CompareTag_m8066D30BAF46C213161115499FFE735E8F908591,
	Extensions_CompareTag_m8BD0E526513A1A248A6D3BB5B969F1E945B4887A,
	Extensions_Reset_m78C0D911D9C1CBFB300E4E0E6C1C5A8E8AF0522D,
	Extensions_With_mD86BB64814E7D7193E74965DE97A4977FA367366,
	Extensions_With_mE957B67B975ACF3C2A51432B17CAABA2B4C3693D,
	Extensions_IsVisibleFrom_m8ACD272CE7215E96F85ADA744C7525F86F4120B0,
	AnimatedButton_Awake_m4E2F723B4237823C0A60D357A5DF4942C66B73A9,
	AnimatedButton_Start_mCE1CB9B4FD668038F2C72AE76B713B50F3296363,
	AnimatedButton_OnPointerDown_m3D456C20D75506D88BBBBD06F0916F84E4C1EF56,
	AnimatedButton_OnPointerEnter_m6266A26411B03170EAFCB95FA1BD270D9752C1F8,
	AnimatedButton_OnPointerExit_m83E61A5D507EF08468B356257ECACC2F44146BA8,
	AnimatedButton_OnPointerUp_m46BBB03A94FCC162D538FB9EAF9BB04E0929BB7E,
	AnimatedButton_uz_m45FBA4A93BC49110C02435D09000232B54A7829F,
	AnimatedButton_va_m58FFC1BDAFF4278F7FFD19BBA8FA6858E7B952B1,
	AnimatedButton_vb_mCBD0EB7E406664D3061E066AE79B13F37CC6613F,
	AnimatedButton_vc_m2447352D9FE22BBF77C7D51F8601EDBDCEE4652F,
	AnimatedButton_SetInteractable_mE3A7E9459260C9CCFEF1F1D1EAEBC4B5024A28BF,
	AnimatedButton__ctor_m4412A85585ABF3C66EA6001A3F36702F797709A6,
	AnimatedButton_vd_m747611B7F6FE10F6F794BE50E97FB79D2658E648,
	AnimatedButton_ve_m07FC85300A619AAB6ED6D3CD309606DC8A6EE998,
	AnimatedButton_vf_m0C6B53CE37011B570C340D488F2CF111A3298D71,
	AnimatedButton_vg_mA57EC9E6A844BF92838F8294A920E0168EBD727F,
	BombsCount_Awake_m27B63C209DF4F27B5BE424C7E9D738F27BE81BD9,
	BombsCount_OnEnable_m53991A03D4381D8723B059BBB9E7C63DA9EE8FBD,
	BombsCount_OnDisable_m884B640889C4D6E20846A7948013826574FBE466,
	BombsCount_vh_mF7B0ECA67F56AF5075E077165D3E95AC95088B4E,
	BombsCount__ctor_m682C153D14F6F0F782B21E704C92C00A293BAEC0,
	CoinsText_Awake_m59B0A0B5C51DAFB73D0F4F3444D01A0CCFDD47A5,
	CoinsText_Start_mB2FFC5FAE8E2E91BCAAF5E93ADF185F3BE6E7109,
	CoinsText_OnEnable_mBA0DE21006D49627FCE117BCB47714F5F717DB3B,
	CoinsText_OnDisable_mA9E51994C62CC29B1B36E6213A90590CDC482ADD,
	CoinsText_vi_m4ED3282D49C738236435650A630435461FE23FD0,
	CoinsText__ctor_m43F84CF6FE84D974A8EA9AD8C357DC000C5BE120,
	Counter_OnEnable_mFFE5EFB40149AE9FFEACE8F1A287C690C72F230E,
	Counter_OnDisable_m095FEE4B2C9B38020A25D0EED84DA97001A3E463,
	Counter_Start_mC6BD83AE11EC13FC97896993522C7667B38F0371,
	Counter_vj_m0DE4631646399BAC24A0EFF8E85F269A9F04B24E,
	Counter_vk_m23F3B17E109CF2B154BB0FD1F2720C4979CE9D65,
	Counter_StartCounter_m598F53A6D0A01363AB6AA33D5442A4F131CEE05F,
	Counter_vl_mD51B8EB80F299B87E4F41DEF4C08F36C4A51212C,
	Counter_vm_mA3B394DB684261AB1CE694B0DF5C329F3F8A14FD,
	Counter_PauseCounter_mC194FCED35718D484A8A3D03CE8ADFF6775F7195,
	Counter_ResumeCounter_m0D4D2F93072AF08D845C4FBC466D376AC08DB055,
	Counter__ctor_m637C9D5FA3D4A621C198FDD4521970B6C1A7D5D1,
	Dialog_get_Shown_mEDEF53FC6AAC28CB734D4D95F6A3E8DE6522761D,
	Dialog_set_Shown_m01177B5FE5943361B77CEC5EC188A7F94ECC20D9,
	Dialog_vr_mCE03DAFB7F7A5DCD942BBB6C12E753D650DA10BB,
	Dialog_ShowDialog_m55C6B44EFDE4FCF65FF90DD877882122071357E5,
	Dialog_HideDialog_m0293351CCC7CC96F15D4D11843093FCE956210A3,
	Dialog__ctor_m0398413233CDCC5536BDB4F0815926F40CAE5C7C,
	cd__ctor_mD26C03CB5E963FCE78372140411435F1767C8731,
	cd_vn_mBA9FB1B3B1EBCDAB5FA7FA0291CC20DFD219305B,
	cd_MoveNext_mA57287C3BAB0201DB9A89492E05C9A0D53EE1BC4,
	cd_vo_m10B1BCA7BCDCFCAC1D3BAB92F7338D49EFC03259,
	cd_vp_m07E2D604C1FBEA1E4E71C629F768638E704E6585,
	cd_vq_m4EB25EC6AD7A7601504B521A2386BE5185990F1F,
	FuelProgressBar_Awake_mCDCA69EC2A4E2B70E9C135DEE6D1C707C296095A,
	FuelProgressBar_OnEnable_mEE68B8DCBEE02F0A74566826EFC4CA8F54E5B2C8,
	FuelProgressBar_OnDisable_m29D89427491DAF2D6080618BEC33DA2CC4654336,
	FuelProgressBar_vs_m7707394AB5D1166D3571A3C860DAB78A43485481,
	FuelProgressBar__ctor_m57D47DB5006495320CF1EC42445DADD98C9BD0E0,
	MagnetModeIndicator_Awake_m15690B59489D71BB25EE85CF5E2CF843DC5571DF,
	MagnetModeIndicator_OnEnable_m9AA8F54644B630CB145C02DCDA68DBC0DEB218BE,
	MagnetModeIndicator_OnDisable_mA33022C3D4A15657742E83102E5AD27A9D54F832,
	MagnetModeIndicator_OnDestroy_mACA4CA4D71BCE20B792EF9923CAA1197ADAEDA57,
	MagnetModeIndicator_vx_mF457A7EDA66E05A561E0237C23B29061FC0F2660,
	MagnetModeIndicator_StartCounter_mBC48ED292C92E08CB7E177DEEFE39C1FE702EE93,
	MagnetModeIndicator__ctor_mF142EA273A5E5AFED9E4BAC4E9CE474FDC98FFAF,
	ce__ctor_m9FC2934B19F17BE74A62E5E4AB15F01B57C74EC3,
	ce_vt_mBB08FB0C0E4B3645D291E81A262F4B14C969D247,
	ce_MoveNext_mB664FD4971B0A7958AB3F82224AD59E9B974A37A,
	ce_vu_m57C8C9D6BE0CC50785FC086EDAC7F263C3DBD4D8,
	ce_vv_m0807F912CF8BF83BC39AE4BABD3C307740D80F46,
	ce_vw_m03BF6CA94543FCB0C75C5D3E40ACBF09A24DED51,
	CarScriptableObject__ctor_m2D7A4BFCE9458FB3262F090B7FBBAB735FF2268C,
	MapScriptableObject__ctor_m6FF5F943ABB16150F0C36D14DDF3309BF49F96E8,
	BaseBehaviour_Start_m58F33C1B7E33C71B952C33271C6AAEF3C116F043,
	NULL,
	BaseBehaviour__ctor_m2E07FE9AB983681359A621176E50EB9D19A68C01,
	BaseCollectable_OnEnable_m4DD2D2CA56F4426F143A692245A746F1BCE79212,
	BaseCollectable_OnTriggerEnter_m67289A06F6ECCA8B27B6A592FDA0F47AB54763D4,
	BaseCollectable_BaseUpdate_m748D6AFF2B781313817DAB56CA1919D3EDA280D6,
	NULL,
	BaseCollectable__ctor_m91EBD7179E8C8ED28567B8CEC93F3F1A36B4B77D,
	Bomb_Awake_m1B2DCD9BE33AF1CAA7463FC19408B3442475175D,
	Bomb_OnEnable_m4329A1FACE67CB62D18282994416757C641F8A8E,
	Bomb_OnCollisionEnter_m0F2DF4E5B89B619DE1A58ED67AE3546D1CFB64A4,
	Bomb_EnableCollider_m283CB0D97FAFBDDECEE50D477DCD04DD809CA770,
	Bomb__ctor_m89AC6B18117F7DBB6BBC8C12817AF2BA44AD14CE,
	Car__ctor_m16823A9C0BF4746ACC70FDB806FD40824C8D6C45,
	Coin_Awake_m77EBB5A9FB1D6C4D908A9FBA850000170D1FC7DD,
	Coin_OnEnable_m661D39CD4820EA34BECFA8800D6446DC38F6BAAB,
	Coin_OnDisable_m3EB3F561B9E78D6E75C9C280E4FDBE7067F5A6AF,
	Coin_OnTriggerEnter_m82E8B6501943CB61F8EB69033CA1C6C64CC68BC8,
	Coin_OnTriggerExit_m038ECF32C2EE563873A9C117E756803D0D418201,
	Coin_vy_mA9021F00E37C9B61868343757E084AD934C29702,
	Coin_wd_mB291316468D1C1DACAEFD50D67DB076673065311,
	Coin_we_m5873A769E81F7603D2E8EA843D58EB9940B7E519,
	Coin_wf_mE0F81F6B2A1E6E6FCF94C589855F372F481EED98,
	Coin_SetCurrentTile_mCA4E990B24CF9B58EBFAC52EED12442258AB48F5,
	Coin__ctor_m0FD6A5D75C4E45DBC44718A490659C0E8B731FA7,
	cf__ctor_mB08806534BD183A9AA3385DB27D3164B255DBF99,
	cf_vz_m00EF2A063E94129A190C68E4C62FAED075711C55,
	cf_MoveNext_mDA7CC8754656522C830978A438E9891F0DF0098F,
	cf_wa_m51C6517A6D8E854834752B6BC2C9F8A7819B320D,
	cf_wb_m4266281F32149BB8A7276D8FE9C88EF3679B4468,
	cf_wc_mD4276501D5E84BB93FC0013465C3187657D1B9F7,
	Explosion_Explode_m76F1016C6CF835BC970FBC9AB7A76878D95E9748,
	Explosion__ctor_m91CDAE004330874986CA97E2F335972DE2070A68,
	FuelTank_vy_mF675A4DBAFB968AF1E95EC5A57C8DCBD4720EEFD,
	FuelTank__ctor_mA7A7542F6BA5F7E50751B8E1431AD9D18FA9C42D,
	Lights_Start_m3B135931A334CED0E3993AD2536ABF76A4DEC46F,
	Lights_OnEnable_m6553C96D0C7399381743887240D1F242AD97F11F,
	Lights_OnDisable_m1A8035DD59B37314BBC0ADEEA680A63F709FB2C9,
	Lights_wk_m0690FE42F68D7720D0F73C2CFF90BE6240447E91,
	Lights__ctor_m31922245534BEE816BBBA03F6676D5009FA61FBB,
	cg__ctor_m16BE6DF0B85C3B790DB35DFB99941B2D826C46B0,
	cg_wg_mF12FEC7B48419FA1B6ABDD8403C04EB90990CEA6,
	cg_MoveNext_m2BC44A24BD503AC830D4733F04A9CF2FA6AA9B2D,
	cg_wh_mE2C4648489DA768D9659F3E418AE6060119B0EB7,
	cg_wi_m2F643D523A1497E680A718EBB384658DBBCDACE7,
	cg_wj_m7FBB7F9DB300999EF76781FB9701E0F3358E926E,
	Magnet_vy_mE4474474D35018582F22741632633A6C793B52DC,
	Magnet__ctor_m8E85619251C8E68332E0731B5DFEE26CB1B64C89,
	PresentBox_OnEnable_m0DA65A907B0C96561EE8FB02652731FC8D37ED3E,
	PresentBox_vy_mD0115656FE76A328F5A17011D7E7F385F335855F,
	PresentBox_wp_mF80509C8774852EB36B45C0C5642E75CAE1834CF,
	PresentBox__ctor_m6EFD779175168FA53B2793B52294081694DC5D05,
	ch__ctor_m8D3B93F090EF2D330DF3BA98C3AC18A309900BF2,
	ch_wl_m0FBE1F8688683CDD0A732430C447C03E4348D561,
	ch_MoveNext_m4FD39D27911EEF159EEB2E7282DA836D675F4D19,
	ch_wm_mFACB9BACA62091B04BF0B9782E19A47A982C142C,
	ch_wn_mB831097C994A8C446579D4DED6DE0943D038EE08,
	ch_wo_m6E573F6BA773F50CA2B258B68469CB652248A80B,
	Tile_Start_mBBECA202289A4A9A189B092F6C16E8E9C7A2649C,
	Tile_wq_mACE815BCC27FEFED3E102AC3CF910E8DF834AF76,
	Tile_InitObjects_m1A80F5EB409A0F7EF1CDD8A5F1F0CB675B797EAD,
	Tile_SpawnAbilities_mC4CA1D6831C74FB452010EDBD4EB0F10A3C663F3,
	Tile_BaseUpdate_mCB80C33066F9CFE4224FA4DAC93A13B3B3F0C5EB,
	Tile_SetCoinCollected_m2B8D5D2BC7853D189F4BA17BEC0F4516EB8EC420,
	Tile__ctor_m92CD11B528C8BF0FFF4F687714906899F8ECAFE6,
	CarsManager_get_Instance_m5D3D0C0A97D0BAFFDCD48ED8FAFB97E477E00412,
	CarsManager_set_Instance_m1C1E3A0F46D275B8BA5FAC3EF38B5AFEAE605AD6,
	CarsManager_add_OnCarSelected_m3B3FD817E26C2981D8F8907B5552F2FEE9561CBA,
	CarsManager_remove_OnCarSelected_mFEA21F5151524D8924EEB2730DCEBA339BF9C6BA,
	CarsManager_Awake_m3CD7607CCF71CC67010CAAF38559B236A26C51CB,
	CarsManager_OnEnable_m63E18DB5F4E97896A5E34A52AB3A5B5AA6EA95A6,
	CarsManager_OnDisable_m0F92B50D3C6F281EA5EC8DE8AC770BA18BD1FB74,
	CarsManager_wr_m60C251A25A8799FACABC89CE7B81418262FE074E,
	CarsManager_ws_m831682076D93A87C2B2A99325C4644C0AE5E2C97,
	CarsManager_GetSelectedCar_mFE43EB202FF61C1D1A3779C7BC826579A8B19589,
	CarsManager_ChangeCar_m7005A51D27C31B18AD8A950B2FEBF6EAB77B4CB5,
	CarsManager_ResetSelectedCarIndex_m557C00D1B8E7660F21AF58A5B4407E2117890CAF,
	CarsManager_UnlockACar_m650799BD5A1A1691208F072D059FA53C0C3CD641,
	CarsManager_SelectCar_m4A8BC77841EBC1FE35439CC8B31911D10E0D53C3,
	CarsManager__ctor_m9011AFBC8BD942DEB9A1D71D1F081A1653958B6A,
	CurrencyManager_add_OnCoinsUpdated_mF2E6096BD39B6FE89C7F40156063136574911868,
	CurrencyManager_remove_OnCoinsUpdated_mD700E7B4E8430C46875514E4BD8B97C474ECD35B,
	CurrencyManager_get_Instance_mC44C06A7308844AC95AF8286E6C1151C5DD905D6,
	CurrencyManager_set_Instance_m1D9BFC189D7ED5BFB3FB46BDA271F743D1050B13,
	CurrencyManager_get_Coins_m917ABEF1A0F458B90E51984AAB9692A0CE0E32F9,
	CurrencyManager_set_Coins_mE37697882527E42D2E13D064E9471985E512ACAA,
	CurrencyManager_Awake_mA97F2186C8AEBEF96626820400F8D4E44F45B997,
	CurrencyManager_wt_mDF83066492570826459F6D2BF03E03D8BACC8DE5,
	CurrencyManager_AddCoins_mE8C76802F7387E3557F2BB53C24C3F903FA9A2EA,
	CurrencyManager_SubtractCoins_m73ADFF4BC8CC8390F5395E550467E42748B8825C,
	CurrencyManager__ctor_m0FDE645A55D2D9D1732971990077444358E8FD34,
	GameManager_add_OnGameStarted_mAD71F2355824C3FA12D31015F0613AB021D5C7ED,
	GameManager_remove_OnGameStarted_m5539562F31EA600409DFB86FB9E2AEA921292544,
	GameManager_add_OnGameOver_mB9E6494EF2694816A08705711CA2E93B423E54AF,
	GameManager_remove_OnGameOver_m5EB19B4AB4825E9F39F595B7F4A7E15BF6C0F34E,
	GameManager_add_OnMagnetModeChanged_m247895196DE2679DB45406F3ECF2D1E79DF50D6B,
	GameManager_remove_OnMagnetModeChanged_m9EF9E83C6CC84F0FD0EE6E743A8FD6931D2EB7F5,
	GameManager_get_CollectedCoins_mD84143859726DA875A266E557840142E3036A559,
	GameManager_set_CollectedCoins_m4AFC02635B9BE12DAC0D2346B62024F62D7BE418,
	GameManager_get_IsMagnetModeEnabled_m683B828F1D5B2C08FAD4405E5F0D6AA787FFFE1F,
	GameManager_set_IsMagnetModeEnabled_mD11E45FAB3A94567FE26E68670B37A5F675D015E,
	GameManager_Awake_mACFB2BABD5AC56855BC97D16BF18A0A8CB14F8AC,
	GameManager_OnEnable_m73A52AF6AE29245BC87E9FE58C30A6DB595FD731,
	GameManager_OnDisable_m87357F569CDEE0E5C4FA0417922750CEA1C058AB,
	GameManager_Start_m086F6FAF5ACC48DCDB69CDEE99F1C6D9B0C5E61C,
	GameManager_xk_m7A50F9936C3021E9052F644833E9297B3C43FDE7,
	GameManager_xl_m3595789886350F8EF192EAB28585E511B3C3C03D,
	GameManager_xm_m26959CE5B93B8A68A58E5AE829C0086F6F6E6A09,
	GameManager_xn_mBF2BDA29793DB1BE51A3C1ADB1DD6DAC6CEE7127,
	GameManager_xo_mD91183D559155083EC70AF67724011635CDBF062,
	GameManager_xp_m541EF81B56D312F710297CDAD38101DDAEE51F82,
	GameManager_DisableMagnetMode_m4B92F04EFC87EE1E2A1B5A5F7F8B41B7B6B98FA7,
	GameManager_StartGame_m5DB7F4DAE4964889FF17B0D86013B0EA2A56B5F8,
	GameManager_RestartGame_mBCE3D2414197ABD9DD209755A4899AC43E3F5379,
	GameManager_IsPaused_m80F8255A7CDE24490152969AB30FE9E2E5C38977,
	GameManager_IsGameStarted_m9C6F91DEDB157210F8455518875F7ED88C094074,
	GameManager_EnableMagnetMode_mDA610F8D650D8C7EDA6AC509A768FDE29E7BE827,
	GameManager_GetPlayerTransform_mA3E13ED99C54BB926A56E4B9A495F7DEFA882A0D,
	GameManager_GetTravelledDistance_m8A529D82754574206A7BC5DE32A56BAAB71335F0,
	GameManager_IsGameOver_m46DF36724A7D7F180A391AA32DF0D82768492B48,
	GameManager__ctor_m5E44982A4E710D519DD287F7BF9831B82B95676B,
	GameManager_xq_mC19DD29042B997B022B2F38FCFBCC6D9851CCAD3,
	GameManager_xr_mD4423E62AED934395B82ACE6F605E6812C8319CA,
	GameManager_xs_m0521F184DE5F422DF19731498CB172E99229BDDE,
	GameManager_xt_m8B9EBA770E41F32C8076E6F1259A08AB45930F6A,
	cj__ctor_m74458FC83C8E1F6F2691146B74588EA7E345357C,
	cj_wu_m17A67CEBA429E7986DC50038AC955365A9B23186,
	cj_MoveNext_m5817D7AEB365A13BB199BDB5CDE2217AFA4401FC,
	cj_wv_mC444465954941F75D1297AB1B585525B63C5DF02,
	cj_ww_mF17EF8A6B38706E2C8A4233F03FE0BD2354EDB93,
	cj_wx_mB28E149BB97310B14F159D3163EA95D6F85F30BE,
	ck__ctor_m54EADC92BBD810EFC2FABB3270A8DE99200A8685,
	ck_wy_mDF736A356D65E4CD85E255F87C7327661706B6F2,
	ck_MoveNext_mA9AD2F8CA35A3CB130972BFBD8733A0C3EA41BBB,
	ck_wz_mBC4068905E434B9B77B61F9F53B8963A0C9E0EB3,
	ck_xa_mB671D325ECC7977AA18CFAC2AD06CABD3A8C42FF,
	ck_xb_mCDB6EECC76FECEE52E59DCD98D6FB0733016DD80,
	cl__ctor_m1FA6B1C62D86DEDE841062FC63C3496F776CA14C,
	cl_xc_m97417C09FD8FA93A341DB9E9255897609762AB61,
	cl_MoveNext_m9B7DFF94A2C980CBCA4C973908C85700A6C0B2EC,
	cl_xd_mEDC945C874322F898762B063AF727A97A822CA43,
	cl_xe_m342503C37890ED2B58B9F40844F5FADAB52C0B89,
	cl_xf_mF303503F26FA0BDD0D5C2855C00C4FB6E3C9DD11,
	cm__ctor_m5CA0F6D6B7B05AEB7E08BE9AF27E18B53C0A904F,
	cm_xg_m74600D7069B6D73455A1D26F17DA3F4F0B1E1D6B,
	cm_MoveNext_m6D1EBFE6F42EC1E1087AF866762987DA8581184A,
	cm_xh_m35BC84247D0559105A4825DF40320B8250BA9863,
	cm_xi_m235FB603086A5E2D6A4CD5C903CD89680069EE09,
	cm_xj_m8AFCFBCC811EA19C574680D940B01A5977C2289B,
	MapManager_get_Instance_m746F960C371CFB4E2CE0DAC5AB0303A5A0400B5B,
	MapManager_set_Instance_m5273563B2A10AB105AF65C8FB3056D4CEB7AE6C7,
	MapManager_add_OnMapSelected_m57ED02104C0455905B62E7E2343FAB4B4FC883E7,
	MapManager_remove_OnMapSelected_mD6DB68589D9A25DB525434A67414EA3DC3065970,
	MapManager_get_SelectedMapIndex_m3879571B947B9EF38BEF1DFAD8D9925F8FA46B9C,
	MapManager_set_SelectedMapIndex_m18648D51103F027E11A211AADB3E55171064B359,
	MapManager_Awake_m923541E958A8F328720E0E0CC961C073FF31C88D,
	MapManager_GetSelectedMap_mF4FA0B834EE12AF815AAC46274FAB9CC5834956E,
	MapManager_ChangeMap_m15A7E626E67CE199FB5B05EEE9A1EAB4A0AB2690,
	MapManager_SelectMap_mA46F9494CB593E05608E57BEAC7A4571B9873D38,
	MapManager_ResetSelectedMapIndex_m24CE2EF6C08EC4129687A12388CC7D0F3FE7A104,
	MapManager_LoadSelectedMap_m5FE25ABEC634C88A814B6E5B9D632B4B4720AF97,
	MapManager__ctor_mF2BFDFEB7BCA407659ABBB92DF6EC9EBD647A06B,
	PoolManager_get_Instance_m5F5096461D745CFC79FD9C08F4BD2B771700A5F1,
	PoolManager_set_Instance_m2011EF385CAF7610F1034F32F349F7398856D96E,
	PoolManager_add_OnMapItemsInitialized_mB34BA1E4FBCDB49A696705270162BC5CE407A2CB,
	PoolManager_remove_OnMapItemsInitialized_m64F68D13B6567D370CCBF2C34BD0709741549C9E,
	PoolManager_Awake_m24733113D4D7FF04EAAA10930AA70A286D1C111A,
	PoolManager_OnEnable_m9EDA54BEB5CE9898CC4A3F7FB0D71E43DC6D0928,
	PoolManager_OnDisable_m7D309E0BEA7ADE9D5C4D07D721BCB8A685714E01,
	PoolManager_xv_m51E8990D0717EFF7163C1D84C59072A7B41EE9DA,
	PoolManager_xw_m234D0D90CF5D34D7337F2F518408121F4D53CA23,
	PoolManager_xx_mD6C11B552491008BDB7CB91F359CC8B2B1E7AF2A,
	PoolManager_xy_mE090DA71C672E3197CBD8D77632B2545AB9CEC5B,
	PoolManager_PlayExplosion_m8BF2A355C96B8039DCC7A99847619FB8EBE2950D,
	PoolManager_GetPooledObject_m49587865ADF1E54D9A4C4BC20F643D023B7BEBD9,
	PoolManager__ctor_m0A73471A1C90485A73CAAE7E9763E5624899634B,
	PoolItem__ctor_mD886AE06D514FE1A4281966C5B6AB64D9559BC56,
	U3CU3Ec__cctor_m26A34F4E0266475F0BE42E309A27E22DE02E34D5,
	U3CU3Ec__ctor_mD07E5B2FD9E609C964C17F72BD1F41FCB7FD00A1,
	U3CU3Ec_xu_m4A65E588D3B7CA28DE464EB378307C05AC2E0229,
	SnackBarManager_get_Instance_mAB4E6E0E62E6099A5C153FCF665FE3E161A17D34,
	SnackBarManager_set_Instance_m6C5D9358FF963AF7BB6A9DFFBED6E01D6CD764BD,
	SnackBarManager_Awake_mF14DBAE67BCCDC1A20373A36A367F8A3515E110E,
	SnackBarManager_OnDestroy_m25A8C2BA7B64D29EB6D6CC6625FE515B026A510E,
	SnackBarManager_yh_m52B9368D16FBC39173A894C70F5AB78874D02C8F,
	SnackBarManager_yi_mA699E87E7A397F1340CE4DAAB372FFFDEFF41391,
	SnackBarManager_ShowSnackbar_mD7BC1C9DCCF7491F936C25D3F639D76E400F6F41,
	SnackBarManager__ctor_m9DF65A910D27555DB9EEBE839401710FEED3083F,
	cn__ctor_m573C69A773201B4D83CC82E21D32C3EA5D854E96,
	cn_xz_m9F56513DEA3DF30CF2B69A8C9C5AF312D8525B51,
	cn_MoveNext_mDBA7A3884CB5556A54C2E34C6286617A6B1CD910,
	cn_ya_m174DD0F62CD65CFE5B7AE0B779B643D3B56E5783,
	cn_yb_m67C4191C6E6AC05DAF44BEE63D6CF843DEE5FF78,
	cn_yc_mEEFB471EDF8EA5A2D93C77C7722BFC795EFF36C3,
	co__ctor_m4DF2F1A4FCD8B8CAEB451A6BD4366784F8C3BB89,
	co_yd_m1CC98CBE550F5169E5E6EB7BBA62E6A7C12DC477,
	co_MoveNext_m2D001FF637E63C57F805A8950D4D0D4AF152D317,
	co_ye_mEC59A14524992D4E8E74DD92271CD68C758CCA5D,
	co_yf_m48CCF5A5FF5134D8CC18B4C6B0B3511759EAF787,
	co_yg_mA86FDC74C8952C2BD60BAC534F533EC6E2C315C9,
	SoundManager_get_Instance_mB51CBBAB8D905910FD56499F2752BE40C9BEB4AB,
	SoundManager_set_Instance_mB2FA7C9A8A3DE91318D6D8DFD576C2893B2CEB49,
	SoundManager_Awake_mD2AAE32AA89EA38E274EC60BD73F9979CC1489E9,
	SoundManager_yj_m5EE775BD1C64495853821A821AD23EF49FDDFDD7,
	SoundManager_GetMusicState_m8F0A4C9BC39C006B6C81BF60ADBE01CC61D83F65,
	SoundManager_UpdateMusicState_m754476DF44CE9FA749028F32B4C454EECF45DBFD,
	SoundManager_PlayBackgroundChaseSound_m5AA533C50EF468F147D107172701B4592BE5D95B,
	SoundManager_StopBackgroundChaseSound_mC559E98CFF876FDEE69504928A330A7F67BAFC2D,
	SoundManager_PlayCollectCoin_m0A6DB36AD3DF28DFD6942CEA4AE92690EC462467,
	SoundManager_PlayExplosionSound_mBC2667FA6A5DE3238C275B0471AC7769222AC00D,
	SoundManager_PlayButtonClickSound_m953A36DEB0A178A1C82C1E039E6BB826A8A9C8AD,
	SoundManager__ctor_m5706466DF19DE3266DB8F6125E37E8BB013DF2C4,
	TileSpawnManager_get_Instance_m35E23F0FFE8CC068A3DD95CB064B94914E5C2BDC,
	TileSpawnManager_set_Instance_m20F8FE15C46E1D47DC203B0D442B04DBB9BCD71B,
	TileSpawnManager_add_OnTilesCreated_m2E4535A2A79D5BEDE94788D609AAA6885D78EC21,
	TileSpawnManager_remove_OnTilesCreated_m5FA9B722952EC73C8510D0C5397CFC521B6C922C,
	TileSpawnManager_yl_mDEC843DF206C548C44ED0630F34F1F052140C5DB,
	TileSpawnManager_ym_mB2D16FEAE7450D6A2A0DDC0C5E61EB18195B7553,
	TileSpawnManager_Awake_mC018813FBD2A705E9942D960FD572B5BE5CDFFA4,
	TileSpawnManager_yn_m74A5E8EBEF3B91E33D9A4026A60C4A8CAF8FCC52,
	TileSpawnManager_yo_mE5B00E7C3105A5269C0BDD2EBC3F026432D93FF5,
	TileSpawnManager_InitMap_mB2BF4395B7E61EB630FC34FCC6FD3CB850749FED,
	TileSpawnManager_InitTileObjects_mDF27F30AAE26F686564BD5F0577FE46747DCF19B,
	TileSpawnManager_CheckTilesAroundCurrentOne_m59B1C6E49E67EFAEE4DCAA1935E287F7EF28D2E1,
	TileSpawnManager_GetActiveTile_m40738D353E55437F6E499F4BA67BCA0CA92ADA37,
	TileSpawnManager_HideTileAtIndex_m215F27B3C85F561CFBC0B50DC0B551331FC002FB,
	TileSpawnManager__ctor_mD71304BDB7B350C9520EE0CB748D82ECD13D454B,
	U3CU3Ec__cctor_m1321E63F62B0C654E3F645545DBB11FA525DFC85,
	U3CU3Ec__ctor_mF9F8B982D25F96C90D6ABB4791CEF0D0BA975632,
	U3CU3Ec_yk_m327AF7E8970442DF53E5E1CB6BD8EE5EFD737786,
	UiManager_OnEnable_m4307CD617D23176E221D75D760C33C589D8DE042,
	UiManager_OnDisable_m3A5302126124A5E983ABEAD14707BF37BF55D901,
	UiManager_Start_m0CC6C31B7E8D642CB0EC4A8E376254E3BF63F38C,
	UiManager_yp_mCF2F3D780DA6C14D1839A00925346BE0987A80AA,
	UiManager_yq_mC2A9EF119E6735F61A497DC29C67727BE32C37EE,
	UiManager_yr_m46845878EA9A33EF78989F446A73C2EF479958D8,
	UiManager_ys_mA1F06E9FEE6CA510DDE13DAFA8943B4492D612D2,
	UiManager_StartGame_m4908AC31ED6C6CBE90D337100BD549FDF93F85D5,
	UiManager_RestartGame_m84DA67551F5761E6EF6E61638F475A32EDAC3CFE,
	UiManager_GoToInitScene_m5FECC7F18BC5E309E13862EBAAC191A5DCF75660,
	UiManager__ctor_mD1F1C8C9A22F491D2D4FCF8A35281A88842E1E13,
	UpdateManager_get_Instance_mF677A7E574917A2C1D77D8CAD868ABB7D2BECBE2,
	UpdateManager_set_Instance_mA3654A58EF47906724581626AD99467790F5F790,
	UpdateManager_Awake_m8F94D3D6D28EBDE1F348A8AEDFB6EEF069EFD079,
	UpdateManager_Update_mE31245ACCFED139E61F54AEBC625926011A8F291,
	UpdateManager_OnDestroy_m8C0357837A0207EB75F25B3179984D6B94160521,
	UpdateManager_Add_m690C087371717CF4A057F3BE56F35DED4859B580,
	UpdateManager__ctor_m8CFEC2C0D5BBECA92B913059CBB4277AF1EA6F7F,
	CarController_add_OnFuelConsumed_m885A96DB6904E114C04CFDA1F51C484A48289A3B,
	CarController_remove_OnFuelConsumed_m5A08D91EB43745CE6A30E2EBE4B7BBC517700A0D,
	CarController_add_OnBombsCountUpdated_mCB5FA081586D46D93AB29697FD09533A14DDA69C,
	CarController_remove_OnBombsCountUpdated_m9ED67AEA521FD6FFA8B99CA26579608D789BD487,
	CarController_add_OnPlayerDied_m6393154C6F116D54E5BA4BD0C055343E7FE86671,
	CarController_remove_OnPlayerDied_m1F39B259AECDB17D904142491D9A8C77E119486D,
	CarController_get_Speed_mE4BAC885A1D3C6CA88EA22615A7ABEC8A099BE29,
	CarController_Awake_m1393A9B328D714F4F4F5AE8367E9E422D1E1541A,
	CarController_OnEnable_m804623733A55494E0A98C8D4A1E91085331C71DB,
	CarController_OnDisable_m52B34C66FE824FC864813FC5BF8B35FA0921C95B,
	CarController_Start_m3F4F08725FBD2BFC8381D492C7BC6BD485E7286B,
	CarController_BaseUpdate_m12B828AC3D1088F16B6912FF2136C66CF5131D54,
	CarController_FixedUpdate_mE352E6855869538D867DD56B21EAE3C202F2BA3F,
	CarController_OnCollisionEnter_mC907E6591F99D8E2910EC89E2EC9E831286B016F,
	CarController_yx_m8F4D4BB878C1D12D9BBCB7888A5E1B8BC7A1E023,
	CarController_yy_mC578AB1B23351291FA8BFCB2749F8E72D41D08FC,
	CarController_yz_mF21A6441539EAB4DA03831DB745D16F495C2CBC7,
	CarController_za_mA19F86918065BA1B5F761976C4D8CE47600B526F,
	CarController_zb_mA37947BCB02CD0A19CACF3504FE9A9D0125FA0F8,
	CarController_zc_m38FE16AA6FF5E0FE9584B9CCDA10AC98008E93D0,
	CarController_Move_m980B2CDD1D387898F893F93194849045E8D44143,
	CarController_Action_m0164C9FBC668AEFECEAE87AAB8B872885FC01580,
	CarController_Boost_m42226244B953791EE52564CE7BA92F6FB67A8BFE,
	CarController_AddBombs_m6B1F9993A7B54746C4712F302D53CED35013F247,
	CarController_AddFuel_m68BA1B4A272D232014D150A37EE278F835CC26A9,
	CarController_GetTravelledDistance_mBBF1B4CD1A28516D405137DE417E1E9F50079D16,
	CarController__ctor_m228A076830FFF5A8C1C8E2885FBD9345799EE7F5,
	cp__ctor_m946EC04D2196063E33D82923EAF9148E9C228820,
	cp_yt_mB1566E7002A0F9ED287F49E3E4ACF6B5D0132F69,
	cp_MoveNext_m54FD1CA200DAE943D6A14F23CEB3136A110C2A07,
	cp_yu_mE0E4AFD03467A6C99A996FE5C48E2766FC47FE96,
	cp_yv_m749C3C3E1E198302D9262134331A56903B0CC3DB,
	cp_yw_m7880EA5F510675A1D18E94C4BC5888301C116E5B,
	CopAIController_add_OnCopDied_m1E76C9D2861AE18CDC19FE0DF72CBD299CDFF23A,
	CopAIController_remove_OnCopDied_m75F1A23CEAE16C40B402B05B1CE85B63562023E9,
	CopAIController_OnEnable_m438317627FB0A480ED1298AEA26D4ED04F098A6F,
	CopAIController_BaseUpdate_mC8002FD51FDD923DC0077D9D9AF23EFF60585290,
	CopAIController_FixedUpdate_m24DC85EC3955CBCF2D25FC0BF4C963355593E12E,
	CopAIController_OnCollisionEnter_mA36162F243D5561374124CDE7A8E05D442F49532,
	CopAIController_DisableCop_m8E407F7D4C6E8ED35915940FE302E6A6ED43056F,
	CopAIController_zd_m29DA8284301F320D42A0892A8E543E6209BAFC8B,
	CopAIController__ctor_mB393BE36AE297E2111C58777B19157950E08E2EC,
	JSONNode_Add_mA73D290ED9AFFE3F42861A87D20DD066CBF02BA3,
	JSONNode_get_Item_mD7BBCD16DB5D0500676559AFB2A2594B0103D8F3,
	JSONNode_set_Item_mD4F5BEDADBDB66141EC0EB6F3E8E0C04A481CD6B,
	JSONNode_get_Item_m85A1C5F8B26938A5BDD5CBA14C84E4A37B3D4ACF,
	JSONNode_set_Item_mD9F8F18C2B5F9054A722B4CFD8129E2C48A4BE5D,
	JSONNode_get_Value_m422474020628AD90572986DC9999B5E33D51044F,
	JSONNode_set_Value_mF7DD7A968B9CE3DE2DEC99CFBB8F6A0035DA0BBC,
	JSONNode_get_Count_m6A898D357D50A5C2E8042AFCB3D0E795B3F0FB25,
	JSONNode_Add_m8EE67AF49568BB6CC3DD0968C5E7E206EF3F4A99,
	JSONNode_Remove_mCC44CF5159F5A8661D1906F9444477EF5C990B71,
	JSONNode_Remove_m404F68BE1FB04E8F4B50EADF3490CAC177BB57FA,
	JSONNode_Remove_m68BB8C3076B8DE3EB84D43AC8FEFA4057F2973A1,
	JSONNode_get_Childs_m041DDDFC000548A689CE29D3288D53AA643C3482,
	JSONNode_get_DeepChilds_m44F560506E902A0B0DABB6B82562D127259FEFC5,
	JSONNode_ToString_mCE094AA189C11D45C5915FA3D229C9FD1076A6E1,
	JSONNode_ToString_m729A82FD7DD7412637589CDE0158F98B5E7C14C2,
	JSONNode_get_AsInt_m06D351315628BE80806748B43605699EBD2724C7,
	JSONNode_set_AsInt_m351063CC07FA09FFBF85FB543D40ADC1F90B751A,
	JSONNode_get_AsFloat_m18888F5DF88D1ABD784270641319B7ABDDDBE9E6,
	JSONNode_set_AsFloat_mD796198844BF326B7288864EB6C4D5AD4E510E9C,
	JSONNode_get_AsDouble_m3CBB3A48ADA115310C2187857B1C71ABC7806752,
	JSONNode_set_AsDouble_mA5DA00CF265864C946AA28CCBAC8A7FF4D22BA76,
	JSONNode_get_AsBool_m8D64DD4B3FC875095E283DBE5AC867003123635F,
	JSONNode_set_AsBool_mB969B70E7AF09CCF88B79BCF1FBFC03D9F8DB6AD,
	JSONNode_get_AsArray_m7B937E045E18295E048B40D1B7C3A58E01F710F3,
	JSONNode_get_AsObject_m09F640638EA27AE9A75A66D413209E1B0A4C1700,
	JSONNode_op_Implicit_m9105117784C6916F6B646D36A1370FB58A9C6762,
	JSONNode_op_Implicit_m8DDFBC3FCEA2F2087B7A132A483F724B1529B183,
	JSONNode_op_Equality_mF53AB65ABCF70E4C7D035DF059648FED12577634,
	JSONNode_op_Inequality_m31F67DC83671EE7A334E8A1A0445AC08CFDD9BD5,
	JSONNode_Equals_m024E691480E99CC5FCAC978BEA06E3C89AC936BE,
	JSONNode_GetHashCode_m205B8B106B815AF3CFC5FFAC4A4E253452BC08E5,
	JSONNode_zs_m12F77455895F24BBA9071F9946E381E85E01BB10,
	JSONNode_Parse_mD6D2A7EBFFEDF36552C27F27CB53F9DBA6C9D12E,
	JSONNode_Serialize_m7B9E652622D50367BB8DB62252A0ADDBD2A02A4F,
	JSONNode_SaveToStream_mAAF83974E2DB60FD527965E28263B1202E195C27,
	JSONNode_SaveToCompressedStream_mDFE8495C0252DA781F53FB62A4E580D9267594B1,
	JSONNode_SaveToCompressedFile_m5EE5A35989E44A29862AC7BA21F5C78179BCBD71,
	JSONNode_SaveToCompressedBase64_mC5D7414CD46FEBB67BB8567FDA778D453B87FA7B,
	JSONNode_Deserialize_m5C0519240162A7247E99053C69876E590587B38C,
	JSONNode_LoadFromCompressedFile_mEB55B68104BA51A066485FBBC1CB6A1D16DAA504,
	JSONNode_LoadFromCompressedStream_m7368A19FFACEDA51ED6E39DE7919FDF11836F030,
	JSONNode_LoadFromCompressedBase64_mE4C615EAEC9DD685E6659BC69C77CDF7A6FE1CFB,
	JSONNode_LoadFromStream_mD78A68A7F71EE78FB1E12311DBFFFAA3E6AF8B69,
	JSONNode_LoadFromBase64_m33C55652F013772F68C8CA8B54CEE7EBE297B08C,
	JSONNode__ctor_mCC776C44E5B26CB440CD077D35CF116A45EAE5EA,
	cq__ctor_m4044EAC23C652482EBBEB617F327E0FD2101017D,
	cq_ze_mA27ED6EBE1AE5B216F8EB3816FC2CB90F5DDBA7C,
	cq_MoveNext_m1882B101044E36CA0447BA869438F2FDA5CA68A3,
	cq_zf_mBA3957DE8CDD716F30ABF9A0BCE59C34131389CC,
	cq_zg_m51D111E08778B75ADC111542B9B7B5A73F5BE8F3,
	cq_zh_m9D39FF5B15496A972DFD8A0A828C8F5C3504E033,
	cq_zi_m4293FDE103E4214A8D0A5443523395E38F626C92,
	cq_zj_mAABB424EC28B01BFAD1680F387540F6573EB048C,
	cr__ctor_mA576954E592AB5CF94A5FBC324EAC3AFF8D751FB,
	cr_zk_mD3B92350D22B14145A9893F3713A61B1510175F8,
	cr_MoveNext_m22F21D368C71A1D524F14596988DDC2F01A5EF05,
	cr_zl_mF82C4ACD5EA6A4F07DA23269277293BC2CD3990F,
	cr_zm_m1C40AB63B5297DA0E216BBA5C3B3C710A04D280C,
	cr_zn_m688C0017487141EBF471594E7BD672DB25428255,
	cr_zo_m8898F6737EC87FA678B162993D2D026224E068B9,
	cr_zp_mDA099CB536AC248624C10901DCF03E8047C6F6F0,
	cr_zq_m3556EA3768B3A9414314130BAB820A6B31E6DB10,
	cr_zr_m8F21627A170FD142F78A9826B71AB91583641D24,
	JSONArray_get_Item_m755E3E8E6725A4FBF408E5BD1A8432C419410503,
	JSONArray_set_Item_mC4EF1F71394FFDFAE7153C0AC8E698EE4AA5B9F5,
	JSONArray_get_Item_m70C52946E9D9B5DE99DBE66423561B152ECCB454,
	JSONArray_set_Item_m2AB7F7C0919BABC722D5262F62960F58A0BCCFDE,
	JSONArray_get_Count_m810A578C961B778B760E51A3D0111CFB8152864B,
	JSONArray_Add_m11ED297B0CE157395B41622C3A70577A4CCDEE1B,
	JSONArray_Remove_m294856B07209D367010855B3B71CEACD50B8F6AF,
	JSONArray_Remove_m6C5A0F04019B3D0DC675038B5B04AE38079B786A,
	JSONArray_get_Childs_mC749B8F046AD011544966486F60CA9C0C217A874,
	JSONArray_GetEnumerator_m4E4EA5BBF194BA5E068CA738E573C73DD1ED5635,
	JSONArray_ToString_m58F4AFD74189F06A5380F48EB94330B1F62787AB,
	JSONArray_ToString_m97E2C58394C45CEB1C54965B96C6E9CB19B8005B,
	JSONArray_Serialize_m85EA6F504087F2B91019ADACE0AF84BE56C977DF,
	JSONArray__ctor_m6ECA2300A22DEFC3387A72AF03FEC3355B150C4E,
	cs__ctor_mD7515745585E31CAB8AA98CBA539BF0D587F667D,
	cs_zt_m44770F3B3255682687E6507451F166B54301A8B7,
	cs_MoveNext_mEC847BFD683E4B6EC7E0B4C454BB031BDCA4AA0A,
	cs_zu_mE834DE4E0EB7D2992A9E005C0FE7E9D75E067C1F,
	cs_zv_m546C6B8CC1A6FA9422BCF0412BB7E48355952A3F,
	cs_zw_m677C8BEEDDE68F61C7A8E6959295E5FEA4966750,
	cs_zx_m7284A5CB9B08A487AEE5E38C979715DCEB39C18C,
	cs_zy_mD2C3CEC74ED5ED243639021664D3C922FB5A842F,
	cs_zz_m265F6467EB8B11393055C3342B8A78494C7F7950,
	ct__ctor_mDA1E8DFE4E336588B13B3D387378CB746FD70A53,
	ct_baa_m797FCC0F026085CECC48930A6CCE4D3FB354F07E,
	ct_MoveNext_m701B24977BAB7680C2C9CB5AC01C87BD3785FE1B,
	ct_bab_mD02935AAE79B0F307D99ADC8251A084FD7A5E88B,
	ct_bac_m1397738AFAD2164B0DDB0C77E0EA31137CCCA6F8,
	ct_bad_mF193D25CF936A8DC3F709F444762A3D7899CE990,
	ct_bae_m573870A631776F76FC63B777CAEF5726DDBBAA11,
	JSONClass_get_Item_m49CF33B11E4FCA72EBC0D1E89BFA1F64D1C82D93,
	JSONClass_set_Item_m887D13A167F244972827C10AD304E445D273B72E,
	JSONClass_get_Item_m332F023B545862E9766531549B9A5D8276645551,
	JSONClass_set_Item_m8E3A9CA1DEDAFDA4165B2D5CEE425BC01B829B71,
	JSONClass_get_Count_mD26CAC564152CABFB3D996FE189D7184F6C9FB3C,
	JSONClass_Add_m8016B5271985A574BAC8E7745DE5F950299FFF38,
	JSONClass_Remove_mC59E957355660E1DE1D528916D23F86B3EB5F680,
	JSONClass_Remove_m3A246109A77359EAAF01B6324AD2F436608DA680,
	JSONClass_Remove_mDE17444DFA5339F564FBB7071E7913D9C1B0C877,
	JSONClass_get_Childs_m7ED5CDF26A34F041E5CBF93B9EC616E940AEC925,
	JSONClass_GetEnumerator_mC63BF81FA10F977320F60BFF46CDA748A7F09DAD,
	JSONClass_ToString_m2851DE91AA6840330E1A2A64F5803D702AD3BF37,
	JSONClass_ToString_m82935C8A4E9EB9D8E5693077AB9603452F71B027,
	JSONClass_Serialize_mBA8EAE5D8ADB2413DE2A726447B0037B5686BFF8,
	JSONClass__ctor_m02C51CEB21719D911A19C729D02745553A1EB446,
	cu__ctor_m7D6B9EDF0DB9592830BA9C5CBFC521BCE83DB8AC,
	cu_baf_m7B5B02F8EA750BAE1421D707C6620974BC74ED99,
	cv__ctor_mC3C1BB385D6A5DE18E76450B1666916C9130B5BE,
	cv_bag_m87720C53BD286866D9328CBD7BCC6E76E30A39D3,
	cv_MoveNext_m76AE02FFC3A2ACFC0CA3D9DA5829529D0656F9CF,
	cv_bah_mEC8D55BA31540AE8CFD0114A4F88EE79B8AA8AC1,
	cv_bai_mAFCB312A5EFA4F98D15EFA99F8094BBDCD1F0144,
	cv_baj_mFB41E64CDEF68685D327AF202B5AA5C1E5755ECB,
	cv_bak_m110DD3FA7A664D16ADD0A82B8C7146C4D414FC5E,
	cv_bal_m35E55EC8830427D373E5D370B03E82AAC3D25CDE,
	cv_bam_mFD157059A06FC444A8959B843B4D1928A5B2A5CC,
	cw__ctor_m83E88076AE7AF0D4F42CE5BAAAC48659B5D0A6F4,
	cw_ban_m4715EF672276CB85DD88E918478261824A78DA17,
	cw_MoveNext_m39848F7CE7CB5DC9CF577899DA649892CBFA1F00,
	cw_bao_m906A91B10CEB60EDB3C05433A0DAEB58521FEE23,
	cw_bap_mBAF1DBD60DF2E2ACF518E0124F6500F2F64D9C9F,
	cw_baq_mB94D7F9D5AB74C284A7424E5EC09279DE5F1AF6D,
	cw_bar_m9839CCDAA7A75B82C884BDAF6FD2EA95D0E494C1,
	JSONData_get_Value_m3ADA5A705F607FB7C35D4605EA832E89599BE425,
	JSONData_set_Value_m217DDEC42EA58FCFA6CBC6EFE473BD448A16DBE2,
	JSONData__ctor_mF07078A36644CD1C44FD4394482FFF67BCCEEAC5,
	JSONData__ctor_m0BA2DCE6697B2DF087570A042E0BD1D48B1B7AD2,
	JSONData__ctor_m2EDE89D6B666DDFB6A4D2399A8462D82EC448AEE,
	JSONData__ctor_m0AB47342A4FDA49AF5B0775BE663DCE5A697B350,
	JSONData__ctor_m312352D90C0634BDBABD84416086C56157235C3F,
	JSONData_ToString_m4C8593075AFF9E56B99908CEEA6988EB2AE61580,
	JSONData_ToString_m67FDD9B98DF0DD19D397B6E210603DF3194840FD,
	JSONData_Serialize_m509455814FED79C97E0C05F354839F51488D9D91,
	cx__ctor_m9BD0C97D3C0C696535EF8966A0DCA823F1704B8D,
	cx__ctor_mE068864EA917C5A327317818E676C0E514416414,
	cx_bas_m5F8A64329A9C11CD352D007E26BF3B7F01AF011C,
	cx_get_Item_m203CFDFCE7924FDD3CF95AD900A8B080229E0E18,
	cx_set_Item_m3BB206EF1D8A84818737B3BCADF49F5B7AC02AA2,
	cx_get_Item_m7CB753895D45B23D846E0C7E9DDFAA8BB0B81619,
	cx_set_Item_mE7C09FDAFB47C8B55F805C7691A392F05988BD68,
	cx_Add_m09AF39B907ED532773F1EFC5AE4B56D3AE0A4011,
	cx_Add_m632A7EA17AD7E47A2B20D13977C94F4E47BE59BE,
	cx_op_Equality_mDA5942C6916726F6F51A88EEB3BCFA05691241DD,
	cx_op_Inequality_mB8DC05A734397941501FD283C409D8094BCCEC6B,
	cx_Equals_m53C3E5BA4844BCCB0324EECD527C599D37FD0240,
	cx_GetHashCode_m1FEBF53AA382CA8E42BFE9D8D22210227596A02E,
	cx_ToString_m230DE8B515EC7FCCB37E290B6B9E62C00E685601,
	cx_ToString_m153F285B63E900796FC7D574369F1E37D5C211C2,
	cx_get_AsInt_m9B88090A1763D0EA7F3FE801809E82C9795E24DC,
	cx_set_AsInt_m5425D36540774C637B7B729BCEC0C15C9810C028,
	cx_get_AsFloat_m8D79B0B6C23D6DDCF58D7C9AD2F02D14312ACCFF,
	cx_set_AsFloat_m8D0B74B6FB390D6056183DBF30D7ADC39971876E,
	cx_get_AsDouble_m888A32BDCC84EEDAAE47DFF2EC2684D4EDF1B110,
	cx_set_AsDouble_m24CECF617343501DB1103E42C9D6EA1A479C7C04,
	cx_get_AsBool_mBF5BEB63AC409E24323DCCAEBCF4DADA0F247AE0,
	cx_set_AsBool_m505B8BC2AE764F8D45F4439485C96C55CB2DCFE6,
	cx_get_AsArray_mEE34DDDC408E0AD02D66F0A0C534D6003DBAB48A,
	cx_get_AsObject_m7C55ED22AC5ED4133ECB475D338EE60037E7A51B,
	JSON_Parse_m64D44E2E2DCDC73C4FC5A08B5D13F92209F2482E,
	AdjustiOS__AdjustLaunchApp_m48B7F15D15001A96F611AF64CAE22CE898367F0C,
	AdjustiOS__AdjustTrackEvent_mA076136EF8FB10FC204C569BEDCDD0030BC75EFF,
	AdjustiOS__AdjustSetEnabled_m706DA3D25C337389171010CA5C9C1780466B8E28,
	AdjustiOS__AdjustIsEnabled_m6546882679AE7B220BE17EA489E51D70C12FF7C8,
	AdjustiOS__AdjustSetOfflineMode_m3BB67F740D3A2266C0B76C310ED0B441E9B6888A,
	AdjustiOS__AdjustSetDeviceToken_m1AE42A3D7B1774D44B9624D84E130BA8114C6038,
	AdjustiOS__AdjustAppWillOpenUrl_mF7D7BF178886A07122CF342AA889EAB70F77F47E,
	AdjustiOS__AdjustGetIdfa_mD12F0F0C29EA4CCAFF80DE5BA48608C31702A63C,
	AdjustiOS__AdjustGetAdid_m8647E5B2818A9E1CA464609A84C2BBA86DE721F0,
	AdjustiOS__AdjustGetSdkVersion_m40F9E7EF215CDF9D06BEF5B06D646BCE42A5442F,
	AdjustiOS__AdjustGdprForgetMe_m137841B602947A115C668A382491D340401CC504,
	AdjustiOS__AdjustDisableThirdPartySharing_m2C4EE65750742ED9E13E6106F33D67A86864CC13,
	AdjustiOS__AdjustGetAttribution_m60CAD18B259E45D37F5507601E1E62769D392E6D,
	AdjustiOS__AdjustSendFirstPackages_m7A8D2B989AA0E74CE4412F1F56977B9F546FBA16,
	AdjustiOS__AdjustAddSessionPartnerParameter_m49AF272411EFFF3E2EDB7479D89BE858D1C6BA5F,
	AdjustiOS__AdjustAddSessionCallbackParameter_m25B46D6EEAD09B7F24D50CE736C486159B6EB7DC,
	AdjustiOS__AdjustRemoveSessionPartnerParameter_m8F69CA9E7048C0F72C516DE519F39978C496F5C6,
	AdjustiOS__AdjustRemoveSessionCallbackParameter_mFDDC161139248B53E9BE4E1E9C0D65834B1B99A6,
	AdjustiOS__AdjustResetSessionPartnerParameters_m1C40932340A054A4DDD734653591F810DB34BF26,
	AdjustiOS__AdjustResetSessionCallbackParameters_m1872CBAFD960F0325E569456106EBC5DCB10C23E,
	AdjustiOS__AdjustTrackAdRevenue_m9876079F57C48B10782219549DC6196C631B2B0C,
	AdjustiOS__AdjustTrackAdRevenueNew_m474166EB4172174A11A87399BC9B354FC3E80DB1,
	AdjustiOS__AdjustTrackAppStoreSubscription_m8E52F62DE40B61892DE25621515EAADE24BEB45B,
	AdjustiOS__AdjustTrackThirdPartySharing_m7AE5A4E423F25FA2940B02926E56DFEFCA3AD544,
	AdjustiOS__AdjustTrackMeasurementConsent_mD7716533FDD4FC8A6DA98F8E12CA1F87D4A9AC37,
	AdjustiOS__AdjustSetTestOptions_m46134BDA07D02C0C98C58B9D8AE9E8F153C8E10F,
	AdjustiOS__AdjustRequestTrackingAuthorizationWithCompletionHandler_m337778AEC74ACC7EF9675B1E4CCC68B53B125AA5,
	AdjustiOS__AdjustUpdateConversionValue_m92D7E58F26D4F431025B2987FD3AF2E7B7C8AAA3,
	AdjustiOS__AdjustUpdateConversionValueWithCallback_mDC7B4FAC0E48D800C52D5C43866D1FEAACCF4A0D,
	AdjustiOS__AdjustUpdateConversionValueWithCallbackSkad4_m05CAD07C12C399EA18F45403421466C82C953422,
	AdjustiOS__AdjustCheckForNewAttStatus_m0D3815DD047C72CB1970334B83A1C1B859A9810D,
	AdjustiOS__AdjustGetAppTrackingAuthorizationStatus_mC6724413894E2E28F21D584E29175A89B2199673,
	AdjustiOS__AdjustTrackSubsessionStart_m24D23D6D287CB825CB4547CD73E27439DCE1FBA6,
	AdjustiOS__AdjustTrackSubsessionEnd_mF1B9FCF570312D80CD5E6F78F51FB602B35E8561,
	AdjustiOS__AdjustGetLastDeeplink_mE482EFF8C64642E746FEFCA539C306EF2A3B2DBE,
	AdjustiOS__AdjustVerifyAppStorePurchase_m67CE68ED39D744F0445E25628635C901B9DFFD09,
	AdjustiOS__ctor_m2004768A8ED7B143D79B19053ADFDB7F779F846B,
	AdjustiOS_Start_mDC27C5DAAE652685F628EDC7668F67134484793F,
	AdjustiOS_TrackEvent_m9A44E273F8844ADD9186D9266655B257EA0B608C,
	AdjustiOS_SetEnabled_m52758F3082AF7EBB207DECCA5111E3058443A27A,
	AdjustiOS_IsEnabled_m2F34A7E95874105C1785D08A1271E06DB3BD800C,
	AdjustiOS_SetOfflineMode_m682E9FF10715A461A311D0E1738C59B6B523D6C9,
	AdjustiOS_SendFirstPackages_m013286B86A0037ED04FE641A8E707A2E2C17C219,
	AdjustiOS_AppWillOpenUrl_mC9E8DFA392B4C6062E9B8E3D0CA35567D86243D5,
	AdjustiOS_AddSessionPartnerParameter_m095E9BC036146C18D12F91C72ABF1E97A4C655AA,
	AdjustiOS_AddSessionCallbackParameter_mCF436C86975D262B880040D958EF92BA94D0BF16,
	AdjustiOS_RemoveSessionPartnerParameter_m5772B4D6BCCB899D88243A08B51D84BC0D695AC3,
	AdjustiOS_RemoveSessionCallbackParameter_m521A79D726DB870ECEE835A3057497D14E82E042,
	AdjustiOS_ResetSessionPartnerParameters_mE9DC21EF018307277A35AE8FAF94D07C933C4170,
	AdjustiOS_ResetSessionCallbackParameters_m5F8CABA15F440EDAEDB9F4172BE8DF8EE52EF16C,
	AdjustiOS_TrackAdRevenue_m81E71CFC6CD90555EA9DA436A4059405ADACD07C,
	AdjustiOS_TrackAdRevenue_mAE68D4D7959CBC41EB171AA4C1F8D5607978CF35,
	AdjustiOS_TrackAppStoreSubscription_m2D3A411D472D7690014DE47B0CCABD3AAF0898DA,
	AdjustiOS_TrackThirdPartySharing_m3DCDD5683B6320AD01C259F79491EB2BF87832E3,
	AdjustiOS_TrackMeasurementConsent_m5C7390162C7F37B704B7230B80779F3EF53D5A20,
	AdjustiOS_RequestTrackingAuthorizationWithCompletionHandler_m95594475A57F52D6E55A438D965B3EEC6E878279,
	AdjustiOS_UpdateConversionValue_m9B549F0ED21C26F73419A529082CD35EB054BC4B,
	AdjustiOS_UpdateConversionValue_m4DA9BC581EA2D9E6F2F9AB5C576A33916B28F543,
	AdjustiOS_UpdateConversionValue_mA9166832F19A58739C6F43980D3C1D1048AA03E2,
	AdjustiOS_CheckForNewAttStatus_m6ED68A3566C06CC5BAE83D9DE65909F9ABD52F3A,
	AdjustiOS_GetAppTrackingAuthorizationStatus_m4360763287FF0595EABEF07C0F81F1E50A189434,
	AdjustiOS_SetDeviceToken_mED18B01FEBCA5CC835B823C676ABB3A3FD3D150C,
	AdjustiOS_GetIdfa_m90B5A77AF3A65336435F770A2D5E1260322653E1,
	AdjustiOS_GetAdid_mF436DCAA79328E5C6B8E7B024E7E7392FBB3B451,
	AdjustiOS_GetSdkVersion_mAE7A8E2C96A8B5EB3F37442E81965A62D50D721F,
	AdjustiOS_GdprForgetMe_m72283B010575C38702DC11B6E7C7AB49266A7D83,
	AdjustiOS_DisableThirdPartySharing_m87D57BD5B6ABBEEF045CD2ADFB06A1CC974FD27F,
	AdjustiOS_GetAttribution_m567CBC2E722BFDEA812159738FEF97CA1822CE6E,
	AdjustiOS_GetLastDeeplink_m59E6BDE8479809D82440F075167F7FD7415561BE,
	AdjustiOS_VerifyAppStorePurchase_m4CF366F97B6516A802682FC807B00EA3C6A6332F,
	AdjustiOS_SetTestOptions_m8E6BE3E6E960B581D19C47D13CA8C49BBC3557BC,
	AdjustiOS_TrackSubsessionStart_m8C073ADE4C06B7037A1557ACD9257D77582AADCF,
	AdjustiOS_TrackSubsessionEnd_mBEEC538934EE716E4380FDBD582CBBF4D5C4321A,
	Adjust_Awake_m1B0E9298029BFF09C771F77DF8181CADFCB63BA8,
	Adjust_OnApplicationPause_m3ADC342D8050B80840CB85B003EBCF689C8E4012,
	Adjust_start_mF24352A04B12F9A3D5314851E1F476DD4BCBF0E3,
	Adjust_trackEvent_m788CBA9B9C606FE179B1582368C6A8A171425E36,
	Adjust_setEnabled_mEF3633C6A6BBC4F91FCDB4F934A281DDE96712F1,
	Adjust_isEnabled_m254951D8A14448BE51FD7527AC80CC3950E6EE4B,
	Adjust_setOfflineMode_mDA3666A20F780FFDD8BCC07F9DB4D3215823C360,
	Adjust_setDeviceToken_mC1407160399AC998FF11E556CBFA3B38950B67A8,
	Adjust_gdprForgetMe_m7A0CFD6A9B4137418F35E5AA201B6410737359C0,
	Adjust_disableThirdPartySharing_m302224B75CA744974396F66A08AA1FE587873BE5,
	Adjust_appWillOpenUrl_m1E13932CE37598AA3C42C4509D2323491569A6F7,
	Adjust_sendFirstPackages_mCFE8665FB15B08EC04CB5BCF14C2923E5189883F,
	Adjust_addSessionPartnerParameter_m4AC3D2786FFF94A176E93DEBF049FCC6BB71B3E6,
	Adjust_addSessionCallbackParameter_mCCA2594D1EAADD151C56F4537823EBBFE3EA645F,
	Adjust_removeSessionPartnerParameter_mA50E1FF8D276CA300DFBC2D8C52E1C60194BD98C,
	Adjust_removeSessionCallbackParameter_m9D480CA9958869ACA6205BCA9CE84D235B4E45B1,
	Adjust_resetSessionPartnerParameters_m788D347452CE9472C4B4BFCB581272C33A903459,
	Adjust_resetSessionCallbackParameters_m092D40CDF59B8BE4FB165BB105F914E67E61D7B4,
	Adjust_trackAdRevenue_mE7017F85963C48E260AE4A390B6E03C1365CEC8F,
	Adjust_trackAdRevenue_m7F1739F079028F6AFA4F42629B803A152F0BB9AE,
	Adjust_trackAppStoreSubscription_m3BE586B120A00A43CD00D754363EE0548488EB5B,
	Adjust_trackPlayStoreSubscription_mB28584B6B95978DD627EF386DE383E258A55C394,
	Adjust_trackThirdPartySharing_mDBB4F949AA7D3532188DB67890C38CF006E5C3AE,
	Adjust_trackMeasurementConsent_m28091C4476B42F91B98E2864860109C8B2FF7F3C,
	Adjust_requestTrackingAuthorizationWithCompletionHandler_m0F4258A1D04183560AE8A0DB926BD1DD063330D2,
	Adjust_updateConversionValue_mB83B123737964C3CDD68E5B94588575C973EEADF,
	Adjust_updateConversionValue_m22B9DD112274E04D0CE5267196B8FC850FF32BF0,
	Adjust_updateConversionValue_m7619FC95F34018A0C8B115B114417776FB016C2A,
	Adjust_checkForNewAttStatus_mE8CA001423E1EFFB7D610102747D76F5B2DE02AF,
	Adjust_getAppTrackingAuthorizationStatus_m2CD6E53C3C56055BEF58BCA99C30417FE4231006,
	Adjust_getAdid_m2285DFA62339E5ED400D271E8E661FD0174600A6,
	Adjust_getAttribution_m3B7BBB4900BDCC0B92D6A103FE178D9E808F2188,
	Adjust_getWinAdid_m528B95DD3250ED9D5BCAB2B58C3B8CD7F6B940F6,
	Adjust_getIdfa_mB068686DAF448330241C2367A2767891EADA60CB,
	Adjust_getSdkVersion_m8F7D96C6A76363F65E34D362C4912D3D5DA34E7D,
	Adjust_setReferrer_m223FB53F552416333FC5B55D1E6F3D5DD5F9270B,
	Adjust_getGoogleAdId_m9F83C2BEA8B17987BC6BD492C1593444D427CB06,
	Adjust_getAmazonAdId_mCF6657242C0F74B3D50BD6C412C91311CFE96688,
	Adjust_getLastDeeplink_mF3BC9E789AEB1C8FB8C54BEDF98F3D04049D2D66,
	Adjust_verifyAppStorePurchase_mE3B63AAEDB2569792D5167200AEFE589E69AE2FC,
	Adjust_verifyPlayStorePurchase_m769B3D59149BABD106127A3D5E9C84F67CFA3865,
	Adjust_GetNativeAttribution_mC07ACAADB8E276755784A0A592779820032C4C18,
	Adjust_GetNativeEventSuccess_m0E93EE7CEAA6FFE60BDE52C2ABE236C93278CD69,
	Adjust_GetNativeEventFailure_m73DD7A70DBE04F90F8B5B8E83B35ABC8EF02B42F,
	Adjust_GetNativeSessionSuccess_mF82661EBAB1FCB7EAD4E14096B48F6F74941F09F,
	Adjust_GetNativeSessionFailure_mDEB4E95D140D4168954E922418B64E1C84F60D90,
	Adjust_GetNativeDeferredDeeplink_m3881E728E772FE695964BB594E54E7FE2BF9F6F0,
	Adjust_GetNativeConversionValueUpdated_m137A58ED56529A7A6EE2D9F4D9DB8BC2E902B581,
	Adjust_GetNativeSkad4ConversionValueUpdated_m071B1B5ADFCA375028CF566698A2F4F6156607C5,
	Adjust_GetNativeSkadCompletionDelegate_mF7D29D0B2A1AA7D38B1C4923DFC34072920B3FDC,
	Adjust_GetNativeSkad4CompletionDelegate_mABA4E95DF2115E03AEBF4754E8C67EA61116C57E,
	Adjust_GetAuthorizationStatus_mEB2B4F74FBAAD83B387FACE5EC1F6B0DEB344883,
	Adjust_GetNativeVerificationInfo_m7D6A32E6A3F4A393312B5BDDE97814D96D2C976D,
	Adjust_bat_m7BCB001B58A932B97FDB4BC1BB8F83309E0714AC,
	Adjust_SetTestOptions_m5313B20E29B0029F24CF33ECE44DC008CDD6F360,
	Adjust__ctor_m3303F268C76843435B868BB9D9E307FBD20A8F0B,
	AdjustAdRevenue__ctor_m4C94C4313766148F6D9DC7451483B6EC847EEFB8,
	AdjustAdRevenue_setRevenue_mB37B06AC7FE6C0D6FF8BF3DEDD7C5E2A58E3E3A7,
	AdjustAdRevenue_setAdImpressionsCount_m3181A0D66506FA4D9971B18CC8E3DDB921EB1115,
	AdjustAdRevenue_setAdRevenueNetwork_m500036ED01D100B35A12B3DD99AA9E754EA72B25,
	AdjustAdRevenue_setAdRevenueUnit_mF46B42441260BED2E68D98661A8D90D4F202C856,
	AdjustAdRevenue_setAdRevenuePlacement_m1C1843A4ED920DDBAD223DFCD78131655804CC0B,
	AdjustAdRevenue_addCallbackParameter_m2B3A25714F44C6FBA06C09FB6ABD9F703EC9335C,
	AdjustAdRevenue_addPartnerParameter_mAF18DE2CE37C15D2179C77ADF244D1AB260D32D3,
	AdjustAppStorePurchase__ctor_mC1D24DD9ABED88BF92A169ABE33573DAB8FFF404,
	AdjustAppStoreSubscription__ctor_m0D3482433734BA539F0A09252DE3659D21FD1536,
	AdjustAppStoreSubscription_setTransactionDate_mD1BA71DA15248006C26B22602D1BF4A83B0ACC0C,
	AdjustAppStoreSubscription_setSalesRegion_m0E1646795FA1466592F7E7A7D14B04EC02D6E39B,
	AdjustAppStoreSubscription_addCallbackParameter_mD67B08D11C9DCD410CB8966744F3962905E8AA70,
	AdjustAppStoreSubscription_addPartnerParameter_m6B639A50999BE4CC82D58CCCE7D1F50536D62019,
	AdjustAttribution_get_adid_m7FEE4DDFADFF7764690922FE17064A8475DCC159,
	AdjustAttribution_set_adid_m8FF9650D73A3B30569FA924D09F2A1B5841800F6,
	AdjustAttribution_get_network_m8430B735848CDEF80E9054A358E1147FBD19AEE3,
	AdjustAttribution_set_network_m68ED3E4E1E6850226D667FDE9829B402AF120D20,
	AdjustAttribution_get_adgroup_m15DAB5440B779D12C1BD8BCF9C47B20F14692416,
	AdjustAttribution_set_adgroup_m04EB13F0176574C01F8E233A15E6E7AB71CDEBFB,
	AdjustAttribution_get_campaign_mB839E1C4DD4EC624B6C46E9444F1A9D868EA0750,
	AdjustAttribution_set_campaign_m29AC5BBED526925450C7D081A5A656E9A71470E9,
	AdjustAttribution_get_creative_mC15C380B618E220C2143920CCB88EBAF8A864B36,
	AdjustAttribution_set_creative_mF0F350C3D8521BBC5D841A28428210CD9CF41183,
	AdjustAttribution_get_clickLabel_m45D150F891EF508E44F219A4CBE768A05BCA866D,
	AdjustAttribution_set_clickLabel_mAAFCDD0362AFE2EF2F6AEC66E6973B65B75692DE,
	AdjustAttribution_get_trackerName_mEA8576F240393B289A3C0CC66F9D7F2E965EEB52,
	AdjustAttribution_set_trackerName_m731697B9763F60A9FC502CC6A1A27BDBD2574876,
	AdjustAttribution_get_trackerToken_mB2CB9686A8CC7243A6C4391F4728F1BA8197F64A,
	AdjustAttribution_set_trackerToken_m6093F9C8CC27B2425BB1373F51EDFA26B9E2103F,
	AdjustAttribution_get_costType_m94B271C6C975D4C945D5912D7879C411BB2F25C6,
	AdjustAttribution_set_costType_m2B994A60E50367E752D803F431BE9B010BE784B0,
	AdjustAttribution_get_costAmount_m570856A2EFDAE1646AB3EBE61E9D11FC7A872182,
	AdjustAttribution_set_costAmount_m8C20F2BD1C52F1109660D5A965B5159BA4DC5647,
	AdjustAttribution_get_costCurrency_m746AD16AC39C41F680D4420B830529EAF595E999,
	AdjustAttribution_set_costCurrency_m4C83141F90E118ADEA5CCA620335B9FDD0C38D51,
	AdjustAttribution_get_fbInstallReferrer_m730BCBF4BD7687B6ABA49F85E1E3592944782A68,
	AdjustAttribution_set_fbInstallReferrer_m03CE43EE59FB3D653CB09AB9BD1DE86EE11D292D,
	AdjustAttribution__ctor_m36B38620BB1475A4ACE1EDB1CCA466AB2F754307,
	AdjustAttribution__ctor_m8274D1B29F0C4D4D99E2067269DBF55161E3B98A,
	AdjustAttribution__ctor_mE3820E52AF63417CE1FF2ADAAE8B1BFA701344C9,
	AdjustConfig__ctor_m718373AA152F4C6F3AB5E805B4630AB008A32395,
	AdjustConfig__ctor_m96C4907B142108F8818BEBC52EDC03D90B5C6EA7,
	AdjustConfig_setLogLevel_mDA93163BE7A5E536C670CCDC0CCF7C93B9B3E54F,
	AdjustConfig_setDefaultTracker_mA67C3195A19A5E9AA2B5AF9E071336CA9E1AB724,
	AdjustConfig_setExternalDeviceId_m5AA54126D0A69091B9573F3A530BD2AF8B450FDF,
	AdjustConfig_setLaunchDeferredDeeplink_m8D6806307929E8E3AE2F01CE3C08BF96DDCD526F,
	AdjustConfig_setSendInBackground_m039AABBAF2DB300CE62F8CBF78DA3A5E36604317,
	AdjustConfig_setEventBufferingEnabled_mBB81E8C7A41ABCA6326F518EE53905C327B1F982,
	AdjustConfig_setCoppaCompliantEnabled_m43149C9F256F85E6149011100CEC777326B818DF,
	AdjustConfig_setNeedsCost_m27ACE0EB3E57AECBD640B2A1B4510BCFBE8553DD,
	AdjustConfig_setDelayStart_m5E3583922F84F6E2B9988052D54ABECE6113B0B6,
	AdjustConfig_setUserAgent_mDD4FFFE5044037A2BC22003F631A9989361DFA1D,
	AdjustConfig_setIsDeviceKnown_mAD1C556F14A0DBAED60254F330EF9625F3AB6EDA,
	AdjustConfig_setUrlStrategy_m43C184E9915977FC7955F22A086111B7836E2263,
	AdjustConfig_setAppSecret_mCF9AAAE31F6A695F806709B8599E319706BE15DE,
	AdjustConfig_setDeferredDeeplinkDelegate_m0434CB6325F267D824956505E38F55C1BC69F750,
	AdjustConfig_getDeferredDeeplinkDelegate_m5E71CF0E1CD8ED86E14052643073B2B34A19E574,
	AdjustConfig_setAttributionChangedDelegate_m16311DC0B297069CC826AB0CEE81C747C47B7054,
	AdjustConfig_getAttributionChangedDelegate_m0B91F876BC47C733C887A0C674C69C7A2AAE859E,
	AdjustConfig_setEventSuccessDelegate_mC93D376662090A2A7D5341FCB0EB6F5D47034C00,
	AdjustConfig_getEventSuccessDelegate_m803B0AF83809209BDCA4FD72ADCD37A3D6525AAE,
	AdjustConfig_setEventFailureDelegate_mDF106AB503D7AE6A0EF9FC23C86FDB561C53D919,
	AdjustConfig_getEventFailureDelegate_m55B097E3E827DAA9D0A03C3827815990DEEFAA73,
	AdjustConfig_setSessionSuccessDelegate_m38873292BB0382A5A82272A971C2C8FB32EE97ED,
	AdjustConfig_getSessionSuccessDelegate_mDD3BD6C6F62AF59330E60B1570D2FC3D42DE20C1,
	AdjustConfig_setSessionFailureDelegate_m3BEF1CB7417F8E3E12E59E610DBE1FEA8584E2AC,
	AdjustConfig_getSessionFailureDelegate_mB847ACF06A571D19D85DD18BA596E78F646AED66,
	AdjustConfig_setAllowiAdInfoReading_mBCAE2AC7ED0E99E915648114A3424E985EFE469C,
	AdjustConfig_setAllowAdServicesInfoReading_m232716609D173872EF41FD5837A9D0133419C4C1,
	AdjustConfig_setAllowIdfaReading_m439C9CAB2FDE23F534F838B3BEAC30B917E483CA,
	AdjustConfig_deactivateSKAdNetworkHandling_m9E3A12F2125AE97AF898E7AC49DBCE9085D93B9E,
	AdjustConfig_setLinkMeEnabled_mC3B85AB4A602F3BB59B8B4B7FA973D9F2B8EB55E,
	AdjustConfig_setConversionValueUpdatedDelegate_m944853EAA8941CDC4ECEA27C4C9CAD01279639B4,
	AdjustConfig_getConversionValueUpdatedDelegate_mA8286519D143FC8FA6AA32373F2169099ABEEE23,
	AdjustConfig_setSkad4ConversionValueUpdatedDelegate_mBDC7D976A8BD22E4680DA70B9EC5EE7ECC4A45E4,
	AdjustConfig_getSkad4ConversionValueUpdatedDelegate_m791D25B57223A51EDE3A28E916F1A6AB43EC2FFF,
	AdjustConfig_setAttConsentWaitingInterval_m207E02DC9D926C771965BB2270A18A914B1B1DA3,
	AdjustConfig_setProcessName_mA05E8249BDBEECED54C503BAAE53011D4EF18E53,
	AdjustConfig_setReadMobileEquipmentIdentity_m60C524B45682B362D3A43D8EA2AAB5E324F3D16C,
	AdjustConfig_setPreinstallTrackingEnabled_m50FF6E90421C467AAB8D1668E426E2F2F5B15BDA,
	AdjustConfig_setPreinstallFilePath_mF70F4E2F50F2E73E7EAF1DEAB6351F6AB6EB728A,
	AdjustConfig_setPlayStoreKidsAppEnabled_m6786F76DFEE24836BA51A2FA1B798FB2AEA86484,
	AdjustConfig_setFinalAndroidAttributionEnabled_m6A5F408AE10EAB53570FA465B37920760A0E5F60,
	AdjustConfig_setLogDelegate_mCD5A0B2CC87D71A6618CB76ED218FFDB346D487C,
	AdjustEnvironmentExtension_ToLowercaseString_mAEDC5B0CBA386D07FB258ED1BDFC83CB4394D49B,
	AdjustEvent__ctor_mB6F2EEAE794AF0DBA97B384BD745A06235288C03,
	AdjustEvent_setRevenue_mA8B68C9A56C7A90FDD61D07D6E9B527EA4BAEB49,
	AdjustEvent_addCallbackParameter_m69836E8BCB0600E59592F2226886F7E3717267DC,
	AdjustEvent_addPartnerParameter_m5C8A9B71C8E3668F18D2A7107128C2AA7F60115B,
	AdjustEvent_setCallbackId_m77A43125761431059046C8BD038A4090A6F67A98,
	AdjustEvent_setTransactionId_mD82CAE578CF9FBBB0F73937723AE9679D33AA254,
	AdjustEvent_setProductId_mA5A287AD1813C5E813F12A097C4587AD23B2C8AA,
	AdjustEvent_setReceipt_m0DA7BC506BF585B0EFDD3E0FEDC51EECE0406BFD,
	AdjustEvent_setReceipt_m44238E44C88E2249BF57CB4775FF867EAEA76497,
	AdjustEvent_setPurchaseToken_m921AABC8958704EE95BB091E047E4DF47E4E4F3F,
	AdjustEventFailure_get_Adid_m63A229A1E387D51BA76FD857843A30909472F4E9,
	AdjustEventFailure_set_Adid_m1C9E862F9EE373D5F36B28D07F944581B4733FCC,
	AdjustEventFailure_get_Message_m39E32498366357A63414ACBF2D829D67E378435C,
	AdjustEventFailure_set_Message_m67C166B4D02AD43A8835555633ED6A41B6470472,
	AdjustEventFailure_get_Timestamp_m8AD7E740ED2BAD647DF69D3E9E20DA10AEA7894C,
	AdjustEventFailure_set_Timestamp_m144FA4FAB62F3AE2D92C8A729A4D80C78129FC8F,
	AdjustEventFailure_get_EventToken_m790B0C32B96810DB063845DB41C7EA5392511E0F,
	AdjustEventFailure_set_EventToken_m0107E2C7300ECD415209E1F64A6B8AD04F33798E,
	AdjustEventFailure_get_CallbackId_m7C6B49AB5A6AE7A9287E309C85E4DDC8B6E01F6F,
	AdjustEventFailure_set_CallbackId_mE4D4EE9B87B3B947F952C7BC539A177AA609B0FD,
	AdjustEventFailure_get_WillRetry_m437C69AED2629C0A51F93160CF269ECB51C48138,
	AdjustEventFailure_set_WillRetry_m4C79E145286998F97FFFC7106C792794C06669E9,
	AdjustEventFailure_get_JsonResponse_mB7A9E1270C3CA4F577552217E4FDB3CCFB32852A,
	AdjustEventFailure_set_JsonResponse_mC129C66E6BD3773556DD9984F8A9B41987A480EE,
	AdjustEventFailure__ctor_m528922562AC18ADE49AC59EFECDF9DDDF06D9827,
	AdjustEventFailure__ctor_mD35BD0B33754A00AF01D005F17CE529500281A14,
	AdjustEventFailure__ctor_mE44FDD70724F8F42E19DE705B7A0771C23BE0284,
	AdjustEventFailure_BuildJsonResponseFromString_mFC779A74C66E513EC19EF86F780AE363B25A828A,
	AdjustEventFailure_GetJsonResponse_m4A9D1FDB6FF13C9F955E00C64A4996F5826A31FD,
	AdjustEventSuccess_get_Adid_m9107BA449922578E0F9B8CB8B4541FE26A6C56C5,
	AdjustEventSuccess_set_Adid_mF832EF6F1DC6FE8156A132AD42AA1060E539A7AD,
	AdjustEventSuccess_get_Message_m5B29D1C7B3CF3C7CED972991740A888131931DE2,
	AdjustEventSuccess_set_Message_m38D9A47DB181615424C49B59C6E4A562B3E5F89F,
	AdjustEventSuccess_get_Timestamp_m193EB4EBA0B8DA8CF0863D1DF75FEF141B1D3B10,
	AdjustEventSuccess_set_Timestamp_m0CCE0BEF1E47ACA8E07187A73BBE9ACFEEC6586B,
	AdjustEventSuccess_get_EventToken_m5784EFFBAE4463DA0ECFF6A537731DC98E286A3E,
	AdjustEventSuccess_set_EventToken_mAF539927077C6E4B98FC29622DE5D26C3A5F2C64,
	AdjustEventSuccess_get_CallbackId_m3D7D77C8EF5837C5EAAB45998FD4C7A02C04D983,
	AdjustEventSuccess_set_CallbackId_mA49D8F4F34D8A1C9FB36A15EFB7572AC187A28C9,
	AdjustEventSuccess_get_JsonResponse_mC1ED1F8BC320A1BE406D403D15DB0EA699A01A75,
	AdjustEventSuccess_set_JsonResponse_mCA8F4E6DE391C1D4B8BCEEFB437BA5EE1E717D90,
	AdjustEventSuccess__ctor_m8E95350D1027E90E42E4A890D5D8F6C683C1388C,
	AdjustEventSuccess__ctor_m3AF21839E90ADA4ACF33D117311F354A788FFE1B,
	AdjustEventSuccess__ctor_m572E2ED470E4819DFF8462F86CD0A35EE856DE75,
	AdjustEventSuccess_BuildJsonResponseFromString_mB45093E3AE421B1E1C210318F2081EB7016C065C,
	AdjustEventSuccess_GetJsonResponse_mC8F1B778DCD86E0CFCE0A7F34D2AE30E440E465B,
	AdjustLogLevelExtension_ToLowercaseString_mEF9C47460E6774026C495F7646A4369476C53588,
	AdjustLogLevelExtension_ToUppercaseString_m457BEEAE7375DBA0C92F1180B69A432CE360A133,
	AdjustPlayStorePurchase__ctor_m7B9ABD9ED2899FDF05BDF64D3E52047C489915F2,
	AdjustPlayStoreSubscription__ctor_m8FAA1BDF8B8C354B18FB090ACB1EF65E0B381EA1,
	AdjustPlayStoreSubscription_setPurchaseTime_mFB33C90CFC1A515912E08927A41E27DEB80094F4,
	AdjustPlayStoreSubscription_addCallbackParameter_m384EC847C6931509BE14FF2275D6AB32F493F9A4,
	AdjustPlayStoreSubscription_addPartnerParameter_m864989B749FAE715C806A3772FC7B968FFD4A5F4,
	AdjustPurchaseVerificationInfo_get_code_m8FB046D3E105D4581BDA3A4C60BD5FE7FE0608BF,
	AdjustPurchaseVerificationInfo_set_code_mC3B02737B3A0F2A350DFF882C39A2829D2E7A8EC,
	AdjustPurchaseVerificationInfo_get_message_mF44C745A7801EFE4681CA685163736AAD65BD093,
	AdjustPurchaseVerificationInfo_set_message_m2CF4884928E67730C04532244236225CF728AEB6,
	AdjustPurchaseVerificationInfo_get_verificationStatus_mBD654E0CF645A3B31439DFD66DEEF7A18469FF97,
	AdjustPurchaseVerificationInfo_set_verificationStatus_mAD3AC1434552BFC186F6B8A51F2116F04CE9D692,
	AdjustPurchaseVerificationInfo__ctor_mFD144FB4E4ACF2B998F0FA447CD249C2850972F1,
	AdjustPurchaseVerificationInfo__ctor_m8C6412DCE35238BF47E8E4202598F74CE085668E,
	AdjustSessionFailure_get_Adid_m55CBA752E653E41BB100CA0666E984AC41A1C986,
	AdjustSessionFailure_set_Adid_m9D52E417E29F03D868D2A5C1BA50578FAE232BC7,
	AdjustSessionFailure_get_Message_m7FB5952110E6198593306F2D2206C87878241071,
	AdjustSessionFailure_set_Message_m84D2E372880BCEAB77F55A2D5E3228A2D0179835,
	AdjustSessionFailure_get_Timestamp_m16815BDDD78D3DC8836D6929D7ECA0287567E1C9,
	AdjustSessionFailure_set_Timestamp_m4620F96554EF0DBF543BF574C3B9E2CBEA0BF46E,
	AdjustSessionFailure_get_WillRetry_mDC6EF21BB9ED54A38E87A437F25B3E1ABFB64CB7,
	AdjustSessionFailure_set_WillRetry_m891830EFFC0F200C979980F639EF51F2357E6BCF,
	AdjustSessionFailure_get_JsonResponse_m3CC10F98CEFA48F10203B4B21CA8B7F48313E337,
	AdjustSessionFailure_set_JsonResponse_m9697C8316211570DED147C08CA044DB7A9626B6E,
	AdjustSessionFailure__ctor_m55084005614B14B05358BFC8D8093D0E1BA5D577,
	AdjustSessionFailure__ctor_mC8D3BF875D5D8A394B38A08DA6FD82FE78D65AB2,
	AdjustSessionFailure__ctor_mF96CCCD25D8F54F5FE37C1532E5A7D5B1FADEB3F,
	AdjustSessionFailure_BuildJsonResponseFromString_m2D4F30200FC6361CACC4417A512F8E14FF9C38A6,
	AdjustSessionFailure_GetJsonResponse_mE5D4C31B41ED1899C26AB32CD2648ADEFDE09351,
	AdjustSessionSuccess_get_Adid_m647C0D4B4E911D6C8BE1634A171F548461180414,
	AdjustSessionSuccess_set_Adid_m4393AA9B18910CE351BB43D1C510132B4F971573,
	AdjustSessionSuccess_get_Message_m86BB21FF8BEC5DA95055C3A12413D7CEAF1731EA,
	AdjustSessionSuccess_set_Message_mD680D8861FD8EE269D0994D51498AC2210694E99,
	AdjustSessionSuccess_get_Timestamp_mE2D213502F0F03A341B1E39DC4152AEF5C68F813,
	AdjustSessionSuccess_set_Timestamp_m2ED4611CC016044E197BF515B3A7C81C27B207EA,
	AdjustSessionSuccess_get_JsonResponse_m13404EAE48C660945ED5BBC50A26E9AB2E4B8595,
	AdjustSessionSuccess_set_JsonResponse_mCFFE1E0F01BD95837EE0A4E9D89CE5913C3E0FBC,
	AdjustSessionSuccess__ctor_m5D4F0E9806EDCE8130DE98471E7ECA654B744F9A,
	AdjustSessionSuccess__ctor_m468034512A1D2682AA0F15926CE8CA80F239C31D,
	AdjustSessionSuccess__ctor_mFD79CF038E807DE1559B54362B6E87EFAEFCD542,
	AdjustSessionSuccess_BuildJsonResponseFromString_m2CA7E40EDAD331AE6DEDF385D364682D7AC8ACCE,
	AdjustSessionSuccess_GetJsonResponse_m22B1531644212867F4EFF412E5B90CC8F7A15C5D,
	AdjustThirdPartySharing__ctor_mD050F802304C5E3A20E88D7C1F8AE85586641A82,
	AdjustThirdPartySharing_addGranularOption_m430DCE18F822237234F208C6FFD6C7837A2A1A77,
	AdjustThirdPartySharing_addPartnerSharingSetting_m46C4F5606AF8CE842EFA05FD126197ACCEC911E1,
	AdjustUrlStrategyExtension_ToLowerCaseString_mC501B171FABC8E81E217A019B01F9D079D4DC7A0,
	AdjustUtils_ConvertLogLevel_mF7D0CB4C0B08008E37686670B7361871B737A53F,
	AdjustUtils_ConvertBool_mBFC3BC841A003201C7056448C67C35625379E786,
	AdjustUtils_ConvertDouble_m328F7E087047FA52AEF1D681FCCD32D80791B749,
	AdjustUtils_ConvertInt_mE9AACF8054BA25B7605B3F8727091ED4F41CF37C,
	AdjustUtils_ConvertLong_m7B66091ED09C4DA947FB5C61D5AC40762100FAF4,
	AdjustUtils_ConvertListToJson_m0834067B90DD8AA9713B0A395933C806BDB84689,
	AdjustUtils_GetJsonResponseCompact_mB1763C6F6A17665BAA0534CE919BCFB7D7D491F6,
	AdjustUtils_GetJsonString_m7E4ABC127B656F2CF1D6D5C2973CCDC9345477A1,
	AdjustUtils_WriteJsonResponseDictionary_m45C6F803D1190D8144D7E3441A4CF870606463ED,
	AdjustUtils_TryGetValue_m3BF1818C3435B2DD8794C6BF52073DE2D50A57E9,
	AdjustUtils_GetSkad4ConversionValue_mF1B95F499F7AECC0987FA3A4DD57E10F9582741E,
	AdjustUtils_GetSkad4CoarseValue_m6A96D9597EAAD2D606A7B8730683A1870E324FCA,
	AdjustUtils_GetSkad4LockWindow_mE9E55E3A5B683CDF1BF463568133655A4BEEA39C,
	AdjustUtils__ctor_mEE74F3B9A26BAE12B3C426FF63604FD7396544A2,
	AdjustUtils__cctor_m4489DD780E5669549E8C7EDAF985BDEC7AC456E1,
};
static const int32_t s_InvokerIndices[1856] = 
{
	6893,
	5465,
	5494,
	5494,
	5494,
	5494,
	5494,
	5494,
	5494,
	6893,
	10695,
	6893,
	5494,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	5548,
	6893,
	6893,
	5385,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6762,
	6762,
	6893,
	6893,
	6893,
	6893,
	10695,
	6893,
	2192,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	10695,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	5494,
	10695,
	6893,
	5494,
	5494,
	5494,
	5494,
	5494,
	5494,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6762,
	5465,
	6762,
	5494,
	4880,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	5465,
	6893,
	6654,
	6893,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	4894,
	6893,
	6893,
	6893,
	6893,
	10611,
	10471,
	6893,
	6893,
	6893,
	6893,
	5494,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	1457,
	6893,
	1457,
	6893,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6893,
	6893,
	5494,
	5494,
	3214,
	3214,
	1618,
	1618,
	1639,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	4880,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	5494,
	4880,
	4880,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6762,
	6762,
	6893,
	10695,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	5465,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	5465,
	6893,
	6893,
	6893,
	3214,
	3214,
	1618,
	1618,
	1639,
	6893,
	6893,
	6893,
	6893,
	5494,
	5494,
	6893,
	6893,
	6893,
	6893,
	5494,
	6893,
	5494,
	5494,
	5494,
	5494,
	5465,
	6893,
	6893,
	6893,
	6893,
	5465,
	6893,
	6893,
	6893,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	5494,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	5494,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	5494,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	5494,
	6762,
	6893,
	6893,
	2165,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	4880,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6821,
	6893,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	6762,
	6893,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	5494,
	6893,
	6893,
	6893,
	6732,
	6893,
	10646,
	10481,
	6893,
	6893,
	6893,
	6893,
	6893,
	5494,
	6762,
	6883,
	6893,
	6893,
	5465,
	6893,
	10646,
	10481,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	10695,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	1693,
	6893,
	6893,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	10695,
	6893,
	6893,
	10646,
	10481,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	6654,
	6893,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	10646,
	10481,
	6893,
	6893,
	6893,
	5465,
	6732,
	6732,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	5465,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	6893,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6821,
	5465,
	6893,
	6893,
	6762,
	6893,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	5465,
	6893,
	6893,
	6732,
	6893,
	6893,
	6893,
	5465,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	5494,
	6893,
	6893,
	6762,
	6893,
	6762,
	5465,
	6654,
	6654,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	5465,
	6893,
	6893,
	6732,
	6893,
	6893,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	6893,
	6883,
	6821,
	6642,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6821,
	6893,
	6893,
	6893,
	6762,
	6893,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6821,
	6821,
	6893,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	6893,
	6893,
	5494,
	5494,
	5494,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	6654,
	6893,
	6893,
	5465,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6762,
	5548,
	5548,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	6893,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6821,
	6821,
	6821,
	6821,
	6893,
	6893,
	6893,
	6762,
	6893,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	5494,
	6893,
	5494,
	5494,
	5494,
	6893,
	6893,
	6893,
	5465,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	5548,
	5548,
	6893,
	10646,
	10481,
	6893,
	6893,
	2461,
	4880,
	6893,
	6893,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	10646,
	10481,
	6893,
	6893,
	6893,
	6893,
	6893,
	5494,
	2832,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	5494,
	5494,
	5465,
	6732,
	5465,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	8717,
	8717,
	8707,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	10262,
	9801,
	10055,
	9212,
	9212,
	10481,
	8267,
	8245,
	9212,
	6893,
	6893,
	5494,
	5494,
	5494,
	5494,
	5494,
	5494,
	6893,
	5385,
	5385,
	6893,
	5494,
	5494,
	5494,
	5494,
	6893,
	6893,
	6893,
	5465,
	6893,
	6893,
	6893,
	6893,
	6893,
	2832,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6654,
	5385,
	6762,
	6893,
	6893,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	5548,
	6893,
	6893,
	6893,
	6893,
	6893,
	5385,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	0,
	6893,
	6893,
	5494,
	6893,
	0,
	6893,
	6893,
	6893,
	5494,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	5494,
	5494,
	6893,
	5385,
	6893,
	6762,
	5494,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	5605,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6893,
	6893,
	6893,
	6762,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6893,
	6732,
	6893,
	6893,
	6893,
	5494,
	6893,
	10646,
	10481,
	10481,
	10481,
	6893,
	6893,
	6893,
	6893,
	6893,
	6762,
	5465,
	6893,
	6654,
	6893,
	6893,
	10481,
	10481,
	10646,
	10481,
	6732,
	5465,
	6893,
	6893,
	5465,
	5465,
	6893,
	10481,
	10481,
	10481,
	10481,
	10481,
	10481,
	6732,
	5465,
	6654,
	5385,
	6893,
	6893,
	6893,
	6893,
	6893,
	2832,
	6893,
	6893,
	5465,
	6893,
	6893,
	6893,
	6893,
	6654,
	6654,
	6893,
	6762,
	6732,
	6654,
	6893,
	6762,
	6762,
	6762,
	6762,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	10646,
	10481,
	10481,
	10481,
	6732,
	5465,
	6893,
	6762,
	5465,
	6893,
	6893,
	6893,
	6893,
	10646,
	10481,
	10481,
	10481,
	6893,
	6893,
	6893,
	6893,
	6893,
	5494,
	4880,
	5605,
	4874,
	6893,
	6893,
	10695,
	6893,
	3917,
	10646,
	10481,
	6893,
	6893,
	1368,
	6762,
	1643,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	10646,
	10481,
	6893,
	3152,
	6732,
	6732,
	6893,
	6893,
	6893,
	5548,
	6893,
	6893,
	10646,
	10481,
	10481,
	10481,
	6762,
	5494,
	6893,
	5605,
	5494,
	5494,
	6893,
	5494,
	6762,
	5605,
	6893,
	10695,
	6893,
	3917,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	10646,
	10481,
	6893,
	6893,
	6893,
	5494,
	6893,
	10481,
	10481,
	10481,
	10481,
	10481,
	10481,
	6821,
	6893,
	6893,
	6893,
	6893,
	6893,
	6893,
	5494,
	6893,
	6762,
	6893,
	5494,
	5385,
	6893,
	5676,
	5676,
	6893,
	5465,
	5465,
	6732,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	10481,
	10481,
	6893,
	6893,
	6893,
	5494,
	6893,
	6893,
	6893,
	3144,
	4874,
	2859,
	4880,
	3144,
	6762,
	5494,
	6732,
	5494,
	4880,
	4874,
	4880,
	6762,
	6762,
	6762,
	4880,
	6732,
	5465,
	6821,
	5548,
	6681,
	5415,
	6654,
	5385,
	6762,
	6762,
	10262,
	10262,
	9212,
	9212,
	3917,
	6732,
	10262,
	10262,
	5494,
	5494,
	5494,
	5494,
	6762,
	10262,
	10262,
	10262,
	10262,
	10262,
	10262,
	6893,
	5465,
	6893,
	6654,
	6762,
	6893,
	6762,
	6762,
	6762,
	5465,
	6893,
	6654,
	6893,
	6893,
	6762,
	6893,
	6762,
	6762,
	6762,
	4874,
	2859,
	4880,
	3144,
	6732,
	3144,
	4874,
	4880,
	6762,
	6762,
	6762,
	4880,
	5494,
	6893,
	5465,
	6893,
	6654,
	6893,
	6762,
	6893,
	6762,
	6762,
	6762,
	5465,
	6893,
	6654,
	6893,
	6762,
	6893,
	6762,
	4880,
	3144,
	4874,
	2859,
	6732,
	3144,
	4880,
	4874,
	4880,
	6762,
	6762,
	6762,
	4880,
	5494,
	6893,
	6893,
	3648,
	5465,
	6893,
	6654,
	6893,
	6762,
	6893,
	6762,
	6762,
	6762,
	5465,
	6893,
	6654,
	6893,
	6762,
	6893,
	6762,
	6762,
	5494,
	5494,
	5548,
	5415,
	5385,
	5465,
	6762,
	4880,
	5494,
	5494,
	3144,
	5494,
	4874,
	2859,
	4880,
	3144,
	5494,
	3144,
	9212,
	9212,
	3917,
	6732,
	6762,
	4880,
	6732,
	5465,
	6821,
	5548,
	6681,
	5415,
	6654,
	5385,
	6762,
	6762,
	10262,
	7079,
	7118,
	10477,
	10638,
	10477,
	10481,
	10481,
	10646,
	10646,
	10646,
	10695,
	10695,
	10646,
	10695,
	9814,
	9814,
	10481,
	10481,
	10695,
	10695,
	9814,
	7141,
	7142,
	8981,
	10477,
	7088,
	10481,
	10477,
	9741,
	8333,
	10695,
	10638,
	10695,
	10695,
	10646,
	8370,
	6893,
	10481,
	10481,
	10471,
	10611,
	10471,
	10695,
	10481,
	9814,
	9814,
	10481,
	10481,
	10695,
	10695,
	9814,
	10481,
	10481,
	10481,
	10471,
	10481,
	10477,
	9741,
	8331,
	10695,
	10638,
	10481,
	10646,
	10646,
	10646,
	10695,
	10695,
	10646,
	10646,
	9814,
	10481,
	10481,
	10481,
	6893,
	5385,
	10481,
	10481,
	10471,
	10611,
	10471,
	10481,
	10695,
	10695,
	10481,
	10695,
	9814,
	9814,
	10481,
	10481,
	10695,
	10695,
	9814,
	10481,
	10481,
	10481,
	10481,
	10471,
	9814,
	10477,
	8981,
	7772,
	10695,
	10638,
	10646,
	10646,
	10646,
	10646,
	10646,
	10481,
	10481,
	10646,
	10646,
	9023,
	9814,
	5494,
	5494,
	5494,
	5494,
	5494,
	5494,
	5494,
	5494,
	5494,
	5494,
	5494,
	5494,
	10611,
	10481,
	6893,
	5494,
	2591,
	5465,
	5494,
	5494,
	5494,
	3144,
	3144,
	1643,
	1122,
	5494,
	5494,
	3144,
	3144,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6554,
	5296,
	6762,
	5494,
	6762,
	5494,
	6893,
	5494,
	5494,
	3136,
	1614,
	5465,
	5494,
	5494,
	5385,
	5385,
	5385,
	5385,
	5385,
	5415,
	5494,
	5385,
	5494,
	514,
	3144,
	6762,
	3144,
	6762,
	3144,
	6762,
	3144,
	6762,
	3144,
	6762,
	3144,
	6762,
	5385,
	5385,
	5385,
	6893,
	5385,
	3144,
	6762,
	3144,
	6762,
	5465,
	5494,
	5385,
	5385,
	5494,
	5385,
	5385,
	5494,
	10258,
	5494,
	2591,
	3144,
	3144,
	5494,
	5494,
	5494,
	3144,
	5494,
	5494,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6654,
	5385,
	6762,
	5494,
	6893,
	5494,
	5494,
	5494,
	6762,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6893,
	5494,
	5494,
	5494,
	6762,
	10258,
	10258,
	3144,
	273,
	5494,
	3144,
	3144,
	6732,
	5465,
	6762,
	5494,
	6762,
	5494,
	6893,
	5494,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6654,
	5385,
	6762,
	5494,
	6893,
	5494,
	5494,
	5494,
	6762,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6762,
	5494,
	6893,
	5494,
	5494,
	5494,
	6762,
	5295,
	1643,
	1636,
	10258,
	10144,
	10145,
	10098,
	10146,
	10176,
	10262,
	10262,
	9463,
	9814,
	9463,
	10164,
	10262,
	10055,
	6893,
	10695,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1856,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
